/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { combineReducers } from 'redux-immutable';
import { reducer as formReducer } from 'redux-form/immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import sessionReducer from 'blocks/session/reducer';
import { LOG_OUT_SUCCESS } from 'blocks/session/constants';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
      });
    default:
      return state;
  }
}

function reducers(asyncReducers){
  return combineReducers({
    route: routeReducer,
    language: languageProviderReducer,
    session: sessionReducer,
    form: formReducer,
    ...asyncReducers,
  });
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers) {
  const appReducer = (state, action) => {
    if(action.type === LOG_OUT_SUCCESS){
      state = undefined;
    }
    return reducers(asyncReducers)(state, action);
  }

  return appReducer;
}