import React from 'react';
import App from 'containers/App';
import { name, module } from 'utils/tools';
import schema from './schema';
import schema_1 from './schema_1';
import formSchemaActions from 'blocks/forms/actions.js';
import formSchemaSelector from 'blocks/forms/selectors.js';

const patientsColumns = schema().patients().columns;
const documentsColumn = schema().documents().columns;
const coursesColumn = schema().courses().columns;
const patientCoursesColumn = schema().patientCourses().columns;
const ciicoursesColumn = schema().ciicourses().columns;
const ordersColumns = schema().orders('dentist').columns;
const ordersProducts = schema().orders().products;
const productColumns = schema().orders().productPayment;
const productNotesColumns = schema().orders().productNotes;
const vendorColumns = schema().orders().vendorPayment;
const paymentColumns = schema().orders().paymentDetails;
const shippingColumns = schema().shipping().columns;
const requestConsultationColumns = schema().patients().requestConsultation;
const requestConsultationColumn = schema().requestConsultation().columns;
const insuranceColumns = schema().insurance().columns;

const reportsColumns = schema_1().reports();
const reportsForms = schema_1().reportsForm();
const implantLogColumns = schema_1().implantLog().columns;
const boneGraftDataColumns = schema_1().boneGraftLog().columns;
const ledgerColumn = schema_1().ledger().columns;
const educationColumns = schema_1().dentistEducationRequests().columns;
const tutorialsColumns = schema_1().tutorialsList().columns;
const consultationsColumns = schema_1().consultations().columns;
const inOfficeSupportColumns = schema_1().inOfficeSupport().columns;

export default function (simpleLazyLoadedRoute) {
  const pages = [
    simpleLazyLoadedRoute({
      path: 'dashboard',
      name: 'dashboard',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/dashboard`,
        title: 'Dashboard',
        icon: 'gauge',
      },
      require: ['DashboardPage/Dentist', 'analytics'],
      container: function DashboardPage(analytics) {
        return this('analytics', process.env.PUBLIC_PATH || '', false, analytics.actions, analytics.selectors);
      }
    }),
    simpleLazyLoadedRoute({
      path: 'reports',
      name: 'reports',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/reports`,
        title: 'Reports',
        icon: 'article',
      },
      require: ['ReportsPage', 'reports', 'depositSlip', 'unScheduledTreatmentPlan', 'outstandingInsurance', 'PatientBalance', 'totalCollectedAmount', 'PatientType', 'recallReport', 'referralSource', 'referredToo', 'newPatient', 'providerAppointment', 'cancelReport', 'implantData'],
      container: function RecordsPage(reports, ...children) {
        return this('reports', `${process.env.PUBLIC_PATH || ''}/reports`, reportsColumns, reportsForms, reports.selectors, Object.keys(reportsColumns).reduce((a, el) => Object.assign({}, a, {[el]: Object.keys(reportsColumns[el]).map(col => children.filter(_=>_.name === col)[0])}), {}));
      },
    }),
    simpleLazyLoadedRoute({
      path: 'ledger',
      name: 'ledger',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/ledger`,
        title: 'Ledger',
        imgIcon: true,
        icon: 'ledger',
      },
      multiBlocks: true,
      require: ['LedgerPage', 'ledger'],
      container: function LedgerPage(ledger) {
        return this('ledger', `${process.env.PUBLIC_PATH || ''}/ledger`, ledgerColumn, ledger.actions, ledger.selectors);
      },
    }),
    simpleLazyLoadedRoute({
      path: 'setup',
      name: 'setup',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/setup`,
        title: 'Practice Setup',
        icon: 'cog-outline',
      },
      require: ['RecordsPage', 'setup', 'officeInfo', 'workingHours', 'operatories', 'providers', 'staffMembers', 'nonWorkingDays', 'referralSources','referredTo','dentalCarrier', 'insuranceFee', 'preMadeClinicalNotes', 'saas', 'settings'],
      route: true,
      container: function RecordsPage(module, ...children) {
        return this('setup' , `${process.env.PUBLIC_PATH || ''}/setup`, module.actions, module.selectors, children);
      }
    }),
    /*simpleLazyLoadedRoute({
      path: 'insurance',
      name: 'insurance',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/insurance`,
        title: 'Fee Schedule',
        icon: 'road',
      },
      require: ['InsurancePage', 'insurance'],
      container: function InsurancePage(insurance) {
        return this('insurance', `${process.env.PUBLIC_PATH || ''}/insurance`, insuranceColumns, insurance.actions, insurance.selectors);
      },
    }),*/
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
      path: 'patients',
      name: 'patients',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/patients`,
        title: 'Patients',
        icon: 'group',
      },
      require: ['PatientsPage', 'patients'],
      container: function RecordsPage(patients) {
        return this('patients', `${process.env.PUBLIC_PATH || ''}/patients`, true, patientsColumns, Object.assign({}, patients.actions, formSchemaActions), { selectors: patients.selectors, selectFormSchema: formSchemaSelector });
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'patients.create',
          require: ['CreatePatientPage', 'patients'],
          container: function CreateRecordPage(patients) {
            return this('patients.create', `${process.env.PUBLIC_PATH || ''}/patients`, patientsColumns, { selectRecord: patients.selectors.selectRecord, selectFormSchema: formSchemaSelector }, patients.actions.createRecord, patients.selectors.selectRecords, patients.actions, patients.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'patients.view',
          require: ['ViewPatientsPage', 'patients'],
          container: function ViewPatientsPage(patients) {
            return this(`${process.env.PUBLIC_PATH || ''}/patients`, patientsColumns, patients.actions.updateRecord, { selectRecord: patients.selectors.selectRecord, selectFormSchema: formSchemaSelector }, patients.actions);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/form',
          name: 'patients.view',
          require: ['ViewPatientsForm', 'patients', 'consultations'],
          container: function ViewPatientsForm(patients, consultations) {
            return this(`${process.env.PUBLIC_PATH || ''}/patients`, patientsColumns, requestConsultationColumns, patients.actions.updateRecord, { selectRecords: patients.selectors.selectRecords, selectRecord: patients.selectors.selectRecord, selectFormSchema: formSchemaSelector, selectRecordsMetaData: patients.selectors.selectRecordsMetaData }, patients.actions, patients.selectors.selectLoading, patients.selectors.pageLoading, patients.selectors.selectError, patients.selectors.selectUpdateError, consultations.actions, patients.selectors.selectExternalRecords);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/:formId/edit',
          name: 'patients.edit',
          require: ['EditPatientPage', 'patients'],
          container: function EditPatientPage(patients) {
            const view = true;
            return this('patients.edit', `${process.env.PUBLIC_PATH || ''}/patients`, patientsColumns, patients.actions.updateRecord, { selectRecord: patients.selectors.selectRecord, selectFormSchema: formSchemaSelector }, patients.selectors.selectUpdateError, view, patients.actions, patients.selectors.selectExternalRecords);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({

      path: 'documents',
      name: 'documents',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/documents`,
        title: 'Resource Ctr',
        icon: 'docs',
      },
      require: ['DocumentsPage', 'documents'],
      container: function DocumentsPage(documents) {
        return this('documents', `${process.env.PUBLIC_PATH || ''}/documents`, false, documentsColumn, documents.actions, documents.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'documents.view',
          require: ['ViewDocumentsPage', 'documents'],
          container: function ViewDocumentsPage(documents) {
            return this('documents', `${process.env.PUBLIC_PATH || ''}/documents`, documentsColumn, documents.actions.updateRecord, { selectRecords: documents.selectors.selectRecords, selectRecordsMetaData: documents.selectors.selectRecordsMetaData }, documents.actions, documents.selectors.selectLoading, documents.selectors.selectError, documents.selectors.selectUpdateError);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
     path: 'implantLog',
     name: 'implantLog',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/implantLog`,
        title: 'Implant Logbook',
        imgIcon: true,
        icon: 'implant',
      },
      require: ['RecordsPage', 'implantLog'],
      container: function RecordsPage(implantLog) {
        return this('implantLog', `${process.env.PUBLIC_PATH || ''}/implantLog`, false, implantLogColumns, implantLog.actions, implantLog.selectors, true);
      },
    }),
    simpleLazyLoadedRoute({
     path: 'boneGraftLog',
     name: 'boneGraftLog',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/boneGraftLog`,
        title: 'Bone Graft Logbook',
        icon: 'database',
      },
      require: ['RecordsPage', 'boneGraftLog'],
      container: function RecordsPage(boneGraftLog) {
        return this('boneGraftLog', `${process.env.PUBLIC_PATH || ''}/boneGraftLog`, false, boneGraftDataColumns, boneGraftLog.actions, boneGraftLog.selectors, true);
      },
    }),
    simpleLazyLoadedRoute({
      path: 'patient-courses',
      name: 'patientCourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/patient-courses`,
        title: 'Patient Ed',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/courses'
        }
      },
      require: ['DocumentsPage', 'patientCourses'],
      container: function DocumentsPage(courses) {
        return this('Patient Education', `${process.env.PUBLIC_PATH || ''}/patient-courses`, false, patientCoursesColumn, courses.actions, courses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'patient-courses.view',
          require: ['ViewDocumentsPage', 'patientCourses'],
          container: function ViewDocumentsPage(courses) {
            return this('patient-courses', `${process.env.PUBLIC_PATH || ''}/patient-courses`, patientCoursesColumn, courses.actions.updateRecord, { selectRecords: courses.selectors.selectRecords, selectRecordsMetaData: courses.selectors.selectRecordsMetaData }, courses.actions, courses.selectors.selectLoading, courses.selectors.selectError, courses.selectors.selectUpdateError);
          },
        }),
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    {
      data: true,
      type: 'menu',
      name: 'mixpanel',
      icon: 'icon-fontello-money',
      label: 'Patient Financing',
      id: 'patientFinancing',
      children: [
        {
          data: true,
          type: 'external-route',
          name: 'mixpanel',
          url: 'https://www.myalphaeoncredit.com',
          icon: 'icon-fontello-credit-card',
          label: 'Alphaeon',
        },
        {
          data: true,
          type: 'external-route',
          name: 'mixpanel',
          url: 'https://www.lendingclub.com/patientsolutions/',
          icon: 'icon-fontello-dollar',
          label: 'Lending Club',
        },
        {
          data: true,
          type: 'external-route',
          name: 'mixpanel',
          url: 'https://www.carecredit.com/providercenter/',
          icon: 'icon-fontello-home',
          label: 'Care Credit',
        }
      ]
    },
    {
      data: true,
      type: 'external-route',
      name: 'mixpanel',
      icon: 'icon-fontello-shield',
      label: 'eClaims',
      id: 'eclaims',
      url: 'https://dental.changehealthcare.com/dps/securelogin.aspx',
    },
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
      path: 'catalog',
      name: 'catalog',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/catalog`,
        title: 'Catalog',
        icon: 'cart',
      },
      require: ['DentistCatalogPage', 'vendors'],
      container: function DentistCatalogPage(vendors) {
        return this(vendors.actions, vendors.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'catalog.shop',
          require: ['DentistShopPage', 'products'],
          container: function ViewOrderPage(products) {
            return this(products.actions);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'orders',
      name: 'orders',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/orders`,
        title: 'Orders',
        icon: 'truck',
      },
      require: ['RecordsPage', 'orders'],
      container: function RecordsPage(orders) {
        return this('orders', `${process.env.PUBLIC_PATH || ''}/orders`, false, ordersColumns, orders.actions, orders.selectors, true);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'orders.view',
          require: ['ViewOrderPage', 'orders'],
          container: function ViewOrderPage(orders) {
            return this(`${process.env.PUBLIC_PATH || ''}/orders`, Object.assign({}, { ordersColumns, productColumns, vendorColumns, paymentColumns, productNotesColumns }), ordersProducts, orders.actions.updateRecord, orders.selectors.selectRecord, false);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'shipping',
      name: 'shipping',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/shipping`,
        title: 'Shipping Setup',
        icon: 'road',
      },
      require: ['RecordsPage', 'shipping'],
      container: function RecordsPage(shipping) {
        return this('shipping', `${process.env.PUBLIC_PATH || ''}/shipping`, true, shippingColumns, shipping.actions, shipping.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'shipping.create',
          require: ['CreateRecordPage', 'shipping'],
          container: function CreateRecordPage(shipping) {
            return this('shipping.create', `${process.env.PUBLIC_PATH || ''}/shipping`, shippingColumns, shipping.selectors.selectRecord, shipping.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'shipping.view',
          require: ['ViewRecordPage', 'shipping'],
          container: function ViewRecordPage(shipping) {
            return this(`${process.env.PUBLIC_PATH || ''}/shipping`, shippingColumns, shipping.actions.deleteRecord, shipping.selectors.selectRecord, shipping.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'shipping.edit',
          require: ['EditRecordPage', 'shipping'],
          container: function EditRecordPage(shipping) {
            const view = true;
            return this('shipping.edit', `${process.env.PUBLIC_PATH || ''}/shipping`, shippingColumns, shipping.actions.updateRecord, shipping.selectors.selectRecord, shipping.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({

      path: 'courses',
      name: 'courses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/courses`,
        title: 'Continuing Ed',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/courses'
        }
      },
      require: ['DocumentsPage', 'courses'],
      container: function DocumentsPage(courses) {
        return this('courses', `${process.env.PUBLIC_PATH || ''}/courses`, false, coursesColumn, courses.actions, courses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'courses.view',
          require: ['ViewDocumentsPage', 'courses'],
          container: function ViewDocumentsPage(courses) {
            return this('courses', `${process.env.PUBLIC_PATH || ''}/courses`, coursesColumn, courses.actions.updateRecord, { selectRecords: courses.selectors.selectRecords, selectRecordsMetaData: courses.selectors.selectRecordsMetaData }, courses.actions, courses.selectors.selectLoading, courses.selectors.selectError, courses.selectors.selectUpdateError);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'ceCourses',
      name: 'ceCourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/ceCourses`,
        title: 'My CE Courses',
        icon: 'inbox',
      },
      require: ['RecordsPage', 'dentistEducationRequests'],
      container: function RecordsPage(ceCourses) {
        return this('ceRequests', `${process.env.PUBLIC_PATH || ''}/ceCourses`, false, educationColumns, ceCourses.actions, ceCourses.selectors);
      },
    }),
    simpleLazyLoadedRoute({
      path: 'ciicourses',
      name: 'ciicourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/ciicourses`,
        title: 'Implant Education',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/ciicourses'
        }
      },
      require: ['DocumentsPage', 'ciicourses'],
      container: function DocumentsPage(ciicourses) {
        return this('Implant Education', `${process.env.PUBLIC_PATH || ''}/ciicourses`, false, ciicoursesColumn, ciicourses.actions, ciicourses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'ciicourses.view',
          require: ['ViewDocumentsPage', 'ciicourses'],
          container: function ViewDocumentsPage(ciicourses) {
            return this('ciicourses', `${process.env.PUBLIC_PATH || ''}/ciicourses`, ciicoursesColumn, ciicourses.actions.updateRecord, { selectRecords: ciicourses.selectors.selectRecords, selectRecordsMetaData: ciicourses.selectors.selectRecordsMetaData }, ciicourses.actions, ciicourses.selectors.selectLoading, ciicourses.selectors.selectError, ciicourses.selectors.selectUpdateError);
          },
        }),
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
     path: 'consultations',
      name: 'consultations',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/consultations`,
        title: 'Consultations',
        icon: 'clock',
      },
      require: ['RecordsPage', 'consultations'],
      container: function RecordsPage(consultations) {
        return this('consultations', `${process.env.PUBLIC_PATH || ''}/consultations`, false, consultationsColumns, consultations.actions, consultations.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'consultations.view',
          require: ['ViewConsultationPage', 'consultations'],
          container: function ViewConsultationPage(consultations) {
            return this(`${process.env.PUBLIC_PATH || ''}/consultations`, consultationsColumns, consultations.actions.updateRecord, { selectRecord: consultations.selectors.selectRecord, selectFormSchema: formSchemaSelector }, Object.assign({}, consultations.actions, formSchemaActions));
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'consultations.edit',
          require: ['EditRecordPage', 'consultations'],
          container: function EditRecordPage(consultations) {
            const view = true;
            return this('consultations.edit', `${process.env.PUBLIC_PATH || ''}/consultations`, consultationsColumns, consultations.actions.updateRecord, consultations.selectors.selectRecord, consultations.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'inOfficeSupport',
      name: 'inOfficeSupport',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`,
        title: 'In-office Support',
        icon: 'comment',
        isHiddenInNavigation: true,
      },
      require: ['RecordsPage', 'inOfficeSupport'],
      container: function RecordsPage(inOfficeSupport) {
        return this('inOfficeSupport', `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, true, inOfficeSupportColumns, inOfficeSupport.actions, inOfficeSupport.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'inOfficeSupport.create',
          require: ['RequestConsultationPage', 'inOfficeSupport', 'emailMappings'],
          container: function RecordsPage(inOfficeSupport, emailMappings) {
            return this('requestConsultation', `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, requestConsultationColumn, inOfficeSupport, emailMappings);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'inOfficeSupport.view',
          require: ['ViewConsultationPage', 'inOfficeSupport'],
          container: function ViewConsultationPage(inOfficeSupport) {
            return this(`${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, inOfficeSupportColumns, inOfficeSupport.actions.updateRecord, { selectRecord: inOfficeSupport.selectors.selectRecord, selectFormSchema: formSchemaSelector }, Object.assign({}, inOfficeSupport.actions, formSchemaActions));
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'inOfficeSupport.edit',
          require: ['EditRecordPage', 'inOfficeSupport'],
          container: function EditRecordPage(inOfficeSupport) {
            const view = true;
            return this('inOfficeSupport.edit', `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, inOfficeSupportColumns, inOfficeSupport.actions.updateRecord, inOfficeSupport.selectors.selectRecord, inOfficeSupport.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'requestConsultation',
      name: 'requestConsultation',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/requestConsultation`,
        title: 'In-office Support',
        icon: 'comment',
      },
      require: ['RequestConsultationPage', 'consultations', 'emailMappings'],
      container: function RecordsPage(consultations, emailMappings) {
        return this('requestConsultation', `${process.env.PUBLIC_PATH || ''}/requestConsultation`, requestConsultationColumn, consultations, emailMappings);
      }
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
      path: 'tutorials',
      name: 'tutorials',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/tutorials`,
        title: 'Tutorials',
        icon: 'road',
      },
      require: ['TutorialsForm', 'tutorials'],
      container: function InsurancePage(tutorials) {
        return this('tutorials', `${process.env.PUBLIC_PATH || ''}/tutorials`, tutorialsColumns, tutorials.actions, tutorials.selectors);
      },
    }),
    simpleLazyLoadedRoute({
      path: 'manuals',
      name: 'manuals',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/manuals`,
        title: 'Manuals',
        icon: 'docs',
      },
      require: ['TutorialsForm', 'manuals'],
      container: function InsurancePage(manuals) {
        return this('manuals', `${process.env.PUBLIC_PATH || ''}/manuals`, tutorialsColumns, manuals.actions, manuals.selectors);
      },
    }),
    {
      data: true,
      type: 'referFriend',
      icon: 'icon-fontello-user-plus',
      label: 'Refer a Friend',
    },
    {
      data: true,
      type: 'email',
      icon: 'mail',
      label: 'Contact Support'
    },
    {
      data: true,
      type: 'email',
      icon: 'mail',
      label: 'Sug. Improvements'
    },
    {
      data: true,
      type: 'password',
      name: 'password',
      icon: 'icon-fontello-cog-outline',
      label: 'Settings',
      id: 'settings',
    },
    {
      data: true,
      type: 'separator',
    },
    {
      data: true,
      type: 'version',
    },
  ];

  return module(simpleLazyLoadedRoute, pages);
}
