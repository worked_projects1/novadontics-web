export default function (simpleLazyLoadedRoute) {
  const formPages = [
    simpleLazyLoadedRoute({
      path: 'forgot',
      name: 'forgot',
      container: 'ForgotPasswordPage',
    }),
    simpleLazyLoadedRoute({
      path: 'reset/:token',
      name: 'reset',
      container: 'ResetPasswordPage',
    }),
  ];

  return {
    index: simpleLazyLoadedRoute({
      name: 'login',
      container: 'LoginPage',
    }),
    routes: {
      childRoutes: formPages,
    },
  };
}
