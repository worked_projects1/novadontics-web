import React from 'react';

import App from 'containers/App';

import schema from './schema';
import schema_1 from './schema_1';

import formSchemaActions from 'blocks/forms/actions.js';
import formSchemaSelector from 'blocks/forms/selectors.js';

import settingsActions from 'blocks/emailMappings/actions.js';
import AccountExpiry from "../components/AccountExpiry";
import SaasPayment from "../components/SaasPayment";
import VendorPrice from "../components/VendorPrice";

const ordersColumns = schema().orders().columns;
const ordersProducts = schema().orders().products;
const productColumns = schema().orders().productPayment;
const productNotesColumns = schema().orders().productNotes;
const vendorColumns = schema().orders().vendorPayment;
const paymentColumns = schema().orders().paymentDetails;
const categoriesColumns = schema().categories().columns;
const ciicategoriesColumns = schema().ciicategories().columns;
const patientsColumns = schema().patients().columns;
const productsColumns = schema().products().columns;
const accountColumns = schema().accounts().columns;
const practicesColumns = schema().practices().columns;
const documentsColumn = schema().documents().columns;
const coursesColumn = schema().courses().columns;
const ciicoursesColumn = schema().ciicourses().columns;
const patientCoursesColumn = schema().patientCourses().columns;
const vendorsColumn = schema().vendors().columns;
const vendorsViewDecorators = schema().vendors().viewDecorators;
const consentColumnFactory = schema().consents;
const prescriptionColumns = schema().prescriptions().columns;
const procedureColumns = schema().procedure().columns;
const educationColumns = schema().educationRequests().columns;
const requestConsultationColumns = schema().patients().requestConsultation;
const productCategoriesColumns = schema().productCategories().columns;

const manualSectionColumns = schema_1().manualSection().columns;
const purchasedCEColumn = schema_1().purchasedCE().columns;
const tutorialSectionColumns = schema_1().tutorialSection().columns;
const consultationsColumns = schema_1().consultations().columns;
const inOfficeSupportColumns = schema_1().inOfficeSupport().columns;
const reportsColumns = schema_1().adminReports();
const reportsForms = schema_1().adminReportsForm();
const saasColumns = schema_1().saasAdmin().columns;
const friendReferralColumns = schema_1().friendReferral().columns;
const emailMappingColumns = schema_1().emailMappings().columns;
const usersColumns = schema_1().users().columns;

const toSingularName = (name) => name && name.slice(-1) === 's' ? name.slice(0,-1) : name;

const ResourceCategoriesFactory = (simpleLazyLoadedRoute, resource, disableChildren) =>
    simpleLazyLoadedRoute({
      path: `resource-categories/${resource}`,
      name: `resource-categories/${resource}`,
      require: ['CategoriesPage', `resource-categories/${resource}`],
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/resource-categories/${resource}`,
        title: 'Resource Categories',
        isHiddenInNavigation: true
      },
      container: function RecordsPage(resourceCategories) {
        return this(`${toSingularName(resource)} Categories`, `${process.env.PUBLIC_PATH || ''}/resource-categories/${resource}`, true, categoriesColumns, resourceCategories.actions, resourceCategories.selectors, resourceCategories.actions.deleteRecord, resourceCategories.actions.updateRecords, disableChildren);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: `resource-categories.${resource}.create`,
          require: ['CreateRecordPage', `resource-categories/${resource}`],
          container: function CreateRecordPage(resourceCategories) {
            return this(`resource-categories/${resource}.create`, `${process.env.PUBLIC_PATH || ''}/resource-categories/${resource}`, (resource === 'ciicourses') ? ciicategoriesColumns : categoriesColumns, resourceCategories.selectors.selectRecord, resourceCategories.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: `resource-categories.${resource}.edit`,
          require: ['EditRecordPage', `resource-categories/${resource}`],
          container: function EditRecordPage(resourceCategories) {
            const view = false;
            return this(`resource-categories/${resource}.edit`, `${process.env.PUBLIC_PATH || ''}/resource-categories/${resource}`, (resource === 'ciicourses') ? ciicategoriesColumns : categoriesColumns, resourceCategories.actions.updateRecord, resourceCategories.selectors.selectRecord, resourceCategories.selectors.selectUpdateError, view);
          },
        }),
      ]
    });

export default function (simpleLazyLoadedRoute) {
  const pages = [
    simpleLazyLoadedRoute({
      path: 'practices',
      name: 'practices',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/practices`,
        title: 'Practices',
        icon: 'hospital',
      },
      require: ['RecordsPage', 'practices'],
      container: function RecordsPage(practices) {
        return this('practices', `${process.env.PUBLIC_PATH || ''}/practices`, true, practicesColumns, practices.actions, practices.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'practices.create',
          require: ['CreateRecordPage', 'practices'],
          container: function CreateRecordPage(practices) {
            return this('practices.create', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.selectors.selectRecord, practices.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'practices.view',
          require: ['ViewPracticesPage', 'practices'],
          container: function ViewPracticesPage(practices) {
            return this(`${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions.deleteRecord, practices.selectors.selectRecord, practices.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'practices.edit',
          require: ['EditRecordPage', 'practices'],
          container: function EditRecordPage(practices) {
            const view = true;
            return this('practices.edit', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions.updateRecord, practices.selectors.selectRecord, practices.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'accounts',
      name: 'accounts',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/accounts`,
        title: 'Accounts',
        icon: 'users',
      },
      require: ['RecordsPage', 'accounts'],
      container: function RecordsPage(accounts) {
        return this('accounts', `${process.env.PUBLIC_PATH || ''}/accounts`, true, accountColumns, accounts.actions, accounts.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'accounts.create',
          require: ['CreateRecordPage', 'accounts'],
          container: function CreateRecordPage(accounts) {
            return this('accounts.create', `${process.env.PUBLIC_PATH || ''}/accounts`, accountColumns, accounts.selectors.selectRecord, accounts.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'accounts.view',
          require: ['ViewRecordPage', 'accounts'],
          container: function ViewRecordPage(accounts) {
            return this(`${process.env.PUBLIC_PATH || ''}/accounts`, accountColumns, accounts.actions.deleteRecord, accounts.selectors.selectRecord, accounts.selectors.selectUpdateError, true, AccountExpiry);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'accounts.edit',
          require: ['EditRecordPage', 'accounts'],
          container: function EditRecordPage(accounts) {
            const view = true;
            return this('accounts.edit', `${process.env.PUBLIC_PATH || ''}/accounts`, accountColumns, accounts.actions.updateRecord, accounts.selectors.selectRecord, accounts.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'reports',
      name: 'reports',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/reports`,
        title: 'Reports',
        icon: 'article',
      },
      multiBlocks: true,
      require: ['ReportsPage', 'reports', 'catalogOrdersRevenue', 'consultationsRevenue', 'inOfficeSupportRevenue', 'orderHistory'],
      container: function RecordsPage(reports, ...children) {
        return this('reports', `${process.env.PUBLIC_PATH || ''}/reports`, reportsColumns, reportsForms, reports.selectors, Object.keys(reportsColumns).reduce((a, el) => Object.assign({}, a, {[el]: Object.keys(reportsColumns[el]).map(col => children.filter(_=>_.name === col)[0])}), {}));
      },
    }),
    {
      data: true,
      type: 'external-route',
      name: 'mixpanel',
      url: 'https://app.segment.com/login/',
      icon: 'icon-fontello-chart-pie',
      label: 'Analytics',
    },
    simpleLazyLoadedRoute({
      path: 'saasAdmin',
      name: 'saasAdmin',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/saasAdmin`,
        title: 'SaaS',
        icon: 'cog-outline',
      },
      require: ['RecordsPage', 'saasAdmin'],
      container: function RecordsPage(saasAdmin) {
        return this('saasAdmin', `${process.env.PUBLIC_PATH || ''}/saasAdmin`, true, saasColumns, saasAdmin.actions, saasAdmin.selectors, true);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'saasAdmin.create',
          require: ['CreateRecordPage', 'saasAdmin'],
          container: function CreateRecordPage(saasAdmin) {
            return this('saasAdmin.create', `${process.env.PUBLIC_PATH || ''}/saasAdmin`, saasColumns, saasAdmin.selectors.selectRecord, saasAdmin.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'saasAdmin.view',
          require: ['ViewRecordPage', 'saasAdmin'],
          container: function ViewRecordPage(saasAdmin) {
            return this(`${process.env.PUBLIC_PATH || ''}/saasAdmin`, saasColumns, saasAdmin.actions.deleteRecord, saasAdmin.selectors.selectRecord, saasAdmin.selectors.selectUpdateError, true, SaasPayment);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'saasAdmin.edit',
          require: ['EditRecordPage', 'saasAdmin'],
          container: function EditRecordPage(saasAdmin) {
            const view = true;
            return this('saasAdmin.edit', `${process.env.PUBLIC_PATH || ''}/saasAdmin`, saasColumns, saasAdmin.actions.updateRecord, saasAdmin.selectors.selectRecord, saasAdmin.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
      path: 'prescriptions',
      name: 'prescriptions',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/prescriptions`,
        title: 'Prescriptions',
        icon: 'beaker',
        categoriesButton: {
          path: '/resource-categories/prescriptions'
        },
      },
      require: ['RecordsPage', 'prescriptions'],
      container: function RecordsPage(prescriptions) {
        return this('prescriptions', `${process.env.PUBLIC_PATH || ''}/prescriptions`, true, prescriptionColumns, prescriptions.actions, prescriptions.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'prescriptions.create',
          require: ['CreateRecordPage', 'prescriptions'],
          container: function CreateRecordPage(prescriptions) {
            return this('prescriptions.create', `${process.env.PUBLIC_PATH || ''}/prescriptions`, prescriptionColumns, prescriptions.selectors.selectRecord, prescriptions.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'prescriptions.view',
          require: ['ViewRecordPage', 'prescriptions'],
          container: function ViewRecordPage(prescriptions) {
            return this(`${process.env.PUBLIC_PATH || ''}/prescriptions`, prescriptionColumns, prescriptions.actions.deleteRecord, prescriptions.selectors.selectRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'prescriptions.edit',
          require: ['EditRecordPage', 'prescriptions'],
          container: function EditRecordPage(prescriptions) {
            return this('prescriptions.edit', `${process.env.PUBLIC_PATH || ''}/prescriptions`, prescriptionColumns, prescriptions.actions.updateRecord, prescriptions.selectors.selectRecord, prescriptions.selectors.selectUpdateError, true);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'procedure',
      name: 'procedure',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/procedure`,
        title: 'CDT Codes',
        icon: 'newspaper',
        categoriesButton: {
          path: '/resource-categories/procedureCode'
        },
      },
      require: ['RecordsPage', 'procedure'],
      container: function RecordsPage(procedure) {
        return this('procedure', `${process.env.PUBLIC_PATH || ''}/procedure`, true, procedureColumns, procedure.actions, procedure.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'procedure.create',
          require: ['CreateRecordPage', 'procedure'],
          container: function CreateRecordPage(procedure) {
            return this('procedure.create', `${process.env.PUBLIC_PATH || ''}/procedure`, procedureColumns, procedure.selectors.selectRecord, procedure.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'procedure.view',
          require: ['ViewRecordPage', 'procedure'],
          container: function ViewRecordPage(procedure) {
            return this(`${process.env.PUBLIC_PATH || ''}/procedure`, procedureColumns, procedure.actions.deleteRecord, procedure.selectors.selectRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'procedure.edit',
          require: ['EditRecordPage', 'procedure'],
          container: function EditRecordPage(procedure) {
            return this('procedure.edit', `${process.env.PUBLIC_PATH || ''}/procedure`, procedureColumns, procedure.actions.updateRecord, procedure.selectors.selectRecord, procedure.selectors.selectUpdateError, true);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'documents',
      name: 'documents',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/documents`,
        title: 'Resource Ctr',
        icon: 'docs',
        categoriesButton: {
          path: '/resource-categories/documents'
        }
      },
      require: ['RecordsPage', 'documents'],
      container: function RecordsPage(documents) {
        return this('documents', `${process.env.PUBLIC_PATH || ''}/documents`, true, documentsColumn, documents.actions, documents.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'documents.create',
          require: ['CreateRecordPage', 'documents'],
          container: function CreateRecordPage(documents) {
            return this('documents.create', `${process.env.PUBLIC_PATH || ''}/documents`, documentsColumn, documents.selectors.selectRecord, documents.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'documents.view',
          require: ['ViewCoursesPage', 'documents'],
          container: function ViewDocumentsPage(documents) {
            return this(`${process.env.PUBLIC_PATH || ''}/documents`, documentsColumn, true, documents.actions.deleteRecord, documents.selectors.selectRecord, documents.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'documents.edit',
          require: ['EditRecordPage', 'documents'],
          container: function EditRecordPage(documents) {
            const view = true;
            return this('documents.edit', `${process.env.PUBLIC_PATH || ''}/documents`, documentsColumn, documents.actions.updateRecord, documents.selectors.selectRecord, documents.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'consultations',
      name: 'consultations',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/consultations`,
        title: 'Consultations',
        icon: 'clock',
      },
      require: ['RecordsPage', 'consultations'],
      container: function RecordsPage(consultations) {
        return this('consultations', `${process.env.PUBLIC_PATH || ''}/consultations`, false, consultationsColumns, consultations.actions, consultations.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'consultations.view',
          require: ['ViewConsultationPage', 'consultations'],
          container: function ViewConsultationPage(consultations) {
            return this(`${process.env.PUBLIC_PATH || ''}/consultations`, consultationsColumns, consultations.actions.updateRecord, { selectRecord: consultations.selectors.selectRecord, selectFormSchema: formSchemaSelector }, Object.assign({}, consultations.actions, formSchemaActions));
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'consultations.edit',
          require: ['EditRecordPage', 'consultations'],
          container: function EditRecordPage(consultations) {
            const view = true;
            return this('consultations.edit', `${process.env.PUBLIC_PATH || ''}/consultations`, consultationsColumns, consultations.actions.updateRecord, consultations.selectors.selectRecord, consultations.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'inOfficeSupport',
      name: 'inOfficeSupport',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`,
        title: 'In-office Support',
        icon: 'comment',
      },
      require: ['RecordsPage', 'inOfficeSupport'],
      container: function RecordsPage(inOfficeSupport) {
        return this('inOfficeSupport', `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, false, inOfficeSupportColumns, inOfficeSupport.actions, inOfficeSupport.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'inOfficeSupport.view',
          require: ['ViewConsultationPage', 'inOfficeSupport'],
          container: function ViewConsultationPage(inOfficeSupport) {
            return this(`${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, inOfficeSupportColumns, inOfficeSupport.actions.updateRecord, { selectRecord: inOfficeSupport.selectors.selectRecord, selectFormSchema: formSchemaSelector }, Object.assign({}, inOfficeSupport.actions, formSchemaActions));
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'inOfficeSupport.edit',
          require: ['EditRecordPage', 'inOfficeSupport'],
          container: function EditRecordPage(inOfficeSupport) {
            const view = true;
            return this('inOfficeSupport.edit', `${process.env.PUBLIC_PATH || ''}/inOfficeSupport`, inOfficeSupportColumns, inOfficeSupport.actions.updateRecord, inOfficeSupport.selectors.selectRecord, inOfficeSupport.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'patient-courses',
      name: 'patientCourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/patient-courses`,
        title: 'Patient Ed',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/patient-courses'
        }
      },
      require: ['RecordsPage', 'patientCourses'],
      container: function RecordsPage(courses) {
        return this('Patient Education', `${process.env.PUBLIC_PATH || ''}/patient-courses`, true, patientCoursesColumn, courses.actions, courses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'patient-courses.create',
          require: ['CreateRecordPage', 'patientCourses'],
          container: function CreateRecordPage(courses) {
            return this('patient-courses.create', `${process.env.PUBLIC_PATH || ''}/patient-courses`, patientCoursesColumn, courses.selectors.selectRecord, courses.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'patient-courses.view',
          require: ['ViewCoursesPage', 'patientCourses'],
          container: function ViewCoursesPage(courses) {
            return this(`${process.env.PUBLIC_PATH || ''}/patient-courses`, patientCoursesColumn, true, courses.actions.deleteRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'patient-courses.edit',
          require: ['EditRecordPage', 'patientCourses'],
          container: function EditRecordPage(courses) {
            const view = true;
            return this('patient-courses.edit', `${process.env.PUBLIC_PATH || ''}/patient-courses`, patientCoursesColumn, courses.actions.updateRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError, view);
          },
        })
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
      path: 'courses',
      name: 'courses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/courses`,
        title: 'Continuing Ed',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/courses'
        }
      },
      require: ['RecordsPage', 'courses'],
      container: function RecordsPage(courses) {
        return this('courses', `${process.env.PUBLIC_PATH || ''}/courses`, true, coursesColumn, courses.actions, courses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'documents.create',
          require: ['CreateRecordPage', 'courses'],
          container: function CreateRecordPage(courses) {
            return this('courses.create', `${process.env.PUBLIC_PATH || ''}/courses`, coursesColumn, courses.selectors.selectRecord, courses.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'courses.view',
          require: ['ViewCoursesPage', 'courses'],
          container: function ViewCoursesPage(courses) {
            return this(`${process.env.PUBLIC_PATH || ''}/courses`, coursesColumn, true, courses.actions.deleteRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'courses.edit',
          require: ['EditRecordPage', 'courses'],
          container: function EditRecordPage(courses) {
            const view = true;
            return this('courses.edit', `${process.env.PUBLIC_PATH || ''}/courses`, coursesColumn, courses.actions.updateRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError, view);
          },
        })
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'educationRequests',
      name: 'educationRequests',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/educationRequests`,
        title: 'CE Requests',
        icon: 'inbox',
      },
      require: ['RecordsPage', 'educationRequests'],
      container: function RecordsPage(education) {
        return this('Continuing Education Requests', `${process.env.PUBLIC_PATH || ''}/educationRequests`, true, educationColumns, education.actions, education.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'educationRequests.view',
          require: ['ViewRecordPage', 'educationRequests'],
          container: function ViewRecordPage(education) {
            return this(`${process.env.PUBLIC_PATH || ''}/educationRequests`, educationColumns, education.actions.deleteRecord, education.selectors.selectRecord, undefined, true);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'educationRequests.edit',
          require: ['EditRecordPage', 'educationRequests'],
          container: function EditRecordPage(education) {
            const view = true;
            return this('educationRequests.edit', `${process.env.PUBLIC_PATH || ''}/educationRequests`, educationColumns, education.actions.updateRecord, education.selectors.selectRecord, education.selectors.selectUpdateError, view);
          },
        })
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'purchasedCE',
      name: 'getPurchasedCECourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/purchasedCE`,
        title: 'Purchased CE',
        icon: 'newspaper',
      },
      multiBlocks: true,
      require: ['PurchasedCEPage', 'getPurchasedCECourses'],
      container: function PurchasedCEPage(purchasedCE) {
        return this('getPurchasedCECourses', `${process.env.PUBLIC_PATH || ''}/purchasedCE`, purchasedCEColumn, purchasedCE.actions, purchasedCE.selectors);
      },
    }),
    simpleLazyLoadedRoute({
      path: 'ciicourses',
      name: 'ciicourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/ciicourses`,
        title: 'Implant Education',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/ciicourses'
        }
      },
      require: ['RecordsPage', 'ciicourses'],
      container: function RecordsPage(courses) {
        return this('ciicourses', `${process.env.PUBLIC_PATH || ''}/ciicourses`, true, ciicoursesColumn, courses.actions, courses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'documents.create',
          require: ['CreateRecordPage', 'ciicourses'],
          container: function CreateRecordPage(courses) {
            return this('ciicourses.create', `${process.env.PUBLIC_PATH || ''}/ciicourses`, ciicoursesColumn, courses.selectors.selectRecord, courses.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'ciicourses.view',
          require: ['ViewCoursesPage', 'ciicourses'],
          container: function ViewCoursesPage(courses) {
            return this(`${process.env.PUBLIC_PATH || ''}/ciicourses`, ciicoursesColumn, true, courses.actions.deleteRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'ciicourses.edit',
          require: ['EditRecordPage', 'ciicourses'],
          container: function EditRecordPage(courses) {
            const view = true;
            return this('ciicourses.edit', `${process.env.PUBLIC_PATH || ''}/ciicourses`, ciicoursesColumn, courses.actions.updateRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError, view);
          },
        })
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'tutorials',
      name: 'tutorials',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/tutorials`,
        title: 'Tutorials',
        icon: 'hospital',
      },
      require: ['CategoriesPage', 'tutorials'],
      container: function RecordsPage(tutorials) {
        return this('tutorials', `${process.env.PUBLIC_PATH || ''}/tutorials`, true, tutorialSectionColumns, tutorials.actions, tutorials.selectors, tutorials.actions.deleteRecord, tutorials.actions.updateRecords);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'tutorials.create',
          require: ['CreateRecordPage', 'tutorials'],
          container: function CreateRecordPage(tutorials) {
            return this('tutorials.create', `${process.env.PUBLIC_PATH || ''}/tutorials`, tutorialSectionColumns, tutorials.selectors.selectRecord, tutorials.actions.createRecord, tutorials.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'tutorials.edit',
          require: ['EditRecordPage', 'tutorials'],
          container: function EditRecordPage(tutorials) {
            const view = false;
            return this('tutorials.edit', `${process.env.PUBLIC_PATH || ''}/tutorials/`, tutorialSectionColumns, tutorials.actions.updateRecord, tutorials.selectors.selectRecord, tutorials.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'manuals',
      name: 'manuals',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/manuals`,
        title: 'Manuals',
        icon: 'docs',
      },
      require: ['CategoriesPage', 'manuals'],
      container: function RecordsPage(manuals) {
        return this('manuals', `${process.env.PUBLIC_PATH || ''}/manuals`, true, manualSectionColumns, manuals.actions, manuals.selectors, manuals.actions.deleteRecord, manuals.actions.updateRecords);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'manuals.create',
          require: ['CreateRecordPage', 'manuals'],
          container: function CreateRecordPage(manuals) {
            return this('manuals.create', `${process.env.PUBLIC_PATH || ''}/manuals`, manualSectionColumns, manuals.selectors.selectRecord, manuals.actions.createRecord, manuals.selectors.selectUpdateError);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'manuals.edit',
          require: ['EditRecordPage', 'manuals'],
          container: function EditRecordPage(manuals) {
            const view = false;
            return this('manuals.edit', `${process.env.PUBLIC_PATH || ''}/manuals/`, manualSectionColumns, manuals.actions.updateRecord, manuals.selectors.selectRecord, manuals.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
      path: 'catalog',
      name: 'products.list',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/catalog`,
        title: 'Products',
        icon: 'clipboard',
      },
      require: ['RecordsPage', 'products'],
      container: function RecordsPage(products) {
        return this('products', `${process.env.PUBLIC_PATH || ''}/catalog`, true, productsColumns, products.actions, products.selectors, true);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'products.create',
          require: ['CreateRecordPage', 'products'],
          container: function CreateRecordPage(products) {
            return this('products.create', `${process.env.PUBLIC_PATH || ''}/catalog`, productsColumns, products.selectors.selectRecord, products.actions.createRecord,false,products.actions.updateRecordsMetaData);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'products.view',
          require: ['ViewCatalogPage', 'products'],
          container: function ViewCatalogPage(products) {
            return this(`${process.env.PUBLIC_PATH || ''}/catalog`, products.actions.deleteRecord, products.selectors.selectRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'products.edit',
          require: ['EditRecordPage', 'products'],
          container: function EditRecordPage(products) {
            const view = true;
            return this('products.edit', `${process.env.PUBLIC_PATH || ''}/catalog`, productsColumns, products.actions.updateRecord, products.selectors.selectRecord, products.selectors.selectUpdateError, view,products.actions.updateRecordsMetaData);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'categories',
      name: 'categories',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/categories`,
        title: 'Categories',
        icon: 'tag',
      },
      require: ['CategoriesPage', 'categories'],
      container: function RecordsPage(categories) {
        return this('categories', `${process.env.PUBLIC_PATH || ''}/categories`, true, productCategoriesColumns, categories.actions, categories.selectors, categories.actions.deleteRecord, categories.actions.updateRecords);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'categories.create',
          require: ['CreateRecordPage', 'categories'],
          container: function CreateRecordPage(categories) {
            return this('categories.create', `${process.env.PUBLIC_PATH || ''}/categories`, productCategoriesColumns, categories.selectors.selectRecord, categories.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'categories.edit',
          require: ['EditRecordPage', 'categories'],
          container: function EditRecordPage(categories) {
            const view = false;
            return this('categories.edit', `${process.env.PUBLIC_PATH || ''}/categories/`, productCategoriesColumns, categories.actions.updateRecord, categories.selectors.selectRecord, categories.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'vendors',
      name: 'vendors',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/vendors`,
        title: 'Vendors',
        icon: 'building',
        categoriesButton: {
          path: '/resource-categories/vendorGroups'
        },
      },
      require: ['RecordsPage', 'vendors'],
      container: function RecordsPage(vendors) {
        return this('vendors', `${process.env.PUBLIC_PATH || ''}/vendors`, true, vendorsColumn, vendors.actions, vendors.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'vendors.create',
          require: ['CreateRecordPage', 'vendors'],
          container: function CreateRecordPage(vendors) {
            return this('vendors.create', `${process.env.PUBLIC_PATH || ''}/vendors`, vendorsColumn, vendors.selectors.selectRecord, vendors.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'vendors.view',
          require: ['ViewRecordPage', 'vendors'],
          container: function ViewRecordPage(vendors) {
            return this(`${process.env.PUBLIC_PATH || ''}/vendors`, vendorsColumn, vendors.actions.deleteRecord, vendors.selectors.selectRecord, vendorsViewDecorators, true, VendorPrice);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'vendors.edit',
          require: ['EditRecordPage', 'vendors'],
          container: function EditRecordPage(vendors) {
            const view = true;
            return this('venodrs.edit', `${process.env.PUBLIC_PATH || ''}/vendors`, vendorsColumn, vendors.actions.updateRecord, vendors.selectors.selectRecord, vendors.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'patient-courses'),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'courses'),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'documents'),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'ciicourses'),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'prescriptions', true),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'vendorGroups'),
    ResourceCategoriesFactory(simpleLazyLoadedRoute, 'procedureCode'),
    simpleLazyLoadedRoute({
      path: 'orders',
      name: 'orders',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/orders`,
        title: 'Orders',
        icon: 'basket',
      },
      require: ['RecordsPage', 'orders'],
      container: function RecordsPage(orders) {
        return this('orders', `${process.env.PUBLIC_PATH || ''}/orders`, false, ordersColumns, orders.actions, orders.selectors, true);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'orders.view',
          require: ['ViewOrderPage', 'orders'],
          container: function ViewOrderPage(orders) {
            return this(`${process.env.PUBLIC_PATH || ''}/orders`, Object.assign({}, { ordersColumns, productColumns, vendorColumns, paymentColumns, productNotesColumns }), ordersProducts, orders.actions.updateRecord, orders.selectors.selectRecord, true);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'orders.edit',
          require: ['EditRecordPage', 'orders'],
          container: function EditRecordPage(orders) {
            const view = true;
            return this('orders.edit', `${process.env.PUBLIC_PATH || ''}/orders`, ordersColumns, orders.actions.updateRecord, orders.selectors.selectRecord, orders.selectors.selectUpdateError, view);
          },
        })
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    simpleLazyLoadedRoute({
     path: 'friendReferral',
     name: 'friendReferral',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/friendReferral`,
        title: 'Referrals',
        icon: 'user-add',
      },
      require: ['RecordsPage', 'friendReferral'],
      container: function RecordsPage(friendReferral) {
        return this('friendReferral', `${process.env.PUBLIC_PATH || ''}/friendReferral`, false, friendReferralColumns, friendReferral.actions, friendReferral.selectors);
      },
    }),
    simpleLazyLoadedRoute({
      path: 'settings',
      name: 'emailMappings',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/settings`,
        title: 'Settings',
        icon: 'wrench',
      },
      require: ['RecordsPage', 'emailMappings'],
      container: function RecordsPage(emailMappings) {
        return this('emailMappings', `${process.env.PUBLIC_PATH || ''}/settings`, false, emailMappingColumns, emailMappings.actions, emailMappings.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'emailMappings.view',
          require: ['ViewSettingsPage', 'emailMappings'],
          container: function ViewRecordPage(emailMappings) {
            return this(`${process.env.PUBLIC_PATH || ''}/settings`, emailMappingColumns, settingsActions.addUserEmail, settingsActions.removeUserEmail, emailMappings.selectors.selectRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'emailMappings.edit',
          require: ['EditRecordPage', 'emailMappings'],
          container: function EditRecordPage(emailMappings) {
            const view = true;
            return this('emailMappings.edit', `${process.env.PUBLIC_PATH || ''}/settings`, emailMappingColumns, emailMappings.actions.updateRecord, emailMappings.selectors.selectRecord, emailMappings.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'users',
      name: 'users',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/users`,
        title: 'Users',
        icon: 'user',
      },
      require: ['RecordsPage', 'users'],
      container: function RecordsPage(users) {
        return this('users', `${process.env.PUBLIC_PATH || ''}/users`, true, usersColumns, users.actions, users.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'users.create',
          require: ['CreateRecordPage', 'users'],
          container: function CreateRecordPage(users) {
            return this('users.create', `${process.env.PUBLIC_PATH || ''}/users`, usersColumns, users.selectors.selectRecord, users.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'users.view',
          require: ['ViewRecordPage', 'users'],
          container: function ViewRecordPage(users) {
            return this(`${process.env.PUBLIC_PATH || ''}/users`, usersColumns, users.actions.deleteRecord, users.selectors.selectRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'users.edit',
          require: ['EditRecordPage', 'users'],
          container: function EditRecordPage(users) {
            const view = true;
            return this('users.edit', `${process.env.PUBLIC_PATH || ''}/users`, usersColumns, users.actions.updateRecord, users.selectors.selectRecord, users.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    {
      data: true,
      type: 'separator',
    },
    {
      data: true,
      type: 'version',
    },
  ];

  return {
    index: simpleLazyLoadedRoute({
      require: ['DashboardPage/Admin', 'analytics'],
      container: function DashboardPage(analytics) {
        const Template = App(pages); // eslint-disable-line new-cap
        const Page = this('analytics', process.env.PUBLIC_PATH || '', false, analytics.actions, analytics.selectors);
        const Component = () => <Template><Page /></Template>;

        Component.await = Page.await;

        return Component;
      },
    }),
    routes: {
      component: App(pages), // eslint-disable-line new-cap
      childRoutes: pages,
    },
  };
}
