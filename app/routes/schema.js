import { archiveVendor, unarchiveVendor } from '../blocks/vendors/actions';
import React from 'react';
import moment from 'moment-timezone';
import { connect } from "react-redux";
import { createSelector } from "reselect";
import AccountExtendButton, { expirationNoticeText } from "../components/AccountExpiry";
import { currencyFormatter } from 'utils/currencyFormatter.js';

export default function schema() {

  function orders(role = false) {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: 'Orders #',
          visible: true,
          viewMode: true,
          editRecord: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Dentist',
          visible: true,
          viewMode: true,
          editRecord: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'practice',
          label: 'Practice',
          visible: true,
          viewMode: true,
          editRecord: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'location',
          label: 'Location',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 5,
          value: 'dateCreated',
          label: 'Order Date',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 6,
          value: 'dateFulfilled',
          label: 'Fulfill Date',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 7,
          value: 'phone',
          label: 'Phone',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 8,
          value: 'email',
          label: 'Email',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 9,
          value: 'grandTotal',
          label: 'Total',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 10,
          value: 'status',
          label: 'Status',
          visible: false,
          viewMode: false,
          editRecord: true,
          type: 'select',
          splitColumn: true,
          oneOf: (role === 'dentist') ? [
            'Received',
            'Canceled',
            'Returned'
          ] : [
            'Received',
            'Fulfilled',
            'Closed',
            'Canceled',
            'Returned'
          ],
        },
        {
          id: 11,
          value: 'approvedBy',
          label: 'Approved By',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
      ],
      products: [
        {
          id: 1,
          value: 'id',
          label: '#',
          viewRecord: false,
        },
        {
          id: 2,
          value: 'serialNumber',
          label: 'Reference Number',
          viewRecord: true,
        },
        {
          id: 3,
          value: 'name',
          label: 'Item',
          viewRecord: true,
        },
        {
          id: 4,
          value: 'image',
          image: true,
          viewRecord: false,
        },
        {
          id: 5,
          value: 'amount',
          label: 'Qty.',
          viewRecord: true,
        },
        {
          id: 6,
          value: 'price',
          label: 'Price',
          viewRecord: true,
        },
        {
          id: 7,
          value: 'totalPrice',
          label: 'Total',
          viewRecord: true,
        },
      ],
      productPayment: [
        {
          id: 1,
          value: 'paymentType',
          label: 'Payment Type',
          visible: false,
          viewMode: false,
          editRecord: true,
          type: 'select',
          onChange: true,
          selectOption: true,
          oneOf: [
            'Cash',
            'Credit Card',
            'Check'
          ],
        },
        {
          id: 2,
          value: 'checkNumber',
          label: 'Check Number',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'paymentCompleted',
          label: 'Payment Completed',
          visible: false,
          viewMode: false,
          editRecord: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
      ],
      productNotes: [
        {
          id: 1,
          value: 'note',
          label: 'Note',
          visible: false,
          viewMode: false,
          editRecord: true,
          type: 'textarea'
        },
      ],
      vendorPayment: [
        {
          id: 1,
          value: 'vendorName',
          label: 'Vendor Name',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: true,
          disabled: true,
          type: 'input'
        },
        {
          id: 2,
          value: 'invoiceNum',
          label: 'Vendor Invoice #',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 3,
          value: 'paymentType',
          label: 'Payment Type',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: true,
          type: 'select',
          onChange: true,
          oneOf: [
            'Cash',
            'Credit Card',
            'Check'
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 4,
          value: 'checkNumber',
          label: 'Check Number',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 5,
          value: 'amountPaid',
          label: 'Amount Paid',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: true,
          type: 'input',
          number: true,
          negative: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 6,
          value: 'paymentDate',
          label: 'Payment Date',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 7,
          value: 'notes',
          label: 'Notes',
          visible: false,
          viewMode: false,
          viewRecord: true,
          editRecord: true,
          type: 'textarea',
        },
        {
          id: 8,
          value: 'invoice',
          label: 'Invoice',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          type: 'invoice',
        },
        {
          id: 9,
          value: 'action',
          label: 'Action',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          width: 180,
          type: 'input',
        }
      ],
      paymentDetails: [
        {
          id: 1,
          value: 'vendorName',
          label: 'Vendor Name',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input'
        },
        {
          id: 2,
          value: 'invoiceNum',
          label: 'Vendor Invoice',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input'
        },
        {
          id: 3,
          value: 'paymentType',
          label: 'Payment Type',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input'
        },
        {
          id: 4,
          value: 'checkNumber',
          label: 'Check Number',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input'
        },
        {
          id: 5,
          value: 'amountPaid',
          label: 'Amount Paid',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input'
        },
        {
          id: 6,
          value: 'paymentDate',
          label: 'Date of Payment',
          visible: true,
          viewMode: false,
          editRecord: false,
          type: 'input'
        }
      ]
    };
  }

  function patients() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'patientId',
          label: 'Patient ID / Chart #',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          sort: true,
          sortLabel: 'Patient ID / Chart # ⇅',
          type: 'input',
        },
        {
          id: 3,
          value: 'firstName',
          label: 'First Name',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'First Name⇅',
        },
        {
          id: 4,
          value: 'lastName',
          label: 'Last Name',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Last Name⇅',
        },
        {
          id: 5,
          value: 'birthDate',
          label: 'Date of Birth',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Date of Birth⇅',
        },
        {
          id: 6,
          value: 'patientStatus',
          label: 'Patient Status',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Patient Status⇅',
        },
        {
          id: 7,
          value: 'onlineRegistration',
          label: 'Online Registration',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Online Registration⇅',
        },
        {
          id: 8,
          value: 'onlineRegistrationStatus',
          label: 'Online Registration Status',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Online Registration Status⇅',
        },
        {
          id: 9,
          value: 'family',
          label: 'Family',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          sort: true,
          type: 'booleanLabel',
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          sortLabel: 'Family⇅',
        },
        {
          id: 10,
          value: 'firstVisit',
          label: 'Initial Visit',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Initial Visit⇅',
        },
        {
          id: 11,
          value: 'lastEntry',
          label: 'Last Entry',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Last Entry⇅',
        },
        {
          id: 12,
          value: 'dentistName',
          label: 'Dentist',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          sort: true,
          sortLabel: 'Dentist⇅',
        },
        {
          id: 13,
          value: 'providerName',
          label: 'Primary Provider',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          sort: true,
          sortLabel: 'Primary Provider⇅',
        },
        {
          id: 14,
          value: 'phoneNumber',
          label: 'Phone Number',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          sort: true,
          sortLabel: 'Phone Number⇅',
        },
        {
          id: 15,
          value: 'email',
          label: 'Patient Email',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Patient Email⇅',
        },
        {
          id: 16,
          value: 'recallDueDate',
          label: 'Next Recall',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Next Recall⇅',
        },
        {
          id: 17,
          value: 'sharedByYou',
          label: 'Shared By You',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Shared By You⇅',
        },
        {
          id: 18,
          value: 'shared',
          label: 'Shared With You',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'booleanLabel',
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          sort: true,
          sortLabel: 'Shared With You⇅',
        },
        {
          id: 19,
          value: 'sharedBy',
          label: 'Shared With You By',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Shared With You By⇅',
        },
        {
          id: 20,
          value: 'sharedDate',
          label: 'Shared With You Date',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Shared With You Date⇅',
        }
      ],
      patientInvite: [
        {
          id: 1,
          value: 'full_name',
          label: 'First Name',
          visible: false,
          asteriskColor: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'last_name',
          label: 'Last Name',
          visible: false,
          asteriskColor: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'email',
          label: 'Email',
          visible: false,
          asteriskColor: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'phone',
          label: 'Phone',
          visible: false,
          asteriskColor: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'country',
          label: 'Country',
          visible: false,
          asteriskColor: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            'USA',
            'Canada',
            'Australia',
            'England',
            'India'
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
      requestConsultation: [
        {
          name: 'comment',
          title: 'Notes',
          type: 'Textarea',
          renderTextarea: true,
          onChange: true
        },
        {
          name: 'chooseConsultArea',
          title: 'Choose the Consult Area',
          type: 'Select',
          options: [
            {
              'title': 'Mandible',
              'name': 'Mandible'
            },
            {
              'title': 'Maxilla',
              'name': 'Maxilla'
            },
            {
              'title': 'Both Arches',
              'name': 'Both Arches'
            },
            {
              'title': 'Medical Condition',
              'name': 'Medical Condition'
            }
          ],
          onChange: true,
          showDropdown: true,
        },
        {
          name: 'overThePhone',
          title: 'Request phone consultation',
          type: 'Checkbox',
          newStyle: true,
          onChange: true
        },
        {
          text: '<div style="font-size: 14px;font-weight: bold;"><a href="https://s3.amazonaws.com/static.novadonticsllc.com/AdvisoryBoard/index.html" target="_blank" style="color: #818284 !important;"><u>Consultants Biographies</u></a></div>',
          type: 'InfoBlock',
        },
        {
          text: '<div style="font-size: 12px;font-weight: 400;">Disclaimer <br/> Novadontics, LLC. does not engage in the practice of dentistry. It facilitates consultation and in-office visit requests by allowing Novadontics software licensees to request assistance through the software. All dental and other professional service fees will be payable to the consultants or other third party designee by Novadontics, LLC. upon collecting the fee. Your credit card on file will be billed upon initiating the consultation request. For in-office visit fees please inquire at: <a href="mailto:team@novadontics.com">team@novadontics.com</a> Requesting a consultation or in-office professional services through the Novadontics software means you understand and agree with this disclaimer. </div>',
          type: 'InfoBlock',
          title: 'Disclaimer'

        }
      ],
      ctScan: [
        {
          id: 1,
          value: 'ctScanFile',
          label: 'CT Scan Files',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          multiUpload: true,
          type: 'files',
        }
      ],
      xray: [
        {
          id: 1,
          value: 'xray',
          label: 'X-Ray',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          multiImage: true,
          inline: true,
          type: 'image',
        }
      ],
      Contracts: [
        {
          id: 1,
          value: 'documentType',
          label: 'Document Type',
          visible: false,
          editRecord: true,
          onChange: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: 'All',
              label: 'All',
            },
            {
              value: "Health History Forms",
              label: "Health History Related"
            },
            {
              value: "HIPAA Forms",
              label: "HIPAA"
            },
            {
              value: "Contracts",
              label: "Contracts"
            },
            {
              value: "Patient Insurance",
              label: "Patient Insurance"
            },
            {
              value: "Patient Treatment",
              label: "Patient Treatment"
            }
          ],
        },
        {
          id: 2,
          value: 'documents',
          label: 'Document',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          multiUpload: true,
          type: 'contracts',
        }
      ],
      scratchPad: [
        {
          id: 1,
          value: 'scratchPad',
          label: 'Scratch Pad',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          share: true,
          height: "560",
          type: 'signature',
        },
        {
          id: 2,
          value: 'scratchPadText',
          label: 'Text',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          share: true,
          inline: true,
          type: 'textarea',
        }
      ],
      measurementHistory: [
        {
          Header: "Date",
          accessor: "dateCreated",
          Cell: props => <span>{props.value ? moment(props.value).format('DD MMM, YYYY') : '-'}</span>
        },
        {
          Header: "Time",
          accessor: "dateCreated",
          Cell: props => <span>{props.value ? moment(props.value).format('h:mm A') : '-'}</span>
        },
        {
          Header: "Systolic",
          accessor: "systolic",
          Cell: props => <span style={{ color: parseFloat(props.value) < 90 || parseFloat(props.value) > 129 ? 'red' : '#00000' }}>{props.value ? props.value : '-'}</span>
        },
        {
          Header: "Diastolic",
          accessor: "diastolic",
          Cell: props => <span style={{ color: parseFloat(props.value) < 60 || parseFloat(props.value) > 79 ? 'red' : '#00000' }}>{props.value ? props.value : '-'}</span>
        },
        {
          Header: "Pulse",
          accessor: "pulse",
          Cell: props => <span style={{ color: parseFloat(props.value) < 44 || parseFloat(props.value) > 85 ? 'red' : '#00000' }}>{props.value ? props.value : '-'}</span>
        },
        {
          Header: "O2 Level",
          accessor: "oxygen",
          Cell: props => <span style={{ color: parseFloat(props.value) < 95 ? 'red' : '#00000' }}>{props.value ? props.value : '-'}</span>
        },
        {
          Header: "Body T",
          accessor: "bodyTemp",
          Cell: props => <span style={{ color: parseFloat(props.value) > 98.6 ? 'red' : '#00000' }}>{props.value ? props.value.toFixed(1) : '-'}</span>
        },
        {
          Header: "Glucose",
          accessor: "glucose",
          Cell: props => <span style={{ color: parseFloat(props.value) > 195 ? 'red' : '#00000' }}>{props.value ? props.value.toFixed(1) : '-'}</span>
        },
        {
          Header: "HbA1c",
          accessor: "a1c",
          Cell: props => <span style={{ color: parseFloat(props.value) > 7 ? 'red' : '#00000' }}>{props.value ? props.value.toFixed(1) : '-'}</span>
        },
        {
          Header: "INR",
          accessor: "inr",
          Cell: props => <span style={{ color: parseFloat(props.value) < 2 || parseFloat(props.value) > 4 ? 'red' : '#00000' }}>{props.value ? props.value.toFixed(1) : '-'}</span>
        },
        {
          Header: "PT",
          accessor: "pt",
          Cell: props => <span style={{ color: parseFloat(props.value) < 10 || parseFloat(props.value) > 14 ? 'red' : '#00000' }}>{props.value ? props.value.toFixed(1) : '-'}</span>
        }
      ],
      ToothChart: [
        {
          id: 1,
          value: 'toothId',
          label: 'Tooth Id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'toothConditions',
          label: 'Tooth Conditions',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          group: true,
          type: 'multicheckbox',
          oneOf: 'toothChartOptions'
        }
      ],
      ToothTreatmentPlan: [
        {
          id: 1,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          existing: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          orangeTitle: true,
          number: true,
          type: 'select',
          oneOf: 'providers',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'datePlanned',
          label: 'Date',
          visible: false,
          editRecord: true,
          existing: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          orangeTitle: true,
          defaultDate: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'surface',
          label: 'Surface',
          visible: false,
          editRecord: true,
          existing: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          orangeTitle: true,
          type: 'select',
          oneOf: [
            'Buccal/Facial',
            'Mesial',
            'Occlusal/Incisal',
            'Distal',
            'Lingual'
          ]
        },
        {
          id: 4,
          value: 'prognosis',
          label: 'Prognosis',
          visible: false,
          editRecord: true,
          existing: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          orangeTitle: true,
          type: 'select',
          oneOf: [
            'Hopeless',
            'Poor',
            'Guarded',
            'Good',
            'Excellent'
          ]
        },
        {
          id: 5,
          value: 'priority',
          label: 'Priority',
          visible: false,
          editRecord: true,
          existing: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          orangeTitle: true,
          type: 'select',
          oneOf: [
            'Yes',
            'No'
          ]
        },
        {
          id: 6,
          value: 'codeNumber',
          label: 'CDT Code',
          visible: false,
          editRecord: true,
          existing: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          toothWidth: true,
          orangeTitle: true,
          search: true,
          type: 'search',
          oneOf: 'codeNumber',
          ref: 'codeDescription',
        },
        {
          id: 7,
          value: 'codeDescription',
          label: 'Procedure Description',
          visible: false,
          editRecord: true,
          existing: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          toothWidth: true,
          orangeTitle: true,
          search: true,
          type: 'search',
          oneOf: 'codeDescription',
          ref: 'codeNumber',
        },
        {
          id: 8,
          value: 'procedureCode',
          label: 'CDT Code',
          visible: false,
          editRecord: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          toothWidth: true,
          orangeTitle: true,
          search: true,
          type: 'search',
          oneOf: 'procedureCode',
          ref: 'procedureDescription',
        },
        {
          id: 9,
          value: 'procedureDescription',
          label: 'Procedure Description',
          visible: false,
          editRecord: true,
          treatment: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          toothWidth: true,
          orangeTitle: true,
          search: true,
          type: 'search',
          oneOf: 'procedureDescription',
          ref: 'procedureCode',
        },
      ],
      ImplantExamImaging: [
        {
          id: 1,
          value: 'implantExamImaging',
          label: 'Implant Exam and imaging',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          group: true,
          type: 'multicheckbox',
          oneOf: 'implantExamImaging'
        }
      ],
      PerioChart: [
        {
          id: 1,
          value: 'id',
          label: 'id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'rowPosition',
          label: 'Row Postition',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'dateSelected',
          label: 'Selected Date',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          noPad: true,
          type: 'date',
        },
        {
          id: 4,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          number: true,
          type: 'select',
          oneOf: 'providers',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'perioType',
          label: 'Perio Type',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'select',
          oneOf: [
            'Buccal',
            'Lingual'
          ],
        },
        {
          id: 6,
          value: 'perioLabel',
          label: 'Perio Label',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
        },
        {
          id: 7,
          value: 'perioNumbers[0]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          max: 3,
          noPad: true,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[0]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            onChange: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[0]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[0]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[0]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[0]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[0]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[0]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[0]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[0]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[0]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[0]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 8,
          value: 'perioNumbers[1]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          max: 3,
          noPad: true,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[1]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[1]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[1]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[1]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[1]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[1]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[1]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[1]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[1]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[1]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[1]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 9,
          value: 'perioNumbers[2]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          max: 3,
          noPad: true,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[2]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[2]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[2]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[2]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[2]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[2]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[2]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[2]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[2]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[2]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[2]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 10,
          value: 'perioNumbers[3]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[3]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[3]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[3]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[3]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[3]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[3]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[3]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[3]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[3]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[3]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[3]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 11,
          value: 'perioNumbers[4]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[4]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[4]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[4]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[4]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[4]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[4]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[4]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[4]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[4]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[4]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[4]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 12,
          value: 'perioNumbers[5]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[5]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[5]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[5]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[5]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[5]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[5]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[5]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[5]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[5]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[5]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[5]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 13,
          value: 'perioNumbers[6]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[6]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[6]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[6]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[6]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[6]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[6]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[6]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[6]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[6]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[6]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[6]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 14,
          value: 'perioNumbers[7]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[7]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[7]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[7]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[7]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[7]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[7]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[7]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[7]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[7]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[7]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[7]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 15,
          value: 'perioNumbers[8]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[8]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[8]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[8]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[8]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[8]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[8]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[8]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[8]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[8]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[8]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[8]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 16,
          value: 'perioNumbers[9]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[9]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[9]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[9]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[9]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[9]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[9]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[9]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[9]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[9]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[9]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[9]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 17,
          value: 'perioNumbers[10]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[10]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[10]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[10]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[10]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[10]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[10]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[10]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[10]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[10]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[10]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[10]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 18,
          value: 'perioNumbers[11]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[11]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[11]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[11]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[11]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[11]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[11]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[11]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[11]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[11]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[11]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[11]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 19,
          value: 'perioNumbers[12]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[12]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[12]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[12]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[12]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[12]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[12]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[12]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[12]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[12]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[12]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[12]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 20,
          value: 'perioNumbers[13]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[13]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[13]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[13]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[13]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[13]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[13]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[13]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[13]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[13]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[13]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[13]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 21,
          value: 'perioNumbers[14]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[14]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[14]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[14]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            noPad: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[14]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"], Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[14]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[14]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[14]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[14]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[14]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[14]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[14]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 22,
          value: 'perioNumbers[15]',
          label: 'Perio Numbers',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          max: 3,
          type: 'input',
          typeB: "checkbox",
          typeR: "input",
          typeF: "select",
          typeM: "select",
          typePq: "select",
          typeBL: "select",
          typeS: "select",
          typeJ: "input",
          typeC: "input",
          typePr: "select",
          PN: {
            value: 'perioNumbers[15]["PN"]',
            label: 'Perio Numbers',
            type: 'input',
            onChange: true,
            number: true,
            noPad: true,
            max: 3
          },
          B: {
            value: 'perioNumbers[15]["B"]',
            label: 'B',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          R: {
            value: 'perioNumbers[15]["R"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            onChange: true,
            number: true,
            max: 3,
          },
          F: {
            value: 'perioNumbers[15]["F"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["I", "II", "III", "0"],
            Emptyoption: true,
            optionsOnly: true
          },
          M: {
            value: 'perioNumbers[15]["M"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: [0, 1, 2, 3],
            Emptyoption: true,
            optionsOnly: true
          },
          PQ: {
            value: 'perioNumbers[15]["PQ"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["None", "Light", "Moderate", "Heavy"],
            Emptyoption: true,
            optionsOnly: true
          },
          BL: {
            value: 'perioNumbers[15]["BL"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"],
            Emptyoption: true,
            optionsOnly: true
          },
          S: {
            value: 'perioNumbers[15]["S"]',
            label: 'S',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No"], Emptyoption: true,
            optionsOnly: true
          },
          J: {
            value: 'perioNumbers[15]["J"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            number: true,
            max: 1
          },
          C: {
            value: 'perioNumbers[15]["C"]',
            label: 'Perio Numbers',
            type: 'input',
            noPad: true,
            max: 3
          },
          PR: {
            value: 'perioNumbers[15]["PR"]',
            label: 'Perio Numbers',
            type: 'select',
            noPad: true,
            oneOf: ["Good", "Fair", "Guarded", "Poor", "Hopless"],
            Emptyoption: true,
            optionsOnly: true
          },
        }
      ],
      EndoChart: [
        {
          id: 1,
          value: 'id',
          label: 'id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'rowPosition',
          label: 'Row Postition',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'dateSelected',
          label: 'Selected Date',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          noPad: true,
          type: 'date',
        },
        {
          id: 4,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          number: true,
          type: 'select',
          oneOf: 'providers',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'endoLabel',
          label: 'Testing',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
        },
        {
          id: 6,
          value: 'data[0]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[0]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[0]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[0]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[0]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[0]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[0]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[0]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[0]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[0]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[0]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[0]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[0]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[0]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[0]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[0]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[0]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[0]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[0]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[0]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[0]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[0]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[0]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 7,
          value: 'data[1]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[1]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[1]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[1]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[1]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[1]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[1]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[1]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[1]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[1]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[1]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[1]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[1]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[1]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[1]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[1]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[1]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[1]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[1]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[1]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[1]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[1]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[1]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 8,
          value: 'data[2]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[2]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[2]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[2]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[2]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[2]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[2]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[2]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[2]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[2]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[2]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[2]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[2]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[2]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[2]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[2]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[2]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[2]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[2]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[2]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[2]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[2]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[2]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 9,
          value: 'data[3]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[3]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "C & H sensitivity", "Cold sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[3]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[3]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[3]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[3]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[3]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[3]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[3]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[3]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[3]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[3]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[3]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[3]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[3]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[3]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[3]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[3]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[3]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[3]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[3]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[3]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[3]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 10,
          value: 'data[4]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[4]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[4]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[4]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[4]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[4]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[4]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[4]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[4]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[4]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[4]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[4]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[4]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[4]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[4]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[4]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[4]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[4]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[4]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[4]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[4]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[4]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[4]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 11,
          value: 'data[5]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[5]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[5]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[5]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[5]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[5]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[5]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[5]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[5]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[5]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[5]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[5]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[5]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[5]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[5]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[5]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[5]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[5]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[5]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[5]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[5]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[5]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[5]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 12,
          value: 'data[6]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[6]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[6]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[6]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[6]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[6]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[6]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[6]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[6]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[6]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[6]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[6]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[6]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[6]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[6]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[6]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[6]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[6]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[6]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[6]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[6]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[6]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[6]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 13,
          value: 'data[7]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[7]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[7]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[7]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[7]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[7]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[7]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[7]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[7]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[7]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[7]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[7]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[7]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[7]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[7]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[7]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[7]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[7]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[7]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[7]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[7]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[7]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[7]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 14,
          value: 'data[8]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[8]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[8]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[8]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[8]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[8]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[8]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[8]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[8]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[8]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[8]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[8]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[8]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[8]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[8]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[8]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[8]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[8]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[8]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[8]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[8]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[8]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[8]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 15,
          value: 'data[9]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[9]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[9]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[9]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[9]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[9]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[9]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[9]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[9]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[9]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[9]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[9]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[9]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[9]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[9]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[9]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[9]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[9]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[9]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[9]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[9]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[9]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[9]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 16,
          value: 'data[10]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[10]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[10]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[10]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[10]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[10]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[10]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[10]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[10]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[10]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[10]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[10]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[10]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[10]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[10]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[10]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[10]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[10]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[10]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[10]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[10]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[10]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[10]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 17,
          value: 'data[11]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[11]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[11]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[11]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[11]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[11]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[11]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[11]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[11]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[11]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[11]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[11]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[11]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[11]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[11]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[11]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[11]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[11]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[11]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[11]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[11]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[11]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[11]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 18,
          value: 'data[12]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[12]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[12]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[12]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[12]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[12]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[12]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[12]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[12]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[12]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[12]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[12]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[12]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[12]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[12]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[12]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[12]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[12]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[12]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[12]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[12]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[12]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[12]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 19,
          value: 'data[13]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[13]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[13]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[13]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[13]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[13]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[13]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[13]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[13]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[13]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[13]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[13]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[13]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[13]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[13]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[13]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[13]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[13]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[13]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[13]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[13]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[13]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[13]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 20,
          value: 'data[14]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[14]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[14]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[14]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[14]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[14]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[14]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[14]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[14]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[14]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[14]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[14]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[14]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[14]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[14]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[14]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[14]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[14]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[14]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[14]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[14]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[14]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[14]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        },
        {
          id: 21,
          value: 'data[15]',
          label: 'Endo Data',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          noPad: true,
          type: 'input',
          typeSelect: 'select',
          chiefComplaint: {
            value: 'data[15]["chiefComplaint"]',
            label: 'Chief Complaint',
            type: 'select',
            noPad: true,
            oneOf: ["Bad taste", "Cold sensitivity", "C & H sensitivity", "Discoloration", "Fistula", "Fracture", "History of pain", "Hot sensitivity", "Implant", "None", "Pain", "Pain on chewing", "Pain on percussion", "Palpation soreness", "Sinus drainage", "Sinus tract", "Swelling"],
            Emptyoption: true,
            optionsOnly: true
          },
          palpation: {
            value: 'data[15]["palpation"]',
            label: 'Palpation',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          percussion: {
            value: 'data[15]["percussion"]',
            label: 'Percussion',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          chewing: {
            value: 'data[15]["chewing"]',
            label: 'Chewing',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          bite: {
            value: 'data[15]["bite"]',
            label: 'Bite',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "Upon release", "Upon chewing"],
            Emptyoption: true,
            optionsOnly: true
          },
          mobility: {
            value: 'data[15]["mobility"]',
            label: 'Mobility',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "1", "2", "3"],
            Emptyoption: true,
            optionsOnly: true
          },
          air: {
            value: 'data[15]["air"]',
            label: 'Air',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "+", "++", "+++", "History of"],
            Emptyoption: true,
            optionsOnly: true
          },
          cold: {
            value: 'data[15]["cold"]',
            label: 'Cold',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain", "CS mild", "CS moderate", "CS sever"],
            Emptyoption: true,
            optionsOnly: true
          },
          hot: {
            value: 'data[15]["hot"]',
            label: 'Hot',
            type: 'select',
            noPad: true,
            oneOf: ["WNL", "No response", "Yes", "Yes lingering", "Yes delayed", "History of", "Relives pain"],
            Emptyoption: true,
            optionsOnly: true
          },
          ept: {
            value: 'data[15]["ept"]',
            label: 'EPT',
            type: 'select',
            noPad: true,
            oneOf: ["No response", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            Emptyoption: true,
            optionsOnly: true
          },
          occlusalTrauma: {
            value: 'data[15]["occlusalTrauma"]',
            label: 'Occlusal Trauma',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          sinusTract: {
            value: 'data[15]["sinusTract"]',
            label: 'Sinus Tract',
            type: 'select',
            noPad: true,
            oneOf: ["Yes", "No", "Possibly"],
            Emptyoption: true,
            optionsOnly: true
          },
          fracture: {
            value: 'data[15]["fracture"]',
            label: 'Fracture',
            type: 'select',
            noPad: true,
            oneOf: ["Cuspal", "Enamel only", "Dentical", "Incisal", "Furcal", "Root"],
            Emptyoption: true,
            optionsOnly: true
          },
          pulpalDiagnosis: {
            value: 'data[15]["pulpalDiagnosis"]',
            label: 'Pulpal Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal pulp", "Reversible pulpitis", "Symptomatic irr. pulpitis", "Asymptomatic irr. pulpitis", "Previous RCT", "Necrotic pulp", "Prev. initialed RCT", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          periapicalDiagnosis: {
            value: 'data[15]["periapicalDiagnosis"]',
            label: 'Periapical Diagnosis',
            type: 'select',
            noPad: true,
            oneOf: ["Normal apical tissues", "Asymp. apical periodontitis", "Symp. apical diagnosis", "Acute apical abscess", "Chronic apical abscess", "RCT for pros. purpose"],
            Emptyoption: true,
            optionsOnly: true
          },
          endoPrognosis: {
            value: 'data[15]["endoPrognosis"]',
            label: 'Endo Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Pending"],
            Emptyoption: true,
            optionsOnly: true
          },
          perioPrognosis: {
            value: 'data[15]["perioPrognosis"]',
            label: 'Perio Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent"],
            Emptyoption: true,
            optionsOnly: true
          },
          restorativePrognosis: {
            value: 'data[15]["restorativePrognosis"]',
            label: 'Restorative Prognosis',
            type: 'select',
            noPad: true,
            oneOf: ["Hopeless", "Poor", "Guarded", "Good", "Excellent", "Good with restoration"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx: {
            value: 'data[15]["recommendedTx"]',
            label: 'Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx2: {
            value: 'data[15]["recommendedTx2"]',
            label: '2nd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          recommendedTx3: {
            value: 'data[15]["recommendedTx3"]',
            label: '3rd Recommended Tx',
            type: 'select',
            noPad: true,
            oneOf: ["Completed", "RCT", "Apicoectomy", "Root Amp", "Hemisection", "2nd opinion", "Retreatment", "Retreatment & Apicoectomy", "No treatment", "Extraction", "Int replant", "Eval later", "Eval in 6 weeks", "Eval if symptoms reappear", "Eval when in temps", "Refer to Perio", "Refer to GP", "Refer to OS", "Refer to MD", "Post removal", "Other", "Unknown", "MTA pulp cap"],
            Emptyoption: true,
            optionsOnly: true
          },
          estNumOfVisits: {
            value: 'data[15]["estNumOfVisits"]',
            label: 'Estimate # of Visits',
            type: 'select',
            noPad: true,
            oneOf: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            Emptyoption: true,
            optionsOnly: true
          },
        }
      ],
      measurementInfo: {
        id: 1,
        attributes: { info: { text: `<div style="font-weight: normal;">BP/ PULSE/ O2/ Body T/ Glucose / HbA1c / INR / PT measurements are logged automatically when they are taken and logged in Steps C1and 3A. The icon turns red when the last measurement taken does not fall within the normal limits described below:</div><br /><ul><li>Blood Pressure normal limits: Systolic 90-129 / Diastolic 60-79 (mmHg)</li><li>Resting Pulse normal limits: 44-85</li><li>Oxygen Saturation Level* normal value: 95% or more</li><li>Body Temperature normal level: 98.6 F or less</li><li>Glucose* normal level: 195 mg/dl or less</li><li>HbA1c* normal value: 7.0% or less</li><li>INR normal limits: 2.0-4.0 (Coumadin patients only)</li><li>PT* (Prothrombin Time Test) normal limits: 10-14 sec.</li></ul><div style="font-weight: normal;margin-bottom:10px;">* When you take a breath, oxygen enters your lungs and attaches to a carrier protein called hemoglobin inside your red blood cells. Those same red blood cells release carbon dioxide into your lungs, which leaves your body when you exhale. Newly oxygenated red blood cells carry oxygen to your body.A pulse oximeter uses light to measure the oxygen saturation level, the percentage of hemoglobin in your red blood cells carrying oxygen. Normal oxygen saturation levels fall between 95 and 99 percent.</div><div style="font-weight: normal;margin-bottom:10px;">* Blood glucose level is used for immediate knowledge of blood glucose level. HbA1c is used for predication of how well the diabetic patient is controlled.</div><div style="font-weight: normal;margin-bottom:10px;">* The prothrombin time, or PT, test measures the time it takes your blood to form a clot. This test is also often called ProTime. The results of the prothrombin time test may vary from lab to lab, so healthcare providers use a ratio called the INR (international normalized ratio) to be able to account for the differences. The PT/INR is usually done to measure the effect of blood- thinning medicines (anticoagulants), such as warfarin (Coumadin). If you have a medical condition such as atrial fibrillation or deep vein thrombosis, or have had a heart valve replaced, your blood is more likely to form clots. Clots can block blood vessels and possibly cause a heart attack or stroke. Your healthcare provider may prescribe a blood thinner to help prevent clots. It\'s very important to measure the effect of a blood thinner with this test. The medicine should keep the blood just thin enough to prevent clots. If the blood is too thin, you may bleed too easily. The prothrombin time test may also be done if you have abnormal bleeding or clotting.</div><div><img src=${require('img/patients/measurementHistory1.png')} width="750" /><img src=${require('img/patients/measurementHistory2.jpg')} /><img src=${require('img/patients/measurementHistory4.png')} width="520" /><img src=${require('img/patients/measurementHistory3.png')} width="520" /><img src=${require('img/patients/measurementHistory7.png')} width="550" /><img src=${require('img/patients/measurementHistory5.gif')}  /><img src=${require('img/patients/measurementHistory6.png')}  width="520"/>` } }
      },
      ctScanInfo:{
        id: 1,
        attributes:{info:{
        title:`Uploading patients' CT scan files to their Novadontics file`,
        text: `<div style="font-weight: normal;"><span style="font-weight:bold;">Procedure</span>: First, save the patient's CT Scan DICOM file (.dcm) to your computer desktop (without any special characters in the title). Next, compress (or zip) the file. (If you are unsure as to how to create & save this file to your computer desktop, please contact the technical support of your CT scanner.) Next, click “Add CT Scan Files” and select the appropriate zipped or compressed file from your computer desktop. Once the upload is completed, the .dcm files are now saved within the patient's file on Novadontics.<br/>
        You may then download the CT Scan file from the patient's file onto any computer, open the files, and view them using any DICOM-compatible software.</div>` }}
      },
      treatmentPlanDetailsInfo:{
        id: 1,
        attributes: {
          info: {
            text: `<div style="font-weight: normal;"><div><ul><li>Tx History icon generates a PDF file containing all proposed, accepted, and completed treatment plans as well as referred out treatment plans.</li><li>Tx Estimate icon generates a PDF file containing only accepted and proposed treatment plans.</li><li>Tx Statement icon generates a PDF file containing only completed and accepted treatment plans.</li><li>Referred out Tx icon generates a PDF file containing only referred out treatment plans.</li><li>Existing Conditions icon generates a PDF file containing only teeth conditions described as existing.</li></ul></div></div>`,
            openModal: true, labelStyle: true, title: 'Legend'
          }
        }
      },
      continueEducationInfo:{
        id: 1,
        attributes: {
          info: {
            text: `<div style="font-weight: normal;"><p>Your trial or catalog-only access to the Novadontics continuing education (CE) module comes with three complimentary live lectures (the first three you select to watch). Paid subscriptions come with access to a certain number of lectures per calendar year depending on your plan. Once you access a lecture, you will notice the word "Viewed" appears under the image of the lecture. This word will be replaced with the word "Completed" when you request CE credit for the lecture and successfully answer the associated questions. Once you access your maximum number of lectures for the year, you will notice the word "Purchase" appearing on all lectures that have not been viewed or completed. 
            </p><li>A Novadontics subscription plan with unlimited access is available, or call 888.838.6682 to purchase unlimited access separately.</li><br /><li>You will always have access to the lectures you viewed or completed as long as you have an active subscription with Novadontics.</li><br /><li>All CE courses on the Novadontics platform are priced at $29 each. 
            </li></div>`,
            openModal: true
          }
        }
      },
      staffNotesInfo:{
        id: 1,
        attributes: {info: {
        text: `<div style="font-weight: normal;">The staff notes icon will turn red when recent messages exist.<br /> It will turn white again when there are no messages or existing<br /> messages are older than one week.</div>`}}
      },
      sharedPatientInfo:{
        id: 1,
        attributes:{info:{
        title:"Sharing patient's record",
        text: `<div style="font-weight: normal;">To share a patient's record with another dentist, please click on the ADD DENTIST button, then start typing the name of the dentist in the SHARE WITH box. The dentist's name will appear if he or she is a Novadontics software user. (If the dentist you want to share a patient's file with is not a Novadontics software user, please contact Novadontics support at: 888.838.6682 or at <a href="mailto:team@novadontics.com">team@novadontics.com</a>. The Novadontics admin will grant the non-Novadontics member free view-only, no-obligation access.) Once the dentist is selected, choose the parts of the file you want to share, select the desired access level, and then click SUBMIT.
        Please note that you can stop sharing a patient's files at any time.</div>`}}
      },
      prescriptions: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'patientId',
          label: 'Patient Id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'dateCreated',
          label: 'Date Created',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        }
      ],
      prescriptionHistory: [
        {
          id: 1,
          value: 'dateCreated',
          label: 'Date',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'prescription',
          label: 'Prescription',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'prescribedBy',
          label: 'Prescribed By',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'note',
          label: 'Note',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        }
      ],
      TreatmentPlan: [
        {
          id: 1,
          value: 'dateCreated',
          label: 'Created Date',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'date',
        },
        {
          id: 2,
          value: 'dateUpdated',
          label: 'Updated Date',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'planName',
          label: 'Plan Name',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          style: { marginBottom: '4px' },
          type: 'input',
        },
        {
          id: 4,
          value: 'createdBy',
          label: 'Created By',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
        /*{
          id: 5,
          value: 'status',
          label: 'Status',
          visible: false,
          createRecord: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'select',
          oneOf: [
            'Proposed',
            'Accepted',
            'Completed'
          ]
        },*/
        {
          id: 6,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'input',
        }
      ],
      TreatmentPlanDetails: [
        {
          id: 1,
          value: 'serial',
          label: '#',
          width: 70,
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          serial: true,
          type: 'input',
        },
        {
          id: 2,
          value: 'toothNumber',
          label: 'Tooth',
          width: 80,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'text'
        },
        {
          id: 3,
          value: 'surface',
          label: 'Surface',
          width: 130,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'select',
          oneOf: [
            'Buccal/Facial',
            'Mesial',
            'Occlusal/Incisal',
            'Distal',
            'Lingual'
          ]
        },
        {
          id: 4,
          value: 'cdcCode',
          label: 'CDT Code',
          width: 150,
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'cdcCode',
          search: true,
          ref: 'procedureName',
        },
        {
          id: 5,
          value: 'procedureName',
          label: 'Procedure Description',
          width: 320,
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'procedureName',
          search: true,
          view: true,
          limit: 30,
          ref: 'cdcCode',
        },
        {
          id: 6,
          value: 'datePlanned',
          label: 'Date Planned',
          width: 130,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          noPad: true,
          stylenew: true,
          valueempty: true,
          type: 'date',
        },
        {
          id: 7,
          value: 'prognosis',
          label: 'Prognosis',
          width: 150,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'select',
          oneOf: [
            'Hopeless',
            'Poor',
            'Guarded',
            'Good',
            'Excellent'
          ]
        },
        {
          id: 8,
          value: 'phase',
          label: 'Phase',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'select',
          oneOf: [
            'Phase1',
            'Phase2',
            'Phase3',
            'Phase4',
            'Phase5',
            'Phase6',
            'Phase7',
            'Phase8',
            'Phase9',
            'Phase10',
          ]
        },
        {
          id: 9,
          value: 'priority',
          label: 'Priority',
          width: 130,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'select',
          oneOf: [
            'Yes',
            'No'
          ]
        },
        {
          id: 10,
          value: 'status',
          label: 'Status',
          width: 150,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          sortLabel: "Status⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: [
            'Existing',
            'Proposed',
            'Accepted',
            'Completed',
            'Referred Out'
          ]
        },
        {
          id: 11,
          value: 'providerName',
          label: 'Provider',
          width: 150,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          number: true,
          // resourceColumn: true,
          sortLabel: "Provider⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: 'providers'
        },
        {
          id: 12,
          value: 'datePerformed',
          label: 'Date Performed',
          width: 130,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          noPad: true,
          stylenew: true,
          valueempty: true,
          required: true,
          sortLabel: "Date Performed⇅",
          sort: true,
          sortable: true,
          dateSort:true,
          type: 'date',
        },
        {
          id: 13,
          value: 'insurance',
          label: 'Dental Insurance Name',
          width: 150,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'select',
          oneOf: 'dentalCarrier',
        },
        {
          id: 14,
          value: 'renewalDate',
          label: 'Renewal Date',
          width: 160,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          noPad: true,
          stylenew: true,
          valueempty: true,
          type: 'date',
        },
        {
          id: 15,
          value: 'familyAnnualMax',
          label: 'Family Annual Max',
          width: 100,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          currency: true,
          type: 'number'
        },
        {
          id: 16,
          value: 'familyAnnualDeductible',
          label: 'Family Annual Deductible',
          width: 100,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          currency: true,
          type: 'number'
        },
        {
          id: 17,
          value: 'individualAnnualMax',
          label: 'Individual Annual Max',
          width: 100,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          currency: true,
          type: 'number'
        },
        {
          id: 18,
          value: 'individualAnnualDeductible',
          label: 'Individual Annual Deductible',
          width: 100,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          currency: true,
          type: 'number'
        },
        {
          id: 19,
          value: 'medicalInsuranceName',
          label: 'Medical Insurance Name',
          width: 150,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'text',
        },
        {
          id: 20,
          value: 'fee',
          label: 'Fee',
          width: 100,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          currency: true,
          type: 'number'
        },
        {
          id: 21,
          value: 'estIns',
          label: 'Est. Ins.',
          width: 100,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          currency: true,
          type: 'number',
        },
        {
          id: 22,
          value: 'disc',
          label: 'Balance to Patient',
          width: 120,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          disabled: true,
          currency: true,
          type: 'number',
        },
        {
          id: 23,
          value: 'action',
          label: 'Action',
          width: 120,
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'input',
        },
        {
          id: 24,
          value: 'scheduled',
          label: 'Scheduled',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
      ],
      recallPeriod: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'recallType',
          label: 'Recall Type',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          onChange: true,
          type: 'select',
          oneOf: 'recallProcedureOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'recallCode',
          label: 'Recall Code',
          visible: false,
          viewMode: true,
          editRecord: true,
          fullWidth: true,
          disabled: true,
          type: 'input',
        },
        {
          id: 4,
          value: 'recallCodeDescription',
          label: 'Code Description',
          visible: false,
          viewMode: true,
          editRecord: true,
          fullWidth: true,
          disabled: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'recallNote',
          label: 'Note',
          visible: false,
          viewMode: true,
          editRecord: true,
          type: 'textarea',
        },
        {
          id: 6,
          value: 'intervalNumber',
          label: 'Interval',
          visible: false,
          viewMode: true,
          editRecord: true,
          number: true,
          halfWidth: true,
          onChange: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 7,
          value: 'intervalUnit',
          label: 'Interval',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          halfWidth: true,
          hideLabel: true,
          onChange: true,
          type: 'select',
          optionsOnly: true,
          oneOf: ["months", "days", "weeks", "year"],
        },
        {
          id: 8,
          value: 'recallOneDayPlus',
          label: '+1 Day',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          onChange: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
        {
          id: 9,
          value: 'recallFromDate',
          label: 'From Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          onChange: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'recallDueDate',
          label: 'Due Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          disabled: true,
          type: 'input',
        },
      ],
      report: [
        {
          id: 1,
          value: 'type',
          label: 'Type',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          optionsOnly: true,
          type: 'select',
          oneOf: ["Patient Type Report", "Recall Report", "Referral Source Report", "New Patient Report"],
          onChange: true
        },
        {
          id: 2,
          value: 'format',
          label: 'Format',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          optionsOnly: true,
          type: 'select',
          oneOf: ["PDF", "CSV"]
        },
        {
          id: 3,
          value: 'fromDate',
          label: 'Start Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'date',
        },
        {
          id: 4,
          value: 'toDate',
          label: 'End Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'date',
        },
        {
          id: 5,
          value: 'patientType',
          label: 'Patient Type',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          optionsOnly: true,
          fullWidth: true,
          type: 'multiselect',
          oneOf: 'patientTypeList'
        }
      ],
      clinicalNotes: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          viewMode: false,
          editRecord: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'note',
          label: 'Notes',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'textarea',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'date',
          label: 'Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'provider',
          label: 'Provider',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'select',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
          oneOf: 'providers'
        }
      ],
      appointmentBook: [
        {
          id: 1,
          value: 'date',
          label: 'Date',
          dateLink: true,
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'date',
          width: 100,
        },
        {
          id: 2,
          value: 'dayOfWeek',
          label: 'Day of the week',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          width: 100,
        },
        {
          id: 3,
          value: 'state',
          label: 'Type',
          appointmentBook: true,
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          width: 100,
        },
        {
          id: 4,
          value: 'status',
          label: 'Status',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          width: 100,
        },
        {
          id: 5,
          value: 'reason',
          label: 'Reason(s) for Appointment',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          width: 200,
        },
        {
          id: 6,
          value: 'procedure',
          label: 'Procedure Code',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          width: 120,
        },
        {
          id: 7,
          value: 'procedure',
          label: 'Procedure Description',
          showIcon: true,
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          width: 400,
          limit: 40,
        },
        {
          id: 8,
          value: 'operatoryName',
          label: 'Operatory',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 9,
          value: 'providerName',
          label: 'Provider',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
      ],
      StaffNotes: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'date',
          label: 'Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          onChange: true,
          width: 110,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 3,
          value: 'time',
          label: 'Time',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          onChange: true,
          type: 'time',
          oneOf: {
            time: ["10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45"],
            clock: ["AM", "PM"],
          },
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 4,
          value: 'staffId',
          label: 'Note Creator',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          selectOption: true,
          onChange: true,
          type: 'select',
          oneOf: 'staffOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 5,
          value: 'notesFor',
          label: 'Note For',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          selectOption: true,
          onChange: true,
          type: 'select',
          oneOf: 'staffAllOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 6,
          value: 'notes',
          label: 'Notes',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          renderTextarea: true,
          onChange: true,
          width: 350,
          type: 'textarea',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          }
        },
        {
          id: 7,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          width: 180,
          type: 'input',
        }
      ],
      sharedPatient: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'patientId',
          label: 'Patient ID',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'practiceId',
          label: 'Practice ID',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'practiceName',
          label: 'Practice Name',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 5,
          value: 'staffName',
          label: 'Dentist',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'search',
          oneOf: 'doctorsOptions'
        },
        {
          id: 6,
          value: 'shareTo',
          label: 'Share With',
          placeholder: 'Type to search Novadontics users',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          disabled: {
            edit: true
          },
          type: 'search',
          oneOf: 'doctorsOptions'
        },
        {
          id: 7,
          value: 'sharedBy',
          label: 'Shared By',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 8,
          value: 'accessTo',
          label: "What parts of the patient's file you would like to share?",
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          sort: true,
          allOption: true,
          onChange: true,
          type: 'multiselect',
          oneOf: 'mainSquareIconOptions'
        },
        {
          id: 9,
          value: 'accessLevel',
          label: "Access Level",
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          onChange: true,
          type: 'select',
          oneOf: [
            {
              value: "View",
              label: "View Files Only"
            },
            {
              value: "Edit",
              label: "Allow Edit"
            }
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'patientConsent',
          label: 'Patient Consent to file sharing is on file',
          labelStyle: { paddingLeft: '10px', color: '#EA6225' },
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          onChange: true,
          fullWidth: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
        {
          id: 11,
          value: 'sharedDate',
          label: 'Shared Date',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'date',
        },
        {
          id: 12,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          width: 250,
          type: 'input',
        }
      ],
      sharedAdmin: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'patientId',
          label: 'Patient ID',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'mainSquares',
          label: 'Shared Main Squares',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          type: 'multiselect',
          oneOf: 'mainSquareOptions'
        },
        {
          id: 4,
          value: 'icons',
          label: 'Shared Icons',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          type: 'multiselect',
          oneOf: 'iconsOptions'
        }
      ],
      prescriptionsForm: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'medication',
          label: 'Medication',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Medication⇅',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          disabled: true,
          type: 'input',
          sort: true,
          sortLabel: 'Category⇅',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'dosage',
          label: 'Dosage',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'count',
          label: 'Count',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 1,
            },
          },
        },
        {
          id: 6,
          value: 'instructions',
          label: 'Instructions',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 7,
          value: 'refills',
          label: 'Refills',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'select',
          oneOf: [0, 1, 2, 3, 4, 5],
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 1,
            },
          },
        }
      ],
      familyMembers: [
        {
          id: 1,
          value: 'id',
          label: 'id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input'
        },
        {
          id: 2,
          value: 'name',
          label: 'Name',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'patientId',
          label: 'Name',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'search',
          ref: ['email', 'phoneNumber'],
          oneOf: 'patientListOptions'
        },
        {
          id: 4,
          value: 'email',
          label: 'Email',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'simple'
        },
        {
          id: 5,
          value: 'phone',
          label: 'Phone Number',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'simple'
        },
        {
          id: 6,
          value: 'phoneNumber',
          label: 'Phone Number',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        },
        {
          id: 7,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'input',
        }
      ],
      guarantorForm: [
        {
          id: 1,
          value: 'id',
          label: 'id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        },
        {
          id: 9,
          value: 'patientId',
          label: 'Patient',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: 'guarantorOptions'
        },
        {
          id: 2,
          value: 'patientType',
          label: 'Patient Type',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'simple',
        },

        {
          id: 4,
          value: 'guarantorFirstName',
          label: 'First Name',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        },
        {
          id: 5,
          value: 'guarantorLastName',
          label: 'Last Name',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        },
        {
          id: 5,
          value: 'guarantorGender',
          label: 'Gender',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          selectOption: true,
          type: 'select',
          oneOf: ['Male', 'Female', 'Other']
        },
        {
          id: 5,
          value: 'guarantorEmail',
          label: 'Email',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        },
        {
          id: 5,
          value: 'guarantorPhone',
          label: 'Phone',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        },
        {
          id: 5,
          value: 'guarantorDOB',
          label: 'Date of Birth',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          clearOption: true,
          showAll: true,
          type: 'date'
        },
        {
          id: 5,
          value: 'guarantorSS',
          label: 'SS#',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'simple'
        }
      ],
      subscriber: [
        {
          id: 1,
          value: 'id',
          label: 'id',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input'
        },
        {
          id: 2,
          value: 'firstName',
          label: 'First Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'lastName',
          label: 'Last Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'dob',
          label: 'DOB',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'address',
          label: 'Address',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'city',
          label: 'City',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 7,
          value: 'state',
          label: 'State',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 8,
          value: 'zip',
          label: 'Zip',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 9,
          value: 'phone',
          label: 'Phone',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'ssn',
          label: 'SSN',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 11,
          value: 'relationship',
          label: 'Relationship',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
          oneOf: [
            {
              label: "Brother (br)",
              value: "brother"
            },
            {
              label: "Caregiver (cg)",
              value: "caregiver"
            },
            {
              label: "Child (c)",
              value: "child"
            },
            {
              label: "Father (d)",
              value: "father"
            },
            {
              label: "Foster Child (fc)",
              value: "fosterChild"
            },
            {
              label: "Friend (f)",
              value: "friend"
            },
            {
              label: "Grandchild (gc)",
              value: "grandchild"
            },
            {
              label: "Grandfather (gf)",
              value: "grandfather"
            },
            {
              label: "Grandmother (gm)",
              value: "grandmother"
            },
            {
              label: "Grandparent (gp)",
              value: "grandparent"
            },
            {
              label: "Guardian (g)",
              value: "guardian"
            },
            {
              label: "Life Partner (lp)",
              value: "lifePartner"
            },
            {
              label: "Mother (m)",
              value: "mother"
            },
            {
              label: "Other (o)",
              value: "other"
            },
            {
              label: "Parent (p)",
              value: "parent"
            },
            {
              label: "Self (se)",
              value: "self"
            },
            {
              label: "Sibling (sb)",
              value: "sibling"
            },
            {
              label: "Sister (ss)",
              value: "sister"
            },
            {
              label: "Sitter (s)",
              value: "sitter"
            },
            {
              label: "Spouse (sp)",
              value: "spouse"
            },
            {
              label: "Stepchild (sc)",
              value: "stepchild"
            },
            {
              label: "Stepfather (sf)",
              value: "stepfather"
            },
            {
              label: "Stepmother (sm)",
              value: "stepmother"
            }
          ]
        },
        {
          id: 12,
          value: 'carrierId',
          label: 'Carrier Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'select',
          oneOf: 'dentalCarrier',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 13,
          value: 'memberId',
          label: 'Member ID',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 14,
          value: 'groupNumber',
          label: 'Group Number',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 15,
          value: 'planName',
          label: 'Plan Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 16,
          value: 'employerName',
          label: 'Employer Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 17,
          value: 'employerPhone',
          label: 'Employer Phone',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
      ],
    };
  }

  function products() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          csvRequired: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'manufacturer',
          label: 'Vendor',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          type: 'select',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
          oneOf: 'vendorOptions',
        },
        {
          id: 3,
          value: 'categoryName',
          label: 'Category',
          visible: true,
          editRecord: false,
          viewRecord: false,
          csvRequired: true,
          viewMode: false,
        },
        {
          id: 4,
          value: 'serialNumber',
          label: 'Reference Number',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          csvRequired: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'name',
          label: 'Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          csvRequired: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'parentId',
          label: 'Category',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          number: true,
          onChange: true,
          type: 'select',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
          oneOf: 'categoryParentOptions',
        },
        {
          id: 6,
          value: 'childId',
          label: 'Sub Category',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          number: true,
          type: 'select',
          oneOf: 'categoryChildOptions',
        },
        {
          id: 7,
          value: 'price',
          label: 'Retail Price',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          type: 'input',
          number: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 8,
          value: 'listPrice',
          label: 'Novadontics Price',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          type: 'input',
          number: true,
          formFieldDecorationOptions: {
            general: {
              required: false,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 9,
          value: 'quantity',
          label: 'Counts',
          number: true,
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          csvRequired: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 1,
              step: 1,
            },
          },
        },
        {
          id: 10,
          value: 'unit',
          label: 'UNIT',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          csvRequired: true,
          type: 'select',
          oneOf: ['BAG', 'BAIL', 'BARRELS', 'BOOK', 'BOTTLES', 'BOX', 'BUCKET', 'CAN', 'CARD', 'CARTON', 'CARTRIDGE', 'CASE', 'DOSE', 'DOZEN', 'DRUMS', 'EACH', 'FEET', 'GALLONS', 'GROSS', 'INCHES', 'JAR', 'KIT', 'LINEAR FEET', 'LOT', 'OUNCES', 'PACK', 'PAD', 'PAIR', 'PALLET', 'PIECES', 'PINT', 'POUNDS', 'QUART', 'REAM', 'ROLL', 'SET', 'SHELF PACK', 'SLEEVE', 'STRIP', 'SUPPOSITORY', 'TABLET', 'THOUSAND', 'TRAY', 'TUBE', 'VIAL', 'YARD'],
        },
        {
          id: 11,
          value: 'stockAvailable',
          label: 'Stock Available',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
        {
          id: 12,
          value: 'financeEligible',
          label: 'Eligible for financing',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
        {
          id: 13,
          value: 'description',
          label: 'Description',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          type: 'textarea',
          formFieldDecorationOptions: {
            general: {
              required: false,
            },
          },
        },
        {
          id: 14,
          value: 'imageUrl',
          label: 'Product Image',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 3,
          type: 'image',
        },
        {
          id: 15,
          value: 'document',
          label: 'Product Document',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          csvRequired: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 2,
          type: 'document',
        },
        {
          id: 16,
          value: 'video',
          label: 'Product Video',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          csvRequired: true,
          novaUpload: true,
          productUpload: true,
          allowed: 2,
          type: 'video',
        },
        {
          id: 17,
          value: 'categoryOptions',
          visible: false,
          editRecord: false,
          viewRecord: false,
          csvRequired: false,
          viewMode: false,
        },
      ],
      printpdf: [
        {
          id: 1,
          value: 'printpdf',
          label: 'Select Vendor',
          visible: false,
          editRecord: true,
          viewRecord: false,
          type: 'select',
          oneOf: 'vendorOptions'
        }
      ],
      exportcsv: [
        {
          id: 1,
          value: 'vendorId',
          label: 'Select Vendor',
          visible: false,
          editRecord: true,
          viewRecord: false,
          type: 'select',
          oneOf: 'vendorOptions'
        }
      ],
      orderHistory: [
        {
          Header: "Order No.",
          accessor: "id",
          Cell: props => <span>{props.value ? props.value : '-'}</span>
        },
        {
          Header: "Amount",
          accessor: "grandTotal",
          Cell: props => <span>${props.value ? props.value : '-'}</span>
        },
        {
          Header: "Order Items",
          accessor: "items",
          Cell: props => <span>{props.value ? props.value.length : '-'}</span>
        },
        {
          Header: "Order Date",
          accessor: "dateCreated",
          Cell: props => <span>{props.value ? moment(props.value).format('MM/DD/YYYY') : '-'}</span>
        },
        {
          Header: "Status",
          accessor: "status",
          Cell: props => <span>{props.value ? props.value : '-'}</span>
        },
      ],
      subscribeHistory: [
        {
          id: 1,
          value: 'serial',
          label: 'No',
          width: 60,
          serial: true,
          type: 'input'
        },
        {
          id: 2,
          value: 'productName',
          label: 'Product Name',
          width: 300,
          type: 'input'
        },
        {
          id: 3,
          value: 'vendorName',
          label: 'Vendor',
          width: 170,
          type: 'input'
        },
        {
          id: 4,
          value: 'quantity',
          label: 'Quantity',
          width: 150,
          type: 'input'
        },
        {
          id: 5,
          value: 'productPrice',
          label: 'Novadontics Price',
          width: 150,
          type: 'input'
        },
        {
          id: 6,
          value: 'salesTax',
          label: 'Sales Tax',
          width: 160,
          type: 'input'
        },
        {
          id: 7,
          value: 'shippingCharge',
          label: 'Shipping Charge',
          width: 160,
          type: 'input'
        },
        {
          id: 8,
          value: 'frequency',
          label: 'Deliver Every',
          width: 160,
          type: 'input'
        },
        {
          id: 9,
          value: 'startDate',
          label: 'Subscription Start Date',
          type: 'date',
          width: 160,
        },
        {
          id: 10,
          value: 'lastOrderedDate',
          label: 'Last Ordered Date',
          type: 'date',
          width: 160,
        },
        {
          id: 11,
          value: 'action',
          label: 'Action',
          action: true,
          width: 190,
          type: 'input',
        }
      ],
      PaymentForm: [
        {
          id: 1,
          value: 'cardNumber',
          label: 'Card Number',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
          maxLength: '16',
        },
        {
          id: 2,
          value: 'expirationDate',
          label: 'Expiration Date',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          dateFormat: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'cvv',
          label: 'CVV',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          hideText: true,
          required: true,
          type: 'input',
          maxLength: '4',
        },
        {
          id: 4,
          value: 'address',
          label: 'Address',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'city',
          label: 'City',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
        },
        {
          id: 6,
          value: 'state',
          label: 'State',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
        },
        {
          id: 7,
          value: 'zip',
          label: 'Zip Code',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
        },
        {
          id: 8,
          value: 'country',
          label: 'Country',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
        },
        {
          id: 9,
          value: 'phoneNumber',
          label: 'Phone Number',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          required: true,
          type: 'input',
        }
      ]
    };
  }

  function accounts() {
    return {
      columns: [
        {
          id: 0,
          value: '',
          label: '',
          computed: (row) => {
            if (row) {
              if (row.accountType != "Lead" && row.expired) {
                return { state: 'error', tooltipText: 'Account Expired' };
              } else if (row.accountType != "Lead" && row.daysUntilExpired <= 7 && row.daysUntilExpired >= 0) {
                return { state: 'warning', tooltipText: expirationNoticeText(row.daysUntilExpired) };
              } else if (row.accountType == 'Lead') {
                return { state: row.custToLeadFlag ? 'custToLead' : 'lead', tooltipText: 'Lead Account' };
              } else if (row.accountType == 'Trial') {
                return { state: 'success', tooltipText: 'Trial Account' };
              } else {
                return { state: 'success', tooltipText: 'Paid Account' };
              }
            }

          },
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'marker',
        },
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
        {
          id: 2,
          value: 'photo',
          label: 'Photo',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'image',
        },
        {
          id: 3,
          value: 'title',
          label: 'Title',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: [
            'Dr.',
            'Mr.',
            'Ms.',
            'Mrs.',
            'CDT'
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'accountType',
          label: 'Account Type',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          onChange: true,
          type: 'select',
          splitColumn: true,
          oneOf: ['Paid', 'Trial', 'ICOI', 'Lead', 'Corporate', 'Staff'],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'name',
          label: 'First Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          sort: true,
          sortLabel: 'First Name⇅',
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'lastName',
          label: 'Last Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          sort: true,
          sortLabel: 'Last Name⇅',
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 7,
          value: 'dateOfBirth',
          label: 'Date Of Birth',
          type: 'date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
        },
        {
          id:8,
          value: 'npiNumber',
          label: 'NPI Number',
          type: 'input',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
        },
        {
          id: 9,
          value: 'practiceId',
          label: 'Practice',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          disabled: true,
          type: 'select',
          oneOf: 'practiceOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'practiceName',
          label: 'Practice Name',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 11,
          value: 'dentalLicense',
          label: 'Dental License State/#',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 12,
          value: 'icoiMember',
          label: 'ICOI Member #',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 13,
          value: 'address',
          label: 'Address',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 14,
          value: 'suiteNumber',
          label: 'Suite #',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 15,
          value: 'city',
          label: 'City',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          sort: true,
          sortLabel: 'City⇅',
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 16,
          value: 'state',
          label: 'State',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 17,
          value: 'zip',
          label: 'ZIP',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 18,
          value: 'country',
          label: 'Country',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },

        {
          id: 19,
          value: 'cellPhone',
          label: 'Cell Phone',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 20,
          value: 'email',
          label: 'Email Address',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 21,
          value: 'phone',
          label: 'Business Phone',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 22,
          value: 'password',
          label: 'Password',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            create: {
              required: true,
            },
          },
        },
        {
          id:23,
          value: 'primaryFax',
          label: 'Primary Fax',
          type: 'input',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
        },
        {
          id: 24,
          value: 'udid',
          label: 'UDID1',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 25,
          value: 'udid1',
          label: 'UDID2',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 26,
          value: 'udid2',
          label: 'UDID3',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 27,
          value: 'marketingSource',
          label: 'Marketing Source',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: [
            'CII Seminar',
            'Magazine Ad',
            'Direct Mailer',
            'Email Blast',
            'Social Media',
            'Colleague Referral',
            'Dental Meeting',
            'Novadontics Meeting',
            'Master Program',
            'Vendor',
            'Developer',
            'Doctor Staff',
            'Nova Staff',
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 28,
          value: 'appSquares',
          label: 'Access Type',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          singleColumn: true,
          type: 'multicheckbox',
          oneOf: 'appSquareOptions'
        },
        {
          id: 29,
          value: 'prime',
          label: 'Prime Shipping',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
          sort: true,
          sortLabel: 'Novadontics Prime⇅',
        },
        {
          id: 30,
          value: 'allowAllCECourses',
          label: 'CE Courses / Unlimited',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
          sort: true,
          sortLabel: 'Allow All CE Courses⇅',
        },
        {
          id: 31,
          value: 'ciiPrograms',
          label: 'Implant Education Programs',
          accountStyle: true,
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          type: 'multiselect',
          oneOf: "ciiCourseCategoriesOptions"
        },
        {
          id: 32,
          value: 'role',
          label: 'Role',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            'Dentist',
            'Lab Technician',
            'Receptionist',
            'Fellowship Student',
            'Master Student',
            'Dental Assistant',
            'Continuing Ed'
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 33,
          value: 'plan',
          label: 'Plan',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          sort: true,
          sortLabel: 'Plan⇅',
          number: true,
          oneOf: [0, 995, 1800, 1995, 2388, 2995, 3995, 4995, 5995, 6995, 7995, 8995, 9995, 10995, 11995, 12500, 20000, 24000, 25000, 29000, 29995, 30000, 50000],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 34,
          value: 'accessStartDate',
          label: 'Access Start Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          sort: true,
          sortLabel: 'Access Start Date⇅',
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 35,
          value: 'expirationDate',
          label: 'Access End Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          sort: true,
          sortLabel: 'Access End Date⇅',
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 36,
          value: 'collectedDate',
          label: 'Collected Date',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 37,
          value: 'amountCollected',
          label: 'Collected Amount',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          number: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 38,
          value: 'authorizeCustId',
          label: 'Authorize Customer ID',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 39,
          value: 'mckessonId',
          label: 'McKesson ID',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'input',
        },
        {
          id: 40,
          value: 'doseSpotUserId',
          label: 'DoseSpot User ID',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 41,
          value: 'notes',
          label: 'Notes',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'textarea',
          maxLength: '1000',
        },
        {
          id: 42,
          value: 'contract',
          label: 'Contract',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          type: 'agreement',
        },
      ],
      extendPaidValidity: [
        {
          id: 1,
          value: 'accessStartDate',
          label: 'Access Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'expirationDate',
          label: 'Access End Date',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          sort: true,
          sortLabel: 'Expiration Date⇅',
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'collectedDate',
          label: 'Collected Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'amountCollected',
          label: 'Collected Amount',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          number: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
      extendValidity: [
        {
          id: 1,
          value: 'accessStartDate',
          label: 'Access Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'expirationDate',
          label: 'Access End Date',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          sort: true,
          sortLabel: 'Expiration Date⇅',
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ]
    };
  }

  function practices() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Practice Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'country',
          label: 'Country',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'state',
          label: 'State',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'city',
          label: 'City',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'zip',
          label: 'ZIP',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 7,
          value: 'address',
          label: 'Address',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 8,
          value: 'suite',
          label: 'Suite #',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 9,
          value: 'phone',
          label: 'Phone',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'appointmentReminder',
          label: 'Appointment Reminders',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          practiceCheck: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
        {
          id: 11,
          value: 'onlineRegistrationModule',
          label: 'Online Registration and Consent Forms',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          practiceCheck: true,
          sortLabel: "Online Registration Module",
          sort: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
        {
          id: 12,
          value: 'xvWebURL',
          label: 'XVWeb URL',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 13,
          value: 'xvWebUserName',
          label: 'XVWeb Username',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 14,
          value: 'xvWebPassword',
          label: 'XVWeb Password',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 15,
          value: 'doseSpotClinicId',
          label: 'DoseSpot Clinic ID',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 16,
          value: 'doseSpotClinicKey',
          label: 'DoseSpot Clinic Key',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 17,
          value: 'trojanAccountNo',
          label: 'Trojan Account Number',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
      ],
    };
  }

  function categories() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },

        }
      ],
    };
  }

  function productCategories() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },

        },
        {
          id: 3,
          value: 'parent',
          label: 'Vendor',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'select',
          oneOf: 'categoriesOptions',
        },
        {
          id: 4,
          value: 'taxExempted',
          label: 'Tax Exempted',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        }
      ],
    };
  }

  function ciicategories() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Sub Category',
          visible: true,
          createRecord: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },

        },
        {
          id: 3,
          value: 'categoryId',
          label: 'Category',
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: "color",
          label: "Color",
          visible: true,
          createRecord: true,
          editRecord: true,
          viewRecord: true,
          width: 100,
          viewMode: true,
          fullWidth: true,
          colors: [
            "#818284",
            "#f2ab00",
            "#38a0f1",
            "#ea6225",
            "#91288F",
            "#87b44d",
          ],
          type: "multicolor",
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
    };
  }

  function documents() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Title',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'url',
          label: 'URL',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          novaUpload: true,
          novaLabel: true,
          saveType: 'string',
          allowed: 1,
          type: 'video',
        },
        {
          id: 5,
          value: 'dateCreated',
          label: 'Date Created',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 6,
          value: 'thumbnailUrl',
          label: 'Thumbnail URL',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'thumbnail',
        },
        {
          id: 7,
          value: 'speaker',
          label: 'Author',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: false,
            },
          },
        },
      ],
    };
  }

  function courses() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Title',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'dateCreated',
          label: 'Date Created',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'url',
          label: 'Course Content',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          novaLabel: true,
          saveType: 'string',
          allowed: 1,
          type: 'video',
        },
        {
          id: 6,
          value: 'thumbnailUrl',
          label: 'Course Thumbnail',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'thumbnail',
        },
        {
          id: 7,
          value: 'courseQuestionnaire',
          label: 'Course Questionnaire',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'agreement',
        },
        {
          id: 8,
          value: 'speaker',
          label: 'Speaker',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 9,
          value: 'courseId',
          label: 'Course ID',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
    };
  }

  function ciicourses() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Title',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'dateCreated',
          label: 'Date Created',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'url',
          label: 'URL',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          novaLabel: true,
          saveType: 'string',
          allowed: 1,
          type: 'video',
        },
        {
          id: 6,
          value: 'thumbnailUrl',
          label: 'Thumbnail URL',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'thumbnail',
        },
        {
          id: 7,
          value: 'speaker',
          label: 'Speaker',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 8,
          value: 'courseId',
          label: 'Course ID',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
    };
  }

  function patientCourses() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Title',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'dateCreated',
          label: 'Date Created',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'url',
          label: 'URL',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          novaLabel: true,
          saveType: 'string',
          allowed: 1,
          type: 'video',
        },
        {
          id: 6,
          value: 'thumbnailUrl',
          label: 'Thumbnail URL',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'thumbnail',
        },
        {
          id: 7,
          value: 'speaker',
          label: 'Speaker',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
      ],
    };
  }

  function vendors() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Vendor',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'email',
          label: 'Email Address',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'shippingChargeLimit',
          label: 'Ground Shipping Charge Limit',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          number: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 5,
          value: 'shippingCharge',
          label: 'Ground Shipping Charge',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          number: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 6,
          value: 'tax',
          label: 'Sales Tax(%)',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          number: true,
          percentage: true,
          formFieldDecorationOptions: {
            general: {
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 7,
          value: 'creditCardFee',
          label: 'Processing Fee(%)',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          number: true,
          percentage: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 8,
          value: 'logoUrl',
          label: 'Logo',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'image',
        },
        {
          id: 9,
          value: 'groups',
          label: 'Groups',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'group',
          labelStyle: { marginTop: '1em' },
          fields: [
            {
              name: "groupId",
              type: "Select",
              title: "Group",
              editRecord: true,
              options: "vendorGroupsList",
              showDropdown: true,
              required: true,
              groupColumn: true,
              number: true
            },
            {
              name: "image",
              type: "Images",
              title: "Image",
              editRecord: true,
              attributes: {
                singleImage: true
              },
            },
            {
              name: "action",
              type: "Button",
              title: "Action",
              width: 150,
              editRecord: false,
              action: true
            },
          ]
        },
        {
          id: 10,
          value: 'deleted',
          label: 'Archived',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'booleanLabel',
          booleanLabelValue: (val) => val ? 'Archived' : 'Active',
        },
      ],
      vendorPrice: [
        {
          id: 1,
          value: 'priceIncreasePercent',
          label: 'Retail Price Percentage(%)',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          tax: true,
          number: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 2,
          value: 'listPriceIncreasePercent',
          label: 'Novadontics Price Percentage(%)',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          tax: true,
          number: true,
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 0.01,
            },
          },
        },
        {
          id: 3,
          value: 'categoryId',
          label: 'Category',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          number: true,
          onChange: true,
          type: 'select',
          oneOf: 'categoryParentOptions',
        },
        {
          id: 4,
          value: 'subCategoryId',
          label: 'Sub Category',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          csvRequired: false,
          number: true,
          type: 'select',
          oneOf: 'categoryChildOptions',
        },
      ],
      vendorCategories: [
        {
          id: 1,
          value: 'vendorId',
          label: 'Vendor',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          onChange: true,
          number: true,
          oneOf: 'vendors',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'categories',
          label: 'Categories',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          onChange: true,
          type: 'multiselect',
          oneOf: 'categories',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
      priceChangeHistory: [
        {
          Header: "Updated Date",
          accessor: "dateCreated",
          Cell: props => <span>{props.value ? moment(props.value).format('DD MMM, YYYY') : '-'}</span>
        },
        {
          Header: "Category",
          accessor: "categoryName",
          Cell: props => <span>{props.value ? props.value : ''}</span>
        },
        {
          Header: "Sub Category",
          accessor: "subCategoryName",
          Cell: props => <span>{props.value ? props.value : ''}</span>
        },
        {
          Header: "Retail Price Percentage",
          accessor: "priceIncreasePercent",
          Cell: props => <span>{props.value ? props.value : '-'}</span>
        },
        {
          Header: "Novadontics Price Percentage",
          accessor: "listPriceIncreasePercent",
          Cell: props => <span>{props.value ? props.value : '-'}</span>
        },
      ],
      stateTax: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'state',
          label: 'State',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'stateName',
          label: 'State Name',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'saleTax',
          label: 'Sales Tax',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          type: 'number',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'input',
        }
      ],
      saleTax: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'vendorId',
          label: 'Vendor ID',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'state',
          label: 'State',
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          type: 'select',
          oneOf: 'stateOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'stateName',
          label: 'State Name',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'tax',
          label: 'Tax',
          visible: true,
          editRecord: true,
          fieldRecord: true,
          viewRecord: false,
          viewMode: true,
          number: true,
          text: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'input',
        }
      ],
      viewDecorators: {
        deleteButton: (val) => {
          switch (val.deleted) {
            case true:
              return 'Unarchive';
            case false:
              return 'Archive';
            default:
              return undefined;
          }
        },
        deleteMessage: (val) => {
          switch (val.deleted) {
            case true:
              return 'Are you sure you want to unarchive this vendor? This will return all of the vendor’s products in the catalog.';
            case false:
              return 'Are you sure you want to archive this vendor? It will remove their products from the catalog.';
            default:
              return undefined;
          }
        },
        deleteAction: (record) => {
          if (record.deleted) {
            return unarchiveVendor;
          }
          return archiveVendor;
        },
      },
    };
  }

  function shipping() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Receiver Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'country',
          label: 'Country',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'state',
          label: 'State',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'city',
          label: 'City',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'zip',
          label: 'ZIP',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 7,
          value: 'address',
          label: 'Address',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 8,
          value: 'suite',
          label: 'Suite #',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 9,
          value: 'phone',
          label: 'Phone',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'primary',
          label: 'Primary',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: ['Yes', 'No']
        }
      ],
    };
  }

  function consents(records = []) {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'visit',
          label: 'Visit',
          visible: true,
          editRecord: false,
          createRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: [...new Set(records.map(_ => _.visit))],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'step',
          label: 'Step',
          visible: true,
          editRecord: false,
          createRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: [...new Set(records.map(_ => _.step))],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'title',
          label: 'Title',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'document',
          label: 'Document',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'document',
        },
      ],
    }
  }

  function prescriptions() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'medication',
          label: 'Medication',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          sort: true,
          sortLabel: 'Medication⇅',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          sort: true,
          sortLabel: 'Category⇅',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'dosage',
          label: 'Dosage',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'count',
          label: 'Count',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 1,
            },
          },
        },
        {
          id: 6,
          value: 'instructions',
          label: 'Instructions',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 7,
          value: 'refills',
          label: 'Refills',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 1,
            },
          },
        },
        {
          id: 8,
          value: 'indications',
          label: 'Indications',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 1,
            },
          },
        },
        {
          id: 9,
          value: 'contraindications',
          label: 'Precautions & Contraindications',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
              min: 0,
              step: 1,
            },
          },
        },
        {
          id: 10,
          value: 'medicationInteraction',
          label: 'Medication Interaction',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 11,
          value: 'useInOralImplantology',
          label: 'Use in Oral Implantology',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 12,
          value: 'genericSubstituteOk',
          label: 'Generic Substitute OK',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          booleanLabelValue: (val) => val ? 'Yes' : 'No',
          type: 'checkbox',
        },
      ],
      profile: [
        {
          id: 1,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          onChange: true,
          type: 'select',
          oneOf: 'providers',
          style: { display: 'flex', paddingTop: '8px', paddingBottom: '15px' },
          labelStyle: { width: '175px', marginTop: '1em' },
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'deaNumber',
          label: 'DEA Number',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          newLine: true,
          type: 'input',
          style: { display: 'flex', paddingTop: '8px', paddingBottom: '15px' },
          labelStyle: { width: '175px', marginTop: '1em' },
        },
        {
          id: 3,
          value: 'npiNumber',
          label: 'NPI Number',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          type: 'input',
          style: { display: 'flex', paddingTop: '8px', paddingBottom: '15px' },
          labelStyle: { width: '175px', marginTop: '1em' },
        }
      ]
    }
  }

  function educationRequests() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: 'CE Request ID',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
        {
          id: 2,
          value: 'course',
          label: 'Course',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'courseId',
          label: 'Course Id',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'speaker',
          label: 'Speaker',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 6,
          value: 'dateRequested',
          label: 'Date Requested',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 7,
          value: 'dentist',
          label: 'Dentist',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 8,
          value: 'dentistPhone',
          label: 'Dentist Phone',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 9,
          value: 'dentistEmail',
          label: 'Dentist Email',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 10,
          value: 'note',
          label: 'Note',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 11,
          value: 'status',
          label: 'Status',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          splitColumn: true,
          type: 'select',
          oneOf: [
            'Open',
            'Closed'
          ],
        },
        {
          id: 12,
          value: 'returnQuestionnaire',
          label: 'Return/Completed Questionnaire',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'agreement',
        },
        {
          id: 13,
          value: 'closedDate',
          label: 'Closed Date',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
      ],
      RequestForm: [
        {
          id: 1,
          value: 'name',
          label: 'Request for Dr.',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Course Title',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input'
        },
        {
          id: 3,
          value: 'notes',
          label: 'Note (Optional)',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        }
      ],
    };
  }

  function procedure() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'procedureCode',
          label: 'Procedure Code',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'procedureType',
          label: 'Procedure Description',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'categoryId',
          label: 'Category',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          number: true,
          type: 'select',
          oneOf: 'resourceCategoryOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        }
      ]
    }
  }

  function requestConsultation() {
    return {
      columns: [
        {
          text: '<div style="font-size: 14px;font-weight: 400;">Stop referring patients out of your practice. With just few clicks, Novadontics will send you the doctor you need to treat your patients in your office. Keep more money in your practice and provide better patient experience. Novadontics has a wide network of qualified clinicians who would love to come and help you in your practice by treating your patients in your office or by supervising you doing certain procedures for the first time. The fee list of our clinicians is based on a reasonable schedule that all of our freelancing doctors agreed to. <br></div>',
          title: 'Notes',
          type: 'InfoBlock',
        },
        {
          name: 'comment',
          title: 'Notes',
          type: 'Textarea'
        },
        {
          name: 'needsSurgicalDentist',
          title: 'Request for site visit by a surgical dentist',
          type: 'Checkbox',
        },
        {
          name: 'needsProstodonthist',
          title: 'Request for site visit by a prosthodontist',
          type: 'Checkbox',
        },
        {
          name: 'needsLabTechnician',
          title: 'Request for site visit by a lab technician',
          type: 'Checkbox',
        },
        {
          name: 'endodontist',
          title: 'Request for site visit by an endodontist',
          type: 'Checkbox',
          newStyle: true,
        },
        {
          text: '<div style="font-size: 12px;font-weight: 400;"> Disclaimer Novadontics, LLC. does not engage in the practice of dentistry. It facilitates consultation and in-office visit requests by allowing Novadontics software licensees to request assistance through the software. All dental and other professional service fees will be payable to the consultants or other third party designee by Novadontics, LLC. upon collecting the fee. Requesting a consultation or in-office professional services through the Novadontics software means you understand and agree with this disclaimer. </div>',
          type: 'InfoBlock',
          title: 'Disclaimer'
        }
      ]
    }
  }

  function insurance() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
        {
          id: 2,
          value: 'procedureCode',
          label: 'CDT Code',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          width: 300,
          type: 'input',
        },
        {
          id: 3,
          value: 'procedureType',
          label: 'Procedure Description',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          width: 900,
          type: 'input',
        },
        {
          id: 4,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          width: 400,
          action: true,
          type: 'input',
        }
      ]
    }
  }

  function clinicalNotes() {
    return {
      columns: [
        {
          "title": "Clinical Notes",
          "forms": [
            {
              "id": "1Z",
              "version": 1,
              "title": "Clinical Notes",
              "icon": "PersonalInfo",
              "schema": [
                {
                  "rows": [
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "date",
                                "type": "Date",
                                "title": "Date",
                                "defaultDate": true,
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "time",
                                "type": "Time",
                                "title": "Time",
                                "required": true,
                                "oneOf": {
                                  "time": [
                                    {
                                      "id": 1,
                                      "label": "10:00",
                                      "value": "10:00"
                                    },
                                    {
                                      "id": 2,
                                      "label": "10:15",
                                      "value": "10:15"
                                    },
                                    {
                                      "id": 3,
                                      "label": "10:30",
                                      "value": "10:30"
                                    },
                                    {
                                      "id": 4,
                                      "label": "10:45",
                                      "value": "10:45"
                                    },
                                    {
                                      "id": 5,
                                      "label": "11:00",
                                      "value": "11:00"
                                    },
                                    {
                                      "id": 6,
                                      "label": "11:15",
                                      "value": "11:15"
                                    },
                                    {
                                      "id": 7,
                                      "label": "11:30",
                                      "value": "11:30"
                                    },
                                    {
                                      "id": 8,
                                      "label": "11:45",
                                      "value": "11:45"
                                    },
                                    {
                                      "id": 9,
                                      "label": "12:00",
                                      "value": "12:00"
                                    },
                                    {
                                      "id": 10,
                                      "label": "12:15",
                                      "value": "12:15"
                                    },
                                    {
                                      "id": 11,
                                      "label": "12:30",
                                      "value": "12:30"
                                    },
                                    {
                                      "id": 12,
                                      "label": "12:45",
                                      "value": "12:45"
                                    },
                                    {
                                      "id": 13,
                                      "label": "01:00",
                                      "value": "01:00"
                                    },
                                    {
                                      "id": 14,
                                      "label": "01:15",
                                      "value": "01:15"
                                    },
                                    {
                                      "id": 15,
                                      "label": "01:30",
                                      "value": "01:30"
                                    },
                                    {
                                      "id": 16,
                                      "label": "01:45",
                                      "value": "01:45"
                                    },
                                    {
                                      "id": 17,
                                      "label": "02:00",
                                      "value": "02:00"
                                    },
                                    {
                                      "id": 18,
                                      "label": "02:15",
                                      "value": "02:15"
                                    },
                                    {
                                      "id": 19,
                                      "label": "02:30",
                                      "value": "02:30"
                                    },
                                    {
                                      "id": 20,
                                      "label": "02:45",
                                      "value": "02:45"
                                    },
                                    {
                                      "id": 21,
                                      "label": "03:00",
                                      "value": "03:00"
                                    },
                                    {
                                      "id": 22,
                                      "label": "03:15",
                                      "value": "03:15"
                                    },
                                    {
                                      "id": 23,
                                      "label": "03:30",
                                      "value": "03:30"
                                    },
                                    {
                                      "id": 24,
                                      "label": "03:45",
                                      "value": "03:45"
                                    },
                                    {
                                      "id": 25,
                                      "label": "04:00",
                                      "value": "04:00"
                                    },
                                    {
                                      "id": 26,
                                      "label": "04:15",
                                      "value": "04:15"
                                    },
                                    {
                                      "id": 27,
                                      "label": "04:30",
                                      "value": "04:30"
                                    },
                                    {
                                      "id": 28,
                                      "label": "04:45",
                                      "value": "04:45"
                                    },
                                    {
                                      "id": 29,
                                      "label": "05:00",
                                      "value": "05:00"
                                    },
                                    {
                                      "id": 30,
                                      "label": "05:15",
                                      "value": "05:15"
                                    },
                                    {
                                      "id": 31,
                                      "label": "05:30",
                                      "value": "05:30"
                                    },
                                    {
                                      "id": 32,
                                      "label": "05:45",
                                      "value": "05:45"
                                    },
                                    {
                                      "id": 33,
                                      "label": "06:00",
                                      "value": "06:00"
                                    },
                                    {
                                      "id": 34,
                                      "label": "06:15",
                                      "value": "06:15"
                                    },
                                    {
                                      "id": 35,
                                      "label": "06:30",
                                      "value": "06:30"
                                    },
                                    {
                                      "id": 36,
                                      "label": "06:45",
                                      "value": "06:45"
                                    },
                                    {
                                      "id": 37,
                                      "label": "07:00",
                                      "value": "07:00"
                                    },
                                    {
                                      "id": 38,
                                      "label": "07:15",
                                      "value": "07:15"
                                    },
                                    {
                                      "id": 39,
                                      "label": "07:30",
                                      "value": "07:30"
                                    },
                                    {
                                      "id": 40,
                                      "label": "07:45",
                                      "value": "07:45"
                                    },
                                    {
                                      "id": 41,
                                      "label": "08:00",
                                      "value": "08:00"
                                    },
                                    {
                                      "id": 42,
                                      "label": "08:15",
                                      "value": "08:15"
                                    },
                                    {
                                      "id": 43,
                                      "label": "08:30",
                                      "value": "08:30"
                                    },
                                    {
                                      "id": 44,
                                      "label": "08:45",
                                      "value": "08:45"
                                    },
                                    {
                                      "id": 45,
                                      "label": "09:00",
                                      "value": "09:00"
                                    },
                                    {
                                      "id": 46,
                                      "label": "09:15",
                                      "value": "09:15"
                                    },
                                    {
                                      "id": 47,
                                      "label": "09:30",
                                      "value": "09:30"
                                    },
                                    {
                                      "id": 48,
                                      "label": "09:45",
                                      "value": "09:45"
                                    }
                                  ],
                                  "clock": [
                                    {
                                      "id": 1,
                                      "label": "AM",
                                      "value": "AM"
                                    },
                                    {
                                      "id": 2,
                                      "label": "PM",
                                      "value": "PM"
                                    }
                                  ]
                                }
                              }
                            },
                            {
                              "field": {
                                "name": "provider",
                                "type": "Select",
                                "title": "Provider",
                                "options": "providers",
                                "showDropdown": true,
                                "required": true,
                                "number": true
                              }
                            },
                            {
                              "field": {
                                "name": "assistant1",
                                "type": "Select",
                                "title": "Assistant 1",
                                "options": "assistantOptions",
                                "showDropdown": true,
                              }
                            },
                            {
                              "field": {
                                "name": "assistant2",
                                "type": "Select",
                                "title": "Assistant 2",
                                "options": "assistantOptions",
                                "showDropdown": true,
                              }
                            },
                            {
                              "field": {
                                "name": "assistant3",
                                "type": "Select",
                                "title": "Assistant 3",
                                "options": "assistantOptions",
                                "showDropdown": true,
                              }
                            },
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "dob",
                                "type": "Input",
                                "disabled": true,
                                "title": 'DOB'
                              }
                            },
                            {
                              "field": {
                                "name": "allergies",
                                "type": "Input",
                                "title": '<span style= "color: #EA6225;">Allergies</span>'
                              }
                            },
                            {
                              "field": {
                                "name": "medicalConditions",
                                "type": "Input",
                                "title": '<span style= "color: #EA6225;">Medical Conditions</span>'
                              }
                            },
                            {
                              "field": {
                                "name": "postSurgicalHistory",
                                "type": "Input",
                                "title": "Current Surgical & Prosthetic Treatment Plan"
                              }
                            },
                            {
                              "field": {
                                "name": "diagnosis",
                                "type": "Input",
                                "title": "Declined Treatment Plans Offered"
                              }
                            },
                            {
                              "field": {
                                "name": "escort",
                                "type": "Input",
                                "title": "Escort"
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "procedure",
                                "type": "ViewTable",
                                "title": "Procedure",
                                "noteStyle": true,

                                "fields": [
                                  {
                                    "name": "tooth",
                                    "type": "Input",
                                    "title": "Tooth #",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "procedureCode",
                                    "type": "Search",
                                    "title": "Procedure Code",
                                    "editRecord": true,
                                    "options": "cdcCode",
                                    "ref": "procedureDescription"
                                  },
                                  {
                                    "name": "procedureDescription",
                                    "type": "Search",
                                    "title": "Procedure Description",
                                    "editRecord": true,
                                    "options": "procedureName",
                                    "view": true,
                                    "limit": 30,
                                    "ref": "procedureCode",
                                  },
                                  {
                                    "name": "fee",
                                    "type": "Input",
                                    "title": "Fee",
                                    "currency": true,
                                    "editRecord": true,
                                  },
                                  {
                                    "name": "action",
                                    "type": "Button",
                                    "title": "Action",
                                    "editRecord": false,
                                    "action": true
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "monitors",
                                    "type": "MultiCheckbox",
                                    "title": "Monitors",
                                    "blue_clr": true,
                                    "labelStyle": true,
                                    "blueCheckbox": true,
                                    "collapse": true,
                                    "options": [
                                      {
                                        "value": "NIBP",
                                        "title": "NIBP"
                                      },
                                      {
                                        "value": "EKG",
                                        "title": "EKG"
                                      },
                                      {
                                        "value": "SpO2",
                                        "title": "SpO2"
                                      },
                                      {
                                        "value": "Pulse",
                                        "title": "Pulse"
                                      },
                                      {
                                        "value": "Respiration",
                                        "title": "Respiration"
                                      },
                                      {
                                        "value": "ETCo2",
                                        "title": "ETCO2"
                                      },
                                      {
                                        "value": "Pretracheal Steth",
                                        "title": "Pretracheal Steth"
                                      }
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "anesthesia",
                                    "type": "MultiCheckbox",
                                    "title": "Anesthesia",
                                    "blue_clr":true,
                                    "labelStyle": true,
                                    "blueCheckbox": true,
                                    "collapse": true,
                                    "options": [
                                      {
                                        "value": "IVGA",
                                        "title": "IVGA"
                                      },
                                      {
                                        "value": "Local",
                                        "title": "Local"
                                      },
                                      {
                                        "value": "O2",
                                        "title": "O2"
                                      },
                                      {
                                        "value": "Nitrous",
                                        "title": "Nitrous"
                                      },
                                      {
                                        "value": "IVsed",
                                        "title": "IV Sed"
                                      },
                                      {
                                        "value": "Posed",
                                        "title": "PO Sed"
                                      },
                                      {
                                        "value": "Topical",
                                        "title": "Topical"
                                      },
                                      {
                                        "value": "Volatile Agent",
                                        "title": "Volatile Agent"
                                      }
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "asaStatus",
                                    "type": "Radiobox",
                                    "title": "ASA Physical Status",
                                    "blue_clr": true,
                                    "labelStyle": true,
                                    "blueCheckbox": true,
                                    "collapse": true,
                                    "info": {
                                      "text": "<div style='font-weight: normal;'>The American Society of Anesthesiologists (ASA) Physical Status classification system was initially created in 1941 by the American Society of Anesthetists, an organization that later became the ASA.<br /><br />\nThe purpose of the grading system is simply to evaluate the degree of a patient&#39;s &quot;sickness&quot; or &quot;physical state&quot; before selecting the anesthetic or before performing surgery. Describing patients&#39; preoperative physical status is used for recordkeeping, for communicating between colleagues, and to create a uniform system for statistical analysis. The grading system is not intended for use as a measure to predict operative risk.<br />\n<br />\nThe modern classification system consists of 6 categories, as described below.<br />\n<br />\n<b>ASA PS 1</b><br />\n<br />\nNormal healthy patient. No organic, physiologic, or psychiatric disturbance; excludes the very young and very old; healthy with good exercise tolerance<br />\n<br />\n<b>ASA PS 2</b><br />\n<br />\nPatients with mild systemic disease. No functional limitations; has a well-controlled disease of one body system; controlled hypertension or diabetes without systemic effects, cigarette smoking without chronic obstructive pulmonary disease (COPD); mild obesity, pregnancy<br />\n<br />\n<b>ASA PS 3</b><br />\n<br />\nPatients with severe systemic disease. Some functional limitation; has a controlled disease of more than one body system or one major system; no immediate danger of death; controlled congestive heart failure (CHF), stable angina, old heart attack, poorly controlled hypertension, morbid obesity, chronic renal failure; bronchospastic disease with intermittent symptoms<br />\n<br />\n<b>ASA PS 4</b><br />\n<br />\nPatients with severe systemic disease that is a constant threat to life. Has at least one severe disease that is poorly controlled or at end stage; possible risk of death; unstable angina, symptomatic COPD, symptomatic CHF, hepatorenal failure<br />\n<br />\n<b>ASA PS 5</b><br />\n<br />\nMoribund patients who are not expected to survive without the operation. Not expected to survive &gt; 24 hours without surgery; imminent risk of death; multiorgan failure, sepsis syndrome with hemodynamic instability, hypothermia, poorly controlled coagulopathy<br />\n<br />\n<b>ASA PS 6</b><br />\n<br />\nA declared brain-dead patient whose organs are being removed for donor purposes<br />\n<br />\n<br />\n</div>",
                                      "type": "InfoBlock",
                                      "collapse": true,
                                      "title": "ASA Physical Status"
                                    },
                                    "options": [
                                      {
                                        "value": "1",
                                        "title": "1"
                                      },
                                      {
                                        "value": "2",
                                        "title": "2"
                                      },
                                      {
                                        "value": "3",
                                        "title": "3"
                                      },
                                      {
                                        "value": "4",
                                        "title": "4"
                                      }
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "airwayClass",
                                    "type": "Radiobox",
                                    "title": "Airway Class",
                                    "blue_clr": true,
                                    "labelStyle": true,
                                    "blueCheckbox": true,
                                    "collapse": true,
                                    "info": {
                                      "openModal": true,
                                      "text": `<b style="font-size: 16px; margin-left: 25px">Mallampati score</b><br />\n<br />\n<div style="margin-left: 25px; font-weight: normal;"><img src=${require('img/patients/airway.jpg')} width="150" /><br />\n<br />\nIn anesthesia, the Mallampati score or Mallampati classification, named after the Indian-born American anaesthesiologist Seshagiri Mallampati, is used to predict the ease of endotracheal intubation. The test comprises a visual assessment of the distance from the tongue base to the roof of the mouth, and therefore the amount of space in which there is to work. It is an indirect way of assessing how difficult an intubation will be; this is more definitively scored using the Cormack-Lehane classification system, which describes what is actually seen using direct laryngoscopy during the intubation process itself. A high Mallampati score (class 3 or 4) is associated with more difficult intubation as well as a higher incidence of sleep apnea.<br />\n<br />\n<b>Technique:</b><br />\n<br />\nThe score is assessed by asking the patient, in a sitting posture, to open his or her mouth and to protrude the tongue as much as possible. The anatomy of the oral cavity is visualized; specifically, the assessor notes whether the base of the uvula, faucial pillars (the arches in front of and behind the tonsils) and soft palate are visible. Scoring is generally done without phonation. Depending on whether the tongue is maximally protruded and/or the patient asked to phonate, the scoring may vary.<br />\n<br />\n<b>Modified Mallampati Scoring:</b><br />\n<br />\n<ul><li>Class I: Soft palate, uvula, fauces, pillars visible.</li><li>Class II: Soft palate, major part of uvula, fauces visible.</li><li>Class III: Soft palate, base of uvula visible.</li><li>Class IV: Only hard palate visible.</li></ul><br />\n<br />\n<b>Original Mallampati Scoring:</b><br />\n<br />\n<ul><li>Class 1: Faucial pillars, soft palate and uvula could be visualized.</li><li>Class 2: Faucial pillars and soft palate could be visualized, but uvula was masked by the base of the tongue.</li><li>Class 3: Only soft palate visualized.</li></ul>\nFurther research may be needed to determine the most effective consistent and predictive approach on which to standardize Mallampati Scoring.<br />\n<br />\n<b>Clinical significance</b><br />\n<br />\nWhile Mallampati classes I and II are associated with relatively easy intubation, classes III and IV are associated with increased difficulty.<br />\nA systematic review of 42 studies, with 34,513 participants, found that the modified Mallampati score is a good predictor of difficult direct laryngoscopy and intubation, but poor at predicting difficult bag mask ventilation. Therefore, the study concluded that while useful in combination with other tests to predict the difficulty of an airway, it is not sufficiently accurate alone.<br />\n<br />\n</div>`,
                                      "type": "InfoBlock",
                                      "title": "Airway Class"
                                    },
                                    "options": [
                                      {
                                        "value": "1",
                                        "title": "1"
                                      },
                                      {
                                        "value": "2",
                                        "title": "2"
                                      },
                                      {
                                        "value": "3",
                                        "title": "3"
                                      },
                                      {
                                        "value": "4",
                                        "title": "4"
                                      }
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "noteStyle": true,
                          "title": '<span style= "color: #38a0f1;">Pre-Surgical Vitals Entry</span>',
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "bp",
                                    "type": "Input",
                                    "title": "B/P"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noteStyle": true,
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "pulse",
                                    "type": "Input",
                                    "title": "Pulse"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noteStyle": true,
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "spco2",
                                    "type": "Input",
                                    "title": "SpCo2"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noteStyle": true,
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "co2",
                                    "type": "Input",
                                    "title": "Co2"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noteStyle": true,
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "ekg",
                                    "type": "Input",
                                    "title": "EKG"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noteStyle": true,
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "resp",
                                    "type": "Input",
                                    "title": "Respiration Rate"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noteStyle": true,
                          "columnItems": [
                            {
                              "group": {
                                "fields": [
                                  {
                                    "name": "temp",
                                    "type": "Input",
                                    "title": "Temp"
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    /*{
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": "",
                                "fields": []
                              }
                            }
                          ]
                        }
                      ]
                    },*/
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "flexStyle": true,
                              "group": {
                                "title":'<span style= "color: #38a0f1;">Intraoperative Vitals Record</span>',
                                "inlineStyle": true,
                               "fields": [
                                  {
                                    "name": "vitalsPhoto",
                                    "type": "Images",
                                    "title": ""
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": "",
                                "fields": []
                              }
                            }
                          ]
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "preMedicationTotal",
                                "type": "Table",
                                "title": "Pre-medication Total",
                                "orange_clr":true,
                                "fields": [
                                  {
                                    "name": "medication",
                                    "type": "Input",
                                    "title": "Medication",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "dosage",
                                    "type": "Input",
                                    "title": "Dose",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "amount",
                                    "type": "Input",
                                    "title": "Amount",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "IVsite",
                                    "type": "Select",
                                    "title": "Route/Technique",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Infiltration",
                                        "title": "Infiltration"
                                      },
                                      {
                                        "value": "IAN Block",
                                        "title": "IAN Block"
                                      },
                                      {
                                        "value": "Oral",
                                        "title": "Oral"
                                      },
                                      {
                                        "value": "Sublingual",
                                        "title": "Sublingual"
                                      },
                                      {
                                        "value": "Topical",
                                        "title": "Topical"
                                      },
                                      {
                                        "value": "Buccal",
                                        "title": "Buccal"
                                      },
                                      {
                                        "value": "V2 Block",
                                        "title": "V2 Block"
                                      },
                                      {
                                        "value": "IO Block",
                                        "title": "IO Block"
                                      },
                                      {
                                        "value": "IV",
                                        "title": "IV"
                                      },
                                      {
                                        "value": "Intramuscular",
                                        "title": "Intramuscular"
                                      },
                                      {
                                        "value": "Intraosseous",
                                        "title": "Intraosseous"
                                      },
                                      {
                                        "value": "Inhalable",
                                        "title": "Inhalable"
                                      },
                                      {
                                        "value": "Nasal",
                                        "title": "Nasal"
                                      },
                                      {
                                        "value": "Transdermal",
                                        "title": "Transdermal"
                                      },
                                      {
                                        "value": "Subcutaneous",
                                        "title": "Subcutaneous"
                                      },
                                      {
                                        "value": "Rectal",
                                        "title": "Rectal"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "action",
                                    "type": "Button",
                                    "title": "Action",
                                    "editRecord": false,
                                    "action": true
                                  }
                                ]
                              }
                            },
                          ],
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "medicationEntry",
                                "type": "Table",
                                "title": "Intraoperative Oral, IV and Local Anesthetic Total",
                                "orange_clr":true,
                                "fields": [
                                  {
                                    "name": "oralMedication",
                                    "type": "Input",
                                    "title": "Medication",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "oralDosage",
                                    "type": "Input",
                                    "title": "Dose",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "oralAmount",
                                    "type": "Input",
                                    "title": "Amount",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "oralIVsite",
                                    "type": "MultiSelect",
                                    "title": "Route/Technique",
                                    "fullWidth": true,
                                    "removePadding": true,
                                    "editRecord": true,
                                    "options": [
                                      "Infiltration",
                                      "IAN Block",
                                      "Oral",
                                      "Sublingual",
                                      "Topical",
                                      "Buccal",
                                      "V2 Block",
                                      "IO Block",
                                       "IV",
                                       "Intramuscular",
                                      "Intraosseous",
                                      "Inhalable",
                                      "Nasal",
                                      "Transdermal",
                                      "Subcutaneous",
                                      "Rectal",
                                   ]
                                  },
                                  {
                                    "name": "action",
                                    "type": "Button",
                                    "title": "Action",
                                    "editRecord": false,
                                    "action": true
                                  }
                                ]
                              }
                            },
                          ],
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                  "title": '<span style= "color: #EA6225;">Incision & Flap Type / Maxilla <span style= "color: gray; font-size: 16px;">(Choose one or more)</span></span>',
                                  "fields": [
                                    {
                                      "name": "incision",
                                      "type": "MultiSelect",
                                      "fullWidth": true,
                                      "removePadding": true,
                                      "title": "",
                                      "options": [
                                        "Mid-crestal",
                                        "Midline",
                                        "Mesial Releasing",
                                        "Distal Releasing",
                                        "Sulcular",
                                        "Papilla-sparing",
                                        "Envelop",
                                        "Full thickness",
                                        "Partial thickness",
                                        "2-wall Flap",
                                        "3-wall Flap",
                                        "Flapless"
                                      ]
                                    }
                                  ]
                                }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                  "title": '<span style= "color: #EA6225;">Incision & Flap Type / Mandible <span style= "color: gray; font-size: 16px;">(Choose one or more)</span></span>',
                                 "fields": [
                                    {
                                      "name": "incisionMandible",
                                      "type": "MultiSelect",
                                      "fullWidth": true,
                                      "removePadding": true,
                                      "title": "",
                                      "options": [
                                        "Mid-crestal",
                                        "Midline",
                                        "Mesial Releasing",
                                        "Distal Releasing",
                                        "Sulcular",
                                        "Papilla-sparing",
                                        "Envelop",
                                        "Full thickness",
                                        "Partial thickness",
                                        "2-wall Flap",
                                        "3-wall Flap",
                                        "Flapless"
                                      ]
                                    }
                                  ]
                              }
                            }
                          ]
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "implantData",
                                "type": "Table",
                                "title": "Implant Data",
                                "orange_clr":true,
                                "noteStyle": true,
                                "duplicate": true,
                                "pageSize": 8,
                                "fields": [
                                  {
                                    "name": "implantSite",
                                    "type": "Select",
                                    "title": "Implant Site #",
                                    "editRecord": true,
                                    "required": true,
                                    "orange_clr":true,
                                    "options": [
                                      {
                                        "value": "1 / 18",
                                        "title": "1 / 18"
                                      },
                                      {
                                        "value": "2 / 17",
                                        "title": "2 / 17"
                                      },
                                      {
                                        "value": "3 / 16",
                                        "title": "3 / 16"
                                      },
                                      {
                                        "value": "4 / 15",
                                        "title": "4 / 15"
                                      },
                                      {
                                        "value": "5 / 14",
                                        "title": "5 / 14"
                                      },
                                      {
                                        "value": "6 / 13",
                                        "title": "6 / 13"
                                      },
                                      {
                                        "value": "7 / 12",
                                        "title": "7 / 12"
                                      },
                                      {
                                        "value": "8 / 11",
                                        "title": "8 / 11"
                                      },
                                      {
                                        "value": "9 / 21",
                                        "title": "9 / 21"
                                      },
                                      {
                                        "value": "10 / 22",
                                        "title": "10 / 22"
                                      },
                                      {
                                        "value": "11 / 23",
                                        "title": "11 / 23"
                                      },
                                      {
                                        "value": "12 / 24",
                                        "title": "12 / 24"
                                      },
                                      {
                                        "value": "13 / 25",
                                        "title": "13 / 25"
                                      },
                                      {
                                        "value": "14 / 26",
                                        "title": "14 / 26"
                                      },
                                      {
                                        "value": "15 / 27",
                                        "title": "15 / 27"
                                      },
                                      {
                                        "value": "16 / 28",
                                        "title": "16 / 28"
                                      },
                                      {
                                        "value": "17 / 38",
                                        "title": "17 / 38"
                                      },
                                      {
                                        "value": "18 / 37",
                                        "title": "18 / 37"
                                      },
                                      {
                                        "value": "19 / 36",
                                        "title": "19 / 36"
                                      },
                                      {
                                        "value": "20 / 35",
                                        "title": "20 / 35"
                                      },
                                      {
                                        "value": "21 / 34",
                                        "title": "21 / 34"
                                      },
                                      {
                                        "value": "22 / 33",
                                        "title": "22 / 33"
                                      },
                                      {
                                        "value": "23 / 32",
                                        "title": "23 / 32"
                                      },
                                      {
                                        "value": "24 / 31",
                                        "title": "24 / 31"
                                      },
                                      {
                                        "value": "25 / 41",
                                        "title": "25 / 41"
                                      },
                                      {
                                        "value": "26 / 42",
                                        "title": "26 / 42"
                                      },
                                      {
                                        "value": "27 / 43",
                                        "title": "27 / 43"
                                      },
                                      {
                                        "value": "28 / 44",
                                        "title": "28 / 44"
                                      },
                                      {
                                        "value": "29 / 45",
                                        "title": "29 / 45"
                                      },
                                      {
                                        "value": "30 / 46",
                                        "title": "30 / 46"
                                      },
                                      {
                                        "value": "31 / 47",
                                        "title": "31 / 47"
                                      },
                                      {
                                        "value": "32 / 48",
                                        "title": "32 / 48"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "implantBrand",
                                    "type": "Select",
                                    "title": "Implant Brand",
                                    "listOptions": "implantBrandOptions",
                                    "showDropdown": true,
                                    "required": true,
                                    "orange_clr":true,
                                    "editRecord": true
                                  },
                                  {
                                    "name": "implantSurface",
                                    "type": "Select",
                                    "title": "Implant Surface",
                                    "showDropdown": true,
                                    "number": true,
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Acqua",
                                        "title": "Acqua"
                                      },
                                      {
                                        "value": "Bio Spark",
                                        "title": "Bio Spark"
                                      },
                                      {
                                        "value": "Etched Ti",
                                        "title": "Etched Ti"
                                      },
                                      {
                                        "value": "HA",
                                        "title": "HA"
                                      },
                                      {
                                        "value": "HSA",
                                        "title": "HSA"
                                      },
                                      {
                                        "value": "MP-1 HA",
                                        "title": "MP-1 HA"
                                      },
                                      {
                                        "value": "RBM",
                                        "title": "RBM"
                                      },
                                      {
                                        "value": "SLA",
                                        "title": "SLA"
                                      },
                                      {
                                        "value": "TiUltra",
                                        "title": "TiUltra"
                                      },
                                      {
                                        "value": "TiUnite",
                                        "title": "TiUnite"
                                      },
                                    
                                      {
                                        "value": "TPS",
                                        "title": "TPS"
                                      },
                                      {
                                        "value": "Ultra Clean",
                                        "title": "Ultra Clean"
                                      },
                                      {
                                        "value": "ZLA",
                                        "title": "ZLA"
                                      }
                                    ]
                                  },
                                  {
                                    "name": "implantReference",
                                    "type": "Input",
                                    "title": "Implant Reference #",
                                    "required": true,
                                    "orange_clr":true,
                                    "editRecord": true
                                  },
                                  {
                                    "name": "implantLot",
                                    "type": "Input",
                                    "title": "Implant LOT #",
                                    "required": true,
                                    "orange_clr":true,
                                    "editRecord": true
                                  },
                                  {
                                    "name": "diameter",
                                    "type": "Select",
                                    "title": "Diameter",
                                    "required": true,
                                    "orange_clr":true,
                                    "editRecord": true,
                                    "options": Array.from(Array(76), (x, index) => index === 0 ? `${index + 2.5} mm` : `${2.5 + (index / 10)} mm`)
                                  },
                                  {
                                    "name": "length",
                                    "type": "Select",
                                    "title": "Length",
                                    "required": true,
                                    "orange_clr":true,
                                    "editRecord": true,
                                    "options": Array.from(Array(164), (x, index) => index === 0 ? `${index + 5} mm` : index < 151 ? `${5 + (index / 10)} mm` : index === 151 ? `${30} mm` : `${30 + ((index - 151) * 2.5)}`),
                                  },
                                  {
                                    "name": "surgProtocol",
                                    "type": "Select",
                                    "title": "Surgical Protocol",
                                    "showDropdown": true,
                                    "number": true,
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "One-Stage",
                                        "title": "One-Stage"
                                      },
                                      {
                                        "value": "Two-Stage",
                                        "title": "Two-Stage"
                                      },
                                      {
                                        "value": "Immediate Loading",
                                        "title": "Immediate Loading"
                                      }
                                    ]
                                  },
                                  {
                                    "name": "boneDensity",
                                    "type": "Select",
                                    "title": "Bone Density",
                                    "editRecord": true,
                                    "showDropdown": true,
                                    "number": true,
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "D1 >1000 HU",
                                        "title": "D1 >1000 HU"
                                      },
                                      {
                                        "value": "D2 701 - 1000 HU",
                                        "title": "D2 701 - 1000 HU"
                                      },
                                      {
                                        "value": "D3 250 - 700 HU",
                                        "title": "D3 250 - 700 HU"
                                      },
                                      {
                                        "value": "D4 <250 HU",
                                        "title": "D4 <250 HU"
                                      }
                                    ]
                                  },
                                  {
                                    "name": "osteotomySite",
                                    "type": "Select",
                                    "title": "Osteotomy Site",
                                    "showDropdown": true,
                                    "number": true,
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "HealedSite - NativeBone",
                                        "title": "Healed Site - Native Bone"
                                      },
                                      {
                                        "value": "HealedSite - BoneGraft",
                                        "title": "Healed Site - Bone Graft"
                                      },
                                      {
                                        "value": "Extraction & Immediate Placement",
                                        "title": "Extraction & Immediate Placement"
                                      },
                                      {
                                        "value": "Extraction & Immediate Placement With Bone Graft",
                                        "title": "Extraction & Immediate Placement With Bone Graft"
                                      }
                                    ]
                                  },
                                  {
                                    "name": "ncm",
                                    "type": "Select",
                                    "title": "Initial Ncm",
                                    "required": true,
                                    "orange_clr":true,
                                    "editRecord": true,
                                    "showDropdown": true,
                                    "number": true,
                                    "options": [
                                      {
                                        "value": "15 Ncm",
                                        "title": "15 Ncm"
                                      },
                                      {
                                        "value": "20 Ncm",
                                        "title": "20 Ncm"
                                      },
                                      {
                                        "value": "25 Ncm",
                                        "title": "25 Ncm"
                                      },
                                      {
                                        "value": "30 Ncm",
                                        "title": "30 Ncm"
                                      },
                                      {
                                        "value": "35 Ncm",
                                        "title": "35 Ncm"
                                      },
                                      {
                                        "value": "40 Ncm",
                                        "title": "40 Ncm"
                                      },
                                      {
                                        "value": "45 Ncm",
                                        "title": "45 Ncm"
                                      },
                                      {
                                        "value": "50 Ncm",
                                        "title": "50 Ncm"
                                      },
                                      {
                                        "value": "55 Ncm",
                                        "title": "55 Ncm"
                                      },
                                      {
                                        "value": "60 Ncm",
                                        "title": "60 Ncm"
                                      },
                                      {
                                        "value": "65 Ncm",
                                        "title": "65 Ncm"
                                      },
                                      {
                                        "value": "70 Ncm",
                                        "title": "70 Ncm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "isqBl",
                                    "type": "Select",
                                    "title": "Initial ISQ / BL",
                                    "editRecord": true,
                                    "showDropdown": true,
                                    "number": true,
                                    "options": Array.from(Array(100), (x, index) => index === 0 ? `${index + 1}` : index === 100 ? `` : `${index + 1}`)
                                  },
                                  {
                                    "name": "isqMd",
                                    "type": "Select",
                                    "title": "Initial ISQ / MD",
                                    "editRecord": true,
                                    "showDropdown": true,
                                    "number": true,
                                    "options": Array.from(Array(100), (x, index) => index === 0 ? `${index + 1}` : index === 100 ? `` : `${index + 1}`)
                                  },
                                  {
                                    "name": "implantDepth",
                                    "type": "Select",
                                    "title": "Implant Depth",
                                    "showDropdown": true,
                                    "number": true,
                                    "editRecord": true,
                                    "attributes": {
                                      "info": {
                                        "text": "Implant Platform Depth Relative to Mid-Buccal Crestal Bone"
                                      },
                                    },
                                    "options": [
                                      {
                                        "value": "+3.0 mm",
                                        "title": "+3.0 mm"
                                      },
                                      {
                                        "value": "+2.5 mm",
                                        "title": "+2.5 mm"
                                      },
                                      {
                                        "value": "+2.0 mm",
                                        "title": "+2.0 mm"
                                      },
                                      {
                                        "value": "+1.5 mm",
                                        "title": "+1.5 mm"
                                      },
                                      {
                                        "value": "+1.0 mm",
                                        "title": "+1.0 mm"
                                      },
                                      {
                                        "value": "+0.5 mm",
                                        "title": "+0.5 mm"
                                      },
                                      {
                                        "value": "Flush with Bone",
                                        "title": "Flush with Bone"
                                      },
                                      {
                                        "value": "-0.5 mm",
                                        "title": "-0.5 mm"
                                      },
                                      {
                                        "value": "-1.0 mm",
                                        "title": "-1.0 mm"
                                      },
                                      {
                                        "value": "-1.5 mm",
                                        "title": "-1.5 mm"
                                      },
                                      {
                                        "value": "-2.0 mm",
                                        "title": "-2.0 mm"
                                      },
                                      {
                                        "value": "-2.5 mm",
                                        "title": "-2.5 mm"
                                      },
                                      {
                                        "value": "-3.0 mm",
                                        "title": "-3.0 mm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "loadingProtocol",
                                    "type": "Select",
                                    "title": "Loading Protocol",
                                    "showDropdown": true,
                                    "number": true,
                                    "attributes": {
                                      "info": {
                                        "openModal": true,
                                        "text": `<div style="font-weight: normal;"><img src=${require('img/patients/loading-protocol.png')} width="750" />`
                                      },
                                    },
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Immediate Temporization 0-48h",
                                        "title": "Immediate Temporization 0-48h"
                                      },
                                      {
                                        "value": "Early Loading 48h-3 months",
                                        "title": "Early Loading 48h-3 months"
                                      },
                                      {
                                        "value": "Conventional Loading > 3 months",
                                        "title": "Conventional Loading > 3 months"
                                      }
                                    ]
                                  },
                                  {
                                    "name": "recommendedHT",
                                    "type": "Date",
                                    "title": "RRPSD",
                                    "width": 110,
                                    "attributes": {
                                      "info": {
                                        "text": "Recommended Restoration Phase Start Date"
                                      },
                                    },
                                    "editRecord": true,
                                  },
                                  {
                                    "name": "finalPA",
                                    "type": "Images",
                                    "title": "Final PA",
                                    "imageTitle": true,
                                    "editRecord": true,
                                    "attributes": {
                                      "singleImage": true
                                    },
                                  },
                                  {
                                    "name": "action",
                                    "type": "Button",
                                    "title": "Action",
                                    "width": 150,
                                    "editRecord": false,
                                    "action": true
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "boneGraftingData",
                                "type": "Table",
                                "title": "Bone Graft Data",
                                "orange_clr":true,
                                "noteStyle": true,
                                "pageSize": 8,
                                "fields": [
                                  {
                                    "name": "allograftMaterialUsed",
                                    "type": "Select",
                                    "title": "Allograft Type",
                                    "editRecord": true,
                                    "width": 200,
                                    "options": [
                                      {
                                        "value": "Corticocancellous",
                                        "title": "Corticocancellous"
                                      },
                                      {
                                        "value": "Demineralized Cortical",
                                        "title": "Demineralized Cortical"
                                      },
                                      {
                                        "value": "Mineralized/Demineralized Cortical",
                                        "title": "Mineralized/Demineralized Cortical"
                                      },
                                      {
                                        "value": "Mineralized Cancellous",
                                        "title": "Mineralized Cancellous"
                                      },
                                      {
                                        "value": "Mineralized Cortical",
                                        "title": "Mineralized Cortical"
                                      },
                                      {
                                        "value": "Demineralized bone matrix (DBM) putty",
                                        "title": "Demineralized bone matrix (DBM) putty"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "allograftSize",
                                    "type": "Select",
                                    "title": "Allograft Size",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25-1.0mm",
                                        "title": "0.25-1.0mm"
                                      },
                                      {
                                        "value": "0.5-1.0mm",
                                        "title": "0.5-1.0mm"
                                      },
                                      {
                                        "value": "0.125-0.850mm",
                                        "title": "0.125-0.850mm"
                                      },
                                      {
                                        "value": "1.0-2.0mm",
                                        "title": "1.0-2.0mm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "allograftVolume",
                                    "type": "Select",
                                    "title": "Allograft Volume",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25cc",
                                        "title": "0.25cc"
                                      },
                                      {
                                        "value": "0.5cc",
                                        "title": "0.5cc"
                                      },
                                      {
                                        "value": "0.75cc",
                                        "title": "0.75cc"
                                      },
                                      {
                                        "value": "1.0cc",
                                        "title": "1.0cc"
                                      },
                                      {
                                        "value": "1.25cc",
                                        "title": "1.25cc"
                                      },
                                      {
                                        "value": "1.5cc",
                                        "title": "1.5cc"
                                      },
                                      {
                                        "value": "1.75cc",
                                        "title": "1.75cc"
                                      },
                                      {
                                        "value": "2.0cc",
                                        "title": "2.0cc"
                                      },
                                      {
                                        "value": "2.25cc",
                                        "title": "2.25cc"
                                      },
                                      {
                                        "value": "2.5cc",
                                        "title": "2.5cc"
                                      },
                                      {
                                        "value": "2.75cc",
                                        "title": "2.75cc"
                                      },
                                      {
                                        "value": "3.0cc",
                                        "title": "3.0cc"
                                      },
                                      {
                                        "value": "3.25cc",
                                        "title": "3.25cc"
                                      },
                                      {
                                        "value": "3.5cc",
                                        "title": "3.5cc"
                                      },
                                      {
                                        "value": "3.75cc",
                                        "title": "3.75cc"
                                      },
                                      {
                                        "value": "4.0cc",
                                        "title": "4.0cc"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "allograftBrand",
                                    "type": "Input",
                                    "title": "Allograft Brand",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "xenograftMaterialUsed",
                                    "type": "Select",
                                    "title": "Xenograft Type",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Bovine",
                                        "title": "Bovine"
                                      },
                                      {
                                        "value": "Porcine",
                                        "title": "Porcine"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "xenograftSize",
                                    "type": "Select",
                                    "title": "Xenograft Size",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25-1.0mm",
                                        "title": "0.25-1.0mm"
                                      },
                                      {
                                        "value": "0.5-1.0mm",
                                        "title": "0.5-1.0mm"
                                      },
                                      {
                                        "value": "0.125-0.850mm",
                                        "title": "0.125-0.850mm"
                                      },
                                      {
                                        "value": "1.0-2.0mm",
                                        "title": "1.0-2.0mm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "xenograftVolume",
                                    "type": "Select",
                                    "title": "Xenograft Volume",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25cc",
                                        "title": "0.25cc"
                                      },
                                      {
                                        "value": "0.5cc",
                                        "title": "0.5cc"
                                      },
                                      {
                                        "value": "0.75cc",
                                        "title": "0.75cc"
                                      },
                                      {
                                        "value": "1.0cc",
                                        "title": "1.0cc"
                                      },
                                      {
                                        "value": "1.25cc",
                                        "title": "1.25cc"
                                      },
                                      {
                                        "value": "1.5cc",
                                        "title": "1.5cc"
                                      },
                                      {
                                        "value": "1.75cc",
                                        "title": "1.75cc"
                                      },
                                      {
                                        "value": "2.0cc",
                                        "title": "2.0cc"
                                      },
                                      {
                                        "value": "2.25cc",
                                        "title": "2.25cc"
                                      },
                                      {
                                        "value": "2.5cc",
                                        "title": "2.5cc"
                                      },
                                      {
                                        "value": "2.75cc",
                                        "title": "2.75cc"
                                      },
                                      {
                                        "value": "3.0cc",
                                        "title": "3.0cc"
                                      },
                                      {
                                        "value": "3.25cc",
                                        "title": "3.25cc"
                                      },
                                      {
                                        "value": "3.5cc",
                                        "title": "3.5cc"
                                      },
                                      {
                                        "value": "3.75cc",
                                        "title": "3.75cc"
                                      },
                                      {
                                        "value": "4.0cc",
                                        "title": "4.0cc"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "xenograftBrand",
                                    "type": "Input",
                                    "title": "Xenograft Brand",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "alloplastMaterialUsed",
                                    "type": "Select",
                                    "title": "Alloplast Type",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Hydroxyapatite (HA)",
                                        "title": "Hydroxyapatite (HA)"
                                      },
                                      {
                                        "value": "Tricalcium phosphate (TCP)",
                                        "title": "Tricalcium phosphate (TCP)"
                                      },
                                      {
                                        "value": "Biphasic calcium phosphate (BCP)",
                                        "title": "Biphasic calcium phosphate (BCP)"
                                      },
                                      {
                                        "value": "Calcium sulfate (CS)",
                                        "title": "Calcium sulfate (CS)"
                                      },
                                      {
                                        "value": "Calcium phosphate (CP)",
                                        "title": "Calcium phosphate (CP)"
                                      },
                                      {
                                        "value": "Bioglass (BG)",
                                        "title": "Bioglass (BG)"
                                      },
                                      {
                                        "value": "Carbonate apatite (CA)",
                                        "title": "Carbonate apatite (CA)"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "alloplastSize",
                                    "type": "Select",
                                    "title": "Alloplast Size",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25-1.0mm",
                                        "title": "0.25-1.0mm"
                                      },
                                      {
                                        "value": "0.5-1.0mm",
                                        "title": "0.5-1.0mm"
                                      },
                                      {
                                        "value": "0.125-0.850mm",
                                        "title": "0.125-0.850mm"
                                      },
                                      {
                                        "value": "1.0-2.0mm",
                                        "title": "1.0-2.0mm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "alloplastVolume",
                                    "type": "Select",
                                    "title": "Alloplast Volume",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25cc",
                                        "title": "0.25cc"
                                      },
                                      {
                                        "value": "0.5cc",
                                        "title": "0.5cc"
                                      },
                                      {
                                        "value": "0.75cc",
                                        "title": "0.75cc"
                                      },
                                      {
                                        "value": "1.0cc",
                                        "title": "1.0cc"
                                      },
                                      {
                                        "value": "1.25cc",
                                        "title": "1.25cc"
                                      },
                                      {
                                        "value": "1.5cc",
                                        "title": "1.5cc"
                                      },
                                      {
                                        "value": "1.75cc",
                                        "title": "1.75cc"
                                      },
                                      {
                                        "value": "2.0cc",
                                        "title": "2.0cc"
                                      },
                                      {
                                        "value": "2.25cc",
                                        "title": "2.25cc"
                                      },
                                      {
                                        "value": "2.5cc",
                                        "title": "2.5cc"
                                      },
                                      {
                                        "value": "2.75cc",
                                        "title": "2.75cc"
                                      },
                                      {
                                        "value": "3.0cc",
                                        "title": "3.0cc"
                                      },
                                      {
                                        "value": "3.25cc",
                                        "title": "3.25cc"
                                      },
                                      {
                                        "value": "3.5cc",
                                        "title": "3.5cc"
                                      },
                                      {
                                        "value": "3.75cc",
                                        "title": "3.75cc"
                                      },
                                      {
                                        "value": "4.0cc",
                                        "title": "4.0cc"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "alloplastBrand",
                                    "type": "Input",
                                    "title": "Alloplast Brand",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "autograftMaterialUsed",
                                    "type": "Select",
                                    "title": "Autograft Type",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Chin",
                                        "title": "Chin"
                                      },
                                      {
                                        "value": "Ramus",
                                        "title": "Ramus"
                                      },
                                      {
                                        "value": "Tuberosity",
                                        "title": "Tuberosity"
                                      },
                                      {
                                        "value": "Alveolar bone",
                                        "title": "Alveolar bone"
                                      },
                                      {
                                        "value": "Osteotomy bone",
                                        "title": "Osteotomy bone"
                                      },
                                      {
                                        "value": "Hip",
                                        "title": "Hip"
                                      },
                                      {
                                        "value": "Calvarium",
                                        "title": "Calvarium"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "autograftSize",
                                    "type": "Select",
                                    "title": "Autograft Size",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25-1.0mm",
                                        "title": "0.25-1.0mm"
                                      },
                                      {
                                        "value": "0.5-1.0mm",
                                        "title": "0.5-1.0mm"
                                      },
                                      {
                                        "value": "0.125-0.850mm",
                                        "title": "0.125-0.850mm"
                                      },
                                      {
                                        "value": "1.0-2.0mm",
                                        "title": "1.0-2.0mm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "autograftVolume",
                                    "type": "Select",
                                    "title": "Autograft Volume",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25cc",
                                        "title": "0.25cc"
                                      },
                                      {
                                        "value": "0.5cc",
                                        "title": "0.5cc"
                                      },
                                      {
                                        "value": "0.75cc",
                                        "title": "0.75cc"
                                      },
                                      {
                                        "value": "1.0cc",
                                        "title": "1.0cc"
                                      },
                                      {
                                        "value": "1.25cc",
                                        "title": "1.25cc"
                                      },
                                      {
                                        "value": "1.5cc",
                                        "title": "1.5cc"
                                      },
                                      {
                                        "value": "1.75cc",
                                        "title": "1.75cc"
                                      },
                                      {
                                        "value": "2.0cc",
                                        "title": "2.0cc"
                                      },
                                      {
                                        "value": "2.25cc",
                                        "title": "2.25cc"
                                      },
                                      {
                                        "value": "2.5cc",
                                        "title": "2.5cc"
                                      },
                                      {
                                        "value": "2.75cc",
                                        "title": "2.75cc"
                                      },
                                      {
                                        "value": "3.0cc",
                                        "title": "3.0cc"
                                      },
                                      {
                                        "value": "3.25cc",
                                        "title": "3.25cc"
                                      },
                                      {
                                        "value": "3.5cc",
                                        "title": "3.5cc"
                                      },
                                      {
                                        "value": "3.75cc",
                                        "title": "3.75cc"
                                      },
                                      {
                                        "value": "4.0cc",
                                        "title": "4.0cc"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "membraneUsed",
                                    "type": "Select",
                                    "title": "Membrane Type",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "Bovine",
                                        "title": "Bovine"
                                      },
                                      {
                                        "value": "Porcine",
                                        "title": "Porcine"
                                      },
                                      {
                                        "value": "Allogenic",
                                        "title": "Allogenic"
                                      },
                                      {
                                        "value": "Pericardium",
                                        "title": "Pericardium"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "membraneSize",
                                    "type": "Select",
                                    "title": "Membrane Size",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "10 X 10 mm",
                                        "title": "10 X 10 mm"
                                      },
                                      {
                                        "value": "10 X 15 mm",
                                        "title": "10 X 15 mm"
                                      },
                                      {
                                        "value": "13 X 25 mm",
                                        "title": "13 X 25 mm"
                                      },
                                      {
                                        "value": "25 X 25 mm",
                                        "title": "25 X 25 mm"
                                      },
                                      {
                                        "value": "30 X 40 mm",
                                        "title": "30 X 40 mm"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "membraneBrand",
                                    "type": "Input",
                                    "title": "Membrane Brand",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "additives",
                                    "type": "Select",
                                    "title": "Additives",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "PRP",
                                        "title": "PRP"
                                      },
                                      {
                                        "value": "PRF",
                                        "title": "PRF"
                                      },
                                      {
                                        "value": "Gem 21S",
                                        "title": "Gem 21S"
                                      },
                                      {
                                        "value": "Infuse",
                                        "title": "Infuse"
                                      },
                                      {
                                        "value": "Stem cells",
                                        "title": "Stem cells"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "additivesVolume",
                                    "type": "Select",
                                    "title": "Additives Volume",
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "value": "0.25cc",
                                        "title": "0.25cc"
                                      },
                                      {
                                        "value": "0.5cc",
                                        "title": "0.5cc"
                                      },
                                      {
                                        "value": "0.75cc",
                                        "title": "0.75cc"
                                      },
                                      {
                                        "value": "1.0cc",
                                        "title": "1.0cc"
                                      },
                                      {
                                        "value": "1.25cc",
                                        "title": "1.25cc"
                                      },
                                      {
                                        "value": "1.5cc",
                                        "title": "1.5cc"
                                      },
                                      {
                                        "value": "1.75cc",
                                        "title": "1.75cc"
                                      },
                                      {
                                        "value": "2.0cc",
                                        "title": "2.0cc"
                                      },
                                      {
                                        "value": "2.25cc",
                                        "title": "2.25cc"
                                      },
                                      {
                                        "value": "2.5cc",
                                        "title": "2.5cc"
                                      },
                                      {
                                        "value": "2.75cc",
                                        "title": "2.75cc"
                                      },
                                      {
                                        "value": "3.0cc",
                                        "title": "3.0cc"
                                      },
                                      {
                                        "value": "3.25cc",
                                        "title": "3.25cc"
                                      },
                                      {
                                        "value": "3.5cc",
                                        "title": "3.5cc"
                                      },
                                      {
                                        "value": "3.75cc",
                                        "title": "3.75cc"
                                      },
                                      {
                                        "value": "4.0cc",
                                        "title": "4.0cc"
                                      },
                                    ]
                                  },
                                  {
                                    "name": "additivesBrand",
                                    "type": "Input",
                                    "title": "Additives Brand",
                                    "editRecord": true
                                  },
                                  {
                                    "name": "rrpsd",
                                    "type": "Date",
                                    "title": "RRPSD",
                                    "width": 110,
                                    "attributes": {
                                      "info": {
                                        "text": "Recommended Restoration Phase Start Date"
                                      },
                                    },
                                    "editRecord": true,
                                  },
                                  {
                                    "name": "finalPA",
                                    "type": "Images",
                                    "title": "Final PA",
                                    "imageTitle": true,
                                    "editRecord": true,
                                    "attributes": {
                                      "singleImage": true
                                    },
                                  },
                                  {
                                    "name": "action",
                                    "type": "Button",
                                    "title": "Action",
                                    "width": 150,
                                    "editRecord": false,
                                    "action": true
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Pre, During, Post Surgical Photos / Videos</span>',
                                "fields": [
                                  {
                                    "name": "surgeryPhoto",
                                    "type": "Images",
                                    "imageLabel": true,
                                    "title": ""
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Temporary and Definitive Prostheses Photos / Videos</span>',
                                "fields": [
                                  {
                                    "name": "prosthesesPhoto",
                                    "type": "Images",
                                    "imageLabel": true,
                                    "title": ""
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Products Used</span>',
                                "fields": [
                                  {
                                    "name": "lapsPhoto",
                                    "type": "Images",
                                    "title": ""
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Suturing Materials <span style= "color: gray; font-size: 16px;">(Choose one or more)</span></span>',
                                "fields": [
                                  {
                                    "name": "suturingMaterials",
                                    "type": "MultiSelect",
                                    "fullWidth": true,
                                    "removePadding": true,
                                    "title": "",
                                    "options": [
                                      "Silk",
                                      "Plain Gut",
                                      "Chromic Gut",
                                      "PTFE",
                                      "Nylon",
                                      "PGA",
                                      "Polypropylene",
                                      "Polyviolene",
                                      "Flapless"
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Suturing Techniques <span style= "color: gray; font-size: 16px;">(Choose one or more)</span></span>',
                                "fields": [
                                  {
                                    "name": "suturingTechniques",
                                    "type": "MultiSelect",
                                    "fullWidth": true,
                                    "removePadding": true,
                                    "title": "",
                                    "options": [
                                      "Single Interrupted",
                                      "Figure 8",
                                      "Sling Suture",
                                      "Horizontal Mattress",
                                      "Vertical Mattress",
                                      "Continuous Unlocking",
                                      "Continuous Locking",
                                      "Continuous Double Lock",
                                      "Continuous Horizontal Mattress"
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Did the patient tolerate the procedure well?</span>',
                                "fields": [
                                  {
                                    "name": "patientTolerate",
                                    "type": "Select",
                                    "title": "",
                                    "showDropdown": true,
                                    "editRecord": true,
                                    "options": [
                                      {
                                        "name": "yes",
                                        "title": "Yes"
                                      },
                                      {
                                        "name": "No",
                                        "title": "No"
                                      }
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Reason For Next Appointment <span style= "color: gray; font-size: 16px;">(Choose one or more)</span></span>',
                                "fields": [
                                  {
                                    "name": "reasonForNextAppointment",
                                    "type": "MultiSelect",
                                    "title": "",
                                    "fullWidth": true,
                                    "showDropdown": true,
                                    "removePadding": true,
                                    "editRecord": true,
                                    "options": [
                                      "Continue Treatment",
                                      "Followup",
                                      "Suture Removal",
                                      "Hygiene Visit",
                                      "Recall",
                                      "X-ray",
                                      "CT Scan",
                                    ]
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "impression",
                                    "type": "Checkbox",
                                    "title": "Impressions are Completed",
                                    "label": "Impressions",
                                    "labelStyle": true,
                                    "collapse": true,
                                    "info": {
                                      "text": "In the impression visit we have the following scenarios:<br /><br />\n1. Patient presents a difficult scenario for proper impression making (ex: when you cannot get an impression of the full width and depth of the sulcus). In this case, we will only order (a) custom tray(s).<br />\n<br />\n2. The patient has enough teeth to predictably hand mount the casts in the articulator. In this case, ordering bite blocks is unnecessary. Provide the lab with only the impressions and order study/working models, diagnostic casts and immediate dentures. (For immediate dentures, however, the lab will need a completed Novadontics facial and smile analysis form).<br />\n<br />\n3. The patient has enough teeth to mount the casts in the articulator, but the lab might still need some help just to be sure (number of teeth is adequate but there might be doubts regarding the stability and the reproducibility of the bite in the lab). In this case, there is no need to order bite blocks. Provide the lab with impressions and bite registrations (using Flexitime® Bite material), and order only study/working models, diagnostic casts and immediate dentures. (In regards to immediate dentures, the lab will require a completed Novadontics facial and smile analysis form). Use the bite registration material only in open areas, where no tooth contacts exist.<br />\n<br />\n4. Patient does not have enough teeth to mount the casts in the articulator. In this case, order study/working models, diagnostics casts and bite blocks.<br />\n<br />\nImpression Techniques:<br />\n<br />\nIn the maxilla: Overextend the impression to include the hamular notches and extend to the full depth and width of the sulcus of the mucogingival fold and posteriorly to the area beyond the junction of the hard and soft palate.<br />\n\nIn the mandible: Overextend the impression to include the full depth and width of the sulcus, the retromolar pad (bi-laterally) and the buccal shelf, and fully extend down into the retro-mylohyoid area.<br />\n<br />\nImpression Materials:<br />\n<br />\nFor impressions: Use silicon material (e.g., Elite&reg; by Zhermack)<br />\n\nFor bite registrations: Use rigid silicon 85+ shore hardness (e.g., Flexitime&reg; Bite)<br />\n",
                                      "type": "InfoBlock",
                                      "title": "Scenarios"
                                    },
                                    "attributes": {
                                      "note": false,
                                      "images": false
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "brCompleted",
                                    "type": "Checkbox",
                                    "title": "Bite Registration Completed",
                                    "label": "Bite Registration",
                                    "labelStyle": true,
                                    "collapse": true,
                                    "info": {
                                      "image": "https://s3.amazonaws.com/static.novadonticsllc.com/PDF/2E.pdf"
                                    },
                                    "attributes": {
                                      "note": false,
                                      "images": false
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #91288F;">Occlusion Type</span>',
                                "collapse": true,
                                "fields": [
                                  {
                                    "name": "balancedOcclusion",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Balanced Occlusion",
                                    "attributes": {
                                      "info": {
                                        "text": "Balanced occlusion should be the occlusal scheme of choice in full-arch immediate loading when none of the arches are loaded or when only the lower is immediately loaded vs opposing removable denture or only the upper is loaded vs opposing removable denture. Balanced occlusion can be defined as simultaneous occlusal contacts in centric occlusion and eccentric articulation. Balanced occlusion should be used when either the maxillary or mandibular prosthesis is a complete removable denture. This type of occlusion helps stabilize a complete denture. During eccentric movements, the balancing contacts will help with denture stability. The same balancing contacts used for fixed restorations has been shown to cause ceramic fractures. Balancing contacts are also called non-working side(NWS) interferences. In fixed implant restorations they can be very destructive as a result of the class 1 lever system created (masseter is the effort, the non-working or balancing side is the fulcrum, and the working canine is the load). Balancing or NWS contacts can be very destructive when both arches have fixed restorations.",
                                        "image": "https://s3.amazonaws.com/static.novadonticsllc.com/img/3c.jpg"
                                      },
                                      "note": false,
                                      "images": false
                                    }
                                  },
                                  {
                                    "name": "anteriorGuidance",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Anterior Guidance",
                                    "attributes": {
                                      "info": {
                                        "text": "Anterior contact/guidance occlusion would be the occlusal scheme of choice when both arches are immediately loaded. A simple anterior tooth arrangement should be aimed for. All anterior teeth should have simultaneous contacts during maximum intercuspation (MIP) or centric occlusion (CO). The anterior teeth should have a 1-mm vertical overlap and a 1-mm horizontal overlap. This arrangement translates to a shallow (reduced force) anterior guidance. The posterior teeth should be devoid of contacts during the osseointegration phase. Posterior implants will therefore have lesser loads during this critical phase."
                                      },
                                      "note": false,
                                      "images": false
                                    }
                                  },
                                  {
                                    "name": "canineGuidance",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Canine Guidance",
                                    "attributes": {
                                      "info": {
                                        "text": "This would be the choice for the permanent prosthesis. It eliminates posterior eccentric contacts in the majority of situations, hence simplifying occlusal management of full-mouth reconstructions. Canine guidance must be shallow. This reduces forces on anterior teeth during protrusive movements. If the maxillary definitive fixed prosthesis is acrylic resin based, the anterior occlusal scheme should be designed in the following way: there should be a 1-mm vertical incisal overlap with a 1-mm horizontal open bite only. This will reduce the incidence of maxillary anterior tooth fractures."
                                      },
                                      "note": false,
                                      "images": false
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #91288F;">Pickup and/or Delivery</span>',
                                "collapsePD": true,
                                "fields": [
                                  {
                                    "name": "fullDenture",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Delivery of removable full denture"
                                  },
                                  {
                                    "name": "existingDenture",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Screw-retained converted dentures (direct method) "
                                  },
                                  {
                                    "name": "pmmaProsthesis",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Screw-retained CAD/CAM PMMA prosthesis (indirect method)"
                                  },
                                  {
                                    "name": "compositeProsthesis",
                                    "type": "Checkbox",
                                    "labelStyle": true,
                                    "title": "Screw-retained CAD/CAM composite prosthesis (indirect method)"
                                  },
                                  {
                                    "text": "",
                                    "type": "InfoBlock"
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "noteStyle": true,
                                "fields": [
                                  {
                                    "name": "verbalPostopIns",
                                    "type": "Checkbox",
                                    "title": "Verbal Postop Instructions are given",
                                  },
                                  {
                                    "name": "writtenPostopIns",
                                    "type": "Checkbox",
                                    "title": "Written Postop Instructions are given",
                                  },
                                  {
                                    "name": "rxVisit",
                                    "type": "Checkbox",
                                    "title": "Rx is given on this visit",
                                    "presicon": true,
                                  }
                                ]
                              }
                            }
                          ]
                        },
                      ],
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                              {
                                "group": {
                                  "title": "",
                                  "fields": []
                                }
                              }
                            ]
                        },
                        {
                          "columnItems": [
                              {
                                "group": {
                                  "title": "",
                                  "fields": []
                                }
                              }
                            ]
                        },
                        {
                          "columnItems": [
                              {
                                "field": {
                                  "name": "preMadeNotes",
                                  "type": "Select",
                                  "title": 'Pre-Made Notes',
                                  "showDropdown": true,
                                  "selectOption": true,
                                  "onChange": true,
                                  "changeStyle": true,
                                  "options": 'preMadeClinicalNotesOptions',
                                }
                              }
                            ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "note",
                                "type": "Textarea",
                                "title": 'Notes',
                                "onChange": true,
                                "mode": "inline",
                                "changeStyle": true,
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": '<span style= "color: #EA6225;">Voice Memo</span>',
                                "fields": [
                                  {
                                    "name": "voiceMemo",
                                    "type": "Audios",
                                    "memoLabel": true,
                                    "title": ""
                                  }
                                ]
                              }
                            }
                          ]
                        },
                      ]
                    },
                  ]
                }
              ]
            }
          ]
        }
      ]
    }

  }

  return {
    orders,
    patients,
    products,
    accounts,
    practices,
    categories,
    ciicategories,
    documents,
    ciicourses,
    courses,
    patientCourses,
    vendors,
    shipping,
    consents,
    prescriptions,
    educationRequests,
    module,
    procedure,
    productCategories,
    requestConsultation,
    insurance,
    clinicalNotes
  };
}
