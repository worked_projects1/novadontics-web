// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import simpleLazyLoadedRouteProvider from 'utils/routing/simpleLazyLoadedRoute';
import awaitState from 'utils/routing/awaitState';

import sessionSagas from 'blocks/session/sagas';
import formSchemaSagas from 'blocks/forms/sagas';
import { dentistOrderSagas } from 'blocks/dentistOrders';
import settingsSagas from 'blocks/emailMappings/sagas';
import { selectLoggedIn, selectUser } from 'blocks/session/selectors';

import administratorRoutesProvider from './administrator';
import dentistRoutesProvider from './dentist';
import staffRoutesProvider from './staff';
import anonymousRoutesProvider from './anonymous';
import {accountValiditySaga} from "../blocks/accountValidity";
import { clinicalNotesSaga } from "../blocks/patients/clinicalNotes";
import { perioChartSaga } from "../blocks/patients/periochart";
import { endoChartSaga } from "../blocks/patients/endochart";
import { toothChartSaga } from "../blocks/patients/toothchart";
import { ctScanSaga } from "../blocks/patients/ctscan";
import { contractsSaga } from "../blocks/patients/contracts";
import { xraySaga } from "../blocks/patients/xray";
import { scratchPadSaga } from "../blocks/patients/scratchpad";
import { measurementHistorySaga } from "../blocks/patients/measurementHistory";
import { prescriptionHistorySaga } from "../blocks/prescriptions/prescriptionHistory";
import { treatmentPlanSaga } from "../blocks/patients/treatmentPlan";
import { treatmentPlanDetailsSaga } from "../blocks/patients/treatmentPlanDetails";
import { appointmentSaga } from "../blocks/patients/appointment";
import { providerFilterSaga } from "../module/utils/sagas";
import { stateTaxSaga } from "blocks/vendors/stateTax";
import { saleTaxSaga } from "blocks/vendors/saleTax";
import { reportsSaga } from "blocks/reports";
import { ledgerSaga } from "blocks/ledger";
import { sharedPatientSaga } from 'blocks/patients/sharedPatient';
import { staffNotesSaga } from 'blocks/patients/staffNotes';
import { prescriptionSaga } from 'blocks/patients/prescriptions';
import { familyMembersSaga } from 'blocks/patients/familyMembers';
import { subscriberSaga } from 'blocks/patients/subscriber';

function loggedInExists(state) {
  const loggedIn = selectLoggedIn()(state);
  return loggedIn === true || loggedIn === false;
}

export default function routes(store) {
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  injectSagas(sessionSagas);
  injectSagas(formSchemaSagas);
  injectSagas(settingsSagas);
  injectSagas(dentistOrderSagas);
  injectSagas(accountValiditySaga);
  injectSagas(clinicalNotesSaga);
  injectSagas(perioChartSaga);
  injectSagas(endoChartSaga);
  injectSagas(toothChartSaga);
  injectSagas(ctScanSaga);
  injectSagas(contractsSaga);
  injectSagas(xraySaga);
  injectSagas(scratchPadSaga);
  injectSagas(measurementHistorySaga);
  injectSagas(prescriptionHistorySaga);
  injectSagas(treatmentPlanSaga);
  injectSagas(treatmentPlanDetailsSaga);
  injectSagas(appointmentSaga);
  injectSagas(providerFilterSaga);
  injectSagas(stateTaxSaga);
  injectSagas(saleTaxSaga);
  injectSagas(reportsSaga);
  injectSagas(ledgerSaga);
  injectSagas(sharedPatientSaga);
  injectSagas(staffNotesSaga);
  injectSagas(prescriptionSaga);
  injectSagas(familyMembersSaga);
  injectSagas(subscriberSaga);

  const simpleLazyLoadedRoute = simpleLazyLoadedRouteProvider(injectReducer, injectSagas, store);

  const notFoundRoute = simpleLazyLoadedRoute({
    path: '*',
    name: 'notfound',
    container: 'NotFoundPage',
  });

  const redirectRoute = simpleLazyLoadedRoute({
    path: 'external',
    name: 'external',
    container: 'Redirect',
  });

  const routeSwitcher = (user, loggedIn) => {
    if(!loggedIn) {
      return anonymousRoutesProvider(simpleLazyLoadedRoute);
    }
    switch (user.role) {
      case 'admin':
        return administratorRoutesProvider(simpleLazyLoadedRoute);
      case 'dentist':
        return dentistRoutesProvider(simpleLazyLoadedRoute);
      case 'receptionist':
        return dentistRoutesProvider(simpleLazyLoadedRoute);
      case 'novadonticsstaff':
        return staffRoutesProvider(simpleLazyLoadedRoute);
      default:
        return anonymousRoutesProvider(simpleLazyLoadedRoute);
    }
  };

  return {
    path: process.env.PUBLIC_PATH || '/',

    getIndexRoute(location, cb) {
      awaitState(store, loggedInExists, (state) => {
        const loggedIn = selectLoggedIn()(state);
        const user = selectUser()(state);  
        cb(null, routeSwitcher(user, loggedIn).index);
      }, 1000);
    },

    getChildRoutes(location, cb) {
      awaitState(store, loggedInExists, (state) => {
        const loggedIn = selectLoggedIn()(state);
        const user = selectUser()(state);
        cb(null, {
          childRoutes: [
            routeSwitcher(user, loggedIn).routes || [],
            redirectRoute,
            notFoundRoute,
          ],
        });
      }, 2000);
    },
  };
}
