import React from 'react';
import App from 'containers/App';
import schema from './schema';
import schema_1 from './schema_1';

const patientsColumns = schema().patients().columns;
const documentsColumn = schema().documents().columns;
const coursesColumn = schema().courses().columns;
const patientCoursesColumn = schema().patientCourses().columns;
const ordersColumns = schema().orders().columns;
const ordersProducts = schema().orders().products;
const productsColumns = schema().products().columns;
const categoriesColumns = schema().categories().columns;
const productsPaymentColumns = schema().orders().productPayment;
const vendorPaymentColumns = schema().orders().vendorPayment;

const educationColumns = schema_1().dentistEducationRequests().columns;

export default function (simpleLazyLoadedRoute) {
  const pages = [
    simpleLazyLoadedRoute({
      path: 'documents',
      name: 'documents',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/documents`,
        title: 'Documents',
        icon: 'docs',
      },
      require: ['RecordsPage', 'documents'],
      container: function RecordsPage(documents) {
        return this('documents', `${process.env.PUBLIC_PATH || ''}/documents`, false, documentsColumn, documents.actions, documents.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'documents.view',
          require: ['ViewCoursesPage', 'documents'],
          container: function ViewDocumentsPage(documents) {
            return this(`${process.env.PUBLIC_PATH || ''}/documents`, documentsColumn, false, documents.actions.deleteRecord, documents.selectors.selectRecord, documents.selectors.selectUpdateError);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'patient-courses',
      name: 'patientCourses',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/patient-courses`,
        title: 'Patient Ed',
        icon: 'picture',
        categoriesButton: {
          path: '/resource-categories/courses'
        }
      },
      require: ['RecordsPage', 'patientCourses'],
      container: function RecordsPage(courses) {
        return this('Patient Education', `${process.env.PUBLIC_PATH || ''}/patient-courses`, false, patientCoursesColumn, courses.actions, courses.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'patient-courses.view',
          require: ['ViewCoursesPage', 'patientCourses'],
          container: function ViewCoursesPage(courses) {
            return this(`${process.env.PUBLIC_PATH || ''}/patient-courses`, patientCoursesColumn, false, courses.actions.deleteRecord, courses.selectors.selectRecord, courses.selectors.selectUpdateError);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'educationRequests',
      name: 'educationRequests',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/educationRequests`,
        title: 'CE Requests',
        icon: 'inbox',
      },
      require: ['RecordsPage', 'educationRequests'],
      container: function RecordsPage(education) {
        return this('Continuing Education Requests', `${process.env.PUBLIC_PATH || ''}/educationRequests`, false, educationColumns, education.actions, education.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'educationRequests.view',
          require: ['ViewRecordPage', 'educationRequests'],
          container: function ViewRecordPage(education) {
            return this(`${process.env.PUBLIC_PATH || ''}/educationRequests`, educationColumns, education.actions.deleteRecord, education.selectors.selectRecord, undefined, false);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'orders',
      name: 'orders',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/orders`,
        title: 'Orders',
        icon: 'truck',
      },
      require: ['RecordsPage', 'orders'],
      container: function RecordsPage(orders) {
        return this('orders', `${process.env.PUBLIC_PATH || ''}/orders`, false, ordersColumns, orders.actions, orders.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'orders.view',
          require: ['ViewOrderPage', 'orders'],
          container: function ViewOrderPage(orders) {
            return this(`${process.env.PUBLIC_PATH || ''}/orders`, ordersColumns, productsPaymentColumns, vendorPaymentColumns, ordersProducts, orders.actions.updateRecord, orders.selectors.selectRecord, false);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'catalog',
      name: 'products.list',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/catalog`,
        title: 'Catalog',
        icon: 'clipboard',
      },
      require: ['RecordsPage', 'products'],
      container: function RecordsPage(products) {
        return this('products', `${process.env.PUBLIC_PATH || ''}/catalog`, true, productsColumns, products.actions, products.selectors);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'products.create',
          require: ['CreateRecordPage', 'products'],
          container: function CreateRecordPage(products) {
            return this('products.create', `${process.env.PUBLIC_PATH || ''}/catalog`, productsColumns, products.selectors.selectRecord, products.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id',
          name: 'products.view',
          require: ['ViewCatalogPage', 'products'],
          container: function ViewCatalogPage(products) {
            return this(`${process.env.PUBLIC_PATH || ''}/catalog`, products.actions.deleteRecord, products.selectors.selectRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'products.edit',
          require: ['EditRecordPage', 'products'],
          container: function EditRecordPage(products) {
            const view = true;
            return this('products.edit', `${process.env.PUBLIC_PATH || ''}/catalog`, productsColumns, products.actions.updateRecord, products.selectors.selectRecord, products.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'categories',
      name: 'categories',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/categories`,
        title: 'Categories',
        icon: 'tag',
      },
      require: ['CategoriesPage', 'categories'],
      container: function RecordsPage(categories) {
        return this('categories', `${process.env.PUBLIC_PATH || ''}/categories`, true, categoriesColumns, categories.actions, categories.selectors, categories.actions.deleteRecord, categories.actions.updateRecords);
      },
      childRoutes: [
        simpleLazyLoadedRoute({
          path: 'create',
          name: 'categories.create',
          require: ['CreateRecordPage', 'categories'],
          container: function CreateRecordPage(categories) {
            return this('categories.create', `${process.env.PUBLIC_PATH || ''}/categories`, categoriesColumns, categories.selectors.selectRecord, categories.actions.createRecord);
          },
        }),
        simpleLazyLoadedRoute({
          path: ':id/edit',
          name: 'categories.edit',
          require: ['EditRecordPage', 'categories'],
          container: function EditRecordPage(categories) {
            const view = false;
            return this('categories.edit', `${process.env.PUBLIC_PATH || ''}/categories/`, categoriesColumns, categories.actions.updateRecord, categories.selectors.selectRecord, categories.selectors.selectUpdateError, view);
          },
        }),
      ],
    }),
    simpleLazyLoadedRoute({
      path: 'help',
      name: 'Software Manual',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/help`,
        title: 'Software Manual',
        icon: 'help',
      },
      require: ['HelpPage'],
      container: function HelpPage() {
        return this('help', `${process.env.PUBLIC_PATH || ''}/help`, 'https://s3.amazonaws.com/static.novadonticsllc.com/Manuals/Dentist/Novadontics-Dentist-Manual.pdf');
      },
    }),
    simpleLazyLoadedRoute({
      path: 'tutorials',
      name: 'Tutorials',
      data: {
        path: `${process.env.PUBLIC_PATH || ''}/tutorials`,
        title: 'Tutorials',
        icon: 'flash',
      },
      require: ['HelpPage'],
      container: function HelpPage() {
        return this('tutorials', `${process.env.PUBLIC_PATH || ''}/help`, 'https://s3.amazonaws.com/static.novadonticsllc.com/Manuals/Videos/index.html');
      },
    }),
  ];

  return {
    index: simpleLazyLoadedRoute({
      require: ['RecordsPage', 'patients'],
      container: function RecordsPage(patients) {
        const Template = App(pages); // eslint-disable-line new-cap
        const Page = this('patients', `${process.env.PUBLIC_PATH || ''}/patients`, false, patientsColumns, patients.actions, patients.selectors);
        const Component = () => <Template><Page /></Template>;

        Component.await = Page.await;

        return Component;
      },
    }),
    routes: {
      component: App(pages), // eslint-disable-line new-cap
      childRoutes: pages,
    },
  };
}
