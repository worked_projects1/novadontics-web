import { archiveVendor, unarchiveVendor } from '../blocks/vendors/actions';
import React from 'react';
import moment from 'moment-timezone';
import { connect } from "react-redux";
import { createSelector } from "reselect";
import AccountExtendButton, { expirationNoticeText } from "../components/AccountExpiry";
import { currencyFormatter } from 'utils/currencyFormatter.js';

export default function schema_1() {

  function implantLog() {
    return {
      columns: [
        {
          id: 1,
          value: 'date',
          label: 'Date',
          implantRedColor: true,
          asteriskColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 110,
          sortLabel: "Date⇅",
          sort: true,
          sortable: true,
          dateSort: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'patientName',
          label: 'Patient Name',
          implantRedColor: true,
          asteriskColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          sortLabel: "Patient Name⇅",
          sort: true,
          sortable: true,
          width: 130,
          textSort: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'dob',
          label: 'DOB',
          implantRedColor: true,
          paddingTop: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 110,
          type: 'date',
        },
        {
          id: 4,
          value: 'providerName',
          label: 'Provider',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 130,
          sortLabel: "Provider⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'implantSite',
          label: 'Implant Site #',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "1 / 18",
              label: "1 / 18"
            },
            {
              value: "2 / 17",
              label: "2 / 17"
            },
            {
              value: "3 / 16",
              label: "3 / 16"
            },
            {
              value: "4 / 15",
              label: "4 / 15"
            },
            {
              value: "5 / 14",
              label: "5 / 14"
            },
            {
              value: "6 / 13",
              label: "6 / 13"
            },
            {
              value: "7 / 12",
              label: "7 / 12"
            },
            {
              value: "8 / 11",
              label: "8 / 11"
            },
            {
              value: "9 / 21",
              label: "9 / 21"
            },
            {
              value: "10 / 22",
              label: "10 / 22"
            },
            {
              value: "11 / 23",
              label: "11 / 23"
            },
            {
              value: "12 / 24",
              label: "12 / 24"
            },
            {
              value: "13 / 25",
              label: "13 / 25"
            },
            {
              value: "14 / 26",
              label: "14 / 26"
            },
            {
              value: "15 / 27",
              label: "15 / 27"
            },
            {
              value: "16 / 28",
              label: "16 / 28"
            },
            {
              value: "17 / 38",
              label: "17 / 38"
            },
            {
              value: "18 / 37",
              label: "18 / 37"
            },
            {
              value: "19 / 36",
              label: "19 / 36"
            },
            {
              value: "20 / 35",
              label: "20 / 35"
            },
            {
              value: "21 / 34",
              label: "21 / 34"
            },
            {
              value: "22 / 33",
              label: "22 / 33"
            },
            {
              value: "23 / 32",
              label: "23 / 32"
            },
            {
              value: "24 / 31",
              label: "24 / 31"
            },
            {
              value: "25 / 41",
              label: "25 / 41"
            },
            {
              value: "26 / 42",
              label: "26 / 42"
            },
            {
              value: "27 / 43",
              label: "27 / 43"
            },
            {
              value: "28 / 44",
              label: "28 / 44"
            },
            {
              value: "29 / 45",
              label: "29 / 45"
             },
            {
              value: "30 / 46",
              label: "30 / 46"
            },
            {
              value: "31 / 47",
              label: "31 / 47"
            },
            {
              value: "32 / 48",
              label: "32 / 48"
            },
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'implantBrand',
          label: 'Implant Brand',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          sortLabel: "Implant Brand⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: 'implantBrandList',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 7,
          value: 'implantSurface',
          label: 'Implant Surface',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          hiddenAsterisk: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "Acqua",
              label: "Acqua"
            },
            {
              value: "Bio Spark",
              label: "Bio Spark"
            },
            {
              value: "Etched Ti",
              label: "Etched Ti"
            },
            {
              value: "HA",
              label: "HA"
            },
            {
              value: "HSA",
              label: "HSA"
            },
            {
              value: "MP-1 HA",
              label: "MP-1 HA"
            },
            {
              value: "RBM",
              label: "RBM"
            },
            {
              value: "SLA",
              label: "SLA"
            },
            {
              value: "TiUltra",
              label: "TiUltra"
            },
            {
              value: "TiUnite",
              label: "TiUnite"
            },
            {
              value: "TPS",
              label: "TPS"
            },
            {
              value: "Ultra Clean",
              label: "Ultra Clean"
            },
            {
              value: "ZLA",
              label: "ZLA"
            }
          ]
        },
        {
          id: 8,
          value: 'implantReference',
          label: 'Implant Reference #',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 9,
          value: 'implantLot',
          label: 'Implant LOT#',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 10,
          value: 'diameter',
          label: 'Diameter',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: Array.from(Array(76), (x, index) => index === 0 ? `${index + 2.5} mm` : `${2.5 + (index / 10)} mm`),
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 11,
          value: 'length',
          label: 'Length',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: Array.from(Array(164), (x, index) => index === 0 ? `${index + 5} mm` : index < 151 ? `${5 + (index / 10)} mm` : index === 151 ? `${30} mm` : `${30 + ((index - 151) * 2.5)}`),
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 12,
          value: 'surgProtocol',
          label: 'Surgical Protocol',
          implantRedColor: true,
          visible: true,
          hiddenAsterisk: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          sortLabel: "Surgical Protocol⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: [
            {
              value: "One-Stage",
              label: "One-Stage"
            },
            {
              value: "Two-Stage",
              label: "Two-Stage"
            },
            {
              value: "Immediate Loading",
              label: "Immediate Loading"
            }
          ]
        },
        {
          id: 13,
          value: 'boneDensity',
          label: 'Bone Density',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "D1 >1000 HU",
              label: "D1 >1000 HU"
            },
            {
              value: "D2 701 - 1000 HU",
              label: "D2 701 - 1000 HU"
            },
            {
              value: "D3 250 - 700 HU",
              label: "D3 250 - 700 HU"
            },
            {
              value: "D4 <250 HU",
              label: "D4 <250 HU"
            }
          ]
        },
        {
          id: 14,
          value: 'osteotomySite',
          label: 'Osteotomy Site',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          sortLabel: "Osteotomy Site⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: [
            {
              value: "HealedSite - NativeBone",
              label: "Healed Site - Native Bone"
            },
            {
              value: "HealedSite - BoneGraft",
              label: "Healed Site - Bone Graft"
            },
            {
              value: "Extraction & Immediate Placement",
              label: "Extraction & Immediate Placement"
            },
            {
              value: "Extraction & Immediate Placement With Bone Graft",
              label: "Extraction & Immediate Placement With Bone Graft"
            }
          ]
        },
        {
          id: 15,
          value: 'ncm',
          label: 'Initial Ncm',
          implantRedColor: true,
          visible: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "15 Ncm",
              label: "15 Ncm"
            },
            {
              value: "20 Ncm",
              label: "20 Ncm"
            },
            {
              value: "25 Ncm",
              label: "25 Ncm"
            },
            {
              value: "30 Ncm",
              label: "30 Ncm"
            },
            {
              value: "35 Ncm",
              label: "35 Ncm"
            },
            {
              value: "40 Ncm",
              label: "40 Ncm"
            },
            {
              value: "45 Ncm",
              label: "45 Ncm"
            },
            {
              value: "50 Ncm",
              label: "50 Ncm"
            },
            {
              value: "55 Ncm",
              label: "55 Ncm"
            },
            {
              value: "60 Ncm",
              label: "60 Ncm"
            },
            {
              value: "65 Ncm",
              label: "65 Ncm"
            },
            {
              value: "70 Ncm",
              label: "70 Ncm"
            },
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 16,
          value: 'isqBl',
          label: 'Initial ISQ / BL',
          implantRedColor: true,
          visible: true,
          hiddenAsterisk: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: Array.from(Array(100), (x, index) => index === 0 ? `${index + 1}` : index === 100 ? `` : `${index + 1}`)
        },
        {
          id: 17,
          value: 'isqMd',
          label: 'Initial ISQ / MD',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: Array.from(Array(100), (x, index) => index === 0 ? `${index + 1}` : index === 100 ? `` : `${index + 1}`)
        },
        {
          id: 18,
          value: 'implantDepth',
          label: 'Implant Depth',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          sortLabel: "Implant Depth⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: [
            {
              value: "+3.0 mm",
              label: "+3.0 mm"
            },
            {
              value: "+2.5 mm",
              label: "+2.5 mm"
            },
            {
              value: "+2.0 mm",
              label: "+2.0 mm"
            },
            {
              value: "+1.5 mm",
              label: "+1.5 mm"
            },
            {
              value: "+1.0 mm",
              label: "+1.0 mm"
            },
            {
              value: "+0.5 mm",
              label: "+0.5 mm"
            },
            {
              value: "Flush with Bone",
              label: "Flush with Bone"
            },
            {
              value: "-0.5 mm",
              label: "-0.5 mm"
            },
            {
              value: "-1.0 mm",
              label: "-1.0 mm"
            },
            {
              value: "-1.5 mm",
              label: "-1.5 mm"
            },
            {
              value: "-2.0 mm",
              label: "-2.0 mm"
            },
            {
              value: "-2.5 mm",
              label: "-2.5 mm"
            },
            {
              value: "-3.0 mm",
              label: "-3.0 mm"
            },
          ]
        },
        {
          id: 19,
          value: 'loadingProtocol',
          label: 'Loading Protocol',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 120,
          sortLabel: "Loading Protocol⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: [
            {
              value: "Immediate Temporization 0-48h",
              label: "Immediate Temporization 0-48h"
            },
            {
              value: "Early Loading 48h-3 months",
              label: "Early Loading 48h-3 months"
            },
            {
              value: "Conventional Loading > 3 months",
              label: "Conventional Loading > 3 months"
            }
          ]
        },
        {
          id: 20,
          value: 'recommendedHT',
          label: 'RRPSD',
          implantRedColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 110,
          type: 'date',
        },
        {
          id: 21,
          value: 'finalPA',
          label: 'X-Rays',
          visible: true,
          createRecord: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          novaUpload: true,
          saveType: 'string',
          width: 450,
          allowed: 10,
          type: 'image',
        },
        {
          id: 22,
          value: 'implantRestored',
          label: 'Implant Restored',
          implantRedColor: true,
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          sortLabel: "Implant Restored⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'select',
          oneOf: ["Yes", "No"]
        },
        {
          id: 23,
          value: 'dateOfRemoval',
          label: 'Date of Removal',
          implantRedColor: true,
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          width: 110,
          sortLabel: "Date of Removal⇅",
          sort: true,
          sortable: true,
          dateSort: true,
          type: 'date',
        },
        {
          id: 24,
          value: 'reasonForRemoval',
          label: 'Reason for Removal',
          implantRedColor: true,
          visible: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: ["Broken Implant Neck", "Broken Implant Body", "Unrecoverable Broken Prosthetic Screw", "Loose Implant / No Bone Loss", "Loose Implant / Bone Loss Without Infection", "Loose Implant / Bone Loss With Infection", "Stable Implant / With Infection", "Overheated Bone", "Compression Necrosis", "Unrestorable Implant / Too Deep", "Unrestorable Implant / Excessive Tilt", "Unrestorable Implant / Wrong Position", "Damaged Internal / External Hex of Implant", "Unrecognized Implant Deemed Unrestorable"]
        },
        {
          id: 25,
          value: 'medicalCondition',
          label: 'Medical Condition',
          implantRedColor: true,
          visible: true,
          editRecord: true,
          createRecord: true,
          viewRecord: false,
          viewMode: false,
          width: 140,
          type: 'select',
          oneOf: ["Controlled Diabetes", "Uncontrolled Diabetes", "History of Chemotherapy", "Currently on Chemotherapy", "History of Radiation", "Currently Getting Radiation", "History of Bisphosphonates", "Currently on Bisphosphonates", "Smoker", "Alcohol Addiction", "HIV+"]
        },
        {
          id: 26,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'action',
        }
      ],
      report: [
        {
          id: 1,
          value: 'fromDate',
          label: 'From Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          defaultDate: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'To Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          defaultDate: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'providerId',
          label: 'Provider',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          type: 'select',
          oneOf: 'providersList'
        },
        {
          id: 4,
          value: 'implantBrand',
          label: 'Implant Brand',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          type: 'select',
          oneOf: 'implantBrandList'
        },
        {
          id: 5,
          value: 'loadingProtocol',
          label: 'Loading Protocol',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          type: 'select',
          oneOf: [
            {
              value: "ALL",
              label: "All"
            },
            {
              value: "Immediate Temporization 0-48h",
              label: "Immediate Temporization 0-48h"
            },
            {
              value: "Early Loading 48h-3 months",
              label: "Early Loading 48h-3 months"
            },
            {
              value: "Conventional Loading > 3 months",
              label: "Conventional Loading > 3 months"
            }
          ]
        },
        {
          id: 6,
          value: 'implantRemoved',
          label: 'Date Of Removal',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          type: 'select',
          oneOf: [{
            value: "Both",
            label: "Both"
          },
          {
            value: "yes",
            label: "Yes"
          },
          {
            value: "no",
            label: "No"
          }]
        }
      ]
    };
  }

  function boneGraftLog() {
    return {
      columns: [
        {
          id: 1,
          value: 'date',
          label: 'Date',
          boneGraftColor: true,
          asteriskColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 110,
          sortLabel: "Date⇅",
          sort: true,
          sortable: true,
          dateSort: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: 'patientName',
          label: 'Patient Name',
          boneGraftColor: true,
          asteriskColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          sortLabel: "Patient Name⇅",
          sort: true,
          sortable: true,
          width: 130,
          textSort: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'dob',
          label: 'DOB',
          boneGraftColor: true,
          asteriskColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 110,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'providerName',
          label: 'Provider',
          visible: true,
          boneGraftColor: true,
          asteriskColor: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 130,
          sortLabel: "Provider⇅",
          sort: true,
          sortable: true,
          textSort: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'allograftMaterialUsed',
          label: 'Allograft Type',
          boneGraftColor: true,
          visible: true,
          width: 120,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "Corticocancellous",
              label: "Corticocancellous"
            },
            {
              value: "Demineralized Cortical",
              label: "Demineralized Cortical"
            },
            {
              value: "Mineralized/Demineralized Cortical",
              label: "Mineralized/Demineralized Cortical"
            },
            {
              value: "Mineralized Cancellous",
              label: "Mineralized Cancellous"
            },
            {
              value: "Mineralized Cortical",
              label: "Mineralized Cortical"
            },
            {
              value: "Demineralized bone matrix (DBM) putty",
              label: "Demineralized bone matrix (DBM) putty"
            },
          ]
        },
        {
          id: 6,
          value: 'allograftSize',
          label: 'Allograft Size',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25-1.0mm",
              label: "0.25-1.0mm"
            },
            {
              value: "0.5-1.0mm",
              label: "0.5-1.0mm"
            },
            {
              value: "0.125-0.850mm",
              label: "0.125-0.850mm"
            },
            {
              value: "1.0-2.0mm",
              label: "1.0-2.0mm"
            },
          ]
        },
        {
          id: 7,
          value: 'allograftVolume',
          label: 'Allograft Volume',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25cc",
              label: "0.25cc"
            },
            {
              value: "0.5cc",
              label: "0.5cc"
            },
            {
              value: "0.75cc",
              label: "0.75cc"
            },
            {
              value: "1.0cc",
              label: "1.0cc"
            },
            {
              value: "1.25cc",
              label: "1.25cc"
            },
            {
              value: "1.5cc",
              label: "1.5cc"
            },
            {
              value: "1.75cc",
              label: "1.75cc"
            },
            {
              value: "2.0cc",
              label: "2.0cc"
            },
            {
              value: "2.25cc",
              label: "2.25cc"
            },
            {
              value: "2.5cc",
              label: "2.5cc"
            },
            {
              value: "2.75cc",
              label: "2.75cc"
            },
            {
              value: "3.0cc",
              label: "3.0cc"
            },
            {
              value: "3.25cc",
              label: "3.25cc"
            },
            {
              value: "3.5cc",
              label: "3.5cc"
            },
            {
              value: "3.75cc",
              label: "3.75cc"
            },
            {
              value: "4.0cc",
              label: "4.0cc"
            },
          ]
        },
        {
          id: 8,
          value: 'allograftBrand',
          label: 'Allograft Brand',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 9,
          value: 'xenograftMaterialUsed',
          label: 'Xenograft Type',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "Bovine",
              label: "Bovine"
            },
            {
              value: "Porcine",
              label: "Porcine"
            },
          ]
        },
        {
          id: 10,
          value: 'xenograftSize',
          label: 'Xenograft Size',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25-1.0mm",
              label: "0.25-1.0mm"
            },
            {
              value: "0.5-1.0mm",
              label: "0.5-1.0mm"
            },
            {
              value: "0.125-0.850mm",
              label: "0.125-0.850mm"
            },
            {
              value: "1.0-2.0mm",
              label: "1.0-2.0mm"
            },
          ]
        },
        {
          id: 11,
          value: 'xenograftVolume',
          label: 'Xenograft Volume',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25cc",
              label: "0.25cc"
            },
            {
              value: "0.5cc",
              label: "0.5cc"
            },
            {
              value: "0.75cc",
              label: "0.75cc"
            },
            {
              value: "1.0cc",
              label: "1.0cc"
            },
            {
              value: "1.25cc",
              label: "1.25cc"
            },
            {
              value: "1.5cc",
              label: "1.5cc"
            },
            {
              value: "1.75cc",
              label: "1.75cc"
            },
            {
              value: "2.0cc",
              label: "2.0cc"
            },
            {
              value: "2.25cc",
              label: "2.25cc"
            },
            {
              value: "2.5cc",
              label: "2.5cc"
            },
            {
              value: "2.75cc",
              label: "2.75cc"
            },
            {
              value: "3.0cc",
              label: "3.0cc"
            },
            {
              value: "3.25cc",
              label: "3.25cc"
            },
            {
              value: "3.5cc",
              label: "3.5cc"
            },
            {
              value: "3.75cc",
              label: "3.75cc"
            },
            {
              value: "4.0cc",
              label: "4.0cc"
            },
          ]
        },
        {
          id: 12,
          value: 'xenograftBrand',
          label: 'Xenograft Brand',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 13,
          value: 'alloplastMaterialUsed',
          label: 'Alloplast Type',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "Hydroxyapatite (HA)",
              label: "Hydroxyapatite (HA)"
            },
            {
              value: "Tricalcium phosphate (TCP)",
              label: "Tricalcium phosphate (TCP)"
            },
            {
              value: "Biphasic calcium phosphate (BCP)",
              label: "Biphasic calcium phosphate (BCP)"
            },
            {
              value: "Calcium sulfate (CS)",
              label: "Calcium sulfate (CS)"
            },
            {
              value: "Calcium phosphate (CP)",
              label: "Calcium phosphate (CP)"
            },
            {
              value: "Bioglass (BG)",
              label: "Bioglass (BG)"
            },
            {
              value: "Carbonate apatite (CA)",
              label: "Carbonate apatite (CA)"
            },
          ]
        },
        {
          id: 14,
          value: 'alloplastSize',
          label: 'Alloplast Size',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25-1.0mm",
              label: "0.25-1.0mm"
            },
            {
              value: "0.5-1.0mm",
              label: "0.5-1.0mm"
            },
            {
              value: "0.125-0.850mm",
              label: "0.125-0.850mm"
            },
            {
              value: "1.0-2.0mm",
              label: "1.0-2.0mm"
            },
          ]
        },
        {
          id: 15,
          value: 'alloplastVolume',
          label: 'Alloplast Volume',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25cc",
              label: "0.25cc"
            },
            {
              value: "0.5cc",
              label: "0.5cc"
            },
            {
              value: "0.75cc",
              label: "0.75cc"
            },
            {
              value: "1.0cc",
              label: "1.0cc"
            },
            {
              value: "1.25cc",
              label: "1.25cc"
            },
            {
              value: "1.5cc",
              label: "1.5cc"
            },
            {
              value: "1.75cc",
              label: "1.75cc"
            },
            {
              value: "2.0cc",
              label: "2.0cc"
            },
            {
              value: "2.25cc",
              label: "2.25cc"
            },
            {
              value: "2.5cc",
              label: "2.5cc"
            },
            {
              value: "2.75cc",
              label: "2.75cc"
            },
            {
              value: "3.0cc",
              label: "3.0cc"
            },
            {
              value: "3.25cc",
              label: "3.25cc"
            },
            {
              value: "3.5cc",
              label: "3.5cc"
            },
            {
              value: "3.75cc",
              label: "3.75cc"
            },
            {
              value: "4.0cc",
              label: "4.0cc"
            },
          ]
        },
        {
          id: 16,
          value: 'alloplastBrand',
          label: 'Alloplast Brand',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 17,
          value: 'autograftMaterialUsed',
          label: 'Autograft Type',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "Chin",
              label: "Chin"
            },
            {
              value: "Ramus",
              label: "Ramus"
            },
            {
              value: "Tuberosity",
              label: "Tuberosity"
            },
            {
              value: "Alveolar bone",
              label: "Alveolar bone"
            },
            {
              value: "Osteotomy bone",
              label: "Osteotomy bone"
            },
            {
              value: "Hip",
              label: "Hip"
            },
            {
              value: "Calvarium",
              label: "Calvarium"
            },
          ]
        },
        {
          id: 18,
          value: 'autograftSize',
          label: 'Autograft Size',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25-1.0mm",
              label: "0.25-1.0mm"
            },
            {
              value: "0.5-1.0mm",
              label: "0.5-1.0mm"
            },
            {
              value: "0.125-0.850mm",
              label: "0.125-0.850mm"
            },
            {
              value: "1.0-2.0mm",
              label: "1.0-2.0mm"
            },
          ]
        },
        {
          id: 19,
          value: 'autograftVolume',
          label: 'Autograft Volume',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25cc",
              label: "0.25cc"
            },
            {
              value: "0.5cc",
              label: "0.5cc"
            },
            {
              value: "0.75cc",
              label: "0.75cc"
            },
            {
              value: "1.0cc",
              label: "1.0cc"
            },
            {
              value: "1.25cc",
              label: "1.25cc"
            },
            {
              value: "1.5cc",
              label: "1.5cc"
            },
            {
              value: "1.75cc",
              label: "1.75cc"
            },
            {
              value: "2.0cc",
              label: "2.0cc"
            },
            {
              value: "2.25cc",
              label: "2.25cc"
            },
            {
              value: "2.5cc",
              label: "2.5cc"
            },
            {
              value: "2.75cc",
              label: "2.75cc"
            },
            {
              value: "3.0cc",
              label: "3.0cc"
            },
            {
              value: "3.25cc",
              label: "3.25cc"
            },
            {
              value: "3.5cc",
              label: "3.5cc"
            },
            {
              value: "3.75cc",
              label: "3.75cc"
            },
            {
              value: "4.0cc",
              label: "4.0cc"
            },
          ]
        },
        {
          id: 20,
          value: 'membraneUsed',
          label: 'Membrane Type',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "Bovine",
              label: "Bovine"
            },
            {
              value: "Porcine",
              label: "Porcine"
            },
            {
              value: "Allogenic",
              label: "Allogenic"
            },
            {
              value: "Pericardium",
              label: "Pericardium"
            }
          ]
        },
        {
          id: 21,
          value: 'membraneSize',
          label: 'Membrane Size',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "10 X 10 mm",
              label: "10 X 10 mm"
            },
            {
              value: "10 X 15 mm",
              label: "10 X 15 mm"
            },
            {
              value: "13 X 25 mm",
              label: "13 X 25 mm"
            },
            {
              value: "25 X 25 mm",
              label: "25 X 25 mm"
            },
            {
              value: "30 X 40 mm",
              label: "30 X 40 mm"
            },
          ]
        },
        {
          id: 22,
          value: 'membraneBrand',
          label: 'Membrane Brand',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 23,
          value: 'additives',
          label: 'Additives',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf:[
            {
              value: "PRP",
              label: "PRP"
            },
            {
              value: "PRF",
              label: "PRF"
            },
            {
              value: "Gem 21S",
              label: "Gem 21S"
            },
            {
              value: "Infuse",
              label: "Infuse"
            },
            {
              value: "Stem cells",
              label: "Stem cells"
            },
          ]
        },
        {
          id: 24,
          value: 'additivesVolume',
          label: 'Additives Volume',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: [
            {
              value: "0.25cc",
              label: "0.25cc"
            },
            {
              value: "0.5cc",
              label: "0.5cc"
            },
            {
              value: "0.75cc",
              label: "0.75cc"
            },
            {
              value: "1.0cc",
              label: "1.0cc"
            },
            {
              value: "1.25cc",
              label: "1.25cc"
            },
            {
              value: "1.5cc",
              label: "1.5cc"
            },
            {
              value: "1.75cc",
              label: "1.75cc"
            },
            {
              value: "2.0cc",
              label: "2.0cc"
            },
            {
              value: "2.25cc",
              label: "2.25cc"
            },
            {
              value: "2.5cc",
              label: "2.5cc"
            },
            {
              value: "2.75cc",
              label: "2.75cc"
            },
            {
              value: "3.0cc",
              label: "3.0cc"
            },
            {
              value: "3.25cc",
              label: "3.25cc"
            },
            {
              value: "3.5cc",
              label: "3.5cc"
            },
            {
              value: "3.75cc",
              label: "3.75cc"
            },
            {
              value: "4.0cc",
              label: "4.0cc"
            },
          ]
        },
        {
          id: 25,
          value: 'additivesBrand',
          label: 'Additives Brand',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 26,
          value: 'rrpsd',
          label: 'RRPSD',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          width: 110,
          type: 'date',
        },
        {
          id: 27,
          value: 'finalPA',
          label: 'X-Rays',
          visible: true,
          createRecord: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          novaUpload: true,
          saveType: 'string',
          width: 450,
          allowed: 10,
          type: 'image',
        },
        {
          id: 28,
          value: 'failureDate',
          label: 'Failure Date',
          boneGraftColor: true,
          visible: true,
          createRecord: true,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          width: 110,
          type: 'date',
        },
        {
          id: 29,
          value: 'medicalCondition',
          label: 'Medical Condition',
          boneGraftColor: true,
          visible: true,
          editRecord: true,
          createRecord: true,
          viewRecord: false,
          viewMode: false,
          width: 140,
          type: 'select',
          oneOf: ["Controlled Diabetes", "Uncontrolled Diabetes", "History of Chemotherapy", "Currently on Chemotherapy", "History of Radiation", "Currently Getting Radiation", "History of Bisphosphonates", "Currently on Bisphosphonates", "Smoker", "Alcohol Addiction", "HIV+"]
        },
        {
          id: 30,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          action: true,
          type: 'action',
        }
      ],
      report: [
        {
          id: 1,
          value: 'fromDate',
          label: 'From Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          defaultDate: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'To Date',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          defaultDate: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'providerId',
          label: 'Provider',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          createRecord: true,
          type: 'select',
          oneOf: 'providersList'
        },
      ]
    };
  }

  function referralSourceDetail() {
    return {
      columns: [
        {
          id: 1,
          value: 'patientName',
          label: 'Patient Name',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'phone',
          label: 'Phone',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'email',
          label: 'Email',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'providerName',
          label: 'Provider Name',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
      ],
    };
  }

  function reports() {
    return {
      dailyReports: {
        depositSlip: [
          {
            id: 1,
            value: 'paidDate',
            label: 'Paid Date',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
            Cell: props => <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>
          },
          {
            id: 2,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'bank',
            label: 'Bank',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'paidAmount',
            label: 'Paid Amount',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            Cell: props => <span>{currencyFormatter.format(props.value)}</span>
          },
        ],
        unScheduledTreatmentPlan: [
          {
            id: 1,
            value: 'date',
            label: 'Date',
            visible: true,
            Cell: props => <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>
          },
          {
            id: 2,
            value: 'toothNumber',
            label: 'Tooth',
            visible: true,
          },
          {
            id: 3,
            value: 'procedureCode',
            label: 'Procedure Code',
            visible: true,
          },
          {
            id: 4,
            value: 'procedureDescription',
            label: 'Procedure Description',
            visible: true,
          },
          {
            id: 5,
            value: 'fee',
            label: 'Amount',
            visible: true,
            Cell: props => <span>{props.value ? currencyFormatter.format(props.value): props.value}</span>
          },
        ],
      },
      accounting: {
        outstandingInsurance: [
          {
            id: 1,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'patientId',
            label: 'Patient ID',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'cdtCode',
            label: 'CDT Code',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'datePerformed',
            label: 'Date Performed',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
          },
          {
            id: 5,
            value: 'estIns',
            label: 'Est. Ins.',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 6,
            value: 'insurancePaidAmt',
            label: 'Ins. Paid',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 7,
            value: 'balanceAmt',
            label: 'Balance',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          }
        ],
        PatientBalance: [
          {
            id: 1,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'patientId',
            label: 'Patient ID',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'cdtCode',
            label: 'CDT Code',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'datePerformed',
            label: 'Date Performed',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
          },
          {
            id: 5,
            value: 'balanceToPatient',
            label: 'Balance To Patient',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 6,
            value: 'patientPaid',
            label: 'Patient Paid',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 7,
            value: 'outstandingBalance',
            label: 'Outstanding Balance',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          }
        ],
        totalCollectedAmount: [
          {
            id: 1,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'patientId',
            label: 'Patient ID',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'cdtCode',
            label: 'CDT Code',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'fee',
            label: 'Fee',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 5,
            value: 'paymentReceivedFromInsurance',
            label: 'Paid By Insurance',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 6,
            value: 'paymentReceivedFromPatient',
            label: 'Paid By Patient',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 7,
            value: 'totalCollected',
            label: 'Total Collected',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
            currency: true,
          },
          {
            id: 8,
            value: 'totalCollectedPercent',
            label: 'Collection Percentage',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            percent: true,
            type: 'input',
          }
        ]
      },
      patient: {
        PatientType: [
          {
            id: 1,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'firstVisit',
            label: 'First visit date',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'phoneNumber',
            label: 'Phone Number',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'address',
            label: 'Address',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'email',
            label: 'Email',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 6,
            value: 'providerName',
            label: 'Provider',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 7,
            value: 'patientType',
            label: 'Patient Type',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          }
        ],
        recallReport: [
          {
            id: 1,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'phoneNumber',
            label: 'Phone Number',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'email',
            label: 'Email',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'address',
            label: 'Address',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'referredBy',
            label: 'Referred By',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 6,
            value: 'recallType',
            label: 'Recall Type',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 7,
            value: 'recallCode',
            label: 'Recall Code',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 8,
            value: 'interval',
            label: 'Interval',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 9,
            value: 'nextVisitdate',
            label: 'Recall Due Date',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
            Cell: props => <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>
          },
          {
            id: 10,
            value: 'providerName',
            label: 'Provider',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 11,
            value: 'recallNote',
            label: 'Note',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          }
        ],
        referralSource: [
          {
            id: 1,
            value: 'referralSource',
            label: 'Referral Source',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'size',
            label: 'Count',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'action',
            label: 'Action',
            visible: true,
            editRecord: false,
            viewRecord: true,
            viewMode: true,
            referralSource: true,
            action: true,
            type: 'action',
          }
        ],
        referredToo: [
          {
            id: 1,
            value: 'referredTo',
            label: 'Referred To',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'size',
            label: 'Count',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'action',
            label: 'Action',
            visible: true,
            editRecord: false,
            viewRecord: true,
            viewMode: true,
            referredTo: true,
            action: true,
            type: 'action',
          }
        ],
        newPatient: [
          {
            id: 1,
            value: 'dateOfFirstVisit',
            label: 'Date of First Visit',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
            Cell: props => <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>
          },
          {
            id: 2,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'phoneNumber',
            label: 'Phone Number',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'email',
            label: 'Email',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'address',
            label: 'Address',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 6,
            value: 'providerName',
            label: 'Provider',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 7,
            value: 'referredBy',
            label: 'Referred By',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          }
        ]
      },
      appointment: {
        providerAppointment: [
          {
            id: 1,
            value: 'date',
            label: 'Date',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
            Cell: props => <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>
          },
          {
            id: 2,
            value: 'startTime',
            label: 'Start Time',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'endTime',
            label: 'End Time',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'phoneNumber',
            label: 'Phone Number',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 6,
            value: 'email',
            label: 'Email',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 7,
            value: 'providerName',
            label: 'Provider',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 8,
            value: 'operatoryName',
            label: 'Operatory Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 9,
            value: 'procedureCode',
            label: 'Procedure Code',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 10,
            value: 'reason',
            label: 'Reason',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          }
        ],
        cancelReport: [
          {
            id: 1,
            value: 'date',
            label: 'Date',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'date',
            Cell: props => <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>
          },
          {
            id: 2,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'phoneNumber',
            label: 'Phone Number',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'email',
            label: 'Email',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'providerName',
            label: 'Provider Name',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 6,
            value: 'referredBy',
            label: 'Referred By',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 7,
            value: 'reason',
            label: 'Reason',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 8,
            value: 'status',
            label: 'Status',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          }
        ],
      },
      implantReports: {
        implantData: [
          {
            id: 1,
            value: 'date',
            label: 'Date',
            visible: true,
            width: 110,
          },
          {
            id: 2,
            value: 'patientName',
            label: 'Patient Name',
            visible: true,
            width: 130,
          },
          {
            id: 3,
            value: 'dob',
            label: 'DOB',
            visible: true,
            width: 110,
          },
          {
            id: 4,
            value: 'providerName',
            label: 'Provider',
            visible: true,
            width: 150,
          },
          {
            id: 5,
            value: 'implantSite',
            label: 'Implant Site #',
            visible: true,
          },
          {
            id: 6,
            value: 'implantBrand',
            label: 'Implant Brand',
            visible: true,
          },
          {
            id: 7,
            value: 'implantSurface',
            label: 'Implant Surface',
            visible: true,
          },
          {
            id: 8,
            value: 'implantReference',
            label: 'Implant Reference #',
            visible: true,
            width: 110,
          },
          {
            id: 9,
            value: 'implantLot',
            label: 'Implant LOT#',
            visible: true,
          },
          {
            id: 10,
            value: 'diameter',
            label: 'Diameter',
            visible: true,
          },
          {
            id: 11,
            value: 'length',
            label: 'Length',
            visible: true,
          },
          {
            id: 12,
            value: 'surgProtocol',
            label: 'Surgical Protocol',
            visible: true,
          },
          {
            id: 13,
            value: 'boneDensity',
            label: 'Bone Density',
            visible: true,
          },
          {
            id: 14,
            value: 'osteotomySite',
            label: 'Osteotomy Site',
            visible: true,
          },
          {
            id: 15,
            value: 'ncm',
            label: 'Initial Ncm',
            visible: true,
          },
          {
            id: 16,
            value: 'isqBl',
            label: 'Initial ISQ / BL',
            visible: true,
          },
          {
            id: 17,
            value: 'isqMd',
            label: 'Initial ISQ / MD',
            visible: true,
          },
          {
            id: 18,
            value: 'implantDepth',
            label: 'Implant Depth',
            visible: true,
          },
          {
            id: 19,
            value: 'loadingProtocol',
            label: 'Loading Protocol',
            visible: true,
            width: 120,
          },
          {
            id: 20,
            value: 'recommendedHT',
            label: 'RRPSD',
            visible: true,
            width: 110,
          },
          {
            id: 21,
            value: 'finalPA',
            label: 'Final PA',
            visible: true,
            showImage: true
          },
          {
            id: 22,
            value: 'implantRestored',
            label: 'Implant Restored',
            visible: true,
          },
          {
            id: 23,
            value: 'dateOfRemoval',
            label: 'Date of Removal',
            visible: true,
            width: 110,
          },
          {
            id: 24,
            value: 'reasonForRemoval',
            label: 'Reason for Removal',
            visible: true,
            width: 120,
          },
        ],
      },
    }
  }

  function reportsForm() {
    return {
      depositSlip: [
        {
          id: 1,
          value: 'startDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'endDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'payMethod',
          label: 'Payment Methods',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          fullWidth: true,
          reportsGroup: true,
          allOption: true,
          type: 'multicheckbox',
          oneOf: [
           {
             "id": 1,
             "value": "Cash Payment",
             "label": "Cash Payment"
           },
           {
             "id": 2,
             "value": "Patient Cheque Payment",
             "label": "Patient Cheque Payment"
           },
           {
             "id": 3,
             "value": "Credit Card Payment",
             "label": "Credit Card Payment"
           },
           {
             "id": 4,
             "value": "Electronic Transfer Payment",
             "label": "Electronic Transfer Payment"
           },
           {
             "id": 5,
             "value": "Lending Club Finance Payment",
             "label": "Lending Club Finance Payment"
           },
           {
             "id": 6,
             "value": "Care Credit Payment",
             "label": "Care Credit Payment"
           },
           {
             "id": 7,
             "value": "Alphaeon Credit Payment",
             "label": "Alphaeon Credit Payment"
           },
           {
             "id": 8,
             "value": "Third Party Cheque Payment",
             "label": "Third Party Cheque Payment"
           },
           {
             "id": 9,
             "value": "EFTPOS Payment",
             "label": "EFTPOS Payment"
           },
           {
             "id": 10,
             "value": "Insurance Cheque Payment",
             "label": "Insurance Cheque Payment"
           },
           {
             "id": 11,
             "value": "eClaim Electronic Payment",
             "label": "eClaim Electronic Payment"
           },
           {
             "id": 12,
             "value": "Cheque Refund Payment",
             "label": "Cheque Refund Payment"
           },
           {
             "id": 13,
             "value": "Credit Card Refund Payment",
             "label": "Credit Card Refund Payment"
           },
           {
             "id": 14,
             "value": "Cash Refund Payment",
             "label": "Cash Refund Payment"
           },
         ],
        },
      ],
      unScheduledTreatmentPlan: [
        {
          id: 1,
          value: 'startDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'endDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'providersList'
        }
      ],
      outstandingInsurance: [
        {
          id: 1,
          value: 'provider',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'providersList'
        }
      ],
      PatientBalance: [
        {
          id: 1,
          value: 'provider',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'providersList'
        }
      ],
      totalCollectedAmount: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'provider',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'providersList'
        }
      ],
      PatientType: [
        {
          id: 1,
          value: 'patientType',
          label: 'Patient Type',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          optionsOnly: true,
          fullWidth: true,
          type: 'multiselect',
          oneOf: 'patientTypeList'
        }
      ],
      recallReport: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        }
      ],
      referralSource: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'referralSource',
          label: 'Referral Source',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: 'referralSourcesList'
        }
      ],
      referredToo: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'referredTo',
          label: 'Referral To',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          oneOf: 'referredToList'
        }
      ],
      newPatient: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        }
      ],
      providerAppointment: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'providersList'
        }
      ],
      cancelReport: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        }
      ],
      implantData: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'providerId',
          label: 'Provider',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'providersList'
        },
        {
          id: 4,
          value: 'implantBrand',
          label: 'Implant Brand',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'implantBrandList'
        },
        {
          id: 5,
          value: 'loadingProtocol',
          label: 'Loading Protocol',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: [
            {
              value: "ALL",
              label: "All"
            },
            {
              value: "Immediate Temporization 0-48h",
              label: "Immediate Temporization 0-48h"
            },
            {
              value: "Early Loading 48h-3 months",
              label: "Early Loading 48h-3 months"
            },
            {
              value: "Conventional Loading > 3 months",
              label: "Conventional Loading > 3 months"
            }
          ]
        },
        {
          id: 6,
          value: 'implantRemoved',
          label: 'Implant Removed',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: [
            {
              value: "Both",
              label: "Both"
            },
            {
              value: "yes",
              label: "Yes"
            },
            {
              value: "no",
              label: "No"
            }
          ]
        },
      ],
    }
  }

  function adminReports() {
    return {
      catalog: {
        catalogOrdersRevenue: [
          {
            id: 1,
            value: 'totalOrders',
            label: 'Total Number of Orders',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'totalRevenue',
            label: 'Total Revenue',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'totalTax',
            label: 'Total Tax',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'totalShipping',
            label: 'Total Shipping',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'totalSaving',
            label: 'Total Savings',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
        ]
      },
      consultation: {
        consultationsRevenue: [
          {
            id: 1,
            value: 'totalNumberOfConsultations',
            label: 'Total Number of Consultations',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'totalRevenue',
            label: 'Total Revenue',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
        ]
      },
      inOfficeSupport: {
        inOfficeSupportRevenue: [
          {
            id: 1,
            value: 'totalInOfficeSupport',
            label: 'Total Number of In-office Support',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'totalRevenue',
            label: 'Total Revenue',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
        ]
      },
      orders: {
        orderHistory: [
          {
            id: 1,
            value: 'orderId',
            label: 'Orders #',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 2,
            value: 'staffName',
            label: 'Dentist',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 3,
            value: 'practiceName',
            label: 'Practice',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 4,
            value: 'orderDate',
            label: 'Order Date',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
          {
            id: 5,
            value: 'grandTotal',
            label: 'Total',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            currency: true,
            type: 'input',
          },
          {
            id: 6,
            value: 'totalSavings',
            label: 'Total Savings',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            currency: true,
            type: 'input',
          },
          {
            id: 7,
            value: 'status',
            label: 'Status',
            visible: true,
            editRecord: false,
            viewRecord: false,
            viewMode: false,
            type: 'input',
          },
        ]
      }
    }
  }

  function adminReportsForm() {
    return {
      catalogOrdersRevenue: [
        {
          id: 1,
          value: 'startDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'endDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'vendorId',
          label: 'Vendor',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'vendorOptions',
        }
      ],
      consultationsRevenue: [
        {
          id: 1,
          value: 'startDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'endDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'consultantId',
          label: 'Consultant',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'consultantList',
        }
      ],
      inOfficeSupportRevenue: [
        {
          id: 1,
          value: 'startDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'endDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
      ],
      orderHistory: [
        {
          id: 1,
          value: 'startDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'endDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          showAll: true,
          type: 'date',
        },
        {
          id: 3,
          value: 'staffId',
          label: 'Dentist',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          required: true,
          oneOf: 'doctorsList',
        },
        {
          id: 4,
          value: 'status',
          label: 'Status',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          type: 'select',
          allOption: true,
          required: true,
          oneOf: [
            'Received',
            'Fulfilled',
            'Closed',
            'Canceled',
            'Returned'
          ],
        }
      ],
    }
  }

  function dentistEducationRequests() {
    return {
      columns: [
        {
          id: 1,
          value: 'requestId',
          label: 'CE Request ID',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Course',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'courseId',
          label: 'Course Id',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'speaker',
          label: 'Speaker',
          sortLabel: "Speaker⇅",
          sort: true,
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'category',
          label: 'Category',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 6,
          value: 'cost',
          label: 'Cost',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 7,
          value: 'dateViewed',
          label: 'Date Course Viewed',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 7,
          value: 'dateRequested',
          label: 'Date CE Credit Requested',
          sortLabel: "Date CE Credit Requested⇅",
          sort: true,
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 8,
          value: 'status',
          label: 'Status',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
      ],
    };
  }

  function manualSection() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: "sectionType",
          label: "Section Type",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          onChange: true,
          labelStyle: true,
          width: 150,
          type: "Radiobox",
          oneOf: [
            {
              "value": "section",
              "title": "Section"
            },
            {
              "value": "manuals",
              "title": "Manuals"
            },
          ]
        },
        {
          id: 2,
          value: 'name',
          label: 'Name',
          visible: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'parent',
          label: 'Section',
          visible: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'manualOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'link',
          label: 'Link',
          visible: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'uploadLink',
          label: 'Manual Upload',
          visible: true,
          viewRecord: true,
          viewMode: true,
          onChange: true,
          novaUpload: true,
          saveType: 'string',
          allowed: 1,
          type: 'manuals',
        },
      ],
    };
  }

  function ledger() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
        },
        {
          id: 2,
          value: 'datePerformed',
          label: 'Date Performed',
          visible: true,
          width: 150,
          type: 'date',
        },
        {
          id: 3,
          value: 'cdcCode',
          label: 'CDT Code',
          width: 150,
          visible: true,
        },
        {
          id: 4,
          value: 'procedureName',
          label: 'Procedure Description',
          visible: true,
          width: 400,
          limit: 50,
        },
        {
          id: 5,
          value: 'fee',
          label: 'Fee',
          visible: true,
          currency: true,
        },
        {
          id: 6,
          value: 'totalInsurancePaidAmt',
          label: 'Total Insurance Paid Amount',
          visible: true,
          currency: true,
        },
        {
          id: 7,
          value: 'totalPatientPaidAmt',
          label: 'Total Patient Paid Amount',
          visible: true,
          currency: true,
        },
        {
          id: 8,
          value: 'balance',
          label: 'Balance',
          visible: true,
          currency: true,
        },
        {
          id: 9,
          value: 'action',
          label: 'Action',
          visible: true,
          action: true,
        },
      ],
      ledgerPayment: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
        },
        {
          id: 2,
          value: 'date',
          label: 'Date',
          visible: true,
          editRecord: true,
          type: 'date',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'adjustmentReason',
          label: 'Adjustment Reason',
          visible: true,
          editRecord: true,
          type: 'select',
          oneOf: 'adjustmentReason',
        },
        {
          id: 4,
          value: 'paidBy',
          label: 'Paid By',
          visible: true,
          editRecord: true,
          type: 'select',
          oneOf: ['Insurance', 'Patient'],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'carrierName',
          label: 'Carrier Name',
          visible: true,
          editRecord: false,
          type: 'select',
          oneOf: 'dentalCarrier',
        },
        {
          id: 6,
          value: 'carrierId',
          label: 'Carrier Name',
          visible: false,
          editRecord: true,
          type: 'select',
          oneOf: 'dentalCarrier',
        },
        {
          id: 7,
          value: 'totalAmount',
          label: 'Total Amount',
          visible: true,
          editRecord: true,
          number: true,
          currency: true,
          type: 'input',
        },
        {
          id: 8,
          value: 'amount',
          label: 'Amount for this Procedure',
          visible: true,
          editRecord: true,
          number: true,
          currency: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 9,
          value: 'paymentType',
          label: 'Payment Type',
          visible: true,
          editRecord: true,
          type: 'select',
          oneOf: [
            'Cash',
            'Credit Card',
            'Electronic Transfer',
            'Cheque',
            'Lending Club Finance',
            'Care Credit Payment',
            'Alphaeon Credit Payment',
            'EFTPOS Payment',
          ]
        },
        {
          id: 10,
          value: 'paymentRefNumber',
          label: 'Payment Ref Num',
          visible: true,
          editRecord: true,
          type: 'input',
        },
        {
          id: 11,
          value: 'action',
          label: 'Action',
          visible: true,
          editRecord: false,
          action: true,
          type: 'action',
        },
      ],
    };
  }

  function purchasedCE() {
    return {
      columns: [
        {
          id: 1,
          value: 'courseName',
          label: 'Course Name',
          visible: true,
        },
        {
          id: 2,
          value: 'category',
          label: 'Category',
          visible: true,
        },
        {
          id: 3,
          value: 'staffName',
          label: 'Dentist',
          visible: true,
        },
        {
          id: 4,
          value: 'purchasedDate',
          label: 'Purchased Date',
          visible: true,
        },
        {
          id: 5,
          value: 'cost',
          label: 'Cost',
          visible: true,
          currency: true,
        },
      ],
      ceFilter: [
        {
          id: 1,
          value: 'fromDate',
          label: 'Start Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          showAll: true,
          type: 'date',
        },
        {
          id: 2,
          value: 'toDate',
          label: 'End Date',
          visible: false,
          editRecord: true,
          viewRecord: false,
          viewMode: false,
          fullWidth: true,
          showAll: true,
          type: 'date',
        },
      ],
    };
  }

  function tutorialsList() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Name',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 3,
          value: 'link',
          label: 'Link',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'parent',
          label: 'Section',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
      ]
    }
  }

  function tutorialSection() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: "sectionType",
          label: "Section Type",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          onChange: true,
          labelStyle: true,
          width: 150,
          type: "Radiobox",
          oneOf: [
            {
              "value": "section",
              "title": "Section"
            },
            {
              "value": "tutorials",
              "title": "Tutorials"
            },
          ]
        },
        {
          id: 2,
          value: 'name',
          label: 'Name',
          visible: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'link',
          label: 'Link',
          visible: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'parent',
          label: 'Section',
          visible: true,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          oneOf: 'tutorialsOptions',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
    };
  }

  function consultations() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'dentistName',
          label: 'Dentist',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'practiceName',
          label: 'Practice',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'dateRequestedReadable',
          label: 'Date Requested',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'dentistPhone',
          label: 'Dentist Contact',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 6,
          value: 'patient',
          label: 'Patient Name',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 7,
          value: 'amount',
          label: 'Fee',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          dollar: true,
          placeholder: 'Amount',
          type: 'input',
        },
        {
          id: 8,
          value: 'status',
          label: 'Status',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          onChange: true,
          type: 'select',
          splitColumn: true,
          oneOf: [
            'Open',
            'Closed',
          ],
        },
        {
          id: 9,
          value: 'mode',
          label: 'Mode',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          optionsOnly: true,
          type: 'select',
          oneOf: [
            {
              value: 'Online',
              label: 'Online Consultation',
            },
            {
              value: "in-office visit",
              label: "In-office visit"
            }
          ],
        },
        {
          id: 10,
          value: 'comment',
          label: 'Comment',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'textarea',
        },
      ],
    };
  }

  function inOfficeSupport() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'dentistName',
          label: 'Dentist',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'practiceName',
          label: 'Practice',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 4,
          value: 'dateRequestedReadable',
          label: 'Date Requested',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 5,
          value: 'dentistPhone',
          label: 'Dentist Contact',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'input',
        },
        {
          id: 6,
          value: 'patient',
          label: 'Patient Name',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 7,
          value: 'status',
          label: 'Status',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          onChange: true,
          type: 'select',
          splitColumn: true,
          oneOf: [
            'Open',
            'Closed',
          ],
        },
        {
          id: 8,
          value: 'amount',
          label: 'Amount',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          dollar: true,
          placeholder: 'Amount',
          type: 'input',
        },
        {
          id: 9,
          value: 'mode',
          label: 'Mode',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          optionsOnly: true,
          type: 'select',
          oneOf: [
            'Online',
            'in-office visit',
          ],
        },
        {
          id: 10,
          value: 'comment',
          label: 'Comment',
          visible: false,
          editRecord: false,
          viewRecord: true,
          viewMode: false,
          type: 'textarea',
        },
      ],
    };
  }

  function saasAdmin() {
    return {
      columns: [
         {
           id: 1,
           value: "id",
           label: "S.No",
           visible: false,
           editRecord: false,
           viewRecord: false,
           viewMode: false,
           type: "input",
         },
         {
           id: 2,
           value: "name",
           label: "Name",
           sortLabel: "Name⇅",
           saasStyle: true,
           sortable: true,
           sort: true,
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: true,
           type: "input",
           formFieldDecorationOptions: {
             general: {
               required: true,
             },
           },
         },
         {
           id: 3,
           value: "logo",
           label: "Logo",
           saasImage: true,
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: false,
           required: true,
           novaUpload: true,
           saveType: 'string',
           allowed: 1,
           type: 'image',
         },
         {
           id: 4,
           value: "saasType",
           label: "Category",
           sortLabel: "Category⇅",
           sort: true,
           sortable: true,
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: true,
           type: "select",
           oneOf: ["Business Ops", "Marketing", "Customer Support", "Finance", "HR", "Sales", "Security", "Hosting", "Other"],
           formFieldDecorationOptions: {
             general: {
               required: true,
             },
           },
         },
         {
           id: 5,
           value: "subscriptionType",
           label: "Subscription Type",
           sortLabel: "Subscription Type⇅",
           sort: true,
           sortable: true,
           width: 110,
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: true,
           type: "select",
           oneOf: ["Annual", "Monthly"],
         },
         {
           id: 6,
           value: "subscriptionFee",
           label: "Subscription Fee",
           sortLabel: "Subscription Fee ($)⇅",
           sort: true,
           sortable: true,
           width: 110,
           visible: true,
           number: true,
           editRecord: true,
           viewRecord: true,
           viewMode: true,
           type: "input",
         },
         {
           id: 7,
           value: "subStartDate",
           label: "Subscription Start Date",
           sortLabel: "Subscription Start Date⇅",
           sort: true,
           sortable: true,
           dateSort: true,
           width: 110,
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: true,
           type: "date",
           formFieldDecorationOptions: {
             general: {
               required: true,
             },
           },
         },
         {
           id: 8,
           value: "subExpDate",
           label: "Subscription Expiration Date",
           sortLabel: "Subscription Expiration Date⇅",
           sort: true,
           sortable: true,
           dateSort: true,
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: true,
           width: 120,
           type: "date",
         },
         {
           id: 9,
           value: "autoRenew",
           label: "Auto Renew",
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: false,
           type: "select",
           oneOf: ["Yes", "No"],
         },
         {
           id: 10,
           value: "documents",
           label: "Document",
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: false,
           saasIcon: true,
           novaUpload: true,
           saveType: 'string',
           allowed: 1,
           type: "agreement",
         },
         {
           id: 11,
           value: "notes",
           label: "Notes",
           visible: true,
           editRecord: true,
           viewRecord: true,
           viewMode: false,
           type: "textarea",
         },
       ],
      saasPayment: [
        {
          id: 1,
          value: "year",
          label: "Year",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          saasSelect: true,
          onChange: true,
          type: "select",
          oneOf: [2021, 2020, 2019, 2018, 2017],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 2,
          value: "Jan",
          label: "January",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 3,
          value: "Feb",
          label: "February",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 4,
          value: "Mar",
          label: "March",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 5,
          value: "Apr",
          label: "April",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 6,
          value: "May",
          label: "May",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 7,
          value: "Jun",
          label: "June",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 8,
          value: "Jul",
          label: "July",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 9,
          value: "Aug",
          label: "August",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 10,
          value: "Sep",
          label: "September",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 11,
          value: "Oct",
          label: "October",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 12,
          value: "Nov",
          label: "November",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 13,
          value: "Dec",
          label: "December",
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
        {
          id: 14,
          value: "total",
          label: "Total",
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          number: true,
          type: "input",
        },
      ],
    };
  }

  function friendReferral() {
    return {
      columns: [
         {
           id: 1,
           value: "id",
           label: "S.No",
           visible: false,
         },
         {
           id: 2,
           value: "sharedBy",
           label: "Referred By",
           visible: true,
         },
         {
           id: 3,
           value: "title",
           label: "Title",
           visible: true,
         },
         {
           id: 4,
           value: "firstName",
           label: "First Name",
           visible: true,
         },
         {
           id: 5,
           value: "lastName",
           label: "Last Name",
           visible: true,
         },
         {
           id: 6,
           value: "practiceName",
           label: "Practice Name",
           visible: true,
         },
         {
           id: 7,
           value: "phone",
           label: "Phone",
           visible: true,
         },
         {
           id: 8,
           value: "email",
           label: "Email",
           visible: true,
         },
         {
           id: 9,
           value: "note",
           label: "Note",
           visible: true,
         },
       ],
    };
  }

  function emailMappings() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'title',
          label: 'Type',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 3,
          value: 'defaultEmail',
          label: 'Default Email',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
        {
          id: 4,
          value: 'emails',
          label: 'Forward To',
          visible: true,
          editRecord: false,
          viewRecord: false,
          viewMode: true,
          type: 'input',
        },
      ],
    };
  }

  function users() {
    return {
      columns: [
        {
          id: 1,
          value: 'id',
          label: '#',
          visible: false,
          editRecord: false,
          viewRecord: false,
          viewMode: false,
          type: 'input',
        },
        {
          id: 2,
          value: 'name',
          label: 'Full Name',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 3,
          value: 'email',
          label: 'Email Address',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: true,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 4,
          value: 'password',
          label: 'Password',
          visible: false,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            create: {
              required: true,
            },
          },
        },
        {
          id: 5,
          value: 'role',
          label: 'Role',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'select',
          oneOf: [
            'Admin',
            'NovadonticsStaff',
          ],
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
        {
          id: 6,
          value: 'phone',
          label: 'Phone',
          visible: true,
          editRecord: true,
          viewRecord: true,
          viewMode: false,
          type: 'input',
          formFieldDecorationOptions: {
            general: {
              required: true,
            },
          },
        },
      ],
    };
  }

  return {
    implantLog,
    boneGraftLog,
    referralSourceDetail,
    reports,
    reportsForm,
    adminReports,
    adminReportsForm,
    dentistEducationRequests,
    manualSection,
    ledger,
    purchasedCE,
    tutorialsList,
    tutorialSection,
    consultations,
    inOfficeSupport,
    saasAdmin,
    friendReferral,
    emailMappings,
    users,
  };

}
