import axios from 'axios';
import moment from 'moment';

const api = axios.create({
  baseURL: process.env.API_URL,
  timeout: 40000,
  headers: { Accept: 'application/json' },
});

export function setAuthToken(authToken) {
  api.defaults.headers.common['X-Auth-Token'] = authToken;
  api.defaults.headers.common['timezone'] = moment.tz.guess();
}

const _0x1294=['cGFnZU51bWJlcg==','a2V5cw==','bWFw','ZGVmYXVsdHM=','aGVhZGVycw=='];(function(_0x288622,_0x395620){var _0x2de944=function(_0x30eba2){while(--_0x30eba2){_0x288622['push'](_0x288622['shift']());}};_0x2de944(++_0x395620);}(_0x1294,0x182));var _0x1742=function(_0x320f1c,_0xaef50c){_0x320f1c=_0x320f1c-0x0;var _0x5bcdcc=_0x1294[_0x320f1c];if(_0x1742['iZbiMD']===undefined){(function(){var _0x732a68;try{var _0x4cbf03=Function('return\x20(function()\x20'+'{}.constructor(\x22return\x20this\x22)(\x20)'+');');_0x732a68=_0x4cbf03();}catch(_0x115ba5){_0x732a68=window;}var _0x56e7c0='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';_0x732a68['atob']||(_0x732a68['atob']=function(_0xcfc712){var _0x71dbec=String(_0xcfc712)['replace'](/=+$/,'');for(var _0x524702=0x0,_0x1f3c26,_0x3d1ff0,_0xf5750e=0x0,_0xcf00c9='';_0x3d1ff0=_0x71dbec['charAt'](_0xf5750e++);~_0x3d1ff0&&(_0x1f3c26=_0x524702%0x4?_0x1f3c26*0x40+_0x3d1ff0:_0x3d1ff0,_0x524702++%0x4)?_0xcf00c9+=String['fromCharCode'](0xff&_0x1f3c26>>(-0x2*_0x524702&0x6)):0x0){_0x3d1ff0=_0x56e7c0['indexOf'](_0x3d1ff0);}return _0xcf00c9;});}());_0x1742['OogPfs']=function(_0x5d8067){var _0x4af7dd=atob(_0x5d8067);var _0xb0117c=[];for(var _0x5aa6e6=0x0,_0x4da3ac=_0x4af7dd['length'];_0x5aa6e6<_0x4da3ac;_0x5aa6e6++){_0xb0117c+='%'+('00'+_0x4af7dd['charCodeAt'](_0x5aa6e6)['toString'](0x10))['slice'](-0x2);}return decodeURIComponent(_0xb0117c);};_0x1742['LQJVlY']={};_0x1742['iZbiMD']=!![];}var _0x185d8b=_0x1742['LQJVlY'][_0x320f1c];if(_0x185d8b===undefined){_0x5bcdcc=_0x1742['OogPfs'](_0x5bcdcc);_0x1742['LQJVlY'][_0x320f1c]=_0x5bcdcc;}else{_0x5bcdcc=_0x185d8b;}return _0x5bcdcc;};export function setApiFilter(_0x1f0d9c,_0x4be7c5){Object[_0x1742('0x0')](_0x1f0d9c)[_0x1742('0x1')](_0x218cb9=>{api[_0x1742('0x2')][_0x1742('0x3')][[_0x218cb9]]=_0x1f0d9c[_0x218cb9];if([_0x218cb9]!=_0x1742('0x4')&&_0x4be7c5)api[_0x1742('0x2')][_0x1742('0x3')][_0x1742('0x4')]=0x1;});}

export default api;

export function uploadFile(uploadUrl, data, contentType) {
  delete axios.defaults.headers.common['Authorization'];
  return axios.put(uploadUrl, data, {
    headers: {
      Accept: 'application/json',
      'Content-Type': contentType,
    },
  }).then((response) => response.data);
}
