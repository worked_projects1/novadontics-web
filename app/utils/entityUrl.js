
export default function entityUrl(name) {
    if (name === 'products') {
        return 'catalog';
    } else if (['officeInfo','workingHours', 'operatories', 'providers', 'staffMembers', 'nonWorkingDays', 'referralSources','referredTo','dentalCarrier', 'insuranceFee', 'preMadeClinicalNotes', 'saas', 'settings'].includes(name)) {
        return '';
    }
    return name;
}
