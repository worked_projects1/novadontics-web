import ReactGA from 'react-ga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOG_IN_SUCCESS, VERIFY_SESSION_SUCCESS } from 'blocks/session/constants';
import constants from 'blocks/records/constants';

export const CATEGORIES = {
  dentists: 'Dentists',
  users: 'Users',
  consultations: 'Consultations',
  products: 'Products',
  orders: 'Orders',
  session: 'Session',
};

export const ACTIONS = {
  create: 'CREATE',
  update: 'UPDATE',
  delete: 'DELETE',
};

const orderActions = constants('orders');
const productActions = constants('products');
const userActions = constants('users');
const consultationActions = constants('consultations');
const dentistActions = constants('dentists');

const trackingCode = process.env.NODE_ENV === 'development' ? 'UA-00000000' : (process.env.GA_TRACKING || 'UA-90422117-1');

ReactGA.initialize(trackingCode, { debug: false });

export function logPageView() {
  ReactGA.set({ page: window.location.pathname });
  ReactGA.pageview(window.location.pathname);
}

export function setAnalyticsSessionId(id) {
  ReactGA.ga('set', 'userId', id);
}

export function setAnalyticsEvent(category, action, label) {
  ReactGA.event({
    category,
    action,
    label,
  });
}

export const analyticsMiddleware = store => next => action => {
  const result = next(action);
  analyticsReducer(action);
  return result;
};

const analyticsReducer = action => {
  switch (action.type) {
    // general pageview
    case LOCATION_CHANGE:
      logPageView();
      return;
    // session stuff
    case LOG_IN_SUCCESS:
    case VERIFY_SESSION_SUCCESS:
      setAnalyticsSessionId(action.user.name);
      setAnalyticsEvent(CATEGORIES.session, ACTIONS.create, `${action.user.name} logged in.`);
      return;
    // record updates
    case orderActions.UPDATE_RECORD:
      setAnalyticsEvent(CATEGORIES.orders, ACTIONS.update, 'Updated order.');
      return;
    case userActions.UPDATE_RECORD:
      setAnalyticsEvent(CATEGORIES.users, ACTIONS.update, 'Updated web app user.');
      return;
    case productActions.UPDATE_RECORD:
      setAnalyticsEvent(CATEGORIES.products, ACTIONS.update, 'Updated product.');
      return;
    case dentistActions.UPDATE_RECORD:
      setAnalyticsEvent(CATEGORIES.dentists, ACTIONS.update, 'Updated dentist user.');
      return;
    case consultationActions.UPDATE_RECORD:
      setAnalyticsEvent(CATEGORIES.consultations, ACTIONS.update, 'Updated consultation.');
      return;
    // record creation
    case userActions.CREATE_RECORD:
      setAnalyticsEvent(CATEGORIES.users, ACTIONS.create, 'Created web app user.');
      return;
    case productActions.CREATE_RECORD:
      setAnalyticsEvent(CATEGORIES.products, ACTIONS.create, 'Created product.');
      return;
    case dentistActions.CREATE_RECORD:
      setAnalyticsEvent(CATEGORIES.dentists, ACTIONS.create, 'Created dentist user.');
      return;
    // record deletion
    case userActions.DELETE_RECORD:
      setAnalyticsEvent(CATEGORIES.users, ACTIONS.delete, 'Deleted web app user.');
      return;
    case productActions.DELETE_RECORD:
      setAnalyticsEvent(CATEGORIES.products, ACTIONS.delete, 'Deleted product.');
      return;
    case dentistActions.DELETE_RECORD:
      setAnalyticsEvent(CATEGORIES.dentists, ACTIONS.delete, 'Deleted dentist user.');
      return;
    default:
      return;
  }
};

