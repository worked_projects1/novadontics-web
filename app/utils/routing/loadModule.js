export default function loadModule(args, cb) {
  return (componentModule) => {
    cb(null, args ? componentModule.default.apply(undefined, args) : componentModule.default);
  };
}
