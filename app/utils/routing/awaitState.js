export default function awaitState(store, condition, callback, timeout = 6000) {
  if (condition(store.getState())) {
    callback(store.getState());
    return;
  }

  const timeoutId = setTimeout(handleTimeout, timeout);

  const unsubscribe = store.subscribe(handleChange);
  
  function handleChange() {
    if (condition(store.getState())) {
      clearTimeout(timeoutId);
      unsubscribe();
      callback(store.getState());
    }
  }
  

  function handleTimeout() {
    unsubscribe();
    callback(null);
  }
}
