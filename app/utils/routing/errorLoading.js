export default function errorLoading(err) {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
}
