import errorLoading from './errorLoading';
import records from 'blocks/records';
import { name } from 'utils/tools'; 
const injected = {};

export default (injectReducer, injectSagas, store) => ({ path, pageName, require, route, multiBlocks, container, childRoutes, data }) => ({
  path,
  pageName,
  getComponent(nextState, cb) {
    if (container instanceof Function) {
      const containerProvider = container;
      const [containerName, ...blockNames] = require || {};
      const blocksPromise = route || blockNames.length > 2 || multiBlocks ? blockNames.map((blockName) => Promise.resolve(records(blockName))) :  blockNames.map((blockName) => System.import(`blocks/${blockName}/index`));
      const containerPromise = route ?  System.import(`../../module/index`) : System.import(`containers/${containerName}/index`);
      const promises = [containerPromise, ...blocksPromise];
      if(!route && blockNames.length < 2)
        promises.push(Promise.resolve(records(name)));
        
      Promise.all(promises).then(([container, ...blocks]) => { // eslint-disable-line no-shadow
        blocks.forEach((block) => {
          const { name, reducer, sagas } = block.default || block;

          if (reducer) {
            injectReducer(name, reducer);
          }

          if (sagas) {
            /**
             * The sagas are being injected every time the route is entered, but are not being destroyed during the LOCATION_CHANGE event.
             * This is temporary solution.
             */
            if (injected[name] === undefined) {
              injectSagas(sagas);
              injected[name] = true;
            }
          }
        });

        setTimeout(() => {
          try {
            const finalContainer = containerProvider.apply(container.default, blocks.map((block) => block.default || block));
            if (store && finalContainer && finalContainer.await) {
              finalContainer.await(store, () => {
                try {
                  cb(null, finalContainer);
                } catch (error) {
                  errorLoading(error);
                }
              });
            } else {
              cb(null, finalContainer);
            }
          } catch (error) {
            errorLoading(error);
          }
        });
      }).catch(errorLoading);
    } else {
      System.import(`containers/${container}/index`)
        .then((defaultContainer) => cb(null, defaultContainer.default))
        .catch(errorLoading);
    }
  },
  childRoutes,
  data,
});
