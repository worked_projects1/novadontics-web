

import api from 'utils/api';
import App from 'containers/App';
import records from 'blocks/records';
import routes from '../module/utils/routes';


export const name = 'appointment';

export function remotes(name, remotesBlock) {
    function loadRecords() {
        if(!(name == "insuranceFee")) {
          return api.get(`/${name}`).then((response) =>response.data).catch((error) => Promise.reject(error));
        } else {
            return api.get('/resource-categories/procedureCodeCategory').then((response) => response.data).catch((error) => Promise.reject(error));
        }
    }

    function loadRecord() {
        return api.get(`/${name}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function createRecord(record) {
        return api.post(`/${name}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function updateRecord(record) {
        return api.put(`/${name}/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function deleteRecord(id) {
        return api.delete(`/${name}/${id}`).catch((error) => Promise.reject(error));
    }

    function loadRecordsParams(record) {
        return api.get(`/${name}?${convertObjectToParams(record)}`).then((response) => response).catch((error) => Promise.reject(error));
    }


    return {
        loadRecords,
        loadRecord,
        createRecord,
        updateRecord,
        deleteRecord,
        loadRecordsParams,
        ...remotesBlock
    }


}

export const module = (simpleLazyLoadedRoute, pages) => {
    return {
        index: simpleLazyLoadedRoute({
            require: ['RecordsPage', name, 'officeInfo', 'workingHours', 'operatories', 'providers', 'staffMembers', 'nonWorkingDays', 'referralSources','referredTo', 'dentalCarrier', 'insuranceFee', 'preMadeClinicalNotes', 'saas', 'settings'],
            route: true,
            container: function RecordsPage(module, ...children) {
                return routes(name, pages, module, children, this);
            },
        }),
        routes: {
            component: App(pages), // eslint-disable-line new-cap
            childRoutes: pages,
        },
    }
}

export const moduleBlocks = () => records(name);

export const setupBlocks = () => records('setup');

export function toUpper(str) {
    return str && str
        .toLowerCase()
        .split(' ')
        .map(function (word) {
            return word[0].toUpperCase() + word.substr(1);
        })
        .join(' ') || str;
}


export function convertObjectToParams(obj) {
    var str = "";
    for (var key in obj) {
        if (str != "") {
            str += "&";
        }
        str += key + "=" + encodeURIComponent(obj[key]);
    }

    return str;
}

export function sortBy(sortColumn, sortOrder) {
    const lowerCase = (value) => typeof value === "string" ? value.toLowerCase() : value;
    const key = (row) => lowerCase ? isNaN(Date.parse(lowerCase(row[sortColumn]))) ? lowerCase(row[sortColumn]) : new Date(lowerCase(row[sortColumn])) : row[sortColumn];
    const result = (row1, row2) => ((row1 < row2) ? -1 : ((row1 > row2) ? 1 : 0)) * [-1, 1][Number(Boolean(sortOrder))];

    return function (a, b) {
        return result(key(a), key(b));
    }
}
export function findRadius(values) {
    var radius = (values * values) / (Math.PI * Math.PI);
    return radius;
}
export function findPercentage(values, total) {
    var percentage = (values / total) * 100;
    return percentage;
}
function sum(arr, key) {
    return arr.reduce((a, b) => a + (b[key] || 0), 0);
}
export function findOther(arr) {
    var other = arr && arr.filter(function (obj) {
        return obj.percent <= 4
    });
    var finaldata = arr && arr.filter(function (obj) {
        return obj.percent > 4;
    });
    var persum = sum(other, "percent");
    var ysum = sum(other, "y");

    if (persum >= 3) {
        var data = Object.assign({}, {
            name: "other",
            y: ysum,
            z: findRadius(ysum),
        })
        finaldata.push(data);
    }
    return finaldata;
}

export function iOS() {
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform);
}

export function stringToObjectConverter(value, delimiter) {

    const splitArr = value.split(delimiter);
    const result = splitArr.reduce((a, el, index) => {
        if (el.indexOf(':') > -1) {
            const ObjectArr = el.split(':');
            return Object.assign({}, a, { [ObjectArr[0]]: ObjectArr[1] })
        } else {
            return Object.assign({}, a, { key: el })
        }
    }, {});

    return result;
}


export function parseStructure(str = '') {
    return typeof str == 'string' && str.includes('★') ? JSON.parse(str.replace(/★/g, '"')) : str;
  }
