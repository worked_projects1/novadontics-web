import api from 'utils/api';

export function loadRecords() {
  return api.get('/implantData').then((response) => response).catch((error) => Promise.reject(error));
}

export function createImplantData(record) {
  return api.post(`/implantCreate`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateImplantData(record) {
  const { id } = record;
  return api.put(`/updateImplantData/${id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  updateImplantData,
  createImplantData
};
