import api from 'utils/api';


export function loadVendorInvoice(orderId, vendorId) {
  return api.get(`/vendorInvoice?orderId=${orderId}&vendorId=${vendorId}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function createVendorInvoice(record) {
  return api.post('/vendorInvoice', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateVendorInvoice(record) {
  const { id } = record;
  return api.put(`/vendorInvoice/${id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function deleteVendorInvoice(id) {
  return api.delete(`/vendorInvoice/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadOrderInvoice(orderId) {
  return api.get(`/getVendorInvoices/${orderId}`).then((response) => response.data).catch((error) => Promise.reject(error));
}