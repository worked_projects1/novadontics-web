import api from 'utils/api';

export function loadRecords() {
  return api.get('/orders').then((response) => {

    response.data.map((order) => ({
      id: order.id,
      status: order.status,
      dateCreated: order.dateCreated,
      items: order.items,
      dentist: order.dentist,
      dateFulfilled: order.dateFulfilled,
      approvedBy: order.approvedBy,
      address: order.address,
      paymentType: order.paymentType,
      checkNumber:order.checkNumber,
      allVendorsPaid : order.allVendorsPaid,
      shippingCharge: order.shippingCharge,
      tax : order.tax,
      creditCardFee: order.creditCardFee,
      invoice: order.invoice,
      amountPaid: order.amountPaid,
      totalAmountSaved: order.totalAmountSaved,
      vendorInvoiceNum: order.vendorInvoiceNum,
      paymentDate: order.paymentDate
    }));

      return response;
  });
}

function updateRecord(record) {
  const { id, status, paymentType, checkNumber, vendorId, orderId, invoice, amountPaid, vendorInvoiceNum, paymentDate, paymentCompleted, note } = record;
  const apiRecord = record.vendorId ? { orderId, vendorId, paymentType, checkNumber, invoice, amountPaid, vendorInvoiceNum, paymentDate } : record.notesForm ? { id, status, note } : { id, status, paymentType, checkNumber, paymentCompleted };
  const apiURL = record.vendorId ? `/orders/updateVendorPayment/${orderId}` : `/orders/${id}`;

  return api.put(apiURL, apiRecord).then((response) => {
    return api.get(`/orders/${record.vendorId ? orderId :id}`).then((response) => {
      const { data } = response;
      return data;
    });
  });
}

function deleteRecord(id) {
  return api.delete(`/orders/${id}`);
}


export function catalogOrders(){
  return api.get('/catalogOrders').then((response) => response.data).catch((error) => Promise.reject(error));
}

export function subscribeRecords(){
  return api.get('/getMySubscribedProducts').then((response) => response.data).catch((error) => Promise.reject(error));
}

export function deleteSubscribeHistory(id) {
  return api.put(`/unsubscribeProduct/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}


export default {
  loadRecords,
  updateRecord,
  deleteRecord,
};
