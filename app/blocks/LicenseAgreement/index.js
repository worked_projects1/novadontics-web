import api from 'utils/api';

export function updateRecord(record) {
    return api.put(`/updateAgreementAccepted`,record ).then((response) => response.data).catch((error) => Promise.reject(error));
  }

