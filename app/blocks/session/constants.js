/*
 *
 * Session constants
 *
 */

export const VERIFY_SESSION = 'ds/session/VERIFY_SESSION';
export const VERIFY_SESSION_SUCCESS = 'ds/session/VERIFY_SESSION_SUCCESS';
export const VERIFY_SESSION_ERROR = 'ds/session/VERIFY_SESSION_ERROR';

export const CREATE_PATIENT = 'ds/session/CREATE_PATIENT';

export const LOG_IN = 'ds/session/LOG_IN';
export const LOG_IN_SUCCESS = 'ds/session/LOG_IN_SUCCESS';
export const LOG_IN_ERROR = 'ds/session/LOG_IN_ERROR';

export const LOG_OUT = 'ds/session/LOG_OUT';
export const LOG_OUT_SUCCESS = 'ds/session/LOG_OUT_SUCCESS';
export const LOG_OUT_ERROR = 'ds/session/LOG_OUT_ERROR';

export const REQUEST_PASSWORD_RESET = 'ds/session/REQUEST_PASSWORD_RESET';
export const REQUEST_PASSWORD_RESET_SUCCESS = 'ds/session/REQUEST_PASSWORD_RESET_SUCCESS';
export const REQUEST_PASSWORD_RESET_ERROR = 'ds/session/REQUEST_PASSWORD_RESET_ERROR';

export const VERIFY_RESET_TOKEN = 'ds/session/VERIFY_RESET_TOKEN';
export const VERIFY_RESET_TOKEN_SUCCESS = 'ds/session/VERIFY_RESET_TOKEN_SUCCESS';
export const VERIFY_RESET_TOKEN_ERROR = 'ds/session/VERIFY_RESET_TOKEN_ERROR';

export const RESET_PASSWORD = 'ds/session/RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS = 'ds/session/RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_ERROR = 'ds/session/RESET_PASSWORD_ERROR';

export const SPLIT_BAR = 'ds/session/SPLIT_BAR';

