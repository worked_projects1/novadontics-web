/*
 *
 * Session reducer
 *
 */

import { fromJS, Map } from 'immutable';
import {
  VERIFY_SESSION_SUCCESS,
  VERIFY_SESSION_ERROR,
  CREATE_PATIENT,
  LOG_IN_SUCCESS,
  LOG_OUT_SUCCESS,
  LOG_IN_ERROR,
  LOG_OUT_ERROR,
  REQUEST_PASSWORD_RESET_SUCCESS,
  REQUEST_PASSWORD_RESET_ERROR,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  VERIFY_RESET_TOKEN_SUCCESS,
  VERIFY_RESET_TOKEN_ERROR,
  SPLIT_BAR
} from './constants';

const initialState = fromJS({ error: {}, success: {}, splitBar: false, version: '12.6.30.4',createPatient: false });

function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_PASSWORD_RESET_SUCCESS:
      return state.set('error', Map()).setIn(['success', 'requestPassword'], true);
    case RESET_PASSWORD_SUCCESS:
      return state.set('error', Map()).setIn(['success', 'resetPassword'], action.userType);
    case VERIFY_RESET_TOKEN_SUCCESS:
      return state.set('error', Map()).setIn(['success', 'verifyResetRequest'], true);
    case VERIFY_SESSION_ERROR:
      return state
        .set('loggedIn', false)
        .setIn(['error', 'login'], action.error)
        .delete('user');
    case VERIFY_SESSION_SUCCESS:
    case LOG_IN_SUCCESS:     
      return state
        .set('loggedIn', true)
        .set('user', action.user)
        .deleteIn(['error', 'login']);
    case LOG_IN_ERROR:
      return state
        .set('loggedIn', false)
        .setIn(['error', 'login'], action.error);
    case LOG_OUT_ERROR:
    case LOG_OUT_SUCCESS:
      return state
        .set('loggedIn', false)
        .delete('user')
        .deleteIn(['error', 'login']);
    case REQUEST_PASSWORD_RESET_ERROR:
      return state
        .setIn(['error', 'requestPassword'], action.error);
    case RESET_PASSWORD_ERROR:
      return state
        .setIn(['error', 'resetPassword'], action.error);
    case VERIFY_RESET_TOKEN_ERROR:
      return state
        .setIn(['error', 'verifyResetRequest'], action.error);
    case SPLIT_BAR:
      return state
        .setIn(['splitBar'], !state.get('splitBar'));    
    case CREATE_PATIENT:
      return state 
        .setIn(['createPatient'], action.patient);
    default:
      return state;
  }
}

export default sessionReducer;
