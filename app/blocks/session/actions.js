/*
 *
 * Session actions
 *
 */

import {
  VERIFY_SESSION,
  VERIFY_SESSION_SUCCESS,
  VERIFY_SESSION_ERROR,
  CREATE_PATIENT,
  LOG_IN,
  LOG_IN_SUCCESS,
  LOG_IN_ERROR,
  LOG_OUT,
  LOG_OUT_SUCCESS,
  LOG_OUT_ERROR,
  REQUEST_PASSWORD_RESET,
  REQUEST_PASSWORD_RESET_SUCCESS,
  REQUEST_PASSWORD_RESET_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  VERIFY_RESET_TOKEN,
  VERIFY_RESET_TOKEN_SUCCESS,
  VERIFY_RESET_TOKEN_ERROR,
  SPLIT_BAR
} from './constants';

export function verifySession(secret, access_token, access_token_type, platform) {
  return {
    type: VERIFY_SESSION,
    secret,
    access_token,
    access_token_type,
    platform
  };
}

export function verifySessionSuccess(user) {
  return {
    type: VERIFY_SESSION_SUCCESS,
    user,
  };
}

export function verifySessionError(error) {
  return {
    type: VERIFY_SESSION_ERROR,
    error,
  };
}

export function createPatient(patient) {
  return {
    type: CREATE_PATIENT,
    patient,
  };
}

export function logIn(identifier, secret, form) {
  return {
    type: LOG_IN,
    identifier,
    secret,
    form,
  };
}

export function logInSuccess(user) {
  return {
    type: LOG_IN_SUCCESS,
    user,
  };
}

export function logInError(error) {
  return {
    type: LOG_IN_ERROR,
    error,
  };
}

export function logOut() {
  return {
    type: LOG_OUT,
  };
}

export function logOutSuccess() {
  return {
    type: LOG_OUT_SUCCESS,
  };
}

export function logOutError(error) {
  return {
    type: LOG_OUT_ERROR,
    error,
  };
}

export function requestPasswordReset(email, form) {
  return {
    type: REQUEST_PASSWORD_RESET,
    email,
    form,
  };
}

export function requestPasswordResetSuccess() {
  return {
    type: REQUEST_PASSWORD_RESET_SUCCESS,
  };
}

export function requestPasswordResetError(error) {
  return {
    type: REQUEST_PASSWORD_RESET_ERROR,
    error,
  };
}

export function resetPassword(secret, token) {
  return {
    type: RESET_PASSWORD,
    secret,
    token,
  };
}

export function resetPasswordSuccess(userType) {
  return {
    type: RESET_PASSWORD_SUCCESS,
    userType,
  };
}

export function resetPasswordError(error) {
  return {
    type: RESET_PASSWORD_ERROR,
    error,
  };
}

export function verifyResetToken(token) {
  return {
    type: VERIFY_RESET_TOKEN,
    token,
  };
}

export function verifyResetTokenSuccess() {
  return {
    type: VERIFY_RESET_TOKEN_SUCCESS,
  };
}

export function verifyResetTokenError(error) {
  return {
    type: VERIFY_RESET_TOKEN_ERROR,
    error,
  };
}

export function splitBar() {
  return {
    type: SPLIT_BAR
  };
}
