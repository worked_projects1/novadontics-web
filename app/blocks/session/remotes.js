import api from 'utils/api';
import { DEFAULT_LOGIN_ERROR, WRONG_CREDENTIALS, DEFAULT_SESSION_TOKEN_ERROR, DEFAULT_UNEXPECTED_SESSION_ERROR, EXPIRED_TOKEN, NO_SUCH_USER } from 'utils/errors.js';
import {NOT_AUTHORIZED, ACCOUNT_EXPIRED} from "../../utils/errors";

export function verifySession(secret, access_token, platform) {
  return api.get('/session').then((response) => {
    const { data } = response;
    if (data.Anonymous) {
      return Promise.reject(DEFAULT_SESSION_TOKEN_ERROR);
    }
    const { Admin, Dentist, NovadonticsStaff, Receptionist } = data;
    if (Admin) {
      return {
        id: Admin.id,
        name: Admin.name,
        role: 'admin',
        version: Admin ? Admin.webAppVersion : null,
        secret,
        access_token,
        platform
      };
    } else if (Dentist || Receptionist) {
      return {
        id: Dentist ? Dentist.id : Receptionist.id,
        allowAllCECourses: Dentist ? Dentist.allowAllCECourses : Receptionist.allowAllCECourses,
        name: Dentist ? Dentist.name : Receptionist.name,
        practiceName: Dentist ? Dentist.practiceName : Receptionist.practiceName,
        firstName: Dentist ? Dentist.firstName : Receptionist.firstName,
        lastName: Dentist ? Dentist.lastName : Receptionist.lastName,
        role: 'dentist',
        secret,
        access_token,
        platform,
        practiceId: Dentist ? Dentist.practiceId : Receptionist.practiceId,
        version: Dentist ? Dentist.webAppVersion : Receptionist.webAppVersion,
        webModules: Dentist ? Dentist.webModules && Dentist.webModules.split(',') || [] : Receptionist.webModules && Receptionist.webModules.split(",") || [],
        appSquares: Dentist ? Dentist.appSquares && Dentist.appSquares.split(',') || [] : Receptionist.appSquares && Receptionist.appSquares.split(",") || [],
        ciiPrograms: Dentist ? Dentist.ciiPrograms && Dentist.ciiPrograms.split(',') || [] : Receptionist.ciiPrograms && Receptionist.ciiPrograms.split(",") || [],
        prime: Dentist ? Dentist.prime : Receptionist.prime,
        xvWebURL: Dentist ? Dentist.xvWebURL : Receptionist.xvWebURL,
        xvWebUserName: Dentist ? Dentist.xvWebUserName : Receptionist.xvWebUserName,
        xvWebPassword: Dentist ? Dentist.xvWebPassword : Receptionist.xvWebPassword,
        authorizeCustId: Dentist ? Dentist.authorizeCustId : Receptionist.authorizeCustId,
        doseSpotUserId: Dentist ? Dentist.doseSpotUserId : Receptionist.doseSpotUserId,
        email: Dentist ? Dentist.email : Receptionist.email,
        cartQuantity: Dentist ? Dentist.cartQuantity : Receptionist.cartQuantity,
        onlineRegistrationModule: Dentist ? Dentist.onlineRegistrationModule : Receptionist.onlineRegistrationModule,
        agreementAccepted: Dentist ? Dentist.agreementAccepted : Receptionist.agreementAccepted,
        ceInfo: Dentist ? Dentist.ceInfo : Receptionist.ceInfo
      };
    } else if (NovadonticsStaff) {
      return {
        id: NovadonticsStaff.id,
        name: NovadonticsStaff.name,
        role: 'novadonticsstaff'
      }
    }
    return Promise.reject(DEFAULT_UNEXPECTED_SESSION_ERROR);
  }).catch((error) => {
    const { response = {} } = error;
    const { status } = response;
    if (status === 502) {
      return Promise.reject(DEFAULT_UNEXPECTED_SESSION_ERROR);
    }
    return Promise.reject(DEFAULT_UNEXPECTED_SESSION_ERROR);
  });
}

export function logIn(identifier, secret) {
  return api.put('/session', { identifier, secret, application: 'web' }).then((response) => {
    const { data } = response;
    const user = data.user;
    user.secret = data.token;
    return { secret: data.token, user, identifier };
  }).catch((error) => {
    const { response = {} } = error;
    const { data = {} } = response;
    const { message } = data;
    if (message) {
      if (message === 'login.wrongCredentials') {
        return Promise.reject(WRONG_CREDENTIALS);
      }
      else if(message === 'login.notAuthorized') {
        return Promise.reject(NOT_AUTHORIZED);
      } else if(message === 'login.accountExpired') {
        return Promise.reject(ACCOUNT_EXPIRED);
      }
    }
    return Promise.reject(DEFAULT_LOGIN_ERROR);
  });
}

export function logOut() {
  return api.delete('/session');
}

export function verifyResetToken(secret) {
  return api.get(`/reset/${secret}`).then((response) => (response)).catch(() => (Promise.reject(EXPIRED_TOKEN)));
}

export function requestPasswordReset(email) {
  return api.put(`/forgot/${email}`, { email }).then((response) => (response)).catch(() => (Promise.reject(NO_SUCH_USER)));
}

export function resetPassword(secret, token) {
  return api.put(`/reset/${token}`, { secret }).then((response) => (response.data)).catch(() => (Promise.reject(EXPIRED_TOKEN)));
}

export function updatePassword(record){
  return api.put('/password', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function friendReferral(record){
  return api.post('/friendReferral', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

