import reducer from './reducer';

export default {
  name: 'session',
  reducer,
};
