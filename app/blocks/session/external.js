import axios from 'axios';

/**
 * 
 * @param {string} apiUrl 
 * @param {string} username 
 * @param {string} password 
 */
export const externalLogIn = (apiUrl, username, password) => {
    const record = { username, password, grant_type:'password',remember_me:true }
    return axios.post(`${apiUrl}/api/v2/token`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}
