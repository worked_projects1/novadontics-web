import { createSelector } from 'reselect';
import { Map } from 'immutable';

const selectSession = () => (state) => state ? state.get('session') : Map();

const selectLoggedIn = () => createSelector(
  selectSession(),
  (sessionState) => sessionState.get('loggedIn'),
);

const selectUser = () => createSelector(
  selectSession(),
  (sessionState) => sessionState.get('user'),
);

const selectError = () => createSelector(
  selectSession(),
  (sessionState) => sessionState.get('error') ? sessionState.get('error').toJS() : {},
);

const selectSuccess = () => createSelector(
  selectSession(),
  (sessionState) => sessionState.get('success') ? sessionState.get('success').toJS() : {},
);

const selectSplitBar = () => createSelector(
  selectSession(),
  (sessionState) => sessionState.get('splitBar') ? sessionState.get('splitBar') : false,
);

const selectCreatePatient = () => createSelector(
  selectSession(),
  (sessionState) => sessionState.get('createPatient') ? sessionState.get('createPatient') : false,
);

export {
  selectSession,
  selectLoggedIn,
  selectUser,
  selectError,
  selectSuccess,
  selectSplitBar,
  selectCreatePatient
};
