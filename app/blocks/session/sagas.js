import { delay } from 'redux-saga';
import { call, take, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { DEFAULT_SESSION_TOKEN_ERROR } from 'utils/errors.js';
import { externalLogIn } from './external';

import {
  LOG_IN,
  LOG_OUT,
  REQUEST_PASSWORD_RESET,
  RESET_PASSWORD,
  VERIFY_SESSION,
  VERIFY_RESET_TOKEN,
} from './constants';
import {
  verifySessionSuccess,
  verifySessionError,
  createPatient,
  logInSuccess,
  logInError,
  logOutSuccess,
  logOutError,
  requestPasswordResetSuccess,
  requestPasswordResetError,
  resetPasswordSuccess,
  resetPasswordError,
  verifyResetTokenSuccess,
  verifyResetTokenError,
} from './actions';
import {
  verifySession,
  logIn,
  logOut,
  verifyResetToken,
  requestPasswordReset,
  resetPassword,
} from './remotes';

import store2 from 'store2';

import { setAuthToken } from 'utils/api';
import { setToken } from 'blocks/patients/external';
import { iOS } from 'utils/tools';


const NATIVE_PLATFORM = iOS();

export function* verifyInitialSessionSaga() {
  const secret = store2.get('secret');
  const access_token = store2.get('access_token') || '';
  const access_token_type = store2.get('access_token_type') || '';
  const url = new URL(window.location.href);
  const token = url.searchParams.get("X-Auth-Token") ? url.searchParams.get("X-Auth-Token") : false;
  const platform = url.searchParams.get("platform") ? url.searchParams.get("platform") : store2.get('platform') || NATIVE_PLATFORM || 'web';
  
  if (secret || token) {
    setAuthToken(secret || token);
    setToken(access_token, access_token_type);
    try {
      store2.set('platform', platform);
      const user = yield call(verifySession, secret || token, access_token, platform);
      
      store2.set('secret', secret || token);
      yield put(verifySessionSuccess(user));
      if(user && user.role === "dentist" && user.appSquares ){
        const createPatients = user.appSquares.includes("createPatient")
        yield put(createPatient(createPatients));
      }
      if(token) {
        yield call(delay, 10000);
        yield put(push(process.env.PUBLIC_PATH || url));
      }   
    } catch (error) {
      setAuthToken(null);
      setToken(null);
      store2.remove('secret');
      store2.remove('platform');
      store2.remove('access_token');
      store2.remove('access_token_type');
      yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
      yield put(push(process.env.PUBLIC_PATH || '/'));
    }
  }
}

export function* verifySessionSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { secret, access_token, access_token_type, platform } = yield take(VERIFY_SESSION);
    if (secret) {
      setAuthToken(secret);
      setToken(access_token, access_token_type);
      try {
        const user = yield call(verifySession, secret, access_token, platform);
        if(user && user.role === "dentist" && user.appSquares ){
          const createPatients = user.appSquares.includes("createPatient")
          yield put(createPatient(createPatients));
        }
      
        yield put(verifySessionSuccess(user));
      } catch (error) {
        setAuthToken(null);
        setToken(null);
        store2.remove('secret');
        store2.remove('platform');
        store2.remove('access_token');
        store2.remove('access_token_type');
        yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
        yield put(push(process.env.PUBLIC_PATH || '/'));
      }
    } else {
      setAuthToken(null);
      setToken(null);
      store2.remove('secret');
      store2.remove('platform');
      store2.remove('access_token');
      store2.remove('access_token_type');
      yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
      yield put(push(process.env.PUBLIC_PATH || '/'));
    }
  }
}

export function* loginSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { identifier, secret, form } = yield take(LOG_IN);

    yield put(startSubmit(form));

    try {
      const result = yield call(logIn, identifier, secret);
      const { user = {} } = result || {};
      
      if(user.xvWebURL){
        /**
         * Calling External login
         */
        const { xvWebURL, xvWebUserName, xvWebPassword } = user;
        const external = yield call(externalLogIn, xvWebURL, xvWebUserName, xvWebPassword);
        store2.set('access_token', external.access_token);
        store2.set('access_token_type', external.token_type);
        setToken(external.access_token, external.access_token_type);
      }

      store2.set('secret', result.secret);
      setAuthToken(result.secret);
      yield put(logInSuccess(Object.assign({}, user)));
      
      const {appSquares = [] } = user || {};
      if(appSquares && appSquares.length > 0 && !appSquares.includes("appointments")){ 
        yield put(push(process.env.PUBLIC_PATH || '/catalog'));
      }
      else{
        yield put(push(process.env.PUBLIC_PATH || '/'));       
      }
    } catch (error) {
      store2.remove('secret');
      store2.remove('platform');
      store2.remove('access_token');
      store2.remove('access_token_type');
      setAuthToken(null);
      setToken(null);
      yield put(logInError(error));
    } finally {
      yield put(stopSubmit(form));
    }
  }
}

export function* logOutSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const watcher = yield take(LOG_OUT);

    if (watcher) {
      try {
        yield call(logOut);
        yield put(logOutSuccess());
      } catch (error) {
        yield put(logOutError(error));
      } finally {
        store2.remove('secret');
        store2.remove('platform');
        store2.remove('setup');
        store2.remove('access_token');
        store2.remove('access_token_type');
        setToken(null);
        yield put(push(process.env.PUBLIC_PATH || '/'));
        setAuthToken(null);
      }
    }
  }
}

export function* requestPasswordResetSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { email, form } = yield take(REQUEST_PASSWORD_RESET);

    yield put(startSubmit(form));

    try {
      const result = yield call(requestPasswordReset, email);
      yield put(requestPasswordResetSuccess());
      yield put(stopSubmit(form));
    } catch (error) {
      yield put(requestPasswordResetError(error));
      yield put(stopSubmit(form, { _error: error }));
    }
  }
}

export function* verifyResetTokenSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { token, form } = yield take(VERIFY_RESET_TOKEN);

    yield put(startSubmit(form));

    try {
      const result = yield call(verifyResetToken, token);
      yield put(verifyResetTokenSuccess());
      yield put(stopSubmit(form));
    } catch (error) {
      yield put(verifyResetTokenError(error));
      yield put(stopSubmit(form, { _error: error }));
    }
  }
}

export function* resetPasswordSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { secret, token, form } = yield take(RESET_PASSWORD);

    yield put(startSubmit(form));

    try {
      const result = yield call(resetPassword, secret, token) || {};
      yield put(resetPasswordSuccess(result.role ? result.role : 'user'));
      yield put(stopSubmit(form));
    } catch (error) {
      yield put(resetPasswordError(error));
      yield put(stopSubmit(form, { _error: error }));
    }
  }
}

export default [
  verifyInitialSessionSaga,
  verifySessionSaga,
  loginSaga,
  logOutSaga,
  requestPasswordResetSaga,
  verifyResetTokenSaga,
  resetPasswordSaga,
];
