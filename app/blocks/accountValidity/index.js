import { call, take, put, race } from 'redux-saga/effects';
import api from 'utils/api';
import accountsBlock from 'blocks/accounts';
import { replace } from 'react-router-redux';

export const EXTEND_ACCOUNT_VALIDITY = 'ds/EXTEND_ACCOUNT_VALIDITY';

export const extendAccountValidity = (record) => ({
  type: EXTEND_ACCOUNT_VALIDITY,
  record,
});

export const extendAccountValidityRemote = (record) => {
  const { accountType, amountCollected } = record;
  record.amountCollected = accountType != "Paid" && amountCollected && parseInt(amountCollected) != 0 ? 0 : parseInt(record.amountCollected);
  return api.put(`/accounts/${record.id}/extend`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function* extendAccountValiditySaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(EXTEND_ACCOUNT_VALIDITY),
    });
    const { record } = req;
    if (record) {
      try {
        const result = yield call(
          extendAccountValidityRemote,
          record
        );
        record.expirationDate = result.expirationDate;  
        yield put(accountsBlock.actions.updateRecordSuccess(record));
        yield put(replace(`/accounts/${record.id}`));
        
      } catch (error) {
        console.log(error);
      }
    }
  }
}       	

export const accountValiditySaga = [
  extendAccountValiditySaga,
];
