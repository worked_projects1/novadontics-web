import { call, take, put, race, select } from 'redux-saga/effects';
import { remotes } from 'utils/tools';
import api from 'utils/api';
import records from 'blocks/records';
import { selectUser } from 'blocks/session/selectors';

export const LOAD_REPORTS = 'ds/LOAD_REPORTS';
export const LOAD_COURSES = 'ds/LOAD_COURSES';

export const loadReports = () => ({
  type: LOAD_REPORTS
});

export const loadCourses = () => ({
  type: LOAD_COURSES
});

export const loadCoursesRemote = () =>
  api.get(`/resource-categories/courses`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadDoctorCoursesRemote = () =>
  api.get(`/courses/doctor`).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadReportsSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_REPORTS),
    });
    if (req) {
      try {
        const reportsBlock = yield call(records, 'reports');

        const user = yield select(selectUser());

        if(user.role == "admin") {
          const vendorRemotes = yield call(remotes, 'vendors');
          const vendorOptions = yield call(vendorRemotes.loadRecords);
          const emailMappingsRemotes = yield call(remotes, 'emailMappings');
          const emailMappingsOptions = yield call(emailMappingsRemotes.loadRecords);
          const doctorsRemotes = yield call(remotes, 'accounts');
          const doctorsOptions = yield call(doctorsRemotes.loadRecords);
          const filteredCons = emailMappingsOptions && emailMappingsOptions.filter(r => r.isConsultation && r.name === "overThePhone")[0];
          const recordsMetaData = {
              doctorsList: doctorsOptions && doctorsOptions.map(list => Object.assign({}, {label: list.name, value: list.id})) || [],
              vendorOptions: vendorOptions && vendorOptions.map(list => Object.assign({}, {label: list.name, value: list.id})) || [],
              consultantList: filteredCons && filteredCons.users && Object.keys(filteredCons.users[0]).length > 0 && filteredCons.users.map(list => Object.assign({}, {label: list.name, value: list.id})) || [],
          };

          yield put(reportsBlock.actions.updateRecordsMetaData(recordsMetaData));
        } else {
          const patientTypeRemotes = yield call(remotes, 'listOptionsReadAll/patientType');
          const providersRemotes = yield call(remotes, 'providers');
          const implantBrandRemotes = yield call(remotes, 'listOptionsReadAll/implantBrand');
          const referralSourcesRemotes = yield call(remotes, 'referralSources');
          const referredToRemotes = yield call(remotes, 'referredTo');
          const patientTypeList = yield call(patientTypeRemotes.loadRecords);
          const providersList = yield call(providersRemotes.loadRecords);
          const implantBrandList = yield call(implantBrandRemotes.loadRecords);
          const referralSourcesList = yield call(referralSourcesRemotes.loadRecords);
          const referredToList = yield call(referredToRemotes.loadRecords);
          const recordsMetaData = {
              referralSourcesList: referralSourcesList && referralSourcesList.map(list => Object.assign({}, {label: list.value, value: list.value})) || [],
              referredToList: referredToList && referredToList.map(list => Object.assign({}, {label: list.name, value: list.id})) || [],
              providersList: providersList && providersList.map(list => Object.assign({}, {label: list.name, value: list.id})) || [],
              patientTypeList: patientTypeList && patientTypeList.map(list => Object.assign({}, { label : list.listOptions, value : list.listOptions})),
              implantBrandList: implantBrandList && implantBrandList.map(list => Object.assign({}, { label : list.listOptions, value : list.listOptions})) || [] };

          yield put(reportsBlock.actions.updateRecordsMetaData(recordsMetaData));
        }
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* loadCoursesSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_COURSES),
    });
    if (req) {
      try {
        const courseBlock = yield call(records, 'courses');
        const result = yield call(loadDoctorCoursesRemote);
        const recordsMetaData = yield call(loadCoursesRemote);
        yield put(courseBlock.actions.loadRecordsSuccess(result, recordsMetaData));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export const reportsSaga = [
  loadReportsSaga,
  loadCoursesSaga
];
