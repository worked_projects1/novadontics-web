import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import patientsRemotes from 'blocks/patients/remotes'; 
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import moment from 'moment';

export const LOAD_ENDO_CHART = 'ds/LOAD_ENDO_CHART';

export const UPDATE_ENDO_CHART = 'ds/UPDATE_ENDO_CHART';

export const loadEndoChart = (id, date) => ({
  type: LOAD_ENDO_CHART,
  id,
  date,
});

export const updateEndoChart = (record) => ({
    type: UPDATE_ENDO_CHART,
    record,
});

export const loadEndoChartDates = (id) =>
  api.get(`/getEndoChartEntryDates/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadEndoChartRemote = (id, date) =>
  api.get(`/getEndoChartData?id=${id}&date=${date}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateEndoChartRemote = (record) =>
  api.post(`/updateEndoChartData/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadEndoChartSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_ENDO_CHART),
    });
    const { id, date } = req;
    if (id && date) {
      try {
        const result = yield call(loadEndoChartRemote, id, date);
        const toothChartData = yield call(patientsRemotes.getToothChartData, id);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {endoChartData : result}, {toothChartData: toothChartData} )));
      } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}

export function* updateEndoChartSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_ENDO_CHART),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updateEndoChartRemote, record);
        const dateSelected = moment(record.dateSelected).format('YYYY-MM-DD')
        const result = yield call(patientsRemotes.getEndoChartData, record.patientId, dateSelected);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, {endoChartData : result} )));
      } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}


export const endoChartSaga = [
    loadEndoChartSaga,
    updateEndoChartSaga,
];
