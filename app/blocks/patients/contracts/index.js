import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import patientsRemotes from 'blocks/patients/remotes';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';

export const LOAD_CONTRACTS = 'ds/LOAD_CONTRACTS';
export const UPDATE_CONTRACTS = 'ds/UPDATE_CONTRACTS';

export const loadContracts = (id) => ({
  type: LOAD_CONTRACTS,
  id
});

export const updateContracts = (record) => ({
    type: UPDATE_CONTRACTS,
    record,
});


export const loadContractsRemote = (id) =>
  api.get(`/getDocuments/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateContractsRemote = (record) => 
    api.put(`/updateDocument/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadContractsSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_CONTRACTS),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadContractsRemote, id);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {contractsData : result} )));
      } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}

  export function* updateContractsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_CONTRACTS),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateContractsRemote, record);
          const result = yield call(patientsRemotes.getContractsData, record.id);
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.id}, {contractsData : result} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


export const contractsSaga = [
    loadContractsSaga,
    updateContractsSaga,
];
