import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';

export const UPDATE_XRAY = 'ds/UPDATE_XRAY';


export const updateXray = (record) => ({
    type: UPDATE_XRAY,
    record,
});

  
export const updateXrayRemote = (record) => 
    api.put(`/treatments/updateXray/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

  export function* updateXraySaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_XRAY),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateXrayRemote, record);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.id));
          const result = Object.assign({}, recordInStore.toJS());
          result.patient['xray'] = record.xray; 
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.id}, { patient: result['patient']} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


export const xraySaga = [
    updateXraySaga,
];
