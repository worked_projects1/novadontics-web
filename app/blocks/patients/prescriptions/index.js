import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { startSubmit, stopSubmit, reset } from 'redux-form/immutable';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';
import groupBy from 'utils/groupBy';
import { sortBy } from "lodash.sortby";

export const CREATE_PRESCRIPTION_RECORD = 'ds/CREATE_PRESCRIPTION_RECORD';


export const createPrescriptions = (record, form) => ({
    type: CREATE_PRESCRIPTION_RECORD,
    record,
    form
});


export const loadPrescriptionsRemote = () =>
    api.get(`/prescriptions`).then((response) => response.data).catch((error) => Promise.reject(error));


export const createPrescriptionsRemote = (record) =>
    api.post(`/prescriptions`, record).then((response) => response.data).catch((error) => Promise.reject(error));



export function* createPrescriptionsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(CREATE_PRESCRIPTION_RECORD),
        });
        const { record, form } = req;
        yield put(startSubmit(form));
        if (record) {
            try {
                yield call(createPrescriptionsRemote, record);
                const result = yield call(loadPrescriptionsRemote);
                yield put(patientsBlock.actions.updateRecordsMetaData(Object.assign({}, { prescriptions: result })));
                yield put(stopSubmit(form));
                yield put(reset(form));
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_CREATE_ERROR;
                yield put(patientsBlock.actions.createRecordError(errorMessage));
                yield put(stopSubmit(form, { _error: errorMessage }));
            }
        }
    }
}



export const prescriptionSaga = [
    createPrescriptionsSaga,
];
