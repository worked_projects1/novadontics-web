import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';

export const LOAD_MEASUREMENT_HISTORY = 'ds/LOAD_MEASUREMENT_HISTORY';


export const loadMeasurementHistory = (id) => ({
    type: LOAD_MEASUREMENT_HISTORY,
    id,
});

  
export const loadMeasurementHistoryRemote = (id) => 
    api.get(`/measurementHistory/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadMeasurementHistorySaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
        req: take(LOAD_MEASUREMENT_HISTORY),
        });
        const { id } = req;
        if (id) {
            try {
                const result = yield call(loadMeasurementHistoryRemote, id);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, { measurementHistory: result} )));
            } catch (error) {
                console.log(error)
            }
        }
    }
}


export const measurementHistorySaga = [
    loadMeasurementHistorySaga,
];
