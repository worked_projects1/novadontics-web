import { call, take, put, race, select} from 'redux-saga/effects';
import { replace } from 'react-router-redux';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { moduleBlocks } from 'utils/tools';
const delay = (ms) => new Promise(response => setTimeout(response, ms))
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_TREATMENT_PLAN_DETAILS = 'ds/LOAD_TREATMENT_PLAN_DETAILS';
export const CREATE_TREATMENT_PLAN_DETAILS = 'ds/CREATE_TREATMENT_PLAN_DETAILS';
export const UPDATE_TREATMENT_PLAN_DETAILS = 'ds/UPDATE_TREATMENT_PLAN_DETAILS';
export const DELETE_TREATMENT_PLAN_DETAILS = 'ds/DELETE_TREATMENT_PLAN_DETAILS';
export const CREATE_APPOINTMENT_DETAILS = 'ds/CREATE_APPOINTMENT_DETAILS';
export const CLEAR_APPOINTMENT_DETAILS = 'ds/CLEAR_APPOINTMENT_DETAILS';

export const loadTreatmentPlanDetails = (record) => ({
    type: LOAD_TREATMENT_PLAN_DETAILS,
    record,
});

export const createTreatmentPlanDetails = (record) => ({
    type: CREATE_TREATMENT_PLAN_DETAILS,
    record,
});

export const updateTreatmentPlanDetails = (record) => ({
    type: UPDATE_TREATMENT_PLAN_DETAILS,
    record,
});

export const deleteTreatmentPlanDetails = (record) => ({
    type: DELETE_TREATMENT_PLAN_DETAILS,
    record,
});

export const createAppointmentDetails = (record) => ({
    type: CREATE_APPOINTMENT_DETAILS,
    record,
});

export const clearAppointmentDetails = () => ({
  type: CLEAR_APPOINTMENT_DETAILS
});


export const loadTreatmentPlanDetailsRemote = (id) => 
  api.get(`/treatmentPlanDetails/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createTreatmentPlanDetailsRemote = (record) => 
  api.post(`/treatmentPlanDetails`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  
export const updateTreatmentPlanDetailsRemote = (record) => 
  api.put(`/treatmentPlanDetails/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deleteTreatmentPlanDetailsRemote = (id) => 
  api.delete(`/treatmentPlanDetails/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));


export function* loadTreatmentPlanDetailsSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_TREATMENT_PLAN_DETAILS),
    });
    const { record } = req;
    if (record) {
      try {
        const result = yield call(loadTreatmentPlanDetailsRemote, record);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record}, {treatmentPlanDetails: result})));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* createTreatmentPlanDetailsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CREATE_TREATMENT_PLAN_DETAILS),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(createTreatmentPlanDetailsRemote, record);
          const result = yield call(loadTreatmentPlanDetailsRemote, record.patientId);
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id : record.patientId}, {treatmentPlanDetails : result})));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && (error.response.data.error || error.response.data[0].message) ? error.response.data.error || error.response.data[0].message : DEFAULT_CREATE_ERROR;
            yield put(patientsBlock.actions.createRecordError(errorMessage));
        }
      }
    }
  }


  export function* updateTreatmentPlanDetailsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_TREATMENT_PLAN_DETAILS),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateTreatmentPlanDetailsRemote, record);
          yield delay(1500);
          const result = yield call(loadTreatmentPlanDetailsRemote, record.patientId);
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, {treatmentPlanDetails: result})));
        
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && (error.response.data.error || error.response.data[0].message) ? error.response.data.error || error.response.data[0].message : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


  export function* deleteTreatmentPlanDetailsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(DELETE_TREATMENT_PLAN_DETAILS),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(deleteTreatmentPlanDetailsRemote, record.id);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { treatmentPlanDetails: result['treatmentPlanDetails'].filter(c=> c.id != record.id, {})} )));
        } catch (error) {
            yield put(patientsBlock.actions.deleteRecordError(error));
        }
      }
    }
  }


  export function* createAppointmentDetailsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CREATE_APPOINTMENT_DETAILS),
      });
      const { record } = req;
      if (record) {
        try {
          let procedureObj = {"procedure" : [{tooth: record.toothNumber, fee: record.fee, procedureCode: record.cdcCode, procedureDescription: record.procedureName, status: record.status}]}
          let procedure = procedureObj && JSON.stringify(procedureObj).replace(/"/g,'★').replace(/\//g,"\\/");
          yield put(moduleBlocks().actions.loadExternalRecordSuccess(Object.assign({}, {...record}, {procedure: procedure, phoneNumber: record.phone, referredBy: record.referredBy, alerts: record.medicalCondition})));
          yield put(replace('/'));   
        } catch (error) {
            yield put(patientsBlock.actions.createRecordError(error));
        }
      }
    }
  }

  export function* clearAppointmentDetailsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CLEAR_APPOINTMENT_DETAILS),
      });
        try {
          yield put(moduleBlocks().actions.loadExternalRecordSuccess({}));
        } catch (error) {
            yield put(patientsBlock.actions.createRecordError(error));
        }
    }
  }

export const treatmentPlanDetailsSaga = [
    loadTreatmentPlanDetailsSaga,
    createTreatmentPlanDetailsSaga,
    updateTreatmentPlanDetailsSaga,
    deleteTreatmentPlanDetailsSaga,
    createAppointmentDetailsSaga,
    clearAppointmentDetailsSaga
];
