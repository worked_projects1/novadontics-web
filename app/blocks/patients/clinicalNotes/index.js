import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { startSubmit, stopSubmit, reset } from 'redux-form/immutable';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_CLINICAL_NOTES = 'ds/LOAD_CLINICAL_NOTES';
export const CREATE_CLINICAL_NOTES = 'ds/CREATE_CLINICAL_NOTES';
export const UPDATE_CLINICAL_NOTES = 'ds/UPDATE_CLINICAL_NOTES';
export const DELETE_CLINICAL_NOTES = 'ds/DELETE_CLINICAL_NOTES';
export const LOCK_CLINICAL_NOTES = 'ds/LOCK_CLINICAL_NOTES';

export const loadClinicalNotes = (id) => ({
    type: LOAD_CLINICAL_NOTES,
    id,
});

export const createClinicalNotes = (record, form) => ({
    type: CREATE_CLINICAL_NOTES,
    record,
    form
});

export const updateClinicalNotes = (record, form) => ({
    type: UPDATE_CLINICAL_NOTES,
    record,
    form
});

export const deleteClinicalNotes = (record) => ({
    type: DELETE_CLINICAL_NOTES,
    record,
});

export const lockClinicalNotes = (record) => ({
  type: LOCK_CLINICAL_NOTES,
  record,
});


export const loadClinicalNotesRemote = (id) => 
  api.get(`/clinicalNotes/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createClinicalNotesRemote = (record) => 
  api.post(`/clinicalNotes/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  
export const updateClinicalNotesRemote = (record) => 
  api.put(`/clinicalNotes/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deleteClinicalNotesRemote = (id) => 
  api.delete(`/clinicalNotes/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const lockClinicalNotesRemote = (id) => 
  api.put(`/updateLock/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));  


export function* loadClinicalNotesSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_CLINICAL_NOTES),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadClinicalNotesRemote, id);  
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {clinicalNotesData: result})));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* createClinicalNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CREATE_CLINICAL_NOTES),
      });
      const { record, form } = req;
      yield put(startSubmit(form));
      if (record) {
        try {
          yield call(createClinicalNotesRemote, record);
          const result = yield call(loadClinicalNotesRemote, record.patientId);  
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id : record.patientId}, {clinicalNotesData : result})));
          yield put(stopSubmit(form));
          yield put(reset(form));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_CREATE_ERROR;
            yield put(patientsBlock.actions.createRecordError(errorMessage));
            yield put(stopSubmit(form, { _error: errorMessage }));
        }
      }
    }
  }


  export function* updateClinicalNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_CLINICAL_NOTES),
      });
      const { record, form } = req;
      yield put(startSubmit(form));
      if (record) {
        try {
          yield call(updateClinicalNotesRemote, record);
          const result = yield call(loadClinicalNotesRemote, record.patientId);  
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id : record.patientId}, {clinicalNotesData : result})));
          yield put(stopSubmit(form));
          yield put(reset(form));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
            yield put(stopSubmit(form, { _error: errorMessage }));
        }
      }
    }
  }


  export function* deleteClinicalNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(DELETE_CLINICAL_NOTES),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(deleteClinicalNotesRemote, record.id);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { clinicalNotesData: result['clinicalNotesData'].filter(c=> c.id != record.id, {})} )));
        } catch (error) {
            yield put(patientsBlock.actions.deleteRecordError(error));
        }
      }
    }
  }

  export function* lockClinicalNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(LOCK_CLINICAL_NOTES),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(lockClinicalNotesRemote, record.id);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { clinicalNotesData: result['clinicalNotesData'].map(c=> Object.assign({}, c, {lock: c.id === record.id ? true : c.lock}))} )));
        } catch (error) {
            yield put(patientsBlock.actions.deleteRecordError(error));
        }
      }
    }
  }

export const clinicalNotesSaga = [
    loadClinicalNotesSaga,
    createClinicalNotesSaga,
    updateClinicalNotesSaga,
    deleteClinicalNotesSaga,
    lockClinicalNotesSaga
];
