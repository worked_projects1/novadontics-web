import records from 'blocks/records';

import remotes from './remotes';

import external from './external';

export default records('patients', Object.assign({}, remotes, external));
