import api from 'utils/api';
import { convertObjectToParams } from 'utils/tools';

export function loadRecords() {
  return api.get(`/readPatients?shared=true`).then((response) => {
    const { data } = response;
    return data;
  });
}

export function loadRecord(id) {
  return api.get(`/patients/${id}`).then((response) => {
    const { data } = response;
    return data;
  });
}

export function inviteResend(id) {
  return api.get(`/resendLink?patientId=${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getDoseSpotUrl(id) {
  return api.get(`/getDoseSpotUIUrl/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getDoseSpotDetails() {
  return api.get(`/getDoseSpotNotificationCount`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function createPatientInvite(record) {
  return api.post('/onlinePatientCreate', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function sendNotification(record) {
  return api.put(`/sendConsentFormLink?patientId=${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function sendInstructions(record) {
  return api.put(`/sendInstructions?patientId=${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadPatientTypeStatus() {
  return api.get('/patientTypeStatus').then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadReferralSource(params) {
  return api.get(`/viewReferralSource?${convertObjectToParams(params)}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadReferralTo(params) {
  return api.get(`/viewReferredTo?${convertObjectToParams(params)}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post(`/treatments`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  const { id } = record;
  return api.put(`/treatments/${id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/treatments/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export async function getToothChartData(id){
  return await api.get(`/getToothConditions/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export async function getPerioChartData(id, date){
  return await api.get(`/getPerioChartData?id=${id}&date=${date}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export async function getEndoChartData(id, date){
  return await api.get(`/getEndoChartData?id=${id}&date=${date}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export async function getContractsData(id){
  return await api.get(`/getDocuments/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

function patientList(){
  return api.get(`/readPatients`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  loadRecord,
  createRecord,
  updateRecord,
  deleteRecord,
  getToothChartData,
  getPerioChartData,
  getEndoChartData,
  getContractsData,
  patientList
};
