import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import patientsRemotes from 'blocks/patients/remotes'; 
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import moment from 'moment';

export const LOAD_PERIO_CHART = 'ds/LOAD_PERIO_CHART';

export const UPDATE_PERIO_CHART = 'ds/UPDATE_PERIO_CHART';

export const loadPerioChart = (id, date) => ({
  type: LOAD_PERIO_CHART,
  id,
  date,
});

export const updatePerioChart = (record) => ({
    type: UPDATE_PERIO_CHART,
    record,
});

export const loadPerioChartDates = (id) =>
  api.get(`/getPerioChartEntryDates/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadPerioChartRemote = (id, date) =>
  api.get(`/getPerioChartData?id=${id}&date=${date}`).then((response) => response.data).catch((error) => Promise.reject(error));
  
export const updatePerioChartRemote = (record) => 
  api.post(`/treatments/updatePerioChartData/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadPerioChartSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_PERIO_CHART),
    });
    const { id, date } = req;
    if (id && date) {
      try {
        const result = yield call(loadPerioChartRemote, id, date);
        const toothChartData = yield call(patientsRemotes.getToothChartData, id);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {perioChartData : result}, {toothChartData: toothChartData} )));  
      } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}  

export function* updatePerioChartSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_PERIO_CHART),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updatePerioChartRemote, record);
        const dateSelected = moment(record.dateSelected).format('YYYY-MM-DD')
        const result = yield call(patientsRemotes.getPerioChartData, record.patientId, dateSelected);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, {perioChartData : result} )));  
      } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}


export const perioChartSaga = [
    loadPerioChartSaga,
    updatePerioChartSaga,
];
