import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';

export const UPDATE_CT_SCAN = 'ds/UPDATE_CT_SCAN';


export const updateCtScan = (record) => ({
    type: UPDATE_CT_SCAN,
    record,
});

  
export const updateCtScanRemote = (record) => 
    api.put(`/treatments/updateCtScan/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

  export function* updateCtScanSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_CT_SCAN),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateCtScanRemote, record);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.id));
          const result = Object.assign({}, recordInStore.toJS());
          result.patient['ctScanFile'] = record.ctScanFile; 
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.id}, { patient: result['patient']} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


export const ctScanSaga = [
    updateCtScanSaga,
];
