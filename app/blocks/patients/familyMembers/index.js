import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_FAMILY_MEMBERS = 'ds/LOAD_FAMILY_MEMBERS';
export const CREATE_FAMILY_MEMBERS = 'ds/CREATE_FAMILY_MEMBERS';
export const UPDATE_FAMILY_MEMBERS = 'ds/UPDATE_FAMILY_MEMBERS';
export const DELETE_FAMILY_MEMBERS = 'ds/DELETE_FAMILY_MEMBERS';

export const loadFamilyMembers = (id) => ({
    type: LOAD_FAMILY_MEMBERS,
    id,
});

export const createFamilyMembers = (record, form) => ({
    type: CREATE_FAMILY_MEMBERS,
    record,
    form
});

export const updateFamilyMembers = (record, form) => ({
    type: UPDATE_FAMILY_MEMBERS,
    record,
    form
});


export const deleteFamilyMembers = (record) => ({
    type: DELETE_FAMILY_MEMBERS,
    record,
});


export const loadFamilyMembersRemote = (id) =>
    api.get(`/familyMember/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createFamilyMembersRemote = (record) =>
    api.post(`/familyMember`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateFamilyMembersRemote = (record) =>
    api.put(`/updateGuarantor`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deleteFamilyMembersRemote = (record) =>
    api.delete(`/familyMember?familyId=${record.familyId}&patientId=${record.patientId}`).then((response) => response.data).catch((error) => Promise.reject(error));


export function* loadFamilyMembersSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_FAMILY_MEMBERS),
        });
        const { id } = req;
        if (id) {
            try {
                const result = yield call(loadFamilyMembersRemote, id);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: id }, { familyMembersData: result })));
            } catch (error) {
                console.log(error);
            }
        }
    }
}

export function* createFamilyMembersSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(CREATE_FAMILY_MEMBERS),
        });
        const { record, form } = req;
        if (record) {
            yield put(startSubmit(form));
            try {
                yield call(createFamilyMembersRemote, record);
                const result = yield call(loadFamilyMembersRemote, record.id);
                if(result){
                    yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.id }, { familyMembersData: result })));
                    yield put(stopSubmit(form));
                } else {
                    yield put(stopSubmit(form, { _error: DEFAULT_CREATE_ERROR }));
                }
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_CREATE_ERROR;
                yield put(stopSubmit(form, { _error: errorMessage }));
            }
        }
    }
}


export function* updateFamilyMembersSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(UPDATE_FAMILY_MEMBERS),
        });
        const { record, form } = req;
        if (record) {
            yield put(startSubmit(form));
            try {
                yield call(updateFamilyMembersRemote, record);
                const result = yield call(loadFamilyMembersRemote, record.id);
                if (result) {
                    yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.id }, { familyMembersData: result })));
                    yield put(stopSubmit(form));
                } else {
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_ERROR }));
                }
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
                yield put(stopSubmit(form, { _error: errorMessage }));
            }
        }
    }
}


export function* deleteFamilyMembersSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(DELETE_FAMILY_MEMBERS),
        });
        const { record } = req;
        if (record) {
            try {
                yield call(deleteFamilyMembersRemote, record);
                const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.id));
                const result = Object.assign({}, recordInStore.toJS());
                const { familyMembersData = {} } = result;
                const { family = {}, familyMembers = [] } =  familyMembersData;
                const filteredMembers = familyMembers.filter(c => c.patientId != record.patientId);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.id }, { familyMembersData: Object.assign({}, { family, familyMembers: filteredMembers })})));
            } catch (error) {
                yield put(patientsBlock.actions.deleteRecordError(error));
            }
        }
    }
}

export const familyMembersSaga = [
    loadFamilyMembersSaga,
    createFamilyMembersSaga,
    updateFamilyMembersSaga,
    deleteFamilyMembersSaga,
];
