import { call, take, put, race, select } from 'redux-saga/effects';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';
import { convertObjectToParams } from 'utils/tools';
import axios from 'axios';

export const GET_TOKEN_APTERYX = 'ds/GET_TOKEN_APTERYX';
export const LOAD_APTERYX = 'ds/LOAD_APTERYX';


export const getTokenApteryx = (record) => ({
    type: GET_TOKEN_APTERYX,
    record,
});

export const loadApteryx = (record) => ({
    type: LOAD_APTERYX,
    record,
});


export const getTokenApteryxRemote = (record) =>
    axios.post(`https://Novadontics.xvweb.net/api/v2/token`, record).then((response) => response.data).catch((error) => Promise.reject(error));


export const loadApteryxRemote = (record) =>
    axios.get(`https://novadontics.xvweb.net/#interactive?${convertObjectToParams(record)}`).then((response) => response.data).catch((error) => Promise.reject(error));


export function* getTokenApteryxSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(GET_TOKEN_APTERYX),
        });
        const { record } = req;
        if (record) {
            try {
                const result = yield call(getTokenApteryxRemote, record);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.patientId }, { apteryxData: result })));
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_CREATE_ERROR;
                yield put(patientsBlock.actions.createRecordError(errorMessage));
            }
        }
    }
}

export function* loadApteryxSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_APTERYX),
        });
        const { record } = req;
        if (record) {
            try {
                const result = yield call(loadApteryxRemote, record);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.patientId }, { apteryxData: result })));
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_CREATE_ERROR;
                yield put(patientsBlock.actions.createRecordError(errorMessage));
            }
        }
    }
}


export const apteryxSaga = [
    getTokenApteryxSaga,
    loadApteryxSaga,
];
