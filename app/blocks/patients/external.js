import axios from 'axios';
import { convertObjectToParams } from 'utils/tools';



const externalApi = axios.create({
    baseURL: '',
    timeout: 40000,
    headers: { Accept: 'application/json' },
});

export function setToken(token, type) {
    externalApi.defaults.headers.common['Authorization'] = `${type} ${token}`;
}

/**
 * 
 * @param {string} url 
 * @param {object} params
 */
export const loadExternalRecords = (url, params) => 
    externalApi.get(`${url}/api/v2/patient?${convertObjectToParams(params)}`).then((response) => response.data && response.data.Records || response.data).catch((error) => Promise.reject(error));




/**
 * 
 * @param {string} apiUrl 
 * @param {string} username 
 * @param {string} password 
 */
export const updateExternalRecord = (url, record) => 
    externalApi.post(`${url}/api/v2/patient`, record).then((response) => response.data).catch((error) => Promise.reject(error));



export default {
    loadExternalRecords,
    updateExternalRecord
}