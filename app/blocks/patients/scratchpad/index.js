import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';

export const UPDATE_SCRATCH_PAD = 'ds/UPDATE_SCRATCH_PAD';


export const updateScratchPad = (record) => ({
    type: UPDATE_SCRATCH_PAD,
    record,
});

  
export const updateScratchPadRemote = (record) => 
    api.put(`/treatments/updateScratchPad/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

  export function* updateScratchPadSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_SCRATCH_PAD),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateScratchPadRemote, record);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.id));
          const result = Object.assign({}, recordInStore.toJS());
          result.patient['scratchPad'] = record.scratchPad;
          result.patient['scratchPadText'] = record.scratchPadText; 
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.id}, { patient: result['patient']} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


export const scratchPadSaga = [
    updateScratchPadSaga,
];
