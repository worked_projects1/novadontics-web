import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_STAFF_NOTES = 'ds/LOAD_STAFF_NOTES';
export const CREATE_STAFF_NOTES = 'ds/CREATE_STAFF_NOTES';
export const UPDATE_STAFF_NOTES = 'ds/UPDATE_STAFF_NOTES';
export const DELETE_STAFF_NOTES = 'ds/DELETE_STAFF_NOTES';

export const loadStaffNotes = (id) => ({
    type: LOAD_STAFF_NOTES,
    id,
});

export const createStaffNotes = (record) => ({
    type: CREATE_STAFF_NOTES,
    record,
});

export const updateStaffNotes = (record) => ({
    type: UPDATE_STAFF_NOTES,
    record,
});

export const deleteStaffNotes = (record) => ({
    type: DELETE_STAFF_NOTES,
    record,
});


export const loadStaffNotesRemote = (id) =>
  api.get(`/staffNotes?patientId=${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createStaffNotesRemote = (record) =>
  api.post(`/staffNotes`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateStaffNotesRemote = (record) =>
  api.put(`/staffNotes/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deleteStaffNotesRemote = (id) =>
  api.delete(`/staffNotes/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));


export function* loadStaffNotesSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_STAFF_NOTES),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadStaffNotesRemote, id);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {staffNotesData: result})));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* createStaffNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CREATE_STAFF_NOTES),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(createStaffNotesRemote, record);
          const result = yield call(loadStaffNotesRemote, record.patientId);
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id : record.patientId}, {staffNotesData : result})));
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && (error.response.data.error || error.response.data[0].message) ? error.response.data.error || error.response.data[0].message : DEFAULT_CREATE_ERROR;
          yield put(patientsBlock.actions.createRecordError(errorMessage));
        }
      }
    }
  }


  export function* updateStaffNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_STAFF_NOTES),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateStaffNotesRemote, record);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { staffNotesData: result['staffNotesData'].map(c=> {  c.id === record.id ? c = record  :  null; return c})} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


  export function* deleteStaffNotesSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(DELETE_STAFF_NOTES),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(deleteStaffNotesRemote, record.id);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { staffNotesData: result['staffNotesData'].filter(c=> c.id != record.id, {})} )));
        } catch (error) {
            yield put(patientsBlock.actions.deleteRecordError(error));
        }
      }
    }
  }


export const staffNotesSaga = [
    loadStaffNotesSaga,
    createStaffNotesSaga,
    deleteStaffNotesSaga,
    updateStaffNotesSaga
];
