import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import { replace } from 'react-router-redux';

export const LOAD_APPOINTMENT = 'ds/LOAD_APPOINTMENT';
export const UPDATE_RECALL_PERIOD = 'ds/UPDATE_RECALL_PERIOD';

export const loadAppointment = (id) => ({
  type: LOAD_APPOINTMENT,
  id,
});

export const updateRecallPeriod = (record) => ({
  type: UPDATE_RECALL_PERIOD,
  record,
});


export const loadAppointmentRemote = (id) =>
  api.get(`/appointment/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateRecallPeriodRemote = (record) =>
  api.put(`/treatments/recallPeriod/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadAppointmentSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_APPOINTMENT),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadAppointmentRemote, id);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: id }, { appointmentData: result })));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* updateRecallPeriodSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_RECALL_PERIOD),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updateRecallPeriodRemote, record);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.id }, {recallPeriod : record.recallPeriod} )));
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}



export const appointmentSaga = [
  loadAppointmentSaga,
  updateRecallPeriodSaga,
];
