import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_SHARED_PATIENT = 'ds/LOAD_SHARED_PATIENT';
export const CREATE_SHARED_PATIENT = 'ds/CREATE_SHARED_PATIENT';
export const UPDATE_SHARED_PATIENT = 'ds/UPDATE_SHARED_PATIENT';
export const DELETE_SHARED_PATIENT = 'ds/DELETE_SHARED_PATIENT';
export const LOAD_SHARED_ADMIN_PATIENT = 'ds/LOAD_SHARED_ADMIN_PATIENT';
export const UPDATE_SHARED_ADMIN_PATIENT = 'ds/UPDATE_SHARED_ADMIN_PATIENT';

export const loadSharedPatient = (id) => ({
    type: LOAD_SHARED_PATIENT,
    id,
});

export const createSharedPatient = (record) => ({
    type: CREATE_SHARED_PATIENT,
    record,
});

export const updateSharedPatient = (record) => ({
    type: UPDATE_SHARED_PATIENT,
    record,
});

export const deleteSharedPatient = (record) => ({
    type: DELETE_SHARED_PATIENT,
    record,
});

export const loadSharedAdminPatient = (id) => ({
  type: LOAD_SHARED_ADMIN_PATIENT,
  id,
});

export const updateSharedAdminPatient = (record) => ({
  type: UPDATE_SHARED_ADMIN_PATIENT,
  record,
});


export const loadSharedPatientRemote = (id) => 
  api.get(`/sharedPatient/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createSharedPatientRemote = (record) => 
  api.post(`/sharedPatient`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  
export const updateSharedPatientRemote = (record) => 
  api.put(`/sharedPatient/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deleteSharedPatientRemote = (id) => 
  api.delete(`/sharedPatient/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadSharedAdminPatientRemote = (id) => 
  api.get(`/shareToAdmin/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateSharedAdminPatientRemote = (record) => 
  api.put(`/shareToAdmin/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));



export function* loadSharedPatientSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_SHARED_PATIENT),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadSharedPatientRemote, id);  
        const sharedAdmin = yield call(loadSharedAdminPatientRemote, id);  
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {sharedPatientData: result, sharedAdminPatientData: sharedAdmin[0]})));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* createSharedPatientSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CREATE_SHARED_PATIENT),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(createSharedPatientRemote, record);
          const result = yield call(loadSharedPatientRemote, record.patientId);  
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id : record.patientId}, {sharedPatientData : result})));
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && (error.response.data.error || error.response.data[0].message) ? error.response.data.error || error.response.data[0].message : DEFAULT_CREATE_ERROR;
          yield put(patientsBlock.actions.createRecordError(errorMessage));
        }
      }
    }
  }


  export function* updateSharedPatientSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_SHARED_PATIENT),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateSharedPatientRemote, record);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { sharedPatientData: result['sharedPatientData'].map(c=> {  c.id === record.id ? c = record  :  null; return c})} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


  export function* deleteSharedPatientSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(DELETE_SHARED_PATIENT),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(deleteSharedPatientRemote, record.id);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { sharedPatientData: result['sharedPatientData'].filter(c=> c.id != record.id, {})} )));
        } catch (error) {
            yield put(patientsBlock.actions.deleteRecordError(error));
        }
      }
    }
  }

  export function* loadSharedAdminPatientSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(LOAD_SHARED_ADMIN_PATIENT),
      });
      const { id } = req;
      if (id) {
        try {
          const result = yield call(loadSharedAdminPatientRemote, id);  
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {sharedAdminPatientData: result[0]})));
        } catch (error) {
          console.log(error);
        }
      }
    }
  }

  export function* updateSharedAdminPatientSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_SHARED_ADMIN_PATIENT),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateSharedAdminPatientRemote, record);
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { sharedAdminPatientData: record} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


export const sharedPatientSaga = [
    loadSharedPatientSaga,
    createSharedPatientSaga,
    updateSharedPatientSaga,
    deleteSharedPatientSaga,
    loadSharedAdminPatientSaga,
    updateSharedAdminPatientSaga
];
