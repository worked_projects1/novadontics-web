import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_SUBSCRIBER = 'ds/LOAD_SUBSCRIBER';
export const UPDATE_SUBSCRIBER = 'ds/UPDATE_SUBSCRIBER';

export const loadSubscriber = (id) => ({
    type: LOAD_SUBSCRIBER,
    id,
});

export const updateSubscriber = (record, form) => ({
    type: UPDATE_SUBSCRIBER,
    record,
    form
});


export const loadSubscriberRemote = (id) =>
    api.get(`/subscriber/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateSubscriberRemote = (record) =>
    api.put(`/subscriber/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));


export function* loadSubscriberSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_SUBSCRIBER),
        });
        const { id } = req;
        if (id) {
            try {
                const result = yield call(loadSubscriberRemote, id);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: id }, { subscriberData: result })));
            } catch (error) {
                console.log(error);
            }
        }
    }
}


export function* updateSubscriberSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(UPDATE_SUBSCRIBER),
        });
        const { record, form } = req;
        if (record) {
            yield put(startSubmit(form));
            try {
                yield call(updateSubscriberRemote, record);
                const result = yield call(loadSubscriberRemote, record.patientId);
                if (result) {
                    yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.patientId }, { subscriberData: result })));
                    yield put(stopSubmit(form));
                } else {
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_ERROR }));
                }
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
                yield put(stopSubmit(form, { _error: errorMessage }));
            }
        }
    }
}

export const subscriberSaga = [
    loadSubscriberSaga,
    updateSubscriberSaga,
];
