import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import patientsRemotes from 'blocks/patients/remotes'; 
import moment from 'moment';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';

export const LOAD_TOOTH_CHART = 'ds/LOAD_TOOTH_CHART';
export const UPDATE_TOOTH_CHART = 'ds/UPDATE_TOOTH_CHART';
export const UPDATE_TOOTH_CHART_DATE = 'ds/UPDATE_TOOTH_CHART_DATE';
export const UPDATE_EXAM_IMAGING = 'ds/UPDATE_EXAM_IMAGING';

export const loadToothChart = (id) => ({
  type: LOAD_TOOTH_CHART,
  id,
});

export const updateToothChart = (record) => ({
    type: UPDATE_TOOTH_CHART,
    record,
});

export const updateToothChartDate = (record) => ({
  type: UPDATE_TOOTH_CHART_DATE,
  record,
});

export const updateExamImaging = (record) => ({
  type: UPDATE_EXAM_IMAGING,
  record,
});

export const loadToothChartRemote = (id) => 
  api.get(`/getToothConditions/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateToothChartRemote = (record) => 
  api.post(`/treatments/updateToothChartData/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateToothChartDateRemote = (record) => 
  api.put(`/treatments/updateToothChartDate/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateExamImagingRemote = (record) => 
  api.put(`/updateExamImaging/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function createToothChart(record) {
  return api.post(`/treatmentToothChart/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function setEdentulousMaxilla(record) {
  return api.put(`/setEdentulousMaxilla/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function unsetEdentulousMaxilla(record) {
  return api.put(`/unsetEdentulousMaxilla/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function setEdentulousMandible(record) {
  return api.put(`/setEdentulousMandible/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function unsetEdentulousMandible(record) {
  return api.put(`/unsetEdentulousMandible/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function setAllPrimary(record) {
  return api.put(`/setAllPrimary/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function unsetAllPrimary(record) {
  return api.put(`/unsetAllPrimary/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function setMissingTooth(record) {
  return api.put(`/setMissingTooth/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function unsetMissingTooth(record) {
  return api.put(`/unsetMissingTooth/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function setPrimaryTooth(record) {
  return api.put(`/setPrimaryTooth/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function unsetPrimaryTooth(record) {
  return api.put(`/unsetPrimaryTooth/${record.patientId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function* loadToothChartSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_TOOTH_CHART),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadToothChartRemote, id);
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, { toothChartData: result} )));  
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* updateToothChartSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_TOOTH_CHART),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updateToothChartRemote, record);
        const toothChartData = yield call(patientsRemotes.getToothChartData, record.patientId);
        const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
        const result = Object.assign({}, recordInStore.toJS());
        result.patient['toothChartUpdatedDate'] = moment().format('YYYY-MM-DD'); 
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { patient: result['patient']}, {toothChartData : toothChartData} )));  
      } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}


export function* updateToothChartDateSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_TOOTH_CHART_DATE),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updateToothChartDateRemote, record);
        const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
        const result = Object.assign({}, recordInStore.toJS());
        result.patient['toothChartUpdatedDate'] = record.toothChartUpdatedDate; 
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { patient: result['patient']} ))); 
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}

export function* updateExamImagingSaga() {
  while (true) {
    const { req } = yield race({
      req: take(UPDATE_EXAM_IMAGING),
    });
    const { record } = req;
    if (record) {      
      try {
        yield call(updateExamImagingRemote, record);
        const result = yield call(patientsRemotes.loadRecord, record.patientId);        
        const Update = Object.assign({},{id: record.patientId}, { patient: Object.assign({}, result['patient'], { examImaging: record.examImaging})} );

        yield put(patientsBlock.actions.updateRecordSuccess(Update)); 
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(patientsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}

export const toothChartSaga = [
    loadToothChartSaga,
    updateToothChartSaga,
    updateToothChartDateSaga,
    updateExamImagingSaga
];
