import { call, take, put, race, select} from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR } from 'utils/errors.js';

export const LOAD_TREATMENT_PLAN = 'ds/LOAD_TREATMENT_PLAN';
export const CREATE_TREATMENT_PLAN = 'ds/CREATE_TREATMENT_PLAN';
export const UPDATE_TREATMENT_PLAN = 'ds/UPDATE_TREATMENT_PLAN';
export const DELETE_TREATMENT_PLAN = 'ds/DELETE_TREATMENT_PLAN';

export const loadTreatmentPlan = (id) => ({
    type: LOAD_TREATMENT_PLAN,
    id,
});

export const createTreatmentPlan = (record) => ({
    type: CREATE_TREATMENT_PLAN,
    record,
});

export const updateTreatmentPlan = (record) => ({
    type: UPDATE_TREATMENT_PLAN,
    record,
});

export const deleteTreatmentPlan = (record) => ({
    type: DELETE_TREATMENT_PLAN,
    record,
});


export const loadTreatmentPlanRemote = (id) => 
  api.get(`/treatmentPlan/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createTreatmentPlanRemote = (record) => 
  api.post(`/treatmentPlan`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  
export const updateTreatmentPlanRemote = (record) => 
  api.put(`/treatmentPlan/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deleteTreatmentPlanRemote = (id) => 
  api.delete(`/treatmentPlan/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const getFeeDetails = (record) =>
  api.get(`/getFee?patientId=${record.patientId}&procedureCode=${record.procedureCode}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const getInsuranceFeeDetails = (record) =>
  api.get(`/getFee?patientId=${record.patientId}&insuranceCompanyId=${record.insuranceCompanyId}&procedureCode=${record.procedureCode}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const getTreatmentPlanProcedures = (id) =>
  api.get(`/getTreatmentPlanProcedures?patientId=${id}&forWhich=clinicalNotes`).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadTreatmentPlanSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_TREATMENT_PLAN),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadTreatmentPlanRemote, id);  
        yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {treatmentPlanData: result})));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* createTreatmentPlanSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(CREATE_TREATMENT_PLAN),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(createTreatmentPlanRemote, record);
          const result = yield call(loadTreatmentPlanRemote, record.patientId);  
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id : record.patientId}, {treatmentPlanData : result})));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_CREATE_ERROR;
            yield put(patientsBlock.actions.createRecordError(errorMessage));
        }
      }
    }
  }


  export function* updateTreatmentPlanSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(UPDATE_TREATMENT_PLAN),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(updateTreatmentPlanRemote, record);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { treatmentPlanData: result['treatmentPlanData'].map(c=> {  c.id === record.id ? c = record  :  null; return c})} )));
        } catch (error) {
            const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
            yield put(patientsBlock.actions.updateRecordError(errorMessage));
        }
      }
    }
  }


  export function* deleteTreatmentPlanSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { req } = yield race({
        req: take(DELETE_TREATMENT_PLAN),
      });
      const { record } = req;
      if (record) {
        try {
          yield call(deleteTreatmentPlanRemote, record.id);
          const recordInStore = yield select(patientsBlock.selectors.selectRecord(record.patientId));
          const result = Object.assign({}, recordInStore.toJS());
          yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.patientId}, { treatmentPlanData: result['treatmentPlanData'].filter(c=> c.id != record.id, {})} )));
        } catch (error) {
            yield put(patientsBlock.actions.deleteRecordError(error));
        }
      }
    }
  }

export const treatmentPlanSaga = [
    loadTreatmentPlanSaga,
    createTreatmentPlanSaga,
    updateTreatmentPlanSaga,
    deleteTreatmentPlanSaga,
];
