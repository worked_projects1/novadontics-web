import api from 'utils/api';

export function loadRecords() {
  return api.get('/shipping').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/shipping', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/shipping/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/shipping/${id}`).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
