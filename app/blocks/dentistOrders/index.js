import { call, take, put, race } from 'redux-saga/effects';
import { replace } from 'react-router-redux';
import api from 'utils/api';
import ordersBlock from 'blocks/orders';

const delay = (ms) => new Promise(response => setTimeout(response, ms))
export const PLACE_ORDER = 'ds/PLACE_ORDERS';

export const placeOrder = (order, address) => ({
  type: PLACE_ORDER,
  order,
  address
});

export const placeOrderRemote = (order) =>
  api.post('/orders', order).then((response) => response.data);

export const subscribeOrder = (order) =>
  api.post('/subscribeProducts', order).then((response) => response.data).catch((error) => Promise.reject(error));

export const subscribeOrderUpdate = (order) =>
  api.put(`/subscribeProducts/${order.orderId}`, order).then((response) => response.data);

export function* placeOrderSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(PLACE_ORDER),
    });
    const { order, address } = req;
    if (order) {
      try {
        const placeOrder = order.reduce((acc, item) => {
          acc.items.push({ amount: item.amount, productId: item.id, overnight: item.overnight || false });
          return acc;
        }, { items: [], shippingAddress: address.address, tax:address.tax,shippingCharge:address.shippingCharge,creditCardFee:address.creditCardFee, grandTotal: address.grandTotal, totalAmountSaved: address.totalAmountSaved, customerProfileID: address.customerProfileID, customerPaymentProfileId: address.customerPaymentProfileId, transId: address.transId, transactionCode: address.transactionCode, paymentMethod: address.paymentMethod, vendors: address.vendors });
        const result = yield call(placeOrderRemote, placeOrder);
        const { id } = result;
        yield put(ordersBlock.actions.createRecordSuccess(result));
        yield delay(5000);
        yield put(replace(`/orders/${id}`));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export const dentistOrderSagas = [
  placeOrderSaga,
];
