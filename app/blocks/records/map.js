

export default function map() {
  function accounts() {
    return {
      appSquareOptions: [
        {
          id: 1,
          label: "Appointment Book",
          value: "appointments"
        },
        {
          id: 2,
          label: "Catalog",
          value: "catalog"
        },
        {
          id: 3,
          label: "3 CE Courses / 6 CE Credits Per Year (Trial Accounts)",
          value: "CE3"
        },
        {
          id: 4,
          label: "5 CE Courses / 10 CE Credits Per Year (Plus Plan)",
          value: "CE5"
        },
        {
          id: 5,
          label: "10 CE Courses / 20 CE Credits Per Year (Pro Plan)",
          value: "CE10"
        },
        {
          id: 6,
          label: "Checklists",
          value: "checklist"
        },
        {
          id: 7,
          label: "Request Online Consult",
          value: "requestConsultationOnline"
        },
        {
          id: 8,
          label: "Implant Education",
          value: "CIICourse"
        },
        {
          id: 9,
          label: "Dashboard",
          value: "dashboard"
        },
        {
          id: 10,
          label: "Implant Log Book",
          value: "implantLogBook"
        },
        {
          id: 11,
          label: "Patient Education",
          value: "patientEducation"
        },
        {
          id: 12,
          label: "Patient Module",
          value: "patients"
        },
        {
          id: 13,
          label: "Reports Module",
          value: "reports"
        },
        {
          id: 14,
          label: "Request In-Office Support",
          value: "requestConsultation"
        },
        {
          id: 15,
          label: "Resource Center",
          value: "documents"
        },
        {
          id: 16,
          label: "Create New Patient",
          value: "createPatient"
        },
        {
          id: 17,
          label: "Update Payment",
          value: "updatePayment"
        },
        {
          id: 18,
          label: "Delete Payment",
          value: "deletePayment"
        },
      ],
      webModulesOptions: [
        {
          id: 1,
          label: "Appointment Book",
          value: "appointment"
        },
        {
          id: 2,
          label: "Dashboard",
          value: "dashboard"
        },
        {
          id: 3,
          label: "Reports Module",
          value: "reports"
        },
        {
          id: 4,
          label: "Patient Module",
          value: "patients"
        },
        {
          id: 5,
          label: "Catalog",
          value: "catalog"
        }
      ],
      subscriptionOptions: [
        {
          id: 1,
          label: "1 month",
          value: 1
        },
        {
          id: 2,
          label: "2 months",
          value: 2
        },
        {
          id: 3,
          label: "3 months",
          value: 3
        },
        {
          id: 4,
          label: "4 months",
          value: 4
        },
        {
          id: 5,
          label: "5 months",
          value: 5
        },
        {
          id: 6,
          label: "6 months",
          value: 6
        },
        {
          id: 7,
          label: "1 year",
          value: 12
        },
        {
          id: 8,
          label: "2 years",
          value: 24
        },
        {
          id: 9,
          label: "3 years",
          value: 36
        }
      ]
    }
  }


  function patients() {
    return {
      medicalConditions: {
        medicalCondition1: "Alcohol/Drug Dependency",
        medicalCondition2: "Allergies",
        medicalCondition3: "Anemia",
        medicalCondition4: "Angina",
        medicalCondition5: "Arthritis",
        medicalCondition6: "Artificial joints or implants",
        medicalCondition7: "Asthma",
        medicalCondition8: "Cancer",
        medicalCondition9: "Diabetes",
        medicalCondition10: "Epilepsy",
        medicalCondition11: "Artificial Heart Valve",
        medicalCondition12: "Congenital Heart Problems",
        medicalCondition13: "Congestive Heart Disease",
        medicalCondition14: "Heart Attack",
        medicalCondition15: "Heart Disease",
        medicalCondition16: "Heart Murmur",
        medicalCondition17: "Heart Pacemaker",
        medicalCondition18: "Heart Surgery",
        medicalCondition19: "Mitral Valve Prolapse",
        medicalCondition20: "Stroke",
        medicalCondition21: "High Blood Pressure",
        medicalCondition22: "Low Blood Pressure",
        medicalCondition23: "Glaucoma",
        medicalCondition24: "HIV Infection (AIDS)",
        medicalCondition25: "Hay Fever",
        medicalCondition26: "Frequent Headaches",
        medicalCondition27: "Ulcers",
        medicalCondition28: "Lung Disease Tuberculosis",
        medicalCondition29: "Thyroid Problems",
        medicalCondition30: "Hepatitis A",
        medicalCondition31: "Hepatitis B",
        medicalCondition32: "Hepatitis C",
        medicalCondition33: "Kidney Disease",
        medicalCondition34: "Leukemia",
        medicalCondition35: "Liver Disease",
        medicalCondition36: "Seizures /Fainting",
        medicalCondition37: "Respiratory Problems",
        medicalCondition38: "Rheumatic Fever",
        medicalCondition39: "Sinus Problems",
        medicalCondition40: "Smoking",
        medicalCondition41: "Other",
        medicalCondition42: "Taking Bisphosphonate Medication",
        medicalCondition43: "Radiation Therapy",
        medicalCondition44: "Chemo Therapy",
        medicalCondition45: "Bleeding Disorder"
      },
      allergies: {
        allergy1: "Penicillin",
        allergy2: "Latex",
        allergy3: "Codeine",
        allergy4: "Anesthetics",
        allergy5: "Sulfa Drugs",
        allergy6: "Aspirin",
        allergy7: "Other"
      },
      toothChartOptions: [
        {
          options: [
            {
              id: 1,
              label: "Single X-ray",
              value: "Single X",
              TS: true
            },
            // {
            //   id: 2,
            //   label: "Multi X-ray",
            //   value: "Multi X",
            //   E: true
            // },
            // {
            //   id: 3,
            //   label: "FMX",
            //   value: "FMX",
            //   E: true
            // },
            // {
            //   id: 4,
            //   label: "CT Scan",
            //   value: "CT",
            //   E: true
            // },
            // {
            //   id: 5,
            //   label: "Prophy Visit",
            //   value: "Prophy",
            //   E: true
            // },
            // {
            //   id: 6,
            //   label: "Sealants",
            //   value: "Sealants",
            //   E: true,
            //   TS: true
            // },
            // {
            //   id: 7,
            //   label: "Recall Visit",
            //   value: "Recall",
            //   E: true
            // },
            // {
            //   id: 8,
            //   label: "Emergency Exam",
            //   value: "Emer. V",
            //   E: true
            // },
            // {
            //   id: 9,
            //   label: "Tooth Whitening",
            //   value: "Whitening",
            //   E: true
            // },
            // {
            //   id: 10,
            //   label: "Panoramic X-ray ",
            //   value: "PX-ray",
            //   E: true
            // }
          ]
        },
        {
          options: [
            {
              id: 11,
              label: "Crown",
              value: "CR",
              E: true,
              TS: true
            },
            {
              id: 12,
              label: "Veneer",
              value: "VN",
              E: true,
              TS: true
            },
            {
              id: 13,
              label: "Pontic",
              value: "PN",
              E: true,
              TS: true
            },
            {
              id: 14,
              label: "Occlusal Onlay",
              value: "OCO",
              E: true,
              TS: true
            },
            {
              id: 15,
              label: "MO Onlay",
              value: "MOO",
              E: true,
              TS: true
            },
            {
              id: 16,
              label: "DO Onlay",
              value: "DOO",
              E: true,
              TS: true
            },
            {
              id: 17,
              label: "MOD Onlay",
              value: "MDO",
              E: true,
              TS: true
            },
            {
              id: 18,
              label: "Buccal Onlay",
              value: "BCO",
              E: true,
              TS: true
            },
            {
              id: 19,
              label: "Post",
              value: "PO",
              E: true,
              TS: true
            }
          ]
        },
        {
          options: [
            {
              id: 20,
              label: "Occlusal Cavity",
              value: "OC",
              E: true
            },
            {
              id: 21,
              label: "Mesial Cavity",
              value: "MC",
              E: true
            },
            {
              id: 22,
              label: "Distal Cavity",
              value: "DC",
              E: true
            },
            {
              id: 23,
              label: "Buccal Cavity",
              value: "BC",
              E: true
            },
            {
              id: 24,
              label: "Lingual/Palatal Cavity",
              value: "LPC",
              E: true
            },
            {
              id: 25,
              label: "Broken Tooth",
              value: "BT",
              E: true
            },
            {
              id: 32,
              label: "Missing Tooth",
              value: "MT",
              E: true
            },
            {
              id: 33,
              label: "Periapical lesion",
              value: "PL",
              E: true
            },
            {
              id: 34,
              label: "Deciduous tooth",
              value: "DT",
              E: true
            },
          ]
        },
        {
          options: [
            {
              id: 26,
              label: "Occlusal filling",
              value: "OCF",
              E: true,
              TS: true
            },
            {
              id: 27,
              label: "MO filling",
              value: "MOF",
              E: true,
              TS: true
            },
            {
              id: 28,
              label: "DO filling",
              value: "DOF",
              E: true,
              TS: true
            },
            {
              id: 29,
              label: "MOD filling",
              value: "MDF",
              E: true,
              TS: true
            },
            {
              id: 30,
              label: "Buccal filling",
              value: "BCF",
              E: true,
              TS: true
            },
            {
              id: 31,
              label: "Lingual/Palatal filling",
              value: "LPF",
              E: true,
              TS: true
            }
          ]
        },
        {
          options: [
            {
              id: 39,
              label: "Extraction",
              value: "EXT",
              TS: true
            },
            {
              id: 40,
              label: "Hemisection",
              value: "HEMI",
              E: true,
              TS: true
            },
            {
              id: 41,
              label: "Apicoectomy",
              value: "APICO",
              E: true,
              TS: true
            },
            {
              id: 35,
              label: "Root Canal",
              value: "RC",
              E: true,
              TS: true
            }
          ]
        },
        {
          options: [
            {
              id: 36,
              label: "Implant",
              value: "IM",
              E: true,
              TS: true
            },
            {
              id: 37,
              label: "Mini Implant",
              value: "MI",
              E: true,
              TS: true
            },
            {
              id: 38,
              label: "Socket Graft",
              value: "SG",
              E: true,
              TS: true
            }
          ]
        }
      ],
      implantExamImaging: [ //For Implant ExamImaging
        {
          options: [
            {
              id: 1,
              label: "Multi X-ray",
              value: "Multi X",
              TS: true
            },
            {
              id: 2,
              label: "FMX",
              value: "FMX",
              TS: true
            },
            {
              id: 3,
              label: "CT Scan",
              value: "CT",
              TS: true
            },
            {
              id: 4,
              label: "Prophy Visit",
              value: "Prophy",
              TS: true
            },
            {
              id: 6,
              label: "Recall Visit",
              value: "Recall",
              TS: true
            },
            {
              id: 7,
              label: "Emergency Exam",
              value: "Emer. V",
              TS: true
            },
            {
              id: 5,
              label: "Sealants",
              value: "Sealants",
              E: true,
              TS: true
            },
            {
              id: 8,
              label: "Tooth Whitening",
              value: "Whitening",
              TS: true
            },
            {
              id: 9,
              label: "Panoramic X-ray ",
              value: "PX-ray",
              TS: true
            }
          ]
        }
      ],
      continueEducationInfo:{
        id: 1,
        attributes: {
          info: {
            text: `<div style="font-weight: normal; font-size: 16px"><p>Your trial or catalog-only access to the Novadontics continuing education (CE) module comes with three complimentary live lectures (the first three you select to watch). Paid subscriptions come with access to a certain number of lectures per calendar year depending on your plan. Once you access a lecture, you will notice the word "Viewed" appears under the image of the lecture. This word will be replaced with the word "Completed" when you request CE credit for the lecture and successfully answer the associated questions. Once you access your maximum number of lectures for the year, you will notice the word "Purchase" appearing on all lectures that have not been viewed or completed. 
            </p><li>A Novadontics subscription plan with unlimited access is available, or call 888.838.6682 to purchase unlimited access separately.</li><br /><li>You will always have access to the lectures you viewed or completed as long as you have an active subscription with Novadontics.</li><br /><li>All CE courses on the Novadontics platform are priced at $29 each. 
            </li></div>`,
            openModal: true
          }
        }
      },
    }
  }

  return {
    accounts,
    patients
  }
}