import actions from './actions';
import constants from './constants';
import reducer from './reducer';
import sagas from './sagas';
import selectors from './selectors';
import { remotes } from 'utils/tools';
import entityUrl from 'utils/entityUrl';

export default function records(name, remotesBlock, additionalSagas) {
  const actualConstants = constants(name);
  const actualActions = actions(actualConstants);
  const actualRemotes = remotes(name, remotesBlock);
  const actualReducer = reducer(actualConstants, name);
  const actualSelectors = selectors(name);
  let actualSagas = sagas(actualConstants, actualActions, actualRemotes, actualSelectors, entityUrl(name));
  if (additionalSagas) {
    actualSagas = actualSagas.concat(additionalSagas);
  }
  return {
    name,
    actions: actualActions,
    constants: actualConstants,
    reducer: actualReducer,
    remotes: actualRemotes,
    sagas: actualSagas,
    selectors: actualSelectors,
  };
}

