/*
 *
 * Records actions
 *
 */

export default function actions(constants) {
  const {
    LOAD_RECORD,
    LOAD_RECORD_SUCCESS,
    LOAD_RECORD_ERROR,
    LOAD_RECORDS,
    LOAD_RECORDS_SUCCESS,
    LOAD_RECORDS_ERROR,
    CREATE_RECORD,
    CREATE_RECORD_SUCCESS,
    CREATE_RECORD_ERROR,
    UPDATE_RECORD,
    UPDATE_RECORD_SUCCESS,
    UPDATE_RECORD_ERROR,
    UPDATE_RECORDS,
    UPDATE_RECORDS_SUCCESS,
    UPDATE_RECORDS_ERROR,
    DELETE_RECORD,
    DELETE_RECORD_SUCCESS,
    DELETE_RECORD_ERROR,
    LOAD_RECORDS_VALID_CACHE,
    UPDATE_HEADER,
    UPDATE_CHILDREN,
    UPDATE_SEARCH,
    UPDATE_SEARCH_VALUE,
    UPDATE_RECORDS_META_DATA,
    UPDATE_LEDGER_META_DATA,
    UPDATE_LEDGER_LOADING,
    UPDATE_DATE,
    LOAD_RECORDS_PARAMS,
    LOAD_EXTERNAL_RECORDS,
    LOAD_EXTERNAL_RECORDS_SUCCESS,
    LOAD_EXTERNAL_RECORD_SUCCESS,
    UPDATE_EXTERNAL_RECORD
  } = constants;


  function loadRecordsCacheHit() {
    return {
      type: LOAD_RECORDS_VALID_CACHE,
    };
  }

  function loadRecord(id) {
    return {
      type: LOAD_RECORD,
      id,
    };
  }

  function loadPracticeAccounts(id) {
    return {
      type: LOAD_RECORDS,
      id,
    };
  }

  function loadRecordSuccess(record) {
    return {
      type: LOAD_RECORD_SUCCESS,
      record
    };
  }

  function loadRecordError(error) {
    return {
      type: LOAD_RECORD_ERROR,
      error,
    };
  }

  function loadRecords(invalidateCache) {
    return {
      type: LOAD_RECORDS,
      invalidateCache,
    };
  }

  function loadRecordsSuccess(records, recordsMetaData) {
    return {
      type: LOAD_RECORDS_SUCCESS,
      records,
      recordsMetaData,
    };
  }

  function loadRecordsError(error) {
    return {
      type: LOAD_RECORDS_ERROR,
      error,
    };
  }

  function createRecord(record, form) {
    return {
      type: CREATE_RECORD,
      record,
      form,
    };
  }

  function createRecordSuccess(record) {
    return {
      type: CREATE_RECORD_SUCCESS,
      record,
    };
  }

  function createRecordError(error) {
    return {
      type: CREATE_RECORD_ERROR,
      error,
    };
  }

  function updateRecord(record, form) {
    return {
      type: UPDATE_RECORD,
      record,
      form,
    };
  }

  function updateRecordSuccess(record) {
    return {
      type: UPDATE_RECORD_SUCCESS,
      record,
    };
  }

  function updateRecordError(error) {
    return {
      type: UPDATE_RECORD_ERROR,
      error,
    };
  }

  function updateRecords(records, callback) {
    return {
      type: UPDATE_RECORDS,
      records,
      callback,
    };
  }

  function updateRecordsSuccess(records) {
    return {
      type: UPDATE_RECORDS_SUCCESS,
      records,
    };
  }

  function updateRecordsError(error) {
    return {
      type: UPDATE_RECORDS_ERROR,
      error,
    };
  }

  function deleteRecord(id, form) {
    return {
      type: DELETE_RECORD,
      id,
      form,
    };
  }

  function deleteRecordSuccess(id) {
    return {
      type: DELETE_RECORD_SUCCESS,
      id,
    };
  }

  function deleteRecordError(error) {
    return {
      type: DELETE_RECORD_ERROR,
      error,
    };
  }

  function updateHeader(filter) {
    return {
      type: UPDATE_HEADER,
      filter
    }
  }

  function updateChildren(action) {
    return {
      type: UPDATE_CHILDREN,
      action
    }
  }

  function updateSearch(action, searchedValue) {
    return {
      type: UPDATE_SEARCH,
      action
    }
  }

  function updateSearchValue(action, searchedValue) {
    return {
      type: UPDATE_SEARCH_VALUE,
      action
    }
  }


  function updateRecordsMetaData(recordsMetaData){
    return {
      type: UPDATE_RECORDS_META_DATA,
      recordsMetaData
    }
  }


  function updateLedgerMetaData(recordsMetaData){
    return {
      type: UPDATE_LEDGER_META_DATA,
      recordsMetaData
    }
  }


  function updateLedgerLoading(action){
    return {
      type: UPDATE_LEDGER_LOADING,
      action
    }
  }


  function updateDate(date) {
    return {
      type: UPDATE_DATE,
      date
    }
  }

  function loadRecordsParams(record, invalidateCache) {
    return {
      type: LOAD_RECORDS_PARAMS,
      record,
      invalidateCache
    };
  }

  
  function loadExternalRecords(params) {
    return {
      type: LOAD_EXTERNAL_RECORDS,
      params
    };
  }

  
  function loadExternalRecordSuccess(record) {
    return {
      type: LOAD_EXTERNAL_RECORD_SUCCESS,
      record
    };
  }

  function loadExternalRecordsSuccess(records) {
    return {
      type: LOAD_EXTERNAL_RECORDS_SUCCESS,
      records
    };
  }

  function updateExternalRecord(record) {
    return {
      type: UPDATE_EXTERNAL_RECORD,
      record
    };
  }
  
  return {
    loadRecord,
    loadRecordSuccess,
    loadRecordError,
    loadRecords,
    loadRecordsSuccess,
    loadRecordsError,
    createRecord,
    createRecordSuccess,
    createRecordError,
    updateRecord,
    updateRecordSuccess,
    updateRecordError,
    updateRecords,
    updateRecordsSuccess,
    updateRecordsError,
    deleteRecord,
    deleteRecordSuccess,
    deleteRecordError,
    loadRecordsCacheHit,
    loadPracticeAccounts,
    updateHeader,
    updateChildren,
    updateSearch,
    updateSearchValue,
    updateRecordsMetaData,
    updateLedgerMetaData,
    updateLedgerLoading,
    updateDate,
    loadRecordsParams,
    loadExternalRecords,
    loadExternalRecordSuccess,
    loadExternalRecordsSuccess,
    updateExternalRecord
  };
}
