import { delay } from 'redux-saga';
import { call, take, put, race, select } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { replace } from 'react-router-redux';
import { DEFAULT_UPDATE_ERROR, DEFAULT_CREATE_ERROR, DEFAULT_DELETE_ERROR } from 'utils/errors.js';
const VALID_CACHE_DIFF = -30;
import categoryRemotes from '../categories/remotes';
import tutorialsRemotes from '../tutorials/remotes';
import manualsRemotes from '../manuals/remotes';
import practiceRemotes from '../practices/remotes';
import vendorRemotes from '../vendors/remotes';
import userRemotes from '../users/remotes';
import emailMappingsRemotes from '../emailMappings/remotes';
import resourceCategoriesRemotes_ from '../resource-categories/remotes';
import shippingRemotes from '../shipping/remotes';
import { name, remotes as appRemotes, moduleBlocks, setupBlocks, iOS } from 'utils/tools';
import prescriptionsRemotes from '../prescriptions/remotes';
import { selectUser } from 'blocks/session/selectors';

const videoCategoriesRemotes = resourceCategoriesRemotes_('courses');
const documentsCategoriesRemotes = resourceCategoriesRemotes_('documents');
const ciiCategoriesRemotes = resourceCategoriesRemotes_('ciicourse');
const prescriptionsCategoriesRemotes = resourceCategoriesRemotes_('prescriptions');
const vendorsCategoriesRemotes = resourceCategoriesRemotes_('vendorGroups');
const procedureCodeCategoryRemotes = resourceCategoriesRemotes_('procedureCodeCategory');

const NATIVE_PLATFORM = iOS();

export default function sagas(constants, actions, remotes, selectors, entityUrl) {
  const {
    LOAD_RECORD,
    LOAD_RECORDS,
    CREATE_RECORD,
    UPDATE_RECORD,
    UPDATE_RECORDS,
    DELETE_RECORD,
    UPDATE_CHILDREN,
    UPDATE_SEARCH,
    UPDATE_SEARCH_VALUE,
    UPDATE_RECORD_SUCCESS,
    LOAD_RECORDS_PARAMS,
    LOAD_EXTERNAL_RECORDS,
    UPDATE_EXTERNAL_RECORD
  } = constants;

  const {
    loadRecord: loadRecordAction,
    loadRecordSuccess,
    loadRecordError,
    loadRecords: loadRecordsAction,
    loadRecordsSuccess,
    loadRecordsError,
    createRecordSuccess,
    createRecordError,
    updateRecordSuccess,
    updateRecordError,
    updateRecordsSuccess,
    updateRecordsError,
    deleteRecordSuccess,
    deleteRecordError,
    loadRecordsCacheHit,
    updateHeader, 
    updateChildren,
    updateSearch,
    updateSearchValue,
    updateRecordsMetaData,
    loadExternalRecordsSuccess
  } = actions;

  const {
    loadRecord,
    loadRecords,
    createRecord,
    updateRecord,
    updateRecords,
    deleteRecord,
    loadRecordsParams,
    loadExternalRecords,
    updateExternalRecord   
  } = remotes;

  const {
    selectRecord,
    selectRecords,
    selectUpdateTimestamp,
  } = selectors;


  function* loadRecordsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const load = yield race({
        explicitLoad: take(LOAD_RECORDS),
      });

      const { explicitLoad } = load;
      const { invalidateCache } = explicitLoad || {};
      const lastLoad = yield select(selectUpdateTimestamp());
      const currentTimestamp = Math.floor(Date.now() / 1000);

      if (explicitLoad) {
        if (!invalidateCache && (lastLoad && (lastLoad - currentTimestamp) > VALID_CACHE_DIFF) && entityUrl != 'patients' && LOAD_RECORDS.indexOf('operatories') === -1) {
          yield put(loadRecordsCacheHit());
        } else {
          try {
            const records = yield call(loadRecords);

            const user = yield select(selectUser()); 
            let recordsMetaData;
            if (entityUrl === 'catalog') {
              const categories = yield call(categoryRemotes.loadRecords);
              const vendors = yield call(vendorRemotes.loadRecords);
              const shipping = yield call(shippingRemotes.loadRecords);
              const vendorCategoriesRemotes  = yield call(appRemotes, 'getVendorCategories');
              const vendorCategoriesList = yield call(vendorCategoriesRemotes.loadRecords);
              recordsMetaData = { categories, vendors, shipping, categoriesList: categories, vendorCategoriesList };
            } else if (entityUrl === 'accounts') {
              const ciiCourseRemotes = yield call(appRemotes, 'resource-categories/ciicourse');
              const ciiCourse = yield call(ciiCourseRemotes.loadRecords);
              const practices = yield call(practiceRemotes.loadRecords);
              recordsMetaData = { practices, ciiCourse};
            } else if (entityUrl === 'emailMappings' && user.role === 'admin') {
              recordsMetaData = yield call(userRemotes.loadRecords);
            } else if (entityUrl === 'orders') {
              recordsMetaData = yield call(vendorRemotes.loadRecords);
            } else if (entityUrl === 'courses') {
              recordsMetaData = yield call(videoCategoriesRemotes.loadRecords);
            } else if (['ciicourses', 'resource-categories/ciicourses'].includes(entityUrl)) {
              recordsMetaData = yield call(ciiCategoriesRemotes.loadRecords);
            } else if (entityUrl === 'documents') {
              recordsMetaData = yield call(documentsCategoriesRemotes.loadRecords);
            } else if (entityUrl === 'procedure') {
              recordsMetaData = yield call(procedureCodeCategoryRemotes.loadRecords);
            } else if (entityUrl === 'patient-courses') {
              recordsMetaData = yield call(videoCategoriesRemotes.loadRecords);
            } if (entityUrl === 'vendors') {
              const stateTaxRemotes  = yield call(appRemotes, 'stateTax');
              const stateRemotes  = yield call(appRemotes, 'state');
              const shipping = yield call(shippingRemotes.loadRecords);
              const categories = yield call(categoryRemotes.loadRecords);
              const stateTax = yield call(stateTaxRemotes.loadRecords);
              const state = yield call(stateRemotes.loadRecords);
              const vendorGroups = yield call(vendorsCategoriesRemotes.loadRecords);
              const vendors = yield call(vendorRemotes.loadRecords);
              const vendorCategoriesRemotes  = yield call(appRemotes, 'getVendorCategories');
              const vendorCategoriesList = yield call(vendorCategoriesRemotes.loadRecords);
              recordsMetaData = { shipping, categories, stateTax, state, vendorGroups, vendors, vendorCategoriesList };
            } else if(entityUrl === 'patients' && user.role != 'admin') {
              const patientTypeRemotes = yield call(appRemotes, 'listOptionsReadAll/patientType');
              const providersRemotes = yield call(appRemotes, 'providers');
              const dentalCarrierRemotes = yield call(appRemotes, 'dentalCarrier');
              const procedureRemotes = yield call(appRemotes, 'procedure');
              const profileRemotes = yield call(appRemotes, 'profile');
              const doctorsRemotes = yield call(appRemotes, 'getDoctorsList');
              const mainSquareRemotes = yield call(appRemotes, 'mainSquareIconList');
              const staffMembersRemotes = yield call(appRemotes, 'staffMembers');
              const implantBrandRemotes = yield call(appRemotes, 'listOptionsReadAll/implantBrand');
              const preMadeClinicalNotesRemotes = yield call (appRemotes, 'preMadeClinicalNotes');
              const recallProcedureRemotes = yield call(appRemotes, 'recallProcedure');
              const prescriptionsCategoryRemotes = yield call(appRemotes, 'resource-categories/prescriptions');
              const patientRemotes = yield call(appRemotes, 'readPatients');
              const emailMappings  = yield call(emailMappingsRemotes.loadRecords);
              const prescriptions = yield call(prescriptionsRemotes.loadRecords);
              const patientTypeList = yield call(patientTypeRemotes.loadRecords);
              const preMadeClinicalNotesList = yield call(preMadeClinicalNotesRemotes.loadRecords);
              const providersList = yield call(providersRemotes.loadRecords);
              const dentalCarrierList = yield call(dentalCarrierRemotes.loadRecords);
              const procedureList = yield call(procedureRemotes.loadRecords);
              const profileList = yield call(profileRemotes.loadRecords);
              const doctorsList = yield call(doctorsRemotes.loadRecords);
              const mainSquareIconList = yield call(mainSquareRemotes.loadRecords);
              const staffMembersList = yield call(staffMembersRemotes.loadRecords);
              const implantBrandList = yield call(implantBrandRemotes.loadRecords);
              const recallProcedureList = yield call(recallProcedureRemotes.loadRecords);
              const prescriptionsCategoryList = yield call(prescriptionsCategoryRemotes.loadRecords);
              const procedureCategoryList = yield call(procedureCodeCategoryRemotes.loadRecords);
              const patientList = yield call(patientRemotes.loadRecords);
              recordsMetaData = { emailMappings, prescriptions, patientTypeList, providersList, dentalCarrierList, procedureList, profileList, doctorsList, mainSquareIconList, staffMembersList, implantBrandList, recallProcedureList,preMadeClinicalNotesList, prescriptionsCategoryList, procedureCategoryList, patientList };
            } else if(entityUrl === 'patients' && user.role === 'admin') {
              const patientTypeRemotes = yield call(appRemotes, 'listOptionsReadAll/patientType');
              const procedureRemotes = yield call(appRemotes, 'procedure');
              const emailMappings  = yield call(emailMappingsRemotes.loadRecords);
              const prescriptions = yield call(prescriptionsRemotes.loadRecords);
              const patientTypeList = yield call(patientTypeRemotes.loadRecords);
              const procedureList = yield call(procedureRemotes.loadRecords);
              recordsMetaData = { emailMappings, prescriptions, patientTypeList, procedureList };
            } else if(entityUrl === 'categories' && user.role === 'admin') {
              const categories = yield call(categoryRemotes.loadRecords);
              recordsMetaData = { categories };
            } else if(entityUrl === 'tutorials' && user.role === 'admin') {
              const tutorials = yield call(tutorialsRemotes.loadRecords);
              recordsMetaData = { tutorials };
            } else if(entityUrl === 'manuals' && user.role === 'admin') {
              const manuals = yield call(manualsRemotes.loadManualRecords);
              recordsMetaData = { manuals };
            } else if(LOAD_RECORDS.indexOf('providers') > -1){
              const roleRemotes  = yield call(appRemotes, 'role');
              const role = yield call(roleRemotes.loadRecords);
              recordsMetaData = { role: role && role.length > 0 ? role.reduce((a, el) => { a.push(Object.assign({}, { label: el.role, value: el.role })); return a; }, []) : [] };
            } else if(LOAD_RECORDS.indexOf('staffMembers') > -1) {
              const staffRoleRemotes = yield call(appRemotes, 'listOptionsReadAll/staffRole');
              const staffRole = yield call(staffRoleRemotes.loadRecords);
              recordsMetaData = { staffRole: staffRole && staffRole.length > 0 ? staffRole.reduce((a, el) => { a.push(Object.assign({}, { label: el.listOptions, value: el.listOptions })); return a; }, []) : [] };
            } else if(LOAD_RECORDS.indexOf('dentalCarrier') > -1) {
              const insuranceCompanyRemotes = yield call(appRemotes, 'insuranceCompany');
              const insuranceCompany = yield call(insuranceCompanyRemotes.loadRecords);
              const stateRemotes = yield call(appRemotes, 'state');
              const state = yield call(stateRemotes.loadRecords);
              recordsMetaData = {   state: state && state.length > 0 ? state.reduce((a, el) => { a.push(Object.assign({}, { label: el.stateName, value: el.state })); return a; }, []) : [] , insuranceCompany: insuranceCompany && insuranceCompany.length > 0 ? insuranceCompany.reduce((a, el) => { a.push(Object.assign({}, { label: el.name, value: el.name })); return a; }, []) : [] };
            } else if(LOAD_RECORDS.indexOf('insuranceFee') > -1) {
              const procedureRemotes = yield call(appRemotes, 'procedure');
              const insuranceCompanyRemotes = yield call(appRemotes, 'insuranceCompany');
              const dentalCarrierRemotes = yield call(appRemotes, 'dentalCarrier');
              const procedureList = yield call(procedureRemotes.loadRecords);
              const insuranceCompanyList = yield call(insuranceCompanyRemotes.loadRecords);
              const dentalCarrierList = yield call(dentalCarrierRemotes.loadRecords);
              recordsMetaData = { procedureList, insuranceCompanyList, dentalCarrierList };
            } else if(LOAD_RECORDS.indexOf('officeInfo') > -1 && user.role != 'admin') {
              const providersRemotes = yield call(appRemotes, 'providers');
              const providers = yield call(providersRemotes.loadRecords);
              recordsMetaData = { providers: providers && providers.length > 0 ? providers.reduce((a, el) => { a.push(Object.assign({}, { label: el.name, value: el.name })); return a; }, []) : [] };
            } else if (entityUrl === 'prescriptions') {
              recordsMetaData = yield call(prescriptionsCategoriesRemotes.loadRecords);
            } else if (entityUrl === 'implantLog') {
              const providersRemotes = yield call(appRemotes, 'providers');
              const providersList = yield call(providersRemotes.loadRecords);
              const implantBrandRemotes = yield call(appRemotes, 'listOptionsReadAll/implantBrand');
              const implantBrandList = yield call(implantBrandRemotes.loadRecords);
              recordsMetaData = { implantBrandList: implantBrandList && implantBrandList.length > 0 ? implantBrandList.reduce((a, el) => { a.push(Object.assign({}, { label: el.listOptions, value: el.listValue })); return a; }, []) : [], providersList: providersList && providersList.length > 0 ? providersList.reduce((a, el) => { a.push(Object.assign({}, { label: el.name, value: el.id })); return a; }, []) : []};
            } else if (entityUrl === 'boneGraftLog') {
              const providersRemotes = yield call(appRemotes, 'providers');
              const providersList = yield call(providersRemotes.loadRecords);
              recordsMetaData = { providersList: providersList && providersList.length > 0 ? providersList.reduce((a, el) => { a.push(Object.assign({}, { label: el.name, value: el.id })); return a; }, []) : []};
            } else if(entityUrl === 'insurance'){
              const procedureRemotes = yield call(appRemotes, 'procedure');
              const insuranceCompanyRemotes = yield call(appRemotes, 'insuranceCompany');
              const procedureList = yield call(procedureRemotes.loadRecords);
              const insuranceCompanyList = yield call(insuranceCompanyRemotes.loadRecords);
              recordsMetaData = { procedureList, insuranceCompanyList };
            }

            if(records && records.headers){
              yield put(updateHeader(records.headers));
              yield put(loadRecordsSuccess(records.data, recordsMetaData));
            } else if (records) {
              if(LOAD_RECORDS.indexOf('implantData') > -1 && entityUrl === 'implantData') {
                yield put(loadRecordsSuccess(records.implantData, recordsMetaData));
              } else {
                yield put(loadRecordsSuccess(records, recordsMetaData));
              }
            } else {
              yield put(loadRecordsError());
            }
          } catch (error) {
            yield put(loadRecordsError(error));
          }
        }
      }
    }
  }


  function* loadRecordsParamsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { load } = yield race({
          load: take(LOAD_RECORDS_PARAMS),
      });
      const { record, invalidateCache } = load || {};
      const lastLoad = yield select(selectUpdateTimestamp());
      const currentTimestamp = Math.floor(Date.now() / 1000);
      if (!invalidateCache && (lastLoad && (lastLoad - currentTimestamp) > VALID_CACHE_DIFF)) {
        yield put(loadRecordsCacheHit());
      } else {
        try {
          const records = yield call(loadRecordsParams, record);
          let recordsMetaData;
          if(entityUrl === 'ledger') {
            const carrierRemotes = yield call(appRemotes, 'dentalCarrier');
            const dentalCarrier = yield call(carrierRemotes.loadRecordsParams, {includeTrojan: true});
            const dentalCarrierList = dentalCarrier.data;
            const adjustmentReasonRemotes = yield call(appRemotes, 'listOptionsReadAll/adjustmentReason');
            const adjustmentReason = yield call(adjustmentReasonRemotes.loadRecordsParams);
            const adjustmentReasonList = adjustmentReason.data;
            recordsMetaData = { dentalCarrier: dentalCarrierList && dentalCarrierList.length > 0 ? dentalCarrierList.reduce((a, el) => { a.push(Object.assign({}, { label: el.name, value: el.id })); return a; }, []) : [], adjustmentReason: adjustmentReasonList && adjustmentReasonList.length > 0 ? adjustmentReasonList.reduce((a, el) => { a.push(Object.assign({}, { label: el.listOptions, value: el.listOptions })); return a; }, []) : [] };
          }
          if(records && records.headers) {
            if(LOAD_RECORDS.indexOf('implantData') > -1 && entityUrl === 'implantData') {
              yield put(updateHeader(records.headers));
              yield put(loadRecordsSuccess(records.data.implantData, recordsMetaData));
            } else {
              yield put(updateHeader(records.headers));
              yield put(loadRecordsSuccess(records.data, recordsMetaData));
            }
          } else if (records) {
            yield put(loadRecordsSuccess(records.data, recordsMetaData));
          } else {
            yield put(loadRecordsError());
          }
        } catch (error) {
          yield put(loadRecordsError(error));
        }
      }  
    }
  }

  function* loadRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const loadRequest = yield race({
        request: take(LOAD_RECORD),
      });
      const { request } = loadRequest;

      if (request) {
        const { id } = request;
        try {
          const record = yield call(loadRecord, id);
          const user = yield select(selectUser()); 

          let recordsMetaData;
          if(entityUrl === 'patients' && user.role == 'admin') {
            const providersRemotes = yield call(appRemotes, 'providers');
            const providersList = yield call(providersRemotes.loadRecordsParams, {practiceId: record.practiceId || ''});
            recordsMetaData = { providersList };
          }
          if(entityUrl === 'patients' && user.role != 'admin') {
            const preMadeClinicalNotesRemotes = yield call (appRemotes, 'preMadeClinicalNotes');
            const preMadeClinicalNotesList = yield call(preMadeClinicalNotesRemotes.loadRecords);
            recordsMetaData = { preMadeClinicalNotesList };
          }

          if(recordsMetaData){
            yield put(updateRecordsMetaData(recordsMetaData));
          }

          if (record) {
            // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
            while (true) { // eslint-disable-line no-constant-condition
              const recordsInStore = yield select(selectRecords());
              if (recordsInStore && recordsInStore.size) {
                break;
              }
              yield call(delay, 500);
            }
            yield put(loadRecordSuccess(record));
          } else {
            yield put(loadRecordError());
          }
        } catch (error) {
          yield put(loadRecordError(error));
        }
      }
    }
  }

  function* createRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { create } = yield race({
        create: take(CREATE_RECORD),
      });
      const { record, form } = create || {};
      if (create) {
        yield put(startSubmit(form));

        try {
          const user = yield select(selectUser());
          const result = yield call(createRecord, record);
          const { id } = result;
          const redirectPath = (entityUrl === '') ? replace(window.location.pathname) : entityUrl == "categories" || entityUrl == "tutorials" || entityUrl == "manuals" ? replace(`/${entityUrl}`) : entityUrl.indexOf('resource-categories') > -1 && id ? replace(`/${entityUrl}`) : id ? replace(`/${entityUrl}/${id}${entityUrl === 'patients' ? '/form' : ''}`) : replace(`/${entityUrl}`);
          if (CREATE_RECORD.indexOf('categories') > -1 || CREATE_RECORD.indexOf('tutorials') > -1 || CREATE_RECORD.indexOf('manuals') > -1) {
            yield put(createRecordSuccess(record));
            yield put(loadRecordsAction(true));
          } else if(entityUrl === 'inOfficeSupport'){
            yield put(loadRecordsAction(true));
          } else if(CREATE_RECORD.indexOf('patients') > -1){
            //yield put(loadRecordsAction(true));
          } else if(CREATE_RECORD.indexOf('products') > -1 && user.role == 'admin'){
            const productHeaders = { searchText: record.name }
            yield put(updateHeader(productHeaders));
          } else if(CREATE_RECORD.indexOf(name) > -1 || entityUrl === 'insurance'){
            yield put(createRecordSuccess(record));
          } else {
            yield put(createRecordSuccess(result));
          }

          if(user && user.platform === 'iPad'){
            window.location = `inapp://done&&${id}`;
          } else {  
            if(entityUrl != 'consultations' && entityUrl != 'inOfficeSupport'){
              yield put(redirectPath); 
            }
          }
          yield put(stopSubmit(form));
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && (error.response.data.error || (error.response.data[0] &&error.response.data[0].message)) ? error.response.data.error || error.response.data.message || error.response.data[0].message : DEFAULT_CREATE_ERROR;
          yield put(createRecordError(errorMessage));
          yield put(stopSubmit(form, { _error: errorMessage }));
        }
      }
    }
  }

  function* editRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { edit } = yield race({
        edit: take(UPDATE_RECORD),
      });
      const { record, form } = edit || {};
      if (edit) {
        yield put(startSubmit(form));

        try {
          const user = yield select(selectUser());
          const result = yield call(updateRecord, record);
          const { id, steps } = UPDATE_RECORDS.indexOf('orders') > -1 ? result : record;
          const redirectPath = entityUrl === '' ? replace(window.location.pathname) : entityUrl === 'patients' && id && steps[0]['formId'].match( /(1L|1M)/ ) ? `${process.env.API_URL}/${steps[0]['formId'] == '1L' ? 'generateHealthHistory' : 'generateConsentForm'}/${id}/pdf?X-Auth-Token=${localStorage.getItem('secret').slice(1, -1)}` : entityUrl === 'patients' && id ? replace(`/${entityUrl}/${id}/form`) : entityUrl.match( /(ciicourses|courses|documents|patient-courses|categories|tutorials|manuals)/ ) == null && id ? replace(`/${entityUrl === 'emailMappings' ? 'settings' : entityUrl}/${id}`) : replace(`/${entityUrl}`);
          if (result !== undefined) {
            /* TODO fix this logic */
            if (UPDATE_RECORDS.indexOf('categories') > -1 || UPDATE_RECORDS.indexOf('shipping') > -1 || UPDATE_RECORDS.indexOf('tutorials') > -1 || UPDATE_RECORDS.indexOf('manuals') > -1) {
              yield put(loadRecordsAction(true));
            } else if(UPDATE_RECORDS.indexOf('orders') > -1){
              yield put(updateRecordSuccess(result));
            } else if(UPDATE_RECORDS.indexOf('patients') > -1){
              yield put(loadRecordAction(id));
            } else {
              if(UPDATE_RECORDS.indexOf('accounts') > -1 && result.expirationDate != undefined){
              	record.expirationDate = result.expirationDate;
              } 	
              yield put(updateRecordSuccess(record));
            }

            if(user && user.platform === 'iPad'){
              window.location = "inapp://done";
            } else {
              yield put(typeof(redirectPath) === 'object' ? redirectPath : window.open(redirectPath)); 
            }
            yield put(stopSubmit(form));
          } else {
            yield put(updateRecordError(DEFAULT_UPDATE_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_ERROR }));
          }
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && (error.response.data.error || error.response.data[0].message) ? error.response.data.error || error.response.data[0].message : DEFAULT_UPDATE_ERROR;
          yield put(updateRecordError(errorMessage));
          yield put(stopSubmit(form, { _error: errorMessage }));
        }
      }
    }
  }

  function* editRecordsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { edit } = yield race({
        edit: take(UPDATE_RECORDS),
      });
      const { records, callback } = edit || {};
      if (edit) {
        try {
          const result = yield call(updateRecords, records);
          if (result !== undefined) {
            /* TODO fix this logic */
            if (UPDATE_RECORDS.indexOf('categories') > -1 || UPDATE_RECORDS.indexOf('tutorials') > -1 || UPDATE_RECORDS.indexOf('manuals') > -1) {
              yield put(updateRecordsSuccess(records));
              yield put(loadRecordsAction(true));
            } else {
              yield put(updateRecordsSuccess(records));
              callback();
            }
          } else {
            callback({error: {message: "Import failed, please try again."}});
            yield put(updateRecordsError(DEFAULT_UPDATE_ERROR));
          }
        } catch (error) {
          callback({error:  error.response.data});
          yield put(updateRecordsError(DEFAULT_UPDATE_ERROR));
        }
      }
    }
  }

  function* deleteRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { del } = yield race({
        del: take(DELETE_RECORD),
      });
      const { id, form } = del || {};
      if (del) {
        yield put(startSubmit(form));
        try {
          const redirectPath = entityUrl === '' ? replace(window.location.pathname) : replace(`/${entityUrl}`);
          yield call(deleteRecord, id);
          yield put(deleteRecordSuccess(id));
          yield put(stopSubmit(form));
          yield put(redirectPath);
          /* TODO fix this logic */
          if (UPDATE_RECORDS.indexOf('categories') > -1 || UPDATE_RECORDS.indexOf('tutorials') > -1 || UPDATE_RECORDS.indexOf('manuals') > -1) {
            yield put(loadRecordsAction(true));
          }
        } catch (error) {
          if (UPDATE_RECORDS.indexOf('categories') > -1 || UPDATE_RECORDS.indexOf('tutorials') > -1 || UPDATE_RECORDS.indexOf('manuals') > -1) {
            yield put(stopSubmit(form, { _error: error.response.data ? error.response.data : DEFAULT_DELETE_ERROR }));
          } 
          if (UPDATE_RECORDS.indexOf('practices') > -1) {
            yield put(stopSubmit(form, { _error: error.response.data ? error.response.data : null }));
          }
          else {
            yield put(stopSubmit(form, { _error: DEFAULT_DELETE_ERROR }));
          }
          const errorMessage = error && error.response && error.response.data && (error.response.data.error || error.response.data[0].message) ? error.response.data.error || error.response.data[0].message : DEFAULT_UPDATE_ERROR;
          yield put(deleteRecordError(errorMessage));
        }
      }
    }
  }

  function* updateChildrenSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { child } = yield race({
        child: take(UPDATE_CHILDREN),
      });
      const { action } = child || {};

      if (child) {
        try {
          yield call(updateChildren, action);
          yield put(replace(window.location.pathname));
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(updateRecordError(errorMessage));
        }
      }
    }
  }

  function* updateSearchSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { child } = yield race({
        child: take(UPDATE_SEARCH),
      });
      const { action } = child || {};

      if (child) {
        try {
          yield call(updateSearch, action);
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(updateRecordError(errorMessage));
        }
      }
    }
  }

  function* updateSearchValueSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { child } = yield race({
        child: take(UPDATE_SEARCH_VALUE),
      });
      const { action } = child || {};

      if (child) {
        try {
          yield call(updateSearchValue, action);
        } catch (error) {
          const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
          yield put(updateRecordError(errorMessage));
        }
      }
    }
  }

  function* updateRecordSuccessSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const updateSuccess = yield race({
        request: take(UPDATE_RECORD_SUCCESS),
      });
      const { request } = updateSuccess;
      const path = window.location.pathname;
      if (request && UPDATE_RECORD_SUCCESS.indexOf('officeInfo') > -1) {
        try {
          yield put(path.indexOf('setup') > -1 ? setupBlocks().actions.updateChildren(false) : moduleBlocks().actions.updateChildren(false));
        } catch (error) {
          yield put(updateRecordError('Something went wrong. Please refresh page and try again'));
        }
      }
    }
  }


  function* loadExternalRecordsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { load } = yield race({
        load: take(LOAD_EXTERNAL_RECORDS),
      });
      const { params } = load || {};

      if(params){
        try {
          const user = yield select(selectUser()); 
          const records = yield call(loadExternalRecords, user.xvWebURL, params);
          
          if(records)
            yield put(loadExternalRecordsSuccess(records));
          
        } catch (error) {
          //yield put(updateRecordError('Something went wrong. Please refresh page and try again'));
        }
      }
    }
  }


  function* editExternalRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { edit } = yield race({
        edit: take(UPDATE_EXTERNAL_RECORD),
      });
      const { record } = edit || {};

      if(record){
        try {
          const user = yield select(selectUser());
          yield call(updateExternalRecord, user.xvWebURL, record);
        } catch (error) {
          //yield put(updateRecordError('Something went wrong. Please refresh page and try again'));
        }
      }
    }
  }

  return [
    loadRecordSaga,
    loadRecordsSaga,
    createRecordSaga,
    editRecordSaga,
    editRecordsSaga,
    deleteRecordSaga,
    updateChildrenSaga,
    updateSearchSaga,
    updateSearchValueSaga,
    updateRecordSuccessSaga,
    loadRecordsParamsSaga,
    loadExternalRecordsSaga,
    editExternalRecordSaga
  ];
}
