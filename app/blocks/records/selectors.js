import { createSelector } from 'reselect';
import { List, Map } from 'immutable';
import mapRecords from './utils.js';
import { parseStructure } from '../../containers/ViewPatientsForm/utils';
import groupBy from 'utils/groupBy';
import { sortBy } from "lodash.sortby";
import map from './map';

const accounts = map().accounts();
const patients = map().patients();

export default function selectors(name) {
  const selectDomain = () => (state) => state.get(name);

  const formDomain = () => (state) => state.get('form');

  const selectLoading = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('loading') : false,
  );

  const pageLoading = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('pageLoader') : false,
  );

  const selectRecordsMetaData = () => createSelector(
    selectDomain(),
    (domain) => mapRecords(
      domain ? domain.get('recordsMetaData') : List(),
      (name === 'emailMappings') ? 'settings' : name
    ) || List(),
  );

  const selectRecords = () => createSelector(
    selectDomain(),
    selectRecordsMetaData(),
    (domain, metaData) => mapRecords(domain ? domain.get('records') : List(), name, metaData) || List(),
  );

  const selectExternalRecord = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.get('externalRecord') || {},
  );

  const selectExternalRecords = () => createSelector(
    selectDomain(),
    (domain) => mapRecords(domain ? domain.get('externalRecords') : List(), name) || List(),
  );

  const selectRecord = (id) => createSelector(
    selectRecords(),
    selectRecordsMetaData(),
    (records, metaData) => {
      let record =
        records.find((record_) =>
          record_.get('id') === id) ||
        records.reduce((r, v) =>
          ((v.get('children') || List()).find((child) => child.get('id') === id) || r), Map());

      if (metaData && metaData.get('categories') && metaData.get('vendors') && name === 'products') {

        let categoriesParentList = List();
        metaData.get('categories').map((item) => {
          categoriesParentList = categoriesParentList.push(Map().set('value', item.get('id')).set('label', (item.get('name')).toUpperCase()));
        });

        record = record.setIn(['metaData', 'categoryParentOptions'], categoriesParentList);

        let categoriesChildList = List();
        metaData.get('categories').map((item) => {
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              categoriesChildList = categoriesChildList.push(
                Map()
                  .set('value', child.get('id'))
                  .set('label', child.get('name'))
                  .set('parent', child.get('parent'))
              );
            });
          }
        });

        record = record.setIn(['metaData', 'categoryChildOptions'], categoriesChildList);

        record = record.setIn(['metaData', 'vendorCategoryOptions'], metaData.get('vendorCategoriesList') && metaData.get('vendorCategoriesList').map((vendorCategoriesList) =>
          vendorCategoriesList
            .set('vendorId', vendorCategoriesList.get('vendorId'))
            .set('categories', vendorCategoriesList.get('categories'))
        ));

        let categoriesList = List();
        metaData.get('categories').map((item) => {
          categoriesList = categoriesList.push(Map().set('value', item.get('id')).set('label', (item.get('name')).toUpperCase()));
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              categoriesList = categoriesList.push(
                Map()
                  .set('value', child.get('id'))
                  .set('label', (`-- ${child.get('name')}`))
              );
            });
          }
        });
        record = record.setIn(['metaData', 'categoriesList'], metaData.get('categoriesList') && metaData.get('categoriesList').map((categoriesList) =>
        categoriesList
          .set('value', categoriesList.get('id'))
          .set('label', categoriesList.get('name'))
          .set('children',categoriesList.get('children') || [])
      ));
      
        metaData = Map({ categories: categoriesList, vendors: metaData.get('vendors') });

        record = record.setIn(['metaData', 'categoryOptions'], metaData.get('categories'));
      } else if (name === 'emailMappings') {
        record = record.setIn(['metaData', 'userOptions'], metaData)
      }

      if (['ciicourses', 'courses', 'documents', 'patient-courses', 'prescriptions'].includes(name)) {
        let resourceCategoriesList = List();
        metaData.map((item) => {
          resourceCategoriesList = resourceCategoriesList.push(Map().set('value', item.get('name')).set('label', (item.get('name')).toUpperCase()));
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              resourceCategoriesList = resourceCategoriesList.push(
                Map()
                  .set('value', child.get('name'))
                  .set('label', (`-- ${child.get('name')}`))
              );
            });
          }
        });
        record = record.setIn(['metaData', 'resourceCategoryOptions'], resourceCategoriesList);
      } else if (['procedure'].includes(name)) {
        let resourceCategoriesList = List();
        metaData.map((item) => {
          resourceCategoriesList = resourceCategoriesList.push(Map().set('value', item.get('id')).set('label', (item.get('name')).toUpperCase()));
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              resourceCategoriesList = resourceCategoriesList.push(
                Map()
                  .set('value', child.get('id'))
                  .set('label', (`-- ${child.get('name')}`))
              );
            });
          }
        });
        record = record.setIn(['metaData', 'resourceCategoryOptions'], resourceCategoriesList);
      } else if (name === 'resource-categories/ciicourses') {
        record = record.setIn(['metaData', 'resourceCategoryOptions'], metaData.map((resourceCategory) =>
          resourceCategory
            .set('value', resourceCategory.get('id'))
            .set('label', resourceCategory.get('name'))
        ));
      } else if (name === 'categories') {
        record = record.setIn(['metaData', 'categoriesOptions'], metaData.get('categories') && metaData.get('categories').map((categoriesList) =>
          categoriesList
            .set('value', categoriesList.get('id'))
            .set('label', categoriesList.get('name'))
        ));
      } else if (name === 'tutorials') {
        record = record.setIn(['metaData', 'tutorialsOptions'], metaData.get('tutorials') && metaData.get('tutorials').map((tutorialsList) =>
          tutorialsList
            .set('value', tutorialsList.get('id'))
            .set('label', tutorialsList.get('name'))
        ));
      } else if (name === 'manuals') {
        record = record.setIn(['metaData', 'manualOptions'], metaData.get('manuals') && metaData.get('manuals').map((manualList) =>
          manualList
            .set('value', manualList.get('id'))
            .set('label', manualList.get('name'))
        ));
      }

      record = record.setIn(
        ['metaData', 'vendorOptions'],
        metaData.get('vendors', List())
          .filter((vendor) => vendor.get('deleted') === false)
          .map((vendor) =>
            vendor
              .set('value', vendor.get('name'))
              .set('label', vendor.get('name'))
          )
      );

      if (name === 'accounts') {

        let ciiCourseCategoriesList = List();
        metaData.get('ciiCourse').map((item) => {
          ciiCourseCategoriesList = ciiCourseCategoriesList.push(Map().set('value', item.get('name')).set('label', (item.get('name')).toUpperCase()));
        });
        record = record.setIn(['metaData', 'ciiCourseCategoriesOptions'], ciiCourseCategoriesList);

        record = record.setIn(
          ['metaData', 'subscriptionOptions'],
          accounts.subscriptionOptions
        );

        record = record.setIn(
          ['metaData', 'appSquareOptions'],
          accounts.appSquareOptions
        );

        record = record.setIn(
          ['metaData', 'webModulesOptions'],
          accounts.webModulesOptions
        );

        record = record.setIn(['metaData', 'practiceOptions'], metaData.get('practices') && metaData.get('practices').map((practicesList) =>
          practicesList
            .set('value', practicesList.get('id'))
            .set('label', practicesList.get('name'))
        ));
      }

      if (name === 'patients') {
        const { steps: stepsRoot = {} } = record.toJS();
        const { steps } = stepsRoot;
        const patientForm = steps ? steps.filter(c => c.formId == '1A').reduce((obj, c) => Object.assign(obj, { value: c.value }), {}) : {};
        const photographsForm = steps ? steps.filter(c => c.formId == '1H').reduce((obj, c) => Object.assign(obj, { value: c.value }), {}) : {};

        record = record.setIn(['metaData', 'doctorsOptions'], metaData.get('doctorsList') && metaData.get('doctorsList').map((doctorsList) =>
          doctorsList
            .set('value', doctorsList.get('staffId'))
            .set('label', doctorsList.get('name'))
        ));

        record = record.setIn(['metaData', 'mainSquareIconOptions'], metaData.get('mainSquareIconList') && metaData.get('mainSquareIconList').map((mainSquareIconList) =>
          mainSquareIconList
            .set('value', mainSquareIconList.get('value'))
            .set('label', mainSquareIconList.get('title'))
        ));

        record = record.setIn(['metaData', 'providers'], metaData.get('providersList') && metaData.get('providersList').map((providersList) =>
          providersList
            .set('value', providersList.get('id'))
            .set('label', providersList.get('name'))
        ));

        record = record.setIn(['metaData', 'recallProcedureOptions'], metaData.get('recallProcedureList') && metaData.get('recallProcedureList').map((recallProcedureList) =>
          recallProcedureList
            .set('value', recallProcedureList.get('recallType'))
            .set('label', recallProcedureList.get('recallType'))
        ));

        record = record.setIn(['metaData', 'preMadeClinicalNotesOptions'], metaData.get('preMadeClinicalNotesList') && metaData.get('preMadeClinicalNotesList').map((preMadeClinicalNotesList) =>
        preMadeClinicalNotesList
          .set('value', preMadeClinicalNotesList.get('name'))
          .set('label', preMadeClinicalNotesList.get('name'))
       ));

        record = record.setIn(['metaData', 'procedureCodeCategory'], metaData.get('procedureCategoryList') && metaData.get('procedureCategoryList').map((procedureCategoryList) =>
        procedureCategoryList
          .set('value', procedureCategoryList.get('name'))
          .set('label', procedureCategoryList.get('name'))
       ));

        record = record.setIn(['metaData', 'implantBrandOptions'], metaData.get('implantBrandList') && metaData.get('implantBrandList').map((implantBrandList) =>
          implantBrandList
            .set('value', implantBrandList.get('listOptions'))
            .set('label', implantBrandList.get('listOptions'))
            .set('title', implantBrandList.get('listOptions'))
            .set('name', implantBrandList.get('listOptions'))
        ));

        record = record.setIn(['metaData', 'staffMembers'], metaData.get('staffMembersList') && metaData.get('staffMembersList').map((staffMembersList) =>
          staffMembersList
            .set('value', staffMembersList.get('id'))
            .set('label', staffMembersList.get('name'))
        ));

        record = record.setIn(['metaData', 'dentalCarrier'], metaData.get('dentalCarrierList') && metaData.get('dentalCarrierList').map((dentalCarrierList) =>
          dentalCarrierList
            .set('value', dentalCarrierList.get('id'))
            .set('label', dentalCarrierList.get('name'))
        ));

        record = record.setIn(['metaData', 'provider'], patientForm && patientForm.value && patientForm.value.provider);

        record = record.setIn(['metaData', 'cdcCode'], metaData.get('procedureList') && metaData.get('procedureList').map((procedureList) =>
          procedureList
            .set('value', procedureList.get('procedureCode'))
            .set('title', procedureList.get('procedureCode'))
            .set('label', procedureList.get('procedureCode'))
            .set('ref', procedureList.get('procedureType'))
        ));

        record = record.setIn(['metaData', 'procedureName'], metaData.get('procedureList') && metaData.get('procedureList').map((procedureList) =>
          procedureList
            .set('value', procedureList.get('procedureType'))
            .set('title', procedureList.get('procedureType'))
            .set('label', procedureList.get('procedureType'))
            .set('ref', procedureList.get('procedureCode'))
        ));

        record = record.setIn(['metaData', 'patientListOptions'], metaData.get('patientList') && metaData.get('patientList').map((patientList) =>
        patientList
            .set('value', patientList.get('id'))
            .set('label', patientList.get('name'))
        ));

        record = record.setIn(
          ['patientPhoto'],
          patientForm && patientForm['value'] ? parseStructure(patientForm['value']['patient_photo'])['images'] : ''
        );
        record = record.setIn(
          ['patientPhoto1'],
          photographsForm && photographsForm['value'] ? parseStructure(photographsForm['value']['1b1'])['images'] : ''
        );

        record = record.setIn(
          ['metaData', 'toothChartOptions'],
          patients.toothChartOptions
        )
        record = record.setIn(
          ['metaData', 'implantExamImaging'],
          patients.implantExamImaging
        )
        
        record = record.setIn(
          ['emailMappings'],
          metaData.get('emailMappings')
        )

        if (metaData && metaData.get('prescriptionsCategoryList')) {
          let categoriesList = List();
          metaData.get('prescriptionsCategoryList').map((item) => {
            categoriesList = categoriesList.push(Map().set('value', item.get('name')).set('label', (item.get('name')).toUpperCase()));
            if (item.get('children') && item.get('children').size) {
              item.get('children').map((child) => {
                categoriesList = categoriesList.push(
                  Map()
                    .set('value', child.get('id'))
                    .set('label', (`-- ${child.get('name')}`))
                );
              });
            }
          });

          record = record.setIn(
            ['metaData', 'prescriptionsCategory'],
            categoriesList.toJS()
          )


        }

        if (metaData && metaData.get('prescriptions')) {

          const prescriptions = groupBy('category')(metaData.get('prescriptions').toJS());
          const prescriptionsColumns = prescriptions ? _.sortBy(Object.keys(prescriptions).reduce((c, el) => {
            c.push({
              title: el && el != 'undefined' || el != '' ? el : 'Other',
              fields: _.sortBy(prescriptions[el].reduce((data, item) => {
                data.push({
                  "id": item.id,
                  "name": "2j_2_" + item.id,
                  "type": "Checkbox",
                  "title": item.medication + " " + item.dosage + ", " + item.count + " count",
                  "required": false,
                  "disabled": false,
                  "indications": item.indications,
                  "contraindications": item.contraindications,
                  "medicationInteraction": item.medicationInteraction,
                  "useInOralImplantology": item.useInOralImplantology,
                  "prescriptionValues": {
                    "id": item.id,
                    "name": item.medication + " " + item.dosage,
                    "count": item.count,
                    "instruction": item.instructions,
                    "refills": item.refills,
                    "genericSubstituteOk": item.genericSubstituteOk
                  },
                  "attributes": {
                    "images": false,
                    "printTemplate": `${process.env.API_URL}/prescription/${item.id}/pdf`,
                    "info": {
                      "text": item.instructions
                    },
                    "note": false
                  }
                });
                return data;
              }, []), 'title')
            })
            return c;
          }, []), 'title') : [];

          record = record.setIn(['prescriptionsColumns'], prescriptionsColumns);

        }
      }

      if (metaData && metaData.get('categories') && name === 'vendors') {

        record = record.setIn(['metaData', 'vendorCategoryOptions'], metaData.get('vendorCategoriesList') && metaData.get('vendorCategoriesList').map((vendorCategoriesList) =>
          vendorCategoriesList
            .set('vendorId', vendorCategoriesList.get('vendorId'))
            .set('categories', vendorCategoriesList.get('categories'))
        ));

        let categoriesList = List();
        metaData.get('categories').map((item) => {
          categoriesList = categoriesList.push(Map().set('value', item.get('id')).set('label', (item.get('name')).toUpperCase()));
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              categoriesList = categoriesList.push(
                Map()
                  .set('value', child.get('id'))
                  .set('label', (`-- ${child.get('name')}`))
              );
            });
          }
        });

        record = record.setIn(['metaData', 'categoryOptions'], categoriesList);

        let categoriesParentList = List();
        metaData.get('categories').map((item) => {
          categoriesParentList = categoriesParentList.push(Map().set('value', item.get('id')).set('label', (item.get('name')).toUpperCase()));
        });

        record = record.setIn(['metaData', 'categoryParentOptions'], categoriesParentList);

        let categoriesChildList = List();
        metaData.get('categories').map((item) => {
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              categoriesChildList = categoriesChildList.push(
                Map()
                  .set('value', child.get('id'))
                  .set('label', child.get('name'))
                  .set('parent', child.get('parent'))
              );
            });
          }
        });

        record = record.setIn(['metaData', 'categoryChildOptions'], categoriesChildList);

        let vendorGroupsList = List();
        metaData.get('vendorGroups').map((item) => {
          vendorGroupsList = vendorGroupsList.push(Map().set('value', item.get('id')).set('label', (item.get('name')).toUpperCase()));
          if (item.get('children') && item.get('children').size) {
            item.get('children').map((child) => {
              vendorGroupsList = vendorGroupsList.push(
                Map()
                  .set('value', child.get('id'))
                  .set('label', (`-- ${child.get('name')}`))
              );
            });
          }
        });

        record = record.setIn(['metaData', 'vendorGroupsList'], vendorGroupsList);
      }

      if (metaData && metaData.get('state') && name === 'vendors') {
        record = record.setIn(['metaData', 'stateOptions'], metaData.get('state') && metaData.get('state').map((state) =>
          state
            .set('value', state.get('state'))
            .set('label', state.get('stateName'))
        ));
      }

      return record;
    }
  );

  const selectError = () => createSelector(
    selectDomain(),
    (domain) => domain.get('error'),
  );

  const selectUpdateError = () => createSelector(
    selectDomain(),
    (domain) => domain.get('updateError'),
  );

  const selectUpdateTimestamp = () => createSelector(
    selectDomain(),
    (domain) => domain.get('lastUpdate'),
  );

  const selectHeaders = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('headers') : false,
  );

  const selectChildren = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('activeChildren') : false,
  );

  const selectSearch = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('searchedRecords') : false,
  );

  const selectSearchValue = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('searchValue') : false,
  );

  const selectDate = () => createSelector(
    selectDomain(),
    (domain) => domain ? domain.get('date') : false,
  );

  const selectForm = (name) => createSelector(
    formDomain(),
    (domain) => domain && name ? domain.get(name) : false,
  );


  return {
    selectDomain,
    selectLoading,
    pageLoading,
    selectRecords,
    selectExternalRecord,
    selectExternalRecords,
    selectRecordsMetaData,
    selectRecord,
    selectError,
    selectUpdateError,
    selectUpdateTimestamp,
    selectHeaders,
    selectChildren,
    selectSearch,
    selectSearchValue,
    selectDate,
    selectForm
  };
}
