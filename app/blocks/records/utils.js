import { currencyFormatter } from 'utils/currencyFormatter.js';
import moment from 'moment-timezone';
import { Map, List } from 'immutable';
import {sortBy} from "lodash.sortby";
import {fromJS} from "immutable";

export function mapOrders(records, vendors) {
  if (records && records.size && !vendors ) {
    return records
  }
  if (records && records.size) {
    return fromJS(_.sortBy(records.toJS(), 'id').reverse()).map((record) => {
      const total = record.get('items').map((item) => item.get('price') * item.get('amount')).reduce((a, b) => a + b, 0);
      if(record.get('dateFulfilled')) {
        record = record.set('dateFulfilled', moment(record.get('dateFulfilled')).format('LLL'));
      } else {
 	      record = record.set('dateFulfilled', '-');
      }
      record = record.set('approvedBy', record.get('approvedBy'));
      record = record.set('summaryPrice', currencyFormatter.format(total || 0));
      record = record.set('name', record.getIn(['dentist', 'name']));
      record = record.set('practice', record.getIn(['dentist', 'practice']));
      record = record.set('dateCreated', moment(record.get('dateCreated')).format('LLL'));
      record = record.update('items', (items) => items.map((item) => item.set('totalPrice', item.get('price') * item.get('amount'))));
      record = record.set('vendors', vendors);
      record = record.set('address', record.get('address'));
      if(record.get('status') == 'Void'){
      	record = record.set('status', 'Closed');	
      }	
      return record;
    });
  }
  return records
}

export function mapConsultations(records) {
  if (records && records.size) {
    return records.map((record) => {
      record = record.set('dentistName', record.getIn(['dentist', 'name']));
      record = record.set('practiceName', record.getIn(['dentist', 'practice']));
      record = record.set('patient', record.getIn(['treatment', 'patientName']));
      record = record.set('datetimeRequested', moment(record.get('dateRequested')).format('LLL'));
      record = record.set('dateRequestedReadable', moment(record.get('dateRequested')).format('ll'));
      record = record.set('dentistPhone', record.getIn(['dentist', 'phone']));
      record = record.set('dentistEmail', record.getIn(['dentist', 'email']));
      record = record.set('cost', record.get('amount'));
      let amount = record.get('amount');
      if(amount != undefined && amount != 'undefined'){
		    record = record.set('amount', amount.toString().indexOf('$') > -1 ? amount : currencyFormatter.format(amount));
      }	else {
      	record = record.set('amount', currencyFormatter.format(0));
      }
      return record;
    });
  }
  return records;
}

export function mapEducationRequests(records) {
  if (records && records.size) {
    return records.map((record) => {
      record = record.set('category', record.getIn(['course', 'category']));
      record = record.set('speaker', record.getIn(['course', 'speaker']));
      record = record.set('courseId', record.getIn(['course', 'courseId']));
      record = record.set('course', record.getIn(['course', 'title']));

      record = record.set('dateRequested', moment(record.get('dateRequested')).format('DD MMM, YYYY'));
      record = record.set('dentistPhone', record.getIn(['dentist', 'phone']));
      record = record.set('dentistEmail', record.getIn(['dentist', 'email']));
      record = record.set('dentist', record.getIn(['dentist', 'name']));
      return record;
    });
  }
  return records;
}

export function mapCeCourses(records) {
  if (records && records.size) {
    return records.map((record) => {
      record = record.set('cost', currencyFormatter.format(record.get('cost')));
      if(record.get('requestId') != 0) {
        record = record.set('requestId', record.get('requestId'));
      } else {
 	      record = record.set('requestId', null);
      }
      return record;
    });
  }
  return records;
}

export function mapPatients(records) {
  if (records && records.size) {
    return records.map((record) => {
      record = record.set('patientId', record.getIn(['patient', 'patientId']));
      record = record.set('dentistName', record.getIn(['dentist', 'name']));
      record = record.set('patientEmail', record.getIn(['patient', 'email']));
      record = record.set('providerName', record.getIn(['patient', 'providerName']));
      record = record.set('patientStatus', record.getIn(['patient', 'patient_status']));
      record = record.set('patientPhone', record.getIn(['patient', 'phone']));
      record = record.set('patientFirstName', record.getIn(['patient', 'name']));
      record = record.set('patientLastName', record.getIn(['patient', 'last_name']) ? record.getIn(['patient', 'last_name']) : '-');
      record = record.set('firstVisit', moment.utc(record.getIn(['patient', 'firstVisit'])).format('ll'));
      record = record.set('lastEntry', moment(record.getIn(['patient', 'lastEntry'])).format('ll'));
      record = record.set('birthDate', moment(record.getIn(['patient', 'birthDate'])).format('ll'));
      record = record.set('shared', record.getIn(['patient', 'shared']) ? 'Yes' : 'No');
      record = record.set('family', record.getIn(['patient', 'family']) ? 'Yes' : 'No');
      record = record.set('sharedBy', record.getIn(['patient', 'sharedBy']));
      record = record.set('sharedDate', record.getIn(['patient', 'sharedDate']) ? moment(record.getIn(['patient', 'sharedDate'])).format('ll') : '-');
      record = record.set('recallDueDate', record.getIn(['patient', 'recallDueDate']) ? moment(record.getIn(['patient', 'recallDueDate'])).format('ll') : '-');
      return record;
    });
  }
  return records;
}


export function mapProducts(records, categories) {
  if (records && records.size) {
    return records.map((record) => {
      if (!record.get('imageUrl')) {
        record = record.set('imageUrl', 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg');
      }
      if (categories && categories.size) {
        const id = record.get('categoryId');
        const categoryOptions = categories.get('categories');
        const targetCategory = categoryOptions.find((category) => category.get('id') === id) || categoryOptions.reduce((r, v) => ((v.get('children') || List()).find(child => child.get('id') === id) || r), Map());
        record = record.set('categoryName', targetCategory.get('name') || 'Unknown');
      }
      return record;
    });
  }
  return records;
}

export function mapDocuments(records, resourceCategories){
  if (records && records.size && !resourceCategories) {
    return records;
  }
  if (records && records.size) {
    return records.map((record) => {
      record = record.set('title', record.get('title'));
      record = record.set('url', record.get('url'));
      record = record.set('category', record.get('category'));
      record = record.set('dateCreated', moment(record.get('dateCreated')).format('LLL'));
      return record;
    });
  }
  return records;
}

export function mapCourses(records, resourceCategories){
  if (records && records.size && !resourceCategories) {
    return records;
  }
  if (records && records.size) {
    return records.map((record) => {
      record = record.set('title', record.get('title'));
      record = record.set('url', record.get('url'));
      record = record.set('thumbnailUrl', record.get('thumbnailUrl'));
      record = record.set('category', record.get('category'));
      record = record.set('dateCreated', moment(record.get('dateCreated')).format('LLL'));
      return record;
    });
  }
  return records;
}

export function mapAccounts(records, metaData){
  const { practices } = metaData || {};
  const clearNull = v => v === null ? undefined : v;
  if (records && records.size){
    return records.map((record) => {
      let expirationDate = moment(record.get('expirationDate'));

      record = record.set('expired', expirationDate.clone().isBefore(moment()));
      record = record.set('daysUntilExpired', expirationDate.clone().diff(moment(), 'days'));
      record = record.set('expirationDate', expirationDate.clone().format('DD MMM, YYYY'));
      if(record.get('accessStartDate')) {
        record = record.set('accessStartDate', moment(record.get('accessStartDate')).format('DD MMM, YYYY'));
      } else {
 	      record = record.set('accessStartDate', '');
      }
	
      record = record.set('country', clearNull(record.get('country')));
      record = record.set('city', clearNull(record.get('city')));
      record = record.set('state', clearNull(record.get('state')));
      record = record.set('zip', clearNull(record.get('zip')));
      record = record.set('address', clearNull(record.get('address')));

      if (practices && practices.size) {
        const id = record.get('practiceId');
        const practice = practices.find((practice) => practice.get('id') === id);
        record = record.set('practice', practice.get('name') || 'Unknown');
      }
      return record;
    });
  }
}

export function mapEmailMappings(records){
  if(records && records.size){
    return records.map(record => {
      if(!record.get('users').get(0).size) record = record.set('users', List());

      let emails = record.get('users').map(user => `${user.get('name', "")}<${user.get('email', "")}>, `).reduce((a,b) => a + b, "");
      record = record.set('emails', emails.slice(0, -2));
      return record
    })
  }
  return records
}

export function mapShipping(records){
  if(records && records.size){
    return records.map(record =>
      record
        .set('isPrimary', record.get('primary'))
        .set('primary', record.get('primary') ? 'Yes' : 'No')
    );
  }
  return records;
}

export function mapConsents(records){
  return records;
}

export function mapNonWorkingDays(records){
  if(records && records.size){  
    return records.map(record => {
      record = record.set('date', moment(record.get('date')).format('MM/DD/YYYY'));
      return record
    })
  }  
  return records
}

export function mapProviders(records){
  if(records && records.size){
    return fromJS(records.toJS()).map((record) => {
      const educationObj = { education: List(record.get('education')).toJS() }
      record = record.set('education', educationObj);
      const experienceObj = { experience: List(record.get('experience')).toJS() }
      record = record.set('experience', experienceObj);
      const awardsObj = { awards: List(record.get('awards')).toJS() }
      record = record.set('awards', awardsObj);
      return record;
    })
  }
  return records
}

export default function mapRecords(records, name, metaData) {
  switch (name) {
    case 'orders':
      return mapOrders(records, metaData);
    case 'consultations':
      return mapConsultations(records);
    case 'inOfficeSupport':
      return mapConsultations(records);
    case 'dentistEducationRequests':
      return mapCeCourses(records);
    case 'products':
      return mapProducts(records, metaData);
    case 'documents':
      return mapDocuments(records, metaData);
    case 'courses':
      return mapCourses(records, metaData);
    case 'patient-courses':
      return mapCourses(records, metaData);
    case 'accounts':
      return mapAccounts(records, metaData);
    case 'emailMappings':
      return mapEmailMappings(records);
    case 'shipping':
      return mapShipping(records);
    case 'consents':
      return mapConsents(records);
    case 'educationRequests':
      return mapEducationRequests(records);
    case 'nonWorkingDays':
      return mapNonWorkingDays(records);
    case 'providers':
      return mapProviders(records);
    default:
      return records;
  }
}
