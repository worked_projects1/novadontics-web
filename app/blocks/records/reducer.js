/*
 *
 *  Records reducer
 *
 */

import { fromJS } from 'immutable';
import { LOAD_FORMSCHEMA_SUCCESS } from 'blocks/forms/actions.js';
import { CHANGE_USER_EMAIL_ERROR, CHANGE_USER_EMAIL_SUCCESS } from 'blocks/emailMappings/actions.js';
import { ARCHIVE_VENDOR_SUCCESS, UNARCHIVE_VENDOR_SUCCESS, PRICE_CHANGE_HISTORY_SUCCESS, DELETE_ALL_PRODUCTS_SUCCESS, DELETE_ALL_PRODUCTS_ERROR } from 'blocks/vendors/actions.js';
import { _map } from '../../module/utils/library';
import moment from 'moment';

const initialState = fromJS({
  loading: false,
  pageLoader: false,
  error: false,
  lastUpdate: null,
  headers: { recPerPage: 25, searchText: '', pageNumber: 1, status: '' },
  activeChildren: parseInt(_map.length),
  searchedRecords: [],
  searchValue: null,
  date: moment.utc().format('YYYY/MM/DD')
});

export default function reducer(constants, name) {
  const {
    LOAD_RECORD,
    LOAD_RECORD_SUCCESS,
    LOAD_RECORD_ERROR,
    LOAD_RECORDS,
    LOAD_RECORDS_SUCCESS,
    LOAD_RECORDS_ERROR,
    CREATE_RECORD,
    CREATE_RECORD_SUCCESS,
    CREATE_RECORD_ERROR,
    UPDATE_RECORD,
    UPDATE_RECORD_SUCCESS,
    UPDATE_RECORD_ERROR,
    UPDATE_RECORDS,
    UPDATE_RECORDS_SUCCESS,
    UPDATE_RECORDS_ERROR,
    DELETE_RECORD,
    DELETE_RECORD_SUCCESS,
    DELETE_RECORD_ERROR,
    LOAD_RECORDS_VALID_CACHE,
    UPDATE_HEADER,
    UPDATE_CHILDREN,
    UPDATE_SEARCH,
    UPDATE_SEARCH_VALUE,
    UPDATE_RECORDS_META_DATA,
    UPDATE_LEDGER_META_DATA,
    UPDATE_LEDGER_LOADING,
    UPDATE_DATE,
    LOAD_DATE,
    LOAD_RECORDS_DATE,
    LOAD_RECORDS_PARAMS,
    LOAD_EXTERNAL_RECORD_SUCCESS,
    LOAD_EXTERNAL_RECORDS_SUCCESS
  } = constants;

  return function recordsReducer(state = initialState, { schema, type, records, recordsMetaData, record, id, error, filter, action, date }) {
    switch (type) {
      case LOAD_RECORDS_VALID_CACHE:
        return state
          .set('loading', false)
          .delete('error')
          .delete('updateError')
          .delete('success');
      case LOAD_RECORD:
        return state
          .set('loading', true)
          .set('pageLoader', true)
          .delete('error')
          .delete('updateError')
          .delete('success');
      case PRICE_CHANGE_HISTORY_SUCCESS:
      case LOAD_RECORD_SUCCESS:
        return state
          .updateIn(['records', state.get('records').findIndex((row) => row.get('id') === record.id)], (rows) => rows.merge(fromJS(Object.assign({}, record))))
          // TODO: This should not be here, it belongs in some sort of "globals" reducer. This is a temporary hack and should be fixed!!!
          .delete('error')
          .delete('updateError')
          .delete('success')
          .set('loading', false)
          .set('pageLoader', false);
      case LOAD_RECORD_ERROR:
        return state
          .set('loading', false)
          .set('error', true)
          .delete('success');
      case LOAD_RECORDS_PARAMS:
      case LOAD_RECORDS_DATE:
      case LOAD_RECORDS:
        return state
          .set('loading', true)
          .delete('error')
          .delete('updateError')
          .delete('success');
      case LOAD_RECORDS_SUCCESS:
        return state
          .set('loading', false)
          .set('records', fromJS(records))
          .set('recordsMetaData', fromJS(recordsMetaData))
          .set('lastUpdate', Math.floor(Date.now() / 1000))
          .delete('error')
          .delete('updateError')
          .delete('success');
      case LOAD_RECORDS_ERROR:
        return state
          .set('loading', false)
          .set('error', true)
          .delete('success');
      case CREATE_RECORD:
        return state
          .set('loading', true)
          .delete('updateError')
          .delete('success');
      case CREATE_RECORD_SUCCESS:
        return state
          .set('loading', false)
          .update('records', fromJS([]), (rows) => rows.unshift(fromJS(record)))
          .delete('updateError')
          .delete('success');
      case CREATE_RECORD_ERROR:
        return state
          .set('loading', false)
          .set('updateError', error)
          .delete('success');
      case CHANGE_USER_EMAIL_SUCCESS:
        return state
          .updateIn(['records', state.get('records').findIndex((row) => row.get('id') === record.id)], (rows) => rows.merge(fromJS(record)))
      case UPDATE_RECORD:
        return state
          .set('loading', true)
          .delete('updateError')
          .delete('success');
      case DELETE_ALL_PRODUCTS_SUCCESS:
      case UPDATE_RECORD_SUCCESS:
        return state
          .set('loading', false)
          .set('success', true)
          .updateIn(['records', state.get('records').findIndex((row) => row.get('id') === record.id)], (rows) => rows.merge(fromJS(record)))
          .delete('updateError');
      case DELETE_ALL_PRODUCTS_ERROR:
      case UPDATE_RECORD_ERROR:
        return state
          .set('loading', false)
          .set('updateError', error)
          .delete('success');
      case UPDATE_RECORDS:
        return state
          .set('loading', true)
          .delete('updateError')
          .delete('success');
      case UPDATE_RECORDS_SUCCESS:
        state = state.set('loading', false);
        return UPDATE_RECORDS_SUCCESS.indexOf('categories') > -1 ? state : state.set('records', state.get('records') ? state.get('records').merge(fromJS(records)) : fromJS(records)); /* TODO fix this logic */
      case UPDATE_RECORDS_ERROR:
        return state
          .set('updateError', error)
          .set('loading', false)
          .delete('success');
      case DELETE_RECORD:
        return state
          .set('loading', true)
          .delete('updateError')
          .delete('success');
      case DELETE_RECORD_SUCCESS:
        return state
          .set('loading', false)
          .update('records', (rows) => (rows ? rows.filterNot((row) => row.get('id') === id) : rows))
          .delete('updateError')
          .delete('success');
      case DELETE_RECORD_ERROR:
        return state
          .set('loading', false)
          .set('updateError', error)
          .delete('success');
      case ARCHIVE_VENDOR_SUCCESS:
        return state
          .updateIn(['records', state.get('records').findIndex((row) => row.get('id') === id)], (rows) => rows.merge(fromJS({ deleted: true })));
      case UNARCHIVE_VENDOR_SUCCESS:
        return state
          .updateIn(['records', state.get('records').findIndex((row) => row.get('id') === id)], (rows) => rows.merge(fromJS({ deleted: false })));
      case LOAD_FORMSCHEMA_SUCCESS:
        if (name === 'consultations') {
          return state
            .set('formSchema', fromJS(schema));
        } else if (name === 'inOfficeSupport') {
          return state
            .set('formSchema', fromJS(schema));
        } else if (name === 'patients') {
          return state
            .set('formSchema', fromJS(schema));
        }
        return state;
      case UPDATE_HEADER:
        return state.update('headers', fromJS({}), (rows) => rows.merge(fromJS(filter)))
      case UPDATE_CHILDREN:
        return state
          .set('loading', false)
          .set('activeChildren', action && (typeof action === 'number' || Array.isArray(action)) ? action : !action && (state.get('activeChildren') === 0 || state.get('activeChildren') === parseInt(_map.length)) ? 0 : action ? state.get('activeChildren') + 1 : 0)
          .delete('error')
          .delete('updateError')
          .delete('success');
      case UPDATE_SEARCH:
        return state
          .set('searchedRecords', action)
      case UPDATE_SEARCH_VALUE:
        return state
          .set('searchValue', action)
      case UPDATE_RECORDS_META_DATA:
        return state
          .set('loading', false)
          .set('recordsMetaData', fromJS(Object.assign({}, state.get('recordsMetaData') && state.get('recordsMetaData').toJS() || {}, recordsMetaData)))
          .delete('error')
          .delete('updateError')
          .delete('success');
      case UPDATE_LEDGER_META_DATA:
        return state
          .set('pageLoader', true)
          .set('recordsMetaData', fromJS(Object.assign({}, state.get('recordsMetaData') && state.get('recordsMetaData').toJS() || {}, recordsMetaData)))
          .delete('error')
          .delete('updateError')
          .delete('success');
      case UPDATE_LEDGER_LOADING:
        return state
          .set('pageLoader', false)
      case LOAD_DATE:
        return state
          .set('date', moment.utc(date).format('YYYY/MM/DD'))
          .set('activeChildren', parseInt(_map.length))
      case UPDATE_DATE:
        return state
          .set('date', moment.utc(date).format('YYYY/MM/DD'))
      case LOAD_EXTERNAL_RECORD_SUCCESS:
        return state
          .set('externalRecord', record)
      case LOAD_EXTERNAL_RECORDS_SUCCESS:
        return state
          .set('externalRecords', fromJS(records))
      case CHANGE_USER_EMAIL_ERROR:
      default:
        return state;
    }
  };
}
