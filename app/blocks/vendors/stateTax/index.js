import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import vendorsBlock from 'blocks/vendors';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import { replace } from 'react-router-redux';

export const LOAD_STATE_TAX = 'ds/LOAD_STATE_TAX';
export const UPDATE_STATE_TAX = 'ds/UPDATE_STATE_TAX';

export const loadStateTax = () => ({
  type: LOAD_STATE_TAX
});

export const updateStateTax = (record) => ({
  type: UPDATE_STATE_TAX,
  record,
});


export const loadStateTaxRemote = () =>
  api.get(`/stateTax`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateStateTaxRemote = (record) =>
  api.put(`/stateTax/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadStateTaxSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_STATE_TAX),
    });
    if (req) {
      try {
        const result = yield call(loadStateTaxRemote);
        yield put(vendorsBlock.actions.updateRecordsMetaData({ stateTax: result }));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* updateStateTaxSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_STATE_TAX),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updateStateTaxRemote, record);          
        const result = yield call(loadStateTaxRemote);
        yield put(vendorsBlock.actions.updateRecordsMetaData({ stateTax: result }));
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(vendorsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}

export function updateVendorCategory(record) {
  const { vendorId } = record;
  return api.put(`/updateVendorCategories/${vendorId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getVendorCategory() {
  return api.get(`/getVendorCategories`).then((response) => response.data).catch((error) => Promise.reject(error));
}



export const stateTaxSaga = [
  loadStateTaxSaga,
  updateStateTaxSaga,
];
