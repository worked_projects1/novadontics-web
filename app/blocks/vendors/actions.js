export const ARCHIVE_VENDOR = 'ds/vendors/ARCHIVE_VENDOR';
export const ARCHIVE_VENDOR_SUCCESS = 'ds/vendors/ARCHIVE_VENDOR_SUCCESS';
export const UNARCHIVE_VENDOR = 'ds/vendors/UNARCHIVE_VENDOR';
export const UNARCHIVE_VENDOR_SUCCESS = 'ds/vendors/UNARCHIVE_VENDOR_SUCCESS';
export const INCREASE_VENDOR_PRICE = 'ds/INCREASE_VENDOR_PRICE';
export const PRICE_CHANGE_HISTORY = 'ds/PRICE_CHANGE_HISTORY';
export const PRICE_CHANGE_HISTORY_SUCCESS = 'ds/PRICE_CHANGE_HISTORY_SUCCESS';
export const DELETE_ALL_PRODUCTS = 'ds/DELETE_ALL_PRODUCTS';
export const DELETE_ALL_PRODUCTS_SUCCESS = 'ds/DELETE_ALL_PRODUCTS_SUCCESS';
export const DELETE_ALL_PRODUCTS_ERROR = 'ds/DELETE_ALL_PRODUCTS_ERROR';

export const archiveVendor = (id, form) => ({
  type: ARCHIVE_VENDOR,
  id,
  form,
});

export const unarchiveVendor = (id, form) => ({
  type: UNARCHIVE_VENDOR,
  id,
  form,
});

export function unarchiveVendorSuccess(id) {
  return {
    type: UNARCHIVE_VENDOR_SUCCESS,
    id,
  };
}

export function archiveVendorSuccess(id) {
  return {
    type: ARCHIVE_VENDOR_SUCCESS,
    id,
  };
}

export const increaseVendorPrice = (record) => ({
  type: INCREASE_VENDOR_PRICE,
  record,
});

export const priceChangeHistory = (record) => ({
  type: PRICE_CHANGE_HISTORY,
  record,
});

export const priceChangeHistorySuccess = (record) => ({
  type: PRICE_CHANGE_HISTORY_SUCCESS,
  record,
});

export const deleteAllProducts = (id) => ({
  type: DELETE_ALL_PRODUCTS,
  id,
});

export const deleteAllProductsSuccess = (record) => ({
  type: DELETE_ALL_PRODUCTS_SUCCESS,
  record
});

export const deleteAllProductsError = (error) => ({
  type: DELETE_ALL_PRODUCTS_ERROR,
  error
});

export default {
  archiveVendor,
  archiveVendorSuccess,
  unarchiveVendor,
  unarchiveVendorSuccess,
  increaseVendorPrice,
  priceChangeHistory,
  priceChangeHistorySuccess,
  deleteAllProducts,
  deleteAllProductsSuccess,
  deleteAllProductsError
};

