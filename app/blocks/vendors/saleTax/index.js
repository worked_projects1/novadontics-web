import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import vendorsBlock from 'blocks/vendors';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import { replace } from 'react-router-redux';

export const LOAD_SALE_TAX = 'ds/LOAD_SALE_TAX';
export const CREATE_SALE_TAX = 'ds/CREATE_SALE_TAX';
export const UPDATE_SALE_TAX = 'ds/UPDATE_SALE_TAX';

export const loadSaleTax = (id) => ({
  type: LOAD_SALE_TAX,
  id
});

export const createSaleTax = (record) => ({
  type: CREATE_SALE_TAX,
  record,
});

export const updateSaleTax = (record) => ({
  type: UPDATE_SALE_TAX,
  record,
});


export const loadSaleTaxRemote = (id) =>
  api.get(`/saleTax/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createSaleTaxRemote = (record) =>
  api.post(`/saleTax`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateSaleTaxRemote = (record) =>
  api.put(`/saleTax/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadSaleTaxSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_SALE_TAX),
    });
    const { id } = req;
    if (id) {
      try {
        const result = yield call(loadSaleTaxRemote, id);
        yield put(vendorsBlock.actions.updateRecordSuccess(Object.assign({}, {id: id}, {saleTax: result})));
      } catch (error) {
        console.log(error);
      }
    }
  }
}


export function* createSaleTaxSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(CREATE_SALE_TAX),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(createSaleTaxRemote, record);          
        const result = yield call(loadSaleTaxRemote, record.vendorId);
        yield put(vendorsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.vendorId}, {saleTax: result})));
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(vendorsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}

export function* updateSaleTaxSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(UPDATE_SALE_TAX),
    });
    const { record } = req;
    if (record) {
      try {
        yield call(updateSaleTaxRemote, record);          
        const result = yield call(loadSaleTaxRemote, record.vendorId);
        yield put(vendorsBlock.actions.updateRecordSuccess(Object.assign({}, {id: record.vendorId}, {saleTax: result})));
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(vendorsBlock.actions.updateRecordError(errorMessage));
      }
    }
  }
}



export const saleTaxSaga = [
  loadSaleTaxSaga,
  createSaleTaxSaga,
  updateSaleTaxSaga,
];
