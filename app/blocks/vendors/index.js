import records from 'blocks/records';
import remotes from './remotes';
import sagas from './sagas';

export default records('vendors', remotes, sagas);
