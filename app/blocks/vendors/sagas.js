import { replace } from 'react-router-redux';
import { call, take, put, race } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import actions from './remotes.js';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import {
  archiveVendorSuccess, unarchiveVendorSuccess, priceChangeHistorySuccess, deleteAllProductsSuccess, deleteAllProductsError,
  ARCHIVE_VENDOR, UNARCHIVE_VENDOR, INCREASE_VENDOR_PRICE, PRICE_CHANGE_HISTORY, DELETE_ALL_PRODUCTS
} from './actions.js';
import vendorsBlock from 'blocks/vendors';

function* archiveVendorSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { archive } = yield race({
      archive: take(ARCHIVE_VENDOR),
    });

    const { id, form } = archive || {};

    if (archive) {
      yield put(startSubmit(form));
      try {
        yield call(actions.archiveRecord, id);
        yield put(stopSubmit(form));
        yield put(archiveVendorSuccess(id));
        yield put(replace('/vendors'));
      } catch (error) {
        yield put(stopSubmit(form, { _error: 'Archiving failed. Reload the page and try again.' }));
      }
    }
  }
}

function* unarchiveVendorSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { unarchive } = yield race({
      unarchive: take(UNARCHIVE_VENDOR),
    });

    const { id, form } = unarchive || {};

    if (unarchive) {
      yield put(startSubmit(form));
      try {
        yield call(actions.unarchiveRecord, id);
        yield put(stopSubmit(form));
        yield put(unarchiveVendorSuccess(id));
        yield put(replace('/vendors'));
      } catch (error) {
        yield put(stopSubmit(form, { _error: 'Unarchiving failed. Reload the page and try again.' }));
      }
    }
  }
}

export function* increaseVendorPriceSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(INCREASE_VENDOR_PRICE),
    });
    const { record } = req;
    if (record) {
      try {
        yield put(replace(`/vendors/${record.id}`)); 
        yield call(actions.increaseVendorPrice,record); 
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* priceChangeHistorySaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(PRICE_CHANGE_HISTORY),
    });
    const { record } = req;
    if (record) {
      try {
        const result = yield call(actions.priceChangeHistory, record.id);
        // yield put(priceChangeHistorySuccess(Object.assign({}, {id:record.id}, {priceChangeHistory: result})));
        const priceChange = Object.assign({}, {id:record.id}, {priceChangeHistory: result})
        yield put(vendorsBlock.actions.updateRecordSuccess(priceChange));
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function* deleteAllProductsSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(DELETE_ALL_PRODUCTS),
    });
    const { id } = req;
    if (id) {
      try {
        yield call(actions.deleteAllProducts, id);  
        yield put(deleteAllProductsSuccess({id:id}));
      } catch (error) {
        const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
        yield put(deleteAllProductsError(errorMessage));
      }
    }
  }
}

export default [
  archiveVendorSaga,
  unarchiveVendorSaga,
  increaseVendorPriceSaga,
  priceChangeHistorySaga,
  deleteAllProductsSaga
];
