import api from 'utils/api';

function loadRecords() {
  return api.get('/vendors').then((response) => response.data);
}

function createRecord(record) {
  return api.post('/vendors', record).then((response) => response.data);
}

function updateRecord(record) {
  return api.put(`/vendors/${record.id}`, record).then((response) => response.data);
}

function deleteRecord(id) {
  return api.delete(`/vendors/${id}`);
}

function archiveRecord(id) {
  return api.delete(`/vendors/${id}`);
}

function unarchiveRecord(id) {
  return api.put(`/vendors/${id}/unarchive`);
}

function increaseVendorPrice(record) { 
  return api.put(`/vendors/increasePrice/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function priceChangeHistory(id) { 
  return api.get(`/vendors/priceChangeHistory/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteAllProducts(id) { 
  return api.delete(`/vendors/deleteAllProducts/${id}`).then((response) => response.data).catch((error) => Promise.reject(error)); 
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
  archiveRecord,
  unarchiveRecord,
  increaseVendorPrice,
  priceChangeHistory,
  deleteAllProducts
};
