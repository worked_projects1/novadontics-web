import api from 'utils/api';

export function loadRecords() {
  return api.get('/tutorials').then((response) => response.data).catch((error) => Promise.reject(error));
}

export function createRecord(record) {
  return api.post('/tutorials', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateRecord(record) {
  return api.put(`/tutorials/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateRecords(records) {
  return api.put('/tutorials', records).then((response) => response.data);
}

function deleteRecord(id) {
  return api.delete(`/tutorials/${id}`).catch((error) => Promise.reject(error));
}


export default {
  loadRecords,
  createRecord,
  updateRecord,
  updateRecords,
  deleteRecord,
};
