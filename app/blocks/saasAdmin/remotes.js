import api from 'utils/api';

function loadRecords() {
  return api.get('/saasAdmin').then((response) => response).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/saasAdmin', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/saasAdmin/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/saasAdmin/${id}`).catch((error) => Promise.reject(error));
}

export function loadAdminSaasPaymentSummary() {
 return api.get(`/saasAdminPaymentSummary`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadAdminSaasPayment(saasId, year) {
  return api.get(`/getSaasAdminPayment?saasAdminId=${saasId}&year=${year}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateAdminSaasPayment(saasId, year, record) {
  return api.put(`/updateSaasAdminPayment?saasAdminId=${saasId}&year=${year}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
  loadAdminSaasPaymentSummary,
};
