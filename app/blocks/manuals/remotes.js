import api from 'utils/api';

export function loadManualRecords() {
  return api.get('/manuals').then((response) => response.data).catch((error) => Promise.reject(error));
}

export function createRecord(record) {
  return api.post('/manuals', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateRecord(record) {
  return api.put(`/manuals/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateRecords(records) {
  return api.put('/manuals', records).then((response) => response.data);
}

function deleteRecord(id) {
  return api.delete(`/manuals/${id}`).catch((error) => Promise.reject(error));
}


export default {
  loadManualRecords,
  createRecord,
  updateRecord,
  updateRecords,
  deleteRecord,
};
