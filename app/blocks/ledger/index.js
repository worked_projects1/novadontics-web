import { call, take, put, race, select } from 'redux-saga/effects';
import { remotes } from 'utils/tools';
import api from 'utils/api';
import records from 'blocks/records';
import { selectUser } from 'blocks/session/selectors';
import patientsRemotes from 'blocks/patients/remotes';
const delay = (ms) => new Promise(response => setTimeout(response, ms))

export const LOAD_LEDGER = 'ds/LOAD_LEDGER';

export const loadLedger = () => ({
  type: LOAD_LEDGER
});

export function* loadLedgerSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { req } = yield race({
      req: take(LOAD_LEDGER),
    });
    if (req) {
      try {
        const ledgerBlock = yield call(records, 'ledger');
        const user = yield select(selectUser());
        let recordsMetaData = yield select(ledgerBlock.selectors.selectRecordsMetaData());
        if (recordsMetaData) {
          const patientList = yield call(patientsRemotes.patientList);
          const recordsMetaData = {
              patientList: patientList && patientList.map(list => Object.assign({}, {label: list.name, value: list.id})) || [] || [] };
          yield put(ledgerBlock.actions.updateLedgerMetaData(recordsMetaData));
          yield put(ledgerBlock.actions.updateLedgerLoading(true));
        }
      } catch (error) {
        console.log(error);
      }
    }
  }
}

export function loadLedgerPayment(record) {
  return api.get(`/ledgerPayment?treatmentPlanId=${record.treatmentPlanId}&patientId=${record.patientId}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function createLedgerPayment(record) {
  return api.post('/ledgerPayment', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateLedgerPayment(record) {
  const { id } = record;
  return api.put(`/ledgerPayment/${id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function deleteLedgerPayment(id) {
  return api.delete(`/ledgerPayment/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export const ledgerSaga = [
  loadLedgerSaga,
];
