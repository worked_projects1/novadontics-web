import api from 'utils/api';

function loadRecords() {
  return api.get('/getMyCECourses').then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/educationRequests/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  deleteRecord
};
