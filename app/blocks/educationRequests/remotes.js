import api from 'utils/api';

function loadRecords() {
  return api.get('/educationRequests').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/educationRequests', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/educationRequests/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/educationRequests/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function consultRecord(record) {
  return api.post('/educationRequests', record).then((response) => response.data);
}

export function ceRequestRecord(courseId) {
  return api.get(`/trackCECourses/${courseId}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function ceRequestLimit() {
  return api.get(`/checkCECourseLimit`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function cePurchase(record) {
  return api.post(`/ceCoursePurchase`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord
};
