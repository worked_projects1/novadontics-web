/*
 *
 * Analytics actions
 *
 */

import {
  LOAD_ANALYTICS,
  LOAD_ANALYTICS_SUCCESS,
  LOAD_ANALYTICS_ERROR,
  LOAD_ANALYTICS_VALID_CACHE,
} from './constants';

export function loadAnalytics() {
  return {
    type: LOAD_ANALYTICS,
  };
}

export function loadAnalyticsSuccess(data) {
  return {
    type: LOAD_ANALYTICS_SUCCESS,
    data,
  };
}

export function loadAnalyticsCacheHit() {
  return {
    type: LOAD_ANALYTICS_VALID_CACHE,
  };
}

export function loadAnalyticsError(error) {
  return {
    type: LOAD_ANALYTICS_ERROR,
    error,
  };
}
