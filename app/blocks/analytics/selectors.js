import { createSelector } from 'reselect';

const selectDomain = () => (state) => state.get('analytics');

const selectLoading = () => createSelector(
  selectDomain(),
  (domain) => domain.get('loading'),
);

const selectData = () => createSelector(
  selectDomain(),
  (domain) => domain.get('data'),
);

const selectError = () => createSelector(
  selectDomain(),
  (domain) => domain.get('error'),
);

const selectUpdateTimestamp = () => createSelector(
  selectDomain(),
  (domain) => domain.get('lastUpdate'),
);

export default {
  selectDomain,
  selectLoading,
  selectData,
  selectError,
  selectUpdateTimestamp,
};
