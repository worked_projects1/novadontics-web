import { call, take, put, race, select } from 'redux-saga/effects';
const VALID_CACHE_DIFF = -30;

import {
  LOAD_ANALYTICS,
} from './constants';

import {
  loadAnalyticsSuccess,
  loadAnalyticsError,
  loadAnalyticsCacheHit,
} from './actions';

import {
  loadAnalytics as analyticsRemote,
} from './remotes';

import selectors from './selectors.js';

const { selectUpdateTimestamp } = selectors;

function* loadAnalyticsSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const load = yield race({
      explicitLoad: take(LOAD_ANALYTICS),
    });
    const { explicitLoad } = load;

    const lastLoad = yield select(selectUpdateTimestamp());
    const currentTimestamp = Math.floor(Date.now() / 1000);

    if (explicitLoad) {
      if ((lastLoad && (lastLoad - currentTimestamp) > VALID_CACHE_DIFF)) {
        yield put(loadAnalyticsCacheHit());
      } else {
        try {
          const analytics = yield call(analyticsRemote);
          if (analytics) {
            yield put(loadAnalyticsSuccess(analytics));
          } else {
            yield put(loadAnalyticsError());
          }
        } catch (error) {
          yield put(loadAnalyticsError(error));
        }
      }
    }
  }
}

export default [
  loadAnalyticsSaga,
];
