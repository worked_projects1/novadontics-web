/*
 *
 * Analytics constants
 *
 */

export const LOAD_ANALYTICS = 'ds/analytics/LOAD_ANALYTICS';
export const LOAD_ANALYTICS_SUCCESS = 'ds/analytics/LOAD_ANALYTICS_SUCCESS';
export const LOAD_ANALYTICS_VALID_CACHE = 'ds/analytics/LOAD_ANALYTICS_VALID_CACHE';
export const LOAD_ANALYTICS_ERROR = 'ds/analytics/LOAD_ANALYTICS_ERROR';
