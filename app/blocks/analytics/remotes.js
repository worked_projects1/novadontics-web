import api from 'utils/api';

export function loadAnalytics() {
  return api.get('/analytics').then((response) => response.data);
}
