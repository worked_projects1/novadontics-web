import { loadAnalytics } from './actions';
import reducer from './reducer';
import sagas from './sagas';
import selectors from './selectors';

export default {
  name: 'analytics',
  actions: {
    loadAnalytics,
  },
  reducer,
  sagas,
  selectors,
};
