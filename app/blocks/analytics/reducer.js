/*
 *
 * Analytics reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_ANALYTICS,
  LOAD_ANALYTICS_SUCCESS,
  LOAD_ANALYTICS_ERROR,
  LOAD_ANALYTICS_VALID_CACHE,
} from './constants';

const initialState = fromJS({
  loading: false,
  error: false,
  lastUpdate: null,
});

export default function analyticsReducer(state = initialState, { type, data }) {
  switch (type) {
    case LOAD_ANALYTICS:
      return state
        .set('loading', true)
        .delete('error');
    case LOAD_ANALYTICS_SUCCESS:
      return state
        .set('loading', false)
        .set('data', fromJS(data))
        .set('lastUpdate', Math.floor(Date.now() / 1000));
    case LOAD_ANALYTICS_ERROR:
      return state
        .set('loading', false)
        .set('error', true);
    case LOAD_ANALYTICS_VALID_CACHE:
      return state
        .set('loading', false)
        .delete('error');
    default:
      return state;
  }
}
