import api from 'utils/api';

function loadRecords() {
  return api.get('/courses/ciicourse').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/courses/ciicourse', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/courses/ciicourse/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateCeInfo(record){
  return api.put(`/updateCeInfo`,record ).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/courses/ciicourse/${id}`).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
