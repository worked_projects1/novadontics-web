import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import patientsBlock from 'blocks/patients';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import { remotes } from 'utils/tools';

export const LOAD_PRESCRIPTION_HISTORY = 'ds/LOAD_PRESCRIPTION_HISTORY';
export const CREATE_PRESCRIPTION_HISTORY = 'ds/CREATE_PRESCRIPTION_HISTORY';
export const UPDATE_PRESCRIPTION_HISTORY = 'ds/UPDATE_PRESCRIPTION_HISTORY';
export const DELETE_PRESCRIPTION_HISTORY = 'ds/DELETE_PRESCRIPTION_HISTORY';
export const LOAD_PROFILE = 'ds/LOAD_PROFILE';
export const UPDATE_PROFILE = 'ds/UPDATE_PROFILE';

export const loadPrescriptionHistory = (id) => ({
    type: LOAD_PRESCRIPTION_HISTORY,
    id,
});


export const createPrescriptionHistory = (record) => ({
    type: CREATE_PRESCRIPTION_HISTORY,
    record,
});


export const updatePrescriptionHistory = (record) => ({
    type: UPDATE_PRESCRIPTION_HISTORY,
    record,
});


export const deletePrescriptionHistory = (record) => ({
    type: DELETE_PRESCRIPTION_HISTORY,
    record,
});

export const loadProfile = (id) => ({
    type: LOAD_PROFILE,
    id
});

export const updateProfile = (record, practiceId) => ({
    type: UPDATE_PROFILE,
    record,
    practiceId,
});

export const loadPrescriptionHistoryRemote = (id) =>
    api.get(`/prescriptionHistory/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const createPrescriptionHistoryRemote = (record) =>
    api.post(`/prescriptionHistory`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const updatePrescriptionHistoryRemote = (record) =>
    api.put(`/prescriptionHistory/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const deletePrescriptionHistoryRemote = (record) =>
    api.delete(`/prescriptionHistory/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadProfileRemote = () =>
    api.get(`/profile`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateProfileRemote = (record) =>
    api.put(`/providers/${record.providerId}`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export function* loadPrescriptionHistorySaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_PRESCRIPTION_HISTORY),
        });
        const { id } = req;
        if (id) {
            try {
                const result = yield call(loadPrescriptionHistoryRemote, id);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: id }, { prescriptionHistory: result })));
            } catch (error) {
                console.log(error)
            }
        }
    }
}

export function* createPrescriptionHistorySaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(CREATE_PRESCRIPTION_HISTORY),
        });
        const { record } = req;
        if (record) {
            try {
                yield call(createPrescriptionHistoryRemote, record);
                const result = yield call(loadPrescriptionHistoryRemote, record.patientId);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.patientId }, { prescriptionHistory: result })));
            } catch (error) {
                console.log(error)
            }
        }
    }
}

export function* updatePrescriptionHistorySaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(UPDATE_PRESCRIPTION_HISTORY),
        });
        const { record } = req;
        if (record) {
            try {
                yield call(updatePrescriptionHistoryRemote, record);
                const result = yield call(loadPrescriptionHistoryRemote, record.patientId);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.patientId }, { prescriptionHistory: result })));
            } catch (error) {
                console.log(error)
            }
        }
    }
}


export function* deletePrescriptionHistorySaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(DELETE_PRESCRIPTION_HISTORY),
        });
        const { record } = req;
        if (record) {
            try {
                yield call(deletePrescriptionHistoryRemote, record);
                const result = yield call(loadPrescriptionHistoryRemote, record.patientId);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: record.patientId }, { prescriptionHistory: result })));
            } catch (error) {
                console.log(error)
            }
        }
    }
}


export function* loadProfileSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_PROFILE),
        });
        const { id } = req;
        if (id) {
            try {
                const result = yield call(loadProfileRemote, id);
                yield put(patientsBlock.actions.updateRecordSuccess(Object.assign({}, { id: id }, { profile: result })));
            } catch (error) {
                console.log(error)
            }
        }
    }
}


export function* updateProfileSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(UPDATE_PROFILE),
        });
        const { record, practiceId } = req;
        if (record) {
            try {
                yield call(updateProfileRemote, record);
                const providersRemotes = yield call(remotes, 'providers');
                const providersList = yield call(providersRemotes.loadRecordsParams, {practiceId: practiceId || ''});
                yield put(patientsBlock.actions.updateRecordsMetaData(Object.assign({}, { providersList: providersList.data })));
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
                yield put(patientsBlock.actions.updateRecordError(errorMessage));
            }
        }
    }
}


export const prescriptionHistorySaga = [
    loadPrescriptionHistorySaga,
    createPrescriptionHistorySaga,
    updatePrescriptionHistorySaga,
    deletePrescriptionHistorySaga,
    loadProfileSaga,
    updateProfileSaga
];
