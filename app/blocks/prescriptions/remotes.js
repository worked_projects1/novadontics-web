import api from 'utils/api';

function loadRecords() {
  return api.get('/prescriptions').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/prescriptions', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/prescriptions/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/prescriptions/${id}`).catch((error) => Promise.reject(error));
}


export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
