import api from 'utils/api';

function loadRecords() {
  return api.get('/resource-categories/procedureCodeCategory').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.put('/insuranceFee', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/insuranceFee`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/insuranceFee/${id}`).catch((error) => Promise.reject(error));
}

export function loadFeeFields(code) {
  return api.get(`/insuranceFee?procedureCode=${code}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateFeeDetails(code, record) {
  return api.put(`/updateInsuranceFeeByPCode?procedureCode=${code}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
