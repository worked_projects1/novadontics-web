import api from 'utils/api';

function loadRecords() {
  return api.get('/users').then((response) => response.data);
}

function createRecord(record) {
  return api.post('/users', record).then((response) => response.data);
}

function updateRecord(record) {
  return api.put(`/users/${record.id}`, record).then((response) => response.data);
}

function deleteRecord(id) {
  return api.delete(`/users/${id}`);
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
