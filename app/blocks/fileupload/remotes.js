import api, { uploadFile } from 'utils/api';

function getSignature(filename, contentType) {
  return api.get(`/signature/${filename}` + `?Content-Type=${contentType}`).then((response) => response.data);
}

export default {
  getSignature,
  uploadFile,
};
