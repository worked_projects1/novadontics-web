import api from 'utils/api';
import axios from 'axios';
import {convertObjectToParams} from 'utils/tools';

function loadRecords() {
  return api.get('/products').then((response) => response);
}

function createRecord(record) {
  return api.post('/products', record).then((response) => response.data);
}

function updateRecord(record) {
  return api.put(`/products/${record.id}`, record).then((response) => response.data);
}

function updateRecords(records) {
  return api.put('/products', records).then((response) => response.data);
}

function deleteRecord(id) {
  return api.delete(`/products/${id}`).then((response) => response.data);
}

export function addFavourite(productId) {
  return api.post(`/products/${productId}/favourite`).then((response) => response.data);
}

export function removeFavourite(productId) {
  return api.delete(`/products/${productId}/favourite`).then((response) => response.data);
}

export function Cart() {
  return api.get(`/cartWithProducts`).then((response) => response.data);
}

export function productTotalCount() {
  return api.get(`/productTotalCount`).then((response) => response.data);
}

export function getProductDetail(id) {
  return api.get(`/products/${id}`).then((response) => response.data);
}

export async function addCart(record) {
  return await api.post(`/cart`,record).then((response) => response.data);
}

export function removeCart(productId) {
  return api.delete(`/cart/${productId}`).then((response) => response.data);
}

export function findProducts(record){
  return api.post(`/findProducts`,record).then((response) => response);
}

export function findSaleTax(id){
  return api.get(`stateTax/findSaleTax/${id}`).then((response) => response);
}
export function findProductSaletax(record)
{
  return api.get(`/findProductSaleTax?${convertObjectToParams(record)}`).then((response) => response);
}

export function loadAuthorize(){
  return api.get(`/getAuthorizeNetDetails`).then((response) => response.data);
}

export function authorizeApi(url, json, config) {
  return axios.post(url, json, config).then((response) => response.data);
}

export function updateCustId(record) {
  return api.post(`/accounts/updateAuthorizeCustId`,record).then((response) => response);
}

export function addSaveLater(productId) {
  return api.post(`/saveLater`, {productId: productId}).then((response) => response.data);
}

export function moveSaveLater(productId) {
  return api.put(`/saveLater/${productId}`).then((response) => response.data);
}

export function deleteSaveLater(productId) {
  return api.delete(`/saveLater/${productId}`).then((response) => response.data);
}


export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
  updateRecords
};
