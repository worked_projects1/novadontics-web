import api from 'utils/api';

function loadRecords() {
  return api.get('/categories').then((response) => response.data);
}

export function updateRecord(record) {
  return api.put(`/categories/${record.id}`, record).then((response) => response.data);
}

export function createRecord(records) {
  return api.post('/categories', records).then((response) => response.data);
}

export function updateRecords(records) {
  return api.put('/categories', records).then((response) => response.data);
}

function deleteRecord(id) {
  return api.delete(`/categories/${id}`).catch((error) => Promise.reject(error));
}

export function findCategories(record){
  return api.post(`/getCategories`,record).then((response) => response.data);
}

export default {
  loadRecords,
  updateRecord,
  createRecord,
  updateRecords,
  deleteRecord,
};
