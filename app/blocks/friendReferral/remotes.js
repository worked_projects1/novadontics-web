import api from 'utils/api';

export function loadRecords() {
  return api.get('/friendReferral').then((response) => response).catch((error) => Promise.reject(error));
}

export function friendReferral(record){
  return api.post('/friendReferral', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  friendReferral
};
