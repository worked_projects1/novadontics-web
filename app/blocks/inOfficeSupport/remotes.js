import api from 'utils/api';

export function loadRecords() {
  return api.get('/consultationsFilter/inOfficeSupport').then((response) => {
    const { data } = response;
    return data;
  });
}

export function loadRecord(id) {
  return api.get(`/consultations/${id}`).then((response) => {
    const { data } = response;
    return data;
  });
}

export function createRecord(record){
  return api.post(`/consultations`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateRecord(record) {
  return api.put(`/consultations/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/consultations/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}


export default {
  loadRecords,
  loadRecord,
  createRecord,
  updateRecord,
  deleteRecord
};
