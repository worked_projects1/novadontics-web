import api from 'utils/api';

function loadRecords() {
  return api.get('/courses/resource').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/courses/resource', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/courses/resource/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/courses/resource/${id}`).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
