import records from 'blocks/records';

import remotes from './remotes';

export default records('patient-courses', remotes);
