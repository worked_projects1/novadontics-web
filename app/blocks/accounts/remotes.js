import api from 'utils/api';

function loadRecords() {
  return api.get('/accounts').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/accounts', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/accounts/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/accounts/${id}`).catch((error) => Promise.reject(error));
}

function loadPracticeAccounts(id){
  return api.get(`/accounts?practice=${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * Todo Fix this logic, This is for Temporary
 * @param {id} id 
 */
export function loadPaymentHistory(id){
  return api.get(`/renewalDetails/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * Todo Fix this logic, This is for Temporary
 * @param {record} record 
 */

export function updatePaymentHistory(record){
  return api.put(`/renewalDetails/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
  loadPracticeAccounts,
};
