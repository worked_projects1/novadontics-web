import api from 'utils/api';

export function loadRecords() {
  return api.get('/boneGraftData').then((response) => response).catch((error) => Promise.reject(error));
}

export function createBoneGraftData(record) {
  return api.post(`/boneGraftingCreate`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateBoneGraftData(record) {
  const { id } = record;
  return api.put(`/updateBoneGraftData/${id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createBoneGraftData,
  updateBoneGraftData
};
