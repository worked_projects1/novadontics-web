import api from 'utils/api';

function loadRecords() {
  return api.get('/courses/doctor').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/courses/doctor', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/courses/doctor/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/courses/doctor/${id}`).catch((error) => Promise.reject(error));
}

export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
