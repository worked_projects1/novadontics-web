import { call, take, put, race } from 'redux-saga/effects';
import { removeUser, addUser } from './remotes.js';
import actions, { ADD_USER_EMAIL, REMOVE_USER_EMAIL} from './actions.js';

function* removeUserEmailSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const load = yield race({
      request: take(REMOVE_USER_EMAIL),
    });

    const { request } = load;

    if (request) {
      const {record, userId} = request;
      try {
        const result = yield call(removeUser, record.id, userId);

        if(result === 200){
          const users = record.users.filter(u => u.id !== userId);
          let data = Object.assign(record, {'users': users.length === 0 ? [{}] : users});
          yield put(actions.changeUserEmailSuccess(data));
        }
      } catch (error) {
        yield put(actions.changeUserEmailError("Couldn't remove. Please try again."));
      }
    }
  }
}

function* addUserEmailSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const load = yield race({
      request: take(ADD_USER_EMAIL),
    });

    const { request } = load;

    if (request) {
      const {record, user} = request;
      try {
        const result = yield call(addUser, record.id, user.id);

        if(result === 200 || result === 201){
          record.users.push(user);
          yield put(actions.changeUserEmailSuccess(record));
        }
      } catch (error) {
        yield put(actions.changeUserEmailError("Couldn't remove. Please try again."));
      }
    }
  }
}

export default [
  addUserEmailSaga,
  removeUserEmailSaga,
];
