export const ADD_USER_EMAIL = 'emailMappings/ADD_USER_EMAIL';
export const REMOVE_USER_EMAIL = 'emailMappings/REMOVE_USER_EMAIL';
export const CHANGE_USER_EMAIL_ERROR = 'emailMappings/CHANGE_USER_EMAIL_ERROR';
export const CHANGE_USER_EMAIL_SUCCESS = 'emailMappings/CHANGE_USER_EMAIL_SUCCESS';


function addUserEmail(record, user){
  return {
    type: ADD_USER_EMAIL,
    record,
    user
  }
}


function removeUserEmail(record, userId){
  return {
    type: REMOVE_USER_EMAIL,
    record,
    userId
  }
}

function changeUserEmailError(error){
  return {
    type: CHANGE_USER_EMAIL_ERROR,
    error,
  }
}

function changeUserEmailSuccess(record){
  return {
    type: CHANGE_USER_EMAIL_SUCCESS,
    record,
  }
}

export default {
  addUserEmail,
  removeUserEmail,
  changeUserEmailError,
  changeUserEmailSuccess,
};
