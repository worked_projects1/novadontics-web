import api from 'utils/api';

export function loadRecords() {
  return api.get('/emailMappings').then((response) => response.data);
}

export function updateRecord(record) {
  return api.put(`/emailMappings/${record.id}`, record).then((response) => response.data);
}

export function addUser(id, userId) {
  return api.put(`/emailMappings/${id}/add?userId=${userId}`).
  then((response) => {
    const { status } = response;
    return status;
  });
}

export function removeUser(id, userId) {
  return api.put(`/emailMappings/${id}/rem?userId=${userId}`).
  then((response) => {
    const { status } = response;
    return status;
  });
}


export default {
  loadRecords,
  updateRecord,
  addUser,
  removeUser,
};
