import api from 'utils/api';

function loadRecords() {
  return api.get('/practices').then((response) => response.data).catch((error) => Promise.reject(error));
}

function createRecord(record) {
  return api.post('/practices', record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function updateRecord(record) {
  return api.put(`/practices/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

function deleteRecord(id) {
  return api.delete(`/practices/${id}`).catch((error) => Promise.reject(error));
}


export default {
  loadRecords,
  createRecord,
  updateRecord,
  deleteRecord,
};
