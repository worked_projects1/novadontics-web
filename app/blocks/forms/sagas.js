import { call, take, put, race } from 'redux-saga/effects';
import { loadFormSchema } from './remotes.js';
import actions, { LOAD_FORMSCHEMA } from './actions.js';

function* loadFormSchemaSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const load = yield race({
      explicitLoad: take(LOAD_FORMSCHEMA),
    });
    const { explicitLoad } = load;

    if (explicitLoad) {
      try {
        const schema = yield call(loadFormSchema);
        if (schema) {
          yield put(actions.loadFormSchemaSuccess(schema));
        } else {
          yield put(actions.loadRecordsError());
        }
      } catch (error) {
        yield put(actions.loadFormSchemaError(error));
      }
    }
  }
}

export default [
  loadFormSchemaSaga,
];
