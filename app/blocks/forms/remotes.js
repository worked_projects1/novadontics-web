import api from 'utils/api';

export function loadFormSchema() {
  return api.get('/forms').then((response) => {
    const { data } = response;
    return data;
  });
}
