export const LOAD_FORMSCHEMA = 'consultations/LOAD_FORMSCHEMA';
export const LOAD_FORMSCHEMA_SUCCESS = 'consultations/LOAD_FORMSCHEMA_SUCCESS';
export const LOAD_FORMSCHEMA_ERROR = 'consultations/LOAD_FORMSCHEMA_ERROR';

function loadFormSchema() {
  return {
    type: LOAD_FORMSCHEMA,
  };
}

function loadFormSchemaSuccess(schema) {
  return {
    type: LOAD_FORMSCHEMA_SUCCESS,
    schema,
  };
}

function loadFormSchemaError(error) {
  return {
    type: LOAD_FORMSCHEMA_ERROR,
    error,
  };
}

export default {
  loadFormSchema,
  loadFormSchemaSuccess,
  loadFormSchemaError,
};
