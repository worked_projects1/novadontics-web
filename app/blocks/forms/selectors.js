export default (rootPath) => (state) => state.getIn([rootPath, 'formSchema']);
