import api from 'utils/api';

export const loadRecords = (type) => () =>
  api.get(`/resource-categories/${type}`).then((response) => response.data);

export const updateRecord = (type) => (record) =>
  api.put(`/resource-categories/${type}/${record.id}`, record).then((response) => response.data);

export const createRecord = (type) => (records) =>
  api.post(`/resource-categories/${type}`, records).then((response) => response.data);

export const updateRecords = (type) => (records) =>
  api.put(`/resource-categories/${type}`, records).then((response) => response.data);

export const deleteRecord = (type) => (id) =>
  api.delete(`/resource-categories/${type}/${id}`).catch((error) => Promise.reject(error));

export default (type) => ({
  loadRecords: loadRecords(type),
  updateRecord: updateRecord(type),
  createRecord: createRecord(type),
  updateRecords: updateRecords(type),
  deleteRecord: deleteRecord(type),
});
