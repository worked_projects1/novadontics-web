import records from 'blocks/records';

import remotes from '../remotes';

export default records('resource-categories/patient-courses', remotes('courses'));
