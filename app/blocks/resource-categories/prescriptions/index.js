import records from 'blocks/records';

import remotes from '../remotes';

export default records('resource-categories/prescriptions', remotes('prescriptions'));
