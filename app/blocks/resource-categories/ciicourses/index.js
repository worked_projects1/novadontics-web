import records from 'blocks/records';

import remotes from '../remotes';

export default records('resource-categories/ciicourses', remotes('ciicourse'));
