# Consultations

## JSON Schema for Steps

```javascript

  Array [
    {
      formId: `String`, // e.g. '1A'
      formVersion: `Number`,
      dateCreated: `String`, // timestamp
      value: {
        // object of key-value pairs from schema; e.g. { full_name: 'Jon Snow', email: 'jonsnow@nightswatch.gov' }
      }
    },
    // …
  ],

```

## JSON Schema for Form Schemas

```javascript
  Array [
    {
      version: `Number`,
      title: `String`, // e.g. "Visit One - Data Collection"
      forms: [
        icon: `String`, // e.g. "PersonalInfo"
        id: `String`, // e.g. "1A", maps to the formId value from "steps"
        title: `String`, // e.g. "1A Personal Info",
        version: `Number`,
        schema: [
          {
            title: `String`, // e.g. "1A Personal Info",
            rows: [
              {
                columns: [
                  columnItems: [
                    {
                      /*
                        Object of key value pairs and display schema.
                        E.g.
                          {
                            name:"full_name",
                            required: true,
                            title: "Full Name",
                            type: "Input",
                          }
                      */
                    },
                    // …
                  ],
                ],
              },
              // …
            ]
          }
        ],
      ],
    },
    // …
  ]

```

