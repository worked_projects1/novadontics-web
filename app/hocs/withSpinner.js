import React from 'react';
import {branch, renderComponent} from 'recompose';
import Spinner from "../components/Spinner";

export default Component => branch(
  props => props.loading,
  renderComponent(Spinner)
)(Component)