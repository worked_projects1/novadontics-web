import React from 'react';
import {Alert, Icon} from "@sketchpixy/rubix/lib/index";
import {DEFAULT_ANALYTICS_ERROR} from "../utils/errors";
import {Link} from "react-router";
import withSpinner from "./withSpinner";
import {branch, compose, renderComponent} from "recompose";

const Error = ({}) => (
  <Alert danger>
    <p><Icon glyph="icon-fontello-flag" /> { DEFAULT_ANALYTICS_ERROR }</p>
    <p><strong><Link style={{ color: '#a94442' }} to="/">Click to retry now.</Link></strong></p>
  </Alert>
);

export default Component => compose(
  withSpinner,
  branch(
    props => props.error,
    renderComponent(Error)
  )
)(Component)