/*
 * ViewPracticesPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import DeleteRecordForm from 'components/DeleteRecordForm';

import styles from './styles.css';
import {Icon} from "@sketchpixy/rubix/lib/index";


export default function (path, columns, deleteRecord, selectRecord) {
  function handleDelete(data, dispatch, { form }) {
    const { id } = data.toJS();
    dispatch(deleteRecord(id, form));
  }

  class ViewPracticePage extends React.Component {
    static propTypes = {
      record: PropTypes.object,
      dispatch: PropTypes.func,
      params: PropTypes.object,
    };

    constructor(props){
      super(props);
      this.state = {};
    }

    componentDidMount() {
      const {id} = this.props.params;
      this.state = {id: id};
    }

    render() {
      const {record = {}} = this.props;
      // React.cloneElement(this.props, {data: this.state.data, setData: this.updateData});

      return(
      <div className={styles.viewPracticePage}>
        <div className={styles.actions}>
          <div className="closeDetailViewButton">
            <Link to={`${path}`}>Close</Link>
          </div>
          <Link to={{ pathname: '/accounts', state:{data: {practiceId: record.id, practiceName: record.name}}}} className={styles.viewAccounts}><Icon glyph="icon-fontello-users" style={{paddingRight:"8px"}}/>Add or View
            Accounts</Link>
          <Link  to={`${path}/${record.id}/edit`}>Edit</Link>
          <DeleteRecordForm initialValues={{id: record.id}} form={`deleteRecord.${record.id}`} onSubmit={handleDelete}/>
        </div>

        {columns.map((column) =>
          (column.viewRecord ?
            <div key={column.id}>
              <div>
                <div className={styles.label}>{column.label}:</div>
                <p>{record[column.value]}</p>
                <hr />
              </div>
            </div>
            : null)
        )}
      </div>
      )
    }

  }
  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : {},
    };
  }


  return connect(mapStateToProps)(ViewPracticePage);
}
