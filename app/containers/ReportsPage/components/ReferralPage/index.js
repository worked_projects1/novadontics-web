/**
*
* ReferralPage
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import { Link } from 'react-router';
import styles from './styles.css';
import moment from 'moment-timezone';
import Info from 'containers/PatientsPage/components/Info';
import ReactTable from "react-table";
import Spinner from 'components/Spinner';
import { loadReferralSource, loadReferralTo } from 'blocks/patients/remotes';

const ButtonItem = ({config, open}) => {
  const { title, glyphIcon, imageIcon, btnType, width, height, printIcon, referIcon, inviteButton } = config;
  switch(btnType){
    case 'link':
      return <Link className={referIcon ? styles.referStyles : printIcon ? styles.printLink : styles.link} onClick={open}>
               {title || ''}
             </Link>
    case 'image':
      return <Col onClick={open}>
              {imageIcon && imageIcon.map((image,index)=><img key={index} width={width} height={height} src={image} alt={title || ''} className={styles.img} />)}
             </Col>
    default:
      return <Button bsStyle="standard" onClick={open} className={inviteButton ? `action-button ${styles.button}` : `action-button`}>{title || ''}</Button>
  }
}

const Children = ({children}) => children ? <Col style={{textAlign:"center"}} dangerouslySetInnerHTML={{__html: children}} /> : null;

const Tooltip = ({text, show}) => show ? <span className={styles.tooltip}>{text}</span> : null;

class ReferralPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, loading: false, confirmDialogBox : false, disableShare: false, records: [] };
  }

  componentDidMount(){
    if(this.props.onRef) {
      this.props.onRef(this);
    }
  }

  componentWillReceiveProps(props) {
    if(this.props.error != props.error && this.props.config && this.props.config.showError) {
      this.props.config.showError(props.error);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { fields, initialValues, handleSubmit, error, updateError, pristine, submitting, config = {}, noteText, notes, dispatch, form, sharedId, infoColumns, metaData, getCategories } = this.props;
    const { confirmDialogBox, disableShare, loading, records } = this.state;
    const { recallProcedureOptions = [] } = metaData || {}
    const { centerAlign, confirmDialog, title, bsSize, tooltip, viewType, shareDet, topView, bottomView, closeModal, modalClass, btnName, className, style, disable, titleStyle, hideSaveButton, hideCancelBtn, modalTitle } = config;
    const RowComponent = viewType == 'tab' ? Tabs : Row;
    const ColComponent = viewType == 'tab' ? Tab : 'div';
    const tableColumns = fields.filter((column) => column.visible);

    return (
      <Col style={style} className={title == 'Extend Validity' ? null : `${className ? className : ''} ${styles.ModalRecordForm}`}>
        <Children children={topView} />
        <ButtonItem config={config} open={this.open.bind(this)} />
        <Children children={bottomView} />
        <Tooltip text={title} show={tooltip} />
        <Modal show={this.state.showModal} bsSize={bsSize} onHide={this.close.bind(this)} backdrop="static" className="referralSource">
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span className={styles.header} style={{color:"#ea6225"}}>{modalTitle}</span>
            </Modal.Title>
          </Modal.Header>
		      <Modal.Body id="modalrecordform">
            <Col>
              {!loading ?
                <ReactTable
                  columns={tableColumns.map((column) => Object.assign(
                      {},
                      { Header: column.label, accessor: column.value, width: column.width, ...column }
                  ))}
                  data={records}
                  pageSize={records && records.length > 0 ? 10 : 0}
                  showPageJump={false}
                  resizable={false}
                  showPageSizeOptions={false}
                  previousText={"Back"}
                  pageText={""}
                  noDataText={"No entries to show."}
                  sortable={false}
                /> : <Spinner />}
            </Col>
          </Modal.Body>
        </Modal>
      </Col>
    );
  }

  showconfirmDialog() {
    this.setState({ confirmDialogBox: true });
  }

  close() {
    this.setState({ showModal: false });
  }

  open(e) {
    e.stopPropagation();
    const { columns, referralName, referredId, formName } = this.props;
    var submitData = this.props.columns && this.props.columns.reduce((a, el) => Object.assign({}, a, { [el.value]: el.type === "date" ? document.getElementById(el.value).value : document.getElementsByName(el.value)[0].value }), {});
    submitData.fromDate = moment(submitData.fromDate).format('YYYY-MM-DD')
    submitData.toDate = moment(submitData.toDate).format('YYYY-MM-DD')
    if(formName === 'referralSource') {
      submitData.referralSource = referralName
      this.setState({ showModal: true, loading: true });
      loadReferralSource(submitData)
        .then(response => {
          this.setState({ records: response, loading: false })
        })
        .catch(() => this.setState({ records: [], loading: false }));
    }
    if(formName === 'referredToo') {
      submitData.referredTo = referredId
      this.setState({ showModal: true, loading: true });
      loadReferralTo(submitData)
        .then(response => {
          this.setState({ records: response, loading: false })
        })
        .catch(() => this.setState({ records: [], loading: false }));
    }
  }

}

ReferralPage.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  updateError: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  record: PropTypes.object,
  fields: PropTypes.array,
  config: PropTypes.object
};

export default reduxForm({
  form: 'modalRecord',
  enableReinitialize: true,
})(ReferralPage);
