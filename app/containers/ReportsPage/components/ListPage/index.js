import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Alert } from '@sketchpixy/rubix';
import ReactTable from "react-table";
import { FormattedMessage } from 'react-intl';
import messages from 'containers/ViewRecordPage/messages';
import Spinner from 'components/Spinner';
import EditRecordForm from 'components/EditRecordForm';
import moment from 'moment-timezone';
import styles from '../../styles.css';
import ReferralPage from '../ReferralPage';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import ReactTableWrapper from "components/ReactTableWrapper";
import ReactPagination from "components/ReactTableWrapper/pagination";
import Gallery from 'components/Gallery';
import library from 'routes/schema_1';

class ListPage extends React.Component {
    constructor(props) {
        super(props);
    }

    editableColumnProps = {
      Cell: props => {
        const { column } = props;
        const { columns, formColumns, name } = this.props;
        return (
          column.label == 'Final PA' && column.showImage == true ?
              <Gallery data={{ images: [`${props.value && props.value || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}`] }} width="60" implantStyle={true} /> :
          column.currency ? <span>{currencyFormatter.format(props.value)}</span> :
          column.percent ? <span>{props.value ? props.value + ' %' : ''}</span> :
          column.type == 'date' ? <span>{props.value && props.value != '-' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span> :
          column.action && column.referralSource ?
            <Col className={styles.actions}>
              <ReferralPage
                fields={library().referralSourceDetail().columns}
                columns={formColumns}
                form={`referralName.${props.original.referralSource}`}
                referralName={props.original.referralSource}
                formName={name}
                //onRef={referralName => (this[`referralName.${props.original.referralSource}`] = referralName)}
                config={{ title: "View", modalTitle: props.original.referralSource, btnType: 'link' }}
              />
            </Col> :
          column.action && column.referredTo ?
            <Col className={styles.actions}>
              <ReferralPage
                fields={library().referralSourceDetail().columns}
                columns={formColumns}
                form={`referralName.${props.original.referredTo}`}
                referredId={props.original.id}
                formName={name}
                //onRef={referralName => (this[`referralName.${props.original.referralSource}`] = referralName)}
                config={{ title: "View", modalTitle: props.original.referredTo, btnType: 'link' }}
              />
            </Col> : <span>{props.value}</span>
        );
      }
    }

    render() {
        const { name, path, error, loading, records, columns, formColumns, headers, handleFilter, handleFormSubmit, submitRecord, metaData = {} } = this.props;
        const { totalcount, recPerPage, searchText, status } = headers;

        if (loading) {
            return <Spinner />
        }

        const totalRows = records && records.length > 0 ? records.reduce((a, el)=>Object.assign({}, a, Object.keys(a).reduce((k, m) => Object.assign({}, k, {[m] : Number(a[m]) + Number(el[m])}), {})), columns.filter(_ => _.currency).reduce((a, el) => Object.assign({}, a, {[el.value]: 0}), {})) : {};
        const data = records && records.length > 0 && Object.keys(totalRows).length > 0 ? records.concat([Object.assign({}, {totalColumn: true}, totalRows)]) : records;
        const pageSize = data.length > 0 && recPerPage && data.length < parseInt(recPerPage) ? data.length : data.length > 0 && recPerPage && data.length >= parseInt(recPerPage) ? parseInt(recPerPage) : data.length > 0 ? 25 : 0;
        return <Col>
            {error ?
                <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> : records ?
                    <Col className={name == 'implantData' ? "ImplantReport" : null}>
                        {formColumns ? <Row><Col sm={6} className={styles.formColumn}><EditRecordForm
                            initialValues={submitRecord || formColumns.reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{})}
                            name={name}
                            fields={formColumns}
                            form={name}
                            onSubmit={handleFormSubmit.bind(this)}
                            path={path}
                            disableCancel={true}
                            metaData={metaData && metaData.referralSourcesList && metaData.referredToList && metaData.providersList && metaData.implantBrandList && Object.assign({}, metaData, {referralSourcesList: metaData.referralSourcesList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}, {referredToList: metaData.referredToList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}, {implantBrandList: metaData.implantBrandList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) ||
                            metaData && metaData.vendorOptions && metaData.consultantList && metaData.doctorsList && Object.assign({}, metaData, {vendorOptions: metaData.vendorOptions.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])}, {consultantList: metaData.consultantList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{label:'All', value:'all'}])}, {doctorsList: metaData.doctorsList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{label:'All', value:'all'}])}) || metaData}
                            btnName={'Done'}
                        /> </Col></Row> : null}
                        {name === "consultationsRevenue" ?
                          <Col className={styles.revenueColumn}>
                            <Col className={styles.revenueColumn}>
                              <Col sm={6} className={styles.contentTop}>
                                <h4>Total Number of Consultations: {records.totalNumberOfConsultations}</h4>
                              </Col>
                              <Col sm={6} className={styles.contentTop}>
                                <h4>Total Revenue: {currencyFormatter.format(records.totalRevenue)}</h4>
                              </Col>
                            </Col>
                          </Col> :
                          name === "inOfficeSupportRevenue" ?
                            <Col className={styles.revenueColumn}>
                              <Col className={styles.revenueColumn}>
                                <Col sm={6} className={styles.contentTop}>
                                  <h4>Total Number of In-Office Support Request: {records.totalInOfficeSupport}</h4>
                                </Col>
                                <Col sm={6} className={styles.contentTop}>
                                  <h4>Total Revenue: {currencyFormatter.format(records.totalRevenue)}</h4>
                                </Col>
                              </Col>
                            </Col> :
                          name === "catalogOrdersRevenue" ?
                            <Col className={styles.revenueColumn}>
                              <Col className={styles.revenueStyle}>
                                <h4>Total Number of Orders: {records.totalOrders}</h4>
                                <h4>Total Revenue: {currencyFormatter.format(records.totalRevenue)}</h4>
                                <h4>Total Tax: {currencyFormatter.format(records.totalTax)}</h4>
                                <h4>Total Shipping: {currencyFormatter.format(records.totalShipping)}</h4>
                                <h4>Total Savings: {currencyFormatter.format(records.totalSaving)}</h4>
                                <h4>Total Net Gross: {currencyFormatter.format(records.totalNetGross)}</h4>
                                <h4>Net Profit: {currencyFormatter.format(records.totalNetProfit)}</h4>
                              </Col>
                            </Col> :
                          name == "orderHistory" || name == "implantData" ?
                          <ReactTableWrapper
                            columns={
                              columns.map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, ...column, ...this.editableColumnProps }
                              ))}
                            data={data}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            filter={handleFilter.bind(this)}
                            pageSize={pageSize}
                            pageText={""}
                            PaginationComponent={ReactPagination}
                            noDataText={loading ? "Fetching entries." : "No entries to show."}
                            sortable={false}
                            hideFilters={true}
                            headers={headers}
                            path={path}
                            name={name}
                            loadingRecords={loading}
                            defaultStatusKey={0}
                          />  :
                          <ReactTable
                            columns={columns.map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                            ))}
                            data={data}
                            getTdProps={(state, rowProps) => {
                                return {
                                    style: rowProps && rowProps.original.totalColumn ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold'} : {}
                                }
                            }}
                            pageSize={data && data.length ? data.length : 0}
                            showPageJump={false}
                            resizable={false}
                            sortable={false}
                            showPageSizeOptions={false}
                            showPagination={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        />}
                    </Col> : null}
        </Col>
    }
}

function mapStateToProps(state, props) {
    const { selectors = {} } = props;
    const { selectHeaders, selectRecords, selectError, selectLoading } = selectors;
    let records = selectRecords()(state, props);
    const error = selectError()(state, props);
    const loading = selectLoading()(state, props);
    const headers = selectHeaders()(state, props);
    return {
        records: records.toJS() || false,
        error: error,
        loading: loading,
        headers: headers ? headers.toJS() : false
    }
}

export default connect(mapStateToProps)(ListPage);