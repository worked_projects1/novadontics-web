import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Alert, Table, Button, Icon } from '@sketchpixy/rubix';
import ReactTable from "react-table";
import { FormattedMessage } from 'react-intl';
import messages from 'containers/ViewRecordPage/messages';
import Spinner from 'components/Spinner';
import EditRecordForm from 'components/EditRecordForm';
import moment from 'moment-timezone';
import styles from '../../styles.css';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import ReactTableWrapper from "components/ReactTableWrapper";
import ReactPagination from "components/ReactTableWrapper/pagination";

class TreatmentPlanPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { name, path, error, loading, records, columns, formColumns, headers, handleFilter, handleFormSubmit, submitRecord, metaData = {} } = this.props;
        const isPreDefinedSet = Array.isArray(records);
        const { totalcount, recPerPage, searchText, status } = headers;

        if (loading) {
            return <Spinner />
        }

        return <Col>
            {error ?
                <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> : records ?
                    <Col>
                        {formColumns ? <Row><Col sm={6} className={styles.formColumn}><EditRecordForm
                            initialValues={submitRecord || formColumns.reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{})}
                            name={name}
                            fields={formColumns}
                            form={name}
                            onSubmit={handleFormSubmit.bind(this)}
                            path={path}
                            disableCancel={true}
                            metaData={metaData && metaData.providersList && Object.assign({}, metaData, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) ||
                            metaData && metaData.vendorOptions && metaData.consultantList && metaData.doctorsList && Object.assign({}, metaData, {vendorOptions: metaData.vendorOptions.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])}, {consultantList: metaData.consultantList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{label:'All', value:'all'}])}, {doctorsList: metaData.doctorsList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{label:'All', value:'all'}])}) || metaData}
                            btnName={'Done'}
                        /> </Col></Row> : null}
                        <div>
                          {records && records.length > 0 ?
                            records.map((record) => {
                              return (
                                <div>
                                  <Table className={`${styles.Table}`} condensed responsive>
                                    <thead>
                                      <tr className={styles.treatmentPlanRow}>
                                        <th style={{width: '12%'}}>Patient Name</th>
                                        <th style={{width: '8%'}}>Date</th>
                                        <th style={{width: '5%'}}>Tooth</th>
                                        <th style={{width: '6%'}}>Code</th>
                                        <th style={{width: '55%'}}>Description</th>
                                        <th style={{width: '19%'}}>Amount</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>{record.patientName}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      {record && record.treatmentPlan && record.treatmentPlan.length > 0 ?
                                        record.treatmentPlan.map((plan) => {
                                          return (
                                            <tr>
                                              <td></td>
                                              <td>{plan.date}</td>
                                              <td>{plan.toothNumber}</td>
                                              <td>{plan.procedureCode}</td>
                                              <td>{plan.procedureDescription}</td>
                                              <td>{currencyFormatter.format(plan.fee)}</td>
                                            </tr>
                                          )
                                        }) : null}
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td className={styles.totalValue}>Treatment Plan Total</td>
                                        <td style={{ fontWeight: '800' }}>{currencyFormatter.format(record.treatmentPlanTotal)}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                  {record && record.insurance && record.insurance.length > 0 ?
                                    <div className={`${styles.insuranceTable} unscheduledPlanReport`}>
                                      <Table className={`${styles.Table} unscheduledPlanReport`} condensed responsive>
                                        <thead>
                                          <tr className={styles.insuranceRow}>
                                            <th className={styles.insuranceColumn}>Insurance Company Name</th>
                                            <th className={styles.insuranceColumn}>Renewal Date</th>
                                            <th className={styles.insuranceColumn}>Family Annual Max</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            {record.insurance.map((plan) => {
                                              return (
                                                <tr>
                                                  <td>{plan.name}</td>
                                                  <td>{plan.renewalDate}</td>
                                                  <td>{currencyFormatter.format(plan.familyAnnualMax)}</td>
                                                </tr>
                                              )
                                            })}
                                        </tbody>
                                      </Table>
                                    </div> : null}
                                </div>
                              )
                            }) : null}
                        </div>
                    </Col> : null}
        </Col>
    }
}

function mapStateToProps(state, props) {
    const { selectors = {} } = props;
    const { selectHeaders, selectRecords, selectError, selectLoading } = selectors;
    let records = selectRecords()(state, props);
    const error = selectError()(state, props);
    const loading = selectLoading()(state, props);
    const headers = selectHeaders()(state, props);
    return {
        records: records.toJS() || false,
        error: error,
        loading: loading,
        headers: headers ? headers.toJS() : false
    }
}

export default connect(mapStateToProps)(TreatmentPlanPage);
