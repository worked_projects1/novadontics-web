import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Alert } from '@sketchpixy/rubix';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/ViewRecordPage/messages';
import ModalForm from 'components/ModalRecordForm';
import moment from 'moment-timezone';
import styles from '../../styles.css';
import { convertObjectToParams } from 'utils/tools';
import AlertMessage from 'components/Alert';

class PdfPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = { alertMessage: false, alertText: "" };
    }

    handleTotalCollectedAmount(data, type) {
      const { formColumns, metaData = {}, form, forms, routes, token} = this.props;
      const record = data.toJS();
      if(routes === 'depositSlip') {
        if(record.payMethod == undefined || record.payMethod == "") {
          this.setState({ alertMessage: true, alertText: "Please select payment method" });
        } else {
          window.open(`${process.env.API_URL}/${routes}${type}?X-Auth-Token=${token}&${convertObjectToParams(record)}`, '_blank')
        }
      } else {
        window.open(`${process.env.API_URL}/${routes}${type}?X-Auth-Token=${token}&${convertObjectToParams(record)}`, '_blank')
      }
    }

    render() {
        const { formColumns, metaData = {}, form, forms, routes, token } = this.props;
        const { alertMessage, alertText } = this.state;

        return <Col className={styles.formRow}>
            {routes === 'depositSlip' || routes === 'unScheduledTreatmentPlan' || routes === 'implantData' ? null :
              <ModalForm
                initialValues={formColumns && formColumns.reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{}) || {}}
                fields={forms[routes]}
                form={`CSV-${form}`}
                metaData={metaData && metaData.providersList && Object.assign({}, metaData, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) ||
                          metaData && metaData.vendorOptions && Object.assign({}, metaData,{consultantList: metaData.consultantList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])},{doctorsList: metaData.doctorsList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])} ,{vendorOptions: metaData.vendorOptions.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])}) || metaData}
                onSubmit={(data) => this.handleTotalCollectedAmount(data, 'CSV')}
                config={{
                    title: "CSV", btnType: "image", imageIcon: [require(`img/patients/csv.svg`)], width: '28px', height: '28px', btnName: 'Print', style: { marginRight: '3px', marginTop: '3px' }
                }}
              />}
            {routes === 'catalogOrdersRevenue' || routes === 'consultationsRevenue' || routes === 'inOfficeSupportRevenue' || routes === 'orderHistory' ? null :
              <ModalForm
                initialValues={formColumns && formColumns.reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{}) || {}}
                fields={forms[routes]}
                form={`PDF-${form}`}
                metaData={metaData && metaData.providersList && metaData.implantBrandList && Object.assign({}, metaData, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}, {implantBrandList: metaData.implantBrandList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) || metaData}
                onSubmit={(data) => this.handleTotalCollectedAmount(data, 'PDF')}
                config={{
                    title: "PDF", btnType: "image", imageIcon: [require(`img/patients/pdf1.svg`)], width: '35px', height: '35px', btnName: 'Print', style: { marginRight: '0px' }
                }}
              /> }
            {this.state.alertMessage ?
              <AlertMessage warning dismiss={() => { this.setState({ alertMessage: false }); }}>{this.state.alertText}</AlertMessage> : null}
        </Col>
    }
}

export default PdfPage;
