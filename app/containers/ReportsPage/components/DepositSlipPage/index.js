import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Alert } from '@sketchpixy/rubix';
import ReactTable from "react-table";
import { FormattedMessage } from 'react-intl';
import messages from 'containers/ViewRecordPage/messages';
import Spinner from 'components/Spinner';
import EditRecordForm from 'components/EditRecordForm';
import moment from 'moment-timezone';
import styles from '../../styles.css';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import ReactTableWrapper from "components/ReactTableWrapper";
import ReactPagination from "components/ReactTableWrapper/pagination";

class DepositSlipPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { name, path, error, loading, records, columns, formColumns, headers, handleFilter, handleFormSubmit, submitRecord, metaData = {} } = this.props;
        const isPreDefinedSet = Array.isArray(records);
        const { totalcount, recPerPage, searchText, status } = headers;
        let depositArr = records && records.deposits && records.deposits.length > 0 ? records.deposits : [];
        let depositPayMethods = depositArr.length > 0 ? depositArr.map(({ payMethod, total }) => ({payMethod, total})) : [];
        let refundArr = records && records.refund && records.refund.length > 0 ? records.refund : [];
        let refundPayMethods = refundArr.length > 0 ? refundArr.map(({ payMethod, total }) => ({payMethod, total})) : [];
        let depositTotal = records && records.depositTotal;
        let refundTotal = records && records.refundTotal;
        let grandTotal = records && records.grandTotal;
        let depositEntries = records && records.depositTotalEntries;
        let refundEntries = records && records.refundTotalEntries;
        let totalPaymentEntries = depositEntries + refundEntries;

        if (loading) {
            return <Spinner />
        }

        return <Col>
            {error ?
                <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> : records ?
                    <Col>
                        {formColumns ? <Row><Col sm={6} className={styles.formColumn}><EditRecordForm
                            initialValues={submitRecord || formColumns.reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{})}
                            name={name}
                            fields={formColumns}
                            form={name}
                            onSubmit={handleFormSubmit.bind(this)}
                            path={path}
                            disableCancel={true}
                            metaData={metaData && metaData.providersList && Object.assign({}, metaData, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) ||
                            metaData && metaData.vendorOptions && metaData.consultantList && metaData.doctorsList && Object.assign({}, metaData, {vendorOptions: metaData.vendorOptions.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])}, {consultantList: metaData.consultantList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{label:'All', value:'all'}])}, {doctorsList: metaData.doctorsList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{label:'All', value:'all'}])}) || metaData}
                            btnName={'Done'}
                        /> </Col></Row> : null}
                        <div className={"DepositSlip"}>
                          {records && records.deposits && records.deposits.length > 0 ?
                            <Row>
                              <Col sm={12} className={styles.flexibleTitleTop}>
                                <h1 className={styles.title}>Deposits</h1>
                              </Col>
                              {records.deposits.map((deposit) => {
                                return (
                                  <Row>
                                    <Col sm={12} className={styles.flexibleTop} style={{ marginLeft: '15px' }}>
                                      <h1 className={styles.title}>{deposit.payMethod}</h1>
                                    </Col>
                                    <Col className={styles.colMargin}>
                                      <ReactTable
                                        columns={columns.map((column) => Object.assign(
                                            {},
                                            { Header: column.label, accessor: column.value, width: column.width, ...column }
                                        ))}
                                        data={deposit.entries}
                                        getTdProps={(state, rowProps) => {
                                            return {
                                                style: rowProps && rowProps.original.totalColumn ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold'} : {}
                                            }
                                        }}
                                        pageSize={deposit.entries && deposit.entries.length || 0}
                                        showPageJump={false}
                                        resizable={false}
                                        sortable={false}
                                        showPageSizeOptions={false}
                                        showPagination={false}
                                        previousText={"Back"}
                                        pageText={""}
                                        noDataText={"No entries to show."}
                                      />
                                      <div style={{ paddingLeft: '10px', paddingTop: '20px' }}><span>{deposit.totalEntries} items</span></div>
                                    </Col>
                                  </Row>
                                );
                              })}
                            </Row> : null}
                          {records && records.refund && records.refund.length > 0 ?
                            <Row>
                              <Col sm={12} className={styles.flexibleTitleTop}>
                                <h1 className={styles.title}>Refunds</h1>
                              </Col>
                              {records.refund.map((refundData) => {
                                return (
                                  <Row>
                                    <Col sm={12} className={styles.flexibleTop} style={{ marginLeft: '15px' }}>
                                      <h1 className={styles.title}>{refundData.payMethod}</h1>
                                    </Col>
                                    <Col className={styles.colMargin}>
                                      <ReactTable
                                        columns={columns.map((column) => Object.assign(
                                            {},
                                            { Header: column.label, accessor: column.value, width: column.width, ...column }
                                        ))}
                                        data={refundData.entries}
                                        getTdProps={(state, rowProps) => {
                                            return {
                                                style: rowProps && rowProps.original.totalColumn ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold'} : {}
                                            }
                                        }}
                                        pageSize={refundData.entries && refundData.entries.length || 0}
                                        showPageJump={false}
                                        resizable={false}
                                        sortable={false}
                                        showPageSizeOptions={false}
                                        showPagination={false}
                                        previousText={"Back"}
                                        pageText={""}
                                        noDataText={"No entries to show."}
                                      />
                                      <div style={{ paddingLeft: '10px', paddingTop: '20px' }}><span>{refundData.totalEntries} items</span></div>
                                    </Col>
                                  </Row>
                                );
                              })}
                            </Row> : null}
                        </div>
                        {totalPaymentEntries != 0 ?
                          <Row><Col><div style={{ paddingLeft: '20px', paddingTop: '15px' }}><span>{totalPaymentEntries} total items</span></div></Col></Row> : null}
                        <div className={styles.alignTotal}>
                          {depositPayMethods && depositPayMethods.length > 0 ?
                            <div>
                              {depositPayMethods.map((depositPay) => {
                                return (
                                  <div className={styles.alignPayment}>
                                    <span>{depositPay.payMethod} Total:</span>
                                    <span className={styles.alignAmount}>{currencyFormatter.format(depositPay.total)}</span>
                                  </div>
                                )
                              })}
                            </div> : null
                          }
                          {depositTotal != 0 ?
                            <div className={styles.alignPayment}>
                              <span style={{ fontWeight: '700' }}>Deposit Total:</span>
                              <span className={styles.alignAmount}>{currencyFormatter.format(depositTotal)}</span>
                            </div> : null}
                          {refundPayMethods && refundPayMethods.length > 0 ?
                            <div>
                              {refundPayMethods.map((refundPay) => {
                                return (
                                  <div className={styles.alignPayment}>
                                    <span>{refundPay.payMethod} Total:</span>
                                    <span className={styles.alignAmount}>{currencyFormatter.format(refundPay.total)}</span>
                                  </div>
                                )
                              })}
                            </div> : null
                          }
                          {refundTotal != 0 ?
                            <div className={styles.alignPayment}>
                              <span style={{ fontWeight: '700' }}>Refund Total:</span>
                              <span className={styles.alignAmount}>{currencyFormatter.format(refundTotal)}</span>
                            </div> : null}
                          {grandTotal != 0 ?
                            <div className={styles.alignPayment}>
                              <span style={{ fontWeight: '700' }}>Grand Total:</span>
                              <span className={styles.alignAmount}>{currencyFormatter.format(grandTotal)}</span>
                            </div> : null}
                        </div>
                    </Col> : null}
        </Col>
    }
}

function mapStateToProps(state, props) {
    const { selectors = {} } = props;
    const { selectHeaders, selectRecords, selectError, selectLoading } = selectors;
    let records = selectRecords()(state, props);
    const error = selectError()(state, props);
    const loading = selectLoading()(state, props);
    const headers = selectHeaders()(state, props);
    return {
        records: records.toJS() || false,
        error: error,
        loading: loading,
        headers: headers ? headers.toJS() : false
    }
}

export default connect(mapStateToProps)(DepositSlipPage);
