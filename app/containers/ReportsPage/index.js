/*
 * ReportsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';

import { Row, Grid, Col } from '@sketchpixy/rubix';
import styles from './styles.css';

import { selectUser } from 'blocks/session/selectors';
import { Collapse } from 'antd';
import ListPage from './components/ListPage';
import DepositSlipPage from './components/DepositSlipPage';
import TreatmentPlanPage from './components/TreatmentPlanPage';
import PdfPage from './components/PdfPage';
import moment from 'moment-timezone';
import ModalForm from 'components/ModalRecordForm';
import { loadReports } from 'blocks/reports';
import { convertObjectToParams } from 'utils/tools';
import { setApiFilter } from 'utils/api';
import AlertMessage from 'components/Alert';


const { Panel } = Collapse;
export default function (name, path, columns, forms, selectors, sagas) {
    const {selectHeaders,selectRecordsMetaData} = selectors;

    const viewIcons = (form, routes, token, metaData, formColumns) => {
      return forms[routes] ? (<Col>
        <PdfPage form={form} forms={forms} routes={routes} token={token} metaData={metaData} formColumns={formColumns} />
      </Col>) : (<Col>
        <Link to={`${process.env.API_URL}/${routes}CSV?X-Auth-Token=${token}`} target="_blank">
            <img src={require('img/patients/csv.svg')} className={styles.csv} onClick={(e) => e.stopPropagation()} />
        </Link>
        <Link to={`${process.env.API_URL}/${routes}PDF?X-Auth-Token=${token}`} target="_blank">
            <img src={require('img/patients/pdf1.svg')} className={styles.pdf} onClick={(e) => e.stopPropagation()} />
        </Link>
      </Col>)
    }

    class ReportsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

        static propTypes = {
            records: PropTypes.array,
            children: PropTypes.object,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool
        };

        constructor(props) {
            super(props);
            this.state = { activeKeys: [], submitRecord: false, alertMessage: false, alertText: "" };
            this.loadRecords = this.loadRecords.bind(this);
            this.handleFilters = this.handleFilters.bind(this);
        }

        handleFormSubmit(data, dispatch, { form }) {
            const submitRecord = data.toJS();
            if(form === 'depositSlip') {
              if(submitRecord.payMethod == undefined || submitRecord.payMethod == "") {
                this.setState({ alertMessage: true, alertText: "Please select payment method" });
              } else {
                this.setState({ submitRecord: Object.assign({}, this.state.submitRecord, {[form]: submitRecord}) });
                const activeSagas = Object.keys(sagas).reduce((a, el) => Object.assign({}, a, sagas[el].find(_ => _.name === form)), {});
                const { actions } = activeSagas || {};
                setApiFilter({ 'pageNumber': 1, 'recPerPage': 25 }, true);//set filter in api
                dispatch(actions.loadRecordsParams(submitRecord, true));
              }
            } else {
              this.setState({ submitRecord: Object.assign({}, this.state.submitRecord, {[form]: submitRecord}) });
              const activeSagas = Object.keys(sagas).reduce((a, el) => Object.assign({}, a, sagas[el].find(_ => _.name === form)), {});
              const { actions } = activeSagas || {};
              setApiFilter({ 'pageNumber': 1, 'recPerPage': 25 }, true);//set filter in api
              dispatch(actions.loadRecordsParams(submitRecord, true));
            }
        }

        componentDidMount(){
           this.props.dispatch(loadReports());
        }

        shouldComponentUpdate(nextProps, nextState) {
            return shallowCompare(this, nextProps, nextState);
        }

        render() {
            const { user = {}, metaData = {}, headers } = this.props;
            const { secret } = user;
            return (<div>
                <Helmet title="Novadontics" />
                {Object.keys(columns).map((section, index) => {
                    let displayName = section;
                    if (section === 'dailyReports') {
                        displayName = 'daily reports';
                    } else if (section === 'accounting') {
                        displayName = 'accounting reports';
                    } else if (section === 'patient') {
                        displayName = 'patient reports';
                    } else if (section === 'appointment') {
                        displayName = 'appointment reports';
                    } else if (section === 'catalog') {
                        displayName = 'catalog reports';
                    } else if (section === 'consultation') {
                        displayName = 'consultation reports';
                    } else if (section === 'inOfficeSupport') {
                        displayName = 'in-office support reports';
                    } else if (section === 'orders') {
                        displayName = 'order reports';
                    } else if (section === 'implantReports') {
                        displayName = 'logbook reports';
                    }
                    return <div key={index} className={styles.view}>
                        <Grid>
                            <Row>
                                <Col sm={12} className={styles.flexibleRow}>
                                    <h1 className={styles.titleHead}>{displayName}</h1>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={12} className={styles.timeline}>
                                    <Collapse onChange={this.loadRecords.bind(this)} expandIconPosition={'left'}>
                                        {Object.keys(columns[section]).map((column, i) => {
                                            let header = column;
                                            if (column === 'depositSlip') {
                                                header = "Deposit Slip Report";
                                            } else if (column === 'unScheduledTreatmentPlan') {
                                                header = "Unscheduled Treatment Plan Report";
                                            } else if (column === 'outstandingInsurance') {
                                                header = "Outstanding Balances from Insurances Report";
                                            } else if (column === 'PatientBalance') {
                                                header = "Outstanding Balances from Patients Report";
                                            } else if (column === 'totalCollectedAmount') {
                                                header = "Total Production / Collection Amount for a Time Period Report";
                                            } else if (column === 'PatientType') {
                                                header = "Patient Type Report";
                                            } else if (column === 'recallReport') {
                                                header = "Recall Period for a Time Period Report";
                                            } else if (column === 'referralSource') {
                                                header = "Patients by Referral Source for a Time Period Report";
                                            } else if (column === 'referredToo') {
                                                header = "Patients by Referred To for a Time Period Report";
                                            } else if (column === 'newPatient') {
                                                header = "Patients for a Time Period Report";
                                            } else if (column === 'providerAppointment') {
                                                header = "Provider Appointments for a Time Period Report";
                                            } else if (column === 'cancelReport') {
                                                header = "Cancelled / No Show Appointments for a Time Period Report";
                                            } else if (column === 'catalogOrdersRevenue') {
                                                header = "Catalog Orders Revenue Report";
                                            } else if (column === 'consultationsRevenue') {
                                                header = "Consultations Revenue Report";
                                            } else if (column === 'inOfficeSupportRevenue') {
                                                header = "In-Office Support Revenue Report";
                                            } else if (column === 'orderHistory') {
                                                header = "Order History Report";
                                            } else if (column === 'implantData') {
                                                header = "Implant Logbook Report";
                                            }

                                            const { name, actions, selectors } = sagas[section][i] || {};
                                            return <Panel header={header} key={`${section}-${i}`} extra={name == undefined  ? null : viewIcons(`${section}-${i}`, name, secret, metaData, forms[name])}>
                                                {name == 'depositSlip' ?
                                                  <DepositSlipPage
                                                    name={name}
                                                    path={path}
                                                    columns={columns[section][name]}
                                                    formColumns={forms[name]}
                                                    actions={actions}
                                                    headers={headers}
                                                    selectors={selectors}
                                                    handleFilter={this.handleFilters.bind(this)}
                                                    handleFormSubmit={this.handleFormSubmit.bind(this)}
                                                    submitRecord={this.state.submitRecord && this.state.submitRecord[name]}
                                                    {...this.props} /> :
                                                 name == 'unScheduledTreatmentPlan' ?
                                                  <TreatmentPlanPage
                                                    name={name}
                                                    path={path}
                                                    columns={columns[section][name]}
                                                    formColumns={forms[name]}
                                                    actions={actions}
                                                    headers={headers}
                                                    selectors={selectors}
                                                    handleFilter={this.handleFilters.bind(this)}
                                                    handleFormSubmit={this.handleFormSubmit.bind(this)}
                                                    submitRecord={this.state.submitRecord && this.state.submitRecord[name]}
                                                    {...this.props} /> : <ListPage
                                                    name={name}
                                                    path={path}
                                                    columns={columns[section][name]}
                                                    formColumns={forms[name]}
                                                    actions={actions}
                                                    headers={headers}
                                                    selectors={selectors}
                                                    handleFilter={this.handleFilters.bind(this)}
                                                    handleFormSubmit={this.handleFormSubmit.bind(this)}
                                                    submitRecord={this.state.submitRecord && this.state.submitRecord[name]}
                                                    {...this.props} /> }
                                            </Panel>
                                        })}
                                    </Collapse>
                                </Col>
                            </Row>
                        </Grid>
                        {this.state.alertMessage ?
                          <AlertMessage warning dismiss={() => { this.setState({ alertMessage: false }); }}>{this.state.alertText}</AlertMessage> : null}
                    </div>
                })}
            </div>);
        }

        handleFilters(type, input) {
            const { dispatch } = this.props;
            const { orderHistory,implantData } = this.state.submitRecord;
            const activeSagas = Object.keys(sagas).reduce((a, el) => Object.assign({}, a, sagas[el].find(_ => _.name === "orderHistory" || _.name === "implantData")), {});
            const { actions } = activeSagas || {};
            dispatch(actions.updateHeader({ [type]: input }));//set filter in reducer
            setApiFilter({ [type]: input }, true);//set filter in api
            if(orderHistory != undefined) {
              dispatch(actions.loadRecordsParams(orderHistory, true));
            } else if(implantData != undefined) {
                dispatch(actions.loadRecordsParams(implantData, true));
            } else {
            dispatch(forms["orderHistory"] ? actions.loadRecordsParams(forms["orderHistory"].reduce((a, el) => Object.assign({}, a, el.type === 'date' ? { [el.value]: moment().format('YYYY-MM-DD') } : {}), {}), true) : forms["implantData"] ? actions.loadRecordsParams(forms["implantData"].reduce((a, el) => Object.assign({}, a, el.type === 'date' ? { [el.value]: moment().format('YYYY-MM-DD') } : {}), {}), true) : actions.loadRecords());
            }
        }

        loadRecords(activeKeys) {
            const { dispatch } = this.props;
            this.setState({ activeKeys: activeKeys });
            activeKeys.map(key => {
                const keys = key.split('-');
                const { name, actions } = sagas[keys[0]][keys[1]] || {};
                setApiFilter({ 'pageNumber': 1, 'recPerPage': 25 }, true);//set filter in api
                dispatch(forms[name] ? actions.loadRecordsParams(forms[name].reduce((a, el) => Object.assign({}, a, el.type === 'date' ? { [el.value]: moment().format('YYYY-MM-DD') } : {}), {})) : actions.loadRecords());
            })
        }


    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    return connect(
        createSelector(
            selectUser(),
            selectRecordsMetaData(),
            selectHeaders(),
            (user, metaData, headers) => ({
                user,
                metaData: metaData ? metaData.toJS() : false,
                headers: headers ? headers.toJS() : false })
        ), mapDispatchToProps)(ReportsPage);
}
