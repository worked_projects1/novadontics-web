/*
 *
 * ForgotPasswordPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Alert } from '@sketchpixy/rubix';

import AnonymousRouteWrapper from 'components/AnonymousRouteWrapper';
import CredentialsForm from 'components/CredentialsForm';
import { requestPasswordReset } from 'blocks/session/actions';
import { selectError, selectSuccess } from 'blocks/session/selectors';


export class ForgotPasswordPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    error: PropTypes.object,
    success: PropTypes.object,
  };

  static handleSubmit(data, dispatch, { form }) {
    const { identifier } = data.toJS();
    dispatch(requestPasswordReset(identifier, form));
  }

  constructor(props) {
    super(props);

    this.handleSubmit = ForgotPasswordPage.handleSubmit.bind(this);
  }

  render() {
    const { error = {}, success = {} } = this.props;
    const { requestPassword: requestPasswordError } = error;
    const { requestPassword: requestPasswordSuccess } = success;
    return (
      <AnonymousRouteWrapper>
        <div>
          <div className="anonymous-form-wrapper">
            <CredentialsForm type="forgot" submitLabel="Request reset" onSubmit={this.handleSubmit} />
          </div>
          {requestPasswordError ?
            <Alert danger>
              <p>{requestPasswordError}</p>
            </Alert> : null}

          {requestPasswordSuccess ?
            <Alert success>
              <p>Request submitted successfully – we sent a <strong>password reset link</strong> to you. Check your email inbox.</p>
            </Alert> : null}
        </div>
      </AnonymousRouteWrapper>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  createSelector(
    selectError(),
    selectSuccess(),
    (error, success) => ({ error, success }),
  ),
  mapDispatchToProps,
)(ForgotPasswordPage);
