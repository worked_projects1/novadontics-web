/*
 * ViewPatientsForm Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Alert, Row, Col, Button } from '@sketchpixy/rubix';
import moment from 'moment-timezone';
import { browserHistory } from 'react-router';
import { selectUser } from 'blocks/session/selectors';
import { FormattedMessage } from 'react-intl';
import messages from '../RecordsPage/messages';
import Spinner from 'components/Spinner';
import styles from './styles.css';
import ViewDocuments from './components/ViewDocuments'
import { loadAuthorize, authorizeApi } from 'blocks/products/remotes.js';
import { ceRequestLimit } from 'blocks/educationRequests/remotes.js';

export default function (name, path, columns, updateRecord, { selectRecords, selectRecordsMetaData }, actions, selectLoading, selectError, selectUpdateError) {

    class ViewDocumentsPage extends React.Component {
        constructor(props) {
            super(props);
            this.state = { cardDetails: [], authDetails: [], activeCard: 0, defaultCard: 0, activePayment: [], customerId: 0, paymentId: 0, transId: 0, transCode: '', loading: false, limitReached: false };
        }

        componentDidMount() {
            document.getElementById("app").style.overflow = "hidden";
            document.getElementById("body").firstChild.style.overflow = "hidden";
            if(name === 'courses') {
              this.loadAuthorizeDetails();
              this.loadCeLimit();
            }
        }

        componentWillUnmount() {
            document.getElementById("app").style.overflow = "initial";
            document.getElementById("body").firstChild.style.overflow = "initial";
        }

        loadCeLimit = () => {
          ceRequestLimit()
            .then(ceLimit => {
              this.setState({ limitReached: ceLimit.limitReached })
          }).catch(() => this.setState({ limitReached: false }));
        }

        loadAuthorizeDetails = () => {
          this.setState({ loading: true })
          const config = {
              transformRequest: [(data, headers) => {
                delete headers.common['Authorization']; delete headers.common['X-Auth-Token']; delete headers.common['timezone'];
                  if (data !== undefined) {
                     headers.post['Content-Type'] = 'application/json;charset=UTF-8';
                     return JSON.stringify(data)
                  } else {
                    return data;
                  }
                return data;
              }]
          };

          loadAuthorize()
            .then(authDetail => {
              authDetail.map(authDetails => {
                this.setState({ authDetails: authDetails })
                if(this.props.user.authorizeCustId != 0) {
                  const cardDetailJson = {"getCustomerProfileRequest": {"merchantAuthentication": {"name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey}, "customerProfileId": this.props.user.authorizeCustId, "unmaskExpirationDate" : "true", "includeIssuerInfo": "true"}}
                  authorizeApi(authDetails.apiUrl, cardDetailJson, config)
                    .then(cardDetails => {
                      if(cardDetails.profile != undefined) {
                        this.setState({ cardDetails: cardDetails.profile })
                        if(cardDetails.profile.length == undefined && cardDetails.profile.paymentProfiles == undefined){
                          this.setState({ loading: false })
                        }
                        if(cardDetails.profile.length != 0 && cardDetails.profile.paymentProfiles != undefined){
                          cardDetails.profile.paymentProfiles.map((card, i) => { if(card.defaultPaymentProfile == true) {this.setState({ activeCard: card.customerPaymentProfileId, defaultCard: card.customerPaymentProfileId, activePayment: card, loading: false})}})
                        }
                      } else {
                        this.setState({ cardDetails: [], loading: false })
                      }
                    }).catch(() => this.setState({ cardDetails: [], loading: false }));
                } else {
                    this.setState({ loading: false })
                }
              })
          }).catch(() => this.setState({ authDetails: [], loading: false }));
        }


        render() {
            const { recordsMetaData = [], user, params = {}, location={}, dispatch } = this.props;
            const { authDetails, cardDetails, activeCard, activePayment, defaultCard, limitReached } = this.state;
            const { state } = location;
            let { records = [] } = this.props;
            const metaData = recordsMetaData.find(_ => params.id && parseInt(_.id) === parseInt(params.id));
            var parentRecords = records && records.length > 0 && records.filter(_ => metaData && metaData.name && _.category === metaData.name) || [];
            const childData = metaData && metaData.children && metaData.children.reduce((b, child) => {
               const childRecords = records && records.filter(_ => _.category === child.name);
               if(childRecords && childRecords.length > 0){
                    b.push(Object.assign({}, b, child))
               }
               return b;
            },[]) || [];

            let displayName = name;
            let pageName = name;
            if (name === 'documents') {
                displayName = 'Resource Center'
            } else if (name === 'courses') {
                displayName = 'Continuing Education'
            } else if (name === 'ciicourses') {
                displayName = 'Implant Education'
            } else if (name === 'patient-courses'){
                displayName = 'Patient Education'
            }
            if (state && state.searchRecords) {
              parentRecords = state.searchRecords;
            }

            return (
                <Row className={styles.ViewDocumentsPage}>
                    <Col>
                        <Col sm={12} className={styles.header} style={this.state.loading ? { marginBottom: '3em' } : null}>
                            <Link onClick={() => user && user.platform === 'iPad' ? window.location = 'inapp://cancel' : browserHistory.goBack()}>
                                <Button type="button" className={styles.btn}>Back</Button>
                            </Link>
                            <h4 className={styles.subtitle}>{displayName}</h4>
                            <Link onClick={() => user && user.platform === 'iPad' ? window.location = 'inapp://cancel' : browserHistory.goBack()}>
                                <Button type="button" className={styles.btn}>Done</Button>
                            </Link>
                        </Col>
                        {this.state.loading ? <Spinner paymentStyle={true} /> :
                          <Col className={styles.body} style={user && user.platform != 'iPad' ? { height: `${document.body.clientHeight - 180}px` } : { height: `${document.body.clientHeight - 100}px` }}>
                              {parentRecords && parentRecords.length > 0 ? <ViewDocuments viewRequest={pageName === 'courses' ? true : false} name={metaData && metaData.name || false}  user={user} records={parentRecords} dispatch={dispatch} displayName={name} cardDetails={cardDetails} authDetails={authDetails} activeCard={activeCard} defaultCard={defaultCard} activePayment={activePayment} limitReached={limitReached} loadAuthorizeDetails={this.loadAuthorizeDetails.bind(this)} loadCeLimit={this.loadCeLimit.bind(this)} /> : null}
                              {metaData && metaData.children && metaData.children.map((child, index) => {
                                 const childRecords = records && records.filter(_ => _.category === child.name);
                                 return childRecords && childRecords.length > 0 ? <ViewDocuments viewRequest={pageName === 'courses' ? true : false} key={index} name={child.name} user={user} records={childRecords} dispatch={dispatch} displayName={name} cardDetails={cardDetails} authDetails={authDetails} activeCard={activeCard} defaultCard={defaultCard} activePayment={activePayment} limitReached={limitReached} loadAuthorizeDetails={this.loadAuthorizeDetails.bind(this)} loadCeLimit={this.loadCeLimit.bind(this)} /> : null
                              })}
                              {parentRecords.length > 0 || childData.length > 0 ? null : <Alert warning><span><FormattedMessage {...messages.noCourse} /></span></Alert>}
                          </Col>}
                    </Col>
                </Row>
            );
        }

    }


    function mapStateToProps(state, props) {
        const selectedRecords = selectRecords()(state, props);
        const recordsMetaData = selectRecordsMetaData()(state, props);
        const user = selectUser()(state, props);
        const loading = selectLoading()(state, props);
        const error = selectError()(state, props);
        const updateError = selectUpdateError()(state, props);
        return {
            records: selectedRecords ? selectedRecords.toJS() : false,
            recordsMetaData: recordsMetaData && recordsMetaData.toJS() || {},
            user,
            loading: loading,
            error: error,
            updateError: updateError
        };
    }

    return connect(mapStateToProps)(ViewDocumentsPage)
}
