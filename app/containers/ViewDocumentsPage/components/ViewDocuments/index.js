import React from 'react';
import { Alert, Row, Col, Button, Modal, Image, Icon } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import { browserHistory } from 'react-router';
import schema from 'routes/schema';
import AlertMessage from 'components/Alert';
import ConsultsForm from 'containers/ViewDocumentsPage/components/ConsultsForm.js';
import PaymentForm from "containers/ViewDocumentsPage/components/PaymentForm";
import { consultRecord , ceRequestRecord, cePurchase } from 'blocks/educationRequests/remotes.js';
import { loadCourses } from 'blocks/reports';



class ViewDocuments extends React.Component {
    constructor(props) {
      super(props);
      this.state = { alertMessage: false, alertFailed: false, alertInfo: false, alertBrowser: false, alertError: false, alertSuccessText: "", alertFailText: "", coursePrice: 29.00 }
    }

    handleSubmit(note, courseId) {
      const submitRecord = note.toJS();
      submitRecord.courseId = parseInt(courseId);
      consultRecord(submitRecord)
        .then(requestData => {
          if(requestData.resultStatus == "passed") {
            this.setState({ alertMessage: true, alertSuccessText: requestData.message });
            this.props.loadCeLimit();
            this.props.dispatch(loadCourses());
          }
          if(requestData.resultStatus == "failed") {
            this.setState({ alertFailed: true, alertFailText: requestData.message });
          }
        })
        .catch((error) => this.setState({ alertError: true }))
    }

    handlePaymentDetails = ({courseId, courseCost, purchasedDate, customerId, paymentId, transId, transCode}) => {
      var submitRecord = {
        courseId: courseId,
        cost: courseCost,
        purchasedDate: purchasedDate,
        customerProfileId: customerId,
        customerPaymentProfileId: paymentId,
        transactionId: transId,
        transactionCode: transCode
      }

      cePurchase(submitRecord)
        .then(requestData => {
          this.setState({ alertMessage: true, alertSuccessText: "Course Purchased Successfully" });
          this.props.loadCeLimit();
          this.props.dispatch(loadCourses());
        })
        .catch((error) => this.setState({ alertError: true }))
    }

    handleRequest() {
        this.setState({ alertInfo: true });
    }

    handleChange(record) {
        const { viewRequest } = this.props;
        const { alertBrowser } = this.state;
        const courseId = record.id;
        const recordUrl = record.url.split('.').pop();

        if(navigator.userAgent.indexOf("Chrome") != -1 && recordUrl == 'mov') {
            this.setState({ alertBrowser: true });
        } else if(navigator.userAgent.indexOf("Firefox") != -1 && recordUrl == 'mov' ) {
            this.setState({ alertBrowser: true });
        } else {
          { !viewRequest ? window.open(record.url, '_blank') : this.props.user.allowAllCECourses ?
            window.open(record.url, '_blank') :
            ceRequestRecord(courseId)
              .then(requestData => {
                if (requestData.Show === true) {
                    this.props.loadCeLimit();
                    this.props.dispatch(loadCourses());
                    window.open(record.url, '_blank');
                } else {
                  this.setState({ alertFailed: true, alertFailText: requestData.message });
                }
              }).catch((error) => { this.setState({ alertError: true }) })
          }
        }
    }

    /* componentDidMount() {
        var imageLinks = document.querySelectorAll('#ViewDocuments img');
        for (var i = 0; i < imageLinks.length; i++) {
            imageLinks[i].addEventListener('click', function (e) {
                e.preventDefau0lt()
                const vidSrc = e.target.getAttribute('href');
                if (vidSrc) {
                    BigPicture({
                        el: e.target,
                        vidSrc: e.target.getAttribute('href')
                    })
                }
            })
        }

    }*/

    render() {
        const { name, user, records, onCancel, viewRequest, displayName, authDetails, cardDetails, activeCard, activePayment, defaultCard, loadAuthorizeDetails, limitReached, loadCeLimit } = this.props;

        return (<Col xs={12} id="ViewDocuments" className={styles.ViewDocuments}>
            <h3>{name || ''}</h3>
            {records && records.length > 0 && records.map((record, index) =>
                <Col key={index} md={3} className={displayName == 'courses' || displayName == 'ciicourses' || displayName == 'documents' ? styles.largeDocuments : null } xs={12}>
                    <Col className={displayName == 'courses' || displayName == 'ciicourses' || displayName == 'documents' ? styles.viewBox : styles.Viewbox} >
                        <Col className={styles.Icons}>{record.url.endsWith('.pdf') ?
                            <Image src={record.thumbnailUrl && record.thumbnailUrl != '' && record.thumbnailUrl || require('img/documents/pdf.png')} className={styles.pdf} onClick={() => this.handleChange(record)} responsive /> :
                            <Col>
                                <Image href={record.url} className={styles.img} src={record.thumbnailUrl && record.thumbnailUrl != '' && record.thumbnailUrl || 'https://s3.amazonaws.com/novadontics-uploads/dataurifile1584343276879-1584343277318.png'} onClick={() => this.handleChange(record)} responsive />
                                <span className={styles.play}>
                                    <Image width={60} href={record.url} onClick={() => this.handleChange(record)} src={require('img/catalog/play4.png')} />
                                </span>
                            </Col>}
                        </Col>
                        <span style={{ padding: '3px' }}>{record.title && record.title.length >= 35 ? record.title.substring(0, 50) + '...' : record.title}</span>
                        <Col className={viewRequest ? styles.teamBorder : styles.team}>
                          {record.requestStatus === 'Closed' ?
                            <span style={{ color: "#ea6225" }}>Completed</span> :
                          record.isWatched === true ?
                            <span style={{ color: "#ea6225" }}>Viewed</span> :
                          limitReached === true && record.isRequested === false && record.isWatched === false && record.isPurchased === false ?
                              <PaymentForm
                                user={user}
                                schema={schema().products()}
                                initialValues={{amount: this.state.coursePrice}}
                                form={`purchaseCourse.${record.id}`}
                                onRef={purchaseForm => (this.purchaseForm = purchaseForm)}
                                courseId={record.id}
                                cardDetails={cardDetails}
                                authDetails={authDetails}
                                activeCard={activeCard}
                                defaultCard={defaultCard}
                                activePayment={activePayment}
                                loadAuthorizeDetails={loadAuthorizeDetails}
                                handlePaymentDetails={this.handlePaymentDetails}
                              >
                                {(open) => <span onClick={open.bind(this)} className={styles.submitButton}>Purchase</span>}
                              </PaymentForm> :
                            <span></span>}
                          <span>{record.speaker || 'Novadontics Team'}</span>
                        </Col>
                        {viewRequest ?
                          <Col>
                              {record.isRequested === true ?
                                  <Button type="button" className={styles.request} onClick={this.handleRequest.bind(this)}>CE Credit Requested </Button> :
                                  <ConsultsForm
                                      user={user}
                                      schema={schema().educationRequests()}
                                      initialValues={{ name: user.name, title: record.title }}
                                      onCancel={onCancel}
                                      onRef={consultsForm => (this.consultsForm = consultsForm)}
                                      courseId={record.id}
                                      handleSubmit={this.handleSubmit.bind(this)}
                                  >
                                      {(open) => <Button type="button" className={styles.request} onClick={open.bind(this)}>Request CE Credit</Button>}
                                  </ConsultsForm>}
                          </Col> : null
                        }
                        {this.state.alertBrowser ?
                          <AlertMessage info dismiss={() => { this.setState({ alertBrowser: false }) }}>This type of video is not supported in this browser. Please try in safari or iPad.</ AlertMessage> : null}
                        {this.state.alertMessage ?
                          <AlertMessage success dismiss={() => { this.setState({ alertMessage: false }); this.consultsForm.closeModal(); this.props.loadAuthorizeDetails();}}>{this.state.alertSuccessText}</AlertMessage> : null}
                        {this.state.alertFailed ?
                          <AlertMessage checklist danger dismiss={() => { this.setState({ alertFailed: false }) }}> {this.state.alertFailText}</AlertMessage> : null}
                        {this.state.alertError ?
                          <AlertMessage danger dismiss={() => { this.setState({ alertError: false }) }}> There was an error while creating the request. Please try again.</AlertMessage> : null}
                        {this.state.alertInfo ?
                          <AlertMessage info dismiss={() => { this.setState({ alertInfo: false }) }}>You already submitted request for this course.</ AlertMessage> : null}
                    </Col>
                </Col>)}
        </Col>)
    }

    close() {
      this.setState({ showModal: false });
    }

    open() {
      this.setState({ showModal: true })
    }
}

export default ViewDocuments;
