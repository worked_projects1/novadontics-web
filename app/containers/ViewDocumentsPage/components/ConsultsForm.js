

import React from 'react';
import EditRecordForm from 'components/EditRecordForm';
import { Alert, Modal, Row, Col, Button } from '@sketchpixy/rubix';
import AlertMessage from 'components/Alert';


const SUCCESS_ALERT_TIMEOUT = 4000;
export default class ConsultsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, alertMessage: false, alertError: false }
    }
    componentDidMount(){
        if(this.props.onRef) {
          this.props.onRef(this);
        }
    }
    render() {
        const { schema, handleSubmit,courseId,initialValues, onCancel } = this.props;
        return (
            <div>
                {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}

                <Modal show={this.state.showModal} backdrop="static"  onHide={this.closeModal.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Request for Continuing Education Credits
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <Col>
                                <EditRecordForm
                                    fields={schema.RequestForm}
                                    initialValues={initialValues}
                                    onCancel={onCancel}
                                    btnName="Submit Request"
                                    disableCancel={true}
                                    displayText={true}
                                    id={courseId}
                                    disableSubmit={false}
                                    onSubmit={(note) => handleSubmit(note,courseId)}
                                />
                            </Col>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
    closeModal() {
        this.setState({ showModal: false });
    }

    openModal() {
        this.setState({ showModal: true });
    }

}