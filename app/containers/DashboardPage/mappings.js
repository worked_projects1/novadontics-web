import numeral from "numeral";
import moment from "moment-timezone";
import {unzip} from 'lodash';
import { findRadius } from 'utils/tools.js';
import { findPercentage } from 'utils/tools.js';
import { findOther } from 'utils/tools.js';

let LastYearLabel = moment().add(-1, 'years').format("YYYY");
let LastPreviousYearLabel = moment().add(-2, 'years').format("YYYY");
let SecondLastPreviousYearLabel = moment().add(-3, 'years').format("YYYY");
let ThirdLastPreviousYearLabel = moment().add(-4, 'years').format("YYYY");
let FourthLastPreviousYearLabel = moment().add(-5, 'years').format("YYYY");
let currentMonthLabel = moment().format("MMM YYYY");
let previousMonthLabel = moment().add(-1, 'months').format("MMM YYYY");

export const mapDentistOrderBlock = ({all, thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear}) => (
  [
    {
      linkTo: '/orders',
      label: 'Up To Date',
      value: numeral(all).format('0,0'),
    },
    {
      label: 'This Month',
      value: numeral(thisMonth).format('0,0'),
    },
    {
      label: 'This Quarter',
      value: numeral(thisQuarter).format('0,0'),
    },
    {
      label: 'This Year',
      value: numeral(thisYear).format('0,0'),
    },
    {
      label: LastYearLabel,
      value: numeral(lastYear).format('0,0'),
    },
    {
      label: LastPreviousYearLabel,
      value: numeral(lastPreviousYear).format('0,0'),
    }
  ]
);

export const mapDentistRevenueBlock = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, total}) => ([
  {
    label: 'Up To Date',
    value: numeral(total).format('$0,0[.]00'),
  },
  {
    label: 'This Month',
    value: numeral(thisMonth).format('$0,0[.]00'),
  },
  {
    label: 'This Quarter',
    value: numeral(thisQuarter).format('$0,0[.]00'),
  },
  {
    label: 'This Year',
    value: numeral(thisYear).format('$0,0[.]00'),
  },
  {
    label: LastYearLabel,
    value: numeral(lastYear).format('$0,0[.]00'),
  },
  {
    label: LastPreviousYearLabel,
    value: numeral(lastPreviousYear).format('$0,0[.]00'),
  }
]);

export const mapOrderBlock = ({all, thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, lastThreeYear, lastFourYear, lastFiveYear}) => (
  [
    {
      linkTo: '/orders',
      label: 'Up To Date',
      value: numeral(all).format('0,0'),
    },
    {
      label: 'This Year',
      value: numeral(thisYear).format('0,0'),
    },
    {
      label: 'This Month',
      value: numeral(thisMonth).format('0,0'),
    },
    {
      label: LastYearLabel,
      value: numeral(lastYear).format('0,0'),
    },
    {
      label: LastPreviousYearLabel,
      value: numeral(lastPreviousYear).format('0,0'),
    },
    {
      label: SecondLastPreviousYearLabel,
      value: numeral(lastThreeYear).format('0,0'),
    },
    {
      label: ThirdLastPreviousYearLabel,
      value: numeral(lastFourYear).format('0,0'),
    },
    {
      label: FourthLastPreviousYearLabel,
      value: numeral(lastFiveYear).format('0,0'),
    }
  ]
);

export const mapRevenueBlock = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, lastThreeYear, lastFourYear, lastFiveYear, total}) => ([
  {
    label: 'Up To Date',
    value: numeral(total).format('$0,0.00'),
  },
  {
    label: 'This Year',
    value: numeral(thisYear).format('$0,0.00'),
  },
  {
    label: 'This Month',
    value: numeral(thisMonth).format('$0,0.00'),
  },
  {
    label: LastYearLabel,
    value: numeral(lastYear).format('$0,0.00'),
  },
  {
    label: LastPreviousYearLabel,
    value: numeral(lastPreviousYear).format('$0,0.00'),
  },
  {
    label: SecondLastPreviousYearLabel,
    value: numeral(lastThreeYear).format('$0,0.00'),
  },
  {
    label: ThirdLastPreviousYearLabel,
    value: numeral(lastFourYear).format('$0,0.00'),
  },
  {
    label: FourthLastPreviousYearLabel,
    value: numeral(lastFiveYear).format('$0,0.00'),
  }
]);

export const mapPatientBlock = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, total}) => [
  {
    label: 'Up To Date',
    value: numeral(total).format('0,0'),
  },
  {
    label: 'This Month',
    value: numeral(thisMonth).format('0,0'),
  },
  {
    label: 'This Quarter',
    value: numeral(thisQuarter).format('0,0'),
  },
  {
    label: 'This Year',
    value: numeral(thisYear).format('0,0'),
  },
  {
    label: LastYearLabel,
    value: numeral(lastYear).format('0,0'),
  },	
  {
    label: LastPreviousYearLabel,
    value: numeral(lastPreviousYear).format('0,0'),
  }
];


export const mapMembersBlock = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, lastThreeYear, lastFourYear, lastFiveYear, total}) => [
  {
    label: '',
    value1: "New",
    value2: "Can.",
    value3: "Ret.",
    value4: "Tot",
  },
  {
    label: 'Up To Date',
    value1: "286",
    value2: "104",
    value3: "89%",
    value4: "195",
  },
  {
    label: 'This Year',
    value1: "18",
    value2: "0",
    value3: "100%",
    value4: "195",
  },
  {
    label: previousMonthLabel,
    value1: "18",
    value2: "0",
    value3: "100%",
    value4: "195",
  },
  {
    label: LastYearLabel,
    value1: "89",
    value2: "24",
    value3: "87%",
    value4: "188",
  },
  {
    label: LastPreviousYearLabel,
    value1: "41",
    value2: "67",
    value3: "60%",
    value4: "166",
  },
  {
    label: SecondLastPreviousYearLabel,
    value1: "91",
    value2: "11",
    value3: "92%",
    value4: "136",
  },
  {
    label: ThirdLastPreviousYearLabel,
    value1: "20",
    value2: "0",
    value3: "100%",
    value4: "45",
  },
  {
    label: FourthLastPreviousYearLabel,
    value1: "27",
    value2: "2",
    value3: "93%",
    value4: "25",
  }
];


export const mapCeCourseRevenue = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, lastThreeYear, lastFourYear, lastFiveYear, total}) => [
  {
    label: 'Up to date',
    value: numeral(total).format('$0,0.00'),
  },
  {
    label: 'This Year',
    value: numeral(thisYear).format('$0,0.00'),
  },
  {
    label: currentMonthLabel,
    value: numeral(thisMonth).format('$0,0.00'),
  },
  {
    label: LastYearLabel,
    value: numeral(lastYear).format('$0,0.00'),
  },
  {
    label: LastPreviousYearLabel,
    value: numeral(lastPreviousYear).format('$0,0.00'),
  },
  {
    label: SecondLastPreviousYearLabel,
    value: numeral(lastThreeYear).format('$0,0.00'),
  },
  {
    label: ThirdLastPreviousYearLabel,
    value: numeral(lastFourYear).format('$0,0.00'),
  },
  {
    label: FourthLastPreviousYearLabel,
    value: numeral(lastFiveYear).format('$0,0.00'),
  }
];


export const mapMembershipRevenueBlock = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, lastThreeYear, lastFourYear, lastFiveYear, total}) => [
  {
    label: 'Up To Date',
    value: numeral(total).format('$0,0.00'),
  },
  {
    label: 'This Year',
    value: numeral(thisYear).format('$0,0.00'),
  },
  {
    label: previousMonthLabel,
    value: numeral(thisMonth).format('$0,0.00'),
  },
  {
    label: LastYearLabel,
    value: numeral(lastYear).format('$0,0.00'),
  },
  {
    label: LastPreviousYearLabel,
    value: numeral(lastPreviousYear).format('$0,0.00'),
  },
  {
    label: SecondLastPreviousYearLabel,
    value: numeral(lastThreeYear).format('$0,0.00'),
  },
  {
    label: ThirdLastPreviousYearLabel,
    value: numeral(lastFourYear).format('$0,0.00'),
  },
  {
    label: FourthLastPreviousYearLabel,
    value: numeral(lastFiveYear).format('$0,0.00'),
  }
];

export const mapCERequestBlock = ({thisMonth, thisQuarter, thisYear, lastYear, lastPreviousYear, total}) => [
  {
    label: 'Up To Date',
    value: numeral(total).format('0,0'),
  },
  {
    label: 'This Month',
    value: numeral(thisMonth).format('0,0'),
  },
  {
    label: 'This Quarter',
    value: numeral(thisQuarter).format('0,0'),
  },
  {
    label: 'This Year',
    value: numeral(thisYear).format('0,0'),
  },
  {
    label: LastYearLabel,
    value: numeral(lastYear).format('0,0'),
  },
  {
    label: LastPreviousYearLabel,
    value: numeral(lastPreviousYear).format('0,0'),
  }
];

export const mapRevenuePie = (orders, total) => {
  let arr = [];
  orders && orders.map((order) => {
    var data = Object.assign({}, {
      name: order.vendor,
      y: order.total,
      z: findRadius(order.total),
      percent: findPercentage(order.total, total)
    })
    arr.push(data);
  })
  return findOther(arr);
};


export const mapItemsPie = (orders, total) => {
  let arr = [];
  orders && orders.map((order) => {
    var data = Object.assign({}, {
      name: order.vendor,
      y: order.amount,
      z: findRadius(order.amount),
      percent: findPercentage(order.amount, total)
    })
    arr.push(data);
  })
  return findOther(arr);
};



export const mapPatientsPie = (patients,total) => {
  let arr = [];
  patients && Object.keys(patients).map((patient) =>
   {
  var data = Object.assign({}, {
    name: patient,
    y: patients[patient],
    z: findRadius(patients[patient]),
    percent: findPercentage(patients[patient],total)
  })
  arr.push(data);
})
return findOther(arr);
};
export const mapNewOrderItems = orders => orders.map(
  order => ({
   "order #": order.id,
    dentist: order.dentist,
    practice: order.practice,
    total: numeral(order.total).format('$0,0[.]00')
  })
);

export const mapNewConsultationItems = consultations => consultations.map(
  consultation => ({
    id: consultation.id,
    dentist: consultation.dentist,
    patient : consultation.patientName,
    practice: consultation.practice,
    date: moment(consultation.date).format('lll')
  })
);

export const mapMembersPie = (members, total) => {
  let arr = [];
  members && members.map((member) => {
    var data = Object.assign({}, {
      name: member.plan,
      y: member.member_count,
      z: findRadius(member.member_count),
      percent: findPercentage(member.member_count, total)
    })
    arr.push(data);
  })
  return findOther(arr);
};

export const mapMembersItemsPie = (members, total) => {
  let arr = [];
  members && members.map((member) => {
    var data = Object.assign({}, {
      name: member.marketingSource,
      y: member.member_count,
      z: findRadius(member.member_count),
      percent: findPercentage(member.member_count, total)
    })
    arr.push(data);
  })
  return findOther(arr);
};