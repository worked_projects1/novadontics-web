import React from 'react';
import {Col, Grid, PanelBody, PanelContainer, Row} from "@sketchpixy/rubix/lib/index";
import styles from '../styles.css';
import Charts from "components/Charts";

const BarChartCard = ({title,defaultvalue,id,data = [],handlefilter,options,labelCurrency}) => 
{
return (
  <Col md={6} className={styles.chartAnalytic}>
    <PanelContainer>
      <PanelBody className={styles.chartPanel}>
        <Grid>
          <Row>
            <Col xs={12}>
              <h4>{title}</h4>
              <Charts
                title={title}
                type="bar"
                id={id}
                defaultvalue={defaultvalue}
                handlefilter={handlefilter}
                labels={data.map(d => d[0])}
                records={data.map(d => d[1])}
                options={options}
                labelCurrency={labelCurrency}
                fixedStepSize
              />
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </PanelContainer>
  </Col>
);
}
export default BarChartCard;
