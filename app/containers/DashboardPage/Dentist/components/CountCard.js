import React from 'react';
import {Col, Grid, Icon, PanelBody, PanelContainer, Row} from "@sketchpixy/rubix/lib/index";
import { Link } from 'react-router';
import styles from '../styles.css';

const CardContent = ({label, value}) => (
  <div>
     <Row className={styles.rowRecords}>
      <Col className={styles.records} xs={7}>{label}</Col>
      <Col className={styles.records} xs={5}>{value}</Col>
    </Row>
  </div>
);

const WrappedContent = ({linkTo, ...rest}) => (
  <Link to={linkTo} className={styles.path}>
    <CardContent {...rest}/>
  </Link>
);

const CardItem = ({linkTo, ...rest}) => (
  linkTo ? <WrappedContent linkTo={linkTo} {...rest}/> : <CardContent {...rest}/>
);

const CountCard = ({ glyph, title, items }) => (
  <Col md={3} className={styles.boxAnalytic}>
    <PanelContainer>
      <PanelBody>
        <Grid>
          <Row>
            <Col xs={12}>
            <h3 className={styles.title}><Icon glyph={glyph} />&nbsp;<span style={{ color: '#ea6225' }}>{title}</span></h3>
             {items.map((item, i) => <CardItem key={`${title}_${i}`} {...item}/>)}
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </PanelContainer>
  </Col>
);

export default CountCard;