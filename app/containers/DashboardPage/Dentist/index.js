/*
 * DashboardPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { createSelector } from 'reselect';
import { selectUser } from 'blocks/session/selectors';
import styles from './styles.css';

import { Row } from '@sketchpixy/rubix';
import HighChartCard from "./components/HighChartCard";
import CountCard from "./components/CountCard";
import ListCard from "./components/ListCard";
import BarChartCard from "./components/BarChartCard";
import withLoading from "../../../hocs/withLoading";
import {compose, lifecycle} from "recompose";
import {
  mapItemsPie,
  mapNewConsultationItems,
  mapNewOrderItems,
  mapOrderBlock,
  mapPatientBlock, mapPatientsPie,
  mapRevenueBlock,
  mapDentistOrderBlock,
  mapDentistRevenueBlock,
  mapRevenuePie,
  mapCERequestBlock
} from "../mappings";
import Spinner from '../../../components/Spinner';
const latestOrderColumns = [
  { id: 0, visible: true, label: 'Dentist' },
  { id: 1, visible: true, label: 'Order #' },
  { id: 2, visible: true, label: 'Practice' },
  { id: 3, visible: true, label: 'Total' },
];

const newConsultationsColumns = [
  { id: 0, visible: true, label: 'Dentist' },
  { id: 1, visible: true, label: 'Patient' },
  { id: 2, visible: true, label: 'Practice' },
  { id: 3, visible: true, label: 'Date' },
];

export default function (name, path, create, actions, selectors) {
  const {
    selectLoading,
    selectData,
    selectError,
  } = selectors;


  class DashboardPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

    constructor(props) {
      super(props);
      this.state = {year: new Date().getFullYear().toString().substr(-2),pageload:false,dropdownValue:{
        Orders:new Date().getFullYear().toString().substr(-2),
        CE_Requests:new Date().getFullYear().toString().substr(-2),
      }}
    }

    static propTypes = {
      data: PropTypes.object,
      loading: PropTypes.bool,
      user: PropTypes.object,
      error: PropTypes.bool,
      dispatch: PropTypes.func,
    };
    render() {
      const { data: { orders = {}, spending = {}, patients = {}, latestConsultations = [], ceRequests = {}, orderByVendor, patientsByReferer = {}, patientsByStatus = {} } = {}, user = {} } = this.props;
      const { platform } = user;
      const {year,pageload,dropdownValue} = this.state;
      const order = orders.byMonth && orders.byMonth.filter((elem,i)  => dropdownValue && dropdownValue.Orders && elem[0].indexOf(dropdownValue.Orders) >-1);
      const orderSlice = orders.byMonth && orders.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const orderOptions = Array.from(new Set(orderSlice));
      const ceRequest = ceRequests.byMonth && ceRequests.byMonth.filter((elem,i) =>  dropdownValue && dropdownValue.CE_Requests && elem[0].indexOf(dropdownValue.CE_Requests) >-1);
      const ceRequestSlice = ceRequests.byMonth && ceRequests.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const ceRequestOptions = Array.from(new Set(ceRequestSlice));
      const allOrdersByVendor = orderByVendor && orderByVendor.all;
      const allOrdersByVendortotal = allOrdersByVendor && allOrdersByVendor.map((item) => item.total).reduce((a, b) => a + b, 0);
      const itemAllOrdersByVendortotal = allOrdersByVendor && allOrdersByVendor.map((item) => item.amount).reduce((a, b) => a + b, 0);
      const allPatientsByReferer = patientsByReferer && patientsByReferer.all
      const PatientsByReferer = allPatientsByReferer && Object.keys(allPatientsByReferer).map(a=>({label: a, value: allPatientsByReferer[a]}));
      const allPatientsByReferertotal = PatientsByReferer && PatientsByReferer.map((item) => item.value).reduce((a, b) => a + b, 0);
      const activePatientsByStatus = patientsByStatus;
      const PatientsByStatus = activePatientsByStatus && Object.keys(activePatientsByStatus).map(a=>({label: a, value: activePatientsByStatus[a]}));
      const activePatientsByStatustotal = PatientsByStatus.map((item) => item.value).reduce((a, b) => a + b, 0);
		
      const orderBlock = mapDentistOrderBlock(orders);
      const revenueBlock = mapDentistRevenueBlock(spending);
      const patientBlock = mapPatientBlock(patients);
      const ceRequestsBlock = mapCERequestBlock(ceRequests);
      
      
      const revenuePie = [
        {
          data: mapRevenuePie(allOrdersByVendor, allOrdersByVendortotal),
          title: 'Spending Up to Date by Vendor',
          labelCurrency: '$',
          glyph: 'icon-fontello-money',
          id: 'revenueuser'
        },
        {
          data: mapItemsPie(allOrdersByVendor, itemAllOrdersByVendortotal),
          title: 'Number of Items Purchased Up to Date by Vendor',
          labelCurrency: '$',
          glyph: 'icon-fontello-money',
          id: 'revenueuserpie'
        },
      ];
      
      const patientsPie = [
        {
          data: mapPatientsPie(allPatientsByReferer,allPatientsByReferertotal),
          title:'Up to Date Patients by Referral Source',
          labelCurrency: '$',
          glyph: 'icon-fontello-hash',
          id: ' patients'
        },
        {
          data: mapPatientsPie(activePatientsByStatus,activePatientsByStatustotal),
          title: 'Up to Date Patients by Status',
          labelCurrency: '$',
          glyph: 'icon-fontello-hash',
          id: 'patientsPie'
        },
      ];
      
      const latestOrderItems = mapNewOrderItems(orders.latest || []);
      const latestConsultationItems = mapNewConsultationItems(latestConsultations);
      return (
        <div className={styles.page}>
          <Helmet title="Dashboard"/>
            <div className={platform && platform === 'iPad' ? styles.ipadView : styles.view}>
              <Row>
                <CountCard title={"Orders"} glyph={"icon-fontello-cart"} items={orderBlock}/>
                <CountCard title={"Spending"} glyph={"icon-fontello-money"} items={revenueBlock}/>
                <CountCard title={"Patients"} glyph={"icon-fontello-users-2"} items={patientBlock}/>
                <CountCard title={"CE Credits"} glyph={"icon-fontello-award"} items={ceRequestsBlock}/>
              </Row>

              <Row><HighChartCard variablePie={patientsPie} /></Row>
              <Row><HighChartCard variablePie={revenuePie} /></Row>
              

              <Row>
                <ListCard title={'Orders'} columns={latestOrderColumns} items={latestOrderItems} linkTo={"/orders"} />
                <ListCard title={'Consultations'} columns={newConsultationsColumns} items={latestConsultationItems} linkTo={"/consultations"} />
              </Row>
              <Row>
                <BarChartCard
                  title={"Orders"}
                  data={order}
                  id={"Orders"}  
                  defaultvalue={dropdownValue["Orders"]}
                  handlefilter={(e) =>this.setState({year:e.target.value,dropdownValue:Object.assign({},dropdownValue,{["Orders"]:e.target.value})})}
                  options={orderOptions}
                />
                <BarChartCard
                  title={"CE Requests"}
                  data={ceRequest}
                  id={"CE_Requests"}  
                  defaultvalue={dropdownValue["CE_Requests"]}
                  handlefilter={(e) =>this.setState({year:e.target.value,dropdownValue:Object.assign({},dropdownValue,{["CE_Requests"]:e.target.value})})}
                  options={ceRequestOptions}
                />
              </Row>
            </div>
        </div>
      );
    }
  }

  const mapDispatchToProps = dispatch => ({dispatch});

  const connectToStore = connect(
    createSelector(
      selectUser(),
      selectLoading(),
      selectData(),
      selectError(),
      (user, loading, data, error) => ({ user, loading, data: data ? data.toJS() : {}, error }),
    ),
    mapDispatchToProps
  );

  return compose(
    connectToStore,
    lifecycle({
      componentDidMount() {
        if (this.props.dispatch) {
          this.props.dispatch(actions.loadAnalytics());
        }
      }
    }),
    withLoading,
  )(DashboardPage);
}
