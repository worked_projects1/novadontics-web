import React from 'react';
import {Col, Grid, Icon, PanelBody, PanelContainer, Row} from "@sketchpixy/rubix/lib/index";
import { Link } from 'react-router';
import styles from '../styles.css';

const CardContent = ({label, value, title, value1, value2, value3, value4}) => (
  <div>
    {title === 'Paid Accounts' ?
      <Row className={styles.accountRowRecords}>
        <Col className={styles.records} xs={4}>{label}</Col>
        <Col className={styles.accountRecords} xs={2}>{value1}</Col>
        <Col className={styles.accountRecords} xs={2}>{value2}</Col>
        <Col className={styles.accountRecords} xs={2}>{value3}</Col>
        <Col className={styles.accountRecords} xs={2}>{value4}</Col>
      </Row> :
      <Row className={styles.rowRecords}>
        <Col className={styles.records} xs={6}>{label}</Col>
        <Col className={styles.records} xs={6}>{value}</Col>
      </Row>}
  </div>
);

const WrappedContent = ({linkTo, ...rest}) => (
  <Link to={linkTo} className={styles.path}>
    <CardContent {...rest}/>
  </Link>
);

const CardItem = ({linkTo, title, ...rest}) => (
  linkTo ? <WrappedContent linkTo={linkTo} {...rest}/> : <CardContent title={title} {...rest}/>
);

const CountCard = ({ admin, glyph, title, items }) => (
  <Col md={4} className={styles.boxAnalytic}>
    <PanelContainer className={admin ? "AdminCard" : null}>
      <PanelBody className={styles.paddzero}>
        <Grid>
          <Row className={styles.row}>
            <Col xs={12} className={styles.paddzero}>
              <h3 className={title === 'Paid Accounts' ? styles.accountsTitle : styles.title}><Icon glyph={glyph} />&nbsp;<span style={{ color: '#ea6225' }}>{title}</span></h3>
              {items.map((item, i) => <CardItem key={`${title}_${i}`} title={title} {...item}/>)}
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </PanelContainer>
  </Col>
);

export default CountCard;
