import React from 'react';
import { Col, Grid, PanelBody, PanelContainer, Row } from "@sketchpixy/rubix/lib/index";
import styles from '../styles.css';
import VariableCharts from "components/VariableCharts";

const Piechart = ({ title, glyph, labelCurrency,id, data = [] }) => 
    (
    <Col xs={6}>
      <Row>
        <h4 className={styles.pieTitle}>{title}</h4>
      </Row>
      <Row>
         <VariableCharts 
         data={data}
         id={id}
         />
      </Row>

    </Col>
  )

const HighChartCard = ({ variablePie }) => (
    <Col md={12} className={styles.chartAnalytic}>
      <PanelContainer>
        <PanelBody className={styles.chartPanel}>
          <Grid>
            <Row>
              <Col xs={12}>
                {variablePie.map((record, index) => (
                    <Piechart
                      {...record}
                    />
                )
                )}
              </Col>
            </Row>
          </Grid>
        </PanelBody>
      </PanelContainer>
    </Col>
  )
export default HighChartCard;
