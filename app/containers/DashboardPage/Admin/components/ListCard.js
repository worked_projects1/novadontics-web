import React from 'react';
import {Col, Grid, PanelBody, PanelContainer, Row} from "@sketchpixy/rubix/lib/index";
import { Link } from 'react-router';
import styles from '../styles.css';
import TableData from "components/TableData";

const ListCard = ({title, columns = [], items = [], linkTo}) => (
  <Col md={6} className={styles.tableAnalytic}>
    <PanelContainer>
      <PanelBody>
        <Grid>
          <Row>
            <Col xs={12}>
              <h2>{title}</h2>
              <TableData columns={columns}>
                {items.map(item =>
                  <tr key={item.id}>
                    {columns.map((column, i) => <td key={i}>{item[column.label.toLowerCase()]}</td>)}
                  </tr>
                )}
              </TableData>
              {linkTo && <Link to={linkTo}>View All</Link>}
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </PanelContainer>
  </Col>
);

export default ListCard;
