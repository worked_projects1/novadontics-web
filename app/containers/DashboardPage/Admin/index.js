/*
 * DashboardPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import styles from './styles.css';
import { Row } from '@sketchpixy/rubix';
import CountCard from "./components/CountCard";
import ListCard from "./components/ListCard";
import BarChartCard from "./components/BarChartCard";
import withLoading from "../../../hocs/withLoading";
import HighChartCard from "./components/HighChartCard";
import { compose, lifecycle } from "recompose";
import {
  mapItemsPie,
  mapNewConsultationItems,
  mapNewOrderItems,
  mapOrderBlock,
  mapPatientBlock,
  mapRevenueBlock,
  mapRevenuePie,
  mapMembersPie,
  mapMembersItemsPie,
  mapMembersBlock,
  mapCeCourseRevenue,
  mapMembershipRevenueBlock
} from "../mappings";
import Spinner from '../../../components/Spinner';

const latestOrdersColumns = [
  { id: 0, visible: true, label: 'Dentist' },
  { id: 1, visible: true, label: 'Order #' },
  { id: 2, visible: true, label: 'Practice' },
  { id: 3, visible: true, label: 'Total' },
];

const latestConsultationsColumns = [
 { id: 0, visible: true, label: 'Dentist' },
  { id: 1, visible: true, label: 'Patient' },
  { id: 2, visible: true, label: 'Practice' },
  { id: 3, visible: true, label: 'Date' },
];

export default function (name, path, create, actions, selectors) {
  const {
    selectLoading,
    selectData,
    selectError,
  } = selectors;


  class DashboardPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

    constructor(props) {
      super(props);
      this.state = {
        year: new Date().getFullYear().toString().substr(-2), pageload: false, dropdownValue: {
          Catlog_Orders: new Date().getFullYear().toString().substr(-2),
          Active_Members: new Date().getFullYear().toString().substr(-2),
          Consultation_Requests: new Date().getFullYear().toString().substr(-2),
          Catalog_Revenue: new Date().getFullYear().toString().substr(-2),
          Membership_Revenue: new Date().getFullYear().toString().substr(-2),
          Consultation_Revenue: new Date().getFullYear().toString().substr(-2)
        }
      }
    }
    static propTypes = {
      data: PropTypes.object,
      loading: PropTypes.bool,
      error: PropTypes.bool,
      dispatch: PropTypes.func,
    };

    render() {
      const { data: { orders = {}, revenue = {}, patients = {}, latestOrders = [], latestConsultations = [], consultations = {}, dentists = {}, orderByVendor, members = {}, membershipRevenue = {}, consultationsRevenue = {}, activeMembers = {}, cePurchase = {} } = {} } = this.props;
      const { year, pageload, dropdownValue } = this.state;
      const order = orders.byMonth && orders.byMonth.filter((elem, i) => dropdownValue && dropdownValue.Catlog_Orders && elem[0].indexOf(dropdownValue.Catlog_Orders) > -1)
      const orderSlice = orders.byMonth && orders.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const orderOptions = Array.from(new Set(orderSlice));

      const member = members.byMonth && members.byMonth.filter((elem, i) => dropdownValue && dropdownValue.Active_Members && elem[0].indexOf(dropdownValue.Active_Members) > -1)
      const memberSlice = members.byMonth && members.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const memberOptions = Array.from(new Set(memberSlice));

      const consultation = consultations.byMonth && consultations.byMonth.filter((elem, i) => dropdownValue && dropdownValue.Consultation_Requests && elem[0].indexOf(dropdownValue.Consultation_Requests) > -1)
      const consultationSlice = consultations.byMonth && consultations.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const consultationOptions = Array.from(new Set(consultationSlice));

      const revenues = revenue.byMonth && revenue.byMonth.filter((elem, i) => dropdownValue && dropdownValue.Catalog_Revenue && elem[0].indexOf(dropdownValue.Catalog_Revenue) > -1)
      const revenuesSlice = revenue.byMonth && revenue.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const revenuesOptions = Array.from(new Set(revenuesSlice));

      const membershipRevenues = membershipRevenue.byMonth && membershipRevenue.byMonth.filter((elem, i) => dropdownValue && dropdownValue.Membership_Revenue && elem[0].indexOf(dropdownValue.Membership_Revenue) > -1)
      const membershipRevenuesSlice = membershipRevenue.byMonth && membershipRevenue.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const membershipRevenuesOptions = Array.from(new Set(membershipRevenuesSlice));

      const consultationsRevenues = consultationsRevenue.byMonth && consultationsRevenue.byMonth.filter((elem, i) => dropdownValue && dropdownValue.Consultation_Revenue && elem[0].indexOf(dropdownValue.Consultation_Revenue) > -1)
      const consultationsRevenuesSlice = consultationsRevenue.byMonth && consultationsRevenue.byMonth.map((elem,i) => elem[0].slice(3,5) );
      const consultationsRevenuesOptions = Array.from(new Set(consultationsRevenuesSlice));

      const allOrdersByVendor = orderByVendor && orderByVendor.all;
      const allOrdersByVendortotal = allOrdersByVendor && allOrdersByVendor.map((item) => item.total).reduce((a, b) => a + b, 0);
      const itemAllorderTotal = allOrdersByVendor && allOrdersByVendor.map((item) => item.amount).reduce((a, b) => a + b, 0);
      const monthOrdersByVendor = orderByVendor && orderByVendor.thisMonth;
      const monthOrdersByVendortotal = monthOrdersByVendor && monthOrdersByVendor.map((item) => item.total).reduce((a, b) => a + b, 0);
      const itemMonthOrderTotal = monthOrdersByVendor && monthOrdersByVendor.map((item) => item.amount).reduce((a, b) => a + b, 0);
      const membersByPlan = members.plan;
      const membersByPlantotal = membersByPlan && membersByPlan.map((item) => item.member_count).reduce((a, b) => a + b, 0);
      const membersByMarketing = members.marketingSource;
      const membersByMarketingtotal = membersByMarketing && membersByMarketing.map((item) => item.member_count).reduce((a, b) => a + b, 0);
      const orderBlock = mapOrderBlock(orders);
      const revenueBlock = mapRevenueBlock(revenue);
      const patientBlock = mapPatientBlock(patients);
      const ceCourseRevenueBlock = mapCeCourseRevenue(cePurchase);
      const membersBlock = mapMembersBlock(activeMembers);
      const membershipRevenueBlock = mapMembershipRevenueBlock(membershipRevenue);

      const revenuePie = [
        {
          data: mapRevenuePie(monthOrdersByVendor, monthOrdersByVendortotal),
          title: "This Month Revenue by Vendor",
          labelCurrency: '$',
          glyph: 'icon-fontello-money',
          id: 'revenue'
        },
        {
          data: mapRevenuePie(allOrdersByVendor, allOrdersByVendortotal),
          title: "Total Revenue Up to Date by Vendor",
          labelCurrency: '$',
          glyph: 'icon-fontello-money',
          id: 'revenuepie'
        },
      ];
      const vendorRevenuePie = [
        {
          data: mapItemsPie(monthOrdersByVendor, itemMonthOrderTotal),
          title: "This Month Number of Items Sold by Vendor",
          labelCurrency: '$',
          glyph: 'icon-fontello-money',
          id: 'vendorRevenue'
        },
        {
          data: mapItemsPie(allOrdersByVendor, itemAllorderTotal),
          title: "Total Number of Items Sold Up to Date by Vendor",
          labelCurrency: '$',
          glyph: 'icon-fontello-money',
          id: 'vendorRevenuePie'
        },
      ];
      const membersPie = [
        {
          data: mapMembersPie(membersByPlan, membersByPlantotal),
          title: "Total Members By Plan",
          labelCurrency: '$',
          glyph: 'icon-fontello-hash',
          id: 'members'
        },
        {
          data: mapMembersItemsPie(membersByMarketing, membersByMarketingtotal),
          title: "Total Members By Marketing Source",
          labelCurrency: '$',
          glyph: 'icon-fontello-hash',
          id: 'membersPie'
        },
      ];
      const latestOrderItems = mapNewOrderItems(latestOrders);
      const latestConsultationItems = mapNewConsultationItems(latestConsultations);

      return (
        <div className={styles.page}>
          <Helmet title="Dashboard" />
          <div className={styles.view}>
            <Row>
              <CountCard admin={true} title={"Catalog Orders"} glyph={"icon-fontello-cart"} items={orderBlock} />
              <CountCard admin={true} title={"Gross Catalog Revenue"} glyph={"icon-fontello-money"} items={revenueBlock} />
              <CountCard admin={true} title={"CE Courses Revenue"} glyph={"icon-fontello-docs"} items={ceCourseRevenueBlock} />
              <CountCard admin={true} title={"Paid Accounts"} glyph={"icon-fontello-users"} items={membersBlock} />
              <CountCard admin={true} title={"Subscription Revenue"} glyph={"icon-fontello-money"} items={membershipRevenueBlock} />
            </Row>
            <Row><HighChartCard variablePie={revenuePie} /></Row>
            <Row><HighChartCard variablePie={vendorRevenuePie} /></Row>
            <Row><HighChartCard variablePie={membersPie} /></Row>

            <Row>
              <ListCard title={'Orders'} columns={latestOrdersColumns} items={latestOrderItems} linkTo={"/orders"} />
              <ListCard title={'Consultations'} columns={latestConsultationsColumns} items={latestConsultationItems} linkTo={"/consultations"} />
            </Row>
            <Row>
              <BarChartCard
                title={"Catalog Orders"}
                id={"Catlog_Orders"}
                data={order}
                defaultvalue={dropdownValue["Catlog_Orders"]}
                handlefilter={(e) => this.setState({ year: e.target.value, dropdownValue: Object.assign({}, dropdownValue, { ["Catlog_Orders"]: e.target.value }) })}
                options={orderOptions}
              />
              <BarChartCard
                title={"Active Members"}
                id={"Active_Members"}
                data={member}
                defaultvalue={dropdownValue["Active_Members"]}
                handlefilter={(e) =>
                  this.setState({ year: e.target.value, dropdownValue: Object.assign({}, dropdownValue, { ["Active_Members"]: e.target.value }) })}
                options={memberOptions}
              />
              <BarChartCard
                title={"Consultation Requests"}
                data={consultation}
                id={"Consultation_Requests"}
                defaultvalue={dropdownValue["Consultation_Requests"]}
                handlefilter={(e) =>
                  this.setState({ year: e.target.value, dropdownValue: Object.assign({}, dropdownValue, { ["Consultation_Requests"]: e.target.value }) })}
                options={consultationOptions}
              />
          </Row>
            <Row>
              <BarChartCard
                title={"Catalog Revenue"}
                data={revenues}
                id={"Catalog_Revenue"}
                labelCurrency={"$"}
                defaultvalue={dropdownValue["Catalog_Revenue"]}
                handlefilter={(e) =>
                  this.setState({ year: e.target.value, dropdownValue: Object.assign({}, dropdownValue, { ["Catalog_Revenue"]: e.target.value }) })}
                options={revenuesOptions}
              />
              <BarChartCard
                title={"Membership Revenue"}
                data={membershipRevenues}
                id={"Membership_Revenue"}
                defaultvalue={dropdownValue["Membership_Revenue"]}
                labelCurrency={"$"}
                handlefilter={(e) =>
                  this.setState({ year: e.target.value, dropdownValue: Object.assign({}, dropdownValue, { ["Membership_Revenue"]: e.target.value }) })}
                options={membershipRevenuesOptions}
              />
              <BarChartCard
                title={"Consultation Revenue"}
                data={consultationsRevenues}
                id={"Consultation_Revenue"}
                defaultvalue={dropdownValue["Consultation_Revenue"]}
                handlefilter={(e) =>
                  this.setState({ year: e.target.value, dropdownValue: Object.assign({}, dropdownValue, { ["Consultation_Revenue"]: e.target.value }) })}
                options={consultationsRevenuesOptions}
              />
            </Row>
          </div>
        </div>
      );
    }
  }

  const mapDispatchToProps = dispatch => ({ dispatch });

  const connectToStore = connect(
    createSelector(
      selectLoading(),
      selectData(),
      selectError(),
      (loading, data, error) => ({ loading, data: data ? data.toJS() : {}, error }),
    ),
    mapDispatchToProps
  );
  return compose(
    connectToStore,
    lifecycle({
      componentDidMount() {
        if (this.props.dispatch) {
          this.props.dispatch(actions.loadAnalytics());
        }
      }
    }),
    withLoading,
  )(DashboardPage);
}
