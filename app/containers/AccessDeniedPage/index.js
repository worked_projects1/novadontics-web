/*
 * PatientsPage Container
 */

import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import Helmet from 'react-helmet';
import _ from 'lodash';
import styles from './styles.css';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

class AccessDeniedPage extends React.Component { // eslint-disable-line react/prefer-stateless-function


    constructor(props) {
        super(props);
    }

    componentDidMount() {
        document.getElementById('body').setAttribute('style', 'margin-top: 75px !important');
        document.getElementById('app').style.background = 'var(--mediumDarkGrey)';
        document.getElementById('body').style.background = 'var(--mediumDarkGrey)';
        document.getElementById('body').style.padding = '0px';
    }

    componentWillUnmount() {
        document.getElementById('app').style.background = '#e7e7e8';
        document.getElementById('body').style.background = '#e7e7e8';
        document.getElementById('body').style.paddingTop = '25px';
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        const title = <FormattedMessage {...messages.title} />;
        const description = <FormattedMessage {...messages.description} />;

        return (
            <div className={styles.notFoundPage}>
                <Helmet
                    title={title.props.defaultMessage}
                    meta={[
                        { name: 'description', content: description.props.defaultMessage },
                    ]}
                />
                <Grid>
                    <Row>
                        <Col xs={8} xsOffset={2} style={{textAlign: 'center'}}>
                            <img src={require('img/lock.svg')} width="120" height="120" style={{marginRight: '25px'}} />
                            <h2 className={styles.message}><FormattedMessage {...messages.message} /></h2>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}


export default AccessDeniedPage;

