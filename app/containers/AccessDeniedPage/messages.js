/*
 * AccessDeniedPage Messages
 *
 * This contains all the text for the AccessDeniedPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.components.AccessDeniedPage.title',
    defaultMessage: 'Novadontics',
  },
  description: {
    id: 'app.components.AccessDeniedPage.description',
    defaultMessage: 'This is access denied page.',
  },
  message: {
    id: 'app.components.AccessDeniedPage.message',
    defaultMessage: 'Your account is not enabled to access this feature.',
  },
  notFound: {
    id: 'app.components.AccessDeniedPage.notFound',
    defaultMessage: '403',
  },
  backToHome: {
    id: 'app.components.AccessDeniedPage.backToHome',
    defaultMessage: 'Go back to home',
  },
});
