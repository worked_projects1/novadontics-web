/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import styles from './styles.css';

import { Button, Grid, Row, Col } from '@sketchpixy/rubix';
import Spinner from 'components/Spinner';

export default class NotFound extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const title = <FormattedMessage {...messages.title} />;
    const description = <FormattedMessage {...messages.description} />;
    const url = new URL(window.location.href);
    const platform = url.searchParams.get("platform");

    return platform != 'iPad' ? 
    (<div className={styles.notFoundPage}>
        <Helmet
          title={title.props.defaultMessage}
          meta={[
            { name: 'description', content: description.props.defaultMessage },
          ]}
        />
        <Grid>
          <Row>
            <Col xs={8} xsOffset={2}>
              <h1 className={styles.notFound}><FormattedMessage {...messages.notFound} /></h1>
              <h2 className={styles.message}><FormattedMessage {...messages.message} /></h2>
              <Link to={process.env.PUBLIC_PATH || '/'} onClick={() => navigator.platform === 'iPad' ? window.location = 'inapp://cancel' : null}><Button type="submit" bsStyle="white" bsSize="sm" outlined><FormattedMessage {...messages.backToHome} /></Button></Link>
            </Col>
          </Row>
        </Grid>
      </div>): <Spinner />;
  }
}
