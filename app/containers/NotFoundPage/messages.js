/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.components.NotFoundPage.title',
    defaultMessage: '404 - Not Found',
  },
  description: {
    id: 'app.components.NotFoundPage.description',
    defaultMessage: 'This is not found page.',
  },
  message: {
    id: 'app.components.NotFoundPage.message',
    defaultMessage: 'Sorry, we can’t seem to find the page you’re looking for.',
  },
  notFound: {
    id: 'app.components.NotFoundPage.notFound',
    defaultMessage: '404',
  },
  backToHome: {
    id: 'app.components.NotFoundPage.backToHome',
    defaultMessage: 'Go back to home',
  },
});
