/*
 *
 * LoginPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Alert } from '@sketchpixy/rubix';

import AnonymousRouteWrapper from 'components/AnonymousRouteWrapper';
import CredentialsForm from 'components/CredentialsForm';
import { logIn } from 'blocks/session/actions';
import { selectError } from 'blocks/session/selectors';

export class LoginPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    error: PropTypes.object,
  };

  static handleSubmit(data, dispatch, { form }) {
    const { identifier, secret } = data.toJS();
    dispatch(logIn(identifier, secret, form));
  }

  constructor(props) {
    super(props);

    this.handleSubmit = LoginPage.handleSubmit.bind(this);
  }

  render() {
    const { error = {} } = this.props;
    const { login: loginError } = error;

    return (
      <AnonymousRouteWrapper>
        <div>
          <div className="anonymous-form-wrapper">
            <CredentialsForm type="login" submitLabel="Log in" onSubmit={this.handleSubmit} />
          </div>
          {loginError ?
            <Alert danger>
              <p>{loginError}</p>
            </Alert> : null}
        </div>
      </AnonymousRouteWrapper>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  createSelector(
    selectError(),
    (error) => ({ error }),
  ),
  mapDispatchToProps,
)(LoginPage);

