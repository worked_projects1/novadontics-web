/*
 * Shows dentist info - practice, name, phone, email.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';

class DentistInfo extends React.Component {

  static propTypes = {
    wrapperClass: PropTypes.string,
    dentistInfo: PropTypes.shape({
      dentistName: PropTypes.string,
      practiceName: PropTypes.string,
      dentistPhone: PropTypes.string,
      dentistEmail: PropTypes.string,
      patient: PropTypes.string,
    }),
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { wrapperClass, dentistInfo = {} } = this.props;
    const { dentistName = '', practiceName = '', dentistPhone = '', dentistEmail = '', patient = '' } = dentistInfo;
    return (
      <Row className={wrapperClass}>
        <Col xs={6}>
          <strong>{dentistName}</strong>
          <p>{practiceName}</p>
          {patient != '' ?
            <div>
              <strong>Patient Name:</strong>
              <p>{patient}</p>
            </div> : null }
        </Col>
        <Col xs={6}>
          <p>{dentistPhone}</p>
          <p>{dentistEmail}</p>
        </Col>
      </Row>
    );
  }

}

export default DentistInfo;
