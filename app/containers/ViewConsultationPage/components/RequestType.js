/*
 * Shows the request type(s).
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';

class RequestType extends React.Component {

  static propTypes = {
    needsAnesthesiologist: PropTypes.bool,
    needsLabTechnician: PropTypes.bool,
    needsProstodonthist: PropTypes.bool,
    needsSurgicalDentist: PropTypes.bool,
    endodontist: PropTypes.bool,
    overThePhone: PropTypes.bool,
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { needsAnesthesiologist, needsLabTechnician, needsProstodonthist, needsSurgicalDentist, endodontist, overThePhone } = this.props;
    if (needsAnesthesiologist || needsLabTechnician || needsProstodonthist || needsSurgicalDentist || endodontist || overThePhone) {
      return (
        <div>
          <p><strong>Request type:</strong></p>
          <ul>
            {needsAnesthesiologist ? <li>Anesthesiologist requested.</li> : null}
            {needsLabTechnician ? <li>Lab technician requested.</li> : null}
            {needsProstodonthist ? <li>Prostodonthist requested.</li> : null}
            {needsSurgicalDentist ? <li>Surgical dentist requested.</li> : null}
            {endodontist ? <li>Endodontist requested.</li> : null}
            {overThePhone ? <li>Consultation over the phone requested.</li> : null}
          </ul>
        </div>
       );
    }
    return null;
  }
}

export default RequestType;
