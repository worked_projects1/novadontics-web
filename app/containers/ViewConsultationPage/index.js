/*
 * ViewConsultationPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Row, Col, Panel, PanelContainer, Tab, Tabs, PanelHeader, Label as InfoLabel } from '@sketchpixy/rubix';
import {List} from 'immutable';
import { selectUser } from 'blocks/session/selectors';

import EditRecordForm from 'components/EditRecordForm';
import Label from 'components/Label';
import Gallery from 'components/Gallery';

import Checkbox from './components/Checkbox.js';
import DentistInfo from './components/DentistInfo.js';
import LoadingState from './components/LoadingState.js';
import RequestType from './components/RequestType';
import { parseStructure } from './utils.js';
import styles from './styles.css';
import { Player } from 'video-react';
import "video-react/dist/video-react.css";
import DeleteActionLink from "../../components/DeleteActionLink";

const DUPLICATE_TITLE_IN_SCHMEA = '1D Immediate Loading Risk Factors';

class MedicalRecord extends React.Component {
  render () {
  const {treatment, formSchema} = this.props;
  const { steps = [] } = treatment || {};
  const getCompletedHashMap = (steps) => steps.reduce((hash, step) => Object.assign(hash, { [step.formId]: step.state }), {});
  const completedHashMap = getCompletedHashMap(steps);

  return (
    <Tabs defaultActiveKey={0} id="ViewConsultationPageTabs">
      {formSchema ? formSchema.map((section, i) =>
        <Tab key={`section${section.title}`} eventKey={i} title={section.title} className={styles.info}>
          {section.forms.map((form) =>
            <Row key={form.id}>
              <Col xs={12}>
                <h4 className={styles.subtitle}>
                  {form.title}
                </h4>
                <p>
                  {completedHashMap[form.id] && completedHashMap[form.id] === 'Completed' ?
                    <InfoLabel bsStyle="success">Completed</InfoLabel> :
                    <InfoLabel bsStyle="warning">In Progress</InfoLabel>
                  }
                </p>
              </Col>

              {completedHashMap[form.id] ? form.schema.map((schemaItem) =>
                  <Col xs={12} key={`schemaItem-${schemaItem.title}`}>
                    { form.title !== schemaItem.title &&
                    schemaItem.title !== DUPLICATE_TITLE_IN_SCHMEA ?
                      <h4>{schemaItem.title}</h4> : null}

                    {schemaItem.rows.map((row) =>
                      row.columns.map((column) =>
                        column.columnItems.map((columnItem, columnIndex) =>
                          <div key={`columnItem-${columnIndex}`}>
                            {columnItem.field && columnItem.field.type ?
                              this.renderSimpleView(columnItem.field, steps.find((step) => step.formId === form.id), false, columnIndex) :
                              this.renderGroup(columnItem, steps.find((step) => step.formId === form.id))}
                          </div>
                        )
                      )
                    )}
                  </Col>) :
                <Col xs={12} key={`nodata-${form.id}`}>
                  <p className={styles.noDataLabel}>No data yet.</p>
                </Col>}

              <Col xs={12} key={`hrRule-${form.id}`}>
                <hr className={styles.hrRule} />
              </Col>
            </Row>
          )}
        </Tab>
      ) : null}
    </Tabs>
    )
  };

  renderSimpleView(field, step = {}, groupMember = false, renderIndex) {
    const { type, name, title, attributes, postfix } = field;
    const { value = {} } = step;

    if (!value[name]) {
      return null;
    }


    let parsedStruct = {};
    if (type === 'Images' || type === 'Checkbox' || type === 'Videos') {
      parsedStruct = parseStructure(value[name]) || {};
    }

    switch (type) {
      case 'Input':
        return (
          <Col xs={12} className={styles.spaceItems} key={`input-${name}-${renderIndex}`}>
            <Label data={{ label: title, value: postfix ? `${value[name]} ${postfix}` : value[name] }} />
          </Col>
        );
      case 'Images':
        return (
          <Col xs={12} className={styles.spaceItems} key={`image-${name}-${renderIndex}`}>
            <Label data={{ label: title, value: '' }} />
            <Gallery data={{ images: parsedStruct && parsedStruct.images ? parsedStruct.images : [] }} />
          </Col>
        );
      case 'Checkbox':
        return <Checkbox key={`checkbox-${name}-${renderIndex}`} struct={parsedStruct} groupMember={groupMember} title={title} selectOptions={attributes ? attributes.options : []} styles={styles} />;
      case 'Select':
        return (
          <Col xs={12} className={styles.spaceItems} key={`select-${name}-${renderIndex}`}>
            <Label data={{ label: title, value: value[name] }} />
          </Col>
        );
      case 'Videos':
        return (
          parsedStruct.videos.map((src, i) =>
            <Col xs={6} className={styles.spaceItems} key={`video-${i}`}>
              <Player>
                <source src={src} />
              </Player>
            </Col>
          )
        );
      default:
        return null;
    }
  }

  renderGroup(field = {}, step = {}) {
    const { value = {} } = step;
    const { group = {} } = field;
    const { fields } = group;
    if (group && fields && value) {
      return (
        <Col xs={12} key={`group-${group.title}`} className={styles.spaceItems}>
          <Label data={{ label: group.title, value: '' }} />
          {fields.map((simpleField, simpleIndex) => this.renderSimpleView(simpleField, step, true, simpleIndex))}
        </Col>
      );
    }
    return null;
  }
}

const PatientInfo = (props) => props.treatment && props.treatment.steps ? <MedicalRecord {...props}/> : <div/>;

export default function (path, columns, updateRecord, { selectRecord, selectFormSchema }, actions) {
  function handleEdit(data, dispatch, { form }) {
    const recordData = data.toJS();
    recordData.amount = recordData.amount != null ? parseFloat(recordData.amount.replace(/\$/g,'')) : 0;	
    dispatch(updateRecord(recordData, form));
  }

  class ViewConsultationPage extends React.Component {

    static propTypes = {
      record: PropTypes.object,
      dispatch: PropTypes.func,
      params: PropTypes.object,
      formSchema: PropTypes.array,
    };

    componentDidMount() {
      const { id } = this.props.params;
      const { record = {}, dispatch, formSchema } = this.props;
      if (id) {
        dispatch(actions.loadRecord(parseInt(id, 10)));
      }
      if (formSchema.length === 0 && actions.loadFormSchema) {
        dispatch(actions.loadFormSchema());
      }
      columns.filter(function(el) { return (el.value == "amount" || el.value == "mode") ? el.editRecord = ((record.status == 'Closed') ? true : false) : el })			
 
    }

    render() {
      const { record = {}, formSchema, user } = this.props;
      const { recordDidNotInit, treatment = {}, id: consultationId, datetimeRequested, dentistName, practiceName, dentistEmail, dentistPhone, patient, data = {}, status } = record;
      const { comment: dentistComment } = data;
      const consultationData = data.consultations && data.consultations.length > 0 ? data.consultations.map(consult => consult.type) : [];
      const consultationRequest = consultationData && consultationData.length > 0 ? Object.assign(...consultationData.map(v => ({ [v]: true }))) : {};
      const { needsAnesthesiologist, needsLabTechnician, needsProstodonthist, needsSurgicalDentist, endodontist, overThePhone } = consultationRequest;
      const initialValues = columns.reduce((acc, el) => Object.assign({}, acc, { [el.value]: record[el.value] }), {});

      if (recordDidNotInit) {
        return <LoadingState path={path} />;
      }
      return (
        <div className="catalogView">
          <Row>
            <Col xs={12}>
              <PanelContainer>
                <Panel>
                  <PanelHeader>
                  {path ?
                    <Row>
                      <Col sm={12} className={styles.headerActions}>
                        <div className="closeDetailViewButton">
                          <Link to={`${path}`}>Close</Link>
                        </div>
                        {user && user.role == 'admin' ?
                          <DeleteActionLink deleteRecord={actions.deleteRecord} record={record}/> : null}
                      </Col>
                    </Row> : null}
                    <Row>
                      <Col sm={9}>
                        <h2 className={styles.headTitle}>Consultation #{consultationId}</h2>
                      </Col>
                      <Col sm={3} className={styles.right}>
                        {user && user.role == 'admin' ?
                          <EditRecordForm
                            name={name}
                            initialValues={initialValues}
                            fields={columns}
                            form={`editRecord.${record.id}`}
                            onSubmit={handleEdit}
                            inline
                          /> :
                          <div>
                            <strong>Status</strong>
                            <p>{status}</p>
                          </div>}
                      </Col>
                      <Col xs={12} className={styles.headerData}>
                        <span>{datetimeRequested}</span>
                      </Col>
                    </Row>
                    <DentistInfo
                      wrapperClass={styles.info}
                      dentistInfo={{ dentistName, practiceName, dentistPhone, dentistEmail, patient }}
                    />
                  </PanelHeader>
                  <hr className="hidden-print" />
                  <RequestType
                    needsAnesthesiologist={needsAnesthesiologist}
                    needsLabTechnician={needsLabTechnician}
                    needsProstodonthist={needsProstodonthist}
                    needsSurgicalDentist={needsSurgicalDentist}
                    endodontist={endodontist}
                    overThePhone={overThePhone}
                  />
                  {dentistComment != '' ?
                    <div>
                      <strong>Note:</strong>
                      <p>{dentistComment}</p>
                    </div> : null }
                </Panel>
              </PanelContainer>
              {formSchema && treatment && <PatientInfo treatment={treatment} formSchema={formSchema} />}
            </Col>
          </Row>
        </div>
      );
    }
  }

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const formSchemaRecord = selectFormSchema ? selectFormSchema('consultations')(state, props) : List();
    const user = selectUser()(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : { recordDidNotInit: true },
      formSchema: formSchemaRecord ? formSchemaRecord.toJS() : [],
      user
    };
  }

  return connect(mapStateToProps)(ViewConsultationPage);
}
