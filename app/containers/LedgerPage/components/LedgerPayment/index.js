/**
*
* LedgerPayment
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import { Link } from 'react-router';
import styles from './styles.css';
import moment from 'moment-timezone';
import Info from 'containers/PatientsPage/components/Info';
import ReactTable from "react-table";
import Spinner from 'components/Spinner';
import EditRecordForm from 'components/EditRecordForm';
import { loadLedgerPayment, createLedgerPayment, updateLedgerPayment, deleteLedgerPayment } from 'blocks/ledger';
import AlertMessage from 'components/Alert';
import ConfirmButton from 'components/ConfirmButton';
import { currencyFormatter } from 'utils/currencyFormatter.js';

const ButtonItem = ({config, open}) => {
  const { title, glyphIcon, imageIcon, btnType, width, height, printIcon, referIcon, inviteButton } = config;
  switch(btnType){
    case 'link':
      return <Link className={referIcon ? styles.referStyles : printIcon ? styles.printLink : styles.link} onClick={open}>
               {title || ''}
             </Link>
    case 'image':
      return <Col onClick={open}>
              {imageIcon && imageIcon.map((image,index)=><img key={index} width={width} height={height} src={image} alt={title || ''} className={styles.img} />)}
             </Col>
    default:
      return <Button bsStyle="standard" onClick={open} className={inviteButton ? `action-button ${styles.button}` : `action-button`}>{title || ''}</Button>
  }
}

const Children = ({children}) => children ? <Col style={{textAlign:"center"}} dangerouslySetInnerHTML={{__html: children}} /> : null;

const Tooltip = ({text, show}) => show ? <span className={styles.tooltip}>{text}</span> : null;

class LedgerPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, showView: false, addForm: false, editForm: false, loading: false, confirmDialogBox : false, failureAlert: false, successAlert: false, alertText: '', records: [], selectedRecord: {} };
  }

  componentDidMount(){
    if(this.props.onRef) {
      this.props.onRef(this);
    }
  }

  componentWillReceiveProps(props) {
    if(this.props.error != props.error && this.props.config && this.props.config.showError) {
      this.props.config.showError(props.error);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  editableColumnProps = {
    Cell: props => {
      const { column } = props;
      return (
        column.action ?
          <Col className={styles.actions}>
            <Button bsStyle="link" bsSize="xs" className={styles.btnDelete} onClick={()=>{this.showEditForm(props.original)}}>
              <Icon glyph="icon-fontello-edit" className={styles.icon} />Edit
            </Button>
            <ConfirmButton onConfirm={() => {this.handleDelete(props.original.id)}}>
              {(click) => <Button bsStyle="link" bsSize="xs" className={styles.btnDelete} onClick={() => click()}>
                  <Icon glyph="icon-fontello-stop" className={styles.icon} />Delete
              </Button>}
            </ConfirmButton>
          </Col> : <span>{props.value ? column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : column.currency == true ? currencyFormatter.format(props.value) : props.value : ''}</span>
      );
    }
  }

  render() {
    const { fields, initialValues, handleSubmit, error, updateError, pristine, submitting, config = {}, noteText, notes, dispatch, form, sharedId, infoColumns, metaData, getCategories, procedureDetails, datePerformed, patientName } = this.props;
    const { confirmDialogBox, failureAlert, showView, addForm, editForm, alertText, loading, records, selectedRecord } = this.state;
    const { dentalCarrier = [] } = metaData || {}
    const { centerAlign, confirmDialog, title, bsSize, tooltip, viewType, shareDet, topView, bottomView, closeModal, modalClass, btnName, className, style, disable, titleStyle, hideSaveButton, hideCancelBtn, modalTitle } = config;
    const RowComponent = viewType == 'tab' ? Tabs : Row;
    const ColComponent = viewType == 'tab' ? Tab : 'div';
    const tableColumns = fields.filter((column) => column.visible);

    return (
      <Col style={style} className={title == 'Extend Validity' ? null : `${className ? className : ''} ${styles.ModalRecordForm}`}>
        <Children children={topView} />
        <ButtonItem config={config} open={this.open.bind(this)} />
        <Children children={bottomView} />
        <Tooltip text={title} show={tooltip} />
        <Modal show={this.state.showModal} bsSize={bsSize} onHide={this.close.bind(this)} backdrop="static" className={!showView ? "LedgerPage" : "LedgerForm"}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span className={styles.header}>{addForm ? 'Add Payment' : editForm ? 'Edit Payment' : title}</span>
              {!showView ?
                <Button bsStyle="standard" bsSize="sm" className={styles.addBtn} onClick={() => this.showAddForm()}>
                  Add Payment
                </Button> : null}
            </Modal.Title>
            {!showView ?
              <div style={{ marginTop: '2em', fontSize: '16px' }}>
                <span><b>Patient Name:</b> {patientName}</span><br />
                <span><b>Procedure Code:</b> {procedureDetails.procedureCode}</span><br />
                <span><b>Date performed:</b> {moment(datePerformed).format('MM/DD/YYYY')}</span>
              </div> : null}
          </Modal.Header>
		      <Modal.Body id="modalrecordform">
            <Col>
              {!loading && !showView ?
                <ReactTable
                  columns={tableColumns.map((column) => Object.assign(
                      {},
                      { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                  ))}
                  data={records}
                  pageSize={records && records.length > 0 ? 10 : 0}
                  showPageJump={false}
                  resizable={false}
                  showPageSizeOptions={false}
                  previousText={"Back"}
                  pageText={""}
                  noDataText={"No entries to show."}
                  sortable={false}
                /> : showView ?
                <div>
                  <EditRecordForm
                    fields={fields}
                    metaData={metaData}
                    form={addForm ? `createLedger` : `editLedger`}
                    onSubmit={this.handleSubmit.bind(this)}
                    initialValues = {selectedRecord}
                    catalogButton={true}
                    disableCancel={true}
                    onCancel={() => this.onCancel()}
                  />
                </div> : <Spinner />}
            </Col>
            {this.state.failureAlert ?
              <AlertMessage warning dismiss={() => this.setState({ failureAlert: false })}>{alertText}</AlertMessage> : null}
            {this.state.successAlert ?
              <AlertMessage success dismiss={() => this.setState({ successAlert: false, showView: false })}>{alertText}</AlertMessage> : null}
          </Modal.Body>
        </Modal>
      </Col>
    );
  }

  showconfirmDialog() {
    this.setState({ confirmDialogBox: true });
  }

  close() {
    this.setState({ showModal: false, showView: false, addForm: false, editForm: false, records: [] });
  }

  showAddForm() {
    this.setState({ selectedRecord: {}, showView: true, addForm: true });
  }

  showEditForm(selected) {
    this.setState({ selectedRecord: selected, showView: true, addForm: false, editForm: true });
  }

  onCancel() {
    this.setState({ showView: false, selectedRecord: {}, addForm: false, editForm: false });
  }

  handleSubmit(data, dispatch, { form }) {
    const { patientId, treatmentPlanId } = this.props;
    let record = {}
    record.patientId = parseInt(patientId);
    record.treatmentPlanId = parseInt(treatmentPlanId);
    let submitRecord = data.toJS();
    if(submitRecord.date === "" || submitRecord.date === undefined) {
      return this.setState({ failureAlert: true, alertText: 'Please select Date.' })
    }
    if(submitRecord.paidBy === "Insurance" && (submitRecord.carrierId === "" || submitRecord.carrierId === undefined)){
      return this.setState({ failureAlert: true, alertText: 'Please select Carrier.' })
    }
    if((submitRecord.paymentType === "" || submitRecord.paymentType === undefined) && (submitRecord.adjustmentReason === "" || submitRecord.adjustmentReason === undefined)) {
      return this.setState({ failureAlert: true, alertText: 'Please select Payment Type.' })
    }
    if((submitRecord.paymentRefNumber === "" || submitRecord.paymentRefNumber === undefined) && (submitRecord.paymentType !== undefined && submitRecord.paymentType !== "" && submitRecord.paymentType !== "Cash")) {
      return this.setState({ failureAlert: true, alertText: 'Please enter Payment Ref Num.' })
    }
    if(form === 'createLedger') {
      const submitData = { ...submitRecord, ...record };
      createLedgerPayment(submitData)
        .then(response => {
          this.getPaymentDetails(record)
          this.setState({ successAlert: true, alertText: 'Payment created successfully' })
        })
        .catch((error) => {
          const errorResponse = JSON.parse(JSON.stringify(error));
          console.log("error = ",errorResponse)
          if(errorResponse != undefined && errorResponse != '') {
            const errorText = errorResponse.response.data.map(( code ) => code.message)
            console.log("errorText = ",errorText)
            this.setState({ failureAlert: true, alertText: errorText[0] });
          } else {
            this.setState({ failureAlert: true, alertText: 'There was an error while creating the payment. Please try again.' })
          }
        });
    } else {
      const submitData = { ...submitRecord, ...record };
      updateLedgerPayment(submitData)
        .then(response => {
          this.getPaymentDetails(record)
          this.setState({ successAlert: true, alertText: 'Payment updated successfully' })
        })
        .catch((error) => {
          const errorResponse = JSON.parse(JSON.stringify(error));
          console.log("error = ",errorResponse)
          if(errorResponse != undefined && errorResponse != '') {
            const errorText = errorResponse.response.data.map(( code ) => code.message)
            console.log("errorText = ",errorText)
            this.setState({ failureAlert: true, alertText: errorText[0] });
          } else {
          this.setState({ failureAlert: true, alertText: 'There was an error while updating the payment. Please try again.' })
        }
      }
      );
        
    }
  }

  handleDelete(id) {
    const { patientId, treatmentPlanId } = this.props;
    let record = {}
    record.patientId = parseInt(patientId);
    record.treatmentPlanId = parseInt(treatmentPlanId);
    deleteLedgerPayment(id)
      .then(response => {
        this.getPaymentDetails(record)
      })
      .catch(() => this.setState({ failureAlert: true, alertText: 'There was an error while deleting the payment. Please try again.' }));
  }

  getPaymentDetails(record) {
    loadLedgerPayment(record)
      .then(paymentData => this.setState({ records: paymentData }))
      .catch(() => this.setState({ records: [], failureAlert: true, alertText: 'There was an error loading the resource. Please try again.' }));
  }

  open(e) {
    e.stopPropagation();
    this.setState({ showModal: true });
    const { treatmentPlanId, dispatch, patientId } = this.props;
    let record = {}
    record.patientId = patientId;
    record.treatmentPlanId = treatmentPlanId;
    this.getPaymentDetails(record);
  }

}

LedgerPayment.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  updateError: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  record: PropTypes.object,
  fields: PropTypes.array,
  config: PropTypes.object
};

export default reduxForm({
  form: 'modalRecord',
  enableReinitialize: true,
})(LedgerPayment);
