/**
*
* InlineRecordForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm, change } from 'redux-form/immutable';

import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';

import styles from './styles.css';

import { Row, Col, Button } from '@sketchpixy/rubix';

const InlineRecordForm = (props) => {
  const { handleSubmit, onProviderChange, pristine, submitting, fields, inline, path, metaData, locationState, record, btnName, disableCancel, disable, dispatch, form, leftAlign } = props;
  const { provider, providers = [], patientList = [] } = metaData || {}
  const buttonStyle = inline ? 'link' : 'standard';
  const buttonClass = inline ? styles.buttonLink : styles.buttonStandard;
  const buttonGroupClass = !inline ? styles.actionButtons : null;

  const handleChange = (val) => {
    fields.filter(function(el) {
      return el.value == "amount" || el.value  == "mode" ?
      el.editRecord = (val == 'Closed' ? true : false) :
      el.value == "collectedDate" || el.value == "amountCollected" ?
      el.editRecord = (val == 'Paid' ? true : false)
      : el})
    if(form === 'ledger') {
      handleSubmit();
    }
  }
  
  return (
    <form onSubmit={handleSubmit}>
      <Row className={leftAlign ? styles.alignLeft : styles.alignRight}>
        {fields.map((field, i) => {
          const Component = ImplementationFor[field.type];
          const contentType = ContentTypes[field.type];
          return (
            (field.editRecord ?
              <div key={i} className="hidden-print">
                {field.newLine ? <div style={{marginTop: '25px'}}><br /><br /><br /></div> : null}
                <Component
                  data={field}
                  contentType={contentType}
                  inline={inline}
                  options={field.formFieldDecorationOptions}
                  style={field.style}
                  labelStyle={field.labelStyle}
                  optionsMode="edit"
                  metaData={metaData}
                  record={record}
                  inputchange={(val) => handleChange(val)}
                 />
              </div>
            : null)
          );
        })}
        <div className={styles.marginTop}>
          <Button bsStyle={buttonStyle} type="submit" disabled={!disable && (pristine || submitting) || disable} className={submitting ? `${buttonClass} buttonLoading` : buttonClass}>{btnName || 'Save'}</Button>
          {!inline && !disableCancel ? <Link to={{pathname:path, state:locationState}}>Cancel</Link> : null}
        </div>
      </Row>
    </form>
  );
};

InlineRecordForm.propTypes = {
  handleSubmit: PropTypes.func,
  onProviderChange: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  inline: PropTypes.bool,
  path: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
  record: PropTypes.object, 	
};

export default reduxForm({
  form: 'editRecord',
})(InlineRecordForm);
