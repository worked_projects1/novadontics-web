/*
 * LedgersPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';

import { Row, Grid, Col } from '@sketchpixy/rubix';
import styles from './styles.css';

import { selectUser } from 'blocks/session/selectors';
import { Collapse } from 'antd';
import moment from 'moment-timezone';
import ModalForm from 'components/ModalRecordForm';
import { loadLedger } from 'blocks/ledger';
import { convertObjectToParams } from 'utils/tools';
import { setApiFilter } from 'utils/api';
import AlertMessage from 'components/Alert';
import EditRecordForm from 'components/EditRecordForm';
import InlineForm from './components/InlineRecordForm';
import LedgerPayment from './components/LedgerPayment';
import Spinner from 'components/Spinner';
import ReactTable from "react-table";
import ReactTooltip from 'react-tooltip';
import ShowText from 'components/ShowText';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import Info from 'containers/PatientsPage/components/Info';
import infoSchema from 'routes/schema';
import InsFeeForm from 'containers/InsurancePage/InsFeeForm.js';
import library from 'routes/schema_1';


const { Panel } = Collapse;
export default function (name, path, columns, actions, selectors) {
    const { selectHeaders, selectRecords, selectRecordsMetaData, selectLoading, pageLoading } = selectors;

    class LedgerPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

        static propTypes = {
            records: PropTypes.array,
            children: PropTypes.object,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool,
            pageLoader: PropTypes.bool
        };

        constructor(props) {
          super(props);
          this.state = { submitRecord: false, showSpinner: true, patientId: null, patientName: '' };
        }

        componentDidMount(){
          const { router = {} } = this.props;
          const { location = {} } = router;
          const { state = {} } = location;
          var patientObject = {}
          const interval = setInterval(() => {
            if(state.patientId) {
              patientObject = {patientId: state.patientId};
              this.setState({ patientId: state.patientId });
            }
            this.props.dispatch(loadLedger());
            this.handlePatientFormSubmit(patientObject);
            clearTimer();
          }, 1000);
          const clearTimer = () => clearInterval(interval);
        }

        shouldComponentUpdate(nextProps, nextState) {
          return shallowCompare(this, nextProps, nextState);
        }

        handleFormSubmit(data, dispatch, { form }) {
          const submitRecord = data.toJS();
          this.setState({ patientId: submitRecord.patientId });
          dispatch(actions.loadRecordsParams(submitRecord, true));
        }

        handlePatientFormSubmit(patientObject) {
          this.props.dispatch(actions.loadRecordsParams(patientObject, true));
        }

        editableColumnProps = {
          Cell: props => {
            const { column } = props;
            const { metaData } = this.props;
            const { patientId } = this.state;
            return (
              column.action ?
                <Col className={styles.actions}>
                  <LedgerPayment
                    fields={library().ledger().ledgerPayment}
                    form={`ledgerPayment.${props.original.treatmentPlanId}`}
                    procedureDetails={{procedureCode: props.original.cdcCode, procedureType: props.original.procedureName}}
                    treatmentPlanId={props.original.treatmentPlanId}
                    datePerformed={props.original.datePerformed}
                    patientId={patientId}
                    patientName={props.original.patientName}
                    metaData={metaData}
                    config={{ title: "View Payment", btnType: 'link' }}
                  />
                </Col> :
              column.label == 'Procedure Description' ?
                  <span style={{ display: 'flex' }}>
                    {column.limit && props.value && props.value.length > column.limit ?
                       props.value.substring(0, column.limit) + '...' : props.value}
                    {column.limit && props.value && props.value.length > column.limit ?
                        <ShowText title={column.label} text={props.value}>{(open) => <div className={styles.viewIcon} onClick={open.bind(this)} />}</ShowText> : null}
                  </span> : <span>{props.value ? column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : column.currency == true ? currencyFormatter.format(props.value) : props.value : ''}</span>
            );
          }
        }

        render() {
            const { user = {}, metaData = {}, headers, records, loading, pageLoader } = this.props;
            const pdfAuthToken = user && user.secret || user && user.token;
            const { showSpinner, patientId, patientName } = this.state;
            const tableColumns = columns.filter((column) => column.visible);
            const patientField = [{ value: 'patientId', label: 'Patient', ledgerWidth: true, showDropdown: true, ledgerChange: true, editRecord: true, type: 'search', oneOf: 'patientList' }]
            const { secret } = user;
            return (<div>
                <Helmet title="Novadontics" />
                <ReactTooltip type="info" html={true} />
                <div>
                  <Grid className={styles.horizontalPadding}>
                    <div className={styles.backgroundStyle} style={{ minHeight: `${document.body.clientHeight - 120}px` }}>
                      <Row className={styles.formColumn}>
                        <Col sm={12}>
                          <span className={styles.titleFee}>
                            Ledger
                          </span>
                        </Col>
                      </Row>
                      {pageLoader ? <Spinner /> :
                        <div>
                          <div style={{ display: 'flex', justifyContent: 'space-between', marginLeft: '3em', marginTop: '3em' }}>
                            <div style={{ display: 'flex' }}>
                              <a href={patientId != null ? `${process.env.API_URL}/treatmentPlanDetailsPDF?X-Auth-Token=${pdfAuthToken}&id=${patientId}&status=Proposed,Accepted,Completed,Referred Out` : null} style={{marginRight:'12px'}} target="_blank" data-rel="external">
                                  <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Treatment Plan Details PDF"} alt="pdf" className={styles.imgC} /><br></br>
                                  <span style={{marginLeft:"14px",color:"black"}}>Tx</span><br></br><span style={{marginLeft:'-1px',color:"black"}}>History</span>
                              </a>
                              <a href={patientId != null ? `${process.env.API_URL}/patientEstimatePDF?X-Auth-Token=${pdfAuthToken}&patientId=${patientId}` : null} style={{marginRight:'16px'}} target="_blank" data-rel="external">
                                  <img src={require('img/patients/estimate.svg')} ref='foo' data-place={"bottom"} data-tip={"Estimate"} alt="pdf" className={styles.estimate} /><br></br>
                                  <span style={{marginLeft:"14px",color:"black"}}>Tx</span><br></br><span style={{marginLeft:'-5px',color:"black"}}>Estimate</span>
                              </a>
                              <a href={patientId != null ? `${process.env.API_URL}/PatientStatementPDF?X-Auth-Token=${pdfAuthToken}&patientId=${patientId}` : null} style={{marginRight:'16px'}} target="_blank" data-rel="external">
                                  <img src={require('img/patients/statement.svg')} ref='foo' data-place={"bottom"} data-tip={"Statement"} alt="pdf" className={styles.statement} /><br></br>
                                  <span style={{marginLeft:"14px",color:"black"}}>Tx</span><br></br><span style={{marginLeft:'-8px',color:"black"}}>Statement</span>
                              </a>
                              <a href={patientId != null ? `${process.env.API_URL}/treatmentPlanDetailsPDF?X-Auth-Token=${pdfAuthToken}&id=${patientId}&status=Referred Out` : null} style={{marginRight:'25px'}} target="_blank" data-rel="external">
                                  <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Referred Out"} alt="pdf" className={styles.referredOut} /><br></br>
                                  <span style={{color:"black"}}>Referred</span><br></br><span style={{marginLeft: "8px", color:"black"}}>out Tx</span>
                              </a>
                              <a href={patientId != null ? `${process.env.API_URL}/treatmentPlanDetailsPDF?X-Auth-Token=${pdfAuthToken}&id=${patientId}&status=Existing` : null} target="_blank" data-rel="external">
                                  <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Existing Conditions"} alt="pdf" className={styles.existing} /><br></br>
                                  <span style={{color:"black"}}>Existing</span><br></br><span style={{marginLeft: "-8px", color:"black"}}>Conditions</span>
                              </a>
                              <Info data={infoSchema().patients().treatmentPlanDetailsInfo}/>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <Link to={patientId != null ? { pathname: `/patients/${patientId}/form`, state:{ openToothChart: true }} : null} style={{ marginRight: '5em' }}>
                                <div className={styles.patientIcon} />
                              </Link>
                              <InlineForm
                                fields={patientField}
                                initialValues={{patientId: patientId}}
                                metaData={metaData}
                                form={name}
                                disableCancel={true}
                                btnName={"Go"}
                                onSubmit={this.handleFormSubmit.bind(this)}
                              />
                            </div>
                          </div>
                          <Row className={loading ? styles.tableSpinner : styles.table}>
                            {loading && !pageLoader ? <Spinner /> :
                              <Col className="LedgerPage">
                                <ReactTable
                                  columns={tableColumns.map((column) => Object.assign(
                                      {},
                                      { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                                  ))}
                                  data={records}
                                  getTdProps={(state, rowProps) => {
                                      return {
                                          style: rowProps && rowProps.original.totalColumn ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold'} : {}
                                      }
                                  }}
                                  pageSize={records && records.length || 0}
                                  showPageJump={false}
                                  resizable={false}
                                  sortable={false}
                                  showPageSizeOptions={false}
                                  showPagination={false}
                                  previousText={"Back"}
                                  pageText={""}
                                  noDataText={""}
                                />
                              </Col>}
                          </Row>
                        </div>}
                    </div>
                  </Grid>
                </div>
            </div>);
        }


    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    return connect(
        createSelector(
            selectUser(),
            selectRecords(),
            selectRecordsMetaData(),
            selectHeaders(),
            selectLoading(),
            pageLoading(),
            (user, records, metaData, headers, loading, pageLoader) => ({
                user,
                records: records ? records.toJS() : [],
                metaData: metaData ? metaData.toJS() : {},
                headers: headers ? headers.toJS() : false,
                loading: loading,
                pageLoader: pageLoader })
        ), mapDispatchToProps)(LedgerPage);
}
