/*
 * CreateRecordPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reset } from 'redux-form';

import CreateRecordForm from '../../components/CreateRecordForm';
import CreateProductForm from '../../components/CreateRecordForm/CreateProductForm.js';
import {Alert} from "@sketchpixy/rubix/lib/index";
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from '../RecordsPage/messages';

const SUCCESS_ALERT_TIMEOUT = 4000;

export default function (name, path, columns, selectRecord, createRecord, selectRecords,updateRecordsMetaData) {
  function handleCreateWithMeta(metaData) {
    return function handleCreate(data, dispatch, {form}) {
      let record = data.toJS();
      if(name == 'products.create') {
        record.categoryId = record.childId != undefined ? record.childId : record.parentId != undefined ? record.parentId : null;
        if (record.stockAvailable == '' || record.stockAvailable == undefined) {
          record.stockAvailable = false
        }
        if (record.financeEligible == '' || record.financeEligible == undefined) {
          record.financeEligible = false
        }
        if(record.video && record.video.length > 0) {
          record.video = record.video.map(url => ({url}))
        }
      }
      if(name == 'practices.create') {
        if (record.appointmentReminder == '' || record.appointmentReminder == undefined) {
          record.appointmentReminder = false
        }
        if (record.onlineRegistrationModule == '' || record.onlineRegistrationModule == undefined) {
          record.onlineRegistrationModule = false
        }
      }
      if(name == 'tutorials.create') {
        record.parent = record.parent != null ? parseInt(record.parent) : null;
      }
      if(name == 'manuals.create') {
        record.parent = record.parent != null ? parseInt(record.parent) : null;
      }
      if (record.taxExempted == '' || record.taxExempted == undefined) {
        record.taxExempted = false
      }
      Object.assign(record, metaData);
      dispatch(createRecord(record, form));
    };
  }

  function handleAccountCreate(data, dispatch, {form}) {
    const { practiceValue, accountAlertMessage } = this.state;
    let record = data.toJS();
    if(name == 'accounts.create') {
      if (record.prime == '' || record.prime == undefined) {
        record.prime = false
      }
      if (record.allowAllCECourses == '' || record.allowAllCECourses == undefined) {
        record.allowAllCECourses = false
      }
      if (record.authorizeCustId != '' && record.authorizeCustId != undefined) {
        record.authorizeCustId = record.authorizeCustId.toString()
      } else {
        record.authorizeCustId = "0"
      }
    }
    if (record.taxExempted == '' || record.taxExempted == undefined) {
      record.taxExempted = false
    }
    if(name == 'accounts.create' && record.appSquares && ((record.appSquares.includes("CE10") && record.appSquares.includes("CE5")) || (record.appSquares.includes("CE5") && record.appSquares.includes("CE3")) || (record.appSquares.includes("CE10") && record.appSquares.includes("CE3")))) {
      this.setState({ accountAlertMessage: true });
    } else {
      const formReload = setInterval(() => {
        Object.assign(record, practiceValue);
        dispatch(createRecord(record, form));
        clearReloadTimer();
      }, 300);
      const clearReloadTimer = () => clearInterval(formReload);
      this.onSuccess();
    }
  }

  function handleCreate(data, dispatch, { form }) {
      const { alertMessage } = this.state;
      const record = data.toJS();

      const groups = _.flow([
                       arr => _.groupBy(arr, 'groupId'),
                       g => _.filter(g, o => o.length > 1),
                       _.flatten
                     ])(record.groups.groups);

      if(groups.length > 0) {
        this.setState({ alertMessage: true });
      } else {
        const vendorReload = setInterval(() => {
          dispatch(createRecord(record, form));
          clearReloadTimer();
        }, 300);
        const clearReloadTimer = () => clearInterval(vendorReload);
        this.onSuccess();
      }
  }

  const resolveColumns = (columns, records) => typeof columns === 'function' ? columns(records).columns : columns;

  class CreateRecordPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = { accountAlertMessage: false, alertMessage: false, practiceValue: name === 'accounts.create' && props.location.state.data ? { practiceId: parseInt(props.location.state.data.practiceId, 10) } : {} };
    }

    render() {
      const { record, records, location = {} } = this.props;
      const { success, alertMessage, accountAlertMessage } = this.state;
      let metaData = {};
      if (name === 'accounts.create' && location.state.data) {
        metaData = {
          practiceId: parseInt(location.state.data.practiceId, 10),
        };
      }
      return (
        <div>
          {name === 'products.create' ?
            <CreateProductForm
              name={name}
              path={path}
              initialValues={name === 'accounts.create' && location.state.data ? {practiceId: parseInt(location.state.data.practiceId, 10)} : null}
              fields={resolveColumns(columns, records)}
              metaData={record.metaData}
              onSubmit={handleCreateWithMeta(metaData)}
              onSubmitSuccess={this.onSuccess.bind(this)}
              locationState={location.state}
              updateRecordsMetaData={updateRecordsMetaData}
              record={record}
            /> :
            <CreateRecordForm
              name={name}
              path={path}
              initialValues={name === 'accounts.create' && location.state.data ? {practiceId: parseInt(location.state.data.practiceId, 10)} : null}
              fields={resolveColumns(columns, records)}
              metaData={record.metaData}
              onSubmit={name === 'vendors.create' ? handleCreate.bind(this) : name === 'accounts.create' ? handleAccountCreate.bind(this) : handleCreateWithMeta(metaData)}
              onSubmitSuccess={name === 'vendors.create' || name === 'accounts.create' ? null : this.onSuccess.bind(this)}
              locationState={location.state}
              updateRecordsMetaData={updateRecordsMetaData}
              record={record}
            /> }
          {success?
            <Alert success>Your data has been saved successfully!</Alert> : null}
          {this.state.alertMessage ?
            <AlertMessage warning dismiss={() => { this.setState({ alertMessage: false }) }}><FormattedMessage {...messages.groupAlert} /></AlertMessage> : null}
          {this.state.accountAlertMessage ?
            <AlertMessage warning dismiss={() => { this.setState({ accountAlertMessage: false }) }}><FormattedMessage {...messages.accountAlert} /></AlertMessage> : null}
        </div>
      );
    }
    onSuccess() {
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ success: true });
      }, 500);
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ success: false });
        if(name == 'tutorials.create' || name == 'manuals.create') {
          if(this.props.records.length == 0) {
            columns.map((field) => {
              if(field.value == 'name' || field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
                field.editRecord = false
              }
            })
          }
        }
      }, SUCCESS_ALERT_TIMEOUT);
    }
  }

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const selectedRecords = selectRecords && selectRecords()(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : {},
      records: selectedRecords ? path.indexOf('tutorials') > -1 || path.indexOf('manuals') > -1 ? selectedRecords : selectedRecords.toJS() : []
    };
  }

  CreateRecordPage.propTypes = {
    record: PropTypes.object,
  };

  return connect(mapStateToProps)(CreateRecordPage);
}
