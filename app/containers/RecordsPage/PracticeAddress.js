import React from 'react';
import {get} from '../../utils/misc';
import {Col, Grid, Label, PanelContainer, Row} from "@sketchpixy/rubix/lib/index";
import {Panel, PanelBody, PanelHeader} from "@sketchpixy/rubix/lib/Panel";
import styles from './styles.css';

const PracticeAddress = ({address = {}}) =>
  <PanelContainer className={styles.practiceAddress}>
    <Panel>
      <PanelHeader>
        <Grid>
          <Row>
            <Col xs={12}>
              <h4>
                Practice Address
              </h4>
              <h5>{address.isPrimary && <Label bsStyle="info">Primary</Label>}&nbsp;</h5>
            </Col>
          </Row>
        </Grid>
      </PanelHeader>
      <PanelBody>
        <Grid>
          <Row>
            <Col md={5}>
              <p>{get(address, 'name')}</p>
              <p>{get(address, 'phone')}</p>
            </Col>
            <Col md={7}>
              <p>{get(address, 'address')}</p>
              <p>{`${get(address, 'city')}, ${get(address, 'state')} ${get(address, 'zip')}`}</p>
              <p>{`${get(address, 'country')}`}</p>
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </Panel>
  </PanelContainer>

export default PracticeAddress;