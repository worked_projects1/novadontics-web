/*
 * RecordsPage Messages
 *
 * This contains all the text for the UsersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  noItems: {
    id: 'app.components.RecordsPage.noItems',
    defaultMessage: 'There are no records. Create the first record by clicking on button above.',
  },
  noCourse: {
    id: 'app.components.RecordsPage.noCourse',
    defaultMessage: 'There are no records to show right now.',
  },
  loading: {
    id: 'app.components.RecordsPage.loading',
    defaultMessage: 'Loading…',
  },
  error: {
    id: 'app.components.RecordsPage.error',
    defaultMessage: 'There was an error loading the resource. Please try again.',
  },
  recallSuccess: {
    id: 'app.components.RecordsPage.success',
    defaultMessage: 'Recall Period updated successfully',
  },
  consultSuccess: {
    id: 'app.components.RecordsPage.consultSuccess',
    defaultMessage: 'Thank you, your payment was successful Your request for a consultation has been successfully sent. The consultant will be contacting you shortly to set up a consultation.',
  },
  xray: {
    id: 'app.components.RecordsPage.xray',
    defaultMessage: 'Please Contact Novadontics for X-Ray / CT Scan Module Access',
  },
  doseSpot: {
    id: 'app.components.RecordsPage.doseSpot',
    defaultMessage: 'Coming soon',
  },
  checklist: {
    id: 'app.components.RecordsPage.checklist',
    defaultMessage: 'Your account is not enabled to access Checklist feature. Please email us at: team@novadontics.com to enable this.',
  },
  requestConsult: {
    id: 'app.components.RecordsPage.requestConsult',
    defaultMessage: 'Your account is not enabled to access Request Online Consult feature. Please email us at: team@novadontics.com to enable this.',
  },
  catalog: {
    id: 'app.components.RecordsPage.catalog',
    defaultMessage: 'Shortly you will be receiving an email confirmation of your order along with your order number.',
  },
  catalogCheck: {
    id: 'app.components.RecordsPage.catalogCheck',
    defaultMessage: 'Order placement was successful. Your items will be shipped after your check is received and cleared by your financial institution.',
  },
  groupAlert: {
    id: 'app.components.RecordsPage.groupAlert',
    defaultMessage: 'You have added same Group multiple times. So please remove and save again.',
  },
  accountAlert: {
    id: 'app.components.RecordsPage.accountAlert',
    defaultMessage: 'You have selected CE courses access type multiple times. So please remove and save again.',
  },
  feeSuccess: {
    id: 'app.components.RecordsPage.feeSuccess',
    defaultMessage: 'Insurance Fee updated successfully',
  },
  feeFailure: {
    id: 'app.components.RecordsPage.feeFailure',
    defaultMessage: 'Please check the details you entered and try again.',
  },
  implantSuccess: {
    id: 'app.components.RecordsPage.implantSuccess',
    defaultMessage: 'Implant Data saved successfully',
  },
  implantFailure: {
    id: 'app.components.RecordsPage.implantFailure',
    defaultMessage: 'There was an error creating this record. Have you supplied all the required fields? Please try again.',
  },
  saasSuccess: {
    id: 'app.components.RecordsPage.saasSuccess',
    defaultMessage: 'SaaS payment updated successfully.',
  },
  documentUpload: {
    id: 'app.components.RecordsPage.documentUpload',
    defaultMessage: 'Please select a document category to add document.',
  }
});
