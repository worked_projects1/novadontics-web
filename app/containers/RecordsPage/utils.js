import React from 'react';
import {sortBy} from "lodash.sortby";
import {Link} from 'react-router';
import DeleteRecordForm from 'components/DeleteRecordForm';
import styles from './styles.css';

export function filterRecords(rows, columns, searchQuery) {
  const filteredRows = [];
  if (searchQuery === null || searchQuery === '') {
    return rows;
  }

  rows.forEach(row => {
    columns.some(column => {
      if (row[column.value] !== undefined && row[column.value] !== null) {
        const rowValue = String(row[column.value]).toLowerCase();
        if (rowValue.length >= searchQuery.length && rowValue.indexOf(searchQuery.toLowerCase()) >= 0) {
          filteredRows.push(row);
          return true;
        }
      }
      return false;
    });
  });
  return filteredRows;
}
