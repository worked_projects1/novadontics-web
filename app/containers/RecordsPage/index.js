/*
 * RecordsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { browserHistory, Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';
import { convertObjectToParams } from 'utils/tools';
import Spinner from 'components/Spinner';
import TableWrapper from 'components/TableWrapper';
import ModalForm from 'components/ModalRecordForm';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import {sortBy} from "lodash.sortby";
import styles from './styles.css';
import Info from '../PatientsPage/components/Info';
import { Alert, Button, Col, PanelBody, PanelContainer, Row } from '@sketchpixy/rubix';
import CsvImporter from 'components/CsvImporter';
import PracticeAddress from './PracticeAddress';
import { Grid } from '@sketchpixy/rubix/lib/index';
import schema from '../../routes/schema';
import library from '../../routes/schema_1';
import { setApiFilter } from 'utils/api';
import { type } from 'os';
import { loadStateTax, updateStateTax, updateVendorCategory, getVendorCategory } from 'blocks/vendors/stateTax';
import { createImplantData } from 'blocks/implantLog/remotes.js';
import { createBoneGraftData } from 'blocks/boneGraftLog/remotes.js';
import StateTax from 'components/StateTax';
import { Collapse } from 'antd';
import { selectUser } from 'blocks/session/selectors';
import AlertMessage from 'components/Alert';
import { filterRecords } from './utils.js';
import SaasPaymentSummary from 'module/fragment/SummaryTable';
import ImplantLogForm from 'components/ImplantLogForm';
import moment from 'moment';
import { currencyFormatter } from 'utils/currencyFormatter.js';

const { Panel } = Collapse;
export default function (name, path, create, columns, actions, selectors, reactTable) {
  const {
    selectLoading,
    selectRecords,
    selectError,
    selectUpdateError,
    selectHeaders,
    selectRecordsMetaData,
    selectChildren,
    selectSearch,
    selectSearchValue
  } = selectors;

  class RecordsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
      records: PropTypes.array,
      children: PropTypes.object,
      error: PropTypes.bool,
      updateError: PropTypes.string,
      loading: PropTypes.bool
    };

    constructor(props) {
      super(props);
      this.state = {
        printHeight: 0,
        searchValue: '',
        editing: false,
        selectedData: false,
        vendorCategoryList: {},
        patientsColStyle: true,
        alertMessage: false,
        implantFailureAlert: false,
      };
    }

    componentDidMount() {
      const { headers = {}, dispatch, children, location = {} } = this.props;
      const [status] = columns.filter((s) => s.value === 'status' || s.value === 'accountType');
      headers.status = status && status.oneOf && headers.status == '' ? status.oneOf[0] : headers.status;
      if (dispatch) {
        setApiFilter(Object.keys(headers).filter(_ => _ != "content-length").reduce((r, k) => Object.assign({}, r, { [k]: headers[k] }), {}));
        dispatch(actions.updateHeader({ 'status': headers.status }));
        dispatch(actions.loadRecords());
      }
      const elmnt = document.getElementById("print-body");
      if (elmnt) {
        this.setState({ printHeight: elmnt.offsetHeight });
      }
      if(location.state && location.state.data) {
        this.props.dispatch(actions.updateSearchValue(null, true));
      }
    }

    componentWillReceiveProps(nextProps) {
      this.setState({ searchValue: nextProps.searchedValue });
    }

    shouldComponentUpdate(nextProps, nextState) {
      return shallowCompare(this, nextProps, nextState);
    }

    handleCsvImport(results, callback) {
      this.props.dispatch(actions.updateRecords(results, callback));
    }

    handleStateTax(data, dispatch, { form }) {
      const record = data.toJS();
      if (JSON.stringify(this.state.selectedData) != JSON.stringify(record)) {
        dispatch(updateStateTax(record));
        this.setState({ editing: false, selectedData: false });
      }
    }

    handleCollapse(data){
      this.props.dispatch(actions.updateChildren(data));
    }

    handleChange = e => {
      var data = e.target.value
      this.setState({ searchValue: e.target.value })
      setTimeout(() => this.handleSearch(), 1000);
    };

    handleSearch() {
      const searchList = this.state.searchValue;
      const search = filterRecords(this.props.records, columns, searchList)
      this.props.dispatch(actions.updateSearch(search, true));
      this.props.dispatch(actions.updateSearchValue(searchList, true));
    }

    render() {
      const { error, updateError, loading, children, location = {}, route = {}, dispatch, headers, metaData, user, activeChildren, searchedRecords, searchedValue } = this.props;
      const { printHeight, patientsColStyle, vendorCategoryList } = this.state;
      // This one is pretty specific for RecordsPage
      const categoriesButtonPath = _.get(route, ['data', 'categoriesButton', 'path']);
      let records = name === 'implantLog' ? this.props.records.implantData : name === 'boneGraftLog' ? this.props.records.boneGraftData : this.props.records;
      let isShippingPage = name === 'shipping';
      const [status] = name === 'implantLog' || name === 'ceRequests' || name === 'boneGraftLog' ? [] : records.map((s) => s.status);
      const [accountType] = name === 'implantLog' || name === 'boneGraftLog' ? [] : records.map((s) => s.accountType);
      let displayName = name;
      let openCost = '';
      let closedCost = '';
      let titleOrange = false;
      if (name === 'accounts' && location.state && location.state.data) {
        const { practiceId, practiceName } = location.state.data;
        create = !!practiceId;
        records = this.props.searchedValue == null ? this.props.records.filter((record) => record.practiceId === practiceId) : this.props.searchedRecords.filter((record) => record.practiceId === practiceId);
        displayName = `${name} - ${practiceName}`;
      } else if (name === 'accounts') {
        create = false;
        records = this.props.searchedValue == null ? this.props.records : this.props.searchedRecords;
      } else if (isShippingPage) {
        displayName = 'Shipping Addresses'
        records = records.filter(_ => !_.isPractice);
        titleOrange = true;
      } else if (name === 'orders') {
        titleOrange = true;
      } else if (name === 'implantLog') {
        displayName = 'Implant Logbook';
        titleOrange = true;
      }  else if (name === 'boneGraftLog') {
        displayName = 'Bone Graft Logbook';
        titleOrange = true;
      } else if (name === 'ceRequests') {
        displayName = 'Viewed and Purchased Continuing Education Courses with CE Status';
        records = _.sortBy(records, 'dateRequested').reverse();
        titleOrange = true;
      } else if (name === 'consultations') {
        const openRecords = records && records.length > 0 ? records.filter((record) => record.status === 'Open') : [];
        const openCostArr = openRecords && openRecords.length > 0 ? openRecords.map(item => item.cost) : [];
        openCost = openCostArr.reduce((a, b) => a + b, 0);
        const closedRecords = records && records.length > 0 ? records.filter((record) => record.status === 'Closed') : [];
        const closedCostArr = closedRecords && closedRecords.length > 0 ? closedRecords.map(item => item.cost) : [];
        closedCost = closedCostArr.reduce((a, b) => a + b, 0);
        titleOrange = true;
      } else if (name === 'saasAdmin') {
        displayName = 'SaaS'
      } else if (name === 'friendReferral') {
        displayName = 'Refer a Friend'
      } else if (name === 'emailMappings') {
        displayName = 'Email Mappings'
      } else if (name === 'inOfficeSupport') {
        displayName = 'Requests'
      } else if (name === 'documents') {
        displayName = 'Resource Center'
      } else if (name === 'courses') {
        displayName = 'Continuing Education'
      } else if (name === 'ciicourses') {
        displayName = 'Implant Education'
      } else if (name === 'products' && metaData) {
        displayName = 'Catalog Products'
        metaData['vendorOptions'] = metaData['vendors'] ? metaData['vendors'].filter((vendor) => vendor.deleted === false)
          .reduce((a, vendor) => {
            a.push({ label: vendor.name, value: vendor.id });
            return a;
          }, []) : [];
      }
      const { pathname } = location;
      const fullView = name === 'inOfficeSupport' && pathname.match( /(create)/ ) != null ? true : false;
      const dentistInOfficeSupport = user && user.role == 'dentist' && name === 'inOfficeSupport' ? true : false;
      const adminInOfficeSupport = user && user.role == 'admin' && name === 'inOfficeSupport' ? true : false;
      const adminAccount = user && user.role == 'admin' && name === 'accounts' ? true : false;

      const handleVendorSubmit = (data, dispatch, { form }) => {
        const record = data.toJS();
        updateVendorCategory(record)
          .then(res => {
              this.vendorCategoryForm.close();
              this.vendorCategoryForm.props.reset();
              this.setState({ vendorCategoryList: {} })
              this.props.dispatch(actions.loadRecords());
          })
          .catch(() => this.setState({ alertMessage: true }));
      };

      const handleCategories = (id) => {
        getVendorCategory()
          .then(res => {
            let filteredVendorCategory = res.find(_ => _.vendorId == id)
            if(filteredVendorCategory != undefined) {
              this.setState({ vendorCategoryList: filteredVendorCategory })
            } else {
              let vendorCatObj = {}
              vendorCatObj.vendorId = id
              this.setState({ vendorCategoryList: vendorCatObj })
            }
          })
          .catch(() => {
            let emptyObj = {}
            emptyObj.vendorId = id
            this.setState({ vendorCategoryList: emptyObj })
          });
      };

      return (
        <div>
          <Helmet title="Novadontics" />
          <div className={!fullView ? styles.view : null}>
            {!fullView ?
              <Grid style={{ paddingLeft: '20px' }}>
                <Row>
                  { dentistInOfficeSupport ?
                    <Col sm={12} className={styles.flexibleRow}>
                      <h1 className={styles.titleHead}>In-Office Support History</h1>
                      <div className={styles.flexible}>
                        <Link onClick={() => browserHistory.push(`/requestConsultation`)}>
                          <Button bsStyle="standard">
                            Create In-Office Support Request
                          </Button>
                        </Link>
                      </div>
                    </Col> :
                    adminInOfficeSupport ?
                    <Col sm={12} className={styles.flexibleRow}>
                      <h1 className={styles.titleHead}>In-Office Support History</h1>
                    </Col> : null}
                </Row>
              </Grid>
            : null}
            {!status || reactTable ?
              <Grid>
                <Row>
                  <Col sm={12} className={name === 'accounts' ? styles.flexibleAccountRow : styles.flexibleRow}>
                    <Col sm={12} className={styles.flexibleColumn}>
                      <div sm={12} className={styles.flexibleColumn}>
                        <h1 className={titleOrange ? styles.titleOrangeHead : styles.titleHead}>{displayName}</h1>
                        {name === 'implantLog' ?
                         <div className ={styles.logbook}>
                            <Info data={{attributes: { info: { text: `<div style="font-weight: normal;">Data displayed in the Logbook is automatically populated from data entered in the Clinical Notes in the patient file or can be entered directly in the Logbook. Changes to or deletion of data in the Logbook can be done here or in the Clinical Notes in the patient file (if the notes are not locked) and will be reflected in both locations.<br /><br /><div style="display: flex"><div style="width: 19px; height: 19px; background:black; border-radius: 50%;"></div><span style="padding-left:12px;"><div></div>Implant has been restored or is in the healing phase.</span></div><br /><div style="display: flex"><div style="width: 19px; height: 19px; background:red; border-radius: 50%;"></div><span style="padding-left:12px;"><div></div>Implant is ready to be restored.</span></div><br /><div style="display: flex"><div style="width: 19px; height: 19px; background: #38a0f1; border-radius: 50%;"></div><span style="padding-left:12px;"><div></div>Patient does not have a Novadontics file.</span></div><br /><div style="display: flex"><div style="width: 19px; height: 19px; background: #91288F; border-radius: 50%;"></div><span style="padding-left:12px;"><div></div>Implant has been removed.</span></div></div>`, openModal: true, labelStyle: true, title: 'Implant Logbook'}} }}  />
                          </div> : null}
                      </div>
                        {name === 'implantLog' ?
                          <div className={styles.flexibleColumn}>
                            <ImplantLogForm
                              fields={library().implantLog().columns}
                              form={`implantRecord`}
                              metaData={metaData}
                              onRef={implantDataForm => (this.implantDataForm = implantDataForm)}
                              onSubmit={handleImplantSubmit.bind(this)}
                              config={{ title:'Add Implant', titleStyle: true, style: { marginRight: '30px', marginTop: '15px' }}}
                            />
                            <ImplantLogForm
                              fields={library().implantLog().report}
                              form={`implantReport`}
                              metaData={metaData && metaData.providersList && metaData.implantBrandList  && Object.assign({}, metaData, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])},
                              {implantBrandList: metaData.implantBrandList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) || metaData}
                              onSubmit={handleReport.bind(this)}
                              config={{ title:'Implant Log', titleStyle: true, style: { marginRight: '20px', marginTop: '0px'}, btnType: "image",imageIcon: [require(`img/patients/reportOrange.svg`)]}}
                            />
                            <h1 className={styles.titleHead}>
                              Success Rate: {this.props.records.successRate}%
                            </h1>
                          </div>
                        : null}
                        {name === 'boneGraftLog' ?
                          <div className={styles.flexibleColumn}>
                            <ImplantLogForm
                              fields={library().boneGraftLog().columns}
                              form={`boneGraftRecord`}
                              metaData={metaData}
                              onRef={boneGraftDataForm => (this.boneGraftDataForm = boneGraftDataForm)}
                              onSubmit={handleBoneGraftSubmit.bind(this)}
                              config={{ title:'Add Bone Grafting Procedure', titleStyle: true, style: { marginRight: '30px', marginTop: '15px' }}}
                            />
                            <ImplantLogForm
                              fields={library().boneGraftLog().report}
                              form={`boneGraftReport`}
                              metaData={metaData && metaData.providersList  && Object.assign({}, metaData, {providersList: metaData.providersList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'ALL',label:'All'}])}) || metaData}
                              onSubmit={handleBoneGraftReport.bind(this)}
                              config={{ title:'Bone Graft Log', titleStyle: true, style: { marginRight: '20px', marginTop: '0px'}, btnType: "image",imageIcon: [require(`img/patients/reportOrange.svg`)]}}
                            />
                            <h1 className={styles.titleHead}>
                              Success Rate: {this.props.records.successRate}%
                            </h1>
                          </div>
                        : null}
                    </Col>
                    {create ?
                      <div className={name === "accounts" ? styles.accountFlexible : styles.flexible}>
                        {name === 'products' ?
                          <ModalForm
                            initialValues={{ printpdf: '' }}
                            fields={schema().products().printpdf}
                            form={`modalRecordPDF`}
                            onSubmit={handleSubmit.bind(this)}
                            metaData={metaData instanceof Array ? {} : metaData}
                            config={{
                              title: "Print PDF", glyphIcon: "icon-fontello-print", btnType: "link", centerAlign: true, printIcon: true
                            }}
                          />
                          : null}
                          {name === 'products' ?
                          <ModalForm
                            initialValues={{ vendorId: '' }}
                            fields={schema().products().exportcsv}
                            form={`modalRecordCSV`}
                            onSubmit={handleExportsubmit.bind(this)}
                            metaData={metaData && metaData.vendors && Object.assign({}, metaData, {vendors: metaData.vendors.reduce((a, vendor)=>{a.push({ label: vendor.name, value: vendor.id }); return a;},[])}) || metaData}
                            config={{
                              title: "Export CSV", glyphIcon: "icon-fontello-print", btnType: "link", centerAlign: true, printIcon: true
                            }}
                          />
                          : null}
                        {name === 'products' ?
                          <CsvImporter
                            name={name}
                            loading={loading}
                            columns={columns.filter(c => c.csvRequired).map(c => c.value)}
                            postError={updateError}
                            handleImport={:: this.handleCsvImport} /> : null}
                        {name != 'vendors' && categoriesButtonPath ? (
                          <Link to={{ pathname: categoriesButtonPath }}>
                            <Button bsStyle="standard" style={{ marginRight: 10 }}>Categories</Button>
                          </Link>
                        ) : null}
                        {name === 'vendors' && categoriesButtonPath ? (
                          <div style={{ display: 'flex', marginTop: '20px' }}>
                          <Link to={{ pathname: categoriesButtonPath }}>
                            <Button bsStyle="standard" style={{ marginRight: 10 }}>Vendor Groups</Button>
                          </Link>
                          <ModalForm
                            fields={schema().vendors().vendorCategories}
                            initialValues={vendorCategoryList}
                            form={`vendorCategories`}
                            metaData={metaData && metaData.vendors && metaData.categories && Object.assign({}, metaData, {vendors: metaData.vendors.reduce((a, vendor)=>{a.push({ label: vendor.name, value: vendor.id }); return a;},[])}, {categories: metaData.categories.reduce((a, category)=>{a.push({ label: category.name, value: category.id }); return a;},[])}) || metaData}
                            onRef={vendorCategoryForm => (this.vendorCategoryForm = vendorCategoryForm)}
                            onSubmit={handleVendorSubmit.bind(this)}
                            getCategories={handleCategories.bind(this)}
                            config={{title:'Categories', btnName: 'Update'}}
                          />
                          </div>
                        ) : null}
                        {displayName == 'SaaS' ?
                          <div>
                            <SaasPaymentSummary user={user} />
                          </div> : null}
                        {name != 'inOfficeSupport' ?
                          <Link to={{ pathname: `${path}/create`, state: location.state }}>
                            <Button bsStyle="standard">{name === 'shipping' ? 'add' : 'create'} {name === 'ciicourses' || name === 'shipping' || name === 'saasAdmin' ? displayName : name}</Button>
                          </Link> : null}
                      </div>
                      : null}
                    {name === 'accounts' ?
                      <span className={styles.groupSearch}>
                          <span className={styles.search}>Search: </span>
                          <input type='text' value={this.state.searchValue} className={styles.inputSearch} onChange={this.handleChange} placeholder={'Type to search...'} onKeyPress={event => {if (event.key === 'Enter') {this.handleSearch()}}}/>
                      </span>
                    : null}
                  </Col>
                </Row>
                <Row>
                  {isShippingPage && <PracticeAddress address={this.props.records.find(_ => _.isPractice)} />}
                </Row>
              </Grid>
              : null}
            <Row>
	            {!fullView ?
              <Col md={children ? 4 : 12} lg={children ? 6 : 12}
                className={children ? 'col-md-collapse-right hidden-print' : null}>
                {error ?
                  <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
                  <div>
                    {(status || accountType) && !reactTable ?
                        columns.map((column) =>
                          (column.oneOf && column.splitColumn ? column.oneOf.map((prop) =>
                            <div style={prop === 'Closed' ? { marginTop: '4em' } : null}>
                              <h1 className={titleOrange ? styles.titleOrange : styles.title}>
                                {prop}{" "}{name === 'inOfficeSupport' ? displayName : name}
                              </h1>
                              {name === 'consultations' ?
                                <div className={styles.costStyle}>
                                  {prop === 'Open' ? <span>Total Outstanding: {currencyFormatter.format(openCost)}</span> : prop === 'Closed' ? <span>Total Collected: {currencyFormatter.format(closedCost)}</span> : ""}
                                </div> : null}
                              <TableWrapper
                                records={records.filter((record) => (accountType ? record.accountType : record.status) === prop)}
                                columns={columns}
                                patientsColStyle={patientsColStyle}
                                children={children}
                                path={path}
                                name={prop}
                                searchValue={this.state.searchValue}
                              />
                            </div>
                          ) : null)
                        ) :
                        <TableWrapper
                          records={records}
                          columns={columns}
                          children={children}
                          path={path}
                          name={name}
                          patientsColStyle={patientsColStyle}
                          locationState={location.state}
                          headers={headers}
                          actions={actions}
                          dispatch={dispatch}
                          reactTable={reactTable}
                          loading={loading}
                          user={user}
                        />}
                  </div>}
                {this.state.alertMessage ?
                    <AlertMessage warning dismiss={() => this.setState({ alertMessage: false })}><FormattedMessage {...messages.feeFailure} /></AlertMessage> : null}
                {this.state.implantFailureAlert ?
                    <AlertMessage warning dismiss={() => this.setState({ implantFailureAlert: false })}><FormattedMessage {...messages.implantFailure} /></AlertMessage> : null}

	            </Col> : null }
              {children ?
                <Col md={!fullView ? 8 : 12} lg={!fullView ? 6 : 12} className={fullView ? `patient-block` : 'print-ready'} style={!fullView && name != 'inOfficeSupport' && name != 'accounts' ? { marginBottom: 50 } : !fullView && (dentistInOfficeSupport || adminAccount) ? { marginTop: 55 } : { marginBottom: 0 }}>
                  <PanelContainer className={!create && status ? styles.space : null}>
                    <PanelBody>
                      <Col sm={12} id="print-body" style={name === 'accounts' ? { minHeight: `${printHeight + 100}px` } : null}>
                        {children}
                      </Col>
                    </PanelBody>
                  </PanelContainer>
                </Col>
                : null}
            </Row>
            {loading ? <Spinner /> : null}
            {!loading && (name === 'implantLog' || name === 'boneGraftLog') ?
              <Row>
                <Col sm={12} className={name === 'implantLog' || name === 'boneGraftLog' ? styles.flexibleRow : null}>
                  <Col sm={12} className={styles.copyrightColumn}>
                    <span style={{ color: '#EA6225', fontWeight: 'bold' }}>&#169;Novadontics, LLC</span>
                  </Col>
                </Col>
              </Row> : null }
          </div>
        </div>
      );
    }

  }

  function handleSubmit(data, dispatch, { form }) {
    const record = data.toJS();
    window.open(process.env.API_URL + '/products/generatePdf/' + parseInt(record['printpdf']) + '/pdf', '_blank');
  }

  function handleExportsubmit(data, dispatch, { form }) {
    const record = data.toJS();
    const secret = localStorage.getItem('secret');
    const CSVRouter = `${process.env.API_URL}/products/generateCSV?X-Auth-Token=${secret && secret.slice(1, -1)}&${convertObjectToParams(record)}`;
    window.open(CSVRouter,`_self`)
  }

  function handleReport (data){
    const { user } = this.props;
    const pdfAuthToken = user && user.secret || user && user.token;
    const date = moment().format('YYYY-MM-DD');
    const { type = 'Patient Report', providerId, implantBrand, loadingProtocol, implantRemoved, format='PDF', fromDate = date, toDate = date } =  data.toJS();
    const route = 'implantData'
    window.open(`${process.env.API_URL}/${route}${format}?X-Auth-Token=${pdfAuthToken}&&fromDate=${fromDate}&&toDate=${toDate}&&providerId=${providerId?providerId:""}&&implantBrand=${implantBrand?implantBrand:""}&&loadingProtocol=${loadingProtocol?loadingProtocol:""}&&implantRemoved=${implantRemoved?implantRemoved:""}`);
  }

  function handleBoneGraftReport (data){
    const { user } = this.props;
    const pdfAuthToken = user && user.secret || user && user.token;
    const date = moment().format('YYYY-MM-DD');
    const { type = 'Patient Report', providerId, implantBrand, loadingProtocol, implantRemoved, format='PDF', fromDate = date, toDate = date } =  data.toJS();
    const route = 'boneGraftData'
    window.open(`${process.env.API_URL}/${route}${format}?X-Auth-Token=${pdfAuthToken}&&fromDate=${fromDate}&&toDate=${toDate}&&providerId=${providerId?providerId:""}`);
  }

  function handleImplantSubmit(data, dispatch, { form }) {
    const record = data.toJS();
    record.recommendedHT = record.recommendedHT != undefined && record.recommendedHT != "" ? moment(record.recommendedHT).format('MM/DD/YYYY') : "";
    if(record.date != undefined) {
      createImplantData(record)
        .then(res => {
          this.implantDataForm.close();
          this.implantDataForm.props.reset();
          this.props.dispatch(actions.loadRecords());
        })
        .catch(() => this.setState({ implantFailureAlert: true }));
    } else {
      this.setState({ implantFailureAlert: true })
    }
  }

  function handleBoneGraftSubmit(data, dispatch, { form }) {
    const record = data.toJS();
    if(record.date != undefined || record.dob != undefined) {
      createBoneGraftData(record)
        .then(res => {
          this.boneGraftDataForm.close();
          this.boneGraftDataForm.props.reset();
          this.props.dispatch(actions.loadRecords());
        })
        .catch(() => this.setState({ implantFailureAlert: true }));
    } else {
      this.setState({ implantFailureAlert: true })
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      dispatch
    };
  }

  return connect(
    createSelector(
      selectLoading(),
      selectRecords(),
      selectError(),
      selectUpdateError(),
      selectHeaders(),
      selectRecordsMetaData(),
      selectUser(),
      selectChildren(),
      selectSearch(),
      selectSearchValue(),
      (loading, records, error, updateError, headers, metaData, user, activeChildren, searchedRecords, searchValue) => ({
        loading,
        records: records ? records.toJS() : [],
        error,
        updateError,
        headers: headers ? headers.toJS() : false,
        metaData: metaData ? metaData.toJS() : false,
        user,
        activeChildren,
        searchedRecords: searchedRecords,
        searchedValue: searchValue
      })
    ),
    mapDispatchToProps
  )(RecordsPage);
}
