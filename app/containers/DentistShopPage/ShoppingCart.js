import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Table } from '@sketchpixy/rubix';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import styles from './style.css';
import {Col, Row, Icon} from "@sketchpixy/rubix/lib/index";
import { Link } from 'react-router';
import {groupBy} from "lodash";
import ConfirmOrder from "./ConfirmOrder";
import VendorInvoice from "./VendorInvoice";
import ModalForm from 'components/ModalRecordForm';

const parseChange = (callback) => e => {
  const { name, checked } = e.target;
  const vendorId = name.split('-').slice(-1)[0];
  callback(vendorId, checked)
};

const VendorOrder = ({products = [], vendor, onRemove, onOvernight, handleEdit, fields, isEditEnabled, orderId}) =>
  <div>
    <Row>
      <Col md={6}>
        {vendor && <h4><b>{vendor.name}</b></h4>}
      </Col>
      <Col md={6}>
        <div className={styles.overnightContainer}>
          <h5 className={styles.overnightLabel}>
            <b>Overnight Shipping</b>
          </h5>
          <input
            type="checkbox"
            name={`overnight-${vendor && vendor.id}`}
            id={`overnight-${vendor && vendor.id}`}
            onChange={parseChange(onOvernight)}
            checked={products[0].overnight || false}
            disabled={!onOvernight}
            style={{
              padding: 0,
              margin: '0px 4px 0px 0px',
              height: 16,
              width: 16,
            }}
          />
        </div>
      </Col>
    </Row>
    <Table>
      <thead>
      <tr>
        <th style={{width: '50%'}}>Item</th>
        <th>Pcs/Pkg</th>
        <th>Price</th>
        <th>Qty.</th>
        <th>Total</th>
        {onRemove && <th/>}
      </tr>
      </thead>
      <tfoot>
      <tr>
        <td>
          <div className={styles.paymentStatus}>
            {isEditEnabled ?
              <VendorInvoice
                initialValues={products[0]}
                vendorId={vendor.id}
                orderId={orderId}
                columns={fields}
                form={`vendorRecord.${products[0].id}`}>
                {(open) => <div style={{color:"#ea6225",cursor:"pointer", fontSize: '14px'}}  onClick={open.bind(this)}> <Icon className={styles.icon} glyph={products[0].paymentType != "" ? "icon-fontello-check" : "icon-fontello-check-empty"} /><span style={{ paddingLeft: '5px' }}>{products[0].paymentType != "" ? "Payment Completed" : "Add Invoice"}</span> </div>}
              </VendorInvoice> : null}
          </div> 
	      </td>
        <td/>
        <td/>
        <td/>
        <td>
          {currencyFormatter.format(products.reduce((acc, p) => acc + (p.amount * p.price), 0))}
        </td>
        <td/>
      </tr>
      </tfoot>
      <tbody>
      {products.map((product) =>
        <tr key={product.id}>
          <td>{product.name}</td>
          <td>
            {product.unit != '' && product.unit != null ?
              product.unit.toLowerCase() == 'each' && product.packageQuantity ? `(${product.packageQuantity} pcs.)` :
              product.unit.toLowerCase() != 'each' && product.packageQuantity ? `( ${product.packageQuantity} pcs. / ${product.unit} )` :
              product.unit.toLowerCase() == 'each' && !product.packageQuantity ? `(1 pcs.)` :
              product.unit.toLowerCase() != 'each' && !product.packageQuantity ? `(1 pcs. / ${product.unit})` : null :
              product.packageQuantity ? `(${product.packageQuantity} pcs.)` : null
            }
          </td>
          <td>{currencyFormatter.format(product.price)}</td>
          <td>{product.amount}</td>
          <td>{currencyFormatter.format(product.amount * product.price)}</td>
          {onRemove && <td><button onClick={() => onRemove(product.id)} className={styles.removeButton}>Remove</button></td>}
        </tr>
      )}
      </tbody>
    </Table>
  </div>;

export const OrderTable = props => {
  const { cart, vendors = [], removeFromCart, onOvernight, handleEdit, fields, isEditEnabled, shippingCharge, tax, creditCardFee, totalAmountSaved, finalTotal, orderId } = props;
  const grandTotal = cart.reduce((acc, p) => acc + (p.amount * p.price), 0);
  const productGroups = groupBy(cart, 'vendorId');
  const overnightCount = Object.keys(productGroups).map(key => productGroups[key][0].overnight).filter(Boolean).length;
  const shippingCost = currencyFormatter.format(overnightCount * 52);
  const orderTotal =  grandTotal  + (overnightCount * 52) + shippingCharge + tax + creditCardFee;
  const savingsTotal = currencyFormatter.format(totalAmountSaved);
  return (
    <div>
      {
        Object.keys(productGroups).map(vendorId =>
          <VendorOrder
            key={vendorId}
            orderId={orderId}
            products={productGroups[vendorId]}
            vendor={vendors.find(_ => String(_.id) === String(vendorId))}
            onRemove={removeFromCart}
            onOvernight={onOvernight}
	          handleEdit={handleEdit}
            fields={fields}
            isEditEnabled={isEditEnabled}	
          />)
      }
      <h4 className={styles.rightAlign}><span>Item(s) Subtotal:</span><b>{` ${currencyFormatter.format(grandTotal)}`}</b></h4>
      <h4 className={styles.rightAlign}><span>Overnight Shipping Cost:</span><b>{` ${shippingCost}`}</b></h4>
      <h4 className={styles.rightAlign}><span>Ground Shipping Cost:</span><b>{` ${currencyFormatter.format(shippingCharge)}`}</b></h4>
      <h4 className={styles.rightAlign}><span>Sales Tax:</span><b>{` ${currencyFormatter.format(tax)}`}</b></h4>
      <h4 className={styles.rightAlign}><span>Processing Fee:</span><b>{` ${currencyFormatter.format(creditCardFee)}`}</b></h4>
      <h4 className={styles.rightAlign}><span>Total Amount Saved:</span>{savingsTotal != "$0.00"?` +`:``}<b>{` ${savingsTotal}`}</b></h4>
      <h4 className={styles.rightAlign}><span>Grand Total:</span><b>{` ${currencyFormatter.format(finalTotal)}`}</b></h4>
    </div>)
};

class ShoppingCart extends React.PureComponent {
  state = {showConfirm: false};

  toggleConfirm = () => {
    this.setState({ showConfirm: !this.state.showConfirm });
  };

  render(){
  const { cart = [], onClose, removeFromCart, placeOrder, shown, vendors = [], onOvernight, orderId} = this.props;

  return (
    <div>
      <ConfirmOrder
        shown={this.state.showConfirm}
        onClose={this.toggleConfirm}
        onConfirm={placeOrder}
      />
      <Modal show={shown} onHide={onClose} bsSize="large">
        <Modal.Header closeButton>
          <Modal.Title>Cart</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {cart && cart.length ?
            <OrderTable
              cart={cart}
              orderId={orderId}
              removeFromCart={removeFromCart}
              onOvernight={onOvernight}
              vendors={vendors}
            />
            :
            <div>
              <p><em>No items in cart.</em></p>
            </div>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onClose}>Close</Button>
          <Button
            type="submit"
            bsStyle="standard"
            disabled={cart.length === 0}
            onClick={this.toggleConfirm}
          >
            Place Order
          </Button>
        </Modal.Footer>
      </Modal>
    </div>)
  }
}

ShoppingCart.propTypes = {
  cart: PropTypes.array,
  onClose: PropTypes.func.isRequired,
  removeFromCart: PropTypes.func.isRequired,
  placeOrder: PropTypes.func.isRequired,
  onOvernight: PropTypes.func.isRequired,
  vendors: PropTypes.array,
  shown: PropTypes.bool,
};

export default ShoppingCart;
