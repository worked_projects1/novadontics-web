import React from "react";
import classNames from 'classnames';
import {mapProps, compose, withState} from "recompose";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {createSelector} from "reselect";
import selectors from "../../blocks/records/selectors";
import { Map, List } from 'immutable';
import {Button, Col, Label, ListGroup, Modal, Row} from "@sketchpixy/rubix/lib/index";
import {get} from '../../utils/misc';
import styles from './style.css';

const AddressItem = ({address = {}, disabled, active, onClick}) =>
  <li className={classNames('list-group-item', disabled && 'disabled', active && 'active', active && styles.noHoverColor)} onClick={()=>onClick(address.id)}>
    <Row>
      <Col md={5}>
        {address.isPractice && <h4><Label>Practice Address</Label></h4>}
        <p>{get(address, 'name')}</p>
        <p>{get(address, 'phone')}</p>
      </Col>
      <Col md={7}>
        <p>{get(address, 'address')}</p>
        <p>{`${get(address, 'city')}, ${get(address, 'state')} ${get(address, 'zip')}`}</p>
        <p>{`${get(address, 'country')}`}</p>
      </Col>
    </Row>
  </li>;

const ConfirmOrder = ({onClose, shown, onConfirm, submitting, setSubmitting, addresses = [], selected, setSelected,}) =>
    <Modal show={shown} backdrop="static" onHide={onClose} backdrop={false}>
      <Modal.Header closeButton>
        <Modal.Title>Choose Shipping Address</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ListGroup componentClass="ul">
          {addresses.map((address, i) => <AddressItem key={i} address={address} active={selected === address.id} onClick={setSelected}/>)}
        </ListGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onClose}>Close</Button>
        <Button
          type="submit"
          bsStyle="standard"
          disabled={submitting}
          className={submitting ? 'buttonLoading' : ''}
          onClick={() => {setSubmitting(true); onConfirm({address: selected})}}
        >
          Place Order
        </Button>
      </Modal.Footer>
    </Modal>;

ConfirmOrder.propTypes = {
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  shown: PropTypes.bool,
};


export default compose(
  connect(
    createSelector(
      selectors('products').selectRecordsMetaData(),
      (meta) => ({ addresses: (meta || Map()).get('shipping', List()).toJS() })
    )
  ),
  mapProps(({addresses, ...rest}) => ({addresses: addresses.sort((l,r) => l.isPractice <= r.isPractice), ...rest})),
  withState('submitting', 'setSubmitting', false),
  withState('selected', 'setSelected', props => (props.addresses.find(_ => _.primary) || {}).id)
)(ConfirmOrder);

