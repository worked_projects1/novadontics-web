import React from 'react';
import { connect } from 'react-redux';
import { Col, Modal, Button, Icon, Alert } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { compose, withProps } from 'recompose';
import { loadVendorInvoice, createVendorInvoice, updateVendorInvoice, deleteVendorInvoice } from 'blocks/orders/invoice';
import ModalForm from 'components/ModalRecordForm';
import ReactTable from "react-table";
import styles from './style.css';
import ConfirmButton from 'components/ConfirmButton';
import { reset } from 'redux-form';
import EditRecordForm from 'components/EditRecordForm';
import Spinner from 'components/Spinner';
import moment from 'moment';
import { currencyFormatter } from 'utils/currencyFormatter.js';

class VendorInvoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, error: false, success: false, records: [] }
    }

    componentDidMount() {
      const { vendorId, orderId } = this.props;
      loadVendorInvoice(orderId, vendorId)
        .then(vendorInvoice => this.setState({ records: vendorInvoice }))
        .catch(() => this.setState({ records: [] }));
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    componentWillReceiveProps(props) {
        if (props.updateError != this.props.updateError && this.state.showModal) {
            setTimeout(() => this.props.clearError(), 3000)
        }
    }

    editableColumnProps = {
        Cell: props => {
            const { column } = props;
            const { columns, editRecord, deleteRecord, vendorId, orderId } = this.props;
            const { error } = this.state;
            const { disableEdit } = props.original;

            const handleEdit = (data, dispatch, { form }) => {
              const { vendorId, orderId } = this.props;
              const submitRecord = data.toJS();
              submitRecord.vendorId = vendorId;
              submitRecord.orderId = orderId;
              submitRecord.amountPaid = parseFloat(submitRecord.amountPaid);
              submitRecord.checkNumber = submitRecord.checkNumber != undefined ? submitRecord.checkNumber : "";
              submitRecord.invoice = submitRecord.invoice != undefined ? submitRecord.invoice : "";

              updateVendorInvoice(submitRecord)
                .then(updateInvoice => {
                  loadVendorInvoice(orderId, vendorId)
                    .then(vendorInvoice => this.setState({ records: vendorInvoice }))
                    .catch(() => this.setState({ records: [] }));
                    this.setState({ success: true })
                    setTimeout(() => this.setState({ success: false }), 3000);
                    this[form].close();
                })
                .catch(() => {
                  this.showError();
                });
            }

            return (column.action && !disableEdit ?
                <Col className={styles.actions}>
                    <ModalForm
                        initialValues={props.original || {}}
                        fields={columns}
                        form={`editVendorInvoice.${props.original.id}`}
                        onSubmit={handleEdit}
                        updateError={error}
                        onRef={editVendorInvoice => (this[`editVendorInvoice.${props.original.id}`] = editVendorInvoice)}
                        config={{ title: "Edit", btnType: 'link', glyphIcon: 'icon-fontello-edit', showError: this.showError.bind(this) }}
                    />
                    <ConfirmButton onConfirm={() => {this.handleDelete(props.original.id)}}>
                        {(click) => <Button bsStyle="link" bsSize="xs" className={styles.btnDelete} onClick={() => click()}>
                            <Icon glyph="icon-fontello-stop" className={styles.icon} />Delete
                        </Button>}
                    </ConfirmButton>
                </Col> : <span>{column.value === 'shareTo' ? metaData[column.oneOf].find(e => e.value === props.value) && metaData[column.oneOf].find(e => e.value === props.value).label || '' : column.type === 'date' && props.value != "" ? moment(props.value).format('MM/DD/YYYY') : column.value === 'amountPaid' ? currencyFormatter.format(props.value) : props.value}</span>);
        }
    };

    render() {
        const { columns, updateError, initialValues, vendorId, orderId } = this.props;
        const { success, error } = this.state;
        var { records } = this.state;
        var totalAmount = records.reduce((accum,item) => accum + item.amountPaid, 0);
        records = records.length > 0 ? [...records, {"checkNumber" : "Total Amount:", "amountPaid" : totalAmount, "disableEdit": true, paymentDate: ""}] : records;

        return <Col>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} backdrop="static" className="vendorInvoice" bsSize="large" onHide={this.close.bind(this)} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <Col className={styles.header}>
                            <h3 className={styles.fontColor}>Vendor Invoice</h3>
                            <ModalForm
                                initialValues={{vendorName: initialValues.manufacturer}}
                                fields={columns}
                                updateError={error}
                                form={`createVendorInvoice`}
                                onSubmit={this.handleCreate.bind(this)}
                                onRef={createVendorInvoice => (this.createVendorInvoice = createVendorInvoice)}
                                config={{ title: "Add New Invoice" }}
                            />
                        </Col>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles.body}>
                    {error ?
                        <Alert danger>{updateError}</Alert> : null}
                    {success ?
                        <Alert success>Successfully submitted</Alert> : null}
                    {records ?
                        <ReactTable
                            columns={columns.filter(a => a.viewRecord).map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                            ))}
                            data={records}
                            getTdProps={(state, rowProps) => {
                                return {
                                  style: rowProps && rowProps.original && rowProps.original.disableEdit ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold' }: {}
                                }
                            }}
                            pageSize={records && records.length > 0 ? records.length : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        /> : <Spinner />}
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false })
    }

    showError(error) {
        this.setState({error: "Please check the details you entered and try again"});
        setTimeout(() => this.setState({error: false}), 3000);
    }

    handleDelete(id) {
      const { vendorId, orderId } = this.props;
      deleteVendorInvoice(id)
        .then(deleteInvoice => {
          loadVendorInvoice(orderId, vendorId)
            .then(vendorInvoice => this.setState({ records: vendorInvoice }))
            .catch(() => this.setState({ records: [] }));
        })
    }

    handleCreate(data, dispatch, { form }) {
        const { vendorId, orderId } = this.props;
        const submitRecord = data.toJS();
        submitRecord.vendorId = vendorId;
        submitRecord.orderId = orderId;
        submitRecord.amountPaid = parseFloat(submitRecord.amountPaid);
        submitRecord.checkNumber = submitRecord.checkNumber != undefined ? submitRecord.checkNumber : "";
        submitRecord.invoice = submitRecord.invoice != undefined ? submitRecord.invoice : "";

        createVendorInvoice(submitRecord)
          .then(newInvoice => {
            loadVendorInvoice(orderId, vendorId)
              .then(vendorInvoice => this.setState({ records: vendorInvoice }))
              .catch(() => this.setState({ records: [] }));
              this.setState({ success: true })
              setTimeout(() => this.setState({ success: false }), 3000);
              this[form].close();
              dispatch(reset(form));
          })
          .catch(() => {
            this.showError();
          });
    }

}


export default VendorInvoice;
