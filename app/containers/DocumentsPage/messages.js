/*
 * RecordsPage Messages
 *
 * This contains all the text for the UsersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  noItems: {
    id: 'app.components.DocumentsPage.noItems',
    defaultMessage: 'There are no records. Create the first record by clicking on button above.',
  },
  loading: {
    id: 'app.components.DocumentsPage.loading',
    defaultMessage: 'Loading…',
  },
  error: {
    id: 'app.components.DocumentsPage.error',
    defaultMessage: 'There was an error loading the resource. Please try again.',
  },
  access: {
    id: 'app.components.DocumentsPage.access',
    defaultMessage: 'You do not have access to this Implant Education. Please contact Administrator.',
  } 
});
