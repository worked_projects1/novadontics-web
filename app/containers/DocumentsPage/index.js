/*
 * DocumentsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';
import { withRouter } from 'react-router';

import Spinner from 'components/Spinner';
import TableWrapper from 'components/TableWrapper';
import ModalForm from 'components/ModalRecordForm';
import { loadAnalytics } from 'blocks/analytics/remotes.js';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import AlertMessage from 'components/Alert';

import styles from './styles.css';

import { Alert, Button, Col, PanelBody, PanelContainer, Row } from '@sketchpixy/rubix';
import CsvImporter from 'components/CsvImporter';
import { Grid } from '@sketchpixy/rubix/lib/index';
import schema from '../../routes/schema';
import { setApiFilter } from 'utils/api';
import { type } from 'os';
import { loadStateTax, updateStateTax } from 'blocks/vendors/stateTax';
import StateTax from 'components/StateTax';
import { Collapse } from 'antd';
import { selectUser } from 'blocks/session/selectors';
import numeral from "numeral";
import Info from 'containers/PatientsPage/components/Info';
import infoSchema from 'routes/schema';

const { Panel } = Collapse;
export default function (name, path, create, columns, actions, selectors) {
    const {
        selectLoading,
        selectRecords,
        selectError,
        selectUpdateError,
        selectHeaders,
        selectRecordsMetaData
    } = selectors;

    class DocumentsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

        static propTypes = {
            records: PropTypes.array,
            children: PropTypes.object,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool
        };

        constructor(props) {
            super(props);
            this.state = {
                editing: false,
                selectedData: false,
                alertMessage: false,
                search: false,
                products: false,
                filterData: [],
                analytics: [],
                updatesearch: ''
            };
        }

        componentDidMount() {
            const { headers = {}, dispatch } = this.props;
            const [status] = columns.filter((s) => s.value === 'status' || s.value === 'accountType');
            headers.status = status && status.oneOf && headers.status == '' ? status.oneOf[0] : headers.status;
            if (dispatch) {
                setApiFilter(Object.keys(headers).filter(_ => _ != "content-length").reduce((r, k) => Object.assign({}, r, { [k]: headers[k] }), {}));
                dispatch(actions.updateHeader({ 'status': headers.status }));
                dispatch(actions.loadRecords());
            }
            loadAnalytics().then(analytics => this.setState({ analytics: analytics.ceRequests })).catch(() => this.setState({ analytics: [] }));
        }

        shouldComponentUpdate(nextProps, nextState) {
            return shallowCompare(this, nextProps, nextState);
        }

        handleChange = e => {
            var data = e.target.value
            this.setState({ updatesearch: e.target.value })
        };        

        render() {
            const { error, updateError, loading, children, location = {}, route = {}, dispatch, headers, metaData, user = {}, params } = this.props;
            const { alertMessage, products, analytics } = this.state;
            const { ciiPrograms = [] } = user;
            // This one is pretty specific for RecordsPage
            let records = this.props.records;
            let displayName = name;

            const handleSearch = e => {
                const searchlist = this.state.updatesearch;
                const updatedList = records.filter(item => {
                    return (
                        (item.category && item.category.toString().toLowerCase().includes(searchlist.toLowerCase())) ||
                        (item.speaker && item.speaker.toString().toLowerCase().includes(searchlist.toLowerCase())) ||
                        (item.id && item.id.toString().toLowerCase().includes(searchlist.toLowerCase())) ||
                        (item.courseId && item.courseId.toString().toLowerCase().includes(searchlist.toLowerCase())) ||
                        (item.title && item.title.toString().toLowerCase().includes(searchlist.toLowerCase()))
                    );
                })
                this.setState({ filterData: updatedList });
                this.props.router.push({
                    pathname: `${path}/1`,
                    state: { searchRecords: updatedList },
                });
            }
            const recordsMetaData = metaData && metaData.reduce((a, el) => {
                const parentRecords = records.filter(_ => _.category === el.name);
                const children = el && el.children && el.children.length > 0 && el.children.reduce((b, child) => {
                    const childRecords = records.filter(_ => _.category === child.name);
                    if(childRecords && childRecords.length > 0){
                        b.push(Object.assign({}, b, child))
                    }
                    return b;
                },[]) || [];
                if(name === 'Implant Education' || ((parentRecords && parentRecords.length > 0) || (children && children.length > 0))){
                    a.push(Object.assign({}, el, {children: children}))
                }
                return a;
            }, []);
            
            if (name === 'documents') {
                displayName = 'Resource Center'
            } else if (name === 'courses') {
                displayName = 'Continuing Education'
            } else if (name === 'ciicourses') {
                displayName = 'Implant Education'
            }

            return (
                <div>
                    <Helmet title="Novadontics" />
                    <div className={styles.view} >
                        {!params.id ? <div style={{ minHeight: `${document.body.clientHeight - 120}px`, background: '#6d6e6f' }}>
                            <Grid className={styles.gridStyle}>
                                <Row>
                                    <Col sm={12} className={styles.flexibleRow}>
                                        <h1 className={displayName === 'Continuing Education' ? styles.titleHead1 : styles.titleHead}>{displayName}</h1>
                                    </Col>
                                </Row>

                                {displayName === 'Implant Education' ?
                                  <Row>
                                    {loading ? <Spinner /> : null}
                                    <Col className={styles.docBox} lg={12} lgOffset={1} md={11} mdOffset={1} collapseLeft collapseRight>
                                      {error ?
                                        <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
                                          recordsMetaData && recordsMetaData.map((data, index) => (
                                            ciiPrograms.includes(data.name) ?
                                              <Link key={index} to={{ pathname: `${path}/${data.id}`, state: location.state }}>
                                                <Col className={styles.box} md={3} sm={4} xs={11}>
                                                    {data.name}
                                                </Col>
                                              </Link> :
                                              <Col key={index} className={styles.box} md={3} sm={4} xs={11} onClick={() => { this.setState({ alertMessage: true }) }}>
                                                  {data.name}
                                              </Col>
                                      ))}
                                        {alertMessage ?
                                           <AlertMessage alert dismiss={()=> {this.setState({ alertMessage: false }); }}><FormattedMessage {...messages.access} /></AlertMessage> : null }
                                    </Col>
                                  </Row> :
                                  displayName === 'Continuing Education' ?
                                  <Row>
                                    <Col lgOffset={1} mdOffset={1} lg={10} md={10} xs={12} className={styles.searchField}>
                                        <label>
                                            <span className={styles.infoCE}>
                                            <Info data={infoSchema().patients().continueEducationInfo}/>
                                            </span>
                                            <input name="ContinueEducation" type="text" value={this.state.updatesearch} placeholder="Search by title,speaker, or code" onChange={(e) => { this.handleChange(e) }} />
                                            <Button bsStyle="standard" className={styles.gobtn} type="button" onClick={handleSearch}>Go</Button>
                                        </label>
                                    </Col>
                                    <Col lgOffset={1} mdOffset={1} lg={10} md={10} style={loading ? { marginBottom: '25px' } : null}>
                                        <Col className={styles.analyticsInfo}>
                                            <Col>
                                                <Col><span>This Month CE</span>{analytics.thisMonth && analytics.thisMonth == null ? '0' : numeral(analytics.thisMonth).format('0,0[.]00')}</Col>
                                            </Col>
                                            <Col>
                                                <Col><span>This Year CE</span>{analytics.thisYear && analytics.thisYear == null ? '0' : numeral(analytics.thisYear).format('0,0[.]00')}</Col>
                                            </Col>
                                            <Col>
                                                <Col><span>Last Year CE</span>{analytics.lastYear && analytics.lastYear == null ? '0' : numeral(analytics.lastYear).format('0,0[.]00')}</Col>
                                            </Col>
                                            <Col>
                                                <Col><span>Up to Date CE</span>{analytics.total && analytics.total == null ? '0' : numeral(analytics.total).format('0,0[.]00')}</Col>&nbsp;
                                            </Col>
                                        </Col>
                                    </Col>
                                    <Col className={styles.boxWidth} lgOffset={1} mdOffset={1} lg={12} md={12} collapseLeft collapseRight>
                                        {error ?
                                          <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
                                          recordsMetaData && recordsMetaData.map((data, index) => (
                                              <Link key={index} to={{ pathname: `${path}/${data.id}`, state: location.state }}>
                                                  <Col className={styles.box} md={3} sm={4} xs={11}>
                                                      {data.name}
                                                  </Col>
                                              </Link>
                                          ))}
                                    </Col>
                                    {loading ? <Spinner /> : null}
                                  </Row> :
                                  <Row>
                                    {loading ? <Spinner /> : null}
                                    <Col className={styles.docBox} lg={12} lgOffset={1} md={11} mdOffset={1} collapseLeft collapseRight>
                                        {error ?
                                          <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
                                          recordsMetaData && recordsMetaData.map((data, index) => (
                                              <Link key={index} to={{ pathname: `${path}/${data.id}`, state: location.state }}>
                                                  <Col className={styles.box} md={3} sm={4} xs={11}>
                                                      {data.name}
                                                  </Col>
                                              </Link>
                                          ))}
                                    </Col>
                                  </Row>
                                  }
                            </Grid></div> : null}
                        {children ?
                            <Row>
                                <Col md={12} lg={12} className={'patient-block'} style={{ marginBottom: 76 }}>
                                    <PanelContainer>
                                        <PanelBody>
                                            <Col sm={12}>
                                                {children}
                                            </Col>
                                        </PanelBody>
                                    </PanelContainer>
                                </Col>
                            </Row> : null}
                    </div>
                </div>
            );
        }

    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    return  withRouter(connect(
        createSelector(
            selectLoading(),
            selectRecords(),
            selectError(),
            selectUpdateError(),
            selectHeaders(),
            selectRecordsMetaData(),
            selectUser(),
            (loading, records, error, updateError, headers, metaData, user) => ({
                loading,
                records: records ? records.toJS() : [],
                error,
                updateError,
                headers: headers ? headers.toJS() : false,
                metaData: metaData ? metaData.toJS() : false,
                user
            })
        ),
        mapDispatchToProps
        )(DocumentsPage));
}
