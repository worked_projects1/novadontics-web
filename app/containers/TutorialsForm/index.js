/*
 * TutorialsForm Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';
import {loadRecords} from '../../blocks/tutorials/remotes';
import {loadManualRecords} from '../../blocks/manuals/remotes';
import ModalForm from 'components/ModalRecordForm';
import styles from './styles.css';
import {  Col, Row, Modal } from '@sketchpixy/rubix';
import { Grid } from '@sketchpixy/rubix/lib/index';
import { Collapse } from 'antd';
import { selectUser } from 'blocks/session/selectors';
import ReactTooltip from 'react-tooltip';

const { Panel } = Collapse;
export default function (name, path, columns, actions, selectors) {
    const {
      selectLoading,
      selectError,
      selectUpdateError,
      selectHeaders,
      selectChildren,
      selectRecordsMetaData
    } = selectors;

    class TutorialsForm extends React.Component { // eslint-disable-line react/prefer-stateless-function

      constructor(props) {
        super(props);
        this.state = {
            records:[]
        };
      }

      componentDidMount() {
        const { dispatch } = this.props;
        const { records } = this.state
        if(name === 'tutorials') {
          const field = loadRecords().then(data => {
          this.setState({ records: _.sortBy(data, 'order') })});
        }
        if(name === 'manuals') {
          const field = loadManualRecords().then(data => {
          this.setState({ records: _.sortBy(data, 'order') })});
        }
      }


      render() {
        const { params } = this.props;
        const { records } = this.state;

        return (
          <div className="insurancePage">
            <Helmet title="Novadontics" />
            <ReactTooltip type="info" html={true} />
              <div className={styles.view} >
                {!params.id ?
                  <Grid>
                    <Row>
                      <Col sm={12} className={styles.flexibleRow}>
                        {name === 'tutorials' ?
                          <span className={styles.titleFee}>
                            Tutorials
                          </span> :
                        name === 'manuals' ?
                          <span className={styles.titleBlue}>
                            Manuals
                          </span> : ""}
                      </Col>
                    </Row>
                    <div style={{ minHeight: `${document.body.clientHeight - 120}px`, backgroundColor: "white", overflowY: "scroll", paddingBottom: '5em' }}>
                      {records.map((field) =>
                        {
                          return field.children.length > 0 ?
                            <Col key={field.id} sm={12} style={{fontSize: "16px", fontWeight: "bold", paddingTop: "3%"}}>
                              <span style={name === 'tutorials' ? {color: "#91288F"} : {color: "#38a0f1"}}> {field.name}</span><br></br>
                              {field.children.map((child) =>
                                <Col key={child.id} sm={12} style={{fontSize: "14px", fontWeight: "bold", paddingTop: "2%"}}>
                                  <a href = {child.link} target="_blank">
                                    <u style={{marginLeft: "3%", color: "#818284"}}>{child.name}</u>
                                  </a>
                                </Col>
                              )}
                            </Col> : null
                        }
                      )}
                    </div>
                  </Grid> : null}
              </div>
          </div>
        );
      }
    }

    function mapDispatchToProps(dispatch) {
      return {
        dispatch
      };
    }

    return connect(
      createSelector(
        selectLoading(),
        selectError(),
        selectUpdateError(),
        selectHeaders(),
        selectChildren(),
        selectRecordsMetaData(),
        selectUser(),
        (loading, error, updateError, headers, activeChildren, metaData, user) => ({
            loading,
            error,
            updateError,
            user
        })
      ),
      mapDispatchToProps
    )(TutorialsForm);
}
