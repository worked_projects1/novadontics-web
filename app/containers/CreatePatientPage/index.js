/*
 * CreatePatientPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import Spinner from 'components/Spinner';
import { selectUser } from 'blocks/session/selectors';
import CreatePatientForm from '../PatientsPage/components/CreatePatientForm';
import Alert from 'components/Alert';

const SUCCESS_ALERT_TIMEOUT = 4000;

export default function (name, path, columns, { selectRecord, selectFormSchema }, createRecord, selectRecords, actions, selectError) {
    
    function handleCreate(data, dispatch, {form}) {
      let submitRecord = data.toJS();
      const { email, phone } = submitRecord;
      const url = new URL(window.location.href);
      const formId = url.searchParams.get("id") ? url.searchParams.get("id") : '1A';
      const stepValues = Object.keys(submitRecord).length > 0 ? 
        Object.keys(submitRecord)
        .filter(key => key)
        .reduce((obj, key) => {
          obj[key] = (typeof(submitRecord[key]) === 'object') ? JSON.stringify(submitRecord[key]).replace(/"/g,'★').replace(/\//g,"\\/") : submitRecord[key];
          return obj;
        }, {}) : {};
      dispatch(createRecord(
        Object.assign({}, 
          {emailAddress:email}, 
          {phoneNumber:phone}, 
          {steps:[{
            formId:formId, 
            formVersion: 1, 
            state: 'Completed', 
            value:stepValues}]}), form));
    };
  
   
  class CreatePatientPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {success: false, showError: false};
    }

    componentDidMount() {
      document.getElementById("app").style.overflow = "hidden"; 
    } 

    componentWillUnmount() {
      document.getElementById("app").style.overflow = "initial"; 
    }

    render() {
      const { record, location = {}, formSchema, error, user = {} } = this.props;
      const { success, showError } = this.state;
      let metaData = {};

      if (formSchema.length === 0) {
        return <Spinner />;
      }

      return (
        <div>
          <CreatePatientForm
            name={name}
            path={path}
            initialValues={{carrierName: "-1"}}
            formSchema={formSchema}
            metaData={record.metaData}
            onSubmit={handleCreate}
            onSubmitSuccess={this.onSuccess.bind(this)}
            locationState={location.state}
            platform={user && user.platform}
            record={record}
          />
          {success ?
            <Alert success>Your data has been saved successfully!</Alert> : null}
          {error && showError?
            <Alert warning dismiss={()=>this.setState({showError:false})}>{error}</Alert> : null}
        </div>
      );
    }
    onSuccess() {
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ showError: true });
      }, 500);
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ success: false });
      }, SUCCESS_ALERT_TIMEOUT);
    }
  }

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const updateError = selectError()(state, props);
    const selectedRecords = selectRecords && selectRecords()(state, props);
    const formSchemaRecord = selectFormSchema('patients')(state, props);
    const user = selectUser()(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : {},
      error: updateError,
      records: selectedRecords ? selectedRecords.toJS() : [],
      formSchema: formSchemaRecord ? formSchemaRecord.toJS() : [],
      user
    };
  }

  CreatePatientPage.propTypes = {
    record: PropTypes.object,
    error: PropTypes.string,
    dispatch: PropTypes.func,
    formSchema: PropTypes.array,
  };

  return connect(mapStateToProps)(CreatePatientPage);
}
