/**
*
* Payment Details
*
*/

import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import styles from './styles.css';
import ReactTable from "react-table";
import { loadOrderInvoice } from 'blocks/orders/invoice';

class PaymentDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, data: [] };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        const { columns } = this.props;
        const { showModal, data } = this.state;
        return (
            <Col>
                <Col className={styles.paymentDetails} onClick={this.open.bind(this)}>Payment Details</Col>
                <Modal show={showModal} className={"PaymentHistory"} bsSize="large" backdrop="static" onHide={this.close.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Payment Details
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className={styles.body}>
                        <ReactTable
                            columns={
                                columns.map((column) => Object.assign(
                                    {},
                                    { Header: column.label, accessor: column.value, ...column }
                                ))}
                            data={data}
                            pageSize={data && data.length > 0 ? 8 : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        />
                    </Modal.Body>
                </Modal>
            </Col>
        );
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
        this.loadOrderInvoiceDetails();
    }

    loadOrderInvoiceDetails() {
      const { orderId } = this.props;
      loadOrderInvoice(orderId)
        .then(orderInvoice => this.setState({ data: orderInvoice }))
        .catch(() => this.setState({ data: [] }));
    }

}

export default PaymentDetails;
