import CSVExportService from 'json2csvexporter';
import {groupBy} from "lodash";
const exporter = CSVExportService.create({
  columns: ['serialNumber', 'manufacturer', 'name', 'amount', 'price', 'totalPrice', 'overnightCount', 'shippingCharge', 'tax', 'creditCardFee', 'totalAmountSaved','orderTotal'],
  headers: {
    serialNumber: 'Reference Number',
    manufacturer: 'Vendor',
    name: 'Product Name',
    amount: 'Quantity',
    price: 'Unit Price',
    totalPrice: 'PRODUCT Total',
    overnightCount: 'Overnight Shipping Cost',
    shippingCharge: 'Ground Shipping Cost',
    tax: 'SALES Tax',
    creditCardFee: 'Processing Fee',
    totalAmountSaved: 'Total Amount Saved',
    orderTotal: 'Grand Total'
  },
  includeHeaders: true,
});

export function downloadOrder(products, shippingCharge, tax, creditCardFee, totalAmountSaved) {
  try {
    const grandTotal = products.reduce((acc, p) => acc + (p.amount * p.price), 0);
    const productGroups = groupBy(products, 'vendorId');
    const overnightCount = Object.keys(productGroups).map(key => productGroups[key][0].overnight).filter(Boolean).length;
    const orderTotal =  grandTotal  + (overnightCount * 52) + shippingCharge + tax + creditCardFee;
    const orders = products.map((product, i) => (i == 0) ? Object.assign({}, product, { totalPrice: product.price * product.amount }, {overnightCount : overnightCount * 52 }, {shippingCharge : shippingCharge.toFixed(2)}, {tax:tax.toFixed(2)}, {creditCardFee:creditCardFee.toFixed(2)}, {totalAmountSaved : totalAmountSaved.toFixed(2)}, {orderTotal : orderTotal.toFixed(2)}) : Object.assign({}, product, { totalPrice: product.price * product.amount }));
    exporter.downloadCSV(orders);
  } catch (err) {
    console.error('Error downloading CSV: \n', err);
  }
}
