/*
 * ViewOrderPage Messages
 *
 * This contains all the text for the UsersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  noItems: {
    id: 'app.components.ViewOrderPage.noItems',
    defaultMessage: 'There are no orders to show right now.',
  },
  loading: {
    id: 'app.components.ViewOrderPage.loading',
    defaultMessage: 'Loading…',
  },
  error: {
    id: 'app.components.ViewOrderPage.error',
    defaultMessage: 'There was an error loading the selected order(s). Please try again.',
  },
});
