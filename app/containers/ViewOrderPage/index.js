/*
 * ViewOrderPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import moment from 'moment-timezone';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import { selectUser } from 'blocks/session/selectors';

import { Alert, Row, Col, Panel, Button, PanelBody, isBrowser, PanelHeader, PanelContainer } from '@sketchpixy/rubix';

import EditRecordForm from 'components/EditRecordForm';
import ModalForm from 'components/ModalRecordForm';

import { downloadOrder } from './utils.js';
import styles from './styles.css';
import {OrderTable} from "../DentistShopPage/ShoppingCart";
import {get} from "../../utils/misc";
import {Grid} from "@sketchpixy/rubix/lib/index";
import ViewPaymentDetails from './components/ViewPaymentDetails';
import groupBy from 'utils/groupBy';


const ERROR_ALERT_TIMEOUT = 6000;

export default function (path, columns, products, updateRecord, selectRecord, isEditEnabled) {
  
  const {
    ordersColumns,
    productColumns,
    productNotesColumns,
    vendorColumns,
    paymentColumns
  }  = columns;


  class ViewOrderPage extends React.Component {
    static propTypes = {
      record: PropTypes.object,
      user: PropTypes.object,
    };

    static print() {
      if (isBrowser()) {
        window.print();
      }
    }

    constructor(props) {
      super(props);
      this.state = { error: false,errormsg: false }	
      this.print = ViewOrderPage.print.bind(this);
    }

    render() {
      const { record = {}, user = {} } = this.props;
      const { name, dateCreated, approvedBy="Test", dateFulfilled, items = [], dentist = {}, shippingCharge, tax, creditCardFee, totalAmountSaved, grandTotal, note } = record || {};
      const initialValues = ordersColumns.reduce((acc, el) => {
        acc[el.value] = record[el.value];
        return acc;
      }, {});
      const {address} = record;
      initialValues['dateFulfilled'] = moment().format('LLL');
      initialValues['approvedBy'] = user && user.name;
      const { error,errormsg } = this.state;
      
      const showError = (msg) => { 
          setTimeout(() => {
          	this.setState({ error: true , errormsg : msg });
          }, 500);
          setTimeout(() => {
         	 this.setState({ error: false, errormsg: false });
          }, ERROR_ALERT_TIMEOUT);  
      }

      const GroupedVendor = record.items && record.items.length > 0 && groupBy('vendorId')(record.items) || [];
      const paymentDetails =  GroupedVendor && Object.keys(GroupedVendor).length > 0 && Object.keys(GroupedVendor).reduce((a, el) => {
        if(GroupedVendor[el] && GroupedVendor[el].length > 0 && GroupedVendor[el][0] && GroupedVendor[el][0].paymentType){
          a.push(GroupedVendor[el][0])
        }
        return a;
      }, []);


      return (
        <div className={styles.viewRecordPage}>
          <Row>
            <Col xs={12}>
              <PanelContainer>
                <Panel>
                  <PanelHeader>
                    <Row>
                      <Col sm={12}>
                        <div className="closeDetailViewButton">
                          <Link to={`${path}`}>Close</Link>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={9}>
                        <h2 className={styles.headTitle}>Order #{record.id}</h2>
                        {user.role != 'dentist' && <ViewPaymentDetails orderId={record.id} columns={paymentColumns} /> || null}
                      </Col>
                      <Col sm={3} className={styles.right}>
                        {isEditEnabled ?
                          <EditRecordForm
                            name={name}
                            initialValues={initialValues}
                            fields={ordersColumns}
                            form={`editRecord.${record.id}`}
                            onSubmit={handleEdit.bind(this)}
                            inline
                          /> :
                          <p>
                            {initialValues && initialValues.status ? initialValues.status : ''}
                          </p>
                        }
                      </Col>
		                  <Col sm={12} className={styles.right}>
                        {error ?
                      		<Alert danger>{errormsg}</Alert> : null}
                      </Col>
                      <Col xs={6} className={styles.headerData}>
                        <span>{moment(dateCreated).format('LLL')}</span>
                      </Col>
                      <Col xs={6} className={styles.paymentData}>
                        {isEditEnabled ?
                          <ModalForm
                            initialValues={record}
                            fields={productColumns}
                            form={`customerRecord.${record.id}`}
                            onSubmit={handleEdit.bind(this)}
                            config={{
                              record:record, btnType:"link", title:record.paymentCompleted == true ? "Customer Payment Completed" : "Mark As Paid", glyphIcon:record.paymentCompleted == true ? "icon-fontello-check" : "icon-fontello-check-empty"
                            }}
                          /> : '' }
                      </Col>
                      <Col xs={6} className={styles.headerData}></Col>
                      <Col xs={6} className={styles.paymentData}>
                        {isEditEnabled ?
                          <ModalForm
                            initialValues={record}
                            fields={productNotesColumns}
                            form={`customerNotes.${record.id}`}
                            onSubmit={handleEdit.bind(this)}
                            config={{
                              record:record, btnType:"link", title: "Notes", glyphIcon: "icon-fontello-docs"
                            }}
                          /> : '' }
                      </Col>
                    </Row>
                    <Row className={styles.info}>
                      <Col xs={6}>
                        <b>{dentist.name}</b>
                        <p>{dentist.clinic}</p>
                        <p>{dentist.location}</p>
                      </Col>

                      <Col xs={6} className={styles.right}>
                        <p>{dentist.phone}</p>
                        <p>{dentist.email}</p>
                      </Col>
                    </Row>
                    {address && <div>
                      <Row>
                        <Col xs={12}>
                        <b>Shipping Address</b>
                        </Col>
                      </Row>
                      <Row className={styles.info}>
                        <Col xs={5}>
                          <p>{get(address, 'name')}</p>
                          <p>{get(address, 'phone')}</p>
                        </Col>
                        <Col xs={7} className={styles.right}>
                          <p>{get(address, 'address')}</p>
                          <p>{`${get(address, 'city')}, ${get(address, 'state')} ${get(address, 'zip')}`}</p>
                          <p>{`${get(address, 'country')}`}</p>
                        </Col>
                      </Row>
                    </div>}
                  </PanelHeader>
                  <hr className="hidden-print" />
                  <PanelBody>
                    <Row>
                      <Col xs={12}>
                        <OrderTable
                          cart={items}
                          orderId={record.id}
                          vendors={record.vendors || []}
			                    handleEdit={handleEdit.bind(this)}
                          fields={vendorColumns}
                          isEditEnabled = {isEditEnabled}
                          shippingCharge={shippingCharge}
                          tax={tax}
                          creditCardFee={creditCardFee}
                          totalAmountSaved={totalAmountSaved}
                          finalTotal={grandTotal}
                        />
                      </Col>
                      <Col xs={12}>
                        <hr className="hidden-print" />
                      </Col>
                      <Col xs={12} className="hidden-print">
                        <Button outlined lg bsStyle="link" className={styles.printButton} onClick={downloadOrder.bind(null, items, shippingCharge, tax, creditCardFee, totalAmountSaved)}>Download as CSV</Button>
                        <Button outlined lg bsStyle="standard" onClick={this.print} className={styles.printButton}>Print Order</Button>
                      </Col>
                    </Row>
                  </PanelBody>
                  { note != undefined && note != "" ?
                    <p style={{ whiteSpace: "pre-wrap" }}>
                      <b>Note: </b> {note}
                    </p>
                      : null }
                  { dateFulfilled ?
                    <p>
                      Approved by <em> {approvedBy} </em> on <b>{dateFulfilled}</b>
                    </p>
                      : null }
                </Panel>
              </PanelContainer>
            </Col>
          </Row>
        </div>
      );

		  function handleEdit(data, dispatch, { form }) {
        const submitrecord = data.toJS();

        if(form === `customerNotes.${submitrecord.id}`) {
          submitrecord.notesForm = true
        }

        if(submitrecord.status != undefined && submitrecord.status == 'Closed') { submitrecord.status = "Void" }
        const { record = {} } = this.props;
        if(submitrecord && submitrecord.paymentCompleted != undefined) {
          submitrecord.paymentCompleted = submitrecord.paymentCompleted == "" ? false : submitrecord.paymentCompleted;
        }
        if(form.indexOf('editRecord') > -1 && record.paymentCompleted == false && (submitrecord.status == 'Fulfilled' || submitrecord.status == 'Void')){
          showError("Mark Customer as Paid!");
          return false;  
        } else if(form.indexOf('editRecord') > -1 && !record.allVendorsPaid && submitrecord.status == 'Void') {
          showError("Vendor Payment Not completed!");
          return false;
        }
        if(record.status === "Received" && submitrecord.status === "Void"){ 
          showError("Only FulFilled orders can be Closed!");
          return false;
        }
        dispatch(updateRecord(submitrecord));
		  }
    }
  }

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const user = selectUser()(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : {},
      user,
    };
  }

  return connect(mapStateToProps)(ViewOrderPage);
}
