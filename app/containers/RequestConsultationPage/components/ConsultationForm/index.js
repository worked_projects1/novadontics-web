/**
*
* RequestConsultationForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm } from 'redux-form/immutable';
import { Col, Row, Button, Alert } from '@sketchpixy/rubix';
import {ImplementationFor} from '../../../PatientsPage/components/CreatePatientForm/utils';
import styles from './styles.css';

class ConsultationForm extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }


  render() {
    const { fields, handleSubmit, error, pristine, submitting } = this.props;
    return (
        <Col>
          <form onSubmit={handleSubmit}>	
              <Row className={styles.rowStandard}>
                {fields.map((field, i) => {
                  const Component = ImplementationFor[field.type];
                  return (
                    <div key={i} className="hidden-print">
                      <Component
                         data={field}
                         options={field.formFieldDecorationOptions}
                         optionsMode="edit"
                      />
                    </div>
                  );
                })}
                  <Button bsStyle='standard' type="submit" disabled={pristine || submitting} className={submitting ? `${styles.submitbtn} buttonLoading` : styles.submitbtn}>
                    Submit Request
                  </Button>
              </Row>
          </form>	
          {error ?
            <Alert danger>{error}</Alert> : null}
      </Col>
    );
  }


}


export default reduxForm({
  form: 'requestConsultation',
})(ConsultationForm);
