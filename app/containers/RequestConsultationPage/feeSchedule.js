/**
*
* FeeSchedule
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Image } from '@sketchpixy/rubix';


class FeeSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { url, show, hide } = this.props;
     return (
        <Modal show={show} onHide={hide} className="clinicalNotes">
            <Modal.Header closeButton></Modal.Header>
            <Modal.Body>
              <Image src={url} responsive />
            </Modal.Body>
        </Modal>
    );
  }

}

FeeSchedule.propTypes = {
  url: PropTypes.string,
  show: PropTypes.bool,
  hide: PropTypes.func,
};

export default FeeSchedule;
