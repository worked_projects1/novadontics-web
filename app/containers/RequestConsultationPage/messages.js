/*
 * RecordsPage Messages
 *
 * This contains all the text for the UsersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.components.RequestConsultationPage.title',
    defaultMessage: 'In-Office Support Request',
  },
  loading: {
    id: 'app.components.RequestConsultationPage.loading',
    defaultMessage: 'Loading…',
  },
  success: {
    id: 'app.components.RequestConsultationPage.success',
    defaultMessage: 'Your request for in-office support has been successfully sent. Novadontics will be contacting you shortly.',
  },
  error: {
    id: 'app.components.RequestConsultationPage.error',
    defaultMessage: 'There was an error loading the resource. Please try again.',
  },
  request: {
    id: 'app.components.RequestConsultationPage.request',
    defaultMessage: 'Please select an request to submit',
  },
});
