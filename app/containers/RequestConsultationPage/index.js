/*
 * RecordsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';
import ConsultationForm from './components/ConsultationForm'
import { Grid, Col, Row, Button, Alert as Error } from '@sketchpixy/rubix';
import styles from './styles.css';
import FeeSchedule from './feeSchedule';
import Spinner from 'components/Spinner';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Alert from 'components/Alert';
import { browserHistory, Link } from 'react-router';

export default function (name, path, columns, consultations, emailMappings) {
    const {
        selectors = {}
    } = emailMappings;
    const {
        selectLoading,
        selectRecords,
        selectError,
        selectUpdateError,
        selectRecordsMetaData
    } = selectors;

    function handleSubmit(data, dispatch, { form }) {
        const record = data.toJS();
        const submitRecord = {};
        const { actions = {} } = consultations;

        /**
         * creating a input data from consulatation submit record
         */
        submitRecord.data = { comment: record.comment && record.comment.notes ? record.comment.notes : '' };
        submitRecord.majorType = "inOfficeSupport";
        const consultant = this.props.records && this.props.records.filter(r => r.isConsultation && record[r.name] && record[r.name]['checked']).map(c => record[c.name]['selectedOption'] ? parseInt(record[c.name]['selectedOption']) : null)[0];
        submitRecord.consultantId = consultant != undefined ? consultant : null;
        submitRecord.data.consultations = this.props.records && this.props.records.filter(r => r.isConsultation && record[r.name] && record[r.name]['checked']).map(c => Object.assign({}, {
            id: c.id,
            consultantName: c.users.filter(r => r.id === parseInt(record[c.name]['selectedOption'])).map(r => r.name)[0],
            patient: record.patient,
            requested: true,
            consultantId: record[c.name]['selectedOption'] ? parseInt(record[c.name]['selectedOption']) : null,
            type: c.name
        }));
        if(submitRecord.data.consultations.length != 0) {
          dispatch(actions.createRecord(submitRecord, form));
          this.setState({ success: true });
        } else {
          this.setState({ failed: true })
        }

    }

    class RecordsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function


        static propTypes = {
            records: PropTypes.array,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool
        };

        constructor(props) {
            super(props);
            this.state = {
                success: false,
                failed: false,
                feeSchedule: false
            }
        }

        componentDidMount() {
            const { actions = {} } = emailMappings;
            this.props.dispatch(actions.loadRecords());

            document.getElementById('body').setAttribute('style', 'margin-top: 75px !important');
            document.getElementById('app').style.background = '#fff';
            document.getElementById('body').style.background = '#fff';
            document.getElementById('body').style.padding = '0px';
        }

        shouldComponentUpdate(nextProps, nextState) {
            return shallowCompare(this, nextProps, nextState);
        }

        componentWillUnmount() {
            document.getElementById('app').style.background = '#e7e7e8';
            document.getElementById('body').style.background = '#e7e7e8';
            document.getElementById('body').style.paddingTop = '25px';
        }

        render() {
            const { records, loading, error } = this.props;
            const { feeSchedule } = this.state;

            if (!records || loading) {
                return <Spinner />;
            }

            /**
           * Setting a option for request consultation columns
           */
            const requestConsultationsColumn = columns;
            {/*.map(col => {
                const filteredCons = records && records.find(r => r.isConsultation && r.name === col.name);
                if (filteredCons && filteredCons.users && filteredCons.users[0] && Object.keys(filteredCons.users[0]).length > 0) {
                    col['attributes'] = { modalOptions: true, options: filteredCons.users.map(u => { return Object.assign({}, { title: u.name, value: u.id }) }) };
                }
                return col;
            });*/}


            return (
                <div>
                    <Helmet title="Novadontics" />
                    <Grid>
                        <Row style={{ marginLeft: '0.5em' }}>
                            {error ? <Error danger><span><FormattedMessage {...messages.error} /></span></Error> :
                            <Row className={styles.flexible}>
                              <Row>
                                <Row>
                                  <Col sm={12} style={{ paddingLeft: 0 }}>
                                    <div className="closeDetailViewButton">
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col sm={12} className={styles.flexibleRow}>
                                      <div>
                                        <h1 className={styles.titleHead}><FormattedMessage {...messages.title} /></h1>
                                      </div>
                                      {name == 'requestConsultation' ?
                                        <div>
                                          <Link onClick={() => browserHistory.push(`/inOfficeSupport`)}>
                                            <Button bsStyle="standard" style={{ marginRight: '15px' }}>
                                              In-office Support History
                                            </Button>
                                          </Link>
                                          <Button bsStyle="standard" onClick={() => this.setState({ feeSchedule: true })} className='btn-standard-blue btn btn-default'>In-office Support Fee Schedule</Button>
                                          <FeeSchedule url={'https://novadontics-uploads.s3.amazonaws.com/inoffice-fee-schedule.png'} show={feeSchedule} hide={() => this.setState({ feeSchedule: false })} />
                                        </div> :
                                      null}
                                  </Col>
                                </Row>
                              </Row>
                              <Row>
                                <Col>
                                  <div>
                                      <ConsultationForm name={name} path={path} fields={requestConsultationsColumn} onSubmit={handleSubmit.bind(this)} />
                                  </div>
                                </Col>
                              </Row>
                            </Row>}
                        </Row>
                        {this.state.failed ?
                            <Alert error dismiss={()=>this.setState({ failed: false })}><FormattedMessage {...messages.request} /></Alert> : null }
                        {this.state.success ?
                            <Alert success dismiss={()=>browserHistory.push(`/inOfficeSupport`)}><FormattedMessage {...messages.success} /></Alert> : null }
                    </Grid>
                </div>
            );
        }

    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    return connect(
        createSelector(
            selectLoading(),
            selectRecords(),
            selectError(),
            selectUpdateError(),
            selectRecordsMetaData(),
            (loading, records, error, updateError, headers, metaData) => ({
                loading,
                records: records ? records.toJS() : false,
                error,
                updateError,
                metaData: metaData ? metaData.toJS() : false
            })
        ),
        mapDispatchToProps
    )(RecordsPage);
}
