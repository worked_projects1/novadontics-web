/*
 * ViewCatalogPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import DeleteRecordForm from 'components/DeleteRecordForm';
import Spinner from 'components/Spinner';
import { Panel, PanelContainer, Row, Col } from '@sketchpixy/rubix';

import { currencyFormatter } from 'utils/currencyFormatter.js';
import styles from './styles.css';
import Gallery from 'components/Gallery';
 
export default function (path, deleteRecord, selectRecord) {
  function handleDelete(data, dispatch, { form }) {
    const { id } = data.toJS();
    dispatch(deleteRecord(id, form));
  }

  const ViewCatalogPage = ({ record }) => <div className={styles.viewCatalogPage}>
      <PanelContainer>
        {record.didNotInit ?
          <Spinner /> :
          <Panel>
            <Row>
              <Col sm={12}>
                <div className="closeDetailViewButton">
                  <Link to={`${path}`}>Close</Link>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm={9}>
                <h3>{record.name}</h3>
              </Col>
              <Col sm={3}>
                <div className={styles.actions}>
                  <Link to={`${path}/${record.id}/edit`}>Edit</Link>
                  <DeleteRecordForm initialValues={{ id: record.id }} form={`deleteRecord.${record.id}`} onSubmit={handleDelete} />
                </div>
              </Col>
            </Row>
            <div className={styles.space}>
              {record.description && record.description.length > 0 ? record.description.split('\n').map((line, i) => <span key={i}> {line} <br /></span>) : ''}
            </div>
            <Row>
              <Col xs={4}>
                <Gallery data={{ images: record.imageUrl && record.imageUrl.split(',') || [] }} singleImage={true} width="100%" />
              </Col>
              <Col xs={8}>
                <strong>Reference Number</strong>
                <div>{record.serialNumber}</div>
                <hr />
                <strong>Vendor</strong>
                <div>{record.manufacturer}</div>
                <hr />
                <strong>Novadontics Price</strong>
                <div>{currencyFormatter.format(record.listPrice || 0)}</div>
                <hr />
                <strong>Retail Price</strong>
                <div>{currencyFormatter.format(record.price || 0)}</div>
              </Col>
            </Row>
          </Panel>}
      </PanelContainer>
        </div>;

  ViewCatalogPage.prototype.propTypes = {
    record: PropTypes.object,
  };

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : { didNotInit: true },
    };
  }

  return connect(mapStateToProps)(ViewCatalogPage);
}
