/*
 * ViewRecordPage Messages
 *
 * This contains all the text for the UsersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  noItems: {
    id: 'app.components.ViewRecordPage.noItems',
    defaultMessage: 'There are no records. Create the first record by clicking on button above.',
  },
  loading: {
    id: 'app.components.ViewRecordPage.loading',
    defaultMessage: 'Loading…',
  },
  error: {
    id: 'app.components.ViewRecordPage.error',
    defaultMessage: 'There was an error loading the requested record. Please try again.',
  },
});
