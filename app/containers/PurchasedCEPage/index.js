/*
 * PurchasedCEPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';

import { Row, Grid, Col } from '@sketchpixy/rubix';
import styles from './styles.css';

import { selectUser } from 'blocks/session/selectors';
import { Collapse } from 'antd';
import moment from 'moment-timezone';
import ModalForm from 'components/ModalRecordForm';
import { convertObjectToParams } from 'utils/tools';
import { setApiFilter } from 'utils/api';
import AlertMessage from 'components/Alert';
import EditRecordForm from 'components/EditRecordForm';
import InlineForm from 'containers/LedgerPage/components/InlineRecordForm';
import Spinner from 'components/Spinner';
import ReactTable from "react-table";
import ReactTooltip from 'react-tooltip';
import ShowText from 'components/ShowText';
import library from '../../routes/schema_1';
import { currencyFormatter } from 'utils/currencyFormatter.js';


const { Panel } = Collapse;
export default function (name, path, columns, actions, selectors) {
    const { selectHeaders, selectRecords, selectRecordsMetaData, selectLoading } = selectors;

    class PurchasedCEPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

        static propTypes = {
            records: PropTypes.array,
            children: PropTypes.object,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool
        };

        constructor(props) {
          super(props);
          this.state = { submitRecord: false, alertFailure: false, alertText: '' };
        }

        componentDidMount() {
          const { dispatch } = this.props;
          if (dispatch) {
            dispatch(actions.loadRecords());
          }
        }

        handleFormSubmit(data, dispatch, { form }) {
          const submitRecord = data.toJS();
          if(submitRecord.fromDate == undefined || submitRecord.fromDate == '' || submitRecord.fromDate == null) {
            this.setState({ alertFailure: true, alertText: 'Please select Start date' })
          } else if(submitRecord.toDate == undefined || submitRecord.toDate == '' || submitRecord.toDate == null) {
            this.setState({ alertFailure: true, alertText: 'Please select End date' })
          } else {
            dispatch(actions.loadRecordsParams(submitRecord, true));
          }
        }

        editableColumnProps = {
          Cell: props => {
            const { column } = props;
            return (
              column.label == 'Description' ?
                  <span style={{ display: 'flex' }}>
                    {column.limit && props.value && props.value.length > column.limit ?
                       props.value.substring(0, column.limit) + '...' : props.value}
                    {column.limit && props.value && props.value.length > column.limit ?
                        <ShowText title={column.label} text={props.value}>{(open) => <div className={styles.viewIcon} onClick={open.bind(this)} />}</ShowText> : null}
                  </span> : <span>{props.value ? column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : column.currency == true ? currencyFormatter.format(props.value) : props.value : ''}</span>
            );
          }
        }

        render() {
          const { user = {}, metaData = {}, headers, records, loading } = this.props;
          const { alertFailure, alertText } = this.state;
          const tableColumns = columns.filter((column) => column.visible);
          const { secret } = user;
          const costArr = records && records.length > 0 ? records.map(item => item.cost) : [];
          const totalCost = costArr.reduce((a, b) => a + b, 0);
          return (<div>
            <Helmet title="Novadontics" />
            <ReactTooltip type="info" html={true} />
            <div>
              <Grid className={styles.horizontalPadding}>
                <div className={styles.backgroundStyle} style={{ minHeight: `${document.body.clientHeight - 120}px` }}>
                  <Row className={styles.formColumn}>
                    <Col sm={12}>
                      <span className={styles.titleFee}>
                        Purchased CE Courses
                      </span>
                    </Col>
                  </Row>
                  <div>
                    <InlineForm
                      fields={library().purchasedCE().ceFilter}
                      form={name}
                      disableCancel={true}
                      leftAlign={true}
                      btnName={"Done"}
                      onSubmit={this.handleFormSubmit.bind(this)}
                    />
                    <Row className={styles.costColumn}>
                      <Col sm={12}>
                        <span className={styles.costFee}>
                          Total Amount: {currencyFormatter.format(totalCost)}
                        </span>
                      </Col>
                    </Row>
                    <Row className={loading ? styles.tableSpinner : styles.table}>
                      {loading ? <Spinner /> :
                        <Col className="CEPurchase">
                          <ReactTable
                            columns={tableColumns.map((column) => Object.assign(
                              {},
                              { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                            ))}
                            data={records}
                            getTdProps={(state, rowProps) => {
                              return {
                                style: rowProps && rowProps.original.totalColumn ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold'} : {}
                              }
                            }}
                            pageSize={records && records.length > 25 ? 25 : records.length}
                            showPageJump={false}
                            resizable={false}
                            sortable={false}
                            showPageSizeOptions={false}
                            showPagination={true}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={""}
                          />
                        </Col>}
                        {alertFailure ?
                          <AlertMessage warning dismiss={() => this.setState({ alertFailure: false })}>{alertText}</AlertMessage> : null}
                    </Row>
                  </div>
                </div>
              </Grid>
            </div>
          </div>);
        }


    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    return connect(
        createSelector(
            selectUser(),
            selectRecords(),
            selectRecordsMetaData(),
            selectHeaders(),
            selectLoading(),
            (user, records, metaData, headers, loading) => ({
                user,
                records: records ? records.toJS() : [],
                metaData: metaData ? metaData.toJS() : {},
                headers: headers ? headers.toJS() : false,
                loading: loading })
        ), mapDispatchToProps)(PurchasedCEPage);
}
