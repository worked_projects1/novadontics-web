/*
 *
 * ResetPasswordPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Alert } from '@sketchpixy/rubix';
import { Link } from 'react-router';

import AnonymousRouteWrapper from 'components/AnonymousRouteWrapper';
import CredentialsForm from 'components/CredentialsForm';
import { resetPassword, verifyResetToken } from 'blocks/session/actions';
import { selectError, selectSuccess } from 'blocks/session/selectors';

export class ResetPasswordPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    error: PropTypes.object,
    success: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    const { token } = props.params;
    this.state = { token };
    props.dispatch(verifyResetToken(token));
  }

  render() {
    const { error = {}, success = {} } = this.props;
    const { resetPassword: resetPasswordError, verifyResetRequest: verifyResetRequestError } = error;
    const { resetPassword: resetPasswordSuccess } = success;
    return (
      <AnonymousRouteWrapper>
        <div>
          {verifyResetRequestError ?
            null :
            <div className="anonymous-form-wrapper">
              <CredentialsForm type="reset" submitLabel="Reset password" onSubmit={this.handleSubmit} />
            </div>}
          {resetPasswordError ?
            <Alert danger>
              <p>{resetPasswordError}</p>
            </Alert> : null}

          {verifyResetRequestError ?
            <Alert danger>
              <p>{verifyResetRequestError}</p>
            </Alert> : null}

          {resetPasswordSuccess ?
            <Alert success>
              <p>Password reset was successful. {resetPasswordSuccess === 'user' ? <Link to="/">You can login now.</Link> : 'Head over to the App and login with your new password.'}</p>
            </Alert> : null}
        </div>
      </AnonymousRouteWrapper>
    );
  }

  handleSubmit(data, dispatch, { form }) {
    const { token } = this.state;
    const { secret } = data.toJS();
    dispatch(resetPassword(secret, token, form));
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  createSelector(
    selectError(),
    selectSuccess(),
    (error, success) => ({ error, success }),
  ),
  mapDispatchToProps,
)(ResetPasswordPage);
