/**
*
* DocumentCenter
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { Col, Modal, Button, Icon, Alert } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { compose, withProps } from 'recompose';
import ModalForm from 'components/ModalRecordForm';
import ReactTable from "react-table";
import styles from './styles.css';
import ConfirmButton from 'components/ConfirmButton';
import { reset } from 'redux-form';
import Spinner from 'components/Spinner';
import EditRecordForm from 'components/EditRecordForm';
import { loadContracts, updateContracts } from 'blocks/patients/contracts';
import moment from 'moment';

class DocumentCenter extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, success: false, pageLoader: false, docType: "All" }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    componentWillReceiveProps(props) {
        if (props.updateError != this.props.updateError && this.state.showModal) {
            setTimeout(() => this.props.clearError(), 3000)
        }

        if(props.records && Array.isArray(props.records) && props.records.length > 0 && this.props.records && Array.isArray(this.props.records) && this.props.records.length > 0 && JSON.stringify(props.records) != JSON.stringify(this.props.records)){
            this.setState({ pageLoader: true });
            setTimeout(() => this.setState({ pageLoader: false}), 3000);
        }
    }

    render() {
        const { title, fields, records, patientId, loadRecord, updateRecord } = this.props;
        const { success, docType } = this.state;
        let fullRecord = records && records.length > 0 ? records.map(_ => _.documents) : [];
        let joinedRecords = fullRecord && fullRecord.length > 0 ? fullRecord.join() : "";

        const updateType = (val) => {
          this.setState({ docType: val })
        }

        return <Col>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} bsSize="large" backdrop="static" onHide={this.close.bind(this)} onEnter={() => loadRecord(patientId)} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <Col className={styles.header}>
                            <h3 className={styles.fontColor}>{title}</h3>
                        </Col>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles.body}>
                  <EditRecordForm
                    fields={fields}
                    initialValues = {{patientId: patientId, documentType: docType, documents: docType == "All" ? joinedRecords : docType != "All" && records.length > 0 && records.find(_ => _.documentType == docType) != undefined ? records.find(_ => _.documentType == docType).documents : ""}}
                    form={`DocumentCenterForm`}
                    onRef={contractForm => (this.contractForm = contractForm)}
                    updateContracts={(data) => { updateRecord(data); }}
                    hideActionButton={true}
                    documentUpdate={true}
                    onCancel={() => this.close()}
                    updateType={(val) => updateType(val)}
                  />
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false, docType: "All" })
    }

}


export default compose(
    connect(),
    withProps(({ dispatch }) => ({
        loadRecord: (id) => dispatch(loadContracts(id)),
        updateRecord: (record) => dispatch(updateContracts(record)),
    }))
)(DocumentCenter);

