import React from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import { getTokenApteryx, loadApteryx } from 'blocks/patients/apteryx';
import styles from './styles.css';

class Apteryx extends React.Component {

    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }

    componentDidMount(){
        this.props.getTokenAction({
            "patientId": this.props.patientId,
            "grant_type":"password",
            "username":"admin",
            "password":"Nova2020*",
            "remember_me":true,
        });
    }

    render() {
        const { title, loadAction, record = {} } = this.props;
        const { access_token } = record;
        return <Col>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} className="clinicalNotes" bsSize="large" backdrop="static" onHide={this.close.bind(this)} >
                <Modal.Body>
                <iframe src={`https://novadontics.xvweb.net/#interactive?patient=1&token=${access_token}`} className={styles.iframe}></iframe>
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({showModal : false});
    }
}


export default compose(
    connect(),
    withProps(({ record, dispatch, error }) => ({
        loadAction: (record) => dispatch(loadApteryx(record)),
        getTokenAction: (record) => dispatch(getTokenApteryx(record)),
        record: record,
        error: error
    }))
)(Apteryx);