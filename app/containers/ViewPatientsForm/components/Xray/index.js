/**
 * 
 * Xray
 * 
 */

import React from 'react';
import { Col, Modal } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import styles from './styles.css';
import Spinner from 'components/Spinner';
import messages from 'containers/RecordsPage/messages';
import { FormattedMessage } from 'react-intl';
import AlertMessage from 'components/Alert';

class Xray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, loading: false,showAlert: false };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    handleFrame() {
        this.setState({loading: true});
        const iframe = document.getElementById('iframe-link');
        if(iframe){
            iframe.onload = () => {
                iframe.style.display = 'block';
                this.setState({loading: false})
            }

            iframe.src = this.props.url;
        }
    }

    render() {

        return <Col className={styles.Xray}>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) }, () => { this.setState({ showAlert: !this.state.showAlert }) })}
            {this.state.showAlert ? 
                <AlertMessage warning dismiss={()=> this.setState({ showAlert: false })}><FormattedMessage {...messages.xray} /></AlertMessage> : null}
            <Modal show={this.state.showModal} className="XrayModal" bsSize="large" onHide={this.close.bind(this)} onEnter={this.handleFrame.bind(this)} >
                <Modal.Body>
                    <Col>
                        <iframe id="iframe-link" className={styles.iframe} />
                    </Col>
                </Modal.Body>
                { this.state.loading ? <Spinner /> : null}
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false });
    }

}

export default Xray;

