/**
 * 
 * TreatmentPlan
 * 
 */

import React from 'react';
import { Col, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { compose, withProps } from 'recompose';
import { connect } from 'react-redux';
import Spinner from 'components/Spinner';
import styles from './styles.css';
import ReactTable from "react-table";
import { loadTreatmentPlan, createTreatmentPlan, updateTreatmentPlan, deleteTreatmentPlan } from 'blocks/patients/treatmentPlan';
import ConfirmButton from 'components/ConfirmButton';
import moment from 'moment';
import TreatmentPlanDetails from '../TreatmentPlanDetails';
import { reset } from 'redux-form';
import { loadTreatmentPlanDetails, createTreatmentPlanDetails, updateTreatmentPlanDetails, deleteTreatmentPlanDetails, createAppointmentDetails } from '../../../../blocks/patients/treatmentPlanDetails';
import ModalForm from 'components/ModalRecordForm';
import AlertMessage from 'components/Alert';

class TreatmentPlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = { editing: false, selectedData: false, selected: false, selectedProps: false, alertMessage: false };
    }

    handleSubmit(data, dispatch, { form }) {
        const { createRecord, resetForm } = this.props;
        createRecord(data.toJS());
        this.treatmentForm.close();
        resetForm(form);
    }

    handleItem(data, dispatch, { form }) {
        const { createItem, editItem, resetForm, patientId } = this.props;
        let submitRecord = data.toJS();
        const { id } = submitRecord;

        submitRecord.cdcCode = document.getElementsByName('cdcCode')[0].value;
        submitRecord.procedureName = document.getElementsByName('procedureName')[0].value;
        submitRecord.fee = document.getElementsByName('fee')[0].value;
        submitRecord.fee = parseInt(submitRecord.fee);
        submitRecord.insurance = document.getElementsByName('insurance')[0].value;
        submitRecord.insurance = parseInt(submitRecord.insurance);
        submitRecord.toothNumber = submitRecord.toothNumber != undefined && submitRecord.toothNumber != '' ? submitRecord.toothNumber.toString() : '';
        var paidTotal = submitRecord.insurancePaidAmt + submitRecord.patientPaidAmt;

        if(paidTotal > submitRecord.fee && JSON.stringify(this.state.selectedData) != JSON.stringify(submitRecord)) {
          this.setState({ alertMessage: true });
        } else {
          if (id && JSON.stringify(this.state.selectedData) != JSON.stringify(submitRecord)) {
              editItem(Object.assign({}, submitRecord, { patientId: patientId }));
              this.setState({ editing: false, selectedData: false });
          } else if (!this.state.selectedData) {
              createItem(Object.assign({}, submitRecord, { patientId: patientId }));
              resetForm(form);
          }
        }
    }

    componentDidMount() {
        const { loadItem, patientId } = this.props;
        loadItem(patientId)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    editableColumnProps = {
        Cell: props => {
            const { column, tdProps } = props;
            const { onEdit, onDelete } = tdProps.rest;
            return (column.action ?
                <Col className={styles.actions}>
                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onEdit}><Icon glyph="icon-fontello-edit" className={styles.icon} />Open</Button>
                    <ConfirmButton onConfirm={onDelete}>{(click) => <Button disabled={this.props.disable} bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => click()}><Icon glyph="icon-fontello-trash" className={styles.icon} />Delete</Button>}</ConfirmButton>
                </Col> : <span>{column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>);
        }
    };

    render() {
        const { title, records = [], patientId, schema, deleteRecord, loadItem, user, chartNumber, patientName, disable, handleClose, phone, email, medicalCondition, referredBy } = this.props;
        const columns = schema.TreatmentPlan;

        return <Col className={styles.TreatmentPlan}>
            {/*<Col className={styles.header}>
                <span style={{ color: '#EA6225' }}> {title} </span>
                <Col className={styles.headerModal}>
                    {!disable ? <ModalForm
                        initialValues={{ patientId: patientId }}
                        fields={columns}
                        form={`createRecord`}
                        onSubmit={this.handleSubmit.bind(this)}
                        onRef={treatmentForm => (this.treatmentForm = treatmentForm)}
                        config={{ title: "Add Treatment Plan" }}
                    /> : null}
                    <Icon glyph="icon-fontello-cancel" className={styles.closeIcon} onClick={handleClose} />
                </Col>
            </Col>
            <h5>Name: {patientName}</h5>
            <h5>Chart Number: {chartNumber}</h5>
            <Col>
                {records ?
                    <ReactTable
                        columns={columns.filter(a => a.viewRecord).map((column) => Object.assign(
                            {},
                            { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                        ))}
                        data={records}
                        getTdProps={(state, rowProps) => {
                            return {
                                onEdit: () => { this.setState({ selected: rowProps.original.id, selectedProps: rowProps }); loadItem({ id: rowProps.original.id, patientId: patientId }) },
                                onDelete: () => deleteRecord({ id: rowProps && rowProps.original.id, patientId: patientId })
                            }
                        }}
                        pageSize={records && records.length > 0 ? 8 : 0}
                        showPageJump={false}
                        resizable={false}
                        showPageSizeOptions={false}
                        previousText={"Back"}
                        pageText={""}
                        noDataText={"No entries to show."}
                    /> : <Spinner />}
            </Col>*/}
                <TreatmentPlanDetails
                    initialValues={this.state.selectedData || {}}
                    form={`editTreatmentPlanDetailRecord`}
                    onSubmit={this.handleItem.bind(this)}
                    user={user}
                    chartNumber={chartNumber}
                    patientName={patientName}
                    medicalCondition={medicalCondition}
                    referredBy={referredBy}
                    phone={phone}
                    email={email}
                    onEditItem={(data) => this.setState({ editing: data && data.original.id, selectedData: data && data.original })}
                    onCancelItem={() => this.setState({ editing: false, selectedData: false })}
                    onCancel={() => this.setState({ selected: false, selectedProps: false })}
                    {...this.state}
                    {...this.state.selectedProps}
                    {...this.props} />
                {this.state.alertMessage ?
                    <AlertMessage warning dismiss={() => this.setState({ alertMessage: false })}>Total paid amount must be less than Fee.</AlertMessage> : null}
        </Col>
    }

}

export default compose(
    connect(),
    withProps(({ dispatch }) => ({
        loadRecord: (id) => dispatch(loadTreatmentPlan(id)),
        createRecord: (record) => dispatch(createTreatmentPlan(record)),
        editRecord: (record) => dispatch(updateTreatmentPlan(record)),
        deleteRecord: (record) => dispatch(deleteTreatmentPlan(record)),
        loadItem: (record) => dispatch(loadTreatmentPlanDetails(record)),
        createItem: (record) => dispatch(createTreatmentPlanDetails(record)),
        createAppointment: (record) => dispatch(createAppointmentDetails(record)),
        editItem: (record) => dispatch(updateTreatmentPlanDetails(record)),
        deleteItem: (record) => dispatch(deleteTreatmentPlanDetails(record)),
        resetForm: (form) => dispatch(reset(form))
    }))
)(TreatmentPlan);

