/**
*
* PrescriptionHistory
*
*/

import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { compose, withProps } from 'recompose';
import { connect } from 'react-redux';
import Spinner from 'components/Spinner';
import styles from './styles.css';
import { loadPrescriptionHistory, createPrescriptionHistory, updatePrescriptionHistory, deletePrescriptionHistory } from 'blocks/prescriptions/prescriptionHistory';
import ReactTable from "react-table";
import PrescriptionForm from "../PrescriptionForm";
import ConfirmButton from 'components/ConfirmButton';
import schema from '../../../../routes/schema';
import moment from 'moment';
import axios from 'axios';

const columns = schema().patients().prescriptionHistory;

class PrescriptionHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, editing: false, selectedData: false, data: false };
        this.handlePrescribeAgain = this.handlePrescribeAgain.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    editableColumnProps = {
      Cell: props => {
          const { dispatch, updateRecord, disable } = this.props;
          const { column, tdProps, original = {} } = props;
          const { id } = original;
          const { editing, selectedData = {} } = this.state

          const handleUpdate = (e) => {
            if (e.key == 'Enter') {
                const submitRecord = Object.assign({}, { 'id': id, 'patientId': this.props.id, 'note': (e && e.target && e.target.value || document.getElementsByName(`${column.value}_${id}`)[0].value) });
                updateRecord(submitRecord);
                this.setState({ editing: false });
            }
          }

          return this.state.editing === `${column.value}_${id}` && column.editRecord ?
              <div>
                  <textarea name={`${column.value}_${id}`} type="input" className={styles.input} defaultValue={props.value} onKeyDown={handleUpdate} />
                  <div className={styles.icons}>
                      <Icon glyph="icon-fontello-ok-circled" className={styles.ok} onClick={() => handleUpdate({ key: 'Enter' })} />
                      <Icon glyph="icon-fontello-cancel-circled" onClick={() => this.setState({ editing: false, selectedData: false })} />
                  </div>
              </div> :
              column.editRecord ?
                <div className={styles.editIcon}>
                  <textarea type="input" className={styles.textArea} value={props.value} disabled />
                  <Icon glyph="icon-fontello-edit" className={styles.edit} onClick={() => { !disable ? this.setState({ editing: `${column.value}_${id}`, selectedData: original }) : null }} />
                </div>:
              <span className={styles.field} data-place={"bottom"} data-tip={column.label} >
                  {props.value}
              </span>;
      }
  }

    render() {

        const { title, records, id, loadRecord, createRecord, updateRecord, deleteRecord, user, patient, profileList, practice } = this.props;
        const pdfAuthToken = user && user.secret || user && user.token;
        const data = records ? records.reduce((item, data) => { item.push(Object.assign({}, data, { dateCreated: moment(data.dateCreated).format('MM/DD/YYYY'), prescription: `${data.medication} ${data.dosage}, ${data.count} count`, prescribedBy: data.staffName })); return item; }, []) : [];
        return (
            <Col>
                <Col className={styles.history} onClick={this.open.bind(this)}>Prescriptions History</Col>
                <Modal show={this.state.showModal} backdrop="static" className={"PaymentHistory"} bsSize="large" onHide={this.close.bind(this)} onEnter={() => loadRecord(id)}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <Col className={styles.header}>
                                <h3>{title}</h3>
                              {pdfAuthToken ?
                                <a href={`${process.env.API_URL}/prescriptionHistory/Pdf?X-Auth-Token=${pdfAuthToken}&patientId=${id}`} target="_blank" data-rel="external">
                                  <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Treatment Plan Details PDF"} alt="pdf" className={styles.img} />
                                </a>  : null
                              }
                            </Col>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className={styles.body}>
                        {data ? 
                        <ReactTable
                            columns={columns.map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, ...column, ...this.editableColumnProps }
                            )).concat({
                                Header: '',
                                accessor: '',
                                sortable: false,
                                Cell: props => this.ActionsCell(props) // Custom cell components!
                            })}
                            data={data}
                            getTdProps={(state, rowProps) => {
                                return {
                                    onDelete: () => deleteRecord({ id: rowProps && rowProps.original.id, patientId: id })
                                }
                            }}
                            pageSize={data && data.length > 0 ? 8 : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        /> : <Spinner />}
                    </Modal.Body>
                </Modal>
            </Col>
        );
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true })
    }

    ActionsCell(props) {
        const { tdProps, original = {} } = props;
        const { disable } = this.props;
        const { onCreate, onDelete } = tdProps.rest;
        const { deleted } = original;
        var prescriptionValues = {
            name: `${original.medication} ${original.dosage}`,
            count: original.count,
            instruction: original.instructions,
            refills: original.refills,
            id: original.prescriptionId
        }

        return deleted ?
            <Col className={styles.archived}>
                Archived    
            </Col> :
            disable ?
            <Col>
              <Button bsStyle="link" bsSize="xs" className={styles.btn}><Icon glyph="icon-fontello-cw" className={styles.icon} />Prescribe Again</Button>
              <Button bsStyle="link" bsSize="xs" className={styles.btn}><Icon glyph="icon-fontello-archive" className={styles.icon} />Archive</Button>
            </Col> :
            <Col>
            <PrescriptionForm filteredProvider={this.props.filteredProvider}  createRecord={this.props.createRecord} patient={this.props.patient} user={this.props.user} practice={this.props.practice} profileList={this.props.profileList} patientId={this.props.id} prescriptionValues={prescriptionValues}>{(click) => <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => click()}><Icon glyph="icon-fontello-cw" className={styles.icon} />Prescribe Again</Button>}</PrescriptionForm>
                <ConfirmButton onConfirm={onDelete}>{(click) => <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => click()}><Icon glyph="icon-fontello-archive" className={styles.icon} />Archive</Button>}</ConfirmButton>
        </Col>;
    }

    handlePrescribeAgain(record) {
        const { id, createRecord } = this.props;
        const { prescriptionId } = record;

        createRecord(record);  
        axios(`${process.env.API_URL}/prescription/${prescriptionId}/pdf?treatment=${id}`, {
            method: 'GET',
            responseType: 'blob' //Force to receive data in a Blob Format
          }).then((response) => {
            const file = new Blob(
              [response.data],
              { type: 'application/pdf' });
            const fileURL = URL.createObjectURL(file);
            window.open(fileURL);
          });

    }
}


export default compose(
    connect(),
    withProps(({ dispatch }) => ({
        loadRecord: (id) => dispatch(loadPrescriptionHistory(id)),
        createRecord: (record) => dispatch(createPrescriptionHistory(record)),
        updateRecord: (record) => dispatch(updatePrescriptionHistory(record)),
        deleteRecord: (record) => dispatch(deletePrescriptionHistory(record))
    }))
)(PrescriptionHistory);
