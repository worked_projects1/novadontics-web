/**
*
* Provider
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '@sketchpixy/rubix';
import styles from './styles.css';
import ModalForm from './ModalForm';
import ToothImage from './ToothImage';
import EditRecordForm from 'components/EditRecordForm';
import { SelectableGroup, createSelectable } from 'react-selectable';

const SelectableAlbum = createSelectable(ToothImage);

class Provider extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        selectedItems: [],
        tolerance: 0,
        selectOnMouseMove: false,
      }
      this.get = this.get.bind(this);
      this.handleSelection = this.handleSelection.bind(this);
      this.clearItems = this.clearItems.bind(this);
      this.handleToleranceChange = this.handleToleranceChange.bind(this);
      this.toggleSelectOnMouseMove = this.toggleSelectOnMouseMove.bind(this);
    }

    render() {
        const { record = {}, provider = {}, view, mode, numberSystem, handleToothSelect, selectedTooth } = this.props;
        const { patientId, columns, metaData, planColumns } = provider;
        return (
            <Row>
              <Col id="ToothChart" sm={12} style={{ display: 'flex', marginRight: '1em' }}>
                <SelectableGroup
                  className="main"
                  ref="selectable"
                  onSelection={this.handleSelection}
                  tolerance={this.state.tolerance}
                  selectOnMouseMove={this.state.selectOnMouseMove}>
                    {view.map((tooth, index)=> {
                      const selected = selectedTooth.indexOf(tooth.id) > -1;
                      return (
                        <SelectableAlbum
                          selectableKey={tooth.id}
                          key={tooth.id}
                          selected={selected}
                          form={`toothChart.${tooth.id}`}
                          toothId={tooth.id}
                          selectedTooth={selectedTooth}
                          handleSelect={this.handleSelect.bind(this)}
                          config={{
                            title: 'Tooth',
                            btnType:"image",
                            imageIcon: this.get(tooth.id, 'Icon'),
                            topView: mode == 'top' ? null : `<span>${tooth[numberSystem]}</span>`,
                            bottomView: mode == 'bottom' ? null : `<span>${tooth[numberSystem]}</span>`,
                            maxilla: record && record.edentulousMaxilla,
                            mandible: record && record.edentulousMandible,
                            mode: mode,
                          }}
                        />
                      );
                    })}
				          </SelectableGroup>
              </Col>
            </Row>
        );
    }


    get(id, type) {
        const { record, mode } = this.props;
        let missingTooth = record && record.missingTooth && record.missingTooth.split(',');
        let primaryTooth = record && record.primaryTooth && record.primaryTooth.split(',');
        const MT = missingTooth && missingTooth.includes(id);
        const DT = primaryTooth && primaryTooth.includes(id);
        const maxilla = record && record.edentulousMaxilla;
        const mandible = record && record.edentulousMandible;
        const allPrimary = record && record.allPrimary;
        let sideView = "";
        let topView = "";
        if(allPrimary) {
          sideView = require(`img/babytoothchart/${mode === 'top' ? `side-view${maxilla || MT ? `-empty` : DT ? `-baby` : ``}` : `top-view${mandible || MT ? `-empty`: DT ? `-baby` : ``}`}${id}.png`);
          topView = require(`img/babytoothchart/${mode === 'bottom' ? `side-view${mandible || MT ? `-empty` : DT? `-baby` : ``}` : `top-view${maxilla || MT ? `-empty`: DT ? `-baby` : ``}`}${id}.png`);
        } else {
          sideView = require(`img/toothchart/${mode === 'top' ? `side-view${maxilla || MT ? `-empty` : DT ? `-baby` : ``}` : `top-view${mandible || MT ? `-empty`: DT ? `-baby` : ``}`}${id}.png`);
          topView = require(`img/toothchart/${mode === 'bottom' ? `side-view${mandible || MT ? `-empty` : DT? `-baby` : ``}` : `top-view${maxilla || MT ? `-empty`: DT ? `-baby` : ``}`}${id}.png`);
        }
        const sideViewHeight = mode == 'top' ? '120px' : '50px';
        const topViewHeight = mode == 'top' ? '50px' : '120px';
        if(type === 'Icon'){
            return [{"url": sideView, "id": id, "width": '35px', "height": sideViewHeight}, {"url": topView, "id": id, "width": '35px', "height": topViewHeight}]
        } else {
            return record;
        }
    }

    handleSelect(id) {
      const { handleToothSelect, mode } = this.props;
      handleToothSelect(id, mode);
    }

    handleSelection (keys) {
      const { handleToothArr, mode } = this.props;
      handleToothArr(keys, mode);
      this.setState({ selectedItems: keys });
    }


    clearItems (e) {
      if(!isNodeInRoot(e.target, this.refs.selectable)) {
        this.setState({
          selectedItems: []
        });
      }
    }


    handleToleranceChange (e) {
      this.setState({
        tolerance: e.target.value
      });
    }

    toggleSelectOnMouseMove () {
      this.setState({
        selectOnMouseMove: !this.state.selectOnMouseMove
      });
    }
}


Provider.propTypes = {
    provider: PropTypes.object,
    records: PropTypes.array,
    view: PropTypes.array,
    handleSubmit: PropTypes.func,
    numberSystem: PropTypes.string
};

export default Provider;

