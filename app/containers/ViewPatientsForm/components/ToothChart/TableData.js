/**
*
* TableData
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col,Table } from '@sketchpixy/rubix';
import { stringToObjectConverter } from 'utils/tools';
import styles from './styles.css';


class TableData extends React.Component {
    constructor(props) {
        super(props); 
    }

    render() {
        const {  records, view, numberSystem } = this.props;
        return (
            <Col className={styles.TableData}>
                <Table className={styles.Table} condensed responsive>
                <thead>
                    <tr>
                        {view.map((tooth,index)=><th key={index}>{tooth[numberSystem]}</th>)}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {view.map((tooth,index)=>{
                        const tc = records && records.filter(_=>_.toothId===tooth.id)[0] && records.filter(_=>_.toothId===tooth.id)[0].toothConditions.split(',') || false;
                        return <td key={index}>{tc && tc.length > 0 && tc.map((c,i) => { 
                            const j = stringToObjectConverter(c || '', '#');
                            return (<div key={i} style={{color: j['TS'] === 'P' ? '#FF0000' : j['TS'] === 'C' ? '#008000' : 'black',fontWeight:'bold'}}>{j.key}</div>) }) || ''}</td>
                        })}
                    </tr>
                </tbody>
                </Table>
            </Col>
        );
    }
}


TableData.propTypes = {
    records: PropTypes.array,
    view: PropTypes.array,
    numberSystem: PropTypes.string
};

export default TableData;

