/**
*
* Tooth Chart
*
*/

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Col, Row, Modal, Icon, Tab, Tabs, PanelContainer, PanelBody } from '@sketchpixy/rubix';
import { compose, withProps } from 'recompose';
import shallowCompare from 'react-addons-shallow-compare';
import styles from './styles.css';
import { loadToothChart, createToothChart, setMissingTooth, unsetMissingTooth, setPrimaryTooth, unsetPrimaryTooth, setEdentulousMaxilla, unsetEdentulousMaxilla, setEdentulousMandible, unsetEdentulousMandible, setAllPrimary, unsetAllPrimary } from 'blocks/patients/toothchart';
import { loadTreatmentPlanDetails } from 'blocks/patients/treatmentPlanDetails';
import { default as TopView, default as BottomView } from './Provider';
import { default as TopTable, default as BottomTable } from './TableData';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import InlineMultiCheckBoxForm from './InlineMultiCheckBox';
import EditRecordForm from 'components/EditRecordForm';
import ReactTooltip from 'react-tooltip';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/Alert';
import PerioInfo from '../PerioChart/PerioInfo.js';

const NumberSystem = ({ onChange }) => <select className={styles.Select} onChange={onChange}><option value="AM">AMERICAN</option><option value="IN">INTERNATIONAL</option></select>

const ToothTopView = Array.from(Array(8), (x, index) => Object.assign({}, { id: (index + 1).toString(), AM: index + 1, IN: 18 - index })).concat(Array.from(Array(8), (x, index) => Object.assign({}, { id: (index + 9).toString(), AM: index + 9, IN: 21 + index })));

const ToothBottomView = Array.from(Array(8), (x, index) => Object.assign({}, { id: (32 - index).toString(), AM: 32 - index, IN: 48 - index })).concat(Array.from(Array(8), (x, index) => Object.assign({}, { id: (24 - index).toString(), AM: 24 - index, IN: 31 + index })));

const PrimaryTopView = Array.from(Array(5), (x, index) => Object.assign({}, { id: (index + 10).toString(36).toUpperCase(), AM: (index + 10).toString(36).toUpperCase(), IN: 55 - index })).concat(Array.from(Array(5), (x, index) => Object.assign({}, { id: (index + 15).toString(36).toUpperCase(), AM: (index + 15).toString(36).toUpperCase(), IN: 61 + index })));

const PrimaryBottomView = Array.from(Array(5), (x, index) => Object.assign({}, { id: (29 - index).toString(36).toUpperCase(), AM: (29 - index).toString(36).toUpperCase(), IN: 85 - index })).concat(Array.from(Array(5), (x, index) => Object.assign({}, { id: (24 - index).toString(36).toUpperCase(), AM: (24 - index).toString(36).toUpperCase(), IN: 71 + index })));

class ToothChart extends React.Component {
  constructor(props) {
    super(props);
    const { metaData } = props;
    const { procedureName = [], procedureCodeCategory = [] } = metaData;
    this.state = { showModal: false, numberSystem: 'AM', activeId: procedureCodeCategory && procedureCodeCategory.length > 0 ? procedureCodeCategory.find(_=> _.name == 'Diagnostic CDT Codes').id : 0, selectedTopToothArr: [], selectedBottomToothArr: [], filteredCode: procedureName.length > 0 ? procedureName.filter(_ => _.category == 'Diagnostic CDT Codes') : [], showPrimaryInfo: false, showPermanentInfo: false, showSpinner: false, showLoading: false, warningAlert: false, warningText: '' };
    this.handleProcedureCode = this.handleProcedureCode.bind(this);
    this.handleExProcedureCode = this.handleExProcedureCode.bind(this);
  }

  componentDidMount() {
    this.props.loadAction(this.props.patientId)
  }

  handleToothSelect(id, mode) {
    const { selectedTopToothArr, selectedBottomToothArr } = this.state;
    if(mode == 'top') {
      if(selectedTopToothArr.includes(id)) {
        const unRemovedTooth = selectedTopToothArr.filter(a => a != id);
        this.setState({ selectedTopToothArr: unRemovedTooth });
      } else {
        const selected = [...selectedTopToothArr, id];
        this.setState({ selectedTopToothArr: selected });
      }
    } else {
      if(selectedBottomToothArr.includes(id)) {
        const unRemovedTooth = selectedBottomToothArr.filter(a => a != id);
        this.setState({ selectedBottomToothArr: unRemovedTooth });
      } else {
        const selected = [...selectedBottomToothArr, id];
        this.setState({ selectedBottomToothArr: selected });
      }
    }
  }

  handleToothArr(arr, mode) {
    const { selectedTopToothArr, selectedBottomToothArr } = this.state;
    if(mode == 'top') {
      const selected = [...new Set([...selectedTopToothArr ,...arr])];
      this.setState({ selectedTopToothArr: selected });
    } else {
      const selected = [...new Set([...selectedBottomToothArr ,...arr])];
      this.setState({ selectedBottomToothArr: selected });
    }
  }

  handleProcedureCode(id) {
    const { metaData } = this.props;
    const { procedureName = [] } = metaData;
    const filteredRecords = procedureName.length > 0 ? procedureName.filter(_ => _.categoryId == id) : [];
    this.setState({ filteredCode: filteredRecords, activeId: id });
  }

  handleExProcedureCode(id) {
    const { metaData } = this.props;
    const { procedureName = [] } = metaData;
    const filteredRecords = procedureName.length > 0 ? procedureName.filter(_ => _.categoryId == id) : [];
    this.setState({ filteredCode: filteredRecords, activeId: id });
  }

  handleSubmit(code, status) {
    const { metaData, patientId } = this.props;
    const { procedureCodeCategory = [], procedureName = [] } = metaData;
    const { activeId, selectedTopToothArr, selectedBottomToothArr, warningAlert, warningText } = this.state;
    this.setState({ showSpinner: true });
    var submitData = this.props.planColumns && this.props.planColumns.filter(_ => _.editRecord).reduce((a, el) => Object.assign({}, a, { [el.value]: el.type === "date" ? document.getElementById(el.value).value : document.getElementsByName(el.value)[0].value }), {});
    submitData.cdtCode = code;
    submitData.procedureName = procedureName && procedureName.find(_ => _.procedureCode === code).procedureType;
    submitData.status = status;
    submitData.patientId = patientId;
    let selectedToothArr = selectedTopToothArr.concat(selectedBottomToothArr);
    submitData.toothId = selectedToothArr.join(',');
    submitData.service = procedureCodeCategory && procedureCodeCategory.find(_=> _.id === activeId).name;
    submitData.providerId = submitData.providerId && parseInt(submitData.providerId);
    submitData.datePlanned = submitData.datePlanned && moment(submitData.datePlanned).format('YYYY-MM-DD');
    if(submitData.toothId.length == 0 && submitData.service != "Diagnostic CDT Codes" && submitData.service != "Preventive CDT Codes") {
      this.setState({ warningAlert: true, warningText: "Please select at least one tooth", showSpinner: false });
    } else if(submitData.providerId == "") {
      this.setState({ warningAlert: true, warningText: "Please select Provider", showSpinner: false });
    } else if(submitData.datePlanned == "") {
      this.setState({ warningAlert: true, warningText: "Please select Date", showSpinner: false });
    } else {
      if(submitData.toothId.length == 0 && (submitData.service == "Diagnostic CDT Codes" || submitData.service == "Preventive CDT Codes")) {
        delete submitData.toothId;
      }
      createToothChart(submitData)
        .then(response => {
          this.props.loadTreatmentPlanAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
  }

  handleSave(data, status) {
    const { metaData, patientId } = this.props;
    const { procedureCodeCategory = [], procedureName = [] } = metaData;
    const { activeId, selectedTopToothArr, selectedBottomToothArr, warningAlert, warningText } = this.state;
    var submitData = data.toJS();
    if(status === 'Existing') {
      submitData.cdtCode = document.getElementsByName('codeNumber')[0].value;
      submitData.procedureName = document.getElementsByName('codeDescription')[0].value;
    }
    if(status === 'Proposed') {
      submitData.cdtCode = document.getElementsByName('procedureCode')[0].value;
      submitData.procedureName = document.getElementsByName('procedureDescription')[0].value;
    }

    if(submitData.cdtCode != undefined && submitData.cdtCode != '' && submitData.cdtCode != null) {
      if(submitData.procedureName === undefined || submitData.procedureName === '' || submitData.procedureName === null) {
        submitData.procedureName = procedureName && procedureName.find(_ => _.procedureCode === submitData.cdtCode) && procedureName.find(_ => _.procedureCode === submitData.cdtCode).procedureType;
      }
      submitData.service = procedureName && procedureName.find(_ => _.procedureCode === submitData.cdtCode) && procedureName.find(_ => _.procedureCode === submitData.cdtCode).category;
      submitData.status = status;
      submitData.patientId = patientId;
      let selectedToothArr = selectedTopToothArr.concat(selectedBottomToothArr);
      submitData.toothId = selectedToothArr.join(',');
      submitData.providerId = submitData.providerId && parseInt(submitData.providerId);
      submitData.datePlanned = submitData.datePlanned && moment(submitData.datePlanned).format('YYYY-MM-DD') || moment().format('YYYY-MM-DD');
      if(selectedToothArr.length == 0 && submitData.service != "Diagnostic CDT Codes" && submitData.service != "Preventive CDT Codes") {
        this.setState({ warningAlert: true, warningText: "Please select at least one tooth", showLoading: false });
      } else if(submitData.providerId == "") {
        this.setState({ warningAlert: true, warningText: "Please select Provider", showLoading: false });
      } else if(submitData.datePlanned == "") {
        this.setState({ warningAlert: true, warningText: "Please select Date", showLoading: false });
      } else {
        this.setState({ showLoading: true });
        if(submitData.toothId.length == 0 && (submitData.service == "Diagnostic CDT Codes" || submitData.service == "Preventive CDT Codes")) {
          delete submitData.toothId;
        }
        createToothChart(submitData)
          .then(response => {
            this.props.loadTreatmentPlanAction(this.props.patientId)
            this.setState({ showLoading: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
          })
          .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showLoading: false }))
      }
    } else {
      this.setState({ warningAlert: true, warningText: "Please select CDT code", showLoading: false });
    }
  }

  handleMissingSubmit(value) {
    const { record, patientId } = this.props;
    const { selectedTopToothArr, selectedBottomToothArr, warningAlert, warningText } = this.state;
    let submitRecord = {}
    this.setState({ showSpinner: true });
    if(value === 'Missing') {
      submitRecord.patientId = this.props.patientId;
      let toothId = selectedTopToothArr.concat(selectedBottomToothArr);
      if(toothId.length == 0) {
        this.setState({ warningAlert: true, warningText: "Please select at least one tooth", showSpinner: false });
      } else {
        submitRecord.toothNumber = toothId.join(',');
        setMissingTooth(submitRecord)
          .then(response => {
            this.props.loadAction(this.props.patientId)
            this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
          })
          .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
      }
    }
    if(value === 'Not Missing') {
      submitRecord.patientId = this.props.patientId;
      let toothId = selectedTopToothArr.concat(selectedBottomToothArr);
      if(toothId.length == 0) {
        this.setState({ warningAlert: true, warningText: "Please select at least one tooth", showSpinner: false });
      } else {
        submitRecord.toothNumber = toothId.join(',');
        unsetMissingTooth(submitRecord)
          .then(response => {
            this.props.loadAction(this.props.patientId)
            this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
          })
          .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
      }
    }
    if(value === 'Edentulous Maxilla') {
      submitRecord.patientId = this.props.patientId;
      setEdentulousMaxilla(submitRecord)
        .then(response => {
          this.props.loadAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
    if(value === 'Dentate Maxilla') {
      submitRecord.patientId = this.props.patientId;
      unsetEdentulousMaxilla(submitRecord)
        .then(response => {
          this.props.loadAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
    if(value === 'Edentulous Mandible') {
      submitRecord.patientId = this.props.patientId;
      setEdentulousMandible(submitRecord)
        .then(response => {
          this.props.loadAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
    if(value === 'Dentate Mandible') {
      submitRecord.patientId = this.props.patientId;
      unsetEdentulousMandible(submitRecord)
        .then(response => {
          this.props.loadAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
  }

  handlePrimarySubmit(value) {
    const { record, patientId } = this.props;
    const { selectedTopToothArr, selectedBottomToothArr, warningAlert, warningText } = this.state;
    let submitRecord = {}
    this.setState({ showSpinner: true });
    if(value === 'Primary') {
      submitRecord.patientId = this.props.patientId;
      let toothId = selectedTopToothArr.concat(selectedBottomToothArr);
      if(toothId.length == 0) {
        this.setState({ warningAlert: true, warningText: "Please select at least one tooth", showSpinner: false });
      } else {
        submitRecord.toothNumber = toothId.join(',');
        setPrimaryTooth(submitRecord)
          .then(response => {
            this.props.loadAction(this.props.patientId)
            this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
          })
          .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
      }
    }
    if(value === 'Permanent') {
      submitRecord.patientId = this.props.patientId;
      let toothId = selectedTopToothArr.concat(selectedBottomToothArr);
      if(toothId.length == 0) {
        this.setState({ warningAlert: true, warningText: "Please select at least one tooth", showSpinner: false });
      } else {
        submitRecord.toothNumber = toothId.join(',');
        unsetPrimaryTooth(submitRecord)
          .then(response => {
            this.props.loadAction(this.props.patientId)
            this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
          })
          .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
      }
    }
    if(value === 'Set all primary') {
      submitRecord.patientId = this.props.patientId;
      setAllPrimary(submitRecord)
        .then(response => {
          this.props.loadAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
    if(value === 'Set all permanent') {
      submitRecord.patientId = this.props.patientId;
      unsetAllPrimary(submitRecord)
        .then(response => {
          this.props.loadAction(this.props.patientId)
          this.setState({ showSpinner: false, selectedTopToothArr: [], selectedBottomToothArr: [] });
        })
        .catch((error) => this.setState({ warningAlert: true, warningText: 'There was an error saving your changes. Please try again.', showSpinner: false }))
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { record = {}, toothChartUpdatedDate, loadAction, patientId, metaData = {}, handleClose, infoColumns, infoExamImaging, primaryProvider } = this.props;
    const { toothChartOptions, procedureCodeCategory, procedureName } = metaData;
    const { allPrimary } = record;
    const legendOptions = toothChartOptions.reduce((a, el) => a.concat(el.options), []);
    const { numberSystem, filteredCode, activeId, showSpinner, showLoading, warningAlert, warningText, selectedTopToothArr, selectedBottomToothArr, showPrimaryInfo, showPermanentInfo } = this.state;
    const TabsMenu = [{name: "Existing Conditions"}, {name: "Missing Teeth"}, {name: "Primary Teeth"}, {name: "Treatment Plan"}];
    return (
      <Col className={styles.ToothChart}>
        <div className="ToothChart">
          <div className={styles.marginStyle}>
            <div className={styles.title}>
              <Col style={{ width: '36%' }}>
                <span className={styles.titleStyle}> Tooth Chart </span>
                <NumberSystem onChange={(e) => this.setState({ numberSystem: e.target.value })} />
              </Col>
              <Col className={styles.header}>
                <Icon glyph="icon-fontello-cancel" className={styles.closeIcon} onClick={handleClose} />
              </Col>
            </div>
          </div>
          <div className={styles.marginStyle}>
            <Row className={styles.toothTop}>
            {record.allPrimary == undefined ? <Spinner /> :
              <Row className={styles.toothRow}>
                <Col className={styles.toothColumn}>
                  <div>
                    <TopView provider={this.props} record={record || {}} view={allPrimary ? PrimaryTopView : ToothTopView} handleToothSelect={this.handleToothSelect.bind(this)} handleToothArr={this.handleToothArr.bind(this)} mode="top" numberSystem={numberSystem} disable={this.props.disable} selectedTooth={selectedTopToothArr.concat(selectedBottomToothArr)} />
                    <Col>&nbsp;</Col>
                    <BottomView provider={this.props} record={record || {}} view={allPrimary ? PrimaryBottomView : ToothBottomView} handleToothSelect={this.handleToothSelect.bind(this)} handleToothArr={this.handleToothArr.bind(this)} mode="bottom" numberSystem={numberSystem} disable={this.props.disable} selectedTooth={selectedTopToothArr.concat(selectedBottomToothArr)} />
                  </div>
                </Col>
                <Col className={styles.tabColumn}>
                  <Tabs id="toothChart">
                    {(TabsMenu.map((prop, i) =>
                      <Tab key={i} eventKey={i} title={prop.name}>
                        <PanelContainer>
                          <PanelBody className={styles.tabBackground}>
                            <Col sm={12} className={styles.tabBackground}>
                              {prop.name === "Existing Conditions" ?
                                <Row>
                                  <Col sm={4} className={styles.formWidth}>
                                    {showLoading ? <Spinner /> :
                                      <EditRecordForm
                                        initialValues={{ providerId: primaryProvider && primaryProvider.id }}
                                        fields={this.props.planColumns.filter(e => e.existing)}
                                        metaData={this.props.metaData}
                                        form={'Existing Conditions Treatment Plan Form'}
                                        disableCancel={true}
                                        onSubmit={(data) => this.handleSave(data, 'Existing')}
                                      />}
                                  </Col>
                                  <Col sm={3} className={styles.categoryWidth}>
                                    <div><h5 className={styles.titleText}>Procedure Category</h5></div>
                                    {procedureCodeCategory && procedureCodeCategory.length > 0 ?
                                      procedureCodeCategory.map((category, i) => {
                                        return (
                                          <div key={i} className={activeId == category.id ? `${styles.activeCode} ${styles.listStyle}` : styles.listStyle} onClick={()=> this.handleExProcedureCode(category.id)}>
                                            {category.name.replace(' CDT Codes','')}
                                          </div>
                                        )
                                      }) : <div></div>}
                                  </Col>
                                  <Col sm={5} className={styles.codeWidth}>
                                    <div>
                                      {filteredCode && filteredCode.length > 0 ?
                                        <h5 className={styles.titleText}>CDT Code</h5> : null}
                                    </div>
                                    <div className={filteredCode && filteredCode.length > 40 ? styles.cdtColumn : null}>
                                      {showSpinner ? <Spinner /> :
                                        !showSpinner && filteredCode && filteredCode.length > 0 ?
                                          filteredCode.map((code, i) => {
                                            return (
                                              <Col sm={3} key={i} className={styles.codeStyle} data-place={"left"} data-tip={`<p style="width: 350px;">${code.procedureType}</p>`} data-html={true} onClick={()=> this.handleSubmit(code.procedureCode, 'Existing')}>
                                                {code.procedureCode}
                                                <ReactTooltip type="info" html={true} />
                                              </Col>
                                            )
                                          }) : <div></div>}
                                    </div>
                                  </Col>
                                </Row> : prop.name === "Treatment Plan" ?
                                <Row>
                                  <Col sm={4} className={styles.formWidth}>
                                    {showLoading ? <Spinner /> :
                                      <EditRecordForm
                                        initialValues={{ providerId: primaryProvider && primaryProvider.id }}
                                        fields={this.props.planColumns.filter(e => e.treatment)}
                                        metaData={this.props.metaData}
                                        form={'Tooth Treatment Plan Form'}
                                        disableCancel={true}
                                        onSubmit={(data) => this.handleSave(data, 'Proposed')}
                                      />}
                                  </Col>
                                  <Col sm={3} className={styles.categoryWidth}>
                                    <div><h5 className={styles.titleText}>Procedure Category</h5></div>
                                    {procedureCodeCategory && procedureCodeCategory.length > 0 ?
                                      procedureCodeCategory.map((category, i) => {
                                        return (
                                          <div key={i} className={activeId == category.id ? `${styles.activeCode} ${styles.listStyle}` : styles.listStyle} onClick={()=> this.handleProcedureCode(category.id)}>
                                            {category.name.replace(' CDT Codes','')}
                                          </div>
                                        )
                                      }) : <div></div>}
                                  </Col>
                                  <Col sm={5} className={styles.codeWidth}>
                                    <div>
                                      {filteredCode && filteredCode.length > 0 ?
                                        <h5 className={styles.titleText}>CDT Code</h5> : null}
                                    </div>
                                    <div className={filteredCode && filteredCode.length > 40 ? styles.cdtColumn : null}>
                                      {showSpinner ? <Spinner /> :
                                        !showSpinner && filteredCode && filteredCode.length > 0 ?
                                          filteredCode.map((code, i) => {
                                            return (
                                              <Col sm={3} key={i} className={styles.codeStyle} data-place={"left"} data-tip={`<p style="width: 350px;">${code.procedureType}</p>`} data-html={true} onClick={()=> this.handleSubmit(code.procedureCode, 'Proposed')}>
                                                {code.procedureCode}
                                                <ReactTooltip type="info" html={true} />
                                              </Col>
                                            )
                                          }) : <div></div>}
                                    </div>
                                  </Col>
                                </Row> : prop.name === "Missing Teeth" ?
                                <Row style={{ height: '525px' }}>
                                  {showSpinner ? <Col sm={4}><Spinner /></Col> :
                                    <Col sm={12} className={styles.missingColumn}>
                                      <div className={styles.missingButton} onClick={()=> this.handleMissingSubmit('Missing')}>
                                        Missing
                                      </div>
                                      <div className={styles.missingButton} onClick={()=> this.handleMissingSubmit('Not Missing')}>
                                        Not Missing
                                      </div>
                                      <div className={styles.missingButton} onClick={()=> this.handleMissingSubmit('Edentulous Maxilla')}>
                                        Edentulous Maxilla
                                      </div>
                                      <div className={styles.missingButton} onClick={()=> this.handleMissingSubmit('Dentate Maxilla')}>
                                        Dentate Maxilla
                                      </div>
                                      <div className={styles.missingButton} onClick={()=> this.handleMissingSubmit('Edentulous Mandible')}>
                                        Edentulous Mandible
                                      </div>
                                      <div className={styles.missingButton} onClick={()=> this.handleMissingSubmit('Dentate Mandible')}>
                                        Dentate Mandible
                                      </div>
                                    </Col>}
                                </Row> : prop.name === "Primary Teeth" ?
                                <Row style={{ height: '525px' }}>
                                  {showSpinner ? <Col sm={4}><Spinner /></Col> :
                                    <Col sm={12} className={styles.missingColumn}>
                                      <div>
                                        <Col sm={4} className={styles.missingButton} onClick={()=> this.handlePrimarySubmit('Primary')}>
                                          Primary
                                        </Col>
                                        <img className={styles.infoStyle} src={require(`img/patients/info.svg`)} ref='foo' onClick={() => this.setState({ showPrimaryInfo: true })} />
                                        <PerioInfo url={"https://s3.amazonaws.com/static.novadonticsllc.com/toothChart/primaryTeeth.html"} show={showPrimaryInfo} primary={true} hide={() => this.setState({ showPrimaryInfo: false })} />
                                      </div>
                                      <div>
                                        <Col sm={4} className={styles.missingButton} onClick={()=> this.handlePrimarySubmit('Permanent')}>
                                          Permanent
                                        </Col>
                                        <img className={styles.infoStyle} src={require(`img/patients/info.svg`)} ref='foo' onClick={() => this.setState({ showPermanentInfo: true })} />
                                        <PerioInfo url={"https://s3.amazonaws.com/static.novadonticsllc.com/toothChart/permanentTeeth.html"} show={showPermanentInfo} hide={() => this.setState({ showPermanentInfo: false })} />
                                      </div>
                                      <Col sm={4} className={styles.missingButton} onClick={()=> this.handlePrimarySubmit('Set all primary')}>
                                        Set all primary
                                      </Col>
                                      <Col sm={4} className={styles.missingButton} onClick={()=> this.handlePrimarySubmit('Set all permanent')}>
                                        Set all permanent
                                      </Col>
                                    </Col>}
                                </Row> : null}
                            </Col>
                          </PanelBody>
                        </PanelContainer>
                      </Tab>
                    ) : null)}
                  </Tabs>
                </Col>
                {warningAlert ?
                  <AlertMessage warning dismiss={() => this.setState({ warningAlert: false })}>{warningText}</AlertMessage> : null}
              </Row>}
            </Row>
          </div>
        </div>
      </Col>
    );
  }

  open() {
    this.setState({ showModal: true });
  }

  close() {
    this.setState({ showModal: false });
  }

}

ToothChart.propTypes = {
  patientId: PropTypes.number,
  columns: PropTypes.array,
  metadata: PropTypes.array,
};

export default compose(
  connect(),
  withProps(({ records, dispatch, error }) => ({
    loadAction: (id) => dispatch(loadToothChart(id)),
    loadTreatmentPlanAction: (id) => dispatch(loadTreatmentPlanDetails(id)),
    records: records,
    error: error
  }))
)(ToothChart);
