/**
*
* ToothImage
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from './utils';
import { Link } from 'react-router';
import styles from './styles.css';
import moment from 'moment-timezone';

const ButtonItem = ({config, onSelect}) => {
  const { title, glyphIcon, imageIcon, btnType, width, height, mode, maxilla, mandible } = config;
  switch(btnType){
    case 'link':
      return <Link className={styles.link} onClick={open}>
                <Icon className={styles.icon} glyph={glyphIcon ? glyphIcon : null} />{title || ''}
             </Link>
    case 'image':
      return <Col onClick={mode == 'top' && maxilla || mode == 'bottom' && mandible ? null : onSelect}>
              {imageIcon && imageIcon.map((image,index)=><img key={index} width={image.width} height={image.height} src={image.url} alt={title || ''} id={image.id} className={styles.img} />)}
             </Col>
    default:
      return <Button bsStyle="standard" onClick={open} className='action-button'>{title || ''}</Button>
  }
}

const Children = ({children}) => children ? <Col style={{textAlign:"center"}} dangerouslySetInnerHTML={{__html: children}} /> : null;

const Tooltip = ({text, show}) => show ? <span className={styles.tooltip}>{text}</span> : null;

class ToothImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false, confirmDialogBox : false, disableShare: false };
  }

  componentDidMount(){
    if(this.props.onRef) {
      this.props.onRef(this);
    }
  }

  componentWillReceiveProps(props) {
    if(this.props.error != props.error && this.props.config && this.props.config.showError) {
      this.props.config.showError(props.error);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { fields, metaData, handleSelect, error, updateError, pristine, submitting, config = {}, noteText, notes, dispatch, form, toothId, selectedTooth, selected } = this.props;
    const { centerAlign, confirmDialog, title, bsSize, tooltip, viewType, shareDet, topView, bottomView, closeModal, modalClass, btnName, className, style, disable } = config;
    const { confirmDialogBox, disableShare } = this.state;

    return (
      <Col style={style} className={selected ? `${styles.toothBorder}` : `${className ? className : ''} ${styles.ModalForm}`}>
        <Children children={topView} />
        <ButtonItem config={config} onSelect={this.onSelect.bind(this)} />
        <Children children={bottomView} />
        <Tooltip text={title} show={tooltip} />
      </Col>
    );
  }

  onSelect(e) {
    e.stopPropagation();
    const { config = {}, fields, handleSelect } = this.props;
    const { record = {}, mode } = config;
  	const { start } = this.state;
  	const toothValue = e.target.id;
  	handleSelect(toothValue);
  }

}

ToothImage.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  updateError: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  record: PropTypes.object,
  fields: PropTypes.array,
  config: PropTypes.object
};

export default reduxForm({
  form: 'toothRecord',
  enableReinitialize: true,
})(ToothImage);
