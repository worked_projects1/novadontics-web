/**
*
* MultiCheckboxField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { stringToObjectConverter } from 'utils/tools';
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';


const renderMultiCheckboxField = ({ input, label, renderOptions, data, columnSize, meta: { touched, error }, children }) => {
    let InputArr = input.value ? input.value.split(',') : [];
    let inputName;
    const handleChange = (e) => {
        const { checked, name, type, value } = e.target;
        const targetArr = name.split('_');
        const targetVal = InputArr.find(key => key.split('#')[0] === targetArr[0]);
        // const targetVal = InputArr.find(key => key.indexOf(targetArr[0]) > -1);
        const targetObj = stringToObjectConverter(targetVal || '', '#');
        const isParent = !targetVal || targetArr.length === 1;
        
        if(isParent){
            if (checked && type === 'checkbox') {
                if(targetArr[0] && targetArr[1] != 'E' && targetArr[1] != 'TS'){
                    inputName = targetArr[0] != "" ? `${targetArr[0]}#E:${targetArr[1] === 'E' ? checked && "T" || "F" : targetObj.E || 'F'}#TS:${targetArr[1] === 'TS' ? value || '0' : targetObj.TS || '0'}`:'';
                }      
                InputArr.push(inputName);
                // InputArr.push(name);
            } else {
                InputArr = InputArr.filter(key => key.split('#')[0] != name);
            }
            input.onChange(InputArr.join());
        } else {
            InputArr = InputArr.map(key => key.split('#')[0] === targetObj['key'] ? `${targetObj.key}#E:${targetArr[1] === 'E' ? checked && "T" || "F" : targetObj.E || 'F'}#TS:${targetArr[1] === 'TS' ? value || '0' : targetObj.TS || '0'}` : key);
            input.onChange(InputArr.join());
        }
        
    }

    return (<Col className={renderOptions.length > 1 ? styles.renderField : null}>
        {data.group ? <label htmlFor={label}>
            <span style={label == "Implant Exam and imaging" ? {color: '#38a0f1'} : null}>{label}</span>
        </label> : null}
        {renderOptions.map((r, index) =>
            <Col key={index} xs={columnSize} className={styles.section} style={(renderOptions.length-1) === index ? {borderBottom: 'none'} : null}>
                {r.options.map((option) => {
                    //const InputVal = InputArr.find(key => key.indexOf(option.value) > -1);
                    const InputVal = InputArr.find(key => key.split('#')[0] === option.value);
                    const InputObj = stringToObjectConverter(InputVal || '', '#');
                    return (<label key={option.value} className={label == "Implant Exam and imaging" ? styles.InlineCheckboxcontainer : styles.container}>{option.label}
                        <input name={option.value} type="checkbox" defaultChecked={InputObj['key'] === option.value} onChange={handleChange} />
                        <span className={styles.checkmark}></span>
                        {InputObj['key'] === option.value ? <div className={label == "Implant Exam and imaging"?styles.inlineChildField:styles.childField}>
                            {option.E ? <label style={{marginTop: '0px'}} className={label == "Implant Exam and imaging" ?styles.inlineExistingField:styles.container}>Existing
                                <input name={`${option.value}_E`} type="checkbox" defaultChecked={InputObj.E == 'T' ? true : false} onChange={handleChange} />
                                <span className={styles.checkmark}></span>
                            </label> : null}
                            {option.TS ? <div className={styles.SelectField}>
                                <label>Treatment Status</label>
                                <select name={`${option.value}_TS`} defaultValue={InputObj.TS} onChange={handleChange}>
                                    <option value="">Choose Option</option>
                                    <option value="P">Planned</option>
                                    <option value="C">Completed</option>
                                </select>
                            </div> : null}
                        </div> : null}
                    </label>)
                })}
            </Col>)}
    </Col>)
}

class MultiCheckboxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        data: PropTypes.object,
        inline: PropTypes.bool,
        options: PropTypes.object,
        optionsMode: PropTypes.oneOf(['edit', 'create']),
        metaData: PropTypes.object,
    };

    render() {
        const { data, inline, options = {}, optionsMode, metaData,initialValues } = this.props;
        const { required } = options.general || {};
        const columnSize = inline || data.fullWidth ? 12 : 6;
        let extraFieldOptions = {};
        if (optionsMode === 'edit') {
            extraFieldOptions = options.edit || {};
        } else if (optionsMode === 'create') {
            extraFieldOptions = options.create || {};
        }
        const isPreDefinedSet = Array.isArray(data.oneOf);
        const normalize = data.number ? (value) => Number(value) : null;

        const renderOptions = isPreDefinedSet ? data.oneOf.map((c) => { return { 'label': c, 'value': c } }) : data.group ? metaData[data.oneOf] : [{ label: data.label, options: metaData[data.oneOf] }];


        return (
            <Field
                initialValues={initialValues}
                name={data.value}
                id={data.label}
                label={data.label}
                renderOptions={renderOptions}
                data={data}
                columnSize={columnSize}
                component={renderMultiCheckboxField}
                normalize={normalize}
                onFocus={e => e.preventDefault()}
                onBlur={e => e.preventDefault()}
            />
        );
    }

}

export default MultiCheckboxField;
