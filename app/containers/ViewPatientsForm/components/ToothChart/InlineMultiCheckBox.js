import React from "react";
import PropTypes from "prop-types";
import { reduxForm } from "redux-form/immutable";
import { Col } from "@sketchpixy/rubix";
import styles from "./styles.css";
import { ImplementationFor, ContentTypes } from "./utils";
import shallowCompare from "react-addons-shallow-compare";
import Button from "@sketchpixy/rubix/lib/Button";

class InlineMultiCheckBoxForm extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }
  render() {
    const { initialValues,fields, metaData, handleSubmit, submitting ,pristine} = this.props;    
    return (
      <Col className={styles.implantImaging}>
        <form onSubmit={handleSubmit}>	
          {fields.map((field, i) => {
            const Component = ImplementationFor[field.type];
            const contentType = ContentTypes[field.type];
            return field.editRecord ? (
              <div key={i}>
                <Component
                  initialValues={initialValues}
                  data={field}
                  contentType={contentType}
                  options={field.formFieldDecorationOptions}
                  optionsMode="edit"
                  metaData={metaData}
                  fullWidth={field.fullWidth}                  
                />
              </div>
            ) : null;
          })}
          <div>
            <Button bsStyle={'standard'} type="submit" disabled={pristine || submitting} className='btn-standard-blue btn btn-default'>{'update'}</Button>
          </div>
        </form>
      </Col>
    );
  }
}
InlineMultiCheckBoxForm.propTypes = {
  handleSubmit: PropTypes.func,
  fields: PropTypes.array,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
};
export default reduxForm({
  form: "inlineMultiCheckBoxForm",
  enableReinitialize: true,
})(InlineMultiCheckBoxForm);
