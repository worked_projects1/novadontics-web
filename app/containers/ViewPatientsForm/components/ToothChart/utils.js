
import Textarea from 'components/Textarea';
import InputField from 'components/InputField';
import SelectField from 'components/SelectField';
import MultiSelectField from 'components/MultiSelectField';
import Upload from 'components/Upload';
import DatePicker from 'components/DatePicker';
import CheckboxField from "components/CheckboxField";
import SignatureField from "components/SignatureField";
import MultiCheckboxField from "./MultiCheckboxField";
import MultiColorField from "components/MultiColorField";
import TimeField from 'components/TimeField';
import SearchField from 'components/SearchField';
import SimpleField from 'components/SimpleField';
import GroupField from 'components/GroupField';

export const ImplementationFor = {
  textarea: Textarea,
  input: InputField,
  checkbox: CheckboxField,
  select: SelectField,
  image: Upload,
  thumbnail: Upload,
  document: Upload,
  video: Upload,
  invoice: Upload,
  agreement: Upload,
  zip: Upload,
  contracts: Upload,
  multiselect: MultiSelectField,
  date: DatePicker,
  signature: SignatureField,
  multicheckbox: MultiCheckboxField,
  multicolor: MultiColorField,
  time: TimeField,
  search: SearchField,
  files: Upload,
  simple: SimpleField,
  group: GroupField
};
  
export const ContentTypes = {
  image: 'image/*',
  thumbnail: 'image/*',
  video: 'video/*,video/mp4,application/pdf',
  document: 'application/pdf',
  invoice: 'application/pdf',
  zip: 'application/*',
  contracts: 'application/pdf,image/*',
  agreement: 'application/pdf,image/*',
  files: '*'
};