/**
*
* Prescription Signature Form
*
*/

import React from 'react';
import { Grid, Col, Modal, Row, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import { compose, withProps } from 'recompose';
import { connect } from 'react-redux';
import { createPrescriptionHistory } from 'blocks/prescriptions/prescriptionHistory';
import moment from 'moment-timezone';
import uploadSignature from 'components/SignatureField/uploadUtils.js'
import Pad from 'react-signature-pad';


class PrescriptionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, signatureUrl: '', enablePrint: true};
  }

  render() {
  const {  patient = {}, practice, profileList = {}, prescriptionValues, user, filteredProvider } = this.props;
  const { enablePrint } = this.state;
    return (
      <Col>
        {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
        <Modal bsSize={'large'} show={this.state.showModal} onHide={this.handleHide.bind(this)}>
            <Modal.Header closeButton>
                <Modal.Title>
                    <Col className={styles.header}>
                        <h3>Prescription</h3>
                    </Col>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.body}>
              <div className={styles.dentistBox}>
                <Grid>
                  <Row style={{ display: 'flex' }}>
                    <Col style={{ width: '50%' }}>
                      <h5 style={{ fontWeight: 'bold' }}>{practice.practice}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>{practice.address}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>{practice.city} {practice.state} {practice.zip}</h5>
                    </Col>
                    <Col style={{ width: '50%' }}>
                      <h5 style={{ fontWeight: 'bold' }}>Prescriber: {filteredProvider != undefined ? filteredProvider.name : ""}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>Email: {filteredProvider != undefined ? filteredProvider.emailAddress : ""}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>Telephone: {filteredProvider != undefined ? filteredProvider.phoneNumber : ""}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>DEA NO: {filteredProvider != undefined ? filteredProvider.deaNumber : ""}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>NPI: {filteredProvider != undefined ? filteredProvider.npiNumber : ""}</h5>
                    </Col>
                  </Row>
                </Grid>
              </div>
              <div className={styles.patientBox}>
                <Grid>
                  <Row style={{ display: 'flex' }}>
                    <Col style={{ width: '10%' }}>
                      <h5>Patient:</h5>
                      <h5>Email:</h5>
                    </Col>
                    <Col style={{ width: '40%' }}>
                      <h5>{patient.name} {patient.last_name}</h5>
                      <h5>{patient.email}</h5>
                    </Col>
                    <Col style={{ width: '13%' }}>
                      <h5>Telephone:</h5>
                      <h5>Date:</h5>
                    </Col>
                    <Col style={{ width: '37%' }}>
                      <h5>{patient.phone}</h5>
                      <h5>{moment().format('MM/DD/YY h:mm A')}</h5>
                    </Col>
                  </Row>
                  <Row style={{ display: 'flex', paddingTop: '20px' }}>
                    <Col style={{ width: '10%' }}>
                      <h1 style={{ fontWeight: 'bold', paddingTop: '10px', fontSize: '40px' }}>Rx</h1>
                    </Col>
                    <Col style={{ width: '90%' }}>
                      <h5 style={{ fontWeight: 'bold' }}>{prescriptionValues.name}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>Disp: {prescriptionValues.count}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>{prescriptionValues.instruction}</h5>
                      <h5 style={{ fontWeight: 'bold' }}>Refils: {prescriptionValues.refills}</h5>
                    </Col>
                  </Row>
                  <Row style={{ display: 'flex', paddingTop: '20px' }}>
                    <Col style={{ width: '50%', paddingTop: '60px' }}>
                      {
                        !prescriptionValues.genericSubstituteOk ?
                          <h5>Generic Substitution Not Permitted</h5> : <h5>Generic Substitution Permitted</h5>
                      }
                    </Col>
                    <Col style={{ width: '45%' }}>
                      <Col id="signatureField" className={styles.Signature} style={{ display: 'flex' }}>
                        <Pad style={{ alignItems: 'flex-start' }} ref={signature => (this.signature = signature)} onEnd={this.handleSignature.bind(this)} />
                        {
                          this.signature != null &&  this.state.signatureUrl != '' ?
                        <Button bsStyle="link" bsSize="xs" className={styles.btn} style={{ alignItems: 'flex-end' }} onClick={this.handleClear.bind(this)}><Icon glyph="icon-fontello-cancel-circle" className={styles.icon} /></Button> :
                        null
                        }
                      </Col>
                      <h5>Signature of prescriber</h5>
                    </Col>
                  </Row>
                </Grid>
              </div>
              {
                !enablePrint ?
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                  <Button bsStyle="standard" disabled>Print</Button>
                </div> :
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                  <Button bsStyle="standard" onClick={this.handlePrint.bind(this)}>Print</Button>
                </div>
              }
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  handleClear(){
    this.signature.clear();
    this.signature = null
    this.setState({signatureUrl:''})
  }

  async handleSignature() {
  this.setState({ enablePrint: false})
    const dataURL = this.signature.toDataURL();
    if(dataURL){
      const signatureUrl = await uploadSignature(dataURL);
      this.setState({signatureUrl:signatureUrl, enablePrint: true})
    }
  }

  handlePrint() {
    const { createRecord, user, prescriptionValues, patientId, filteredProvider } = this.props;
    const providerId = filteredProvider != undefined ? filteredProvider.id : ""
    const pdfAuthToken = user && user.secret || user && user.token;
    createRecord({prescriptionId: prescriptionValues.id,patientId:patientId});
    window.open(`${process.env.API_URL}/prescription/${prescriptionValues.id}/pdf?X-Auth-Token=${pdfAuthToken}&&treatment=${patientId}&&signature=${this.state.signatureUrl}&&timezone=${moment.tz.guess()}&&providerId=${providerId}`);
    this.setState({ showModal: false, enablePrint: true, signatureUrl: '' });
  }

  handleHide() {
     this.setState({ showModal: false, enablePrint: true, signatureUrl: '' });
  }
}

export default compose(
  connect(),
  withProps(({ dispatch }) => ({
      createRecord: (record) => dispatch(createPrescriptionHistory(record))
  }))
)(PrescriptionForm);
