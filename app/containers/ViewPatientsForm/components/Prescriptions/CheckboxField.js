/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { compose, withProps } from 'recompose';
import { connect } from 'react-redux';
import { Col } from '@sketchpixy/rubix';
import styles from 'containers/PatientsPage/components/CheckboxField/styles.css';
import Info from 'containers/PatientsPage/components/Info';
import PrescriptionButton from "../PrescriptionButton";
import PrescriptionForm from "../PrescriptionForm";
import { createPrescriptionHistory } from 'blocks/prescriptions/prescriptionHistory';
import axios from 'axios';
import Highlighter from 'react-highlighter';


class CheckboxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.openPrintTemplate = this.openPrintTemplate.bind(this);
  }

  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    initialValue: PropTypes.object,
    path: PropTypes.string,
  };

  render() {
    const { createRecord, data = {}, options = {}, optionsMode, initialValue, renderMode, path, patient = {}, user, practice, profileList, patientId, disable, filteredProvider } = this.props;
    const { attributes = {} } = data;
    const info =  attributes && attributes.info ? attributes.info : data && data.info ? data.info : false;
    const text = info && info.text ? info.text : false;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={12} className={styles.CheckboxField}>
        <div className={styles.checkboxContainer}>
          <Field
            name={optionsMode == 'request' ? `${data.name}` : `${data.name}.checked`}
            id={optionsMode == 'request' ? `${data.name}` : `${data.name}.checked`}
            component="input"
            type={'checkbox'}
            {...options.general}
            {...extraFieldOptions}
            disabled={(!disable && initialValue && data.disabled) || filteredProvider == undefined ? true : disable}
          />
          <label htmlFor={optionsMode == 'request' ? `${data.name}` : `${data.name}.checked`}>
            <span></span>
          </label>
          <div className={styles.titleHead} style={{ justifyContent: attributes && attributes.info ? 'baseline' : 'space-between', fontWeight: 'initial' }}>
            <Highlighter search={this.props.search} className={styles.mark}>{data.title}</Highlighter>
          </div>
          {attributes && attributes.printTemplate ?
          <Col className={styles.prescriptionButton}>
            <PrescriptionForm createRecord={createRecord} patient={patient} user={user} practice={practice} profileList={profileList} patientId={patientId} prescriptionValues={data.prescriptionValues} filteredProvider={filteredProvider}>
              {(open) => <img src={require('img/patients/IconPrinter.svg')} className={styles.img} onClick={open.bind(this)} />}
            </PrescriptionForm>
          </Col> : null}
        </div>
        <div className={styles.indication}>
          {attributes && attributes.info ? <span><b>Instructions</b>: {text}</span> : ''}
          {data.indications ? <span><b>Indications</b>: {data.indications}</span> : ''}
          {data.contraindications ? <span><b>Precautions & Contraindications</b>: {data.contraindications}</span> : ''}
          {<span><b>Medication Interaction</b>: {data.medicationInteraction || ''}</span>}
          {<span><b>Use in Oral Implantology</b>: {data.useInOralImplantology || ''}</span>}
        </div>
      </Col>
    );
  }

  openPrintTemplate(url) {
    const { createRecord, patientId, data } = this.props;
    if (url.indexOf('.pdf') > -1) {
      window.open(url);
    } else {
      createRecord({prescriptionId: data.id,patientId:patientId});
      
      axios(`${url}?treatment=${patientId}`, {
        method: 'GET',
        responseType: 'blob' //Force to receive data in a Blob Format
      }).then((response) => {
        const file = new Blob(
          [response.data],
          { type: 'application/pdf' });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      });
    }
  }


}

export default compose(
  connect(),
  withProps(({ dispatch }) => ({
      createRecord: (record) => dispatch(createPrescriptionHistory(record))
  }))
)(CheckboxField);
