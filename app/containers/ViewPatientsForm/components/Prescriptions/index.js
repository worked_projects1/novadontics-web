/**
 * 
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Col, Modal, Row,Icon, Alert, Button } from '@sketchpixy/rubix';
import { reduxForm, Field } from 'redux-form/immutable';
import CheckboxField from './CheckboxField';
import PrescriptionHistory from '../PrescriptionHistory';
import styles from './styles.css';
import TreatmentPlan from "../TreatmentPlan";
import InlineRecordForm from '../InlineRecordForm';
import { updateProfile } from 'blocks/prescriptions/prescriptionHistory';
import ModalForm from 'components/ModalRecordForm';
import { array } from 'prop-types';
import { createPrescriptions } from 'blocks/patients/prescriptions';
import { Link } from 'react-router';

const SUCCESS_ALERT_TIMEOUT = 4000;

class Prescriptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, success: false, search: false, filteredProvider: props.filteredProvider };
    }

    handleProfile(data, dispatch, { form }) {
        const submitRecord = data.toJS();
        const { user } = this.props;
        const practiceId = user.practiceId
        this.props.dispatch(updateProfile(data.toJS(), practiceId));
        setTimeout(() => this.handleProviderChange(submitRecord.providerId), 3000);
    }

    handleProviderChange(val) {
        const { metaData } = this.props;
        const { filteredProvider } = this.state;
        const { provider, providers = [] } = metaData || {}
        const selectedProvider = providers.find(provider => provider.id == val)
        this.setState({ filteredProvider: selectedProvider });
    }

    render() {
        const { patientId, title, path, columns, practice,handleClose,records, user, patient = {}, profileColumns, recordsMetaData = {}, updateError, allergy, metaData } = this.props;
        const { currenActiveMedication, preferredPharmacyInformation } = patient;
        const { provider, providers = [] } = metaData || {};
        const { profileList = {} } = recordsMetaData;
        const { success, search, filteredProvider } = this.state;
        const filteredColumns = search && columns && columns.length > 0 ? columns.reduce((a, el) => {
            const filtered = el.fields.filter(field => field.title.toUpperCase().indexOf(search.trim().toUpperCase()) > -1);
            if (filtered && filtered.length > 0) {
                a.push(Object.assign({}, el, { fields: filtered }))
            }
            return a;
        }, []) : columns;

        const providerId = filteredProvider != undefined ? filteredProvider.id : ""
        const npiNumber = filteredProvider != undefined ? filteredProvider.npiNumber : ""
        const deaNumber = filteredProvider != undefined ? filteredProvider.deaNumber : ""

        return <Col className={styles.prescriptions}>
            <div className={styles.marginStyle} bsSize={'large'} show={this.state.showModal} onHide={this.handleHide.bind(this)}>
                <Modal.Header>
                    <Modal.Title>
                        <Col className={styles.header}>
                            <div>
                              <span style={{color:'#EA6225'}}> {title} </span>
                              <Link to="https://novadontics-uploads.s3.amazonaws.com/PrescriptionManual.pdf" target="_blank">
                                  <img width="25px" height="25px" src={require(`img/patients/manual.svg`)} onClick={(e) => e.stopPropagation()} />
                              </Link>
                            </div>
                            <PrescriptionHistory filteredProvider={filteredProvider} profileList={profileList} practice={practice} patient={patient} title="Prescriptions History" user={user} id={patientId} records={records} disable={this.props.disable} />
                            <Icon glyph="icon-fontello-cancel" className={styles.closeIcon} onClick={handleClose} />
                        </Col>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles.body}>
                    <Col className={styles.inlineForm}>
                        <Col>
                            <InlineRecordForm
                                fields={profileColumns}
                                initialValues = {{providerId: providerId, npiNumber: npiNumber, deaNumber: deaNumber}}
                                metaData={this.props.metaData}
                                form={`profileForm`}
                                onSubmit={this.handleProfile.bind(this)}
                                onProviderChange={(val) => this.handleProviderChange(val)}
                                onSubmitSuccess={this.onSuccess.bind(this)}
                                disableCancel={true}
                                disable={this.props.disable}
                            />
                            {success && !updateError ?
                                <Alert success>Your changes have been saved successfully!</Alert> : null}
                            {updateError ?
                                <Alert danger>{updateError}</Alert> : null}
                        </Col>
                        <Col className={styles.allergies}> 
                            <span><b>Current Allergies:</b> {allergy && Array.isArray(allergy) && allergy.length > 0 && allergy.map(a=> a.value).join(', ') || '-'}</span>
                        </Col>
                        <Col className={styles.medication}>
                            <span>
                                <b>Current Active Medication:</b> {currenActiveMedication || '-'}
                            </span>
                            <span>
                                <b>Preferred Pharmacy Information:</b> {preferredPharmacyInformation || '-'}
                            </span>
                        </Col>
                        <Col xs={12} className={styles.searchField}>
                            <label>
                                <input name="prescriptionSearch" type="text" placeholder="Search By Medication Name" onChange={() => this.setState({ search: document.getElementsByName('prescriptionSearch')[0].value })} />
                                <Button bsStyle="standard" type="button" onClick={() => this.setState({ search: document.getElementsByName('prescriptionSearch')[0].value })}>Go</Button>
                            </label>
                        </Col>
                        <form>
                            {filteredColumns && filteredColumns.length > 0 ? filteredColumns.map((item, i) => (
                                <Col className={styles.inline} sm={12} key={i}>
                                    <div className={styles.title}>{item.title}</div>
                                    <div className={styles.fields}>
                                        {item.fields.map((field, l) => {
                                            return (field.type ?
                                                <Col key={l}>
                                                    <CheckboxField
                                                        patientId={patientId}
                                                        patient={patient}
                                                        profileList={profileList}
                                                        filteredProvider={filteredProvider}
                                                        providers={providers}
                                                        practice={practice}
                                                        data={field}
                                                        user={user}
                                                        options={field.required ? { general: { required: true } } : {}}
                                                        optionsMode="create"
                                                        path={path}
                                                        disable={this.props.disable}
                                                        search={search}
                                                    />
                                                </Col> : null)
                                        })}
                                    </div>
                                </Col>
                            )) : <Col xs={12} className={styles.noPrescriptions}>
                                    <Col>No Prescriptions Found</Col>
                                    <Col>
                                        <ModalForm
                                            initialValues={{patientId, category: 'Prescriptions by users'}}
                                            fields={this.props.prescriptionsColumns}
                                            form={`createPrescriptions.${patientId}`}
                                            onSubmit={this.handleSubmit.bind(this)}
                                            metaData={this.props.metaData}
                                            config={{
                                                title: "Add Prescription",
                                                disable: this.props.disable,
                                                style: {marginLeft: '23px'},
                                                closeModal: true
                                            }}
                                        />
                                    </Col>
                                </Col>}
                        </form>
                    </Col>
                </Modal.Body>
            </div>
        </Col>
    }

    handleSubmit(data, dispatch, { form }) {
        const submitRecord = data.toJS();
        dispatch(this.props.createAction(submitRecord, form));
    }

    handleSearch(e) {
        if(e.keyCode === 13){
            this.setState({search: e.target.value})
        }
    }

    onSuccess() {
        this.successAlertTimeout = setTimeout(() => {
            this.setState({ success: true });
        }, 500);
        this.successAlertTimeout = setTimeout(() => {
            this.setState({ success: false });
        }, SUCCESS_ALERT_TIMEOUT);
    }

    handleHide() {
        const { dispatch, actions, patientId } = this.props;
        this.setState({ showModal: false });
        //dispatch(actions.loadRecord(patientId));
    }
}


export default compose(
    connect(),
    reduxForm({
        form: 'prescriptions',
    }),
    withProps(({ dispatch, error }) => ({
      createAction: (record, form) => dispatch(createPrescriptions(record, form)),
      error: error
    }))
  )(Prescriptions);