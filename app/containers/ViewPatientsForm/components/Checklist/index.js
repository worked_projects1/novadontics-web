/**
*
* Checklist
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { Col, Modal } from '@sketchpixy/rubix';
import styles from './styles.css';
import { sortBy } from 'utils/tools'; 




class Checklist extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false, confirmDialogBox : false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { list = {},title, type, displayDate, patientId, path } = this.props;
    const { forms } = list; 
    return (
      <Col className={styles.List}>
        {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
        <Modal show={this.state.showModal} backdrop="static" onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
                <Modal.Title>
                {title}
                <Col className={styles.displayDate}>{displayDate ? displayDate : null}</Col> 
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {forms && forms.length > 0 && forms.filter(_=> _.types === type).length > 0 ?
                  forms.filter(_=> _.types === type).sort(sortBy('checkListOrder', true)).map((content, index)=>(
                    <Col key={index} className={styles.content}>{(index+1) + '. '} <Link to={{pathname: `${path}/${patientId}/${content.id}/edit`, state: location.state}}>{content.title}</Link> </Col>
                  )) : <Col className={styles.content}>No List Found</Col>}
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });	
  }

}

Checklist.propTypes = {
  list: PropTypes.array,
  title: PropTypes.string,
  icon: PropTypes.string,
  firstVisit: PropTypes.string,
};

export default Checklist;
