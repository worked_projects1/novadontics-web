import React from 'react';
import { connect } from 'react-redux';
import { Col, Modal, Button, Icon, Alert } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { compose, withProps } from 'recompose';
import { loadSharedPatient, createSharedPatient, updateSharedPatient, deleteSharedPatient, updateSharedAdminPatient } from 'blocks/patients/sharedPatient';
import ModalForm from 'components/ModalRecordForm';
import ReactTable from "react-table";
import styles from './styles.css';
import ConfirmButton from 'components/ConfirmButton';
import { reset } from 'redux-form';
import EditRecordForm from 'components/EditRecordForm';
import Spinner from 'components/Spinner';
import moment from 'moment';
import Info from 'containers/PatientsPage/components/Info';
import EditModalForm from 'containers/PatientsPage/components/EditModalForm';

class SharedPatient extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false, shareAdmin: false, success: false, pageLoader: false }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    componentWillReceiveProps(props) {
        if (props.updateError != this.props.updateError && this.state.showModal) {
            setTimeout(() => this.props.clearError(), 3000)
        }
        if(props.records && Array.isArray(props.records) && props.records.length > 0 && this.props.records && Array.isArray(this.props.records) && this.props.records.length > 0 && JSON.stringify(props.records) != JSON.stringify(this.props.records)){
            this.setState({ pageLoader: true });
            setTimeout(() => this.setState({ pageLoader: false}), 3000);
        }
    }

    componentDidUpdate(props) {
        if (JSON.stringify(props.sharedAdminRecord) !== JSON.stringify(this.props.sharedAdminRecord)) {
            this.setState({ shareAdmin: this.props.sharedAdminRecord && this.props.sharedAdminRecord.shareToAdmin });
        }


    }

    editableColumnProps = {
        Cell: props => {
            const { column } = props;
            const { columns, metaData, editRecord, deleteRecord, patientId } = this.props;
         
            const handleEdit = (data, dispatch, { form }) => {
                dispatch(editRecord(data.toJS()));
                setTimeout(() => dispatch(this.props.loadRecord(patientId)),2000)
                // this[form].close();
            }
            
            return (column.action ?
                <Col className={styles.actions}>
                    <EditModalForm
                      row={props.original}
                      tableColumns={columns}
                      form={`editSharedPatient.${props.original.id}`}
                      sharedId={props.original.id}
                      metaData={metaData}
                      onSubmit={handleEdit}
                    />
                    <ConfirmButton onConfirm={() => deleteRecord({ id: props && props.original.id, patientId: patientId })}>
                        {(click) => <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => click()}>
                            <Icon glyph="icon-fontello-stop" className={styles.icon} />Stop Sharing
                        </Button>}
                    </ConfirmButton>
                </Col> : <span>{column.value === 'shareTo' ? metaData[column.oneOf].find(e => e.value === props.value) && metaData[column.oneOf].find(e => e.value === props.value).label || '' : column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>);
        }
    };

    render() {
        const { title, columns, sharedAdminColumns, records, sharedAdminRecord, patientId, loadRecord, metaData, updateError,infoColumns } = this.props;
        const { shareAdmin, success } = this.state;

        return <Col>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} bsSize="large" backdrop="static" onHide={this.close.bind(this)} onEnter={() => loadRecord(patientId)} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <Col className={styles.header}>
                            <h3 className={styles.fontColor}>{title}{infoColumns ? <span className={styles.sharedInfoIcon}><Info data={infoColumns}/></span> : null}</h3>
                            <ModalForm
                                initialValues={{ patientId: patientId }}
                                fields={columns}
                                form={`createSharedPatient`}
                                onSubmit={this.handleCreate.bind(this)}
                                metaData={metaData}
                                onRef={createSharedPatient => (this.createSharedPatient = createSharedPatient)}
                                config={{ title: "Add Dentist" }}
                            />
                        </Col>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles.body}>
                    {updateError ?
                        <Alert danger>{updateError}</Alert> : null}
                    {success ?
                        <Alert success>successfully submitted</Alert> : null}
                    {records && !this.state.pageLoader ?
                        <ReactTable
                            columns={columns.filter(a => a.viewRecord).map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                            ))}
                            data={records}
                            pageSize={records && records.length > 0 ? 8 : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        /> : <Spinner />}
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false })
    }

    handleCreate(data, dispatch, { form }) {
        const submitRecord = data.toJS();
        dispatch(this.props.createRecord(Object.assign({}, submitRecord, { shareTo: parseInt(submitRecord.shareTo) })));
        this[form].close();
        dispatch(reset(form));
    }

}


export default compose(
    connect(),
    withProps(({ dispatch }) => ({
        loadRecord: (id) => dispatch(loadSharedPatient(id)),
        createRecord: (record) => dispatch(createSharedPatient(record)),
        editRecord: (record) => dispatch(updateSharedPatient(record)),
        deleteRecord: (record) => dispatch(deleteSharedPatient(record)),
        updateAdminRecord: (record) => dispatch(updateSharedAdminPatient(record))
    }))
)(SharedPatient);