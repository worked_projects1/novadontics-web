/**
*
* ClinicalNotes
*
*/

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { compose, withProps } from 'recompose';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import { loadClinicalNotes, createClinicalNotes, updateClinicalNotes, deleteClinicalNotes, lockClinicalNotes } from 'blocks/patients/clinicalNotes';
import styles from './styles.css';
import ListProvider from './ListProvider';
import Spinner from 'components/Spinner';
import moment from 'moment';
import { notification } from 'antd';
import { parseStructure } from '../../../ViewPatientsForm/utils';
import Alert from 'components/Alert';
import FormProvider from './FormProvider';

class ClinicalNotes extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showForm: false, edit: false, cancelbtn: false };
  }

  componentDidMount() {
    this.props.loadAction(this.props.id)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { id, name, birthDate, path, location, title, records, columns, loading, metaData, onPrescriptionsClick,disable, updateError, allergy, medicalCondition, handleClose, accessLevel, displaySquareIcons, user, showIcons } = this.props;
    const prescriptionDisable = accessLevel == 'Edit' ? displaySquareIcons.includes('prescriptions') ? false : true : false
    const { showForm, edit, cancelbtn } = this.state;
    const InitialValues = edit && getInitialValues({ value: edit }, '1Z', columns) || { patientId: id, allergies: allergy.map(a => a.value).join(', '), medicalConditions: medicalCondition.map(a => a.value).join(', ')};

    return (
      <div className={styles.ClinicalNotes}>
        <Col className={styles.header}>
          <span className={styles.titleStyle}> {title} </span>
            <Col>
              {!disable ?
                <Button bsStyle="standard" type="button" onClick={() => this.setState({ showForm: true })}>Add Notes</Button> : null}
              {showIcons === 'clinicalNotes' ? <Icon glyph="icon-fontello-cancel" className={styles.closeIcon} onClick={handleClose} /> : null}
            </Col>
        </Col>
        <div className={styles.space}>
          {records && records.length > 0 && !loading ? records.map((preview, index) => {
            return <ListProvider
              key={index}
              preview={preview}
              handleDelete={this.handleDelete.bind(this)}
              handleLock={this.handleLock.bind(this)}
              handleUpdate={(preview) => this.setState({ edit: preview })}
              disable={disable}
            />
          }) : loading ? <Spinner /> : <div className={styles.noList}>No Notes Found.</div>}
        </div>
        {updateError ?
          <Alert warning dismiss={() => this.props.clearError()}>{updateError}</Alert> : null}
        {edit || showForm ? <FormProvider
          showForm={showForm || edit ? true : false}
          initialValues={Object.keys(InitialValues).reduce((a, el) => Object.assign({}, a, {dob: birthDate}, InitialValues[el] !== 'null' && InitialValues[el] !== null && InitialValues[el] !== '' ? { [el]: InitialValues[el] } : {}), {})}
          name={name}
          edit={edit}
          path={path}
          formSchema={columns}
          form={`ClinicalNotes_${id}`}
          metaData={metaData}
          user={user}
          shared={edit && edit.lock || disable || false}
          prescriptionDisable={prescriptionDisable}
          onSubmit={this.handleSumit.bind(this)}
          onPrescriptionsClick={onPrescriptionsClick}
          onCancel={() => edit ? this.setState({ edit: false }) : this.setState({ showForm: false })}
          editNotes={edit ? true : false}
          locationState={location}
          formId={'1Z'}
          cancelbtn={true}
          cancelBtnName={(edit && edit.lock) || disable ? 'Back' : ' '}
          btnName={'Save'}
          modal /> : null}
      </div>
    );
  }

  handleDelete(record) {
    this.props.deleteAction(record);
  }

  handleLock(record) {
    this.props.lockAction(record);
  }

  handleSumit(data, dispatch, { form }) {
    const { createAction, updateAction } = this.props;
    const { edit } = this.state;
    let submitRecord = Object.assign({}, data.toJS());
    if(!edit) {
      submitRecord.procedure.procedure = submitRecord.procedure && submitRecord.procedure.procedure.length > 0 && submitRecord.procedure.procedure.filter(item => item.selected === true).map(el => { delete el.selected; return el; });
    }
    //const procedureDescription = document.getElementsByName('procedureDescription')[0].value;
    //const procedureCode = document.getElementsByName('procedureCode')[0].value;
    //submitRecord.procedureCode = procedureCode;
    //submitRecord.procedureDescription = procedureDescription;
    submitRecord.implantData = submitRecord.implantData != undefined ? submitRecord.implantData.implantData : [];
    submitRecord.boneGraftingData = submitRecord.boneGraftingData != undefined ? submitRecord.boneGraftingData.boneGraftingData : [];
    const stepValues = Object.keys(submitRecord).length > 0 ?
      Object.keys(submitRecord)
        .filter(key => key)
        .reduce((obj, key) => {
          obj[key] = (typeof (submitRecord[key]) === 'object') && key != "implantData" && key != "boneGraftingData" ? JSON.stringify(submitRecord[key]).replace(/"/g, '★').replace(/\//g, "\\/") : submitRecord[key];
          return obj;
        }, {}) : {};

    submitRecord = Object.assign({}, stepValues, { date: moment(stepValues.date).format('YYYY-MM-DD'), dischargeDate: moment(stepValues.dischargeDate).format('YYYY-MM-DD') });
    if (!submitRecord.date) {
      notification['error']({
        message: 'Error',
        description:
          `Date Field should not be blank`,
      });
      return false;
    }

    if (submitRecord.id) {
      updateAction(submitRecord, form);
      this.setState({ edit: false });
    } else {
      createAction(submitRecord, form);
      this.setState({ showForm: false });
    }

  }


}

function getInitialValues(stepValues = {}, formId, formSchema) {

  let schemaValues;
  formSchema.map((form) => {
    form['forms'].map((schema, index) => {
      if (form['forms'][index]['id'] == formId) {
        schemaValues = form['forms'][index];
      }
    })
  })
  const parsedValues = Object.keys(stepValues).length > 0 ?
    Object.keys(stepValues.value)
      .filter(key => key)
      .reduce((obj, key) => {
        obj[key] = parseStructure(stepValues['value'][key]);
        return obj;
      }, {}) : {};

  return parsedValues;
}

ClinicalNotes.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string
};


export default compose(
  connect(),
  withProps(({ record, dispatch, error }) => ({
    loadAction: (id) => dispatch(loadClinicalNotes(id)),
    createAction: (record, form) => dispatch(createClinicalNotes(record, form)),
    updateAction: (record, form) => dispatch(updateClinicalNotes(record, form)),
    deleteAction: (record) => dispatch(deleteClinicalNotes(record)),
    lockAction: (record) => dispatch(lockClinicalNotes(record)),
    record: record,
    error: error
  }))
)(ClinicalNotes);