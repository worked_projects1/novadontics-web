/**
*
* Form Provider
*
*/

import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { Modal, Col } from '@sketchpixy/rubix';
import EditPatientForm from '../../../PatientsPage/components/EditPatientForm';
import styles from './styles.css';
import { getTreatmentPlanProcedures } from 'blocks/patients/treatmentPlan';

class FormProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isExpand: false, isExpandPD: false, procedureValue: [] }
  }

  componentDidMount() {
    const { initialValues = {}, edit } = this.props;
    const { patientId } = initialValues;
    if(!edit) {
      getTreatmentPlanProcedures(patientId)
        .then(procedureDetails => {
          this.setState({ procedureValue: procedureDetails });
        })
    }
  }

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  handleExpandPD() {
    this.setState({ isExpandPD: !this.state.isExpandPD });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
        const { user, edit, formId, initialValues } = this.props;
        const { procedureValue } = this.state;
        const pdfAuthToken = user && user.secret || user && user.token;

    return (
        <Modal show={this.props.showForm} backdrop="static" className="clinicalNotes" bsSize="large" onHide={this.props.onCancel.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>
              <Col className={styles.header}>
                <span style={{ color:'#EA6225' }}>Clinical Notes Form</span>
                {pdfAuthToken && edit.lock == false ?
                  <a href={`${process.env.API_URL}/clinicalNotes/${edit.id}/pdf?X-Auth-Token=${pdfAuthToken}`} target="_blank" data-rel="external">
                    <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Clinical Notes PDF"} alt="pdf" className={styles.img} />
                  </a>  : null
                }
              </Col>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <EditPatientForm
              {...this.props}
              initialValues={edit && initialValues || Object.assign({}, initialValues, {procedure: { procedure: procedureValue }})}
              isExpandPD={this.state.isExpandPD}
              handleExpandPD={this.handleExpandPD.bind(this)}
              isExpand={this.state.isExpand}
              handleExpand={this.handleExpand.bind(this)}
            />
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <span style={{color:'#EA6225'}}>&#169; Novadontics, LLC</span>
            </div>
           </Modal.Body>
        </Modal>
    );
  }

}

export default FormProvider;