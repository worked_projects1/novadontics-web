/**
*
* LockButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Row, Modal, Button } from '@sketchpixy/rubix';
import styles from './styles.css';


class LockButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    return (
      <Col className={styles.lock}>
      	{this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
        <Modal show={this.state.showModal} className={"deleteModal"}>
          <Modal.Body>
            <Row className={styles.rowStandard}>
              <h4>Permanently lock this note?</h4>
              <Col>Once this note is locked, you will not be able to unlock or edit the content or status of the note.</Col>
            </Row>
          </Modal.Body>
          <Modal.Footer className={styles.footer}>
            <Button bsStyle="standard" type="button" onClick={this.close.bind(this)} className={styles.submitButton}>Cancel</Button>
            <Col className={styles.vr}></Col>
            <Button bsStyle="standard" type="button" onClick={this.lock.bind(this)} className={styles.submitButton}>Permanently Lock</Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
  	this.setState({ showModal: true })
  }

  lock(){
    this.setState({ showModal: false });
    this.props.onLock();
  }

}

LockButton.propTypes = {
    onLock: PropTypes.func,
};

export default LockButton;
