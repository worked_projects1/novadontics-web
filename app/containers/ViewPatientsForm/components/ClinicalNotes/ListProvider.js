

import React from 'react';
import { Col, Button } from '@sketchpixy/rubix';
import ConfirmButton from 'components/ConfirmButton';
import LockButton from './LockButton';
import styles from './styles.css';
import moment from 'moment';

class ListProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showNote: false };
        this.toggleNote = this.toggleNote.bind(this);
    }

    render() {
        const { preview, handleDelete, handleUpdate, handleLock, disable } = this.props;
        const { date, time, note, providerName, lock } = preview;
        const { showNote } = this.state;
        const previewData = Object.assign({}, preview, { implantData: { implantData: preview.implantData }, boneGraftingData: { boneGraftingData: preview.boneGraftingData } });

        return <Col>
            <Col className={styles.listNotes}>
                <Col className={styles.listTitle} onClick={() => handleUpdate(previewData)}>
                <b>{moment(date).format('MM/DD/YYYY')} {time}</b>, {providerName && providerName + ',' || ''} <span style={{fontWeight:"bold"}}>Note: </span>{(note && note.length > 130 && !showNote) ? note.substring(0, 130) + "...." : note}
                </Col>
                <Col className={styles.actions}>
                    <Button bsStyle="link" onClick={() => handleUpdate(previewData)} bsSize="xs" className={styles.btn}>{lock || disable ? 'View' : 'Edit'}</Button>
                    <LockButton onLock={() => handleLock(previewData)}>
                        {(open) => <Button bsStyle="link" onClick={!lock ? open.bind(this) : null} disabled={disable} bsSize="xs" className={styles.btn}>{lock ? 'Locked' : 'Lock'}</Button>}
                    </LockButton>
                    {!lock ? <ConfirmButton onConfirm={() => handleDelete(previewData)}>{(click) => <Button onClick={() => click()} disabled={disable} bsStyle="link" bsSize="xs" className={styles.btn} >Delete</Button>}</ConfirmButton>: <Button bsStyle="link" bsSize="xs" className={styles.btn}><span style={{ color: '#fff' }}>Lock</span></Button>}
                </Col>
            </Col>
        </Col>
    }

    /**
     * Below function to show/hide notes 
     */
    toggleNote() {
        this.setState({ showNote: !this.state.showNote });
    }
}

export default ListProvider;