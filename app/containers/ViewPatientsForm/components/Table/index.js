/**
*
* Table
*
*/

import React from 'react';
import { Col, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import ReactTable from "react-table";
import Info from 'containers/PatientsPage/components/Info';
import styles from './styles.css';

class Table extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.loadRecord();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { records, title, columns, notes, infoColumns, handleClose } = this.props;
    return (
      <Col className={styles.Table}>
        <Col className={styles.header}>
          <Col className={styles.headerInfo}>
            <span className={styles.titleStyle}> {title} </span>
            {infoColumns ? <Info data={infoColumns} /> : null}
          </Col>
          <Icon glyph="icon-fontello-cancel" className={styles.closeIcon} onClick={handleClose} />
        </Col>
        <Col>
          <ReactTable
            columns={columns}
            data={records ? records : []}
            pageSize={records && records.length > 0 ? 8 : 0}
            showPageJump={false}
            resizable={false}
            showPageSizeOptions={false}
            previousText={"Back"}
            pageText={""}
            noDataText={"No entries to show."}
          />
          <span style={{ fontWeight: '500' }}> {notes} </span>
        </Col>
      </Col>
    );
  }

  load() {
    const { loadRecord } = this.props;
    if (typeof (loadRecord) === 'function') {
      loadRecord();
    }
  }

}

export default Table;