/**
*
* TableData
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Table, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import moment from 'moment';
import styles from './styles.css';
import { Field } from 'redux-form/immutable';


class TableData extends React.Component {
    constructor(props) {
        super(props); 
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    textformatting(data,name) {
        name = name.replace("data[","");
        name = name.replace("]","");
        data = data[name];
        return <div style={{ fontWeight: '400' }}>
                  <span>{data.chiefComplaint}</span><br />
                  <span>{data.palpation}</span><br />
                  <span>{data.percussion}</span><br />
                  <span>{data.chewing}</span><br />
                  <span>{data.bite}</span><br />
                  <span>{data.mobility}</span><br />
                  <span>{data.air}</span><br />
                  <span>{data.cold}</span><br />
                  <span>{data.hot}</span><br />
                  <span>{data.ept}</span><br />
                  <span>{data.occlusalTrauma}</span><br />
                  <span>{data.sinusTract}</span><br />
                  <span>{data.fracture}</span><br />
                  <span>{data.pulpalDiagnosis}</span><br />
                  <span>{data.periapicalDiagnosis}</span><br />
                  <span>{data.endoPrognosis}</span><br />
                  <span>{data.perioPrognosis}</span><br />
                  <span>{data.restorativePrognosis}</span><br />
                  <span>{data.recommendedTx}</span><br />
                  <span>{data.recommendedTx2}</span><br />
                  <span>{data.recommendedTx3}</span><br />
                  <span>{data.estNumOfVisits}</span><br />
               </div>;
    }

    render() {
        const { records, view, mode, numberSystem, fields, handleSubmit, error, updateError, pristine, submitting, setRecord, clearRecord, initialValues, metaData, form, dispatch } = this.props;
        const selected = initialValues && initialValues.toJS() || false;
        const editth =  Object.keys(selected).length>0 && selected.rowPosition == mode;

        return (
            <form onSubmit={handleSubmit}>
                <Table className={`${styles.Table} periochartForm`} condensed responsive>
                    <thead style={{ backgroundColor: '#ea6225', color: '#FFF' }}>
                        <tr>
                            <th style={{ position: 'sticky', left: '0', zIndex: '2', background: '#ea6225', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>Provider</th>
                            <th style={{ position: 'sticky', left: '190px', zIndex: '2', background: '#ea6225', outline: '1px solid #8080807a', outlineOffset: '-1px' }}></th>
                            {view.map((tooth,index)=><th key={index}>{tooth[numberSystem]}</th>)}
                            {editth ?<th className={styles.stickycol}></th>:<th className={styles.stickycol}></th> }
                        </tr>
                    </thead>
                    <tbody>
                        {records.map((record,index)=> {
                          const edit =  JSON.stringify(record) == JSON.stringify(selected) || (selected.id && record.id === selected.id);
                          return <tr key={index}>
                            {fields.map((field, i) => {
                              const Component = ImplementationFor[field.type];
                              const contentType = ContentTypes[field.type];
                              const ComponentSelect = ImplementationFor[field.typeSelect];
                              const contentTypeSelect = ContentTypes[field.typeSelect];
                              return (
                                (field.editRecord && edit && field.label == "Endo Data" ?
                                  <td key={i} className="hidden-print">
                                    <div className={styles.multic}>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.chiefComplaint}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.palpation}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.percussion}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.chewing}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.bite}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.mobility}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.air}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.cold}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.hot}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.ept}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.occlusalTrauma}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.sinusTract}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.fracture}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.pulpalDiagnosis}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.periapicalDiagnosis}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.endoPrognosis}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.perioPrognosis}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.restorativePrognosis}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.recommendedTx}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.recommendedTx2}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.recommendedTx3}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          perioStyle={true}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                      <Col xs={12} className={styles.endoCenter}>
                                        <span className={styles.lable}></span>
                                        <ComponentSelect
                                          data={field.estNumOfVisits}
                                          contentType={contentTypeSelect}
                                          options={field.formFieldDecorationOptions}
                                          optionsMode="edit"
                                          inline
                                        />
                                      </Col>
                                    </div>
                                  </td>
                          : field.editRecord && edit && field.label == "Provider" ?
                            <td key={i} className="hidden-print" style={{ position: 'sticky', left: '0', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                              <div className={styles.singlec}>
                                <Component
                                  data={field}
                                  contentType={contentType}
                                  metaData={metaData}
                                  options={field.formFieldDecorationOptions}
                                  optionsMode="edit"
                                  inline
                                />
                              </div>
                            </td>
                          : field.label == 'Testing' && field.viewRecord ?
                              <td key={i} className="hidden-print" onClick={()=> setRecord(record)} style={{ position: 'sticky', left: '190px', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                                <div className={styles.multic1}>
                                  {
                                    edit ?
                                      <div style={{ fontWeight: '700' }}>
                                        <div style={{ marginTop: '0px' }}><span>Chief Complaint<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Palpation<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Percussion<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Chewing<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Bite<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Mobility<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Air<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Cold<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Hot<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>EPT<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Occlusal Trauma<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Sinus Tract<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Fracture<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Pulpal Diagnosis<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Periapical Diagnosis<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Endo Prognosis<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Perio Prognosis<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Restorative Prognosis<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Recommended Tx<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>2nd Recommended Tx<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>3rd Recommended Tx<br /></span></div>
                                        <div style={{ marginTop: '18px' }}><span>Estimate # of Visits<br /></span></div>
                                      </div> :
                                    field.label == 'Testing' && !edit ?
                                      <div style={{ fontWeight: '700' }}>
                                        <span>Chief Complaint<br />Palpation<br />Percussion<br />Chewing<br />Bite<br />Mobility<br />Air<br />Cold<br />Hot<br />EPT<br />Occlusal Trauma<br />Sinus Tract<br />Fracture<br />Pulpal Diagnosis<br />Periapical Diagnosis<br />Endo Prognosis<br />Perio Prognosis<br />Restorative Prognosis<br />Recommended Tx<br />2nd Recommended Tx<br />3rd Recommended Tx<br />Estimate # of Visits<br /></span>
                                      </div> : null
                                  }
                                </div>
                              </td>
                          : field.label == 'Endo Data' && field.viewRecord ?
                              <td key={i} className="hidden-print" onClick={()=> setRecord(record)}>
                                <div className={styles.multic1} style={{ fontWeight: '700' }}>
                                  {
                                    typeof record[field.value] !="string" ?
                                      this.textformatting(record["data"],field.value) : ''
                                  }
                                </div>
                              </td>
                          : field.label == 'Provider' && field.viewRecord ?
                              <td key={i} className="hidden-print" onClick={()=> setRecord(record)} style={{ position: 'sticky', left: '0', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                                <div className={styles.multic1} style={{ fontWeight: '700' }}>
                                  {
                                    record[field.value] != '' && metaData['providers'].find(e => e.value == record[field.value]) ? metaData['providers'].find(e => e.value == record[field.value]).label : ''
                                  }
                                </div>
                              </td> : null ));
                        })}
                            {edit ? <td style={{width:'2%'}} className={styles.stickycolbig}>
                                 <Col>
                                <Button type="submit" bsStyle="link" bsSize="xs" className={styles.btn}><Icon glyph="icon-fontello-floppy" disabled={pristine || submitting} className={submitting ? `${styles.icon}buttonLoading` : `${styles.icon}`} />Save</Button>
                                <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={()=> setRecord({})}><Icon glyph="icon-fontello-cancel-circle" className={styles.icon} />Cancel</Button>
                                <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={()=> clearRecord(record)}><Icon glyph="icon-fontello-cancel" className={styles.icon} />Clear</Button></Col>
                                </td>:
                             editth ?
                            <td style={{width:'2%'}} className={styles.stickycol2}>
                                <Button bsSize="xs" bsStyle="link" className={styles.btn} onClick={()=> setRecord(record)}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>
                            </td>:
                            <td style={{width:'2%'}} className={styles.stickycol1} >
                                <Button bsSize="xs" bsStyle="link" className={styles.btn} onClick={()=> setRecord(record)}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>
                            </td>}
                          </tr>
                          }
                      )}
                    </tbody>
                </Table>
            </form>
        );
    }
}


TableData.propTypes = {
    records: PropTypes.array,
    view: PropTypes.array,
    numberSystem: PropTypes.string,
    fields: PropTypes.array,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    updateError: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,  
};

export default reduxForm({
  form: 'endoChartForm',
  enableReinitialize: true,
})(TableData);

