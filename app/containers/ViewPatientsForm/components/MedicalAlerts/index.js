/**
*
* List
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { Col, Modal } from '@sketchpixy/rubix';
import styles from './styles.css';
import moment from 'moment-timezone';

class MedicalAlerts extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false, confirmDialogBox : false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { list,title, type, notes, displayDate, patientId, path, allergyList, allergyTitle } = this.props;
     return (
      <Col className={styles.List}>
        {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
        <Modal show={this.state.showModal} backdrop="static" onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
                <Modal.Title>
                  <span style={{color:'#EA6225'}}> {title} </span>
                  <Col className={styles.displayDate}>{displayDate ? displayDate : null}</Col>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {list && list.length > 0 ? list.map((content, index)=>(
                    <Col key={index} className={styles.content}>
                      <span>{(index+1) + '. ' + content.value}</span>
                      <span>{moment(content.date).format('MM/DD/YYYY')}</span>
                    </Col>
                )) : <Col className={styles.content}>No List Found</Col>}
                <br /><br />
                <div>
                  <div><span style={{color:'#EA6225', fontSize: '18px'}}> {allergyTitle} </span></div>
                  <br />
                  {allergyList && allergyList.length > 0 ? allergyList.map((content, index)=>(
                      <Col key={index} className={styles.content}>
                        <span>{(index+1) + '. ' + content.value}</span>
                        <span>{moment(content.date).format('MM/DD/YYYY')}</span>
                      </Col>
                  )) : <Col className={styles.content}>No List Found</Col>}
                </div>
                <br /><br />
                <span style= {{fontWeight: '500'}}> {notes} </span>
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });	
  }

}

MedicalAlerts.propTypes = {
  list: PropTypes.array,
  title: PropTypes.string,
  icon: PropTypes.string,
  firstVisit: PropTypes.string,
};

export default MedicalAlerts;
