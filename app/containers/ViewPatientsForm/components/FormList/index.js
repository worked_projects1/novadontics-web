/**
*
* FormList
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { Col, Modal, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import { sortBy } from 'utils/tools';
import Disclaimer from './Disclaimer';

class FormList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false, confirmDialogBox: false };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { list = {}, title, displayDate,display, patientId, path, noIndex, notes } = this.props;
    return (
      <Col className={styles.List}>
        {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
        <Modal show={this.state.showModal} backdrop="static" onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>
              <Col className={styles.header}>
                <span className={styles.fontColor}> {title} </span>
                <Col className={styles.displayDate}>{displayDate ? displayDate : null}</Col>
                { display ?
                <Disclaimer>
                  {(open) => <Button bsStyle="standard" type="button" onClick={open.bind(this)}>Disclaimer</Button>}
                </Disclaimer>:null}
              </Col>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {list && list.length > 0 ?
              list.sort(sortBy('order', true)).map((content, index) => (
                <Col key={index} className={styles.content}>{!noIndex ? (index + 1) + '. ' : ''} <Link to={{ pathname: `${path}/${patientId}/${content.id}/edit`, state: location.state }} className={styles.textContent}>{content.title}</Link> </Col>
              )) : <Col className={styles.content}>No List Found</Col>}
                <br /><br />
                <span style= {{fontSize: '13px', fontWeight: '500', color: '#EA6225'}}> {notes} </span>
          </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

}

FormList.propTypes = {
  list: PropTypes.array,
  title: PropTypes.string,
  icon: PropTypes.string,
  firstVisit: PropTypes.string,
};

export default FormList;
