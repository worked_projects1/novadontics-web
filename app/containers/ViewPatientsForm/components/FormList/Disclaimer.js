

import React from 'react';
import { Modal, Button } from '@sketchpixy/rubix';

export default class Disclaimer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false }
  }

  render() {
    return (
      <div>
        {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}

        <Modal show={this.state.showModal} backdrop="static" onHide={this.closeModal.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>
              Disclaimer
                </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              Like all dental procedures,the Novadontics Checklists & Protocols are not appropriate for every patient.
              Novadontics provides certain tools and training that are helpful to dentists, but Novadontics does not engage in the practice of dentistry.
              As a licensed and experienced dentist, you (“Licensee”) must use Licensee’s professional expertise and judgment to evaluate each patient, discuss all of the options with the patient, and help the patient to determine whether or not the Protocol and Novadontics recommended implant products are appropriate for him or her.
              Licensee must not follow the Protocol or use the products or materials recommended by Novadontics when it is in the patient’s best interest to use other techniques or products.
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="standard" type="button" onClick={this.closeModal.bind(this)}>Ok</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
  closeModal() {
    this.setState({ showModal: false });
  }

  openModal() {
    this.setState({ showModal: true });
  }

}