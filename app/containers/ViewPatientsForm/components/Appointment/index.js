/**
 * Appointment
 */

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, Field } from 'redux-form/immutable';
import { compose, withProps } from 'recompose';
import { Link } from 'react-router';
import { Modal, Col, Icon } from '@sketchpixy/rubix';
import { loadAppointment } from 'blocks/patients/appointment';
import { loadDate } from '../../../../module/utils/sagas';
import moment from 'moment';
import ShowText from 'components/ShowText';
import styles from './styles.css';
import TableWrapper from 'components/TableWrapper';
import ReactTable from "react-table";
const DEFAULT_PATH = process.env.PUBLIC_PATH || '';


const codeComponent = ({ input, type, column, data }) => {
    const parsedValues = typeof data == 'string' && data.includes('★') ? JSON.parse(data.replace(/★/g, '"')) : data;
    var procedureCode = parsedValues != 'false' && parsedValues.procedure && parsedValues.procedure.length > 0 ? parsedValues.procedure.map(item => item.procedureCode).toString() : '';
    return <span>{procedureCode}</span>;
};

const typeComponent = ({ input, type, column, data }) => {
    const parsedValues = typeof data == 'string' && data.includes('★') ? JSON.parse(data.replace(/★/g, '"')) : data;
    var procedureDescriptionValue = parsedValues != 'false' && parsedValues.procedure && parsedValues.procedure.length > 0 ? parsedValues.procedure.map(item => item.procedureDescription).toString() : '';
    var procedureDescription = procedureDescriptionValue != '' ? procedureDescriptionValue.split(".,").join( ".<br /><br /><br />").split(". ,").join( ".<br /><br /><br />") : '';
    return <span style={{ display: 'flex' }}>
              {column.limit && procedureDescription && procedureDescription.length > column.limit ?
                 procedureDescription.substring(0, column.limit) + '...' : procedureDescription}
              {column.limit && procedureDescription && procedureDescription.length > column.limit ?
                  <ShowText title={column.label} procedureArr={true} parsedValues={parsedValues}>{(open) => <div className={styles.view} onClick={open.bind(this)} />}</ShowText> : null}
            </span>;
};


class Appointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    editableColumnProps = {
      Cell: props => {
        const { column } = props;
        return (
          column.label == 'Procedure Description' && column.showIcon == true ?
              <span style={{ display: 'flex' }}>
                <Field name={column.value} id={column.label} component={typeComponent} type={column.type} column={column} data={props.value} />
              </span> :
          column.label == 'Procedure Code' ?
              <span style={{ display: 'flex' }}>
                <Field name={column.value} id={column.label} component={codeComponent} type={column.type} column={column} data={props.value} />
              </span> : <span>{props.value}</span>
        );
      }
    }


    render() {
        const { id, title, record, loadAction, updateAction, columns } = this.props;
        const tableColumns = columns.filter((column) => column.visible);
        const headers = { recPerPage: 25, searchText: '', pageNumber: 1, status: '' }
        const scheduled = record.filter(_ => moment.utc().diff(new Date(moment(_.date).format('YYYY/MM/DD ') + " " + moment(_.startTime, ["h:mm A"]).format("HH:mm")), 'minutes') < 1);
        const past = record.filter(_ => moment.utc().diff(new Date(moment(_.date).format('YYYY/MM/DD ') + " " + moment(_.startTime, ["h:mm A"]).format("HH:mm")), 'minutes') > 1);
        let rows = record;


        tableColumns.forEach((column) => {
          switch (column.type) {
            case 'date':
              column.label == 'Date' && column.dateLink == true ?
                rows = rows.map((row) => Object.assign(
                  row,
                  { [column.value]:
                      <div onClick={() => this.handleDate(row[column.value])}>
                        {moment(row[column.value]).format('MM/DD/YY')}
                      </div>
                  },
                )) :
                rows = rows;
              break;
            case 'input':
              {
                if(column.label == 'Type' && column.appointmentBook == true) {
                  rows = rows.map((row) => {
                    if(row.state == 'Future') {
                      Object.assign(
                        row,
                        {
                          [column.value]:
                            <span>Scheduled</span>
                        },
                      )
                    }
                  })
                }
              }
              break;
            default:
              break;
          }
        });


        return (<Col>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} className="appointmentBook" id='appointmentsBook' backdrop="static" onHide={this.close.bind(this)} onEnter={() => loadAction(id)}>
                <Modal.Header closeButton>
                    <Modal.Title> <span style={{color:'#EA6225'}}>{title}</span> <Icon className={styles.icon} onClick={() => updateAction(moment().format('YYYY-MM-DD'))} glyph="icon-fontello-calendar" /></Modal.Title>
                    {title=='Appointments'?<span className={styles.subHeader}>Click on the date to access the appointment on the calendar</span>:null}
                </Modal.Header>
                    <Modal.Body>
                        <Col className="AppointmentBook">
                          <ReactTable
                            columns={tableColumns.map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                            ))}
                            data={record ? record : []}
                            pageSize={record && record.length > 0 ? 25 : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                          />
                        </Col>
                    </Modal.Body>
            </Modal>
        </Col>)
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

    handleDate(date) {
      const { updateAction } = this.props;
      updateAction(moment(date.props.children).format('YYYY-MM-DD'));
    }
}


Appointment.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    error: PropTypes.string,
};

export default compose(
    reduxForm({
        form: 'appointment',
        enableReinitialize: true,
    }),
    connect(),
    withProps(({ dispatch , error }) => ({
        loadAction: (id) => dispatch(loadAppointment(id)),
        updateAction: (date) => dispatch(loadDate(date)),
        error: error
    }))
)(Appointment);