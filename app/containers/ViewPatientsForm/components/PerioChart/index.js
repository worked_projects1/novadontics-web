/**
*
* Perio Chart
*
*/

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Col, Row, Modal,Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import {compose, withProps} from 'recompose';
import styles from './styles.css';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import { reduxForm, change } from 'redux-form/immutable';
import { loadPerioChartDates, loadPerioChart, updatePerioChart } from 'blocks/patients/periochart';
import moment from 'moment';
import Spinner from 'components/Spinner';
import { default as TopTable, default as BottomTable  } from './TableData';
import PerioInfo from './PerioInfo.js';

const NumberSystem = ({onChange}) => <select className={styles.system} onChange={onChange}><option value="AM">AMERICAN</option><option value="IN">INTERNATIONAL</option></select>
const PerioTopView = Array.from(Array(8), (x, index)=> Object.assign({},{id: index+1, AM: index+1, IN: 18-index})).concat(Array.from(Array(8), (x, index)=> Object.assign({},{id: index+9, AM: index+9, IN: 21+index})));
const PerioBottomView = Array.from(Array(8), (x, index)=> Object.assign({},{id: 32-index, AM: 32-index, IN: 48-index})).concat(Array.from(Array(8), (x, index)=> Object.assign({},{id: 24-index, AM: 24-index, IN: 31+index})));

const perioRows = [{rowPosition: 'top', perioType: 'Buccal'},{rowPosition: 'top', perioType: 'Palatal'},{rowPosition: 'bottom', perioType: 'Buccal'},{rowPosition: 'bottom', perioType: 'Lingual'}];
    

class PerioChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, loader: false, records: false, numberSystem: 'AM', editing : false, perioDate: moment(), dateOptions: [], showInfo: false };
    this.getResult = this.getResult.bind(this);
  }

  handleSubmit(data, dispatch, { form }) {
    this.setState({ loader: true })
    const record = data.toJS();
    record.dateSelected = moment(this.state.perioDate).format('YYYY-MM-DD');
    this.props.updateAction(record); 
    this.setState({editing:false});
    setTimeout(() => {
      loadPerioChartDates(this.props.patientId)
        .then(chartDates => {
          this.setState({ dateOptions: chartDates, loader: false })
        })
        .catch(() => this.setState({ dateOptions: [], loader: false }));
    }, 2000)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }
  componentDidMount() {
    this.setState({ loader: true })
    loadPerioChartDates(this.props.patientId)
      .then(chartDates => {
        this.setState({ dateOptions: chartDates })
        const selectedDate = chartDates.length > 0 ? chartDates[0].dateSelected : moment();
        this.setState({ perioDate: selectedDate })
        const date = moment(selectedDate).format('YYYY-MM-DD');
        this.props.loadAction(this.props.patientId, date);
        this.props.dispatch(change('perioForm', "dateSelected", date));
        setTimeout(() => this.setState({ loader: false }), 2000)
      })
      .catch(() => this.setState({ dateOptions: [], loader: false }));
  }

  render() {
    const { columns, records, toothChartData, loadAction, patientId,handleClose, metaData, dispatch } = this.props;
    const { numberSystem, editing, dateOptions, loader, showInfo } = this.state;
    const dateMetaOptions = dateOptions && dateOptions.length > 0 && dateOptions.map(el => Object.assign({}, { value: moment(el.dateSelected).format('MM/DD/YYYY'), label: moment(el.dateSelected).format('MM/DD/YYYY') })) || [];
    const dateField = [{ value: 'dateClicked', label: '', visible: false, changeStyle: true, onChange: true, editRecord: true, viewRecord: true, viewMode: false, noPad: true, type: 'select', oneOf: "dateMetaOptions" }, { value: 'dateSelected', label: '', visible: false, calendar: true, changeStyle: true, onChange: true, editRecord: true, viewRecord: true, viewMode: false, noPad: true, type: 'date' }]
    const result = this.getResult();

    const handleChange = (val, name) => {
      this.setState({ loader: true })
      const date =  moment(val).format('YYYY-MM-DD')
      this.setState({ perioDate: date });
      this.props.loadAction(this.props.patientId, date)
      if(name == 'dateClicked') {
        dispatch(change('perioForm', "dateSelected", date));
      }
      if(name == 'dateSelected') {
        dispatch(change('perioForm', "dateClicked", ""));
      }
      setTimeout(() => this.setState({ loader: false }), 2000)
    }

    return (
      <Col className={styles.PerioChart}>
      <div>
        <div className="periochart">
          <div className={styles.marginStyle}>
            <div className={styles.title}>
              <Col style={{ width: '50%' }}>
                <div style={{ display: 'flex' }}>
                  <span className={styles.titleStyle}> Perio Chart </span>
                  <img className={styles.infoStyle} src={require(`img/patients/info.svg`)} ref='foo' onClick={() => this.setState({ showInfo: true })} />
                  <PerioInfo url={"https://s3.amazonaws.com/static.novadonticsllc.com/PerioChart/info.html"} show={showInfo} hide={() => this.setState({ showInfo: false })} />
                </div>
              </Col>
              <Col className={styles.header}>
                <Icon glyph="icon-fontello-cancel" className={styles.closeIcon} onClick={handleClose} />
              </Col>
            </div>
          </div>
          <Row>
            <Col sm={4}>
              <div style={{ marginTop: '30px', marginLeft: '25px' }}>
                <NumberSystem onChange={(e)=>this.setState({numberSystem: e.target.value})}/>
              </div>
            </Col>
            <Col sm={7}>
              {dateField.map((field, i) => {
                const Component = ImplementationFor[field.type];
                const contentType = ContentTypes[field.type];
                return (
                    (field.editRecord ?
                      <div key={i} className="hidden-print">
                        <div className={styles.dateField}>
                          <Component
                            data={field}
                            contentType={contentType}
                            metaData={Object.assign({}, { ...metaData }, { dateMetaOptions })}
                            options={field.formFieldDecorationOptions}
                            optionsMode="edit"
                            inputchange={(val, name) => handleChange(val, name)}
                          />
                        </div>
                      </div> :
                        null
                    )
                );
              })}
            </Col>
          </Row>
          <Row>
            <div className={styles.abbdiv}>
              <span className={styles.abbcon}>PLI</span>: Plaque Index, <span className={styles.abbcon}>PPD</span>: Probing pocket depth, <span className={styles.abbcon}>MOB</span>: Mobility, <span className={styles.abbcon}>FUG</span>: Furcation Groove, <span className={styles.abbcon}>GRD</span>: Gingival Recession Defect, <span className={styles.abbcon}>BOP</span>: Bleeding On Probing, <span className={styles.abbcon}>SUP</span>: Suppuration, <span className={styles.abbcon}>MGJ</span>: Mucogingival Junction, <span className={styles.abbcon}>CAL</span>: Clinical Attachment Level, <span className={styles.abbcon}>RBL</span>: Radiographic Bone Loss, <span className={styles.abbcon}>PRG</span>: Prognosis of the tooth
            </div>
          </Row>
        </div>
        </div>
        {!loader ?
          <div className={styles.marginStyleLess}>
            <Row className={styles.marginzero}>
              <Row className={!editing ?styles.periodiv: editing.rowPosition =="top"? styles.periodiv:styles.periodiv}>
                <TopTable fields={columns} form={"perioChartFormTop"} initialValues={editing || {}} records={result && result.filter(_=>_.rowPosition === 'top') || []} view={PerioTopView} onSubmit={this.handleSubmit.bind(this)} mode="top" numberSystem={numberSystem} metaData={metaData} dispatch={dispatch} setRecord={this.setRecord.bind(this)} clearRecord={this.clearRecord.bind(this)} />
              </Row>
              <ToothView record={toothChartData} numberSystem={numberSystem} />
              <Row className={!editing ?styles.periodiv:editing.rowPosition =="bottom"?styles.periodiv:styles.periodiv}>
                <BottomTable fields={columns} form={"perioChartFormBottom"} initialValues={editing || {}} records={result && result.filter(_=>_.rowPosition === 'bottom') || []} view={PerioBottomView} onSubmit={this.handleSubmit.bind(this)} mode="bottom" numberSystem={numberSystem} metaData={metaData} dispatch={dispatch} setRecord={this.setRecord.bind(this)} clearRecord={this.clearRecord.bind(this)} />
              </Row>
            </Row>
          </div> : <Spinner />}
    </Col>
      );
  }

  open() {
    this.setState({ showModal: true });
  }

  close() {
    this.setState({ showModal: false });
  }

  setRecord(record) {
    if(!this.props.disable)
      this.setState({editing:record});
  }

  clearRecord(record){
    var empty=['','','','','','','','','','','','','','','',''];
    record.perioNumbers = this.updatenewformat(empty);
    record.dateSelected = '';
    this.setState({editing:record});
  }

  getResult() {
    const { records, patientId } = this.props;
    const result = [];
    var empty=['','','','','','','','','','','','','','','',''];
    var emptydata =this.updatenewformat(empty)
    perioRows.map(r => {
      const perioMap = records && records.filter(_=>_.rowPosition === r.rowPosition && _.perioType === r.perioType) || [];
      if(perioMap.length == 0){
        result.push(Object.assign({}, r, {staticId:1,patientId: patientId , perioNumbers: emptydata, dateSelected: ''}));
      } else if(perioMap.length == 1){
        var perio1=perioMap[0];
        result.push(Object.assign({}, r,  {staticId:perio1.staticId,patientId:perio1.patientId,perioNumbers:this.updatenewformat(perio1.perioNumbers),dateSelected:perio1.dateSelected,rowPosition:perio1.rowPosition,perioType:perio1.perioType,id:perio1.id,userId:perio1.userId,providerId:perio1.providerId}));
      } else {
        // for(var x in perioMap)
        // {
        //   var perio1=perioMap[x];
        //   result.push(Object.assign({}, r, {staticId:perio1.staticId,patientId:perio1.patientId,perioNumbers:this.updatenewformat(perio1.perioNumbers),dateSelected:perio1.dateSelected,rowPosition:perio1.rowPosition,perioType:perio1.perioType}));
        // }
        var perio1=perioMap[0]; var perio2=perioMap[1];
        result.push(Object.assign({}, r, {staticId:perio1.staticId,patientId:perio1.patientId,perioNumbers:this.updatenewformat(perio1.perioNumbers),dateSelected:perio1.dateSelected,rowPosition:perio1.rowPosition,perioType:perio1.perioType,id:perio1.id,userId:perio1.userId,providerId:perio1.providerId}));
      }
    });

    return result;
  }
  updatenewformat(data)
  {
    var newformat=[];
    for(var x in data)
    {
      if(typeof data[x] !="string")
      {
        newformat.push(data[x])
      }
      else{
        var newdata={
          PN:data[x],
          B:"",
          R:"",
          F:"",
          M:"",
          PQ:"",
          BL:"",
          S:"",
        }
        newformat.push(newdata)
      }
    }
    return newformat;
  }
}


const ToothView = ({numberSystem, record}) => {
  return <Col>
      <Col className={styles.view}>
        {PerioTopView.map((tooth,index) => {
          let missingTooth = record && record.missingTooth && record.missingTooth.split(',').map(x=> +x);
          let primaryTooth = record && record.primaryTooth && record.primaryTooth.split(',').map(x=> +x);
          const MT = missingTooth && missingTooth.includes(tooth.id);
          const DT = primaryTooth && primaryTooth.includes(tooth.id);
          const maxilla = record && record.edentulousMaxilla;
          const mandible = record && record.edentulousMandible;
          const allPrimary = record && record.allPrimary;
          return <span key={index}>
            <img src={require(`img/toothchart/side-view${maxilla || MT ? `-empty` : allPrimary || DT ? `-baby` : ``}${tooth.id}.png`)} />
            {tooth[numberSystem]}
          </span>
        })}
      </Col>
      <Col>&nbsp;</Col>
      <Col className={styles.view}>
        {PerioBottomView.map((tooth,index) => {
          let missingTooth = record && record.missingTooth && record.missingTooth.split(',').map(x=> +x);
          let primaryTooth = record && record.primaryTooth && record.primaryTooth.split(',').map(x=> +x);
          const MT = missingTooth && missingTooth.includes(tooth.id);
          const DT = primaryTooth && primaryTooth.includes(tooth.id);
          const maxilla = record && record.edentulousMaxilla;
          const mandible = record && record.edentulousMandible;
          const allPrimary = record && record.allPrimary;
          return <span key={index}>
            {tooth[numberSystem]}
            <img src={require(`img/toothchart/side-view${mandible || MT ? `-empty` : allPrimary || DT ? `-baby` : ``}${tooth.id}.png`)} />
          </span>
        })}
      </Col>
  </Col>
}


PerioChart.propTypes = {
  patientId: PropTypes.number,
  columns: PropTypes.array,
  records: PropTypes.array,
  toothChartData: PropTypes.array,
};

export default compose(
  connect(),
  withProps(({records, dispatch, error}) => ({
    loadAction: (id, date) => dispatch(loadPerioChart(id, date)),
    updateAction: (record) => dispatch(updatePerioChart(record)),
    records: records,
    error : error
  })),
  reduxForm({
    form: 'perioForm',
    enableReinitialize: true,
  })
)(PerioChart);