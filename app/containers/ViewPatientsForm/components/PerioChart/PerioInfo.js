/**
*
* PerioInfo
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from '@sketchpixy/rubix';


class PerioInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { url, show, hide, primary } = this.props;
     return (
        <Modal show={show} onHide={hide} className="perioInfo">
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <iframe src={url} width="100%" height="100%" style={primary ? { minHeight: "500px", border: "none" } : { minHeight: "650px", border: "none" }} />
          </Modal.Body>
        </Modal>
    );
  }

}

PerioInfo.propTypes = {
  url: PropTypes.string,
  show: PropTypes.bool,
  hide: PropTypes.func,
};

export default PerioInfo;
