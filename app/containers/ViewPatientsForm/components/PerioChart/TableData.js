/**
*
* TableData
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Table, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import moment from 'moment';
import styles from './styles.css';
import { Field } from 'redux-form/immutable';


class TableData extends React.Component {
    constructor(props) {
        super(props); 
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }
    textformat(data,name)
    {
        name = name.replace("perioNumbers[","");
        name = name.replace("]","");
        data =data[name];
        var text="";
        if(data !=undefined)
        {
            if(data.B =="Yes") {
              if(data.F !=undefined && data.F !="" || data.R !=undefined && data.R !="" || data.M !=undefined && data.M !="" || data.PQ !=undefined && data.PQ !="" || data.BL !=undefined && data.BL !="" || data.S !=undefined && data.S !="" || data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text=""
              } else {
                text=""
              }
            }
            if(data.F !=undefined && data.F !="") {
              if(data.R !=undefined && data.R !="" || data.M !=undefined && data.M !="" || data.PQ !=undefined && data.PQ !="" || data.BL !=undefined && data.BL !="" || data.S !=undefined && data.S !="" && data.S !="Yes"&& data.S !="No" || data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text+=" F"+data.F+" /"
              } else {
                text+=" F"+data.F
              }
            }
            if(data.R !=undefined && data.R !="") {
              if(data.M !=undefined && data.M !="" || data.PQ !=undefined && data.PQ !="" || data.BL !=undefined && data.BL !="" || data.S !=undefined && data.S !="" && data.S !="Yes"&& data.S !="No" || data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text+=" R"+data.R+" /"
              } else {
                text+=" R"+data.R
              }
            }
            if(data.M !=undefined && data.M !="") {
              if(data.PQ !=undefined && data.PQ !="" || data.BL !=undefined && data.BL !="" || data.S !=undefined && data.S !="" && data.S !="Yes"&& data.S !="No" || data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text+=" M"+data.M+" /"
              } else {
                text+=" M"+data.M
              }
            }
            if(data.PQ !=undefined && data.PQ !="") {
              if(data.BL !=undefined && data.BL !="" || data.S !=undefined && data.S !="" && data.S !="Yes"&& data.S !="No" || data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text+=" PQ: "+data.PQ.charAt(0)+" /"
              } else {
                text+=" PQ: "+data.PQ.charAt(0)
              }
            }
            if(data.BL !=undefined && data.BL !="") {
              if(data.S !=undefined && data.S !="" && data.S !="Yes"&& data.S !="No" || data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text+=" BL: "+data.BL+" /"
              } else {
                text+=" BL: "+data.BL
              }
            }
            if(data.S !=undefined && data.S !="") {
              if(data.J !=undefined && data.J !="" || data.C !=undefined && data.C !="") {
                text+=""
              } else {
                text+=""
              }
            }
            if(data.J !=undefined && data.J !="") {
              if(data.C !=undefined && data.C !="") {
                text+=" J"+data.J+" /"
              } else {
                text+=" J"+data.J
              }
            }
            if(data.PR !=undefined && data.PR !="") {
              let pValue = data.PN !=undefined && data.PN !="" ? parseInt(data.PN) : 0
              let rValue = data.R !=undefined && data.R !="" ? parseInt(data.R) : 0
              let cValue = pValue + rValue
              text+= cValue != 0 ?  " C"+cValue+" /" : ""
            } else {
              let pValue = data.PN !=undefined && data.PN !="" ? parseInt(data.PN) : 0
              let rValue = data.R !=undefined && data.R !="" ? parseInt(data.R) : 0
              let cValue = pValue + rValue
              text+= cValue != 0 ?  " C"+cValue : ""
            }
            if(data.PR !=undefined && data.PR !="") {
                text+=" PR: "+data.PR.charAt(0)
            }
            if(text =="") {
                text = data.PN;
            }
        }
        return text;
    }
    textformatting(data,name)
    {
        name = name.replace("perioNumbers[","");
        name = name.replace("]","");
        data =data[name];
        let singleZero = "0"
        let doubleZero = "00"
        let pValue = data.PN !=undefined && data.PN !="" ? parseInt(data.PN) : 0
        let pStringValue = pValue == 0 ? "000" : pValue.toString()
        let pFinalValue = pStringValue.length == 3 ? pStringValue : pStringValue.length == 2 ? singleZero.concat(pStringValue) : doubleZero.concat(pStringValue)
        let p1Value = pFinalValue.charAt(0);
        let p2Value = pFinalValue.charAt(1);
        let p3Value = pFinalValue.charAt(2);
        let rValue = data.R !=undefined && data.R !="" ? parseInt(data.R) : 0
        let rStringValue = rValue == 0 ? "000" : rValue.toString()
        let rFinalValue = rStringValue.length == 3 ? rStringValue : rStringValue.length == 2 ? singleZero.concat(rStringValue) : doubleZero.concat(rStringValue)
        let r1Value = rFinalValue.charAt(0);
        let r2Value = rFinalValue.charAt(1);
        let r3Value = rFinalValue.charAt(2);
        let c1Value = parseInt(p1Value) + parseInt(r1Value)
        let c2Value = parseInt(p2Value) + parseInt(r2Value)
        let c3Value = parseInt(p3Value) + parseInt(r3Value)
        let c1FinalValue = c1Value > 9 ? "9" : c1Value.toString()
        let c2FinalValue = c2Value > 9 ? "9" : c2Value.toString()
        let c3FinalValue = c3Value > 9 ? "9" : c3Value.toString()
        return <div style={{ fontWeight: '400' }}>
                  <span>{data.PQ}</span><br />
                  {
                    p1Value == "0" && p2Value == "0" && p3Value == "0" ?
                    <span><br /></span> :
                    <span>
                      {parseInt(p1Value) > 3 && parseInt(p1Value) < 9 ?
                          <span style={{ color: 'red' }}>{p1Value}</span> :
                        parseInt(p1Value) >= 9 ?
                          <span style={{ color: 'blue' }}>{p1Value}</span> :
                        <span>{p1Value}</span>}
                      {parseInt(p2Value) > 3 && parseInt(p2Value) < 9 ?
                          <span style={{ color: 'red' }}>{p2Value}</span> :
                        parseInt(p2Value) >= 9 ?
                          <span style={{ color: 'blue' }}>{p2Value}</span> :
                        <span>{p2Value}</span>}
                      {parseInt(p3Value) > 3 && parseInt(p3Value) < 9 ?
                          <span style={{ color: 'red' }}>{p3Value}</span> :
                        parseInt(p3Value) >= 9 ?
                          <span style={{ color: 'blue' }}>{p3Value}</span> :
                        <span>{p3Value}</span>}
                        <br />
                    </span>
                  }
                  <span>{data.M}</span><br />
                  <span>{data.F}</span><br />
                  {
                    r1Value == "0" && r2Value == "0" && r3Value == "0" ?
                    <span><br /></span> :
                    <span>
                      {parseInt(r1Value) > 0 && parseInt(r1Value) < 9 ?
                          <span style={{ color: 'red' }}>{r1Value}</span> :
                        parseInt(r1Value) >= 9 ?
                          <span style={{ color: 'blue' }}>{r1Value}</span> :
                        <span>{r1Value}</span>}
                      {parseInt(r2Value) > 0 && parseInt(r2Value) < 9 ?
                          <span style={{ color: 'red' }}>{r2Value}</span> :
                        parseInt(r2Value) >= 9 ?
                          <span style={{ color: 'blue' }}>{r2Value}</span> :
                        <span>{r2Value}</span>}
                      {parseInt(r3Value) > 0 && parseInt(r3Value) < 9 ?
                          <span style={{ color: 'red' }}>{r3Value}</span> :
                        parseInt(r3Value) >= 9 ?
                          <span style={{ color: 'blue' }}>{r3Value}</span> :
                        <span>{r3Value}</span>}
                        <br />
                    </span>
                  }
                  <span>{data.B}</span><br />
                  <span>{data.S}</span><br />
                  <span>{data.J}</span><br />
                  {
                    c1FinalValue == "0" && c2FinalValue == "0" && c3FinalValue == "0" ?
                    <span><br /></span> :
                    <span>
                      {parseInt(c1FinalValue) > 3 && parseInt(c1FinalValue) < 9 ?
                          <span style={{ color: 'red' }}>{c1FinalValue}</span> :
                        parseInt(c1FinalValue) >= 9 ?
                          <span style={{ color: 'blue' }}>{c1FinalValue}</span> :
                        <span>{c1FinalValue}</span>}
                      {parseInt(c2FinalValue) > 3 && parseInt(c2FinalValue) < 9 ?
                          <span style={{ color: 'red' }}>{c2FinalValue}</span> :
                        parseInt(c2FinalValue) >= 9 ?
                          <span style={{ color: 'blue' }}>{c2FinalValue}</span> :
                        <span>{c2FinalValue}</span>}
                      {parseInt(c3FinalValue) > 3 && parseInt(c3FinalValue) < 9 ?
                          <span style={{ color: 'red' }}>{c3FinalValue}</span> :
                        parseInt(c3FinalValue) >= 9 ?
                          <span style={{ color: 'blue' }}>{c3FinalValue}</span> :
                        <span>{c3FinalValue}</span>}
                        <br />
                    </span>
                  }
                  <span>{data.BL}</span><br />
                  <span>{data.PR}</span><br />
               </div>;
    }
    textPN(data,name)
    {
        name = name.replace("perioNumbers[","");
        name = name.replace("]","");
        data =data[name];
        var text="";

        if(data !=undefined)
        {
            if(data.PN !=undefined && data.PN !="")
            {
                text = data.PN;
            }
            if(data.B !="Yes" && (data.F == undefined || data.F == "") && (data.R == undefined || data.R == "") && (data.M == undefined || data.M == "") && (data.PQ == undefined || data.PQ == "") && (data.BL == undefined || data.BL == "") && (data.S == undefined || data.S == "") && (data.J == undefined || data.J == "") && (data.C == undefined || data.C == "") )
            {
                text=""
            }
        }
        return text;
    }

    checkbexist(data,name) {
        var exist=false;
        if(name.indexOf("perioNumbers") > -1)
        {
            name = name.replace("perioNumbers[","");
            name = name.replace("]","");
            data =data[name];
            var exist=false;
            if(data !=undefined)
            {
                if(data.B =="Yes")
                {
                    exist=true;
                }
            }
        }
        return exist;
    }

    checkSExist(data,name) {
        var exist=false;
        if(name.indexOf("perioNumbers") > -1)
        {
            name = name.replace("perioNumbers[","");
            name = name.replace("]","");
            data =data[name];
            var exist=false;
            if(data !=undefined)
            {
                if(data.S == "Yes")
                {
                    exist=true;
                }
            }
        }
        return exist;
    }

    render() {
        const {  records, view, mode, numberSystem, fields, handleSubmit, error, updateError, pristine, submitting, setRecord, clearRecord, initialValues, metaData, form, dispatch } = this.props;
        const selected = initialValues && initialValues.toJS() || false;
        const editth =  Object.keys(selected).length>0 && selected.rowPosition == mode;

        const handleChange = (val, name) => {
          let inputName = name;
          if(name.indexOf('PN') > -1) {
            let fieldRName = inputName.replace("PN", "R")
            let fieldCName = inputName.replace("PN", "C")
            let singleZero = "0"
            let doubleZero = "00"
            let pValue = val
            let pStringValue = pValue == 0 ? "000" : pValue.toString()
            let pFinalValue = pStringValue.length == 3 ? pStringValue : pStringValue.length == 2 ? singleZero.concat(pStringValue) : doubleZero.concat(pStringValue)
            let p1Value = pFinalValue.charAt(0);
            let p2Value = pFinalValue.charAt(1);
            let p3Value = pFinalValue.charAt(2);
            let rValue = document.getElementsByName(fieldRName)[0].value != "" ? document.getElementsByName(fieldRName)[0].value : 0;
            let rStringValue = rValue == 0 ? "000" : rValue.toString()
            let rFinalValue = rStringValue.length == 3 ? rStringValue : rStringValue.length == 2 ? singleZero.concat(rStringValue) : doubleZero.concat(rStringValue)
            let r1Value = rFinalValue.charAt(0);
            let r2Value = rFinalValue.charAt(1);
            let r3Value = rFinalValue.charAt(2);
            let c1Value = parseInt(p1Value) + parseInt(r1Value)
            let c2Value = parseInt(p2Value) + parseInt(r2Value)
            let c3Value = parseInt(p3Value) + parseInt(r3Value)
            let c1FinalValue = c1Value > 9 ? "9" : c1Value.toString()
            let c2FinalValue = c2Value > 9 ? "9" : c2Value.toString()
            let c3FinalValue = c3Value > 9 ? "9" : c3Value.toString()
            let cValue = (c1FinalValue.concat(c2FinalValue)).concat(c3FinalValue)
            dispatch(change(form, fieldCName, cValue.toString()));
          }
          if(name.indexOf('R') > -1) {
            let fieldPName = inputName.replace("R", "PN")
            let fieldCName = inputName.replace("R", "C")
            let singleZero = "0"
            let doubleZero = "00"
            let pValue = document.getElementsByName(fieldPName)[0].value != "" ? document.getElementsByName(fieldPName)[0].value : 0;
            let pStringValue = pValue == 0 ? "000" : pValue.toString()
            let pFinalValue = pStringValue.length == 3 ? pStringValue : pStringValue.length == 2 ? singleZero.concat(pStringValue) : doubleZero.concat(pStringValue)
            let p1Value = pFinalValue.charAt(0);
            let p2Value = pFinalValue.charAt(1);
            let p3Value = pFinalValue.charAt(2);
            let rValue = val
            let rStringValue = rValue == 0 ? "000" : rValue.toString()
            let rFinalValue = rStringValue.length == 3 ? rStringValue : rStringValue.length == 2 ? singleZero.concat(rStringValue) : doubleZero.concat(rStringValue)
            let r1Value = rFinalValue.charAt(0);
            let r2Value = rFinalValue.charAt(1);
            let r3Value = rFinalValue.charAt(2);
            let c1Value = parseInt(p1Value) + parseInt(r1Value)
            let c2Value = parseInt(p2Value) + parseInt(r2Value)
            let c3Value = parseInt(p3Value) + parseInt(r3Value)
            let c1FinalValue = c1Value > 9 ? "9" : c1Value.toString()
            let c2FinalValue = c2Value > 9 ? "9" : c2Value.toString()
            let c3FinalValue = c3Value > 9 ? "9" : c3Value.toString()
            let cValue = (c1FinalValue.concat(c2FinalValue)).concat(c3FinalValue)
            dispatch(change(form, fieldCName, cValue.toString()));
          }
        }

        return (
            <form onSubmit={handleSubmit}>
                <Table className={`${styles.Table} periochartForm`} condensed responsive>
                    <thead style={{ backgroundColor: '#EA6225', color: '#FFF' }}>
                        <tr>
                            <th style={{ position: 'sticky', left: '0', zIndex: '2', background: '#EA6225', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>Provider</th>
                            <th style={{ position: 'sticky', left: '160px', zIndex: '2', background: '#EA6225', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>{mode === 'top' ? 'Maxilla' : 'Mandible'}</th>
                            <th style={{ position: 'sticky', left: '255px', zIndex: '2', background: '#EA6225', outline: '1px solid #8080807a', outlineOffset: '-1px' }}></th>
                            {view.map((tooth,index)=><th key={index}>{tooth[numberSystem]}</th>)}
                            {editth ?<th className={styles.stickycol}></th>:<th className={styles.stickycol}></th> }
                        </tr>
                    </thead>
                    <tbody>
                        {records.map((record,index)=> {
                            const edit =  JSON.stringify(record) == JSON.stringify(selected) || (selected.id && record.id === selected.id);
                            return <tr key={index}>
                                {fields.map((field, i) => {
                                    const Component = ImplementationFor[field.type];
                                    const contentType = ContentTypes[field.type];
                                    const ComponentR = ImplementationFor[field.typeR];
                                    const contentTypeR = ContentTypes[field.typeR];
                                    const ComponentF = ImplementationFor[field.typeF];
                                    const contentTypeF = ContentTypes[field.typeF];
                                    return (
                                        (field.editRecord && edit && field.label == "Perio Numbers" ?
                                        <td key={i} className="hidden-print">
                                            <div className={styles.multic}>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.PQ}
                                                        contentType={contentTypeF}
                                                        perioStyle={true}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <Component
                                                    data={field.PN}
                                                    contentType={contentType}
                                                    options={field.formFieldDecorationOptions}
                                                    inputchange={(val, value) => handleChange(val, value)}
                                                    optionsMode="edit"
                                                    inline
                                                />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.M}
                                                        contentType={contentTypeF}
                                                        perioStyle={true}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.F}
                                                        contentType={contentTypeF}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentR
                                                        data={field.R}
                                                        contentType={contentTypeR}
                                                        perioStyle={true}
                                                        options={field.formFieldDecorationOptions}
                                                        inputchange={(val, value) => handleChange(val, value)}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.B}
                                                        contentType={contentTypeF}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.S}
                                                        contentType={contentTypeF}
                                                        perioStyle={true}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentR
                                                        data={field.J}
                                                        contentType={contentTypeR}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentR
                                                        data={field.C}
                                                        contentType={contentTypeR}
                                                        perioStyle={true}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.BL}
                                                        contentType={contentTypeF}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                                <Col xs={12} className={styles.perioCenter}>
                                                    <span className={styles.lable}></span>
                                                    <ComponentF
                                                        data={field.PR}
                                                        perioStyle={true}
                                                        contentType={contentTypeF}
                                                        options={field.formFieldDecorationOptions}
                                                        optionsMode="edit"
                                                        inline
                                                    />
                                                </Col>
                                            </div>
                                          </td>
                                  : field.editRecord && edit && field.label == "Provider" ?
                                    <td key={i} className="hidden-print" style={{ position: 'sticky', left: '0', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                                            <div className={styles.singlec}>
                                            <Component
                                                data={field}
                                                contentType={contentType}
                                                metaData={metaData}
                                                options={field.formFieldDecorationOptions}
                                                optionsMode="edit"
                                                inline
                                            /></div>
                                        </td>
                                        : field.label == 'Perio Label' && field.viewRecord ?
                                        <td key={i} className="hidden-print" onClick={()=> setRecord(record)} style={{ position: 'sticky', left: '255px', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                                        <div className={styles.multic1}>
                                         {
                                          edit ?
                                          <div style={{ fontWeight: '700' }}>
                                            <div style={{ marginTop: '10px' }}><span>PLI</span></div>
                                            <div style={{ marginTop: '15px' }}><span>PPD</span></div>
                                            <div style={{ marginTop: '15px' }}><span>MOB</span></div>
                                            <div style={{ marginTop: '15px' }}><span>FUG</span></div>
                                            <div style={{ marginTop: '15px' }}><span>GRD</span></div>
                                            <div style={{ marginTop: '20px' }}><span>BOP</span></div>
                                            <div style={{ marginTop: '20px' }}><span>SUP</span></div>
                                            <div style={{ marginTop: '23px' }}><span>MGJ</span></div>
                                            <div style={{ marginTop: '15px' }}><span>CAL</span></div>
                                            <div style={{ marginTop: '15px' }}><span>RBL</span></div>
                                            <div style={{ marginTop: '15px' }}><span>PRG</span></div>
                                          </div> :
                                        field.label == 'Perio Label' && !edit ?
                                          <div style={{ fontWeight: '700' }}>
                                            <span>PLI<br />PPD<br />MOB<br />FUG<br />GRD<br />BOP<br />SUP<br />MGJ<br />CAL<br />RBL<br />PRG<br /></span>
                                          </div> : null
                                        }
                                        </div>
                                      </td>
                                  : field.label == 'Perio Numbers' && field.viewRecord ?
                                      <td key={i} className="hidden-print" onClick={()=> setRecord(record)}>
                                        <div className={styles.multic1} style={{ fontWeight: '700' }}>
                                          {
                                            typeof record[field.value] !="string" ?
                                              this.textformatting(record["perioNumbers"], field.value) : ''
                                          }
                                        </div>
                                      </td>
                                  : field.label == 'Provider' && field.viewRecord ?
                                      <td key={i} className="hidden-print" onClick={()=> setRecord(record)} style={{ position: 'sticky', left: '0', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                                        <div className={styles.multic1Provider} style={{ fontWeight: '700' }}>
                                          {
                                            record[field.value] != '' && metaData['providers'].find(e => e.value == record[field.value]) ? metaData['providers'].find(e => e.value == record[field.value]).label : ''
                                          }
                                        </div>
                                      </td>
                                  : field.label == 'Perio Type' && field.viewRecord ?
                                      <td key={i} className="hidden-print" onClick={()=> setRecord(record)} style={{ position: 'sticky', left: '160px', zIndex: '2', background: 'white', outline: '1px solid #8080807a', outlineOffset: '-1px' }}>
                                        <div className={styles.multic1Buccal} style={{ fontWeight: '700' }}>
                                          {record[field.value]}
                                        </div>
                                      </td> :
                                        null
                                        )
                                    );
                                })}
                                {edit ? <td style={{width:'2%'}} className={styles.stickycolbig}>
                                     <Col>
                                    <Button type="submit" bsStyle="link" bsSize="xs" className={styles.btn}><Icon glyph="icon-fontello-floppy" disabled={pristine || submitting} className={submitting ? `${styles.icon}buttonLoading` : `${styles.icon}`} />Save</Button>
                                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={()=> setRecord({})}><Icon glyph="icon-fontello-cancel-circle" className={styles.icon} />Cancel</Button>
                                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={()=> clearRecord(record)}><Icon glyph="icon-fontello-cancel" className={styles.icon} />Clear</Button></Col>
                                    </td>:
                                 editth ?
                                <td style={{width:'2%'}} className={styles.stickycol2}>
                                    <Button bsSize="xs" bsStyle="link" className={styles.btn} onClick={()=> setRecord(record)}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>
                                </td>:
                                <td style={{width:'2%'}} className={styles.stickycol1} >
                                    <Button bsSize="xs" bsStyle="link" className={styles.btn} onClick={()=> setRecord(record)}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>
                                </td>}
                            </tr>
                            }
                        )}    
                    </tbody>
                </Table>
            </form>
        );
    }
}


TableData.propTypes = {
    records: PropTypes.array,
    view: PropTypes.array,
    numberSystem: PropTypes.string,
    fields: PropTypes.array,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    updateError: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,  
};

export default reduxForm({
    form: 'perioChartForm',
    enableReinitialize: true,
  })(TableData);

