/**
*
* RequestConsultationForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm } from 'redux-form/immutable';
import { Col, Row, Modal, Button, Alert } from '@sketchpixy/rubix';
import {ImplementationFor} from '../../../PatientsPage/components/CreatePatientForm/utils';
import styles from './styles.css';
import schema from 'routes/schema';
import { authorizeApi } from 'blocks/products/remotes.js';
import PaymentForm from "./PaymentForm";

class RequestConsultationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, activeCard: 0, defaultCard: 0, activePayment: [], closeModal: false, disableButton: true, consultPrice: 290.00 };
    this.closeConsult = this.closeConsult.bind(this);
    this.openConsult = this.openConsult.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  closeConsult = () => {
    this.setState({ closeModal: true});
  }

  openConsult = () => {
    this.setState({ closeModal: false});
  }

  render() {
    const { fields, optionsMode, patientName, handlePaymentDetails, handleSubmit, error, pristine, submitting, authDetails, cardDetails, activeCard, activePayment, defaultCard , user, loadAuthorizeDetails } = this.props;
    const { closeModal, consultPrice, disableButton } = this.state;

    const handleChange = (val, name) => {
      if(name == 'chooseConsultArea') {
        let consultValue = document.getElementsByName("overThePhone.checked")[0].value
        if(consultValue != '' && val != '') {
          this.setState({ disableButton: false })
        } else {
          this.setState({ disableButton: true })
        }
      }
      if(name == 'comment') {
        let consultArea = document.getElementsByName("chooseConsultArea")[0].value
        let consultValue = document.getElementsByName("overThePhone.checked")[0].value
        if(consultArea != '' && consultValue != '') {
          this.setState({ disableButton: false })
        } else {
          this.setState({ disableButton: true })
        }
      }
      if(name == 'overThePhone') {
        let consultArea = document.getElementsByName("chooseConsultArea")[0].value
        if(consultArea != '' && val == '') {
          this.setState({ disableButton: false })
        } else {
          this.setState({ disableButton: true })
        }
      }
    }

    return (
      <Col className={optionsMode == 'header' ? styles.RequestConsultationForm : null}>
        {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
        <Modal id ="requestConsultationModal" show={this.state.showModal}backdrop="static" onHide={this.close.bind(this)} className={closeModal ? styles.hideModal : null}>
          <Modal.Header closeButton>
            <Modal.Title>
              <div className={styles.title}>Request Consultation for Patient {patientName}</div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row className={styles.rowStandard}>
              {fields.map((field, i) => {
                const Component = ImplementationFor[field.type];
                return (
                  <div key={i} className="hidden-print">
                    <Component
                       data={field}
                       options={field.formFieldDecorationOptions}
                       optionsMode="edit"
                       inputchange={(val, name) => handleChange(val, name)}
                    />
                  </div>
                );
              })}
            </Row>
            <Row>
              <PaymentForm
                user={user}
                schema={schema().products()}
                initialValues = {{amount: 390.00}}
                patientName={patientName}
                closeConsult={this.closeConsult}
                openConsult={this.openConsult}
                handlePaymentDetails={handlePaymentDetails}
                handleSubmit={handleSubmit}
                cardDetails={cardDetails}
                authDetails={authDetails}
                activeCard={activeCard}
                defaultCard={defaultCard}
                activePayment={activePayment}
              >
                {(open) => <Button bsStyle='standard' type="submit" onClick={open.bind(this, this.closeConsult)} disabled={this.state.disableButton} className={submitting ? `${styles.submitbtn} buttonLoading` : styles.submitbtn}>Proceed to Pay</Button>}
              </PaymentForm>
            </Row>
          </Modal.Body>
          {error ?
            <Alert danger>{error}</Alert> : null}
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
    this.props.loadAuthorizeDetails();
  }

  open() {
  	this.setState({ showModal: true })
  }

}

RequestConsultationForm.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  patientName: PropTypes.string,
};

export default reduxForm({
  form: 'requestConsultation',
})(RequestConsultationForm);
