/**
 * 
 * TreatmentPlanDetails
 * 
 */

import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, Field } from 'redux-form/immutable';
import Spinner from 'components/Spinner';
import styles from './styles.css';
import ReactTable from "react-table";
import ConfirmButton from 'components/ConfirmButton';
import { connect } from 'react-redux';
import EditRecordForm from 'components/EditRecordForm';
import ModernDatePicker from 'components/ModernDatePicker';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import AutocompleteField from './AutocompleteField';
import moment from 'moment';
import ShowText from 'components/ShowText';
import Alert from 'components/Alert';
import { getFeeDetails, getInsuranceFeeDetails } from 'blocks/patients/treatmentPlan';
import Info from 'containers/PatientsPage/components/Info';
import infoSchema from 'routes/schema';

const fieldComponent = ({ input, type, data, metaData, patientId }) => {

    const isPreDefinedSet = Array.isArray(data.oneOf);

    const handleChange = (val) => {
      const cdcCode = document.getElementsByName('cdcCode')[0].value;
      const insuranceId = document.getElementsByName('insurance')[0].value;
      if(insuranceId != '') {
        var submitRecord = {}
        submitRecord.patientId = patientId;
        submitRecord.procedureCode = cdcCode;
        submitRecord.insuranceCompanyId = insuranceId;
        getInsuranceFeeDetails(submitRecord)
          .then(feeValue => {
            const fee = document.getElementsByName('fee')[0];
            fee.value = feeValue.fee;
            const insuranceCompanyId = document.getElementsByName('insurance')[0];
            insuranceCompanyId.value = parseInt(feeValue.insuranceCompanyId);
            const estIns = document.getElementsByName('estIns')[0].value;
            const balance = document.getElementsByName('disc')[0];
            const total = fee.value && estIns && parseFloat(fee.value) - parseFloat(estIns) || 0;
            balance.value = total
          })
      } else {
        var submitRecord = {}
        submitRecord.patientId = patientId;
        submitRecord.procedureCode = cdcCode;
        getFeeDetails(submitRecord)
          .then(feeValue => {
            const fee = document.getElementsByName('fee')[0];
            fee.value = feeValue.fee;
            const insuranceCompanyId = document.getElementsByName('insurance')[0];
            insuranceCompanyId.value = parseInt(feeValue.insuranceCompanyId);
            const estIns = document.getElementsByName('estIns')[0].value;
            const balance = document.getElementsByName('disc')[0];
            const total = fee.value && estIns && parseFloat(fee.value) - parseFloat(estIns) || 0;
            balance.value = total
          })
      }
    }

    return data.search && type === 'select' ? <AutocompleteField data={data} inline={true} input={input} metaData={metaData} inputChange={(val) => {handleChange(val)}} /> :
        type === 'select' ?
            <select name={input.name} className={styles.InputField} defaultValue={input.value} onBlur={e => input.onChange(data.number ? parseFloat(e.target.value) : e.target.value)} required={data.required}>
                <option value="" disabled={true}>Please select</option>
                {isPreDefinedSet ?
                    (data.oneOf || []).map((option) =>
                        <option key={option} value={option}>{option}</option>) :
                    (metaData[data.oneOf] || []).map((option) =>
                        <option key={option.value} value={option.value}>{option.label}</option>)}
            </select> : type === 'date' ?
                <Field name={input.name} id={input.name} className={styles.InputField} data={data} component={ModernDatePicker} clearable  /> :
                <input name={input.name} className={styles.InputField} type={type} defaultValue={input.value} onBlur={e => input.onChange(type === 'text' ? e.target.value : parseFloat(e.target.value))} required={data.required} />;
};


class TreatmentPlanDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: true };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    editableColumnProps = {
        Cell: props => {
            const { column, tdProps } = props;
            const { onOpen, onEdit, onCancel, onDelete, edit, onChange, metaData } = tdProps.rest;
            const { create, disableEdit } = props.original;
            const { disable, patientId } = this.props;
           
            return (column.action && !create && !edit && !disableEdit ?
                <Col className={styles.actions}>
                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onEdit}><Icon glyph="icon-fontello-edit" className={styles.icon} /></Button>
                    <ConfirmButton onConfirm={!disable ? onDelete : null}>{(click) => <Button bsStyle="link" bsSize="xs" disabled={disable} className={styles.btn} onClick={() => click()}><Icon glyph="icon-fontello-trash" className={styles.icon} /></Button>}</ConfirmButton>
                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onOpen}><Icon glyph="icon-fontello-calendar" className={styles.icon} /></Button>
                </Col> : column.action && !create && edit && !disableEdit ?
                    <Col className={styles.actions}>
                        <Button bsStyle="link" bsSize="xs" disabled={disable} type="submit" className={styles.btn}><Icon glyph="icon-fontello-floppy" className={styles.icon} />Save</Button>
                        <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onCancel}><Icon glyph="icon-fontello-cancel" className={styles.icon} />Cancel</Button>
                    </Col> : column.action && create && !this.props.editing ?
                        <Button bsStyle="link" bsSize="xs" disabled={disable} type="submit" className={styles.btn}><Icon glyph="icon-fontello-plus-circled" className={styles.icon} />Add</Button> :
                        (create && !this.props.editing && column.editRecord) || (edit && column.editRecord) ?
                        <Field name={column.value == "providerName" ? "providerId" : column.value} id={column.label} component={fieldComponent} type={column.type} metaData={metaData} data={column} onChange={onChange} patientId={patientId} /> :
                            <span style={{ display: 'flex' }}>
                                {column.serial && !create && !disableEdit ? props.index + 1 :
                                    column.currency && (props.value || props.value === 0) ?
                                        currencyFormatter.format(props.value) :
                                        column.type === 'date' && props.value ?
                                            moment(props.value).format('MM/DD/YYYY') :
                                            column.limit && props.value && props.value.length > column.limit ?
                                                props.value.substring(0, column.limit) + '...' :
                                                column.resourceColumn && props.value ?
                                                    metaData && metaData['providers'] && metaData['providers'].filter(_=>_.id === parseInt(props.value)).length != 0 ? metaData['providers'].filter(_=>_.id === parseInt(props.value))[0]['name'] : '' : column.value === 'insurance' ? metaData[column.oneOf].find(e => e.value === parseInt(props.value)) && metaData[column.oneOf].find(e => e.value === parseInt(props.value)).label || '' : props.value}
                                {column.limit && props.value && props.value.length > column.limit ?
                                    <ShowText title={column.label} text={props.value}>{(open) => <div className={styles.view} onClick={open.bind(this)} />}</ShowText> : null}
                            </span>);
        }
    };

    render() {
        const { title, recordsItem, patientId, schema, editRecord, deleteItem, editing, original, handleSubmit, onEditItem, onCancelItem, path, user, patientName, chartNumber, metaData = {}, updateError, disable, createAppointment, phone, email, medicalCondition, referredBy } = this.props;
        const pdfAuthToken = user && user.secret || user && user.token;
        const { fee, estIns, insurancePaidAmt, patientPaidAmt } = recordsItem.reduce((a, el) => Object.assign({}, { fee: el.fee && parseFloat(el.fee) + a.fee || a.fee, estIns: el.estIns && parseFloat(el.estIns) + a.estIns || a.estIns, insurancePaidAmt: el.insurancePaidAmt && parseFloat(el.insurancePaidAmt) + a.insurancePaidAmt || a.insurancePaidAmt, patientPaidAmt: el.patientPaidAmt && parseFloat(el.patientPaidAmt) + a.patientPaidAmt || a.patientPaidAmt, toothNumber: el.toothNumber }), { fee: 0, estIns: 0, insurancePaidAmt: 0, patientPaidAmt: 0 });
        const defaultObject = recordsItem.filter(u => u.medicalInsuranceName === 'Total');

        if(defaultObject.length == 0) {
          if (parseFloat(fee) > 0 || parseFloat(estIns) > 0 || parseFloat(insurancePaidAmt) > 0 || parseFloat(patientPaidAmt) > 0)
              recordsItem.push({ fee: fee, estIns: estIns, medicalInsuranceName: 'Total', disc: parseFloat(fee) - parseFloat(estIns), insurancePaidAmt: insurancePaidAmt, patientPaidAmt: patientPaidAmt, disableEdit: true });
        }

        const data = recordsItem.concat([schema.TreatmentPlanDetails.reduce((a, el) => Object.assign({}, a, { [el.value]: '' }), { create: true })]);

        const onChange = (e) => {
            if (e.target != undefined && e.target.name == "insurance") {
                const insuranceCompanyId = document.getElementsByName('insurance')[0].value;
                const cdcCode = document.getElementsByName('cdcCode')[0].value;
                var submitRecord = {}
                submitRecord.patientId = patientId;
                submitRecord.procedureCode = cdcCode;
                submitRecord.insuranceCompanyId = insuranceCompanyId;
                getInsuranceFeeDetails(submitRecord)
                  .then(feeValue => {
                    const fee = document.getElementsByName('fee')[0];
                    fee.value = feeValue.fee;
                    const insuranceId = document.getElementsByName('insurance')[0];
                    insuranceId.value = parseInt(feeValue.insuranceCompanyId);
                    const estIns = document.getElementsByName('estIns')[0].value;
                    const balance = document.getElementsByName('disc')[0];
                    const total = fee.value && estIns && parseFloat(fee.value) - parseFloat(estIns) || 0;
                    balance.value = total
                  })
            }
            if (e.target != undefined && (e.target.name == "estIns" || e.target.name == "fee")) {
                const fee = document.getElementsByName('fee')[0].value;
                const estIns = document.getElementsByName('estIns')[0].value;
                const balance = document.getElementsByName('disc')[0];
                const total = fee && estIns && parseFloat(fee) - parseFloat(estIns) || 0;
                balance.value = total
            }
            if(e.target != undefined && (e.target.name == "insurancePaidAmt" || e.target.name == "patientPaidAmt" || e.target.name == "fee")){
                const fee = document.getElementsByName('fee')[0].value;
                const insurancePaidAmt = document.getElementsByName('insurancePaidAmt')[0].value;
                const patientPaidAmt = document.getElementsByName('patientPaidAmt')[0].value;
                const patientBalance = document.getElementsByName('patientBalance')[0];
                const total = parseFloat(fee || 0) - (parseFloat(insurancePaidAmt || 0) + parseFloat(patientPaidAmt || 0));
                patientBalance.value = total
            }
        }
        
        const handleSort = (startDate, endDate, c, type) => {
            let startDateValue = startDate;
            let endDateValue =  endDate;
            if(startDateValue != null && endDateValue != null) {
              if (startDateValue.length === endDateValue.length && type == 'date') {
                return moment(startDateValue).format("YYYY-MM-DD") > moment(endDateValue).format("YYYY-MM-DD") ? 1 : -1;
              } else if (type == 'text') {
                if(startDateValue == "" && c ) {
                  return -1;
                }
                if(startDateValue == "" && !c ) {
                  return 1;
                }
                return startDateValue.toLowerCase() > endDateValue.toLowerCase() ? 1 : -1;
              }
            } else {
              if(startDateValue == "" && c  ) {
                return -1;
              }
              if(endDateValue == "" && !c ) {
                return 1;
              }
              if(startDateValue == undefined && c ) {
                return -1;
              }
              if(endDateValue == undefined && !c ) {
                return 1;
              }
              if( startDateValue === null ) return -1;
              if( endDateValue === null ) return 1;
            }
          }

        return <Col className={styles.TreatmentPlanDetails}>
            <div bsSize="large" className={"treatmentPlanDetails"}>
                <div style={{ paddingBottom: 0 }}>
                    <div>
                        <Col className={styles.header}>
                            <span className={styles.titleStyle}>{`${title} Details`}</span>
                            {pdfAuthToken ?
                                <Col className={styles.links}>
                                    <Info data={infoSchema().patients().treatmentPlanDetailsInfo}/>
                                    <a href={`${process.env.API_URL}/treatmentPlanDetailsPDF?X-Auth-Token=${pdfAuthToken}&id=${patientId}&status=Proposed,Accepted,Completed,Referred Out`} style={{marginRight:'12px'}} target="_blank" data-rel="external">
                                        <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Treatment Plan Details PDF"} alt="pdf" className={styles.img} /><br></br>
                                        <span style={{marginLeft:"14px",color:"black"}}>Tx</span><br></br><span style={{marginLeft:'-1px',color:"black"}}>History</span>
                                    </a>
                                    <a href={`${process.env.API_URL}/patientEstimatePDF?X-Auth-Token=${pdfAuthToken}&patientId=${patientId}`} style={{marginRight:'16px'}} target="_blank" data-rel="external">
                                        <img src={require('img/patients/estimate.svg')} ref='foo' data-place={"bottom"} data-tip={"Estimate"} alt="pdf" className={styles.estimate} /><br></br>
                                        <span style={{marginLeft:"14px",color:"black"}}>Tx</span><br></br><span style={{marginLeft:'-5px',color:"black"}}>Estimate</span>
                                    </a>
                                    <a href={`${process.env.API_URL}/PatientStatementPDF?X-Auth-Token=${pdfAuthToken}&patientId=${patientId}`} style={{marginRight:'16px'}} target="_blank" data-rel="external">
                                        <img src={require('img/patients/statement.svg')} ref='foo' data-place={"bottom"} data-tip={"Statement"} alt="pdf" className={styles.statement} /><br></br>
                                        <span style={{marginLeft:"14px",color:"black"}}>Tx</span><br></br><span style={{marginLeft:'-8px',color:"black"}}>Statement</span>
                                    </a>
                                    <a href={`${process.env.API_URL}/treatmentPlanDetailsPDF?X-Auth-Token=${pdfAuthToken}&id=${patientId}&status=Referred Out`} style={{marginRight:'25px'}} target="_blank" data-rel="external">
                                        <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Referred out"} alt="pdf" className={styles.referredOut} /><br></br>
                                        <span style={{color:"black"}}>Referred</span><br></br><span style={{marginLeft: "8px", color:"black"}}>out Tx</span>
                                    </a>
                                    <a href={`${process.env.API_URL}/treatmentPlanDetailsPDF?X-Auth-Token=${pdfAuthToken}&id=${patientId}&status=Existing`} target="_blank" data-rel="external">
                                        <img src={require('img/patients/pdf1.svg')} ref='foo' data-place={"bottom"} data-tip={"Existing Conditions"} alt="pdf" className={styles.existing} /><br></br>
                                        <span style={{color:"black"}}>Existing</span><br></br><span style={{marginLeft: "-8px", color:"black"}}>Conditions</span>
                                    </a>
                                </Col>
                                : null}
                        </Col>
                        <h5>Name: {patientName}</h5>
                        <h5>Chart Number: {chartNumber}</h5>
                    </div>
                </div>
                <div className={styles.body} style={{ paddingTop: 0 }}>
                    <form id="TreatmentPlanDetails" onSubmit={handleSubmit}>
                        {data ?
                            <ReactTable
                                columns={schema.TreatmentPlanDetails.map((column) => Object.assign(
                                    {},
                                    { Header: column.sort ? column.sortLabel : column.label, accessor: column.value,sortable: column.sortable?column.sortable:null, width: column.width,sortMethod: column.dateSort ? (a, b, c) => handleSort(a, b, c, column.type) : column.textSort ? (a,b,c) =>handleSort(a,b,c,"text") : null, ...column, ...this.editableColumnProps }
                                ))}
                                data={data}
                                getTdProps={(state, rowProps) => {
                                    return {
                                        edit: rowProps && editing === rowProps.original.id ? true : false,
                                        metaData: metaData,
                                        onOpen: () => createAppointment({...rowProps.original, "phone": phone, "email": email, "medicalCondition": medicalCondition, "referredBy": referredBy }),
                                        onEdit: () => onEditItem(rowProps),
                                        onCancel: () => onCancelItem(),
                                        onDelete: () => deleteItem({ id: rowProps && rowProps.original.id, patientId: patientId }),
                                        onChange: (val) => onChange(val),
                                        style: rowProps && rowProps.original && rowProps.original.status === "Proposed" ? {color:"#ea6225"}: rowProps && rowProps.original && rowProps.original.status === "Accepted" ? {color:"#38a0f1"} : rowProps && rowProps.original && rowProps.original.status === "Completed" ? {color:"#87b44d"} : rowProps && rowProps.original && rowProps.original.status === "Referred Out" ? {color:"#91288F"} : rowProps && rowProps.original && rowProps.original.disableEdit ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold' } : ((rowProps && rowProps.original && rowProps.original.create) || (rowProps && editing === rowProps.original.id)) ? { height: '100%' } : {}
                                    }
                                }}
                                pageSize={data && data.length > 0 && data.length < 22 ? data.length : data && data.length > 21 ? 21 : 1}
                                showPageJump={false}
                                resizable={false}
                                showPageSizeOptions={false}
                                previousText={"Back"}
                                pageText={""}
                                noDataText={"No entries to show."}
                            /> : <Spinner />}
                    </form>
                    {updateError ?
                        <Alert warning dismiss={()=>this.props.clearError()}>{updateError}</Alert> : null}
                </div>
            </div>
        </Col>
    }

    close() {
        const { onCancel } = this.props;
        this.setState({ showModal: false });
        onCancel();
    }

    open() {
        this.setState({ showModal: true });
    }

}

export default reduxForm({
    form: `editRecord`,
    enableReinitialize: true,
})(TreatmentPlanDetails);
