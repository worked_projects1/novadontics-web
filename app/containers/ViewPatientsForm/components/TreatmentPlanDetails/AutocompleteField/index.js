import React from 'react';
import { Col, Modal } from '@sketchpixy/rubix';
import styles from './styles.css';
import Autocomplete from './autocomplete';

export default class AutocompleteField extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, val: false }
  }

  componentDidMount() {
    const { input, data, metaData } = this.props;
    const isPreDefinedSet = Array.isArray(data.oneOf);
    Autocomplete(document.getElementById(input.name), isPreDefinedSet && data.oneOf ? data.oneOf : metaData[data.oneOf], data, input);
  }

    handleRef() {
      setTimeout(() => {
        const { input } = this.props;
        this.props.inputChange(input.value);
      }, 3000);
    }

  render() {
    const { data, input, inline } = this.props;
    return <Col className={styles.AutocompleteField}>
      <input
        className={inline ? styles.InputField : null}
        name={input && input.name}
        id={input && input.name}
        type="text"
        autoComplete="off"
        onBlur={this.handleRef.bind(this)}
        defaultValue={input && input.value}
        required={data.required} />
      {data.view ? <div onClick={this.showView.bind(this)} className={styles.view}></div> : null}
      <Modal show={this.state.showModal} onHide={this.close.bind(this)} >
        <Modal.Header closeButton>
          <Modal.Title>{data.label}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.state.val ? this.state.val : input.value ? input.value : ''}
        </Modal.Body>
      </Modal>
    </Col>
  }

  close() {
    this.setState({ showModal: false });
  }

  showView() {
    const { input } = this.props;
    const val = document.getElementById(input.name).value;
    if(val)
      this.setState({ showModal: true, val: val })
  }
}