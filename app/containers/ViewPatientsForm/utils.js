export function parseStructure(str = '') {
  return typeof str == 'string' && str.includes('★') ? JSON.parse(str.replace(/★/g, '"')) : str;
}