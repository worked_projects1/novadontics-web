/*
 * ViewPatientsForm Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Alert, Row, Col, Button, Icon } from '@sketchpixy/rubix';
import moment from 'moment-timezone';
import { browserHistory } from 'react-router';
import { selectUser } from 'blocks/session/selectors';
import { loadMeasurementHistory } from 'blocks/patients/measurementHistory';
import { FormattedMessage } from 'react-intl';
import { parseStructure } from './utils.js';
import messages from '../RecordsPage/messages';
import Spinner from 'components/Spinner';
import RequestConsultationForm from './components/RequestConsultationForm';
import DeleteActionLink from "../../components/DeleteActionLink";
import ModalForm from 'components/ModalRecordForm';
import ClinicalNotes from './components/ClinicalNotes';
import Gallery from 'components/Gallery';
import DocumentCenter from './components/DocumentCenter';
import FormList from './components/FormList';
import ListDate from './components/ListDate';
import MedicalAlerts from './components/MedicalAlerts';
import ToothChart from './components/ToothChart';
import PerioChart from './components/PerioChart';
import EndoChart from './components/EndoChart';
import ReactTable from './components/Table';
import AlertMessage from 'components/Alert';
import { loadToothChart } from 'blocks/patients/toothchart';
import { updateCtScan } from 'blocks/patients/ctscan';
import { updateContracts } from 'blocks/patients/contracts';
import { updateScratchPad } from 'blocks/patients/scratchpad';
import styles from './styles.css';
import schema from '../../routes/schema';
import ReactTooltip from 'react-tooltip';
import uploadSignature from '../../components/SignatureField/uploadUtils.js'
import EmailShare from 'react-email-share-link';
import Prescriptions from './components/Prescriptions';
import TreatmentPlan from './components/TreatmentPlan';
import StaffNotes from './components/StaffNotes';
import Appointment from './components/Appointment';
import { updateRecallPeriod } from 'blocks/patients/appointment';
import { getDoseSpotUrl } from 'blocks/patients/remotes.js';
import { sortBy } from 'utils/tools';
import SharedPatient from './components/SharedPatient';
import EditPatientForm from 'containers/PatientsPage/components/EditPatientForm';
import { formValueSelector, submit } from 'redux-form';
import { loadAuthorize, authorizeApi } from 'blocks/products/remotes.js';
import map from '../../blocks/records/map.js';

const DisablePatientSquare = true;

export default function (path, columns, requestConsultationColumns, updateRecord, { selectRecords, selectRecord, selectFormSchema, selectRecordsMetaData }, actions, selectLoading, pageLoading, selectError, selectUpdateError, consultations, selectExternalRecords) {

  function handleEdit(data, dispatch, { form }) {
    const [externalRecords = {}] = this.props.externalRecords || [];

    const submitRecord = data.toJS();
    const { patient = {}, steps } = submitRecord;
    const record = {};
    /*
    Creating API input
    **/

    record.id = submitRecord.id;
    record.emailAddress = submitRecord.email ? submitRecord.email : patient.email;
    record.phoneNumber = submitRecord.phone ? submitRecord.phone : patient.phone;
    record.patientId = patient.patientId;
    record.steps = steps;
    record.steps[0]['state'] = 'Completed';
    delete submitRecord.patient; //deleting patient
    delete submitRecord.id; //deleting id
    delete submitRecord.steps; //deleting steps
    //delete submitRecord.state;//deleting state
    /*
    Creating steps API input
    **/

    const stepValues = Object.keys(submitRecord).length > 0 ?
      Object.keys(submitRecord)
        .filter(key => key)
        .reduce((obj, key) => {
          obj[key] = (typeof (submitRecord[key]) === 'object') ? JSON.stringify(submitRecord[key]).replace(/"/g, '★').replace(/\//g, "\\/") : submitRecord[key];
          return obj;
        }, {}) : {};
    record.steps[0]['value'] = stepValues;

    if (this.props.user && this.props.user.xvWebURL != '' && Object.keys(externalRecords).length > 0) {
      const { full_name, last_name, gender, birth_date } = submitRecord;
      dispatch(actions.updateExternalRecord({ Id: externalRecords.Id, name: `${last_name}^${full_name}`, firstName: full_name, lastName: last_name, gender: gender && gender === 'male' ? 'M' : gender && gender === 'female' ? 'F' : '', birthdate: birth_date && isNaN(Date.parse(birth_date)) ? '' : birth_date && birth_date != null ? moment.utc(birth_date).format('YYYY-MM-DD') : '' }));
    }
    dispatch(updateRecord(record, form));
  }

  function handleSubmit(data, dispatch, { form }) {
    const { patient = {} } = this.props.record;
    const { name, last_name } = patient;
    const patientName = name + ' ' + (last_name ? last_name : '');
    const record = data.toJS();
    const submitRecord = {};

    /**
     * creating a input data from consulatation submit record
     */
    submitRecord.customerProfileID = this.state.customerId;
    submitRecord.customerPaymentProfileId = this.state.paymentId;
    submitRecord.transId = this.state.transId;
    submitRecord.transactionCode = this.state.transCode;
    submitRecord.treatmentId = record.treatmentId;
    submitRecord.majorType = "consultation";
    submitRecord.amount = 390.00;
    const consultant = this.props.record.emailMappings && this.props.record.emailMappings.filter(r => r.isConsultation && record[r.name] && record[r.name]['checked']).map(c => record[c.name]['selectedOption'] ? parseInt(record[c.name]['selectedOption']) : null)[0];
    submitRecord.consultantId = consultant != undefined ? consultant : null;
    submitRecord.data = { comment: record.comment && record.comment.notes ? record.comment.notes : '' };
    submitRecord.consultArea = record.chooseConsultArea ? record.chooseConsultArea : '' ;
    submitRecord.data.consultations = this.props.record.emailMappings && this.props.record.emailMappings.filter(r => r.isConsultation && record[r.name] && record[r.name]['checked']).map(c => Object.assign({}, {
      id: c.id,
      consultantName: c.users.filter(r => r.id === parseInt(record[c.name]['selectedOption'])).map(r => r.name)[0],
      patient: patientName,
      requested: true,
      consultantId: record[c.name]['selectedOption'] ? parseInt(record[c.name]['selectedOption']) : null,
      type: c.name,
      consultArea: record.chooseConsultArea
    }));

    dispatch(consultations.createRecord(submitRecord, form));
    this.setState({ consultAlert: true });
  }

  function handleRecallPeriod(data, dispatch, { form }) {
    let submitRecallRecord = data.toJS();
      if (submitRecallRecord.recallOneDayPlus == '' || submitRecallRecord.recallOneDayPlus == undefined) {
        submitRecallRecord.recallOneDayPlus = false
      }
      if (submitRecallRecord.intervalUnit == '' || submitRecallRecord.intervalUnit == undefined) {
        submitRecallRecord.intervalUnit = "months"
      }
      submitRecallRecord.recallDueDate = moment(submitRecallRecord.recallDueDate).format('YYYY-MM-DD')
      if (submitRecallRecord.recallDueDate == 'Invalid date') {
        submitRecallRecord.recallDueDate = null
      }
      dispatch(updateRecallPeriod(submitRecallRecord));
      this.setState({ alertMessage: true });
  }

  class ViewPatientsForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = { cardDetails: [], authDetails: [], activeCard: 0, defaultCard: 0, activePayment: [],
         showOnScroll: false, recallAlert: false, requestConsultAlert: false, consultAlert: false, alertMessage: false, showAlert: false, emptyDoseSpotAlert: false, invalidDoseSpotAlert: false, showFrame: false, showIcons: false, showPatientScreen: true, formshowid:"1H", customerId: 0, paymentId: 0, transId: 0, transCode: '', errorMessage: '', formLoader: false };
      onScroll = onScroll.bind(this);
      this.handleFrame = handleFrame.bind(this);
      this.handleDoseSpot = handleDoseSpot.bind(this);
      this.handleEClaims = handleEClaims.bind(this);
      this.handleIcons = handleIcons.bind(this);
      this.handleMainIcons = handleMainIcons.bind(this);
    }

    static propTypes = {
      record: PropTypes.object,
      dispatch: PropTypes.func,
      params: PropTypes.object,
      formSchema: PropTypes.array,
      user: PropTypes.object,
    };

    componentDidMount() {
      const { dispatch, params = {}, record = {}, records, router = {} } = this.props;
      const { cardDetails, authDetails } = this.state;
      const { id } = params;
      const { location = {} } = router;
      const { state = {} } = location;
      document.getElementById("app").style.overflow = "hidden";
      document.getElementById("body").firstChild.style.overflow = "hidden";
      {/*$("#app").animate({ scrollTop: 0 }, 500, function () {
        const elmnt = document.getElementById("patientFormBody");
        if (elmnt)
          elmnt.addEventListener('scroll', onScroll, false);
      });*/}
      if (dispatch) {
        this.props.dispatch(loadToothChart(record.id));
        this.loadAuthorizeDetails();
        if (id && records && records.length > 0) {
          this.props.dispatch(actions.loadRecord(parseInt(id, 10)));
        }
      }

      if(selectExternalRecords) {
        const interval = setInterval(() => {
          const { record = {} } = this.props;
          const { patient = {} } = record;
          const { name, last_name, birthDate, gender } = patient;
          if (name && last_name && birthDate && gender && this.props.user.xvWebURL != '') {
            this.props.dispatch(actions.loadExternalRecords({ firstName: name, lastName: last_name, gender: gender && gender === 'male' ? 'M' : gender && gender === 'female' ? 'F' : '', birthdate: birthDate && isNaN(Date.parse(birthDate)) ? '' : birthDate && birthDate != null ? moment.utc(birthDate).format('YYYY-MM-DD') : '' }));
            clearTimer();
          }
        }, 300);
        const clearTimer = () => clearInterval(interval);
      }
      if(state.openToothChart && state.openToothChart == true) {
        this.handleIcons('toothChart');
      }
    }

    /*componentDidUpdate() {
      const elmnt = document.getElementById("patientFormBody");
      if (elmnt)
        elmnt.addEventListener('scroll', onScroll, false);
    }*/

    componentWillUnmount() {
      document.getElementById("app").style.overflow = "initial";
      document.getElementById("body").firstChild.style.overflow = "initial";
      const elmnt = document.getElementById("patientFormBody");
      if (elmnt)
        elmnt.removeEventListener('scroll', onScroll, false);
    }

    openPrescriptions = () => {
      document.getElementById('Prescriptions').click();
    }

    loadAuthorizeDetails = () => {
      const config = {
          transformRequest: [(data, headers) => {
            delete headers.common['Authorization']; delete headers.common['X-Auth-Token']; delete headers.common['timezone'];
              if (data !== undefined) {
                 headers.post['Content-Type'] = 'application/json;charset=UTF-8';
                 return JSON.stringify(data)
              } else {
                return data;
              }
            return data;
          }]
      };

      loadAuthorize()
        .then(authDetail => {
          authDetail.map(authDetails => {
            this.setState({ authDetails: authDetails })
            if(this.props.user.authorizeCustId != 0) {
              const cardDetailJson = {"getCustomerProfileRequest": {"merchantAuthentication": {"name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey}, "customerProfileId": this.props.user.authorizeCustId, "unmaskExpirationDate" : "true", "includeIssuerInfo": "true"}}
              authorizeApi(authDetails.apiUrl, cardDetailJson, config)
                .then(cardDetails => {
                  if(cardDetails.profile != undefined) {
                    this.setState({ cardDetails: cardDetails.profile })
                    if(cardDetails.profile.length != 0 && cardDetails.profile.paymentProfiles != undefined){
                      cardDetails.profile.paymentProfiles.map((card, i) => { if(card.defaultPaymentProfile == true) {this.setState({ activeCard: card.customerPaymentProfileId, defaultCard: card.customerPaymentProfileId, activePayment: card})}})
                    }
                  } else{
                    this.setState({ cardDetails: [] })
                  }
                }).catch(() => this.setState({ cardDetails: [] }));
            }
          })
      }).catch(() => this.setState({ authDetails: [] }));
    }

    handlePaymentDetails = ({customerId, paymentId, transId, transCode}) => {
      this.setState({ customerId: customerId, paymentId: paymentId, transId: transId, transCode: transCode });
    }


    render() {
      const { record = {}, recordsMetaData = {}, formSchema, user, loading, pageLoader, dispatch, loadAction, error, updateError } = this.props;
      const { showOnScroll, showIcons, alertMessage, showAlert, emptyDoseSpotAlert, invalidDoseSpotAlert, consultAlert, showPatientScreen, recallAlert, requestConsultAlert, cardDetails, authDetails, activeCard, activePayment, formshowid, defaultCard, errorMessage } = this.state;
      const { recordDidNotInit, id, patientFirstName, patientLastName, patient = {}, steps: stepsRoot = {}, patientPhoto, patientPhoto1, clinicalNotesData, measurementHistory = [], prescriptionHistory = [], perioChartData = [], endoChartData = [], toothChartData = [], treatmentPlanData = [], treatmentPlanDetails = [], metaData, emailMappings, prescriptionsColumns, allergy, medicalCondition, appointmentData, sharedPatientData = [], staffNotesData = [], contractsData = [], sharedAdminPatientData = {}, apteryxData = {} } = record;
      const { provider, providers = [], dentalCarrier = [], staffMembers = [], implantBrandOptions = [], recallProcedureOptions = [], preMadeClinicalNotesOptions = [], procedureCodeCategory = [] } = metaData || {};
      const staffList = staffMembers && staffMembers.length > 0 ? staffMembers.map(el => Object.assign({}, { label: el.name, value: `S:${el.id}`})) : [];
      const providerList =  providers && providers.length > 0 ? providers.map(el => Object.assign({}, { label: el.name, value: `P:${el.id}`})) : [];
      const creator = providerList.concat(staffList);
      const appMetaData = {};
      appMetaData.creatorList =  creator && creator.length > 0 ? creator.map(el => Object.assign({}, { label: el.label, value: `${el.value}` })) : [];
      appMetaData.assistantList =  creator && creator.length > 0 ? creator.map(el => Object.assign({}, { name: el.label, value: `${el.value}` })) : [];
      const { steps } = stepsRoot;
      const { patientId, name, first_name, last_name, birthDate, gender, phone, email, lastEntry, firstVisit, bp_pulse_o2, toothChartUpdatedDate, clinicalNotes, toothChart, perioChart, endoChart, prescription, treatmentPlan, recallPeriod, appointment, checkList, labs, tx, patient_status, consents, shareIcon, shared, accessTo, accessLevel, recallCode, recallCodeDescription, recallDueDate, recallFromDate, recallNote, recallOneDayPlus, recallType, intervalNumber, intervalUnit, examImaging, referredBy, staffNotesIcon, lastInvitationSent } = patient;
      const dueDateRecall = recallDueDate != null ? moment(recallDueDate).format('DD MMM, YYYY') : ""
      const viewOnly = accessLevel == 'View' ? true : false
      const pdfAuthToken = user && user.secret || user && user.token;
      const appSquares = user && user.appSquares;
      let checklistAccess = false;
      let requestConsultAccess = false;
      if (user != undefined) {
        checklistAccess = appSquares.includes('checklist') ? true : false;
        requestConsultAccess = appSquares.includes('requestConsultationOnline') ? true : false;
      }
      const patientName = name + ' ' + (last_name ? last_name : '');
      const displayPhoto = patientPhoto && patientPhoto.length > 0 ? patientPhoto[patientPhoto.length - 1] : patientPhoto1 && patientPhoto1.length > 0 ? patientPhoto1[patientPhoto1.length - 1] : false;
      const primaryProvider = providers.find(_ => _.id === parseInt(provider));
      const displaySquareIcons = accessTo == "" ? accessTo : accessTo && typeof accessTo === 'string' && accessTo.split(",") || [];

      if (formSchema.length === 0 || recordDidNotInit || pageLoader || steps == undefined) {
        return <Spinner />;
      }

      /**
       * Setting a option for request consultation columns
       */
      requestConsultationColumns.map(columns => {
        const filteredCons = emailMappings && emailMappings.filter(r => r.isConsultation && r.name === columns.name)[0];
        if (filteredCons && filteredCons.users && Object.keys(filteredCons.users[0]).length > 0)
          columns['attributes'] = { modalOptions: true, options: filteredCons.users.map(u => { return { title: u.name, value: u.id } }) };
      });

      if (error) {
        return <Col sm={12} className={styles.error}>
          <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert>
        </Col>
      }

      return (
        <Row className={styles.patientModule}>
          <ReactTooltip type="info" html={true} />
          {showPatientScreen ?
            <Col className={styles.header}>
              <Col sm={12} className={styles.titleHeader}>
                <span><DeleteActionLink deleteRecord={actions.deleteRecord} record={record} /></span>
                <span className={styles.title}>Patient</span>
                <Link to={{ pathname: '/patients' }}>
                  <Button  bsStyle="standard" type="button" className={styles.btn}>close</Button>
                </Link>
              </Col>
              <Col xsHidden sm={12} className={styles.patientName}>
                <Col className={styles.label}>{patientName}</Col>
                <hr className={styles.hr} />
              </Col>
            </Col> : null}
          <Col id={"patientFormBody"} className={styles.body} style={user && user.platform != 'iPad' ? !showPatientScreen ? { height: `${document.body.clientHeight - 50}px` } : { height: `${document.body.clientHeight - 210}px` } : { height: `${document.body.clientHeight - 100}px` }}>
            {showPatientScreen ?
              <Col xsHidden className={styles.patientDetails}>
                <Col md={9} sm={10} className={styles.patientInfo}>
                  <Col>
                    <Col><span>Patient ID</span> {patientId && patientId != null ? patientId : '---'}</Col>&nbsp;
                    <Col><span>Cell Phone</span> {phone}</Col>
                  </Col>
                  <Col>
                    <Col><span>Birthdate</span> {birthDate && isNaN(Date.parse(birthDate)) ? '---' : birthDate && birthDate != null ? moment.utc(birthDate).format('MM/DD/YYYY') : '---'}</Col>&nbsp;
                    <Col><span>Gender</span> {gender && gender != null ? gender[0].toUpperCase() + gender.substring(1) : '---'}</Col>
                  </Col>
                  <Col>
                    <Col><span>Initial Visit</span> {firstVisit ? moment.utc(firstVisit).format('MM/DD/YYYY') : moment(lastEntry).format('MM/DD/YYYY')}</Col>&nbsp;
                    <Col><span>Email Address</span> {email}</Col>
                  </Col>
                  <Col>
                    <Col><span>Primary Provider</span> {primaryProvider && primaryProvider.name || '---'}</Col>&nbsp;
                    <Col><span>Patient Status</span> {patient_status || '---'}</Col>
                  </Col>
                </Col>
                <Col md={3} sm={2} className={styles.patientPhoto}>
                  {displayPhoto ?
                    <Gallery data={{ images: [displayPhoto] }} width="140" /> : null}
                </Col>
              </Col> : null}
            {showPatientScreen ?
              <Col xs={12} className={showOnScroll ? styles.patientIconScroll : styles.patientIcons} style={user && user.platform === 'iPad' && showOnScroll ? { position: 'fixed' } : {}}>
                {displaySquareIcons.includes('1H')  || !shared ?
                  <div style={{ width: '54px' }} className={styles.imgText}>
                      <img src={require(`img/patients/Photographs.svg`)} onClick={() => this.handleMainIcons(`${path}/${id}/1H/edit`,"1H")} ref='foo' data-place={"bottom"} alt="Labs" className={styles.img} style={{ padding: '5px' }} />
                   <span style={{ width: '39px' }}>Photos</span></div> : null}
                   {displaySquareIcons.includes('1D') || !shared ?
                  <div style={{ width: '54px' }} className={styles.imgText}>
                      <img src={require(`img/patients/ClinicalExam.svg`)} onClick={() => this.handleMainIcons(`${path}/${id}/1D/edit`,"1D")} ref='foo' data-place={"bottom"} alt="Labs" className={styles.img} style={{ padding: '5px' }} />
                  <span style={{ width: '39px' }}>Implant Exam</span></div> : null}
                {(user && user.role == 'dentist' && displaySquareIcons.includes('appointmentSchedule')) || !shared ?
                  <div className={styles.imgText} style={{ width: '85px' }}>
                    <Appointment
                      form={`appointmentRecord.${id}`}
                      id={id}
                      record={appointmentData || []}
                      columns={schema().patients().appointmentBook}
                      error={error}
                      title="Appointments">
                      {(open) => <img src={require(`img/patients/appointment.svg`)} ref='foo' data-place={"bottom"} alt="Appointment " className={styles.imgC} onClick={open.bind(this)} />}
                    </Appointment> <span style={{ width: '80px' }}>Appointment History</span></div> : null}
                {!shared ?
                  <div className={styles.imgText} style={{width:'59px'}}>
                    <Link to={{ pathname: '/ledger', state:{ patientId: id }}}>
                      <img src={require(`img/patients/ledger.svg`)} ref='foo' data-place={"bottom"} alt="Ledger" className={styles.imgC} />
                      <span style={{width:'44px', color: 'white'}}>Ledger</span>
                    </Link>
                  </div> : null}
                {displaySquareIcons.includes('recallPeriod') || !shared ?
                  <div className={styles.imgText} style={{ width: '49px' }}>
                    <ModalForm
                      initialValues={{ id: id, recallCode: recallCode, recallCodeDescription: recallCodeDescription, recallDueDate: dueDateRecall, recallFromDate: recallFromDate, recallNote: recallNote, recallOneDayPlus: recallOneDayPlus, recallType: recallType, intervalNumber: intervalNumber, intervalUnit: intervalUnit }}
                      fields={schema().patients().recallPeriod}
                      metaData={metaData}
                      form={`recallPeriodRecord`}
                      onSubmit={handleRecallPeriod.bind(this)}
                      onRef={recallPeriodForm => (this.recallPeriodForm = recallPeriodForm)}
                      config={{
                        record: record, title: "Recall Period", titleStyle: true, btnType: "image", imageIcon: [require(`img/patients/recallPeriod.svg`)], tooltip: false, width: '36px', height: '36px', btnName: 'Save', style: { marginRight: '1.2em', marginBottom: '1.2em' }, disable: viewOnly
                      }}
                    /> <span style={{ width: '38px' }}>Recall</span></div> : null}
                {displaySquareIcons.includes('checkList') || !shared ?
                  <div className={styles.imgText} style={{ width: '78px' }}>
                    <FormList
                      list={formSchema.reduce((a, el) => Object.assign([], a.concat(el.forms.filter(form => form.types === 'checkList'))), [])}
                      path={path}
                      display={true}
                      notes="Copyright-Protected Material."
                      title="Checklists & Protocols"
                      patientId={id}>
                      {(open) => <img src={require(`img/patients/checklist.svg`)} ref='foo' data-place={"bottom"} alt="Checklist" className={styles.img} onClick={checklistAccess ? open.bind(this) : handleRecallAlert.bind(this)} style={{ padding: '1px' }} />}
                    </FormList><span style={{ width: '66px' }}>Checklists</span></div> : null}

                  {displaySquareIcons.includes('consentForms') || !shared ?
                  <div style={{ width: '56px'}} className={styles.imgText}>
                  <img src={require(`img/patients/forms.svg`)} onClick={() => this.handleMainIcons(`${path}/${id}/1AL/edit`,"1AL")} ref='foo' data-place={"bottom"} alt="Patient Forms" className={styles.img} style={{ padding: '1px' }} />
                  <span style={{ width: '39px' }}>Forms</span></div> : null}

                {displaySquareIcons.includes('scratchPad') || !shared ?
                  <div style={{ width: '55px' }} className={styles.imgText}>
                    <ModalForm
                      initialValues={{
                        id: id,
                        scratchPad: patient && patient.scratchPad ? patient.scratchPad : null,
                        scratchPadText: patient && patient.scratchPadText ? patient.scratchPadText : null
                      }}
                      fields={schema().patients().scratchPad}
                      form={`scratchPadRecord.${id}`}
                      onSubmit={handleScratchPad.bind(this)}
                      onRef={scratchPadForm => (this.scratchPadForm = scratchPadForm)}
                      config={{
                        record: record, title: "Scratch Pad", titleStyle: true, btnType: "image", imageIcon: [require(`img/patients/scratchpad.svg`)], bsSize: 'large', viewType: 'tab', tooltip: false, width: '36px', height: '36px', modalClass: 'periochart', btnName: 'Save', style: { marginRight: '1.2em', marginBottom: '1.2em' },
                        shareDet: {
                          image: patient && patient.scratchPad ? patient.scratchPad : null,
                          email: email,
                          subject: `Scratch Pad content for ${patientName}`
                        }, disable: viewOnly
                      }}
                    /> <span style={{ width: '43px' }}>Scratch Pad</span></div> : null}
                {displaySquareIcons.includes('contracts') || !shared ?
                  <div className={styles.imgText} style={{ width: '70px' }}>
                    <DocumentCenter
                      title="Document Center"
                      records={contractsData}
                      form={`contractsRecord.${id}`}
                      fields={schema().patients().Contracts}
                      patientId={id}>
                      {(open) => <img src={require(`img/patients/contracts.svg`)} ref='foo' data-place={"bottom"} alt="DocumentCenter" className={styles.img} onClick={open.bind(this)} style={{ padding: '1px' }} />}
                    </DocumentCenter><span style={{ width: '55px' }}>Document Center</span></div> : null}
                {displaySquareIcons.includes('toothChart') || !shared ?
                  <div className={styles.imgText} style={{width:'59px'}}><img src={require(`img/patients/notes.svg`)} ref='foo' data-place={"bottom"} alt="Clinical Notes" className={styles.imgC} onClick={()=> this.handleIcons('clinicalNotes')} /><span style={{width:'44px'}}>Clinical Notes</span></div> : null}
                {displaySquareIcons.includes('toothChart') || !shared ?
                  <div className={styles.imgText} style={{ width: '59px' }}><img src={require(`img/patients/toothchart.svg`)} ref='foo' data-place={"bottom"} alt="Tooth Chart" className={styles.img} onClick={() => this.handleIcons('toothChart')} style={{ width: '40px' }} /><span style={{ width: '44px' }}>Tooth Chart</span></div> : null}
                {displaySquareIcons.includes('perioChart') || !shared ?
                  <div style={{ width: '57px' }} className={styles.imgText}><img src={require(`img/patients/periochart.svg`)} ref='foo' data-place={"bottom"} alt="Perio Chart" style={{ width: '50px', cursor: 'pointer' }} onClick={() => this.handleIcons('periochart')} /><span style={{ width: '38px' }}>Perio Chart</span></div> : null}
                {displaySquareIcons.includes('endoChart') || !shared ?
                  <div style={{ width: '57px' }} className={styles.imgText}><img src={require(`img/patients/endochart.svg`)} ref='foo' data-place={"bottom"} alt="Endo Chart" style={{ width: '50px', height: '45px', cursor: 'pointer' }} onClick={() => this.handleIcons('endochart')} /><span style={{ width: '50px', marginTop: '5px' }}>Endo Chart</span></div> : null}
                {!shared ?
                  <div style={{ width: '65px' }} className={styles.imgText}>
                  <img src={require(`img/patients/insurance.svg`)} ref='foo' data-place={"bottom"} alt="Prescriptions" className={styles.img} onClick={() => this.handleEClaims()} style={{ width: '50px', height: '45px', marginBottom: '0.5em' }} />
                  <span style={{ width: '50px' }}>eClaims</span></div> : null}
                    {displaySquareIcons.includes('prescriptions') || !shared ?
                     <div style={{ width: '45px' }} className={styles.imgText}>
                    <img src={require(`img/patients/rx.svg`)} ref='foo' data-place={"bottom"} id='Prescriptions' alt="Prescriptions" className={styles.img} onClick={() => this.handleIcons('prescriptions')} />
                    <span style={{ width: '32px' }}>Rx Guide</span></div> : null}
                    {!shared ?
                      <div style={{ width: '65px' }} className={styles.imgText}>
                      <img src={require(`img/patients/dose.svg`)} ref='foo' data-place={"bottom"} alt="Prescriptions" className={styles.img} onClick={() => user.doseSpotUserId && user.doseSpotUserId != '' ? this.handleDoseSpot(id) : this.setState({ emptyDoseSpotAlert: true })} style={{ marginLeft: '1.0em' }} />
                      <span style={{ width: '45px' }}>eRx</span></div> : null}
                    {displaySquareIcons.includes('2C') || !shared ?
                  <div style={{ width: '54px' }} className={styles.imgText}>
                  <img src={require(`img/patients/SmileAnalysis.svg`)} onClick={() => this.handleMainIcons(`${path}/${id}/1D/edit`,"2C")} ref='foo' data-place={"bottom"} alt="Labs" className={styles.img} style={{ padding: '5px' }} />
                  <span style={{ width: '39px' }}>Smile Design</span></div> : null}
                  {displaySquareIcons.includes('labs') || !shared ?
                  <div style={{ width: '55px', paddingRight: '16px' }} className={styles.imgText}>
                  <img src={require(`img/patients/labs.svg`)} onClick={() => this.handleMainIcons(`${path}/${id}/1K/edit`,"1K")} ref='foo' data-place={"bottom"} alt="Labs" className={styles.img} style={{ padding: '5px' }} />
                  <span style={{ width: '39px' }}>Lab</span></div> : null}
                {displaySquareIcons.includes('ctScans') || !shared ?
                  <div className={styles.imgText} style={{ width: '57px' }}>
                    <ModalForm
                      initialValues={{
                        id: id,
                        ctScanFile: patient && patient.ctScanFile ? patient.ctScanFile : null,
                        disable: viewOnly
                      }}
                      fields={schema().patients().ctScan}
                      form={`ctscanRecord.${id}`}
                      updateCtScan={(data) => { dispatch(updateCtScan(data)); }}
                      onRef={ctScanForm => (this.ctScanForm = ctScanForm)}
                      noteText={true}
                      notes="Note: You can upload here Photographs, PDF, STL, OBG, PLY, XML and CT Scan Zip Files."
                      config={{
                        record: record, title: "CT & Intraoral Scans", btnType: "image", imageIcon: [require(`img/patients/ct-scan.svg`)], tooltip: false, width: '36px', height: '36px', btnName: 'Save', style: { marginRight: '1.2em', marginBottom: '1.2em' }, disable: viewOnly, hideSaveButton: true
                      }}
                      infoColumns={schema().patients().ctScanInfo}
                    /> <span style={{ width: '45px' }}>Scans</span></div> : null}
                {displaySquareIcons.includes('xray') || !shared ?
                  <div className={styles.imgText} style={{ width: '64px' }}><img src={require(`img/patients/xray.svg`)} ref='foo' data-place={"bottom"} alt="X-ray" className={styles.img} onClick={() => user.xvWebURL && user.xvWebURL != '' ? this.handleFrame(`${user.xvWebURL}/#query?primaryid=${patientId || ''}&lastname=${patientLastName || ''}&firstname=${patientFirstName || ''}&gender=${gender && gender === 'male' ? 'm' : gender && gender === 'female' ? 'f' : ''}&birthdate=${birthDate && isNaN(Date.parse(birthDate)) ? '' : birthDate && birthDate != null ? moment.utc(birthDate).format('YYYY-MM-DD') : ''}&token=${user.access_token}`) : this.setState({ showAlert: true })} /><span style={{ width: '45px' }}>X-Rays</span></div> : null}
                {displaySquareIcons.includes('email') || !shared ?
                  <div className={styles.imgText} style={{ width: '58px' }}>
                    <EmailShare
                      email={email}
                      subject={''}
                      body={'Type your message here..'}>
                      {link => (
                        <a href={link} data-rel="external"><img src={require('img/patients/mail.svg')} ref='foo' data-place={"bottom"} alt="email" className={styles.img} /></a>
                      )}
                    </EmailShare> <span style={{ width: '46px' }}>Email</span> </div> : null}
                {(user && user.role == 'dentist' && displaySquareIcons.includes('requestConsultation')) || !shared ?
                  <div className={styles.imgText} style={{ width: '69px' }}>
                    <RequestConsultationForm
                      initialValues={{ treatmentId: id, patient: patient.name }}
                      fields={requestConsultationColumns}
                      onSubmit={handleSubmit.bind(this)}
                      loadAuthorizeDetails={this.loadAuthorizeDetails.bind(this)}
                      handlePaymentDetails={this.handlePaymentDetails}
                      patientName={patientName}
                      user = {user}
                      cardDetails = {cardDetails}
                      authDetails = {authDetails}
                      activeCard={activeCard}
                      defaultCard={defaultCard}
                      activePayment={activePayment}
                      optionsMode={'body'} >
                      {(open) => <img src={require(`img/patients/request-consultation.svg`)} ref='foo' data-place={"bottom"} alt="Request Consultation" className={styles.img} onClick={requestConsultAccess ? open.bind(this) : handleConsultAlert.bind(this)} />}
                    </RequestConsultationForm> <span style={{ width: '61px' }}>Consults</span></div> : null}
                  {displaySquareIcons.includes('staffNotes') || !shared ?
                    <div className={styles.imgText} style={{ width: '55px' }}>
                      <StaffNotes
                        title="Staff Notes"
                        records={staffNotesData}
                        path={path}
                        columns={schema().patients().StaffNotes}
                        updateError={this.props.updateError}
                        loadPatientRecord={() => dispatch(actions.loadRecord(parseInt(id, 10)))}
                        clearError={() => dispatch(actions.loadRecordsCacheHit())}
                        metaData={Object.assign({}, metaData, {staffOptions: appMetaData.creatorList}, {staffAllOptions: appMetaData.creatorList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{value:'all',label:'All'}])})}
                        infoColumns={schema().patients().staffNotesInfo}
                        patientId={id}>
                        {(open) => <img src={require(`img/patients/${staffNotesIcon === 'on' ? 'staffNotesOrange' : 'staffNotes'}.svg`)} ref='foo' data-place={"bottom"} alt="StaffNotes" className={styles.img} onClick={open.bind(this)} style={{ padding: '1px' }} />}
                      </StaffNotes><span style={{ width: '40px' }}>Staff Notes</span></div> : null}
                {displaySquareIcons.includes('medicalConditions') || !shared ?
                  <div className={styles.imgText} style={{ width: '65px' }}>
                    <MedicalAlerts
                      allergyList={allergy}
                      allergyTitle="History of Allergies"
                      list={medicalCondition}
                      title="History of Medical Conditions"
                      notes="Note: The data above is of previous and current medical conditions and allergies. Data is captured when inputs are made to the medical conditions and allergies section in the Personal and Health History Information form."
                      icon={(medicalCondition && medicalCondition.length > 0) || (allergy && allergy.length > 0) ? 'medicalOrange' : 'medical'}
                      displayDate={firstVisit ? `Initial Visit: ${moment.utc(firstVisit).format('MM/DD/YYYY')}` : ''} >
                      {(open) => <img src={require(`img/patients/${(medicalCondition && medicalCondition.length > 0) || (allergy && allergy.length > 0) ? 'medicalOrange' : 'medical'}.svg`)} ref='foo' data-place={"bottom"} alt="Allergies" className={styles.imgC} onClick={open.bind(this)} />}
                    </MedicalAlerts> <span style={{ width: '64px' }}>Medical Alerts</span></div> : null}
                {displaySquareIcons.includes('bp') || !shared ?
                  <div style={{ width: '62px' }} className={styles.imgText}><img src={require(`img/patients/${bp_pulse_o2 == 'normal' ? 'pulse' : 'pulseOrange'}.svg`)} ref='foo' data-place={"bottom"} alt="Measurment History" className={styles.img} onClick={() => this.handleIcons('measurementHistory')} /><span style={{ width: '48px' }}>Vitals</span></div> : null}
                {!shared ?
                  <div style={{ width: '57px' }} className={styles.imgText}>
                    <SharedPatient
                      title="Shared Patients"
                      records={sharedPatientData}
                      patientId={id}
                      columns={schema().patients().sharedPatient}
                      path={path}
                      metaData={metaData}
                      updateError={this.props.updateError}
                      clearError={() => dispatch(actions.loadRecordsCacheHit())}
                      sharedAdminColumns={schema().patients().sharedAdmin}
                      sharedAdminRecord={sharedAdminPatientData}
                      infoColumns={schema().patients().sharedPatientInfo}>
                      {(open) => <img src={require(`img/patients/${shareIcon || sharedPatientData && sharedPatientData.length > 0 ? 'shareOrange' : 'share'}.svg`)} ref='foo' data-place={"bottom"} alt="Nova Shared Care" className={styles.img} onClick={open.bind(this)} style={{ padding: '4px' }} />}
                    </SharedPatient> <span style={{ width: '45px' }}>Shared Care</span></div> : null}
              </Col> : null}
            {this.state.alertMessage ?
              <AlertMessage success dismiss={() => { this.setState({ alertMessage: false }); this.recallPeriodForm.close(); browserHistory.push(`${path}/${id}/form`); }}><FormattedMessage {...messages.recallSuccess} /></AlertMessage> : null}
            {this.state.consultAlert ?
              <AlertMessage success checklist dismiss={() => { this.setState({ consultAlert: false }); browserHistory.push(`${path}/${id}/form`); }}><FormattedMessage {...messages.consultSuccess} /></AlertMessage> : null}
            {this.state.recallAlert ?
              <AlertMessage warning checklist dismiss={() => this.setState({ recallAlert: false })}><FormattedMessage {...messages.checklist} /></AlertMessage> : null}
            {this.state.requestConsultAlert ?
              <AlertMessage warning checklist dismiss={() => this.setState({ requestConsultAlert: false })}><FormattedMessage {...messages.requestConsult} /></AlertMessage> : null}
            {this.state.showAlert ?
              <AlertMessage email={email} xrayAlert dismiss={() => this.setState({ showAlert: false })}></AlertMessage> : null}
            {this.state.emptyDoseSpotAlert ?
              <AlertMessage doseSpot dismiss={() => this.setState({ emptyDoseSpotAlert: false })}></AlertMessage> : null}
            {this.state.invalidDoseSpotAlert ?
              <AlertMessage checklist dismiss={() => this.setState({ invalidDoseSpotAlert: false })}>{errorMessage}</AlertMessage> : null}
            {updateError ?
              <AlertMessage warning dismiss={() => dispatch(actions.loadRecordsCacheHit())}>{updateError}</AlertMessage> : null}

            {showPatientScreen && !DisablePatientSquare ?
              formSchema.map((schemas, schemaIndex) => {
                const mainSchemas = schemas.forms.sort(sortBy('order', true)).filter(_ => displaySquareIcons && displaySquareIcons.includes(_.id) && _.types === 'mainSquare' || !displaySquareIcons && _.types === 'mainSquare') || [];
                return (<Col key={schemaIndex}>
                  <Col lg={11} md={10} lgOffset={2} mdOffset={2} collapseLeft collapseRight>
                    {mainSchemas.map((form, formIndex) =>
                      <Col lg={2} md={2} xs={12} sm={3} key={formIndex} className={styles.patientTile}>
                        <Link className={`${styles.patientLink} ${styles.completed}`} to={{ pathname: `${path}/${id}/${form.id}/edit`, state: location.state }}>
                          <img src={require('img/patients/' + form.icon + '.svg')} alt={form.title} className={styles.tile} />
                          <Col className={styles.formTitle}>{form.title}</Col>
                        </Link>
                      </Col>)}
                  </Col>
                </Col>)
              }) : null}
            {showPatientScreen && !this.state.showFrame && !showIcons ?
              <Col xs={12}>
                <EditPatientForm
                  id={record.id}
                  name={name}
                  path={path}
                  initialValues={getInitialValues(record, steps, '1A', formSchema, record.id)}
                  formSchema={formSchema}
                  form={`editPatient.${record.id}`}
                  onSubmit={handleEdit.bind(this)}
                  metaData={record.metaData}
                  formId={'1A'}
                  locationState={location.state}
                  dispatch={dispatch}
                  printUrl={`${process.env.API_URL}/treatments/${record.id}/pdf?X-Auth-Token=${pdfAuthToken}`}
                  disable={viewOnly}
                  shared={shared}
                  lastInvitationSent={lastInvitationSent}
                  disableHeader={false}
                  style={{ padding: '25px' }}
                  platform={user && user.platform}
                  disableHeight={true}
                  roundBorder={true}
                />
              </Col> :  showIcons != 'toothChart' && showIcons != 'clinicalNotes' && showIcons != 'prescriptions' && showIcons != 'measurementHistory' && showIcons != 'periochart' && showIcons != 'endochart'  && this.state.showFrame == false ?
               <Col xs={12}>
               <EditPatientForm
                id={record.id}
                name={name}
                path={path}
                initialValues={getInitialValues(record, steps, formshowid, formSchema, record.id)}
                formSchema={formSchema}
                form={`editPatient.${record.id}`}
                onSubmit={handleEdit.bind(this)}
                metaData={record.metaData}
                formId={formshowid}
                locationState={location.state}
                dispatch={dispatch}
                printUrl={`${process.env.API_URL}/treatments/${record.id}/pdf?X-Auth-Token=${pdfAuthToken}`}
                disable={viewOnly}
                shared={shared}
                disableHeader={false}
                style={{ padding: '25px' }}
                platform={user && user.platform}
                disableHeight={true}
                roundBorder={true}
              /></Col>:null}
            <Col xs={12}>
              {this.state.showFrame ? <Col xs={12} className={styles.frameClose}>
                {showPatientScreen ?
                  <Icon glyph="icon-fontello-popup" style={{ paddingRight: '15px' }} onClick={handleOpenXray.bind(this)} /> :
                  <Icon glyph="icon-fontello-minus" style={{ paddingRight: '15px' }} onClick={handleOpenXray.bind(this)} />}
                <Icon glyph="icon-fontello-cancel-circled2" onClick={handleFrameClose.bind(this)} />
              </Col> : null}
              <iframe id="iframe-link" className={styles.iframe} />
            </Col>
            {showPatientScreen && !this.state.showFrame  ?
              <Col xs={12}>
                {showIcons &&  showIcons === 'toothChart' ?
                  <div>
                    <ToothChart
                      toothChartUpdatedDate={toothChartUpdatedDate}
                      record={toothChartData[0]}
                      patientId={id}
                      primaryProvider={primaryProvider}
                      columns={schema().patients().ToothChart}
                      planColumns={schema().patients().ToothTreatmentPlan}
                      infoColumns={schema().patients().ImplantExamImaging}
                      examImagingdata ={map().patients().implantExamImaging}
                      infoExamImaging={examImaging}
                      metaData={Object.assign({}, metaData, {codeNumber: metaData.cdcCode}, {codeDescription: metaData.procedureName}, {procedureCode: metaData.cdcCode}, {procedureDescription: metaData.procedureName})}
                      handleClose={() => this.setState({ showIcons: false })}
                      disable={viewOnly} />
                    <TreatmentPlan
                      title="Treatment Plan"
                      user={user}
                      chartNumber={patientId}
                      patientName={patientName}
                      medicalCondition={medicalCondition && medicalCondition.length > 0 ? medicalCondition.map(item => item.value) : ""}
                      email={email}
                      phone={phone}
                      referredBy={referredBy}
                      records={treatmentPlanData}
                      patientId={id}
                      schema={schema().patients()}
                      recordsItem={treatmentPlanDetails}
                      handleClose={() => this.setState({ showIcons: false })}
                      path={path}
                      metaData={metaData && metaData.dentalCarrier && Object.assign({}, metaData, {dentalCarrier: metaData.dentalCarrier.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[ {value: -1, label: 'UCR' }, {value: 0, label: 'Private' }])}) || metaData}
                      updateError={this.props.updateError}
                      clearError={() => dispatch(actions.loadRecordsCacheHit())}
                      disable={viewOnly} />
                    <ClinicalNotes
                      id={id}
                      birthDate={birthDate}
                      records={clinicalNotesData || []}
                      title="Clinical Notes"
                      columns={schema().clinicalNotes().columns}
                      metaData={Object.assign({}, metaData, {assistantOptions: appMetaData.assistantList})}
                      disable={viewOnly}
                      accessLevel={accessLevel}
                      displaySquareIcons={displaySquareIcons}
                      dispatch={dispatch}
                      name={name}
                      user={user}
                      path={path}
                      location={location.state}
                      handleClose={() => this.setState({ showIcons: false })}
                      onPrescriptionsClick={this.openPrescriptions}
                      updateError={this.props.updateError}
                      clearError={() => dispatch(actions.loadRecordsCacheHit())}
                      allergy={allergy.filter(a => a.currentlySelected)}
                      medicalCondition={medicalCondition.filter(a => a.currentlySelected)} />
                  </div>
                    : showIcons && showIcons === 'measurementHistory' ?
                      <ReactTable
                        title="Measurement History"
                        notes="Note: Data above is captured when inputs are made to the corresponding measurement sections in the Personal and Health History Information form."
                        className="measurementHistory"
                        handleClose={() => this.setState({ showIcons: false })}
                        records={measurementHistory ? measurementHistory : []}
                        columns={schema().patients().measurementHistory}
                        infoColumns={schema().patients().measurementInfo}
                        loadRecord={() => dispatch(loadMeasurementHistory(id))} />
                    : showIcons && showIcons === 'periochart' ?
                    <PerioChart
                     toothChartData={toothChartData[0]}
                     records={perioChartData}
                     patientId={id}
                     metaData={metaData}
                     handleClose={() => this.setState({ showIcons: false })}
                     columns={schema().patients().PerioChart}
                     disable={viewOnly} >
                     {(open) => <img src={require(`img/patients/periochart.svg`)} ref='foo' data-place={"bottom"} alt="Perio Chart" className={styles.img} onClick={open.bind(this)} />}
                   </PerioChart>  : showIcons && showIcons === 'endochart' ?
                    <EndoChart
                     toothChartData={toothChartData[0]}
                     records={endoChartData}
                     patientId={id}
                     metaData={metaData}
                     handleClose={() => this.setState({ showIcons: false })}
                     columns={schema().patients().EndoChart}
                     disable={viewOnly} >
                     {(open) => <img src={require(`img/patients/endochart.svg`)} ref='foo' data-place={"bottom"} alt="Perio Chart" className={styles.img} onClick={open.bind(this)} />}
                   </EndoChart> : showIcons && showIcons === 'prescriptions' ?
                    <Prescriptions
                    profileColumns={schema().prescriptions().profile}
                    title="Prescriptions"
                    user={user}
                    columns={prescriptionsColumns}
                    prescriptionsColumns={schema().patients().prescriptionsForm}
                    handleClose={() => this.setState({ showIcons: false })}
                    path={path}
                    patientId={id}
                    metaData={metaData}
                    filteredProvider={primaryProvider}
                    practice={record.dentist}
                    records={prescriptionHistory}
                    dispatch={dispatch}
                    actions={actions}
                    patient={patient}
                    recordsMetaData={recordsMetaData}
                    updateError={this.props.updateError}
                    disable={viewOnly}
                    allergy={allergy.filter(a => a.currentlySelected)} >
                  </Prescriptions> : showIcons && showIcons === 'clinicalNotes' ?
                  <ClinicalNotes
                  id={id}
                  birthDate={birthDate}
                  records={clinicalNotesData || []}
                  title="Clinical Notes"
                  showIcons='clinicalNotes'
                  columns={schema().clinicalNotes().columns}
                  metaData={Object.assign({}, metaData, {assistantOptions: appMetaData.assistantList})}
                  disable={viewOnly}
                  accessLevel={accessLevel}
                  displaySquareIcons={displaySquareIcons}
                  dispatch={dispatch}
                  name={name}
                  user={user}
                  path={path}
                  location={location.state}
                  handleClose={() => this.setState({ showIcons: false })}
                  onPrescriptionsClick={this.openPrescriptions}
                  updateError={this.props.updateError}
                  clearError={() => dispatch(actions.loadRecordsCacheHit())}
                  allergy={allergy.filter(a => a.currentlySelected)}
                  medicalCondition={medicalCondition.filter(a => a.currentlySelected)} />
                  :null
                }
              </Col> : <Col xs={12} style={{ minHeight: '0px' }}></Col>}
          </Col>
        </Row>
      );
    }

  }


  function getInitialValues(record, steps, formId, formSchema, patientId) {
    const { patient = {} } = record;
    const stepValues = steps && steps.length > 0 ? steps.filter(c => c.formId == formId).reduce((obj, c) => Object.assign(obj, c), {}) : {};
    let schemaValues;
    formSchema.map((form) => {
      form['forms'].map((schema, index) => {
        if (form['forms'][index]['id'] == formId) {
          schemaValues = form['forms'][index];
        }
      })
    })
    const parsedValues = Object.keys(stepValues).length > 0 ?
      Object.keys(stepValues.value)
        .filter(key => key)
        .reduce((obj, key) => {
          obj[key] = parseStructure(stepValues['value'][key]);
          return obj;
        }, {}) : {};
    parsedValues.patient = patient;
    parsedValues.id = record.id;
    if(formId === '1A' && parsedValues.carrierName == undefined) {
      parsedValues.carrierName = "-1";
    }
    parsedValues.steps = [{
      formId: stepValues.formId ? stepValues.formId : schemaValues.id,
      formVersion: stepValues.formVersion ? stepValues.formVersion : schemaValues.version
    }];
    if (formId === '1A' && (!parsedValues.patient_id || parsedValues.patient_id == '')) {
      parsedValues.patient_id = '';
    }
    return parsedValues;
  }

  function handleIcons(name) {
    this.setState({ showIcons: name })
  }

  function handleMainIcons(name, formid) {
    this.setState({ showIcons: name, formshowid:formid })
  }

  function handleFrameClose() {
    const { showPatientScreen } = this.state;
    const iframe = document.getElementById('iframe-link');
    if (iframe) {
      this.setState({ showFrame: false});
      iframe.style.display = 'none';
    }
    this.setState({ showPatientScreen: true });
    this.handleMainIcons(false,"");
  }

  function handleOpenXray() {
    const { showPatientScreen } = this.state;
    this.setState({ showPatientScreen: !this.state.showPatientScreen });
  }

  function handleRecallAlert() {
    const { recallAlert } = this.state;
    this.setState({ recallAlert: true });
  }

  function handleConsultAlert() {
    const { requestConsultAlert } = this.state;
    this.setState({ requestConsultAlert: true });
  }

  function handleDoseSpot(id) {
    const { record = {} } = this.props;
    const { patient = {} } = record;
    const { country } = patient;
    const countryArr = ['United States of America','USA','United States','U.S','US','America'].map(v => v.toLowerCase().replace(/\s/g, ""))
      if(country != undefined && country != null && country != '' && countryArr.includes(country.toLowerCase().replace(/\s/g, ""))) {
        getDoseSpotUrl(id).then(url => {
          const iframe = document.getElementById('iframe-link');
          if (iframe) {
            iframe.onload = () => {
              const iframeUrl = iframe.getAttribute('src');
                this.setState({ showFrame: true});
                iframe.style.display = 'block';
            }
            iframe.src = url[0].message;
          }
        })
        .catch((error) => {
          const errorResponse = JSON.parse(JSON.stringify(error));
          if(errorResponse != undefined && errorResponse != '') {
            const errorText = errorResponse.response.data.map(( code ) => code.message)
            this.setState({ invalidDoseSpotAlert: true, errorMessage: errorText[0] });
          } else {
            this.setState({ invalidDoseSpotAlert: true, errorMessage: "There was an error while opening DoseSpot. Please try again." });
          }
        })
      } else {
        this.setState({ invalidDoseSpotAlert: true, errorMessage: "Electronic prescription is available only for the patients residing in the USA." });
      }

  }

  function handleEClaims() {
    window.open("https://dental.changehealthcare.com/dps/securelogin.aspx", '_blank')
    /*const iframe = document.getElementById('iframe-link');
    if (iframe) {
      iframe.onload = () => {
        const iframeUrl = iframe.getAttribute('src');
          this.setState({ showFrame: true});
          iframe.style.display = 'block';
      }
      iframe.src = "https://www.emdeondental.com/dps/securelogin.aspx";
    }*/
  }

  function handleFrame(url) {
    if(this.state.showFrame == false){
      const iframe = document.getElementById('iframe-link');
      if (iframe) {
        iframe.onload = () => {
          this.setState({ showFrame: !this.state.showFrame });
          iframe.style.display = iframe.style.display === 'block' ? 'none' : 'block';
        }
        iframe.src = url;
      }
    }
  }

  async function handleScratchPad(data) {
    const record = data.toJS();
    if (record.scratchPad) {
      record.scratchPad = await uploadSignature(record.scratchPad);
    }
    this.scratchPadForm.close();
    this.props.dispatch(updateScratchPad(record));
  }

  function onScroll() {
    const elmnt = document.getElementById("patientFormBody");
    const patientScroll = elmnt.scrollTop;
    {/*this.setState({
      showOnScroll: (patientScroll > 60) ? true : false
    });*/}
  }

  function getTreatment(steps, type)
  {
    const treatmentColor = { c6a1: 'EA6225', c6a2: '228B22', c6a3: 'fff436', c6a4: 'FF69B4', c6a5: 'FF0000', c6a6: '00008B', c6a7: '800080', c6a8: '008080' }
    const stepValues = steps && steps.length > 0 ? steps.filter(c => c.formId == '2D1').reduce((obj, c) => Object.assign(obj, c), {}) : {};
    const parsedValues = Object.keys(stepValues).length > 0 ?
      Object.keys(stepValues.value)
        .filter(key => key)
        .reduce((obj, key) => {
          obj[key] = parseStructure(stepValues['value'][key]);
          return obj;
        }, {}) : {};

    const { treatment } = parsedValues;
    return treatment && treatment.selectedRadioTitle && type == 'title' ? treatment.selectedRadioTitle : treatment && treatment.selectedRadio && type == 'color' ? treatmentColor[treatment.selectedRadio] : 'NA';
  }

  function getCompletedStatus(steps, formId) {
    const stepValues = steps && steps.length > 0 ? steps.filter(c => c.formId == formId).reduce((obj, c) => Object.assign(obj, c), {}) : {};
    return stepValues.state == 'Completed' ? true : false;
  }


  function mapStateToProps(state, props) {
    const selectedRecords = selectRecords()(state, props);
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const recordsMetaData = selectRecordsMetaData()(state, props);
    const formSchemaRecord = selectFormSchema('patients')(state, props);
    const user = selectUser()(state, props);
    const loading = selectLoading()(state, props);
    const pageLoader = pageLoading()(state, props);
    const error = selectError()(state, props);
    const updateError = selectUpdateError()(state, props);
    const externalRecords = user && user.xvWebURL && user.xvWebURL != '' && selectExternalRecords && selectExternalRecords()(state,props) || false;
    //const externalRecords = user && user.xvWebURL && user.xvWebURL != '' && selectExternalRecords && selectExternalRecords();(state,props) || false;

    return {
      records: selectedRecords ? selectedRecords.toJS() : false,
      recordsMetaData: recordsMetaData && recordsMetaData.toJS() || {},
      record: selectedRecord ? selectedRecord.toJS() : { recordDidNotInit: true },
      formSchema: formSchemaRecord ? formSchemaRecord.toJS() : [],
      user,
      loading: loading,
      pageLoader: pageLoader,
      error: error,
      updateError: updateError,
      externalRecords: externalRecords ? externalRecords.toJS() : []
    };
  }

  return connect(mapStateToProps)(ViewPatientsForm)
}
