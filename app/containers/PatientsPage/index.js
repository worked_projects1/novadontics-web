/*
 * PatientsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';
import ModalForm from 'components/ModalRecordForm';
import { reset } from 'redux-form';

import Spinner from 'components/Spinner';
import TableWrapper from 'components/TableWrapper';

import { FormattedMessage } from 'react-intl';
import messages from '../RecordsPage/messages';

import styles from './styles.css';

import { Alert, Button, Col, PanelBody, PanelContainer, Row } from '@sketchpixy/rubix';
import { Grid } from '@sketchpixy/rubix/lib/index';
import { setApiFilter } from 'utils/api';
import { selectUser,selectCreatePatient } from 'blocks/session/selectors';
import schema from 'routes/schema';
import moment from 'moment';
import { loadPatientTypeStatus, createPatientInvite } from 'blocks/patients/remotes.js';
import Filter from './components/Filter/index.js';
import AlertMessage from 'components/Alert';

const reportSchema = schema().patients().report || [];
export default function (name, path, create, columns, actions, { selectors , selectFormSchema}, reactTable) {
  const {
    selectLoading,
    selectRecords,
    selectRecordsMetaData,
    selectError,
    selectUpdateError,
    selectHeaders
  } = selectors;

  class PatientsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
      records: PropTypes.array,
      children: PropTypes.object,
      error: PropTypes.bool,
      updateError: PropTypes.string,
      loading: PropTypes.bool
    };

    constructor(props){
      super(props);
      this.state = { patientsColStyle:true, inviteSuccess: false, inviteFailed: false, inviteMessage: '', report: false, showRecords: false, selectedRecords: [], patientTypeStatus: [], showAlert: false, showFilter: false, selectedStatus: [], selectedType: [] }
    }
    

    componentDidMount() {
      const { dispatch, records, formSchema, headers = {}, user = {}} = this.props;
      if (dispatch) {
        dispatch(actions.loadFormSchema());
      }
      if (dispatch && records.length === 0) {
        setApiFilter(Object.keys(headers).filter(_ => _ != "content-length").reduce((r, k)=>Object.assign({}, r, {[k]:headers[k]}), {}));
        dispatch(actions.loadRecords());
        const interval = setInterval(() => {
          const { dispatch, params = {}, records } = this.props;
          if(params.id && records && records.length > 0){
            dispatch(actions.loadRecord(parseInt(params.id, 10)));
            clearTimer();
          }
        }, 300);
        const clearTimer = () => clearInterval(interval);
      }
      this.setState({ selectedRecords: records });
      if(user.platform === 'iPad'){
        document.getElementById("body").style.marginTop = "0px";
      }
      loadPatientTypeStatus().then(patientTypeStatus => this.setState({ patientTypeStatus: patientTypeStatus })).catch(() => this.setState({ patientTypeStatus: [] }));
    }

    shouldComponentUpdate(nextProps, nextState) {
      return shallowCompare(this, nextProps, nextState);
    }

    render() {
      const { error, updateError, loading, children, location = {}, route = {}, records, metaData = {}, dispatch, headers, createPatient, user = {} } = this.props;
      const { patientTypeList } = metaData;
      const { report, showRecords, selectedRecords, patientTypeStatus, showFilter, selectedStatus, patientsColStyle,selectedType, inviteSuccess, inviteFailed, inviteMessage } = this.state;
      const { pathname } = location;
      const fullView = pathname.match( /(form|create|edit)/ ) != null ? true : false;
      const formView = pathname.match( /(form)/ ) != null ? true : false; 	
      // This one is pretty specific for PatientsPage
      const [status] = records.map((s) => s.status);
      let displayName = name;
      return (
        <div>
          <Helmet title="Novadontics"/>
          <div className={styles.view}>
            {!status && !fullView?
              <Grid>
                <Row>
                  <Col sm={12} className={styles.flexibleRow}>
                    <div>
                      <h1 className={styles.titleHead}>{displayName}</h1>
                    </div>
                    <div className={styles.flexibleItems}>
                      <img src={require(`img/patients/FilterIcon.svg`)} className={styles.img} ref='foo' data-place={"bottom"} data-tip={"Filter"} onClick={() => this.setState({ showFilter: true })} />
                    {create ?
                      <div className={styles.flexible}>
                        <ModalForm
                          initialValues = {{}}
                          fields={report ? reportSchema.filter(a => a.value != 'patientType') : reportSchema.filter(a => a.value != 'fromDate' && a.value != 'toDate')}
                          form={`modalReport`}
                          onChange={this.handleChange.bind(this)}
                          onSubmit={this.handleReport.bind(this)}
                          metaData={Object.assign({}, metaData, {patientTypeList: patientTypeList && patientTypeList.map(el => {return { 'label' : el.listOptions, 'value' : el.listOptions}}) || []})}
                          config={{title:'Patient Reports', width: '37px', height: '37px', btnType: "image", imageIcon: [require(`img/patients/reportOrange.svg`)]}}
                        />
                      {user.onlineRegistrationModule ?
                        <ModalForm
                          fields={schema().patients().patientInvite}
                          form={`patientInvite`}
                          onRef={inviteForm => (this.inviteForm = inviteForm)}
                          onSubmit={this.handleInvite.bind(this)}
                          config={{ title:'Online Registration', inviteButton: true, hideCancelBtn: true, btnName: "Send sms / email invite" }}
                        /> : <div style={{ marginRight: '20px' }}><Button bsStyle="standard" onClick={() => this.setState({ showAlert: true })}>Online Registration</Button></div>}
                        {create ? <Link style={!createPatient ? {pointerEvents:"none"} : null } to={{ pathname: `${path}/create`, state: location.state }}>
                          <Button bsStyle="standard" disabled={!createPatient}>Create a New Patient</Button>
                        </Link> : null}
                      </div>
                      : null}
                    </div>
                  </Col>
                </Row>
              </Grid>
              : null}
            <Row>
	          {!fullView ? 
		        <Col md={children ? 4 : 12} lg={children ? 6 : 12} className={children ? 'col-md-collapse-right hidden-print' : null}>
                {error ?
                  <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
                  <div>
                    {status && !reactTable?
                      columns.map((column) =>
                        (column.oneOf && column.splitColumn ? column.oneOf.map((prop) =>
                          <div>
                            <h1 className={styles.title}>{prop} {name}</h1>
                            <TableWrapper
                              records={records.filter((record) => (record.status) === prop)}
                              columns={columns}
                              children={children}
                              path={path}
                              name={prop}
                              patientsColStyle={patientsColStyle}
                            />
                          </div>
                        ) : null)
                      ) :
                      <TableWrapper
                        records={showRecords ? selectedRecords : records}
                        columns={columns}
                        children={children}
                        path={path}
                        name={name}
                        locationState={location.state}
                        headers={headers}
                        actions={actions}
                        dispatch={dispatch}
                        reactTable={reactTable}
                        patientsColStyle={patientsColStyle}
                      />}
                  </div>}
                  <Filter show={showFilter} hide={() => this.setState({ showFilter: false })} patientTypeStatus={patientTypeStatus} selectedStatus={selectedStatus} selectedType={selectedType} onChange={(inputStatus, inputType) => this.handleSearch(inputStatus, inputType)} />
                {loading ? <Spinner/> : null}
	            </Col> : null }
              {children ?
                <Col md={!fullView ? 8 : 12} lg={!fullView ? 6 : 12} className={formView ? `patient-block patient-formView` : fullView ? `patient-block` : 'print-ready'} style={{ marginBottom: 76 }}>
		              <PanelContainer className={!create && status ? styles.space : null}>
                    <PanelBody>
                      <Col sm={12}>
                        {children}
                      </Col>
                    </PanelBody>
                  </PanelContainer> 
                </Col> : null }
              {this.state.inviteSuccess ?
                <AlertMessage success dismiss={() => { dispatch(actions.loadRecords()); this.setState({ inviteSuccess: false }); }}>{inviteMessage}</AlertMessage> : null}
              {this.state.inviteFailed ?
                <AlertMessage warning dismiss={() => { this.setState({ inviteFailed: false }); }}>{inviteMessage}</AlertMessage> : null}
              {this.state.showAlert ?
                <AlertMessage onlineReg dismiss={() => this.setState({ showAlert: false })}></AlertMessage> : null}
            </Row>
          </div>
        </div>
      );
    }

    handleSearch(inputStatus, inputType) {
      const { records } = this.props;
      const { showRecords, selectedRecords } = this.state;
      if(inputStatus.length != 0){
        const statusFiltered = records.filter(x => inputStatus.includes(x.patientStatus));
        this.setState({ showRecords: true, selectedRecords: statusFiltered });
        if(inputType.length != 0){
          let typeFiltered = []
          const filteredData = statusFiltered.map(x => {
            x.patientType.split(',').map(y => {
              if(inputType.includes(y)){
                typeFiltered.push(Object.assign({}, x));
              }
            })
          })
          let jsonObject = typeFiltered.map(JSON.stringify);
          const uniqueSet = new Set(jsonObject);
          const uniqueArray = Array.from(uniqueSet).map(JSON.parse);
          this.setState({ showRecords: true, selectedRecords: uniqueArray });
        }
      } else if(inputStatus.length == 0 && inputType.length != 0){
        let typeFiltered = []
        const filteredData = records.map(x => {
          x.patientType.split(',').map(y => {
            if(inputType.includes(y)){
              typeFiltered.push(Object.assign({}, x));
            }
          })
        })
        let jsonObject = typeFiltered.map(JSON.stringify);
        const uniqueSet = new Set(jsonObject);
        const uniqueArray = Array.from(uniqueSet).map(JSON.parse);
        this.setState({ showRecords: true, selectedRecords: uniqueArray });
      } else {
        this.setState({ showRecords: false });
      }
    }

    handleChange(data = {}) {
      const formData = data.toJS();
      const { type = false } = formData;
      this.setState({ report: type && (type === 'Recall Report' || type === 'Referral Source Report' || type === 'New Patient Report') ? true : false})
    }

    handleInvite(data, dispatch, { form }) {
      let submitData = {};
      let submitRecord = {};
      submitRecord.formId = '1A';
      submitRecord.formVersion = 1;
      submitRecord.state = 'Completed';
      submitRecord.value = data.toJS();
      submitData.steps = [ submitRecord ];
      createPatientInvite(submitData)
        .then(response => {
          this.inviteForm.close();
          this.inviteForm.props.reset();
          this.setState({ inviteSuccess: true, inviteMessage: 'Patient invited successfully' })
        })
        .catch(() => this.setState({ inviteFailed: true, inviteMessage: 'There was an error while inviting a patient. Please try again.' }));
    }

    handleReport(data) {
      const { user } = this.props;
      const pdfAuthToken = user && user.secret || user && user.token;
      const date = moment().format('YYYY-MM-DD');
      const { type = 'Patient Report', patientType = false, format='PDF', fromDate = date, toDate = date } =  data.toJS();
      const route = (type === 'New Patient Report' ? 'newPatient': type === 'Referral Source Report' ? 'referralSource' : type === 'Recall Report' ? 'recallReport' : 'PatientType');
      window.open(`${process.env.API_URL}/${route}${format}?X-Auth-Token=${pdfAuthToken}&&fromDate=${fromDate}&&toDate=${toDate}&&patientType=${patientType}`);
    }

  }


  function mapDispatchToProps(dispatch) {
    return {
      dispatch
    };
  }

  return connect(
    createSelector(
      selectLoading(),
      selectRecords(),
      selectRecordsMetaData(),
      selectError(),
      selectUpdateError(),
      selectFormSchema('patients'),
      selectHeaders(),
      selectUser(),
      selectCreatePatient(),
      (loading, records, metaData, error, updateError, formSchema, headers, user,createPatient) => ({
        loading,
        records: records ? records.toJS() : [],
        metaData: metaData ? metaData.toJS() : {},
        error,
        updateError,
        formSchema: formSchema ? formSchema.toJS() : [],
        headers: headers ? headers.toJS() : false,
        user,
        createPatient
      })
    ),
    mapDispatchToProps
  )(PatientsPage);
}
