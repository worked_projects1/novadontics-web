/**
*
* Info
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Image, Modal, Row } from '@sketchpixy/rubix';
import ReactTooltip from 'react-tooltip';
import {findDOMNode} from 'react-dom';
import Lightbox from 'react-image-lightbox';
import styles from './styles.css';

class Info extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showModal: false, isOpen: false };
  }

  static propTypes = {
    data: PropTypes.object,
  };

  render() {
    const { showModal, isOpen } = this.state; 
    const { data = {}, noteStyle } = this.props;
    const { attributes = {} } = data;
    const info =  attributes && attributes.info ? attributes.info : data && data.info ? data.info : false;
    const image = info && info.image ? info.image : false;
    const openModal = info && info.openModal ? info.openModal : false;
    const text = info && info.text ? info.text : false;

    const handleOpen = () => {
      if(image && !text && !openModal){
        window.open(info && info.image ? info.image : data.info.image, '_blank');
      } else if((image && text) || (text && !image && text.length > 400) || (image && openModal) || (text && openModal)){
        this.setState({ showModal: true }) 
      } 
    }

    return (
      data ? <Col className={data.labelStyle ? styles.marginStyle : null}>
      <Image src={require('img/patients/info.svg')} className={noteStyle ? styles.noteInfo : styles.Info} ref='foo' data-place={"bottom"} data-tip={text && !openModal && !image && text.length <= 400 ? `<p style="width:400px;">${text}</p>` : null} data-html={true} onClick={handleOpen}/>
	      <ReactTooltip type="info" html={true} />
        <Modal className="info-modal" show={showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>
              <h4 style={info.labelStyle ? {color: '#EA6225', fontWeight: 'bold'} : null}>{info && info.title ? info.title : data && data.title ? data.title : null}</h4>
            </Modal.Title>
          </Modal.Header>
           <Modal.Body className={styles.ModalBody}>
             <Row>
              {image && text ? 
              <Col>	
                  <Col sm={7} className={styles.html} dangerouslySetInnerHTML={{__html: text}} />
                  <Col sm={5}>
                    <Image src={image} responsive onClick={()=>this.setState({isOpen: true})} />
                    <Col className={styles.enlarge}>Click to Enlarge</Col>
                    {isOpen ? <Lightbox
                      imageTitle="image"
                      mainSrc={image}
                      nextSrc={image}
                      prevSrc={image}
                      onCloseRequest={() => this.setState({isOpen: false})}
                     /> : null}
                  </Col> 
                </Col> : text ?
                <Col sm={12} dangerouslySetInnerHTML={{__html: text}} />:
                image && openModal ?
                <Col sm={12}>
                <Image src={image} responsive onClick={()=>this.setState({isOpen: true})} />
                </Col>	
              : null }	
	           </Row> 
          </Modal.Body>
         </Modal> 
      </Col> : null
    );
  }

  close() {
    this.setState({ showModal: false });
  }
}

export default Info;
