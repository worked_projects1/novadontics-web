/**
*
* WebForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Button } from '@sketchpixy/rubix';
import { Link } from 'react-router';

import styles from './styles.css';

class WebForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };
  render() {
    const { data, path, optionsMode } = this.props;
    return (
      optionsMode === 'edit' ? <Col> 
        <Link to={`${path}/${data.FormId}/edit`}>
          <Button bsStyle="standard" className={styles.actionButton}>{data.title}</Button>
        </Link>
      </Col> : null
    );
  }
}

export default WebForm;
