import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from "react-datepicker";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import styles from './styles.css';
import { Col } from '@sketchpixy/rubix';
import Info from '../Info';

class ModernDatepicker extends React.Component {
  static propTypes = {
    input: PropTypes.object,
   };

  constructor(props) {
    super(props);
    this.state = { startDate: false };
    this.handleDate = this.handleDate.bind(this);
  }

  componentDidMount() {
    const { input, type } = this.props;
    if(type === 'calendar'){
      $("#"+input.name.replace('.','')).focus();
    }
  }

  render() {
    const { startDate } = this.state;
    const { input, type, title, data, flexStyle } = this.props;
    
    return (
        <Col>
          <label htmlFor={title} style={flexStyle ? { display: 'flex' } : null}>
            {type === 'calendar' ? '' : title}
            {data && data.required ? <span style={{color:"#ea6225",fontSize:"25px"}}>*</span> : <span style={{fontSize: "25px"}}></span>}
            {data && data.mandatory ? <span>*</span> : null}
            {data.attributes && data.attributes.info ? <Info data={data} noteStyle={true} /> : null}
          </label>
          <DatePicker 
            selected={startDate ? startDate : input.value ? moment.utc(input.value) : data.defaultDate ? moment.utc() : null}
            format={type === 'calendar' ? 'DD MMM, YYYY' : 'MM/DD/YYYY'}    
            peekNextMonth
            showMonthDropdown
            showYearDropdown   
            dropdownMode="select"
            autoComplete="off"
            className={type === 'calendar' ? styles.DatePickerWithNotes : styles.DatePicker}
            onChange={(selected) => this.handleDate(selected)}
            id={input.name.replace('.','')}
          />
        {type === 'calendar' ? <input type="textarea" className={styles.textarea} {...input} value={input.value} /> : null}
        </Col>
    );
  }


  handleDate(selected) {
    const { input, type } = this.props;
    this.setState({ startDate: selected });
    if(selected != "" && selected != null) {
      const pickedDate = moment(selected).format(type === 'calendar' ? 'DD MMM, YYYY' : 'MM/DD/YYYY');
      const pickedTime = moment().format('h:mm A');
      input.onChange(type === 'calendar' ? `${pickedTime} on ${pickedDate}, Note:`: pickedDate);
    } else {
      input.onChange("");
    }
  }


}

export default ModernDatepicker;