
import InputField from '../InputField';
import RadioboxField from '../RadioboxField';
import CheckboxField from '../CheckboxField';
import PrintButton from '../PrintButton';
import InfoBlock from '../InfoBlock';
import SelectField from '../SelectField';
import Upload from '../Upload';
import SignatureField from '../SignatureField';
import WebForm from '../WebForm';
import Textarea from '../Textarea';
import DatePicker from '../DatePicker';
import SearchField from '../SearchField';
import TableField from '../TableField';
import ViewTableField from '../ViewTableField';
import TimeField from '../TimeField';
import MultiCheckBoxField from '../MultiCheckBoxField';
import MultiSelectField from '../MultiSelectField';
import FamilyMembers from '../FamilyMembers';
import Subscriber from '../Subscriber';

export const ImplementationFor = {
  Input: InputField,
  Checkbox: CheckboxField,
  PrintButton: PrintButton,
  InfoBlock: InfoBlock,
  Images : Upload,
  Videos : Upload,
  Audios : Upload,
  Select : SelectField,
  Radiobox : RadioboxField,
  Signature : SignatureField,
  WebForm: WebForm,
  Textarea: Textarea,
  Date: DatePicker,
  Search: SearchField,
  Table: TableField,
  ViewTable: ViewTableField,
  Time: TimeField,
  MultiCheckbox: MultiCheckBoxField,
  MultiSelect:MultiSelectField,
  familyMembers: FamilyMembers,
  subscriber: Subscriber
};
  
export const ContentTypes = {
  Images: 'image/*,application/pdf,.stl',
  Videos: 'video/mp4, video/*',
  Audios: 'audio/mp3, audio/*,video/mp4,video/*'
};