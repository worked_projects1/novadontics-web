/**
*
* CreatePatientForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm } from 'redux-form/immutable';
import { browserHistory } from 'react-router';
import {ImplementationFor, ContentTypes} from './utils';
import Info from '../Info';
import styles from './styles.css';

import { Row, Col, Button } from '@sketchpixy/rubix';

const CreatePatientForm = (props) => {
  const { handleSubmit, pristine, submitting, formSchema, path, metaData, platform, record = {} } = props;
  
  const url = new URL(window.location.href);
  const formId = url.searchParams.get("id") || props.formId || '1A';
  
  const renderSimpleField = (field) => {
    const Component = ImplementationFor[field.radio ? 'Radiobox' : field.type];
    const contentType = ContentTypes[field.type];
	  return ((field.type || field.radio) && Component ?
      <Component
        formId={formId}
        data={field}
        contentType={contentType}
        options={ field.required ? {general:{required: true}} : {}}
        optionsMode="create"
        metaData={metaData}
        path={path}
        record={record}
	    /> : null);
  }

  const renderGroupField = (columnItem) => {
      return columnItem.group.fields.map((field, fieldIndex) =>
          <Col sm={12} className={styles.columnItem} key={`columnItem-${fieldIndex}`}>
          {field && field.type ?
          renderSimpleField(field) : null }	
          </Col>)
  }

  const slider = (slideIndex) => {
    if(slideIndex != undefined){
      formSchema.map((section) =>
          section.forms.map((form) => {
              if(form.id.toString() == formId.toString()) {
                form.schema.map((schema, index) => {
                  if(`page-${index}` == `page-${slideIndex}`){  
                    document.getElementById(`page-${slideIndex}`).classList.remove(styles.dispnone);
                    document.getElementById(`slide-${slideIndex}`).classList.add(styles.activeSlider);
                  } else {
                    document.getElementById(`page-${index}`).classList.add(styles.dispnone);
                    document.getElementById(`slide-${index}`).classList.remove(styles.activeSlider);   
                  }
                }) 
              }
            }))  
      }      
  }


  return (
    <form onSubmit={handleSubmit}>
      {formSchema ? formSchema.map((section, i) =>
          section.forms.map((form) =>
              form.id.toString() == formId.toString() ? 
	              <Row key={`form-${form.id}`}>   	
                    {form.schema.map((schemaItem, schemaIndex) =>
                      <Col id={`page-${schemaIndex}`} className={schemaIndex > 0 ? styles.dispnone : null} key={`schemaItem-${schemaIndex}`}>
                          <Col sm={12} className={styles.header}>
                            <Link onClick={() => platform && platform === 'iPad' ? window.location = 'inapp://cancel' : browserHistory.goBack()}>
                              <Button bsStyle="standard" type="button" className={styles.btn}>Cancel</Button>
                            </Link>
                            <h4 className={styles.subtitle}>{schemaItem.title}</h4>
                            <Button bsStyle="standard" type="submit" disabled={pristine || submitting} className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Save</Button>
                          </Col>
                          <Col sm={12} className={styles.schemaBody} style={{height: `${platform && platform === 'iPad' ? document.body.clientHeight-70 : document.body.clientHeight-200}px`}}>
                            {schemaItem.rows.map((row, rowIndex) =>
                                <Col xs={12} className={styles.rowItem} id={`row-${rowIndex}`} key={`row-${rowIndex}`}>  
                                  {row.columns.map((column, columnIndex) =>
                                    <Col xs={12} sm={row.columns.length === 2 ? 6 : row.columns.length > 2 ? 4 : 12} key={`column-${columnIndex}-${columnIndex}`}>
                                      {column.columnItems.map((columnItem, columnItemIndex) =>
                                        <Col className={styles.columnItems} sm={12} key={`columnItems-${columnItemIndex}-${columnItemIndex}`}>
                                            {columnItem.group ?
                                            <Col sm={12} className={styles.groupTitle}>
						                                    { columnItem.group.title === "Patient Type" ? <div style={{fontSize:"18px"}}>{columnItem.group.title}<span style={{color:"#ea6225",fontWeight:"bold",fontSize:"20px"}}>*</span></div>: columnItem.group.title  ? <h4 dangerouslySetInnerHTML={{__html: columnItem.group.title}} /> : <Col className={styles.withoutTitle}></Col> }
                                                {columnItem.group.info ? <Info data={columnItem.group} /> : null}
					                                  </Col> : null}
                                            {(columnItem.field && columnItem.field.type) || (columnItem.group && columnItem.group.radio) ?
                                                renderSimpleField(columnItem.group && columnItem.group.radio ? columnItem.group : columnItem.field) : renderGroupField(columnItem) }
                                        </Col>
                                      )}
                                    </Col>
                                  )}
                                </Col> 
                            )}
                          </Col>
                        </Col>
                      )}  
                    {form.schema.length > 1 ? 
                      <Col className={styles.footer} sm={12} >
                        {form.schema.map((slideItem, slideIndex) =>
                          <div id={`slide-${slideIndex}`} key={`slideIndex-${slideIndex}`} className={slideIndex === 0 ? styles.activeSlider : null} onClick={()=>slider(slideIndex)} />
                        )}  
                    </Col> : null }
	              </Row> : null
            )
        ) : null}
    </form>
  );
};




CreatePatientForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  formSchema: PropTypes.array,
  path: PropTypes.string,
  metaData: PropTypes.object,
  dispatch: PropTypes.func,
};

export default reduxForm({
  form: 'createPatient'
})(CreatePatientForm);
