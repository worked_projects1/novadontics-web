/**
*
* MultiSelectField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field} from 'redux-form/immutable';
import Select from 'react-select';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class MultiSelectField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    metaData: PropTypes.object,
  };

  render() {
    const { data, inline, options = {}, optionsMode, metaData } = this.props;
    const { required } = options.general || {};
    const columnSize = inline || data.fullWidth ? 12 : 6;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }
    const isPreDefinedSet = Array.isArray(data.options);
    const normalize = data.number ? (value) => Number(value) : null;

    const renderOptions = isPreDefinedSet ? data.options.map((c) => { return { 'label' : c, 'value' : c} }) : metaData[data.options] ? metaData[data.options].map(a=> Object.assign({}, a, {value: a && a.value && a.value.toString() || ''})) : [];
    const renderMultiSelectField = ({ input, label, meta: { touched, error }, children }) => { 
      const values = typeof input.value === 'number' ? input.value.toString() : input.value;
      return (
        <Col xs={columnSize} className={data.removePadding ? styles.removePadding : null}>
              {!inline ?
                <label htmlFor={label}>
                  {label}
                  {required || extraFieldOptions.required ? <span>*</span> : null}
                </label> : null}
                <Select
                  id={input.name}
                  name={input.name}
                  defaultValue={renderOptions && values && renderOptions.filter((c)=> values.split(',').includes(c.value))}
                  onChange={this.handleChange.bind(this, input)}
                  options={renderOptions}
                  isMulti
                  className={styles.MultiSelectField}
                  placeholder={data.placeholder ? data.placeholder : ''}
                />	
            
        </Col>
    )}

		
    return ( 
        <Field
          name={data.name}
          id={data.name}
          label={data.title}
          component={renderMultiSelectField}
          normalize={normalize}
          onFocus={e => e.preventDefault()}
          onBlur={e => e.preventDefault()}
        />
    );
  }
  handleChange = (input, selectedOption) => {
    const value = selectedOption.length > 0 ? selectedOption.map((c) => { return c.value }).join(",") : '';
    input.onChange(value);
  }

}

export default MultiSelectField;

