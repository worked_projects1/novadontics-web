
import moment from 'moment';



export function getFileName(prev){
    return prev && typeof prev === 'string' && prev.substring(prev.lastIndexOf("/") + 1) || prev && typeof prev === 'object' && prev.url.substring(prev.url.lastIndexOf("/") + 1)
}


export function getExtension(prev){
    return prev && typeof prev === 'string' && prev.substring(prev.lastIndexOf('.') + 1) || prev && typeof prev === 'object' && prev.url.substring(prev.url.lastIndexOf('.') + 1) || false
}


export function getUploadDate(prev){
    const name = getFileName(prev);
    return name && moment(parseInt(name.substr(name.lastIndexOf('-') + 1).split('.')[0])).format("MM/DD/YYYY") || false
}

export function getFileType(prev) {
    const extension = getExtension(prev);
    if(['png', 'jpeg', 'jpg', 'gif', 'webp', 'ico'].includes(extension)){
        return 'image'
    } else if(['mov', 'mp4', 'ogg', 'wmv', 'webm', 'flv', 'mp'].includes(extension)){
        return 'video'
    } else if(extension === 'pdf'){
        return 'pdf'
    } else if(extension === 'stl'){
        return 'stl'
    } else {
        return 'file'
    }
}