/**
*
* Gallery
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Player } from 'video-react';
import { Col, Image, Icon,Button } from '@sketchpixy/rubix';
import DeleteButton from '../DeleteButton';
import { getFileName, getUploadDate, getFileType } from './galleryUtils';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import Info from '../Info';


const FileView = ({ name, uploadDate, type, preview, handleOpen }) => {
  switch (type) {
    case 'image':
      return <Col className={styles.previewImg} onClick={handleOpen}>
        <Image src={preview} responsive />
        <span className={styles.uploadDate}>{uploadDate || ''}</span>
      </Col>
    case 'video':
      return <Col className={styles.previewImg}  onClick={handleOpen}>
        <Image href={preview.url} className={styles.img} src={'https://s3.amazonaws.com/novadontics-uploads/dataurifile1584343276879-1584343277318.png'} responsive />
        <span className={styles.uploadDate}>{uploadDate || ''}</span>
        <span className={styles.play}>
          <Image width={60} src={require('img/catalog/play4.png')} />
        </span>
      </Col>
    default:
      return <Col className={styles.previewFile} onClick={handleOpen}>
        <Col>
          <Image src={require(`img/patients/${type === 'pdf' ? 'pdf3' : type}.svg`)} responsive />
          <span>{name.length > 25 ? name.slice(0, 25) + '...' : name}</span>
        </Col>
        <span className={styles.uploadDate}>{uploadDate || ''}</span>
      </Col>
  }
}


class Gallery extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.array,
    entity: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
    this.handleNext = :: this.handleNext;
    this.handlePrev = :: this.handlePrev;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { data = [], entity, uniqueID, singleImage, OnDelete,showtype,renderMode} = this.props;
    const { photoIndex, isOpen } = this.state;
    const nextImage = (photoIndex + 1) % data.length;
    const prevImage = (photoIndex + (data.length - 1)) % data.length;
    const fileType = getFileType(data[photoIndex]);
    const mainImage = fileType && (fileType === 'image' || fileType === 'video') ? data[photoIndex] : require(`img/patients/${fileType === 'pdf' ? 'pdf3' : fileType}.svg`);
    return (
      <Col id="files" className={showtype === 'button' ? styles.galleryBtn : styles.gallery}>
        {data.map((preview, index) => {

          const name = getFileName(preview);
          const uploadDate = getUploadDate(preview);
          const type = getFileType(preview);

          return <Col key={`${type}-${index}-${uniqueID}`} className={showtype ? styles.showicon : styles.preview}>
            {showtype === 'button' ?
              <Info data={{ attributes: { info: { image: preview, openModal: true } } }}  renderMode={renderMode} />:
           
              <FileView
              name={name}
              type={type}
              uploadDate={uploadDate}
              preview={preview}
              handleOpen={() => this.handleOpen(index)} /> }
          
          {showtype  && showtype === 'button' ?
            <Button bsStyle="link" bsSize="xs"  type="button" className={styles.btn,styles.showbtn} onClick={OnDelete && OnDelete(preview)}>
            <Icon glyph="icon-fontello-trash" showtype={showtype} className={styles.icon} />Delete</Button> :
            <DeleteButton type={type} OnDelete={OnDelete && OnDelete(preview)} />}
       </Col>
        })}

        {singleImage  && data.length > 0 ? null :
        showtype === 'button' ?
          <Button onClick={() => document.getElementById(`dropzone-${uniqueID}`).click()} bsStyle="standard" type="button" >Add Image </Button>:
          <Col onClick={() => document.getElementById(`dropzone-${uniqueID}`).click()} className={styles.upload}>
            <img src={require('img/patients/AddPhoto.svg')} alt={'upload'} className={styles.img} />
            <Col style={{ textTransform: 'capitalize' }}>{`Add ${entity}`}</Col>
          </Col>}

        {isOpen &&
          <Lightbox
            imageTitle="image"
            mainSrc={mainImage}
            nextSrc={data[nextImage]}
            prevSrc={data[prevImage]}
            onCloseRequest={this.handleClose}
            onMovePrevRequest={() => this.handlePrev(prevImage)}
            onMoveNextRequest={() => this.handleNext(nextImage)}
          />
        }
      </Col>
    );
  }

  handleOpen(i) {
    const { data = [] } = this.props;
    const file = data[i] || '';
    const type = getFileType(file);
    if (type === 'image') {
      this.setState({
        isOpen: true,
        photoIndex: i,
      });
    } else if(type === 'video'){
        BigPicture({
          el: document.getElementById('files'),
          vidSrc: file && file.url ? file.url : file
        });
    } else {
      window.open(file && file.url ? file.url : file, '_blank');
    }
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handlePrev(nextImage) {
    this.setState({ photoIndex: nextImage });
  }

  handleNext(prevImage) {
    this.setState({ photoIndex: prevImage });
  }
}

export default Gallery;
