/**
*
* Family Members
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Col, Button, Icon, Grid, Alert } from '@sketchpixy/rubix';
import styles from './styles.css';
import ReactTable from "react-table";
import schema from 'routes/schema';
import ConfirmButton from 'components/ConfirmButton';
import moment from 'moment';
import ModalForm from 'components/ModalRecordForm';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import { loadFamilyMembers, createFamilyMembers, updateFamilyMembers, deleteFamilyMembers } from "blocks/patients/familyMembers";
import selectors from 'blocks/records/selectors';
import EditRecordForm from 'components/EditRecordForm';

const columns = schema().patients();
const { familyMembers: fields, guarantorForm } = columns;
const patientSelectors = selectors('patients');
const {
    selectRecord,
    selectError,
    selectUpdateError
} = patientSelectors;

class FamilyMembers extends React.Component { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.state = { showModal: false, selectedType: false, error: false }
    }

    static propTypes = {
        data: PropTypes.object,
    };


    editableColumnProps = {
        Cell: props => {
            const { column, tdProps } = props;
            const { onDelete } = tdProps.rest;
            return (column.action ?
                <Col className={styles.actions}>
                    <ConfirmButton onConfirm={onDelete}>{(confirm) => <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={confirm.bind(this)}><Icon glyph="icon-fontello-trash" className={styles.icon} />Delete</Button>}</ConfirmButton>
                </Col> : <span>{column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : props.value}</span>);
        }
    };

    render() {
        const { id, data, optionsMode, record = {}, dispatch, metaData, location = {} } = this.props;
        const { familyMembersData = {} } = record;
        const { family = {}, familyMembers = [] } = familyMembersData;
        const { familyId, patientType } = family;
        const { selectedType } = this.state;

        const selPatientType = selectedType || patientType;
        const guarantorOptions = familyMembers && familyMembers.length > 0 && familyMembers.map(el => Object.assign({}, { value: el.patientId, label: el.name })) || [];
        const familyMembersList = familyMembers && familyMembers.length > 0 && familyMembers.filter(_ => _.patientId != id) || [];
        return (
            optionsMode === 'edit' && data.showView ? <Col>
                <Button onClick={this.open.bind(this)} bsStyle="standard" className={styles.actionButton}>{data.title}</Button>
                <Modal show={this.state.showModal} bsSize="large" backdrop="static" onHide={this.close.bind(this)} onEnter={this.handleOpen.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title className={styles.title}>
                            <span style={{ color: '#EA6225' }}>{data.title}</span>
                            <ModalForm
                                initialValues={{ id, familyId }}
                                fields={fields}
                                form={`createFamilyMembers`}
                                metaData={metaData}
                                onSubmit={this.handleSubmit.bind(this)}
                                config={{ title: `Add ${data.title}`, closeModal: true, showError: this.showError.bind(this) }}
                            />
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col>
                            {this.state.error ?
                                <Alert danger>{this.state.error}</Alert> : null}
                        </Col>
                        <ReactTable
                            columns={fields.filter(a => a.viewRecord).map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                            ))}
                            data={familyMembersList}
                            getTdProps={(state, rowProps) => {
                                return {
                                    onDelete: this.handleDelete.bind(this, rowProps && rowProps.original.patientId, familyId)
                                }
                            }}
                            pageSize={familyMembersList && familyMembersList.length > 0 ? 8 : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        />
                        <Col>
                            <div><h4>Choose your Guarantor:</h4></div>
                            <input name="patientType" type="radio" checked={(selPatientType && selPatientType === 'patient') || !selPatientType ? true : false} value="patient" onChange={(e) => this.setState({ selectedType: e.target.value })} style={{ width: '30px' }} />Patient
                            <input name="patientType" type="radio" checked={selPatientType && selPatientType === 'non_patient' ? true : false} value="non_patient" onChange={(e) => this.setState({ selectedType: e.target.value })} style={{ width: '30px' }} />Non Patient
                        </Col>
                        <EditRecordForm
                            initialValues={Object.assign({}, { ...family }, { patientId: family.guarantorId, id })}
                            fields={guarantorForm.filter(el => ((!selPatientType || selPatientType === 'patient') && el.value === 'patientId') || (selPatientType === 'non_patient' && el.value != 'patientId'))}
                            form={`guarantor_${id}`}
                            onSubmit={this.handleUpdate.bind(this)}
                            metaData={Object.assign({}, { ...metaData }, { guarantorOptions })}
                            locationState={location.state}
                            record={record}
                            disableCancel={true}
                        />
                    </Modal.Body>
                </Modal>
            </Col> : null
        );
    }


    open() {
        this.setState({ showModal: true });
    }

    close() {
        this.setState({ showModal: false })
    }

    showError(error) {
        this.setState({error});
        setTimeout(() => this.setState({error: false}), 3000);
    }

    handleSubmit(data, dispatch, { form }) {
        const submitRecord = data.toJS();
        dispatch(createFamilyMembers(Object.assign({}, submitRecord, { patientId: parseInt(submitRecord.patientId) }), form))
        dispatch(reset(form));
    }

    handleUpdate(data, dispatch, { form }) {
        const { record = {} } = this.props;
        const { familyMembersData = {} } = record;
        const { family = {} } = familyMembersData;
        const {  patientType } = family;

        const submitRecord = data.toJS();

        const selPatientType = this.state.selectedType || patientType;
        if (selPatientType === 'non_patient') {
            submitRecord.patientId = 0;
            dispatch(updateFamilyMembers(Object.assign({}, submitRecord, { patientType: 'non_patient' }), form));
        } else {
            if(submitRecord.guarantorFirstName != undefined) {
              delete submitRecord.guarantorFirstName;
            }
            if(submitRecord.guarantorLastName != undefined) {
              delete submitRecord.guarantorLastName;
            }
            if(submitRecord.guarantorGender != undefined) {
              delete submitRecord.guarantorGender;
            }
            if(submitRecord.guarantorEmail != undefined) {
              delete submitRecord.guarantorEmail;
            }
            if(submitRecord.guarantorPhone != undefined) {
              delete submitRecord.guarantorPhone;
            }
            if(submitRecord.guarantorDOB != undefined) {
              delete submitRecord.guarantorDOB;
            }
            if(submitRecord.guarantorSS != undefined) {
              delete submitRecord.guarantorSS;
            }
            dispatch(updateFamilyMembers(Object.assign({}, submitRecord, { patientType: 'patient', patientId: parseInt(submitRecord.patientId) }), form));
        }
    }

    handleOpen() {
        this.setState({ selectedType: false });
        const { id, dispatch } = this.props;
        dispatch(loadFamilyMembers(id));
    }

    handleDelete(patientId, familyId) {
        const { id } = this.props; 
        this.props.dispatch(deleteFamilyMembers({ id, patientId, familyId }));
    }
}


function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.id, 10))(state, props);
    const error = selectError()(state, props);
    const updateError = selectUpdateError()(state, props)
    return {
        record: selectedRecord ? selectedRecord.toJS() : { recordDidNotInit: true },
        error,
        updateError
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FamilyMembers);
