/**
*
* InputField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import ReactTable from "react-table";
import { Col, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import { Map as iMap } from "immutable";
import ConfirmButton from 'components/ConfirmButton';
import Info from '../Info';
import SearchField from '../SearchField';
import { ContentTypes } from '../CreatePatientForm/utils';
import ImageUpload from '../ImageUpload';
import MultiSelectField from '../MultiSelectField';
import DatePicker from '../DatePicker';
import ShowText from '../../../../components/ShowText';
import AlertMessage from 'components/Alert';

const renderTableField = (props) => {
  const { input, required, data, metaData, renderMode, handleAlert} = props;
  const Input = iMap(input).toJS();
  const InputValue = Input.value && Input.value[input.name] || [];

  const handleData = () => {
    const fieldIndex = document.getElementsByName(`${input.name}_index`)[0];
    const btnElem = document.getElementById(`${input.name}_btn`);
    var submitData = data.fields && data.fields.filter( _=> _.editRecord).reduce((a, el) => Object.assign({}, a, { [el.name]: el.type === "Date" ? document.getElementById(el.name).value : el.type === "MultiSelect" ? document.getElementsByName(el.name) && document.getElementsByName(el.name).length > 0 ? Array.prototype.slice.call(document.getElementsByName(el.name)).map((c) => { return c.value }).join(',') : '' : document.getElementsByName(el.name)[0].value }), {});

    if (fieldIndex.value != '' && InputValue && InputValue.length > 0) {
      if(data.name === 'implantData') {
        if(submitData.implantSite === '') {
          handleAlert("Implant Site #");
        } else if(submitData.implantBrand === '') {
          handleAlert("Implant Brand");
        } else if(submitData.implantReference === '') {
          handleAlert("Implant Reference #");
        } else if(submitData.implantLot === '') {
          handleAlert("Implant LOT #");
        } else if(submitData.diameter === '') {
          handleAlert("Diameter");
        } else if(submitData.length === '') {
          handleAlert("Length");
        } else if(submitData.ncm === '') {
          handleAlert("Initial Ncm");
        } else {
          const changedData = InputValue.map((a, index) => {
            if (index === parseInt(fieldIndex.value)) {
              return submitData;
            } else {
              return a;
            }
          });
          input.onChange({ [input.name]: changedData });
          fieldIndex.value = '';
          if (btnElem) {
            btnElem.innerHTML = 'Add'
          }
          data.fields && data.fields.map(field => {
            const elem = document.getElementsByName(field.name)[0];
            const element = document.getElementById(field.name);
            if (elem) {
              elem.value = '';
            }
            if (element) {
              if(element.id == "oralIVsite"){
                props.inputchange("", "oralIVsite");
              }
              element.value = '';
            }

            if (props.localState && props.localState[field.name]) {
              props.setValue(field.name, '');
            }
          });
        }
      } else {
        const changedData = InputValue.map((a, index) => {
          if (index === parseInt(fieldIndex.value)) {
            return submitData;
          } else {
            return a;
          }
        });
        input.onChange({ [input.name]: changedData });
        fieldIndex.value = '';
        if (btnElem) {
          btnElem.innerHTML = 'Add'
        }
        data.fields && data.fields.map(field => {
          const elem = document.getElementsByName(field.name)[0];
          const element = document.getElementById(field.name);
          if (elem) {
            elem.value = '';
          }
          if (element) {
            if(element.id == "oralIVsite"){
              props.inputchange("", "oralIVsite");
            }
            element.value = '';
          }

          if (props.localState && props.localState[field.name]) {
            props.setValue(field.name, '');
          }
        });
      }
    } else {
      if(data.name === 'implantData') {
        if(submitData.implantSite === '') {
          handleAlert("Implant Site #");
        } else if(submitData.implantBrand === '') {
          handleAlert("Implant Brand");
        } else if(submitData.implantReference === '') {
          handleAlert("Implant Reference #");
        } else if(submitData.implantLot === '') {
          handleAlert("Implant LOT #");
        } else if(submitData.diameter === '') {
          handleAlert("Diameter");
        } else if(submitData.length === '') {
          handleAlert("Length");
        } else if(submitData.ncm === '') {
          handleAlert("Initial Ncm");
        } else {
          input.onChange({ [input.name]: InputValue && InputValue.length > 0 ? InputValue.concat([submitData]) : [submitData] });
          data.fields && data.fields.map(field => {
            const elem = document.getElementsByName(field.name)[0];
            const element = document.getElementById(field.name);
            if (elem) {
              elem.value = '';
            }
            if (element) {
              if(element.id == "oralIVsite"){
                props.inputchange("", "oralIVsite");
              }
              element.value = '';
            }

            if (props.localState && props.localState[field.name]) {
              props.setValue(field.name, '');
            }
          });
        }
      } else {
        input.onChange({ [input.name]: InputValue && InputValue.length > 0 ? InputValue.concat([submitData]) : [submitData] });
        data.fields && data.fields.map(field => {
          const elem = document.getElementsByName(field.name)[0];
          const element = document.getElementById(field.name);
          if (elem) {
            elem.value = '';
          }
          if (element) {
            if(element.id == "oralIVsite"){
              props.inputchange("", "oralIVsite");
            }
            element.value = '';
          }

          if (props.localState && props.localState[field.name]) {
            props.setValue(field.name, '');
          }
        });
      }
    }

  }

  const InitialData = (index, edit) => {
    const fieldElem = document.getElementsByName(`${input.name}_index`)[0];
    const btnElem = document.getElementById(`${input.name}_btn`);
    if (fieldElem) {
      fieldElem.value = index;
      if (btnElem)
        btnElem.innerHTML = 'Save';
    }

    edit && Object.keys(edit).map(field => {
      const elem = document.getElementsByName(field)[0];
      const element = document.getElementById(field);

      if (elem) {
        elem.value = edit[field];
      }
      if (element) {
        if(element.id == "oralIVsite"){
          props.inputchange(edit[field], "oralIVsite")
        }
        element.value = edit[field];
      }
      if (field.type === 'Images') {
        props.setValue(field.name, edit[field]);
      }

    });

    setTimeout(() => input.onChange({ [input.name]: InputValue }), 1000);
  }

  const DuplicateData = (index, edit) => {
    if(InputValue && InputValue.length > 0) {
      delete edit.id;
      input.onChange({ [input.name]: InputValue.concat([edit]) });
    }
  }

  const editableColumnProps = {
    Cell: props => {
      const { column, tdProps } = props;
      return (column.action ?
        <Col>
          <Col className={styles.actions}>
            <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => InitialData(props.index, props.original)}>
              <Icon glyph="icon-fontello-edit" className={styles.icon} />Edit
            </Button>
            <ConfirmButton onConfirm={() => input.onChange({ [input.name]: InputValue && InputValue.length > 0 ? InputValue.filter((a, index) => index !== props.index) : [] })}>
              {(click) => <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => click()}>
                <Icon glyph="icon-fontello-trash" className={styles.icon} />Delete</Button>}
            </ConfirmButton>
          </Col>
          {data.duplicate ?
            <Col className={styles.actions}>
              <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => DuplicateData(props.index, props.original)}>
                <Icon glyph="icon-fontello-docs" className={styles.icon} />Duplicate
              </Button>
            </Col> : null}
        </Col>: <span style={{ display: 'flex' }}>
          {column.type === "Images" && props.value && props.value.length > 0 ?
            <Info data={{ attributes: { info: { image: props.value.split(','), openModal: true } } }} renderMode={renderMode} /> : column.type === "MultiSelect" && props.value ? props.value.split(',').join(', ')
            : column.limit && props.value && props.value.length > column.limit ?
              props.value.substring(0, column.limit) + '...' :
              column.resourceColumn && props.value ?
                metaData && metaData['providers'] && metaData['providers'].filter(_ => _.id === parseInt(props.value))[0]['name'] || '' : props.value}
          {column.limit && props.value && props.value.length > column.limit ?
            <ShowText title={column.title} text={props.value}>{(open) => <div className={styles.view} onClick={open.bind(this)} />}</ShowText> : null}
        </span>);
    }
  };
  return <Col>
    <Col className={styles.fields}>
      <input name={`${input.name}_index`} type="hidden" />
      {data.fields && data.fields.filter(_ => _.editRecord).map((field, index) => {
        if (field.type === 'Input') {
          return <Col key={index} style={{ marginRight: '20px' }}>
            <label htmlFor={field.title}>
              {field.title}
              {required || field.required ? <span style={field.orange_clr ? {color: "#ea6225", paddingLeft: '5px'} : null}>*</span> : null}
            </label>
            <input name={field.name} className={styles.InputField} type="text" />
          </Col>
        }
        else if (field.type === 'Select') {
          {
            return <Col key={index} style={{ marginRight: '20px' }}>
              <label htmlFor={field.title} style={{ display: 'flex' }}>
                {field.title}
                {required || field.required ? <span style={field.orange_clr ? {color: "#ea6225", paddingLeft: '5px'} : null}>*</span> : null}
                {field.attributes && field.attributes.info ? <Info data={field} noteStyle={true} renderMode={renderMode} /> : null}
              </label>
              <select name={field.name} className={styles.selectField} type="text">
                <option value="" disabled={false}>Choose Option</option>
                {(field && field.options) || (data.options && Array.isArray(data.options)) ?
                  ((field && field.options) || data.options || []).map((option, index) =>
                    <option key={`select-${option.title}-${index}`} value={option.value || option}>{option.title || option}</option>) :
                  data.options && metaData[data.options] ?
                    (data.options && metaData[data.options] || []).map((option, index) =>
                      <option key={`select-${option.name}-${index}`} value={option.value}>{option.name}</option>) :
                    field.listOptions && metaData[field.listOptions] ?
                      (field.listOptions && metaData[field.listOptions] || []).map((option, index) =>
                        <option key={`select-${option.name}-${index}`} value={option.value}>{option.name}</option>) : null}
              </select>
            </Col>
          }
        }
        else if (field.type === 'Search') {
          return <Col key={index} style={{ marginRight: '35px' }}>
            <SearchField data={field} metaData={metaData} />
          </Col>
        }
        else if (field.type === 'Date') {
          return <Col key={index} style={{ marginRight: '20px' }}>
            <DatePicker data={field} flexStyle={true} />
          </Col>
        }
        else if (field.type === 'MultiSelect') {
          return <Col key={index} style={{ marginRight: '20px', width: '300px' }}>
            <MultiSelectField data={field} flexStyle={true} />
          </Col>
        }
        else if (field.type === 'Images') {
          return <Col id='Finalpatest' key={index} style={{ marginRight: '20px' }}>
            <input name={field.name} id={field.name} type="hidden" />
            <ImageUpload
              input={{
                name: field.name,
                value: (props.localState && props.localState[field.name]) || (document.getElementById(field.name) && document.getElementById(field.name).value) || '',
                onChange: (val) => {
                  const currField = document.getElementById(field.name);
                  props.setValue(field.name, val.join(','));
                  if (currField)
                    currField.value = val.join(',');
                }
              }}
              type={'images'}
              contentType="images"
              showtype={'button'}
              label={field.title}
              data={field}
            />
          </Col>
        }


        return '';
      })}
      <Button id={`${input.name}_btn`} bsStyle="standard" type="button" className={data.name == "medicationEntry" ? styles.addButton : styles.saveButton} onClick={handleData}>Add</Button>
    </Col>
    <Col className="reactModule" style={{ marginTop: "20px" }}>

      <ReactTable
        columns={
          data.fields && data.fields.map((field) => Object.assign(
            {},
            { Header: field.title, accessor: field.name, width: field.width, ...field, ...editableColumnProps }
          ))}
        data={InputValue}
        pageSize={InputValue && InputValue.length > 0 ? data.pageSize ? data.pageSize : 4 : 0}
        showPageJump={false}
        resizable={false}
        sortable={false}
        showPageSizeOptions={false}
        previousText={"Back"}
        pageText={""}
        noDataText={"No entries to show."}
      />
    </Col>

  </Col>
}

class TableField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
    renderMode: PropTypes.oneOf(['field', 'group']),
  };

  constructor(props) {
    super(props);
    this.state = { isExpand: props.data.collapse ? true : false, showAlert: false, alertField: '' }
  }

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  handleAlert(fieldName) {
    this.setState({ showAlert: true, alertField: fieldName });
  }

  render() {
    const { data = {}, metaData = {}, options = {}, optionsMode, renderMode } = this.props;
    const { isExpand, alertField } = this.state;
    const { required } = options.general || {};

    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col style={data.noteStyle ? { paddingTop: '30px' } : null}>
        <label htmlFor={data.title} style={{ display: 'flex' }}>
          {data.collapse ?
            isExpand ?
              <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/plus.svg`)} /> :
                <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/minus.svg`)} /> : null}
          
          {data.orange_clr  ?
            <h4 style={{ color: '#EA6225' }}>{data.title}</h4> :
            <h4>{data.title}</h4>}

          {required ? <span>*</span> : null}
        </label>
        {!isExpand ?
          <Field
            name={data.name}
            id={data.name}
            component={renderTableField}
            data={data}
            metaData={metaData}
            renderMode={renderMode}
            handleAlert={this.handleAlert.bind(this)}
            {...options.general}
            {...extraFieldOptions}
            setValue={(name, val) => this.setState({ [name]: val })}
            localState={this.state}
            inputchange={(val, value) =>this.props.inputchange(val,value)}
          /> : null
        }
        {this.state.showAlert ?
          <AlertMessage warning dismiss={() => { this.setState({ showAlert: false }); }}>Please fill {alertField}</AlertMessage> : null}
      </Col>
    );
  }
}

export default TableField;
