/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { Map as iMap } from "immutable";
import { Col, Button } from '@sketchpixy/rubix';
import styles from './styles.css';

const renderRadioField = (props) => {
  const { input, required, data = {} } = props;
  const Input = iMap(input).toJS();
  const clearInput = () => {
    input.onChange('');
    const radioBoxes = document.getElementsByName(Input.name);
    for (var index = 0; index < radioBoxes.length; ++index) {
      radioBoxes[index].checked = false;
    }  
  }

  return <Col className={styles.fields}>
    {data.options.map((option, index) =>
      <label className={styles.container} key={index} >
        <span className={styles.title}>{option.title}</span>
        <input name={Input.name} type="radio" value={option.value} defaultChecked={Input.value === option.value} onChange={(e) => input.onChange(option.value)} required={data.required} />
        <span className={styles.checkmark}></span>
      </label>)}
      <Button className={styles.clearBtn} bsStyle="standard" type="button" onClick={clearInput}>Clear</Button>
  </Col>
}


class MultiRadioBoxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showAttributes: null, selectedOptions: false };
  }

  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    path: PropTypes.string,
  };

  render() {
    const { data = {}, options = {}, optionsMode } = this.props;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }


    return (
      <Col xs={12} className={styles.MultiRadioBoxField}>
        <label htmlFor={data.name}>
          {data.title}
        </label>
        <Field
          name={data.name}
          id={data.name}
          component={renderRadioField}
          data={data}
          {...options.general}
          {...extraFieldOptions}
        />
      </Col>
    );
  }



}

export default MultiRadioBoxField;
