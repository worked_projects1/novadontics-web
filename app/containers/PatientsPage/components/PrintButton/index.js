/**
*
* PrintButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Button } from '@sketchpixy/rubix';

import styles from './styles.css';

class PrintButton extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };
  render() {
    const { data } = this.props;
    return (
      <Col> 
        <Button bsStyle="standard" className={styles.actionButton} onClick={() => window.open(data.url, '_blank')}>{data.title}</Button>
      </Col>
    );
  }
}

export default PrintButton;
