/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';
import { ContentTypes } from '../CreatePatientForm/utils';
import Textarea from '../Textarea';
import SelectField from '../SelectField';
import Upload from '../Upload';
import DatePicker from '../DatePicker';
import Info from '../Info';
import ModalField from '../ModalField';
import axios from 'axios';
import AlertMessage from 'components/Alert';
import { sendNotification, sendInstructions } from 'blocks/patients/remotes.js';

const ImplementationFor = {
  note: Textarea,
  options: SelectField,
  images : Upload,
  calendar: DatePicker,
};


class CheckboxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showAttributes: null, successAlert: false, failureAlert: false, alertText: '', selectedOptions : false, isExpand : props.data.collapse ? true : false };
  }
  
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    initialValue: PropTypes.object,
    path: PropTypes.string,
  };

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  handleFormNotification(method) {
    const { data, id } = this.props;
    let submitRecord = {}
    submitRecord.id = id;
    submitRecord.actionType = method;
    submitRecord.formTitle = data.title;
    submitRecord.consentName = data.name;
    sendNotification(submitRecord)
      .then(resp => { this.setState({ successAlert: true, alertText: resp[0].response }); })
      .catch((error) => {
        const errorResponse = JSON.parse(JSON.stringify(error));
        if(errorResponse != undefined && errorResponse.response != undefined && errorResponse.response.data != '') {
          const errorText = errorResponse.response.data;
          this.setState({ failureAlert: true, alertText: errorText[0].response  });
        } else {
          this.setState({ failureAlert: true, alertText: 'Something went wrong. Please try again. If the problem persists contact support team.' });
        }
      })
  }

  handleInsNotification(method) {
    const { data, id } = this.props;
    const { attributes = {} } = data;
    const { printTemplate } = attributes;
    let submitRecord = {}
    submitRecord.id = id;
    submitRecord.actionType = method;
    submitRecord.formTitle = data.title;
    submitRecord.docLink = printTemplate;
    sendInstructions(submitRecord)
      .then(resp => { this.setState({ successAlert: true, alertText: resp[0].response }); })
      .catch((error) => {
        const errorResponse = JSON.parse(JSON.stringify(error));
        if(errorResponse != undefined && errorResponse.response != undefined && errorResponse.response.data != '') {
          const errorText = errorResponse.response.data;
          this.setState({ failureAlert: true, alertText: errorText[0].response  });
        } else {
          this.setState({ failureAlert: true, alertText: 'Something went wrong. Please try again. If the problem persists contact support team.' });
        }
      })
  }

  render() {
    const { data = {}, options = {}, optionsMode, presicon,initialValue,onPrescriptionsClick, renderMode, path, formId, prescriptionDisable, record = {} } = this.props;
    const { attributes } = data;
    const { showAttributes, selectedOptions, isExpand, successAlert, failureAlert, alertText } = this.state;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }
 
    const setAttributes = (event) => {
      const { value } = event.target;
      this.setState({showAttributes : value == 'false' ? true : value ? false : true , selectedOptions : false});
    }
    const renderAttributeFields = (click) => {
      const Component = attributes.note && !attributes.options && !attributes.calendar ? ImplementationFor['note'] :  attributes.options ? ImplementationFor['options'] : attributes.images ? ImplementationFor['images'] : attributes.calendar ? ImplementationFor['calendar'] : null;
      const contentType = attributes.images ? ContentTypes['Images'] : null;
  
      return (Component && attributes ?
        <Component
          data={data}
          contentType={contentType}
          options={options}
          initialValue = {initialValue}
          showModal={(val) => { this.setState({selectedOptions: val}); click()}}
          formId={formId}
        /> : null);
    }
    const renderMultiAttributeFields = () => {
      const Component =  ImplementationFor['images'];
      const contentType = attributes.images ? ContentTypes['Images'] : null;

      return (Component && attributes ?
        <Component
          data={data}
          contentType={contentType}
          options={options}
          initialValue = {initialValue}
          showModal={(val) => { this.setState({selectedOptions: val}); click()}}
          formId={formId}
        /> : null);
    }

    const containerClass = initialValue && data.disabled ? `${styles.checkboxContainer} ${styles.disabled}` : data.labelStyle ? styles.checkboxPurpleContainer : styles.checkboxContainer
    return (
      <Col xs={12} className={`${styles.CheckboxField} ${data.newStyle ? styles.bottomMargin :  null}`}>
        <label htmlFor={data.label} className={data.labelStyle ? styles.titleStyle : null}>
          {data.collapse ?
            isExpand ?
              <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/plus_purple.svg`)} /> :
                <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/minus_purple.svg`)} /> : null}
          {data.label}
          {data.info ? <Info data={data} /> : null}
        </label>
        {!isExpand ?
          <div>
            <div className={containerClass}>
              <Field
                name={optionsMode == 'request' ? `${data.name}` : `${data.name}.checked`}
                id={optionsMode == 'request' ? `${data.name}` : `${data.name}.checked`}
                component="input"
                type={'checkbox'}
                {...options.general}
                {...extraFieldOptions}
                onChange={(event)=>{
                  if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
                    this.props.inputchange(event.target.value, data.name);
                  }
                  setAttributes(event)}}
                disabled={initialValue && data.disabled ? true: false}
              />
              <label htmlFor={optionsMode == 'request' ? `${data.name}` : `${data.name}.checked`}>
                <span></span>
              </label>
              <div className={styles.titleHead} style={{justifyContent:attributes && attributes.info ? 'baseline' : 'space-between'}}>
              <span dangerouslySetInnerHTML={{__html: data.title}} className={data.title.length > 145 ? styles.titleWidth : data.presicon ? styles.presicon : null } />
                {attributes && attributes.info ? <Info data={data} renderMode={renderMode} /> : null}
                {attributes && attributes.modalOptions ? <span>{selectedOptions}</span> : null}
              </div>
              {attributes && attributes.email ? <span><img src={require('img/patients/email1.svg')} className={styles.emailImg} onClick={() => { window.open(`mailto:${record && record.email || ''}?body=${attributes && attributes.email && attributes.email.body || ''}`, '_blank') }} /></span> : null}
              {attributes && attributes.sendCFSMS ? <span><img src={require('img/patients/chat.svg')} className={styles.chatImg} onClick={this.handleFormNotification.bind(this, 'SMS')} /></span> : null}
              {attributes && attributes.sendCFEmail ? <span><img src={require('img/patients/email1.svg')} className={styles.emailImg} onClick={this.handleFormNotification.bind(this, 'Email')} /></span> : null}
              {attributes && attributes.sendDocumentSMS ? <span><img src={require('img/patients/chat.svg')} className={styles.chatImg} onClick={this.handleInsNotification.bind(this, 'SMS')} /></span> : null}
              {attributes && attributes.sendDocumentEmail ? <span><img src={require('img/patients/email1.svg')} className={styles.emailImg} onClick={this.handleInsNotification.bind(this, 'Email')} /></span> : null}
              {attributes && attributes.printTemplate ? <span><img src={require('img/patients/IconPrinter.svg')} className={styles.img} onClick={() => this.openPrintTemplate(attributes.printTemplate, path)} /></span> : null}
            </div>
            {((initialValue && initialValue.checked && showAttributes == null) || (showAttributes)) && attributes ?
            <div>
              {attributes.modalOptions ?
                 <ModalField
                 title={data.title}
                 >{(click)=> renderAttributeFields(click)}</ModalField>
               : renderAttributeFields()}
               {attributes.note && attributes.images ? renderMultiAttributeFields() : null}
            </div>
             : null }
          </div>
        : null }
        {this.state.successAlert ?
          <AlertMessage success dismiss={() => { this.setState({ successAlert: false }); }}>{alertText}</AlertMessage> : null}
        {this.state.failureAlert ?
          <AlertMessage warning dismiss={() => { this.setState({ failureAlert: false }); }}>{alertText}</AlertMessage> : null}
      </Col>
    );
  }

  openPrintTemplate(url, path) {
    const treatmentID = path.split('/')[2];
    if(url.indexOf('.pdf') > -1){
      window.open(url);
    } else {
      axios(`${url}?treatment=${treatmentID}`, {
        method: 'GET',
        responseType: 'blob' //Force to receive data in a Blob Format
      }).then((response) => {
        const file = new Blob(
          [response.data], 
          {type: 'application/pdf'});
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      });
    }
  }

  
}

export default CheckboxField;
