/**
*
* DeleteButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Row, Modal, Button, Image } from '@sketchpixy/rubix';
import styles from './styles.css';


class DeleteButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { type } = this.props; 
    return (
      <Col className={styles.delete}>
      	<Image src={require('img/patients/close.svg')} onClick={this.open.bind(this)} responsive className={styles.imgDelete} />
        <Modal show={this.state.showModal} className={"deleteModal"}>
          <Modal.Body>
            <Row className={styles.rowStandard}>
              <h4>Confirm</h4>
              <Col>Are you sure?</Col>
            </Row>
          </Modal.Body>
          <Modal.Footer className={styles.footer}>
            <Button bsStyle="standard" type="button" onClick={this.close.bind(this)} className={styles.submitButton}>No</Button>
            <Col className={styles.vr}></Col>
            <Button bsStyle="standard" type="button" onClick={this.props.OnDelete} className={styles.submitButton}>Yes</Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
  	this.setState({ showModal: true })
  }

}

DeleteButton.propTypes = {
  type: PropTypes.string,
};

export default DeleteButton;
