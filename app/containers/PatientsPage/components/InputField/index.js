/**
*
* InputField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field, change } from 'redux-form/immutable';
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

const renderInputField = (props) => {
  const { input, required, data = {} } = props;
  return <input name={input.name} maxLength={data && data.max ? data.max.toString() : ''} className={styles.InputField} defaultValue={input.value} required={required} disabled={data.disabled ? true : false} type={data.hideText ? 'password' : input.type} onBlur={(e) => input.onChange(e.target.value)} />
}

class InputField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };

  componentDidMount() {
    const { data = {}, form, dispatch } = this.props;
    if (data.calculation) {
      const field = document.getElementsByName(data.name)[0];
      field.addEventListener("input", function (e) {
        data.calculation.map(cal => {
          const { action, fields = [], target } = cal;
          const field1 = fields[0] && parseFloat(document.getElementsByName(fields[0])[0].value) || false;
          const field2 = fields[1] && parseFloat(document.getElementsByName(fields[1])[0].value) || false;
          const result = (action === 'addition') ? field1 + field2 : field1 - field2;
          document.getElementsByName(target)[0].value = result;
          setTimeout(() => dispatch(change(form, target, result)), 1000);
        })
      })
    }
  }

  render() {
    const { data = {}, options = {}, optionsMode } = this.props;
    const type = data.number ? 'number' : 'text';
    const { required } = options.general || {};
    const normalize = data.max ? (value) => value.length > data.max ? value.slice(0, parseInt(data.max)) : value : data.number ? (value) => parseFloat(value) : null;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }


    return (
      <Col style={data.noteStyle ? { paddingBottom: '30px' } : null}>
        {!data.noteStyle ? <label htmlFor={data.title}>
          <span dangerouslySetInnerHTML={{__html: data.title}}/>
          {required ? <span style={{color:"#ea6225",fontSize:"25px"}}>*</span> : <span style={{fontSize:"25px"}}></span>}
        </label> : null}
        <Field
          name={data.name}
          id={data.name}
          component={renderInputField}
          type={type}
          className={styles.InputField}
          normalize={normalize}
          {...options.general}
          {...extraFieldOptions}
          placeholder={data.postfix ? data.postfix : ''}
          {...this.props}
        />
      </Col>
    );
  }
}

export default InputField;
