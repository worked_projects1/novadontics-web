/**
*
* Filter
*
*/
import 'rc-slider/dist/rc-slider.min.css';
import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Row, Col, Button } from '@sketchpixy/rubix';
import Slider from 'rc-slider';
import styles from './styles.css';

class Filter extends React.Component {

    constructor(props){
      super(props);
      this.state = { selectedStatus: props.selectedStatus, selectedType: props.selectedType }
    }

  render() {
    const { show, hide, onChange, patientTypeStatus } = this.props;
    const { selectedStatus, selectedType } = this.state;
    const statusMap = patientTypeStatus.length != 0 ? patientTypeStatus.map(_=>_.patientStatus.split(','))[0] : [];
    const typeMap = patientTypeStatus.length != 0 ? patientTypeStatus.map(_=>_.patientType.split(','))[0] : [];

     return (
        <Modal show={show} backdrop="static" onHide={hide} dialogClassName="categorymodel" >
        <Modal.Header closeButton>
          <Modal.Title>Patient Filter</Modal.Title>
          <div className={styles.modelclear} onClick={()=> { this.clearFilter() }}>
            Clear Filter
          </div>
        </Modal.Header>
        <Modal.Body>
        <div className={styles.modelpricesubcontainer}>
          <div className={styles.modlesubtitle}>PATIENT STATUS</div>
          <div className={styles.modelvendorcontainer}>
            <div className={styles.modelvendorlist}>
              <div>
                Select ALL
              </div>
              <div>
                <label className={styles.switch}>
                  <input name="selectall" type="checkbox" checked={statusMap.length === selectedStatus.length} className={styles.checkswitch} onClick={()=>{this.handleChange(statusMap.length != selectedStatus.length ? statusMap : [], 'patientStatus')}} />
                  <span className={`${styles.slider} ${styles.round}`}></span>
                </label>
              </div>
            </div>
          </div>
          {statusMap != undefined? statusMap.map((status, index) =>
          <div key={"checkboxdiv_"+index} className={styles.modelvendorcontainer}>
            <div className={styles.modelvendorlist}>
              <div id={"div_"+index}  >
                {status}
              </div>
              <div>
                <label className={styles.switch}>
                  <input name={"check-"+status} type="checkbox" checked={(selectedStatus.indexOf(status) > -1)}  onClick={()=>this.handleChange(selectedStatus.indexOf(status) > -1 ?  selectedStatus.filter(_=>_ !== status) : selectedStatus.concat(status), 'patientStatus')} className={styles.checkswitch}/>
                  <span className={`${styles.slider} ${styles.round}`}></span>
                </label>
              </div>
            </div>
          </div>
          ):null}
        </div>
        <div className={styles.modelpricesubcontainer}>
          <div className={styles.modlesubtitle}>PATIENT TYPE</div>
          <div className={styles.modelvendorcontainer}>
            <div className={styles.modelvendorlist}>
              <div>
                Select ALL
              </div>
              <div>
                <label className={styles.switch}>
                  <input name="selectall" type="checkbox" checked={typeMap.length === selectedType.length} className={styles.checkswitch} onClick={()=>{this.handleChange(typeMap.length != selectedType.length ? typeMap : [], 'patientType')}} />
                  <span className={`${styles.slider} ${styles.round}`}></span>
                </label>
              </div>
            </div>
          </div>
          {typeMap != undefined? typeMap.map((status, index) =>
          <div key={"checkboxdiv_"+index} className={styles.modelvendorcontainer}>
            <div className={styles.modelvendorlist}>
              <div id={"div_"+index}  >
                {status}
              </div>
              <div>
                <label className={styles.switch}>
                  <input name={"check-"+status} type="checkbox" checked={(selectedType.indexOf(status) > -1)}  onClick={()=>this.handleChange(selectedType.indexOf(status) > -1 ?  selectedType.filter(_=>_ !== status) : selectedType.concat(status), 'patientType')} className={styles.checkswitch}/>
                  <span className={`${styles.slider} ${styles.round}`}></span>
                </label>
              </div>
            </div>
          </div>
          ):null}
        </div>
        </Modal.Body>
        <Modal.Footer>
            <Row className="action-button hidden-print">
              <Col xs={12} className={styles.addButtonStyle}>
                <Button bsStyle={'standard'} type="submit" className={styles.buttonStandard} onClick={() => {onChange(selectedStatus, selectedType); hide();}}>Done</Button>
              </Col>
            </Row>
        </Modal.Footer>
      </Modal>
    );
  }

  clearFilter(){
    this.setState({ selectedStatus: [], selectedType: [] });
  }

  handleChange(input, type){
    const { selectedStatus, selectedType } = this.state;
    if(type == 'patientStatus'){
      this.setState({ selectedStatus: input });
    }
    if(type == 'patientType'){
      this.setState({ selectedType: input });
    }
  }
}

Filter.propTypes = {
  patientTypeStatus: PropTypes.array,
  show: PropTypes.bool,
  hide: PropTypes.func,
  onChange: PropTypes.func,
};

export default Filter;
