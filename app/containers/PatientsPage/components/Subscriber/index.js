/**
*
* Subscriber
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Col, Button, Icon, Grid, Alert, Tab, Tabs, PanelContainer, PanelBody } from '@sketchpixy/rubix';
import styles from './styles.css';
import ReactTable from "react-table";
import schema from 'routes/schema';
import ConfirmButton from 'components/ConfirmButton';
import moment from 'moment';
import ModalForm from 'components/ModalRecordForm';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import { loadSubscriber, updateSubscriber } from "blocks/patients/subscriber";
import selectors from 'blocks/records/selectors';
import EditRecordForm from 'components/EditRecordForm';
import AlertMessage from 'components/Alert';

const columns = schema().patients().subscriber;
const patientSelectors = selectors('patients');
const {
    selectRecord,
    selectError,
    selectUpdateError
} = patientSelectors;

class Subscriber extends React.Component { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.state = { showModal: false, selectedType: false, error: false, showAlert: false }
    }

    static propTypes = {
        data: PropTypes.object,
    };

    render() {
        const { id, data, optionsMode, record = {}, dispatch, metaData, location = {} } = this.props;
        const { subscriberData = {} } = record;
        const { primary = {}, secondary = {} } = subscriberData;
        const TabsMenu = [{name: "Primary Subscriber"}, {name: "Secondary Subscriber"}];
        return (
            optionsMode === 'edit' && data.showView ? <Col>
                <Button onClick={this.open.bind(this)} bsStyle="standard" className={styles.actionButton}>{data.title}</Button>
                <Modal show={this.state.showModal} bsSize="large" backdrop="static" onHide={this.close.bind(this)} onEnter={this.handleOpen.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title className={styles.title}>
                            <span style={{ color: '#EA6225' }}>{data.title}</span>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col>
                          {this.state.error ?
                              <Alert danger>{this.state.error}</Alert> : null}
                        </Col>
                        <Tabs id="Subscriber">
                          {(TabsMenu.map((prop, i) =>
                            <Tab key={i} eventKey={i} title={prop.name}>
                              <PanelContainer>
                                <PanelBody className={styles.tabBackground}>
                                  <Col style={{ marginLeft: '25px', marginRight: '25px' }}>
                                    {prop.name === "Primary Subscriber" ?
                                      <EditRecordForm
                                        initialValues={primary || {}}
                                        fields={columns}
                                        form={`PrimarySubscriber`}
                                        onSubmit={(data) => this.handleUpdate(data, 'primary', 'PrimarySubscriber')}
                                        metaData={metaData && metaData.dentalCarrier && Object.assign({}, metaData, {dentalCarrier: metaData.dentalCarrier.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[ {value: -1, label: 'UCR' }, {value: 0, label: 'Private' }])}) || metaData}
                                        locationState={location.state}
                                        record={record}
                                        disableCancel={true}
                                        catalogNotes={true}
                                        subscriberBtn={true}
                                      /> : prop.name === "Secondary Subscriber" ?
                                      <EditRecordForm
                                        initialValues={secondary || {}}
                                        fields={columns}
                                        form={`SecondarySubscriber`}
                                        onSubmit={(data) => this.handleUpdate(data, 'secondary', 'SecondarySubscriber')}
                                        metaData={metaData && metaData.dentalCarrier && Object.assign({}, metaData, {dentalCarrier: metaData.dentalCarrier.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[ {value: -1, label: 'UCR' }, {value: 0, label: 'Private' }])}) || metaData}
                                        locationState={location.state}
                                        record={record}
                                        disableCancel={true}
                                        catalogNotes={true}
                                        subscriberBtn={true}
                                      /> : null}
                                  </Col>
                                </PanelBody>
                              </PanelContainer>
                            </Tab>
                          ))}
                        </Tabs>
                         {this.state.showAlert ?
                           <AlertMessage warning dismiss={() => this.setState({ showAlert: false })}>
                             There was an error creating this record. Have you supplied all the required fields? Please try again.
                           </AlertMessage> : null}
                    </Modal.Body>
                </Modal>
            </Col> : null
        );
    }


    open() {
        this.setState({ showModal: true });
    }

    close() {
        this.setState({ showModal: false })
    }

    showError(error) {
        this.setState({error});
        setTimeout(() => this.setState({error: false}), 3000);
    }

    handleUpdate(data, type, form) {
        const { record = {}, dispatch, id } = this.props;
        const submitRecord = data.toJS();
        submitRecord.type = type;
        submitRecord.patientId = id;
        if(submitRecord.dob === undefined) {
          this.setState({ showAlert: true })
        } else {
          dispatch(updateSubscriber(submitRecord), form);
        }
    }

    handleOpen() {
        const { id, dispatch } = this.props;
        dispatch(loadSubscriber(id));
    }
}


function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.id, 10))(state, props);
    const error = selectError()(state, props);
    const updateError = selectUpdateError()(state, props)
    return {
        record: selectedRecord ? selectedRecord.toJS() : { recordDidNotInit: true },
        error,
        updateError
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Subscriber);
