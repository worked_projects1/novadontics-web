/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { Map as iMap } from "immutable";
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';
import { useHistory } from 'react-router';

const renderCheckboxField = (props) => {
  const { input, data = {} } = props;
  const Input = iMap(input).toJS();
  const InputValue = Input.value && Input.value.split(",") || [];
  const changeData = (e, val) => {
    if (e.target.checked) {
      InputValue.push(val);
      input.onChange(InputValue.join(','))
    } else {
      input.onChange(InputValue.filter(a => a !== val).join(','))
    }

  }

  return <Col className={styles.fields}>
    {data.options.map((option, t) => <div key={t} className={data.blueCheckbox ? styles.checkboxBlueContainer : styles.checkboxContainer}>
      <input id={`${input.name}.${option.value}`} className={styles.InputField} type="checkbox" defaultChecked={InputValue.find(a => a === option.value)} onChange={(e) => changeData(e, option.value)} />
      <label htmlFor={`${input.name}.${option.value}`}>
        <span></span>
      </label>
      {option.title}
    </div>)}
  </Col>
}


class MultiCheckBoxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showAttributes: null, selectedOptions: false, isExpand: props.data.collapse ? true : false };
  }

  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    path: PropTypes.string,
  };

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  render() {
    const { data = {}, options = {}, optionsMode } = this.props;
    const { isExpand } = this.state;
    const { required } = options.general || {};
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }


    return (
      <Col xs={12} className={styles.CheckboxField}>
        <label htmlFor={data.title} className={data.labelStyle ? styles.titleStyle : null}>
          {data.collapse ?
            isExpand ?
              <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/plus_blue.svg`)} /> :
                <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/minus_blue.svg`)} /> : null}
         
         {data.blue_clr  ?
            <h4 style={{ color: '#38a0f1', marginTop: '7px' }}>{data.title}</h4> :
            <h4 style={{ marginTop: '7px' }}>{data.title}</h4>}

          {required && data.title != '' ? <span>*</span> : null}
        </label>
        {!isExpand ?
          <Field
            name={data.name}
            id={data.name}
            component={renderCheckboxField}
            data={data}
            {...options.general}
            {...extraFieldOptions}
          /> : null
        }
      </Col>
    );
  }



}

export default MultiCheckBoxField;
