import React from 'react';
import { Col, Modal, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import Autocomplete from './autocomplete';

export default class AutocompleteField extends React.Component {
    constructor(props) {
      super(props);
      this.state = { showModal: false, val: false }
    }

    componentDidMount() {
        const { input, data, metaData } = this.props;
        const isPreDefinedSet = Array.isArray(data.options);
        Autocomplete(document.getElementById(input.name), isPreDefinedSet && data.options || metaData[data.options], input, data);
    }

    render() {
        const { data, input, metaData } = this.props;
        const isPreDefinedSet = Array.isArray(data.options);
        const Options = isPreDefinedSet && data.options || metaData && metaData[data.options] || [];
        const InputValue = Options.find(e => input && input.value && e.value.toString() === input.value.toString());
        
        return <Col>
            <label htmlFor={data.title}>
                {data.title}
                {data.required ? <span>*</span> : null}
            </label>
              <input
                  className={data.name === 'procedureDescription' ? styles.InputField1 : styles.InputField}
                  name={input && input.name}
                  id={input && input.name}
                  type="text"
                  autoComplete="off"
                  placeholder="Type to Search"
                  defaultValue={''}
                  required={data.required} />
                  {data.view ? <Icon glyph="icon-fontello-eye" onClick={this.showView.bind(this)} className={styles.view} /> : null}
                  <Modal show={this.state.showModal} onHide={this.close.bind(this)} >
                    <Modal.Header closeButton>
                      <Modal.Title>{data.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      {this.state.val ? this.state.val : input.value ? input.value : ''}
                    </Modal.Body>
                  </Modal>
            </Col>
    }

    close() {
      this.setState({ showModal: false });
    }

    showView() {
      const { input } = this.props;
      const val = document.getElementById(input.name).value;
      if(val)
        this.setState({ showModal: true, val: val })
    }
}