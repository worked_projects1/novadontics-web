export default function autocomplete(field, arr, input, data) {
    const { ref } = data || {};
    var currentFocus;
    field.addEventListener("input", function (e) {
        var div, div2, i, val = this.value;
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        div = document.createElement("DIV");
        div.setAttribute("id", this.id + "autocomplete-list");
        div.setAttribute("class", "autocomplete-field");
        this.parentNode.parentNode.appendChild(div);
        for (i = 0; i < arr.length; i++) {
            if ((arr[i]['title'] && arr[i]['title'].toLowerCase().includes(val.toLowerCase())) || (typeof arr[i] === 'string' && arr[i].toLowerCase().includes(val.toLowerCase()))) {
                div2 = document.createElement("DIV");
                div2.innerHTML = arr[i].title || arr[i];
                div2.innerHTML += "<input type='hidden' " + (arr[i].ref ? "data-ref='" + arr[i].ref + "'" : "") + " data-value='"+ (arr[i].value.replace(/'/g, '&#39;') || arr[i].replace(/'/g, '&#39;')) +"' data-title='" + (arr[i].title.replace(/'/g, '&#39;') || arr[i].replace(/'/g, '&#39;')) + "'>";
                div2.addEventListener("click", function (e) {
                    const currValue = this.getElementsByTagName("input")[0].getAttribute('data-value');
                    const currTitle = this.getElementsByTagName("input")[0].getAttribute('data-title');
                    field.value = currTitle;
                    input.onChange(currValue);
                    if (ref) {
                        const currRef = this.getElementsByTagName("input")[0].getAttribute('data-ref');
                        var refInp = document.getElementsByName(ref)[0];
                        if (refInp)
                            refInp.value = currRef;
                    }
                    closeAllLists();
                });
                div.appendChild(div2);
            }
        }
    });

    field.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-field");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != field) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
            closeAllLists(e.target);
    });
}