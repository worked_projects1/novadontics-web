/**
*
* Textarea
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';

import styles from './styles.css';

const renderTextareaField = (props) => {
  const { input, data = {} } = props;
  return <textarea name={data.mode === 'inline' ? data.name : `${data.name}.notes`} className={data.changeStyle ? styles.textAreaNotes : styles.textarea} defaultValue={input.value} type="input" onBlur={(e) => input.onChange(e.target.value)}
   onChange={(e) => {
     if(typeof props.inputchange !== "undefined" && props.data.onChange){
        props.inputchange(e.target.value, data.name);
     }
   }} />
}

class Textarea extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create','request']),
  };

  render() {
    const { data, options = {}, optionsMode } = this.props;
    let extraFieldOptions = {};
    const { required } = options.general || {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={12} className={styles.Textarea}>
        {optionsMode ? <label htmlFor={data.title} className={data.changeStyle ? styles.titleStyle : null}>
            {data.title}
            {required ? <span>*</span> : null}
          </label> : null}
        <Field 
        name={data.mode === 'inline' ? data.name : `${data.name}.notes`} 
        id={data.mode === 'inline' ? data.name : `${data.name}.notes`}
        className={data.changeStyle ? styles.textAreaNotes : styles.textarea}
        onFocus={e => e.preventDefault()}
        onBlur={e => e.preventDefault()}
        component={data.renderTextarea || data.type == 'Checkbox'? renderTextareaField : "textarea"}
        {...this.props}
        {...options.general}
        {...extraFieldOptions} />
      </Col>
    );
  }
}

export default Textarea;
