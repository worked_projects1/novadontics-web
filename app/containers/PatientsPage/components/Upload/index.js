/**
*
* <Upload />
* Wrapper for redux-form’s <Field /> and delegates work to ImageUpload component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import ImageUpload from '../ImageUpload';
import { Field } from 'redux-form/immutable';

class Upload extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    data: PropTypes.object,
    contentType: PropTypes.string,
    optionsMode: PropTypes.string,
    renderMode: PropTypes.string,
  };

  render() {
    const { data, contentType, optionsMode, renderMode } = this.props;
    const type = contentType.indexOf('image') > -1 ? 'images' : contentType.indexOf('audio') > -1 ? 'audios' : contentType.indexOf('video') > -1 ? 'videos' : '';
    return (
      <Field name={`${data.name}.${type}`} id={`${data.name}.${type}`} type={type} contentType={contentType} label={optionsMode ? data.title : null} component={ImageUpload} data={data} renderMode={renderMode} {...this.props} />
    );
  }

}

export default Upload;
