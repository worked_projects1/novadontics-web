/**
*
* InputField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import ReactTable from "react-table";
import { Col, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import { Map as iMap } from "immutable";
import ConfirmButton from 'components/ConfirmButton';
import Info from '../Info';
import SearchField from '../SearchField';
import { ContentTypes } from '../CreatePatientForm/utils';
import ImageUpload from '../ImageUpload';
import DatePicker from '../DatePicker';
import CheckboxField from '../CheckboxField';
import ShowText from '../../../../components/ShowText';
import { currencyFormatter } from 'utils/currencyFormatter.js';

const renderTableField = (props) => {
  const { input, required, data, metaData, renderMode, editNotes } = props;
  const tableFields = editNotes ? data.fields && data.fields.filter(item => item.editRecord) : data.fields;
  const Input = iMap(input).toJS();
  let InputValue = Input.value && Input.value[input.name] || [];

  const InitialData = (index, edit) => {
    const selectedId = edit.id;
    const selectedRow = InputValue.map(el => {
                          if(el.id == selectedId) {
                            el.selected = el.selected == undefined || el.selected == false ? true : false;
                          }
                          return el;
                        })
    setTimeout(() => input.onChange({ [input.name]: selectedRow }), 1000);
  }

  const editableColumnProps = {
    Cell: props => {
      const { column, tdProps } = props;
      return (column.action ?
        <Col>
          <Col className={styles.actions}>
            <input type="checkbox" className={styles.checkboxStyle} defaultChecked={props.original && props.original.selected} onClick={() => InitialData(props.index, props.original)}  />
          </Col>
        </Col>: <span style={{ display: 'flex' }}>
          {column.type === "Images" && props.value && props.value.length > 0 ?
            <Info data={{ attributes: { info: { image: props.value.split(','), openModal: true } } }} renderMode={renderMode} /> : column.currency && props.value ? currencyFormatter.format(props.value)
            : column.limit && props.value && props.value.length > column.limit ?
              props.value.substring(0, column.limit) + '...' :
              column.resourceColumn && props.value ?
                metaData && metaData['providers'] && metaData['providers'].filter(_ => _.id === parseInt(props.value))[0]['name'] || '' : props.value}
          {column.limit && props.value && props.value.length > column.limit ?
            <ShowText title={column.title} text={props.value}>{(open) => <div className={styles.view} onClick={open.bind(this)} />}</ShowText> : null}
        </span>);
    }
  };
  return <Col>
    <Col className="reactModule" style={{ marginTop: "20px" }}>
      <ReactTable
        columns={
          tableFields && tableFields.map((field) => Object.assign(
            {},
            { Header: field.title, accessor: field.name, width: field.width, ...field, ...editableColumnProps }
          ))}
        data={InputValue}
        pageSize={InputValue && InputValue.length > 0 ? InputValue.length : 0}
        showPageJump={false}
        resizable={false}
        showPageSizeOptions={false}
        showPagination={false}
        previousText={"Back"}
        pageText={""}
        noDataText={"No entries to show."}
      />
    </Col>

  </Col>
}

class ViewTableField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
    renderMode: PropTypes.oneOf(['field', 'group']),
  };

  constructor(props) {
    super(props);
    this.state = { isExpand: props.data.collapse ? true : false }
  }

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  render() {
    const { data = {}, metaData = {}, options = {}, optionsMode, renderMode, editNotes } = this.props;
    const { isExpand } = this.state;
    const { required } = options.general || {};

    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col style={data.noteStyle ? { paddingTop: '30px' } : null}>
        <label htmlFor={data.title} style={{ display: 'flex' }}>
          {data.collapse ?
            isExpand ?
              <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/plus.svg`)} /> :
                <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/minus.svg`)} /> : null}
          
          {data.orange_clr  ?
            <h4 style={{ color: '#EA6225' }}>{data.title}</h4> : data.title == "Procedure" ? <div><h4 style={{ color: '#EA6225' }}>{data.title}<span className={styles.procedure}> (The list below displays the accepted treatment plans for this patient)</span></h4></div> :
            <h4>{data.title}</h4>}

          {required ? <span>*</span> : null}
        </label>
        {!isExpand ?
          <Field
            name={data.name}
            id={data.name}
            component={renderTableField}
            data={data}
            metaData={metaData}
            editNotes={editNotes}
            renderMode={renderMode}
            {...options.general}
            {...extraFieldOptions}
            setValue={(name, val) => this.setState({ [name]: val })}
            localState={this.state}
          /> : null
        }
      </Col>
    );
  }
}

export default ViewTableField;
