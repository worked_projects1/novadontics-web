/**
*
* SubmitButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Row, Modal, Button } from '@sketchpixy/rubix';
import styles from './styles.css';


class SubmitButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { submitting, disable, handleSubmit } = this.props;
    const { showModal } = this.state;
    return (
      <Col>
        <Button type="button" disabled={!disable && submitting || disable} className={!disable && submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton} onClick={this.open.bind(this)}>Save</Button>
        <Modal show={showModal} className={'deleteModal'}>
          <Modal.Body>
            <Row className={styles.rowStandard}>
              <h4>Generate PDF Report</h4>
              Do you want to generate the smile analysis and design report?
            </Row>
          </Modal.Body>
          <Modal.Footer className={styles.footer}>
            <Button type="button" className={styles.submitButton} onClick={handleSubmit}>No</Button>
            <Col className={styles.vr}></Col>
            <Button type="button" className={styles.submitButton} onClick={this.submit.bind(this)}>Yes</Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true })
  }

  submit() {
    const { printUrl } = this.props;
    const { handleSubmit } = this.props;

    this.setState({ showModal: false }); //Closing modal
    window.open(printUrl + '&step=2C', '_blank'); //redirecting to report page
    handleSubmit(); //submitting form
  }

}

SubmitButton.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  dispatch: PropTypes.func,
  printUrl: PropTypes.string,
};

export default SubmitButton;
