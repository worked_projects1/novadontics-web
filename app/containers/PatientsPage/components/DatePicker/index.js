/**
*
* <DatePicker />
* Wrapper for redux-form’s <Field /> and delegates work to ModernDatePicker component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import ModernDatePicker from '../ModernDatePicker';
import { Field } from 'redux-form/immutable';
import { Col } from '@sketchpixy/rubix';

class DatePicker extends React.Component {

  static propTypes = {
    data: PropTypes.object,
  };

  render() {
    const { data, flexStyle } = this.props;
    const { attributes = {} } = data;
    const type = attributes.calendar && attributes.note ? 'calendar' : 'date';
    const name = type === 'calendar' ? `${data.name}.notes` : data.name;
    return (
      <Col>
        <Field name={name} id={name} title={data.title} type={type} component={ModernDatePicker} data={data} flexStyle={flexStyle} />
      </Col>
    );
  }

}

export default DatePicker;