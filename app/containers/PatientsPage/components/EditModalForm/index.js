/**
*
* EditModalForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import ModalForm from 'components/ModalRecordForm';
import EditRecordForm from 'components/EditRecordForm';
import { browserHistory } from 'react-router';
import { updateImplantData } from 'blocks/implantLog/remotes.js';
import moment from 'moment-timezone';
import AlertMessage from 'components/Alert';

class EditModalForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      disableShare: false,
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  componentDidMount(){
    if(this.props.form == `editSharedPatient.${this.props.sharedId}`) {
      this.setState({ disableShare: true })
    }
  }

  render() {
    const { row, tableColumns, singleImage, style, form, metaData, onSubmit, sharedId } = this.props;
    const { photoIndex, isOpen, disableShare } = this.state;

    return (
      <Col xs={12} className={styles.actions}>
        <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => this.handleOpen()}>
          <Icon glyph="icon-fontello-edit" className={styles.icon} style={{color:'#EA6225'}} />Edit
        </Button>
        <Modal show={isOpen} backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span>Edit</span>
            </Modal.Title>
          </Modal.Header>
            <Modal.Body id="modalrecordform">
              <EditRecordForm
                fields={tableColumns}
                metaData={metaData}
                initialValues={row}
                form={form}
                sharedId={sharedId}
                disableShare={disableShare}
                onSubmit={onSubmit}
                patientSubmit={true}
                handleShare={(val) => this.handleShare(val)}
                onCancel={() => this.handleClose()}
              />
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  handleOpen() {
    this.setState({
      isOpen: true
    });
  }

  handleShare(val) {
    val == "true" ? this.setState({ disableShare: true }) : this.setState({ disableShare: false })
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handleSubmit(data) {

    const { row, dispatch, actions } = this.props;
    const { removalAlert, alertMessage } = this.state;
    const submitRecord = data.toJS();

    submitRecord.id = row.id
    submitRecord.dateOfRemoval = submitRecord.dateOfRemoval != "" ? moment(submitRecord.dateOfRemoval).format('MM/DD/YYYY') : "";
    if(submitRecord.dateOfRemoval != "" && submitRecord.reasonForRemoval == "") {
      this.setState({ removalAlert: true, alertMessage: "Please select Reason for Removal" });
    } else if(submitRecord.dateOfRemoval == "" && submitRecord.reasonForRemoval != "") {
      this.setState({ removalAlert: true, alertMessage: "Please select Date of Removal" });
    } else {
      updateImplantData(submitRecord)
        .then(response => {
          this.setState({ isOpen: false });
          dispatch(actions.loadRecords(true));
        })
    }

  }
}

export default EditModalForm;
