import React from 'react';
import PropTypes from 'prop-types';
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';
import "video-react/dist/video-react.css";
import { Map as iMap } from "immutable";
import Pad from 'react-signature-pad';
import shallowCompare from 'react-addons-shallow-compare';

class renderSignature extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    input: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = { showPrint: false};
    this.handleSignatureLoad = this.handleSignatureLoad.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  componentDidMount() {
      this.handleSignatureLoad();
  }
  
  componentDidUpdate(){
    this.handleSignatureLoad();
  }

  render() {
    const { label }= this.props;
    return (
      <Col id="signatureField" className={styles.Signature}>
        <label htmlFor={label}>
          {label}
        </label>
        <Pad ref={signature => (this.signature = signature)} onEnd={this.handleSignature.bind(this)} />
      </Col>
    );
  }

  handleSignature() {
    const { input} = this.props;
    const dataURL = this.signature.toDataURL();
    input.onChange(dataURL);
  }

  handleSignatureLoad() {
    const { input } = this.props;
    const Input = iMap(input).toJS();
    this.signature.fromDataURL(Input.value);
    
  }
  
}

export default renderSignature;
