/**
*
* SelectField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

import Textarea from '../Textarea';
import { inviteResend } from 'blocks/patients/remotes.js';
import AlertMessage from 'components/Alert';
import moment from 'moment';

class SelectField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showNotes: null, successAlert: false, failureAlert: false, alertText: '' };
  }
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };

  handleResend() {
    const { id } = this.props;
    inviteResend(id)
      .then(resp => { this.setState({ successAlert: true, alertText: resp[0].response }); })
      .catch((error) => { this.setState({ failureAlert: true, alertText: 'Something went wrong. Please try again. If the problem persists contact support team.' }); })
  }

  render() {
    const { data = {}, options = {}, optionsMode, initialValue, showModal, metaData, lastInvitationSent } = this.props;
    const { successAlert, failureAlert, alertText } = this.state;
    const { attributes } = data;
    const { required } = options.general || {};
	  let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }
    const normalize = data.number ? (value) => Number(value) : null;
    const { showNotes } = this.state;

    const setNotes = (event) => {
      if(attributes && attributes.modalOptions){
        showModal(event.target.options[event.target.selectedIndex].text);
      }
      this.setState({showNotes : event.target.value == 'other' ? true : false});
    }

    return ( 
      <Col xs={12} className={optionsMode ? styles.selectfield : null}>
        <div id={data.title == 'Choose the Consult Area' ? 'consultArea': null}>
        {optionsMode ?
          <label htmlFor={data.title}>
            {data.title == "Carrier Name (Choose UCR, Private, or any insurance entered in the practice setup)" ?
                <span>Carrier Name <span style={{ fontSize: '13px' }}>(Choose UCR, Private, or any insurance entered in the practice setup)</span></span>
                : data.title == "Online Registration Status" && initialValue == "Invitation Sent" ?
                <span>Online Registration Status <span style={{ color:"#ea6225", cursor: 'pointer' }} onClick={this.handleResend.bind(this)}>(Resend)</span><span style={{ paddingLeft: '5px' }}>Last Invite Sent : {moment(lastInvitationSent).format('MM/DD/YYYY h:mm A')}</span></span>
                : <span style={data.changeStyle ? {color:"#ea6225"} : null}>{data.title}</span>
            }
            {required ? <span style={{color:"#ea6225",fontSize:"25px"}}>*</span> : <span style={{fontSize:"25px"}}></span>}
          </label> : null}
          <Field
            name={optionsMode ? data.name : `${data.name}.selectedOption`}
            id={optionsMode ? data.name : `${data.name}.selectedOption`}
            component='select'
            {...options.general}
            {...extraFieldOptions}
            normalize={normalize}
            onFocus={e => e.preventDefault()}
            onBlur={e => e.preventDefault()}
            className={styles.selectField}
            style={data.showDropdown ? { background: `url(${require('img/module/down.png')}) 98% center / 3% no-repeat` } : null}
            onChange={ (event) => {
              if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
                this.props.inputchange(event.target.value, data.name);
              }
              setNotes(event) }}
            {...options.general} >
              <option value="" disabled={data.selectOption ? false : true}>Choose Option</option>
              {(attributes && attributes.options) || (data.options && Array.isArray(data.options))  ?
                ((attributes && attributes.options) || data.options || []).map((option, index) =>
                  <option key={`select-${option.title}-${index}`} value={option.value}>{option.title}</option>) : 
                  data.options && metaData[data.options] ? 
                  (data.options && metaData[data.options] || []).map((option, index) =>
                  <option key={`select-${option.name}-${index}`} value={option.value}>{option.name}</option>) : null}
          </Field>
        </div>
        {(initialValue && initialValue.selectedOption == 'other' && showNotes == null) || showNotes ? <div>
          <Textarea
            data={data}
            options={options}
            optionsMode={optionsMode}
          />
        </div> : null }
        {this.state.successAlert ?
          <AlertMessage success dismiss={() => { this.setState({ successAlert: false }); }}>{alertText}</AlertMessage> : null}
        {this.state.failureAlert ?
          <AlertMessage warning dismiss={() => { this.setState({ failureAlert: false }); }}>{alertText}</AlertMessage> : null}
        </Col>
    );
  }
}

export default SelectField;
