/**
*
* EditPatientForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm, change } from 'redux-form/immutable';
import { browserHistory } from 'react-router';
import { ImplementationFor, ContentTypes } from '../CreatePatientForm/utils';
import Info from '../Info';
import SubmitButton from '../SubmitButton';
import styles from './styles.css';

import { Row, Col, Button } from '@sketchpixy/rubix';

const EditPatientForm = (props) => {
  const { id, handleSubmit, pristine, submitting, formSchema, onPrescriptionsClick, path, metaData, formId, initialValues, printUrl, dispatch, form, disable, modal, disableHeight, roundBorder, platform, shared, cancelbtn, prescriptionDisable, disableHeader, handleExpand, isExpand, handleExpandPD, isExpandPD, record = {}, editNotes, lastInvitationSent } = props;
  const formArray = ['1H', '1D', '1AL', '2C', '1K'];

  const handleChange = (val, value) => {
    if (value === "preMadeNotes") {
      metaData.preMadeClinicalNotesOptions.map(response => {
        if(val == "") {
          dispatch(change(form, "note", ""));
        }
        if(response.name == val) {
          dispatch(change(form, "note", response.note));
        }
      })
    }
    if(value === "oralIVsite"){
      if(val == ""){
        dispatch(change(form, "oralIVsite", ""));
      } else {
        dispatch(change(form, "oralIVsite", val));
      }
    }

  }

  const renderSimpleField = (field) => {
    const Component = ImplementationFor[field.radio ? 'Radiobox' : field.type];
    const contentType = ContentTypes[field.type];
    return ((field.type || field.radio) && Component ?
      <Component
        id={id}
        form={form}
        formId={formId}
        data={field}
        title={field.title}
        contentType={contentType}
        onPrescriptionsClick={onPrescriptionsClick}
        prescriptionDisable={prescriptionDisable}
        options={field.required ? { general: { required: true } } : {}}
        optionsMode="edit"
        metaData={metaData}
        editNotes={editNotes}
        lastInvitationSent={lastInvitationSent}
        initialValue={initialValues && initialValues.toJS()[field.name] || {}}
        path={path}
        dispatch={dispatch}
        record={record}
        inputchange={(val, value) => handleChange(val, value)}
      /> : null);
  }

  const renderGroupField = (columnItem) => {
    let isCollapse = columnItem.group.collapse ? true : false;
    let isCollapsePD = columnItem.group.collapsePD ? true : false;
    return columnItem.group.fields.map((field, fieldIndex) =>
      <Col sm={12} className={styles.columnItem} key={`columnItem-${fieldIndex}`}>
        {(isCollapse && !isExpand) || (isCollapsePD && !isExpandPD) ? null : field && field.type ?
          renderSimpleField(field) : null}
      </Col>)
  }

  const slider = (slideIndex) => {
    if (slideIndex != undefined) {
      formSchema.map((section) =>
        section.forms.map((form) => {
          if (form.id.toString() == formId.toString()) {
            form.schema.map((schema, index) => {
              if (`page-${index}` == `page-${slideIndex}`) {
                document.getElementById(`page-${slideIndex}`).classList.remove(styles.dispnone);
                document.getElementById(`slide-${slideIndex}`).classList.add(styles.activeSlider);
              } else {
                document.getElementById(`page-${index}`).classList.add(styles.dispnone);
                document.getElementById(`slide-${index}`).classList.remove(styles.activeSlider);
              }
            })
          }
        }))
    }
  }


  return (
    <form onSubmit={handleSubmit} style={props.style || {}}>
      {formSchema ? formSchema.map((section, i) =>
        section.forms.map((form) =>
          form.id.toString() == formId.toString() ?
            <Row key={`form-${form.id}`}>
              {form.schema.map((schemaItem, schemaIndex) =>
                <Col id={`page-${schemaIndex}`} className={schemaIndex > 0 ? styles.dispnone : null} key={`schemaItem-${schemaIndex}`}>
                  {!modal && !disableHeader ? <Col sm={12} className={styles.header1A}>
                    <Link onClick={() => platform && platform === 'iPad' ? window.location = 'inapp://cancel' : formArray.includes(formId) ? browserHistory.push(`${path}/${id}/form`) : browserHistory.goBack()}>
                      {formId === "1A" ? null : <Button type="button" className={styles.btn}>Cancel</Button>}
                    </Link>
                    <h4 className={styles.subtitle}>{schemaItem.title}</h4>
                    {form.print ?
                      <SubmitButton printUrl={printUrl} handleSubmit={handleSubmit} pristine={pristine} submitting={disable} disable={disable} /> :
                      <Button bsStyle="standard" type="submit" disabled={!disable && submitting || disable || shared && formId.toString() == formId} className={!disable && submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Save</Button>}
                  </Col> : null}
                  <Col sm={12} className={roundBorder ? styles.schemaRoundBorder : styles.schemaBody} style={{ height: modal || disableHeight ? 'auto' : `${platform && platform === 'iPad' ? document.body.clientHeight - 70 + 'px' : document.body.clientHeight - 200 + 'px'}` }}>
                    {schemaItem.rows.map((row, rowIndex) =>
                      <Col xs={12} className={styles.rowItem} id={`row-${rowIndex}`} key={`row-${rowIndex}`}>
                        {row.columns.map((column, columnIndex) =>
                          column.noteStyle ?
                            <Col>
                              <h4 className={columnIndex == 0 ? styles.noteTitle : null} dangerouslySetInnerHTML={{ __html: column.title }} />
                              <Col xs={12} sm={row.columns.length === 2 ? 6 : row.columns.length > 2 ? 2 : 12} key={`column-${columnIndex}-${columnIndex}`} className={column.noteStyle ? styles.noteWidth : null}>
                                {column.columnItems.map((columnItem, columnItemIndex) =>
                                  <Col className={styles.columnItems} sm={12} key={`columnItems-${columnItemIndex}-${columnItemIndex}`}>
                                    {(columnItem.field && columnItem.field.type) || (columnItem.group && columnItem.group.radio) ?
                                      renderSimpleField(columnItem.group && columnItem.group.radio ? columnItem.group : columnItem.field) : renderGroupField(columnItem)}
                                  </Col>
                                )}
                              </Col>
                            </Col> :
                            <Col xs={12} sm={row.columns.length === 2 ? 6 : row.columns.length > 2 ? 4 : 12} key={`column-${columnIndex}-${columnIndex}`}>
                              {column.columnItems.map((columnItem, columnItemIndex) =>
                                <Col className={columnItem.flexStyle ? styles.flexItems : styles.columnItems} sm={12} key={`columnItems-${columnItemIndex}-${columnItemIndex}`}>
                                  {columnItem.group ?
                                    <Col sm={12} className={columnItem.group.inlineStyle ? styles.inlineTitle : !columnItem.group.noteStyle ? styles.groupTitle : styles.hideTitle}>
                                      {columnItem.group.collapse ?
                                        isExpand ?
                                          <img onClick={handleExpand} className={styles.plusIcon} src={require(`img/patients/minus_purple.svg`)} /> :
                                          <img onClick={handleExpand} className={styles.plusIcon} src={require(`img/patients/plus_purple.svg`)} /> : null}
                                      {columnItem.group.collapsePD ?
                                        isExpandPD ?
                                          <img onClick={handleExpandPD} className={styles.plusIcon} src={require(`img/patients/minus_purple.svg`)} /> :
                                          <img onClick={handleExpandPD} className={styles.plusIcon} src={require(`img/patients/plus_purple.svg`)} /> : null}
                                      {columnItem.group.title === "Patient Type" ? <div style={{ fontSize: "18px" }}>{columnItem.group.title}<span style={{ color: "#ea6225", fontWeight: "bold", fontSize: "20px" }}>*</span></div> : columnItem.group.title ? <h4 dangerouslySetInnerHTML={{ __html: columnItem.group.title }} /> : <Col className={styles.withoutTitle}></Col>}
                                      {columnItem.group.info ? <Info data={columnItem.group} /> : null}
                                    </Col> : null}
                                  {(columnItem.field && columnItem.field.type) || (columnItem.group && columnItem.group.radio) ?
                                    renderSimpleField(columnItem.group && columnItem.group.radio ? columnItem.group : columnItem.field) : renderGroupField(columnItem)}
                                </Col>
                              )}
                            </Col>
                        )}
                      </Col>
                    )}
                  </Col>
                </Col>
              )}
              {form.schema.length > 1 ?
                <Col className={formArray.includes(formId) ? styles.footer : styles.grayFooter} sm={12} >
                  {form.schema.map((slideItem, slideIndex) =>
                    <div id={`slide-${slideIndex}`} key={`slideIndex-${slideIndex}`} className={slideIndex === 0 ? styles.activeSlider : null} onClick={() => slider(slideIndex)} />
                  )}
                </Col> : null}
            </Row> : null
        )
      ) : null}
      {modal ?
        <Col className={styles.modalBtn}>
          <Button bsStyle="standard" type="submit" disabled={!disable && submitting || shared || disable} className={!disable && submitting ? `buttonLoading` : ''}>{props.btnName || 'Add'}</Button>
          {cancelbtn ? null :
            <Button bsStyle="standard" type="button" className={styles.cancelBtn} onClick={props.onCancel.bind(this)}>{props.cancelBtnName || 'Cancel'}</Button>}
        </Col>
        : null}
    </form>
  );
};




EditPatientForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  formSchema: PropTypes.array,
  path: PropTypes.string,
  metaData: PropTypes.object,
  formId: PropTypes.string,
  printUrl: PropTypes.string,
};

export default reduxForm({
  form: 'editPatient',
  enableReinitialize: true
})(EditPatientForm);
