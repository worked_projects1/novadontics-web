/**
*
* ModalField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Modal } from '@sketchpixy/rubix';
import styles from './styles.css';


class ModalField extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: true };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { title } = this.props;
    return (
      <Col>
        <Modal show={this.state.showModal} backdrop="static" className={'deleteModal'} >
            <Modal.Header>
              <Modal.Title>
                {title} 
              </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.ModalField}>
              {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

}

ModalField.propTypes = {
  title: PropTypes.string
};

export default ModalField;
