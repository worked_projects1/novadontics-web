/**
*
* InfoBlock
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col } from '@sketchpixy/rubix';

import styles from './styles.css';

class InfoBlock extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };
  render() {
    const { data } = this.props;
    return (
      <Col className={styles.InfoBlock} dangerouslySetInnerHTML={{__html: data.text}} />
    );
  }
}

export default InfoBlock;
