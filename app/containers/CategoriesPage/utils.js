import React from 'react';
import {sortBy} from "lodash.sortby";
import {Link} from 'react-router';
import DeleteRecordForm from 'components/DeleteRecordForm';
import styles from './styles.css';

export function setSortedCategories(categories, treeIndex) {
  return categories.map(category => ({
    id: category.id,
    order: treeIndex,
    name: category.titleHelper,
    expanded: false,
    children: category.children ? category.children.map((child, index) => ({
      id: child.id,
      order: index,
      name: child.titleHelper,
    })) : []
  }));
}
export function setCategories(categories, path, deleteRecord) {
  function handleDelete(data, dispatch, {form}) {
    const {id} = data.toJS();
    dispatch(deleteRecord(id, form));
  }

  const listCategories = categories.map(category => ({
    id: category.id,
    order: category.order || -1,
    title: <div>
      <div className={styles.categoryTitle}>{category.name.length > 40 ? category.name.slice(0, 40) + '...' : category.name}</div>
      {path.indexOf('ciicourses') > -1 ? null : <div className={styles.options}>
        <Link to={`${path}/${category.id}/edit`}>Edit</Link>
        <DeleteRecordForm initialValues={{id: category.id}} form={`deleteRecord.${category.id}`} onSubmit={handleDelete}/>
      </div>}
    </div>,
    expanded: path.indexOf('categories') === 1 || path.indexOf('tutorials') === 1 || path.indexOf('manuals') === 1 ? false : true,
    titleHelper: category.name,
    children: category.children ? category.children.map(child => ({
      id: child.id,
      order: child.order || -1,
      title: <div>
        <div className={styles.categoryTitle}>{child.name.length > 40 ? child.name.slice(0, 40) + '...' : child.name}</div>
        <div className={styles.options}>
          <Link to={`${path}/${child.id}/edit`}>Edit</Link>
          <DeleteRecordForm initialValues={{id: child.id}} form={`deleteRecord.${child.id}`} onSubmit={handleDelete}/>
        </div>
      </div>,
      titleHelper: child.name,
    })).sort((a, b) => a.order > b.order) : []
  }));

  return (
    _.sortBy(listCategories, 'order')
  );
}



