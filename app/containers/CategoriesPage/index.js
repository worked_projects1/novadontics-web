/*
 * CategoriesPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';

import Spinner from 'components/Spinner';
import { setCategories, setSortedCategories } from './utils.js';

import SortableTree from 'react-sortable-tree';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import styles from './styles.css';

import { Button, PanelContainer, PanelBody, Row, Col, Alert } from '@sketchpixy/rubix';

export default function (name, path, create, columns, actions, selectors, deleteRecord, updateRecords, disableChildren) {
  const {
    selectLoading,
    selectRecords,
    selectError,
  } = selectors;

  class CategoriesPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
      records: PropTypes.array,
      children: PropTypes.object,
      error: PropTypes.bool,
      loading: PropTypes.bool,
    };

    constructor(props) {
      super(props);

      this.state = {
        treeData: [],
      };

      this.handleChange = CategoriesPage.handleChange.bind(this);
      this.handleReOrder = CategoriesPage.handleReOrder.bind(this);
    }

    componentWillMount() {
      this.setState({
        treeData: setCategories(this.props.records, path, deleteRecord)
      });
    }

    componentDidMount() {
      if (this.props.dispatch) {
        this.props.dispatch(actions.loadRecords());
      }
    }

    componentWillReceiveProps(nextProps) {
      this.setState({
        treeData: setCategories(nextProps.records, path, deleteRecord)
      })
    }

    static handleChange(treeData) {
      const children = treeData.filter(_=>_.children && _.children.length > 0);
      this.setState({
        treeData: (disableChildren && children.length > 0) ? this.state.treeData : treeData,
      });
    }

    static handleReOrder(nodeDiff) {
      const { treeData }  = nodeDiff;
      const children = treeData.filter(_=>_.children && _.children.length > 0);

      const recordsReMap = setSortedCategories(disableChildren && children.length > 0 ? this.state.treeData : treeData);
      this.props.dispatch(updateRecords(recordsReMap));
    }

    render() {
      const { error, loading, children } = this.props;
      const { treeData } = this.state;

      let displayName = name;
      if(name === 'categories') {
        displayName = 'Catalog Categories'
      } else if(name === 'ciicourse Categories'){
        displayName = 'Implant Education Categories'
      } else if(name === 'vendorGroup Categories'){
        displayName = 'Vendor Groups'
      }
      return (
        <div>
          <Helmet title="Novadontics" />
          <div>
            {create ?
              <Row>
                <Col sm={12}>
                  <h1 className={styles.title}>{displayName}</h1>
                  <Link to={`${path}/create`}>
                    <Button bsStyle="standard" className="action-button right">Create {displayName}</Button>
                  </Link>
                </Col>
              </Row> : null}
            <Row>
              <Col md={6} lg={6} style={{ overflow: 'hidden', marginBottom: 100 }}>
                {error ?
                  <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
                  <div style={loading ? { opacity: 0.4 } : {}}>
                    <SortableTree
                      treeData={treeData}
                      maxDepth={2}
                      isVirtualized={false}
                      onMoveNode={this.handleReOrder}
                      onChange={this.handleChange}
                    />
                  </div>}
                {loading ? <Spinner /> : null}
              </Col>
              {children ?
                <Col md={6} lg={6} className="print-ready">
                  <PanelContainer className={!create ? styles.space : null}>
                    <PanelBody>
                      <Col sm={12}>
                        {children}
                      </Col>
                    </PanelBody>
                  </PanelContainer>
                </Col>
                : null}
            </Row>
          </div>
        </div>
      );
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
    };
  }

  return connect(
    createSelector(
      selectLoading(),
      selectRecords(),
      selectError(),
      (loading, records, error) => ({ loading, records: records ? records.toJS() : [], error }),
    ),
    mapDispatchToProps,
  )(CategoriesPage);
}
