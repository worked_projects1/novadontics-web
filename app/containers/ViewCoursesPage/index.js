/*
 * ViewRecordPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import DeleteRecordForm from 'components/DeleteRecordForm';
import { Player } from 'video-react';
import "video-react/dist/video-react.css";
import PdfViewer from 'components/PdfViewer';

import styles from './styles.css';

export default function (path, columns, canEdit, deleteRecord, selectRecord) {
  function handleDelete(data, dispatch, { form }) {
    const { id } = data.toJS();
    dispatch(deleteRecord(id, form));
  }

  const ViewCoursesPage = ({ record }) =>
    <div className={styles.viewRecordPage}>
      {canEdit ?
        <div className={styles.actions}>
          <div className="closeDetailViewButton">
            <Link to={`${path}`}>Close</Link>
          </div>
          <Link to={`${path}/${record.id}/edit`}>Edit</Link>
          <DeleteRecordForm initialValues={{ id: record.id }} form={`deleteRecord.${record.id}`} onSubmit={handleDelete} />
        </div> : null }

      {columns.map((column) =>
        ((column.viewRecord && canEdit) ?
          <div key={column.id}>
            <div>
              <div className={styles.label}>{column.label}:</div>
              <p>{record[column.value]}</p>
              <hr />
            </div>
          </div>
          : null)
      )}
      {record.url ?
        record.url.endsWith('.pdf') ? <PdfViewer pdfUrl={record.url} /> : <Player src={record.url} playsInline={false} />
        : null}
    </div>;

  ViewCoursesPage.prototype.propTypes = {
    record: PropTypes.array,
  };

  function mapStateToProps(state, props) {
    let id = path.indexOf('consents') === -1 ? parseInt(props.params.id) : props.params.id;
    const selectedRecord = selectRecord(id)(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : {},
    };
  }

  return connect(mapStateToProps)(ViewCoursesPage);
}
