import React from "react";
import queryString from 'query-string';

class Redirect extends React.Component {
  componentDidMount() {
    const parsed = queryString.parse(window.location.search);
    window.location.replace(`/${parsed.to ? parsed.to : ''}`);
  }
  render() {
    return null;
  }
}

export default Redirect;