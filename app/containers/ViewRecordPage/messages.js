/*
 * ViewRecordPage Messages
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  noItems: {
    id: 'app.components.ViewRecordPage.noItems',
    defaultMessage: 'There are no records to show right now.',
  },
  loading: {
    id: 'app.components.ViewRecordPage.loading',
    defaultMessage: 'Loading…',
  },
  error: {
    id: 'app.components.ViewRecordPage.error',
    defaultMessage: 'There was an error loading the requested record. Please try again.',
  },  
  pdfError: {
    id: 'app.components.ViewRecordPage.pdfError',
    defaultMessage: 'End date should be greater than Start date',
  }
});
