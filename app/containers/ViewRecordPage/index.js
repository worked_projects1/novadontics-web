/*
 * ViewRecordPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import styles from './styles.css';
import DeleteActionLink from "../../components/DeleteActionLink";
import Gallery from 'components/NovaGallery';

const columValue = (column, value) => {
  switch (column.type) {
    case 'image':
      return (
        <img
          src={value || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}
          role="presentation"
          style={{ height: 64 }}
        />
      );
    case 'agreement':
      return (
        <Gallery data={value && Array.isArray(value) && value.length > 0 ? value : value && typeof value === 'string' ? value.split(',') : ['http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg']} viewOnly={true} />
      );
    case 'booleanLabel':
      return <p>{column.booleanLabelValue(value)}</p>;
    case 'checkbox':
      return <p>{column.booleanLabelValue(value)}</p>;
    case 'multiselect':
      return <p>{value ? value.split(',').map((mapValue,index) => <div key={index}>{mapValue}</div>) : null}</p>;
    case 'multicheckbox':
      return <p>{value ? value.split(',').map((mapValue,index) => <div key={index}>{mapValue}</div>) : null}</p>;    
    default:
      return <p>{value}</p>;
  }
};

export default function (path, columns, deleteRecord, selectRecord, decorators, editRecord = true, decoratorView) {
  const ViewRecordPage = ({ record }) =>
    <div className={styles.viewRecordPage}>
      <div className={styles.actions}>
        <div className="closeDetailViewButton">
          <Link to={{ pathname: `${path}`, state: record.locationState }}>Close</Link>
        </div>
        {editRecord && <Link to={{ pathname: `${path}/${record.id}/edit`, state: record.locationState }}>
          Edit
        </Link>}
        <DeleteActionLink deleteRecord={deleteRecord} record={record} decorators={decorators}/>
      </div>

      {decoratorView && record && React.createElement(decoratorView, {record})}
       
      {columns.map((column) =>
        (column.viewRecord ?
          <div key={column.id}>
            <div>
              <div className={styles.label}>{column.label}:</div>
              {columValue(column, record[column.value])}
              <hr />
            </div>
          </div>
        : null)
       )}
    </div>;

  ViewRecordPage.prototype.propTypes = {
    record: PropTypes.array,
  };

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const record = selectedRecord ? selectedRecord.toJS() : {};
    Object.assign(record, { locationState: props.location.state });
    return {
      record,
    };
  }

  return connect(mapStateToProps)(ViewRecordPage);
}
