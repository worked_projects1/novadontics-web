/**
*
* RequestConsultationForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm } from 'redux-form/immutable';
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab } from '@sketchpixy/rubix';
import styles from './styles.css';
import {ImplementationFor} from './utils';

class ChangePasswordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { fields, handleSubmit } = this.props;
    return (
        <Col>
          {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
          <Modal show={this.state.showModal} backdrop="static" onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
              <Modal.Title>
                <div style={{ color: '#EA6225' }}>Change Password</div>
              </Modal.Title>
            </Modal.Header>
            <form onSubmit={handleSubmit}>
              <Modal.Body>
                <Row className={styles.rowStandard}>
                  {fields.map((field, i) => {
                    const Component = ImplementationFor[field.type];
                    return (
                      <div key={i} className="hidden-print">
                        <Component
                           data={field}
                           options={field.formFieldDecorationOptions}
                           optionsMode="edit"
                        />
                      </div>
                    );
                  })}
                </Row>
                <Row className={styles.rowStandardButton}>
                  <Button bsStyle='standard' type="submit" className={styles.submitbtn}>Submit</Button>
                </Row>
              </Modal.Body>
            </form>
          </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
  	this.setState({ showModal: true });
  }


}


export default reduxForm({
  form: 'changePassword',
})(ChangePasswordForm);
