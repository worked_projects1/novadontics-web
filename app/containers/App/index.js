/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { Link } from 'react-router';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import store2 from 'store2';
import ShowAlert from '../../module/fragment/ShowAlert';
import messages from './messages';
import { selectUser, selectSession, selectSplitBar } from 'blocks/session/selectors';
import { splitBar, logOut, verifySession } from 'blocks/session/actions';
import styles from './styles.css';
import '!style!css!main.css';
import { logInSuccess } from 'blocks/session/actions';
import { updatePassword } from 'blocks/session/remotes';
import { getDoseSpotDetails } from 'blocks/patients/remotes.js';
import { Grid, Row, Col, SidebarNav, Sidebar, Navbar, Icon, SidebarBtn } from '@sketchpixy/rubix';
import { includes } from "lodash";
import AccessDeniedPage from '../AccessDeniedPage';
import map from 'blocks/records/map';
import LicenseAgreement from '../../components/LicenseAgreementForm';
import AlertMessage from '../../components/Alert';
import NavigationItem from './navigationItems.js';
import { FormattedMessage } from 'react-intl';
import { browserHistory } from 'react-router';
import DoseSpot from './components/doseSpot.js';
import  CeInfoForm from '../../components/CeInfoForm';

const WEB_ACCESS = map().accounts().appSquareOptions || [];
const DEFAULT_PATH = process.env.PUBLIC_PATH || '';
const NATIVE_PLATFORM = [
  'iPad Simulator',
  'iPhone Simulator',
  'iPod Simulator',
  'iPad',
  'iPhone',
  'iPod'
].includes(navigator.platform);



export default function (pages) {

  class App extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
      children: PropTypes.node,
      user: PropTypes.object,
      dispatch: PropTypes.func,
    };

    constructor(props) {
      super(props);
      this.handleLogout = this.handleLogout.bind(this);
      this.handleOpenSidebar = this.handleOpenSidebar.bind(this);
      this.handleTabFocus = this.handleTabFocus.bind(this);
      this.checkSession = this.checkSession.bind(this);
      this.checkSessionTimeout = null;
      this.handlealertIncorrectMessage = this.handlealertIncorrectMessage.bind(this);
      this.handlealertWarningMessage = this.handlealertWarningMessage.bind(this);
      this.handlealertErrorMessage = this.handlealertErrorMessage.bind(this);
     
      this.state = {
        open: false,
        doseSpotCount: '0',
        doseSpotUrl: '',
        showDoseSpot: false,
        doseSpotStatus: '',
        versionv:false,
        logOutInProgress: false,
        alertErrorMessage: false,
        alertIncorrectMessage: false,
        alertWarningMessage: false,
        changePasswordSuccess: false,
        friendReferralSuccess: false
      };
      this.navigationItems = pages.slice(0);
      if (this.props.user && includes(['admin', 'novadonticsstaff'], this.props.user.role)) {
        this.navigationItems = this.navigationItems.filter(item => !_.get(item, ['data', 'isHiddenInNavigation']));
      }
      if(this.props.user && this.props.user.role == "dentist") {
        this.navigationItems = this.navigationItems.filter(item => !_.get(item, ['data', 'isHiddenInNavigation']));
      }
    }

    componentWillMount() {
      window.addEventListener('focus', :: this.handleTabFocus);
      const secret = store2.get('secret');
      const platform = store2.get('platform');
      const access_token = store2.get('access_token');
      const access_token_type = store2.get('access_token_type');
      if (secret) {
        this.props.dispatch(verifySession(secret, access_token, access_token_type, platform));
      }
    }

    componentDidMount() {
      console.log('App Started!');
      const elmnt = document.getElementById("splitBar");
      if (elmnt) {
        elmnt.addEventListener('click', () => this.props.dispatch(splitBar()));
      }
      if(this.props.user.doseSpotUserId != null && this.props.user.doseSpotUserId != '') {
        getDoseSpotDetails().then(doseSpotDetail => this.setState({ doseSpotStatus: doseSpotDetail[0].Result, doseSpotCount: doseSpotDetail[0].totalCount, doseSpotUrl: doseSpotDetail[0].message }));
      }
    }

    componentWillUnmount() {
      window.removeEventListener('focus', :: this.handleTabFocus);
      clearTimeout(this.checkSessionTimeout);
    }

    render() {
      const activePath = location.pathname;
      const { user = {}, session = {}, splitBar } = this.props;
      const { version } = session;
      const { appSquares = [], platform, allowAllCECourses } = user;
      const logBookAccess = appSquares && appSquares.length == 1 && appSquares.includes("implantLogBook") ? true : false;
      const { open, logOutInProgress, showDoseSpot, doseSpotStatus,versionv, doseSpotUrl } = this.state;
      const name = activePath.replace(/\//g, "") === '' && user.role === 'dentist' ?  'appointment' : activePath.replace(/\//g, "");
      const appURL = new URL(window.location.href);
      const native_platform = appURL.searchParams.get("platform");

      return (
        <div className={splitBar ? `splitWrapper` : null}>
          {platform === 'iPad' || native_platform === 'iPad' ? null :
            <div id="sidebar" className={open ? 'open' : null}>
              <div id="avatar">
                <Grid>
                  <Row>
                    <Col xs={12}>
                      {!splitBar ? <Link to={DEFAULT_PATH}><div className={styles.navItems}><img src={require('img/logo.svg')} className="img-responsive" alt="Novadontics" /></div></Link> : null}
                    </Col>
                  </Row>
                </Grid>
              </div>
              <div id="sidebar-container" style={{ overflowY: 'auto' }}>
                <div id="splitBar" style={splitBar ? { left: '53px' } : null} className={styles.splitBar}></div>
                <Sidebar sidebar={0} className={`${styles.sidebarInner} ${!splitBar ? styles.sidebar : styles.splitbar}`}>
                  <div>
                    <div className="sidebar-nav-container" style={{ overflowY: 'auto', overflowX: 'hidden' }}>
                      <SidebarNav style={{ marginBottom: 0 }}>
                        {user.role && ['admin'].includes(user.role) ?
                          <li className={activePath === '/' ? 'sidebar-nav-item active' : 'sidebar-nav-item'}>
                            <Link to={DEFAULT_PATH} className="name"><Icon glyph="icon-fontello-gauge" /> {!splitBar ? 'Dashboard' : ''}</Link>
                          </li> : user.role && ['dentist'].includes(user.role) ?
                          <li className={activePath === '/' ? 'sidebar-nav-item active' : 'sidebar-nav-item'}>
                            <Link to={DEFAULT_PATH} className="name"><Icon glyph="icon-fontello-popup" /> {!splitBar ? 'Apt. Book' : ''}</Link>
                          </li> : <span />}
                          {this.navigationItems.map((page, i) =>
                            <NavigationItem key={i}
                              alertWarning={() =>this.setState({  alertWarningMessage:true})}
                              alertError={()=>this.setState({alertIncorrectMessage:true})}
                              error={()=>this.setState({alertErrorMessage:true})}
                              success={()=>this.setState({changePasswordSuccess:true})}
                              referralSuccess={()=>this.setState({friendReferralSuccess:true})}
                              activePath={activePath} page={page} user={user} splitBar={splitBar} />)}
                      </SidebarNav>
                    </div>
                  </div>
                  <div className={styles.version}>
                    &#169; Novadontics, LLC V12.6.30.4
                  </div>
                </Sidebar>
              </div>
            </div>}

          {platform === 'iPad' || native_platform === 'iPad' ? null :
            <Grid id="navbar">
              <Row>
                <Col xs={12}>
                  <Navbar fixedTop fluid id="rubix-nav-header">
                    <Row>
                      <Col xs={3} visible="xs" className="burgerMenu">
                        <div onClick={this.handleOpenSidebar}>
                          <div className={open ? 'open' : null}>
                            <SidebarBtn />
                          </div>
                        </div>
                      </Col>

                      <Col sm={12} xs={9} className={styles.logout} >
                        <div className={open ? styles.open : null}>
                        {session && session.version != user.version ?
                            <div className={styles.passDropdown}>
                              <Col ref='foo' data-place={"bottom"} style={{ marginRight:'45px',marginTop:'-4px',cursor: 'pointer' }} onClick={() => this.setState({ versionv: true })} >
                                <img src={require(`img/alert.svg`)} style={{ width: '40px' }}/>
                              </Col>
                            </div> : null
                        }
                        {user && user.role == 'dentist' && doseSpotStatus == 'Success' ?
                          <div className={styles.passDropdown}>
                            <Col ref='foo' data-place={"bottom"} style={{ marginRight: '25px', cursor: 'pointer' }} onClick={() => this.setState({ showDoseSpot: true })}>
                              <img src={require(`img/patients/dose.svg`)} style={{ width: '35px' }}/>
                              <span className={styles.doseSpotCount}>{this.state.doseSpotCount}</span>
                            </Col>
                          </div> : null
                        }
                        {user && user.role == 'dentist' ?
                          <div className={styles.passDropdown}>
                            <Link to={{ pathname: '/catalog', state:{ openCart: true }}}>
                              <Col ref='foo' data-place={"bottom"} style={{ marginRight: '25px', cursor: 'pointer' }}>
                                <img src={require(`img/vendor/CartIcon.svg`)} style={{ width: '35px' }} />
                                <span className={styles.cartCount}>{user.cartQuantity}</span>
                              </Col>
                            </Link>
                          </div> : null
                        }
                        {user && user.practiceName ? `Hello, ${user.practiceName}` :`Hi, ${user.name}` } <button disabled={logOutInProgress} onClick={this.handleLogout} className="logoutButton"><Icon bundle="fontello" glyph="power" /></button>
                        </div>
                      </Col>
                    </Row>
                  </Navbar>
                </Col>
              </Row>
            </Grid>}

          <div id="body" className={open ? 'open' : null} style={platform === 'iPad' || native_platform === 'iPad' ? { marginTop: '0px' } : {}}>
            <Grid className={styles.wrapper} style={platform === 'iPad' || native_platform === 'iPad' ? { padding: '0px' } : {}}>
              <div className={styles.wrapper}>
              {((WEB_ACCESS.map(_=> _.value).includes(name) && !appSquares.includes(name)) || (!appSquares.includes("patients") && (name =="consultations")) || ((!appSquares.includes("CE10") && !appSquares.includes("CE3") && !appSquares.includes("CE5") && allowAllCECourses == false) && (name =="courses" || name == "educationRequests")) || (!appSquares.includes("implantLogBook") && (name =="implantLog")) || (logBookAccess && (name == "ledger" || name == "ceCourses")) || (!appSquares.includes("requestConsultation") && (name =="inOfficeSupport"))  || (!appSquares.includes("patientEducation") && (name =="patient-courses")) || (!appSquares.includes("CIICourse") && (name =="ciicourses")) ||(!appSquares.includes("appointments") && (name =="setup" || name == "appointment")) || ((!appSquares.includes("catalog") && (name =="shipping" || name == "orders")))) && user.role != 'admin' ?  <AccessDeniedPage />  :
                 React.Children.toArray(this.props.children)}</div>
            </Grid>
          </div>

          <div>
            <DoseSpot url={doseSpotUrl} show={showDoseSpot} hide={() => this.setState({ showDoseSpot: false })} />
          </div>
          <div>
            {user && user.role == 'dentist' && name == "courses" &&user.ceInfo == false ?
              <CeInfoForm data ={map().patients().continueEducationInfo}/> : null}
          </div>
          <div>
            {user && user.role == 'dentist' && user.agreementAccepted != true ?
              <LicenseAgreement/> : null}
          </div>
          {this.state.versionv ?
            <AlertMessage versionAlert dismiss={() => this.setState({ versionv: false })}></AlertMessage> : null}
          {this.state.alertIncorrectMessage ?
            <AlertMessage warning dismiss={this.handlealertIncorrectMessage}><FormattedMessage {...messages.incorrectPasswordError} /></AlertMessage> : null}
            {this.state.alertWarningMessage ?
            <AlertMessage warning dismiss={this.handlealertWarningMessage}><FormattedMessage {...messages.incorrectpasswordWarning} /></AlertMessage> : null}
          {this.state.changePasswordSuccess ?
            <AlertMessage success dismiss={()=>browserHistory.push(activePath)}><FormattedMessage {...messages.updatePasswordSuccess} /></AlertMessage> : null}
          {this.state.friendReferralSuccess ?
            <AlertMessage success checklist dismiss={() => this.setState({ friendReferralSuccess: false })}><FormattedMessage {...messages.referFriendSuccess} /></AlertMessage> : null}
          {this.state.alertErrorMessage ?
            <AlertMessage warning dismiss={this.handlealertErrorMessage}><FormattedMessage {...messages.changePasswordError} /></AlertMessage> : null}

          {/*version && user && user.version && version != user.version ?
            <ShowAlert error={messages.hotReload && messages.hotReload.defaultMessage} disableBtn /> : null */}
        </div>
      );
    }


    handlealertIncorrectMessage() {
      this.setState({ alertIncorrectMessage: false });
    }
    handlealertWarningMessage()
    {
      this.setState({  alertWarningMessage: false})
    }
    handlealertErrorMessage() {
      this.setState({ alertErrorMessage: false });
    }
    handleLogout() {
      this.setState({ logOutInProgress: true });
      this.props.dispatch(logOut());
    }

    handleOpenSidebar() {
      this.setState((prevState) => ({
        open: !prevState.open,
      }));
    }

    handleTabFocus() {
      this.checkSessionTimeout = setTimeout(this.checkSession, 500);
    }

    checkSession() {
      const secret = store2.get('secret');
      const platform = store2.get('platform');
      const access_token = store2.get('access_token');
      const access_token_type = store2.get('access_token_type');
      const { session } = this.props;
      const isLoggedIn = session ? session.loggedIn : false;
      if (isLoggedIn) {
        this.props.dispatch(verifySession(secret, access_token, access_token_type, platform));
      }
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
    };
  }

  return connect(
    createSelector(
      selectUser(),
      selectSession(),
      selectSplitBar(),
      (user, session, splitBar) => ({ user, session: session ? session.toJS() : {}, splitBar }),
    ),
    mapDispatchToProps,
  )(App);
}
