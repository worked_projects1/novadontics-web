import React from 'react';
import ChangePasswordForm from './changePassword.js';
import { SidebarNav, Icon } from '@sketchpixy/rubix';
import { Link } from 'react-router';
import styles from './styles.css';
import '!style!css!main.css';
import { updatePassword } from 'blocks/session/remotes';
import { friendReferral } from 'blocks/friendReferral/remotes';
import ModalForm from '../../components/ModalRecordForm';
import { reset } from 'redux-form';

class NavigationItem extends React.Component {
  constructor(props) {
    super(props);
    this.ChildrenItems = this.ChildrenItems.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFriend = this.handleFriend.bind(this);
  }
  ChildrenItems = () => {
    const { activePath, page, splitBar } = this.props
    const { children, id } = page;
    return <SidebarNav id={id} className={`${styles.dropdownContent} children-nav`} style={{ marginBottom: 0 }}>
      {children && children.map((item, i) => this.NavigationItem({ item }))}
    </SidebarNav>
  }

  NavigationItem = (navProps) => {
    const { activePath, splitBar, user } = this.props;
    const page = navProps && navProps.item || this.props.page;
    let selectedPath = user.role == "dentist" && activePath.indexOf('/inOfficeSupport') > -1 ? '/requestConsultation' : activePath;
    const { children, id } = this.props.page;
    const changePasswordColumns = [{ name: 'currentPassword', title: 'Current Password', type: 'input', hideText: true, formFieldDecorationOptions: { general: { required: true }, }, }, { name: 'newPassword', title: 'New Password', type: 'input', hideText: true, formFieldDecorationOptions: { general: { required: true }, }, }, { name: 'confirmPassword', title: 'Confirm Password', type: 'input', hideText: true, formFieldDecorationOptions: { general: { required: true }, }, }];
    const referFriendColumns = [ { id: 1, value: 'title', label: 'Title', visible: true, required: true, editRecord: true, type: 'input', }, { id: 2, value: 'firstName', label: 'First Name', visible: true, required: true, editRecord: true, type: 'input', }, { id: 3, value: 'lastName', label: 'Last Name', visible: true, required: true, editRecord: true, type: 'input', }, { id: 4, value: 'practiceName', label: 'Practice Name', visible: true, editRecord: true, type: 'input', }, { id: 5, value: 'phone', label: 'Phone#', visible: true, required: true, editRecord: true, type: 'input', }, { id: 6, value: 'email', label: 'Email', visible: true, editRecord: true, type: 'input', }, { id: 7, value: 'note', label: 'Note', visible: true, editRecord: true, type: 'textarea', }, ];

    if (!page.data) {
      return null;
    }

    switch (page.type) {
      case 'external-route':
        return (
          <li>
            <a href={page.url} className="name" target="_blank"><Icon glyph={page.icon} /> {!splitBar ? page.label : ''}</a>
          </li>
        );
      case 'password':
        return (
          <li>
            <a>
              <ChangePasswordForm fields={changePasswordColumns} onSubmit={this.handleSubmit}>
                {(open) => <button onClick={open.bind(this)} className="name" style={{ marginTop: '3px', padding: "0px", color: 'white' }}><Icon glyph={`icon-fontello-cog-outline`} /><span style={{paddingLeft:"5px",fontSize:"14px"}}>Change Password</span></button>}
              </ChangePasswordForm>
            </a>
          </li>
        );
      case 'referFriend':
        return (
          <li>
            <a>
              <ModalForm
                fields={referFriendColumns}
                form={`referFriend`}
                onSubmit={this.handleFriend}
                onRef={referFriend => (this[`referFriend`] = referFriend)}
                config={{ title: "Refer a Friend", btnType: 'link', glyphIcon: 'icon-fontello-user-add', referIcon: true, titleStyle: true }}
              />
            </a>
          </li>
        );
      case 'separator':
        return (
          <li>
            <hr className="separator" />
          </li>
        );
      case 'email':
        return (
          <li>
            <a href="mailto:team@novadontics.com?subject=&body=" className="name" target="_self"><Icon glyph={`icon-fontello-${page.icon}`} /> {!splitBar ? page.label : ''}</a>
          </li>
        )
      case 'menu':
        return (
          <li className={styles.dropdown} >
            <a onMouseOver={(e) => this.openMenu(e, id)} style={{ width: '200px' }} className={styles.dropbtn} target="_blank" ><Icon glyph={page.icon} /> {!splitBar ? page.label : ''}</a>
            {this.ChildrenItems({ activePath: activePath, children: children, splitBar: splitBar, id: id })}
          </li>
        )
      default:
        return (
          <li className={selectedPath.indexOf(page.data.path) >= 0 ? 'sidebar-nav-item active' : 'sidebar-nav-item'}>
            <Link to={page.data.path} className="name">
              {page.data.imgIcon === true ?
                <img src={require(`img/patients/${page.data.icon}.svg`)} ref='foo' className={styles.imgC} /> : <Icon glyph={`icon-fontello-${page.data.icon}`} /> }
              {!splitBar ? page.data.title : ''}
            </Link>
          </li>
        );
    }
  };

  openMenu = (el, id) => {
    const menu = document.getElementById(id);
    const bound = el.target.getBoundingClientRect();
    const top = bound.bottom - 80;
    if (menu) {
      menu.style.top = top + 'px';
    }
  }

  handleSubmit(data, dispatch) {
    const submitRecord = {};
    const record = data.toJS();
    if(record.newPassword != record.confirmPassword) {
      this.props.alertError();
    } else if(record.currentPassword === record.confirmPassword) {
      this.props.alertWarning();
    } else {
      submitRecord.new = record.newPassword
      submitRecord.current = record.currentPassword
      updatePassword(submitRecord)
        .then(passwordChange => this.props.success())
        .catch(() => this.props.error());
    }
  }

  handleFriend(data, dispatch, { form }) {
    const record = data.toJS();
    friendReferral(record)
      .then(response => {
        this[form].close();
        dispatch(reset(form));
        this.props.referralSuccess();
      })
      .catch(() => this.props.error());
  }

  render() {

    return (
      this.NavigationItem()
    )
  }
}
export default NavigationItem;