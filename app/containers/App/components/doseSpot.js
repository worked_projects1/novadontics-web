/**
*
* OrderHistory
*
*/

import React from 'react';
import { Modal, Col } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import Spinner from 'components/Spinner';
import styles from './styles.css';

class DoseSpot extends React.Component {

  constructor(props) {
      super(props);
      this.state = { loading: false };
  }

  handleFrame() {
      this.setState({loading: true});
      const iframe = document.getElementById('iframe-links');
      if(iframe){
          iframe.onload = () => {
              iframe.style.display = 'block';
              this.setState({loading: false})
          }

          iframe.src = this.props.url;
      }
  }

  render() {
    const { show, hide, url } = this.props;
    return (
        <Col>
            <Modal show={show ? true : false} className="clinicalNotes" bsSize="large" onHide={hide} onEnter={this.handleFrame.bind(this)}>
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <Col>
                        <iframe id="iframe-links" className={styles.iframe} />
                    </Col>
                </Modal.Body>
                { this.state.loading ? <Spinner /> : null}
            </Modal>
        </Col>
    );
  }

}

export default DoseSpot;
