
import InputField from 'containers/PatientsPage/components/InputField';

export const ImplementationFor = {
  input: InputField
};

export const ContentTypes = {
  Images: 'image/*,application/pdf,.stl',
  Videos: 'video/mp4, video/*',
};