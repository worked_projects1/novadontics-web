/*
 * App Messages
 *
 * This contains all the text for the DeleteUserForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  navigationTitle: {
    id: 'app.components.App.navigationTitle',
    defaultMessage: 'Navigation',
  },
  hotReload: {
    id: 'app.components.App.error',
    defaultMessage: 'You are using older version of app. <br/> Please do hard refresh to get latest changes. <br/> <br/> How to do hard refresh? <br/><b> For Windows/Linux</b> : Ctrl-F5. <br/><br/><b> For Mac</b>: <br/><b> For Safari</b>: Command (⌘)-Option-R. <br/><b> For Chrome or Firefox</b>: Command(⌘)-Shift-R.',
  },
  incorrectPasswordError: {
    id: 'app.components.App.incorrectPasswordError',
    defaultMessage: 'Confirm Password should match with New Password',
  },
  changePasswordError: {
    id: 'app.components.App.changePasswordError',
    defaultMessage: 'Please check the details you entered and try again',
  },
  incorrectpasswordWarning: {
   id:'app.components.App.incorrectpasswordWarning',
   defaultMessage: 'Current Password should not match with New Password',
  },
  updatePasswordSuccess: {
    id: 'app.components.App.updatePasswordSuccess',
    defaultMessage: 'Password updated successfully',
  },
  referFriendSuccess: {
    id: 'app.components.App.referFriendSuccess',
    defaultMessage: 'Thank you for providing your colleague’s contact information. We will be sending information about the Novadontics platform to your colleague. We appreciate the referral.',
  }
});
