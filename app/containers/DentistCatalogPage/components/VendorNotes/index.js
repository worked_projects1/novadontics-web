/**
*
* VendorNotes
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import { reset } from 'redux-form';
import {connect} from "react-redux";
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import { Link } from 'react-router';
import styles from './styles.css';
import moment from 'moment-timezone';
import Info from 'containers/PatientsPage/components/Info';
import EditRecordForm from 'components/EditRecordForm';

const ButtonItem = ({config, open}) => {
  const { title, glyphIcon, imageIcon, btnType, width, height } = config;
  switch(btnType){
    case 'link':
      return <Link className={styles.link} onClick={open}>
                <Icon className={styles.icon} glyph={glyphIcon ? glyphIcon : null} />{title || ''}
             </Link>
    case 'image':
      return <Col onClick={open}>
              {imageIcon && imageIcon.map((image,index)=><img key={index} width={width} height={height} src={image} alt={title || ''} className={styles.img} />)}
             </Col>
    default:
      return <Button bsStyle="standard" onClick={open} className='action-button'>{title || ''}</Button>
  }
}

const Children = ({children}) => children ? <Col style={{textAlign:"center"}} dangerouslySetInnerHTML={{__html: children}} /> : null;

const Tooltip = ({text, show}) => show ? <span className={styles.tooltip}>{text}</span> : null;

class VendorNotes extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false, confirmDialogBox : false, disableShare: false };
  }

  componentDidMount(){
    if(this.props.onRef) {
      this.props.onRef(this);
    }
  }

  componentWillReceiveProps(props) {
    if(this.props.error != props.error && this.props.config && this.props.config.showError) {
      this.props.config.showError(props.error);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { fields, metaData, onSubmit, error, updateError, pristine, submitting, config = {}, noteText, notes, dispatch, form, sharedId, infoColumns, initialValues, vendor, notesArr } = this.props;
    const { centerAlign, confirmDialog, title, bsSize, tooltip, viewType, shareDet, topView, bottomView, closeModal, modalClass, btnName, className, style, disable } = config;
    const { confirmDialogBox, disableShare } = this.state;
    const RowComponent = viewType == 'tab' ? Tabs : Row;
    const ColComponent = viewType == 'tab' ? Tab : 'div';

    const handleChange = (val, name) => {
      fields.filter(function(el) {
        return (el.value == "checkNumber") ? el.editRecord = ((val == 'Check') ? true : false)
        : el })
    }

    return (
      <Col style={style} className={title == 'Extend Validity' ? null : `${className ? className : ''} ${styles.ModalRecordForm}`}>
        <Children children={topView} />
        <ButtonItem config={config} open={this.open.bind(this)} />
        <Children children={bottomView} />
        <Tooltip text={title} show={tooltip} />
        <Modal show={this.state.showModal} bsSize={bsSize} onHide={this.close.bind(this)} backdrop="static" className={modalClass ? modalClass : ''} >
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span className={infoColumns ? styles.infoIconCls : styles.header} style={{ color:'#EA6225' }}>
                {title}
                {infoColumns ? <span className={styles.sharedInfoIcon}><Info data={infoColumns}/></span> : null}
              </span>
            </Modal.Title>
          </Modal.Header>
		        <Modal.Body id="modalrecordform">
              <Col sm={12} style={{ paddingLeft: '0px' }}>
                <div className={styles.divStyle}>
                  <h5 className={styles.codeStyle}>Vendor Name :
                  <span className={styles.descStyle}>{vendor.name}</span></h5>
                </div>
              </Col>
              <Col>
                <EditRecordForm
                  fields={fields}
                  form={form}
                  initialValues={initialValues}
                  onSubmit={this.handleSubmit.bind(this)}
                  disableCancel={true}
                  btnName={btnName}
                  catalogNotes={true}
                />
              </Col>
            </Modal.Body>
          {error || updateError ?
            <Alert danger>{error || updateError}</Alert> : null}
        </Modal>
      </Col>
    );
  }

  showconfirmDialog() {
    this.setState({ confirmDialogBox: true });
  }

  close() {
    this.setState({ showModal: false });
  }

  handleSubmit(data, dispatch, { form }) {
    this.props.onSubmit(data);
    dispatch(reset(form));
    this.setState({ showModal: false });
  }

  open(e) {
    e.stopPropagation();
    const { config = {}, fields } = this.props;
    const { record = {}, mode } = config;
  	const { start } = this.state;
  	if(!start && (mode == 'vendor' || mode == 'customer')) {
  		fields.filter(function(el) { return (el.value == "checkNumber") ? el.editRecord = ((record.paymentType == 'Check') ? true : false) : el })
    	this.setState({ showModal: true, start: true, confirmDialogBox: false })
    } else {
    	this.setState({ showModal: true, confirmDialogBox: false })
    }
  }

}

VendorNotes.propTypes = {
  notesArr: PropTypes.array,
  error: PropTypes.string,
  updateError: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  record: PropTypes.object,
  fields: PropTypes.array,
  config: PropTypes.object
};

const mapStateToProps = (state, props) => ({
    initialValues: props.notesArr != undefined && props.notesArr.length > 0  ? props.notesArr.find(_ => _.id == props.id) != undefined ? props.notesArr.find(_ => _.id == props.id) : null : null, // retrieve name from redux store
});

export default connect(
   mapStateToProps
)(reduxForm({
  form: 'vendorNotes',
  enableReinitialize: true,
})(VendorNotes));
