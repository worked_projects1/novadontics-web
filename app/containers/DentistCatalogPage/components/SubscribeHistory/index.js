/**
*
* OrderHistory
*
*/

import React from 'react';
import { Modal, Col, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import ReactTable from "react-table";
import schema from 'routes/schema';
import Spinner from 'components/Spinner';
import moment from 'moment';
import ConfirmButton from 'components/ConfirmButton';
import styles from './styles.css';
import { deleteSubscribeHistory } from 'blocks/orders/remotes.js';

class SubscribeHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showOrderDet:false, selectedOrder:false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  editableColumnProps = {
      Cell: props => {
          const { column } = props;
          const { columns, editRecord, deleteRecord, vendorId, orderId } = this.props;
          const { error } = this.state;

          return (column.action ?
              <Col className={styles.actions}>
                  <Button bsStyle="link" bsSize="xs" className={styles.btnDelete} onClick={()=>{this.handleSubscribe(props.original.id, props.original.productId, props.original.quantity, props.original.frequency, props.original.shippingAddressId)}}>
                      <Icon glyph="icon-fontello-edit" className={styles.icon} />Edit
                  </Button>
                  <ConfirmButton onConfirm={() => {this.handleDelete(props.original.id)}} onUnsubscribe={true}>
                      {(click) => <Button bsStyle="link" bsSize="xs" className={styles.btnDelete} onClick={() => click()}>
                          <Icon glyph="icon-fontello-stop" className={styles.icon} />Unsubscribe
                      </Button>}
                  </ConfirmButton>
                  </Col> : <span>{column.value  === 'shareTo' ? metaData[column.oneOf].find(e => e.value === props.value) && metaData[column.oneOf].find(e => e.value === props.value).label || '' : column.type === 'date' ? moment(props.value).format('MM/DD/YYYY') : column.serial  ? props.index + 1 : props.value}</span>);
     }
  };

  render() {
    const { show, hide, vendors,toggleCount, subscribedList, load, user } = this.props;
    const data = subscribedList ? subscribedList : [];
    const { showOrderDet, selectedOrder } = this.state;
    return (
        <Col>
            <Modal className="subscribeHistory" show={show} onHide={hide} onEnter={load}>
                <Modal.Header closeButton>
                    <Modal.Title style={{ color: '#ea6225' }}>
                      Subscribed Products
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {data ?
                      <ReactTable
                          columns={schema().products().subscribeHistory.map((column) => Object.assign(
                              {},
                              { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                          ))}
                          data={data ? data : []}
                          pageSize={data && data.length > 0 ? 10 : 0}
                          showPageJump={false}
                          resizable={false}
                          sortable={false}
                          showPageSizeOptions={false}
                          previousText={"Back"}
                          pageText={""}
                          noDataText={"No entries to show."}
                      /> : <Spinner /> }
                </Modal.Body>
            </Modal>
        </Col>
    );
  }

  handleSubscribe(subscribeId, id, amount, frequency, shippingAddressId){
    this.props.toggleCount(subscribeId, id, amount, frequency, shippingAddressId);
    const subscribeReload = setInterval(() => {
      this.props.hide();
      clearReloadTimer();
    }, 500);
    const clearReloadTimer = () => clearInterval(subscribeReload);
  }

  handleDelete(id) {
    const { load } = this.props;
    deleteSubscribeHistory(id)
      .then(deleteInvoice => {
        load();
      })
  }

}

export default SubscribeHistory;
