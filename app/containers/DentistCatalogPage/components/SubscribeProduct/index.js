import React from 'react';
import PropTypes from 'prop-types';
import {Button, Modal} from '@sketchpixy/rubix';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import styles from './styles.css';
import {Col, Row,Icon} from "@sketchpixy/rubix/lib/index";
import {groupBy} from "lodash";
import ConfirmOrder from "./ConfirmOrder";
import ConfirmButton from 'components/ConfirmButton';


export const OrderTable = ({cart, vendors = [], user, onOvernight, handleEdit, fields, isEditEnabled, viewOnly, salesTax, showOrderDetails, orderDetails, showQuantity, toggleCount, orderFrequency, changeFrequency,getSubscribeProductSalesTax,subproducts}) => {

  const grandTotal = cart.reduce((acc, p) => p.listPrice != undefined ? acc + (p.amount * p.listPrice) : acc + (p.amount * p.price), 0);
  const productGroups = groupBy(cart, 'vendorId');
  var taxtotal =0;
  var shipamttotal =0;
  var cardtotal =0;
  var saveamttotal=0;
  var orderHistoryOrderTotal=0;
  var orderHistoryShippingCost=0;
  var orderHistoryShippingCharge=0;
  var orderHistoryCreditCard=0;
  var orderHistoryTotalAmountSaved=0;
  var orderHistoryTax=0;
  var orderHistoryGrandTotal=0;
  for(var y in productGroups) {
    var taxamt =0;
    var amt =0;

    var selectedvendor={};
    var check=false;
    for(var x in productGroups[y])
    {
      check=true;
      if(productGroups[y][x].listPrice != undefined) {
         amt += productGroups[y][x].listPrice * productGroups[y][x].amount;
         saveamttotal += (productGroups[y][x].price * productGroups[y][x].amount) - (productGroups[y][x].listPrice * productGroups[y][x].amount);
      } else {
          amt += productGroups[y][x].price * productGroups[y][x].amount;
      }
    }
    if(check)
    {
      for(var z in vendors)
      {
        if(vendors[z].id == productGroups[y][0].vendorId)
        {
          selectedvendor= vendors[z];
          break;
        }
      }
      if(selectedvendor.shippingChargeLimit > amt && !productGroups[y][x].overnight)
      {
        shipamttotal += user.prime == true ? 0 : selectedvendor.shippingCharge;
      }
      if(selectedvendor.tax !=undefined && selectedvendor.tax !="")
      {
        taxtotal += ((amt/100)* selectedvendor.tax);
      }
      if(selectedvendor.creditCardFee !=undefined && selectedvendor.creditCardFee !="")
      {
        cardtotal += ((amt/100)* selectedvendor.creditCardFee);
      }
    }
  }
  //const taxAmount = currencyFormatter.format(taxtotal);
  const taxAmount = currencyFormatter.format(salesTax);
  const ShippTotal = currencyFormatter.format(shipamttotal);
  const savingsTotal = currencyFormatter.format(saveamttotal);
  const CreditCard = currencyFormatter.format(Math.round(cardtotal * 100.0) / 100.0);
  const overnightCount = Object.keys(productGroups).map(key => productGroups[key][0].overnight).filter(Boolean).length;
  const shippingCost = currencyFormatter.format(overnightCount * 52);
  const orderTotal =  grandTotal + shipamttotal + parseFloat(salesTax);

    if(showOrderDetails == true) {
          const orderHistoryItemGroups = groupBy(orderDetails.items, 'vendorId');
          orderHistoryOrderTotal =  orderDetails.items.reduce((acc, p) => p.listPrice != undefined ? acc + (p.amount * p.listPrice) : acc + (p.amount * p.price), 0);
          const orderHistoryOvernightCount = Object.keys(orderHistoryItemGroups).map(key => orderHistoryItemGroups[key][0].overnight).filter(Boolean).length;
          orderHistoryShippingCharge = user.prime == true ? currencyFormatter.format(0) : currencyFormatter.format(orderDetails.shippingCharge);
          orderHistoryShippingCost = currencyFormatter.format(orderHistoryOvernightCount * 52);
          orderHistoryTax = currencyFormatter.format(orderDetails.tax)
          orderHistoryCreditCard = currencyFormatter.format(orderDetails.creditCardFee)
          orderHistoryTotalAmountSaved = currencyFormatter.format(orderDetails.totalAmountSaved)
          orderHistoryGrandTotal = orderDetails.grandTotal
    }


  return (
    <div className={styles.OrderTable}>
      {
        Object.keys(productGroups).map((vendorId, vendorIndex) =>
          <Col key={vendorIndex}>
            <VendorOrder
              products={productGroups[vendorId]}
              vendor={vendors.find(_ => String(_.id) === String(vendorId))}
              showFrequency={true}
              toggleCount={toggleCount}
              changeFrequency={changeFrequency}
              orderFrequency={orderFrequency}
              onOvernight={onOvernight}
              handleEdit={handleEdit}
              fields={fields}
              showQuantity={showQuantity}
              isEditEnabled={isEditEnabled}
              getSubscribeProductSalesTax={getSubscribeProductSalesTax}
              subproducts={subproducts}
            />
          </Col>)
      }
      {
        !showOrderDetails ?
          <div>
            <h5><span>Item(s) Subtotal:</span><b>{` ${currencyFormatter.format(grandTotal)}`}</b></h5>
            <h5><span>Overnight Shipping Cost:</span><b> $0.00</b></h5>
            <h5><span>Ground Shipping Cost:</span>{ShippTotal != "$0.00"?` +`:``}<b>{` ${ShippTotal}`}</b></h5>
            <h5><span>Sales Tax:</span>{taxAmount != "$0.00"?` +`:``}<b>{` ${taxAmount}`}</b></h5>
            <h5><span>Processing Fee:</span><b> $0.00</b></h5>
            <h5><span>Total Amount Saved:</span><b>{` ${savingsTotal}`}</b></h5>
            <h5><span>Grand Total:</span><b>{` ${currencyFormatter.format(Math.round(orderTotal * 100.0) / 100.0)}`}</b></h5>
          </div> :
          <div>
            <h5><span>Item(s) Subtotal:</span><b>{` ${currencyFormatter.format(orderHistoryOrderTotal)}`}</b></h5>
            <h5><span>Overnight Shipping Cost:</span><b> $0.00</b></h5>
            <h5><span>Ground Shipping Cost:</span>{orderHistoryShippingCharge != "$0.00"?` +`:``}<b>{` ${orderHistoryShippingCharge}`}</b></h5>
            <h5><span>Sales Tax:</span>{orderHistoryTax != "$0.00"?` +`:``}<b>{` ${orderHistoryTax}`}</b></h5>
            <h5><span>Processing Fee:</span><b> $0.00</b></h5>
            <h5><span>Total Amount Saved:</span><b>{` ${orderHistoryTotalAmountSaved}`}</b></h5>
            <h5><span>Grand Total:</span><b>{` ${currencyFormatter.format(orderHistoryGrandTotal)}`}</b></h5>
          </div>
      }
    </div>)
};
const parseChange = (callback) => e => {
  const { name, checked } = e.target;
  const vendorId = name.split('-').slice(-1)[0];
  callback(vendorId, checked)
};
const defaultImage = 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg';
class VendorOrder extends React.PureComponent {
  state = {amount: undefined};
  render() {
    const { user, products = [], vendor, showFrequency, onOvernight, handleEdit, fields, isEditEnabled, showQuantity, toggleCount, orderFrequency, changeFrequency } = this.props;
    const amount = this.state.amount !=undefined ?this.state.amount: products[0].amount !=undefined?products[0].amount:1;
    const productPrice = currencyFormatter.format(products[0].price);
    const metaData = {frequencyOptions : [{ label: "1 week", value: "1 week" }, { label: "2 weeks", value: "2 weeks" }, { label: "3 weeks", value: "3 weeks" }, { label: "1 month (Most common)", value: "1 month" }, { label: "5 weeks", value: "5 weeks" }, { label: "6 weeks", value: "6 weeks" }, { label: "7 weeks", value: "7 weeks" }, { label: "2 months", value: "2 months" }, { label: "3 months", value: "3 months" }, { label: "4 months", value: "4 months" }, { label: "5 months", value: "5 months" }, { label: "6 months", value: "6 months" }]};
    const isPreDefinedSet = Array.isArray(metaData);
    return (
      <div>
        <Row>
          <Col md={6}>
            {vendor && <h4><b>{vendor.name}</b></h4>}
          </Col>
          <Col md={6}>
            <div className={styles.overnightContainer}>
              <h5 className={styles.overnightLabel}>
                <b>Deliver every</b>
              </h5>
              {
                showFrequency &&
                <td>
                  <select name={"frequency"} className={styles.InputField} defaultValue={orderFrequency} onChange={(e)=> changeFrequency(e.target.value)}>
                    {isPreDefinedSet ?
                      (data.oneOf || []).map((option) =>
                          <option key={option} value={option}>{option}</option>) :
                      (metaData.frequencyOptions || []).map((option) =>
                          <option key={option.value} value={option.value}>{option.label}</option>)}
                  </select>
                </td>
              }
            </div>
          </Col>
        </Row>
         {products.map((product, index) =>
            <Col key={index}>
              <ProductList getSubscribeProductSalesTax ={this.props.getSubscribeProductSalesTax}  product = {product} showQuantity = {showQuantity} showFrequency={showFrequency} toggleCount={toggleCount} subproducts={this.props.subproducts} />
            </Col>
         )}
      </div>
    )
  }
}
class ProductList extends React.PureComponent {
  constructor(props){
    super(props);
    const { product = {} } = props;
    this.state = {amount: product && product.amount ? product.amount : 1};
  }
  render(){
    const { product = {}, showFrequency, user, showQuantity, toggleCount,getSubscribeProductSalesTax,subproducts,} = this.props;
    const { amount } = this.state;
    const productPrice = currencyFormatter.format(product.price);
    return (
      <Row className={styles.productcellcontainer}>
      <Col sm={2} className={styles.imagecontainer}>
        <div className={product.imageUrl?styles.productWithoutImageView:styles.productImageView} >
          <div
            style={{
              height: 70,
              width: 70,
              backgroundImage: `url(${product.imageUrl && product.imageUrl.split(",")[0] || defaultImage})`,
              backgroundSize: 'contain',
            }}
          >
          </div>
        </div>
      </Col>
      <Col sm={4} style={{paddingLeft:0,fontWeight: 'normal'}}>
        <div className={styles.productButton}>
          {product.name}
        </div>
        <div className={styles.productButton} >
         {product.serialNumber}
        </div>
        <div className={styles.productButton} >
          {showQuantity == true ?
            <p>
              {product.unit != '' && product.unit != null ?
                product.unit.toLowerCase() == 'each' && product.packageQuantity ? `(${product.packageQuantity} pcs.)` :
                product.unit.toLowerCase() != 'each' && product.packageQuantity ? `( ${product.p7448ackageQuantity} pcs. / ${product.unit} )` :
                product.unit.toLowerCase() == 'each' && !product.packageQuantity ? `(1 pcs.)` :
                product.unit.toLowerCase() != 'each' && !product.packageQuantity ? `(1 pcs. / ${product.unit})` : null :
                product.packageQuantity ? `(${product.packageQuantity} pcs.)` : null
              }
            </p> : null}
        </div>
        <div style={{ display:"flex",color: "#000"}}>
            {product.listPrice != undefined ?
              <div>
                <p>
                  {product.unit != '' && product.unit != null ?
                    product.unit.toLowerCase() == 'each' && product.quantity ? `(${product.quantity} pcs.)` :
                    product.unit.toLowerCase() != 'each' && product.quantity ? `( ${product.quantity} pcs. / ${product.unit} )` :
                    product.unit.toLowerCase() == 'each' && !product.quantity ? `(1 pcs.)` :
                    product.unit.toLowerCase() != 'each' && !product.quantity ? `(1 pcs. / ${product.unit})` : null :
                    product.quantity ? `(${product.quantity} pcs.)` : null
                  }
                </p>
                <p><b>{currencyFormatter.format(product.listPrice)}</b>
                  <span>(<strike>{currencyFormatter.format(product.price)}</strike>)
                    <div>
                      <b className="save">Save {currencyFormatter.format(product.price - product.listPrice)}</b>
                    </div>
                  </span>
                </p>
              </div> :
            <p><b>${product.price}</b> </p>}
        </div>
      </Col>
      <Col  sm={6}>
        <div className={styles.productoption}>
        {showFrequency &&
          <div className={styles.productoption}>
            <div className={styles.cartaddsub}>
              <div onClick={this.handleChange.bind(this, false)}>
                <Icon bundle="fontello" glyph="minus"className={styles.cartsub}/>
              </div>
              <input
                className={styles.cartinput}
                type="number"
                step="1"
                min="1"
                value={amount}
                onChange={this.onChange}
              />
              <div onClick={this.handleChange.bind(this, true)}>
                <Icon bundle="fontello" glyph="plus" className={styles.cartadd} />
              </div>
            </div>
          </div>}
          <div>
            <div>{amount} items for</div>
            <div>{product.listPrice != undefined ? currencyFormatter.format(amount*product.listPrice) : currencyFormatter.format(amount*product.price)}</div>
          </div>
        </div>
      </Col>
 </Row>
    )
}

  async handleChange(add){
    const { product={} } = this.props;
    const { amount } = this.state;
    const val = add ? amount+1 : amount-1;
    this.setState({ amount : val > 0 ? val : 1 });
    this.props.toggleCount(product.id, val > 0 ? val : 1)
    this.props.getSubscribeProductSalesTax(Object.assign({},this.props.subproducts,{quantity:val}))
  }
}
class SubscribeProduct extends React.PureComponent {

  state = {showConfirm: false, page:1, activeCard: 0, defaultCard: 0, activePayment: [], closeModal: false};

  toggleConfirm = () => {
    this.setState({ showConfirm: !this.state.showConfirm});
  };

  nextstep() {
    this.setState({page:2});
    this.props.getSubscribeProductSalesTax(this.props.subproducts);
    if(this.props.cardDetails.length != 0 && this.props.cardDetails.paymentProfiles != undefined){
      this.props.cardDetails.paymentProfiles.map((card, i) => { if(card.defaultPaymentProfile == true) {this.setState({ activeCard: card.customerPaymentProfileId, defaultCard: card.customerPaymentProfileId, activePayment: card})}})
    }
  }

  closeCart = () => {
    this.setState({ closeModal: !this.state.closeModal});
  }

  closeSubscribe = () => {
    this.setState({page:1});
    this.setState({ closeModal: false});
  }

  render() {
    const { cart = [], user, onClose, addresses, subscribeOrder,getSubscribeProductSalesTax,subproducts, shown, vendors = [], onOvernight, viewOnly, salesTax, getSalesTax, cardDetails, authDetails, product, toggleCount, orderFrequency, changeFrequency, updateSubscribeOrder, subscribeOrderId } = this.props;
    const { activeCard, activePayment, defaultCard, closeModal } = this.state;
  return (
    <div>
      <Modal show={shown} backdrop="static" onHide={() => {this.setState({page:1}); onClose()}} bsSize="large" className={closeModal ? styles.hideModal : null}>
        <Modal.Header closeButton >
        <Modal.Title style={{fontWeight:"bold"}}>{updateSubscribeOrder ? "Update Subscribe" : "Subscribe"}</Modal.Title>
          {cart && cart.length ?
            this.state.page == 1 ?
              <div style={{display:"flex",fontSize:18,justifyContent:"center",fontWeight:"bold"}}>
                Step 1 of 3 (Subscribe Preview)
              </div> :
              <div style={{display: "flex", fontSize: 15, justifyContent: "space-between", alignItems: "center", marginTop: 20}}>
                <div style={{color:"#ea6225",cursor:"pointer"}} onClick={()=>{this.setState({page:1})}}>Back</div>
                <div style={{display:"flex",fontSize:18,justifyContent:"center",fontWeight:"bold"}}>
                  Step 2 of 3 (Select Shipping Address)
                </div>
                <div></div>
              </div>
          : null}
        </Modal.Header>
        <Modal.Body>
          {this.state.page == 1?
            <div>
              {cart && cart.length ?
                <OrderTable
                  user={user}
                  vendors={vendors}
                  viewOnly={viewOnly}
                  salesTax={salesTax}
                  cart={cart}
                  toggleCount={toggleCount}
                  changeFrequency={changeFrequency}
                  orderFrequency={orderFrequency}
                  subproducts={subproducts}
                  getSubscribeProductSalesTax={getSubscribeProductSalesTax}
                />
                :
                <div>
                  <p><em>No items in cart.</em></p>
                </div>
              }
              {cart && cart.length ?<div onClick={()=>this.nextstep()} className={styles.nextbtn}>Next Step</div>:null}
            </div>:
            <div>
              <ConfirmOrder
                user={user}
                shown={this.state.showConfirm}
                onClose={this.toggleConfirm}
                onCloseSubscribe={onClose}
                closeCart={this.closeCart}
                closeSubscribe={this.closeSubscribe}
                onConfirm={subscribeOrder}
                cart={cart}
                onOvernight={onOvernight}
                vendors={vendors}
                salesTax={salesTax}
                subproducts={subproducts}
                getSubscribeProductSalesTax={(records) => getSubscribeProductSalesTax(records)}
                getSalesTax={(id)=> getSalesTax(id)}
                cardDetails={cardDetails}
                authDetails={authDetails}
                activeCard={activeCard}
                defaultCard={defaultCard}
                activePayment={activePayment}
                orderFrequency={orderFrequency}
                updateSubscribeOrder={updateSubscribeOrder}
                subscribeOrderId={subscribeOrderId}
              />
            </div>
          }
        </Modal.Body>
      </Modal>
    </div>)
  }
}





SubscribeProduct.propTypes = {
  cart: PropTypes.array,
  onClose: PropTypes.func.isRequired,
  subscribeOrder: PropTypes.func.isRequired,
  onOvernight: PropTypes.func.isRequired,
  vendors: PropTypes.array,
  shown: PropTypes.bool,
};

export default SubscribeProduct;
