import React from "react";
import classNames from 'classnames';
import { Col, Label, Modal, ListGroup, Row, Icon, Button } from "@sketchpixy/rubix/lib/index";
import styles from './styles.css';
import EditRecordForm from 'components/EditRecordForm';
import moment from 'moment';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import Spinner from 'components/Spinner';
import { authorizeApi, updateCustId } from 'blocks/products/remotes.js';


const CardItem = ({card = {}, disabled, active, onClick, defaultCard, onSubmit}) =>
  <li className={classNames('list-group-item', disabled && 'disabled', active && 'active', active && styles.noHoverColor)} onClick={()=>onClick(card)}>
    {card.payment.creditCard.showCheck ?
      <Row>
        <Col md={3} xs={3}>
           {card.payment.creditCard.cardType == 'Check' ?
            <img style={{ width: '120px', height: '90px' }} src={require(`img/catalog/cheque.png`)} /> : null}
        </Col>
        <Col md={6} xs={6} style={{paddingLeft:0,fontWeight: 'normal'}}>
          <h5 style={{ textAlign: 'left', fontWeight: 'bold' }}>{card.payment.creditCard.checkTitle}</h5>
          <h5 style={{ textAlign: 'left' }}>{card.payment.creditCard.checkText}</h5>
        </Col>
      </Row> :
      <Row>
        <Col md={3} xs={3}>
          {
           card.payment.creditCard.cardType == 'Visa' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/visa.png`)} /> :
           card.payment.creditCard.cardType == 'MasterCard' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/master.png`)} /> :
           card.payment.creditCard.cardType == 'Discover' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/discover.png`)} /> :
           card.payment.creditCard.cardType == 'AmericanExpress' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/american-express.png`)} /> :
           card.payment.creditCard.cardType == 'JCB' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/jcb.png`)} /> :
           card.payment.creditCard.cardType == 'DinersClub' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/dinner.png`)} /> :
           card.payment.creditCard.cardType == 'CarteBlanche' ?
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/carte-blanche.png`)} /> :
            <img style={{ width: '120px', height: '75px' }} src={require(`img/catalog/unknown.png`)} />
          }
        </Col>
        <Col md={2} xs={3} style={{paddingLeft:0,fontWeight: 'normal'}}>
          <h5 style={{ textAlign: 'left' }}>Card Number</h5>
          <h5 style={{ textAlign: 'left' }}>{card.payment.creditCard.cardNumber}</h5>
        </Col>
        <Col md={1} xs={1}>
          {defaultCard == card.customerPaymentProfileId ? <h5 style={{ textAlign: 'left' }}><Label>Default</Label></h5> : null}
        </Col>
        <Col md={6} xs={5} className={styles.address}>
          <h5>EXPIRES ON</h5>
          <h5>{moment(card.payment.creditCard.expirationDate).format('YY/MM')}</h5>
          { defaultCard == card.customerPaymentProfileId ? null :
            <div style={{color:"#ea6225",cursor:"pointer", fontSize: '12px'}} onClick={() => onSubmit(card)}>
              Mark as Default
            </div> }
        </Col>
      </Row> }
  </li>;


class PaymentForm extends React.PureComponent {
 constructor(props) {
    super(props);
    this.state = { showModal: false, activeCard: props.activeCard, defaultCard: props.defaultCard, activePayment: props.activePayment, processingFee: props.cardtotal, newPayment: false, loading: false, cardDetails: props.cardDetails, alertMessage: false, netAmount: props.initialValues.amount };
  }

  onCancel() {
      this.setState({ newPayment: false })
  }

  onSelect(card) {
      const { netAmount, processingFee } = this.state;
      const { orderTotal } = this.props;
      this.setState({ activeCard: card.customerPaymentProfileId, activePayment: card });
      if(card.customerPaymentProfileId == "Check") {
        const updatedAmount = orderTotal + ((orderTotal * 2) / 100);
        const updatedProcessingFee = processingFee + ((orderTotal * 2) / 100);
        this.setState({ netAmount: updatedAmount, processingFee: updatedProcessingFee});
      } else {
        this.setState({ netAmount: this.props.orderTotal, processingFee: this.props.cardtotal});
      }
  }

  saveCard(data) {
    const { authDetails, user } = this.props;
    const { cardDetails, newPayment, alertMessage } = this.state;

    this.setState({ loading: true });
    const config = {
              transformRequest: [(data, headers) => {
                delete headers.common['Authorization'];
                delete headers.common['X-Auth-Token'];
                delete headers.common['timezone'];
                  if (data !== undefined) {
                     headers.post['Content-Type'] = 'application/json;charset=UTF-8';
                     return JSON.stringify(data)
                  } else {
                    return data;
                  }
                return data;
              }]
          };

    if(cardDetails.length != 0) {

      const submitRecord = data.toJS();

      const createPaymentJson = { "createCustomerPaymentProfileRequest": { "merchantAuthentication": { "name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey }, "customerProfileId": cardDetails.customerProfileId, "paymentProfile": { "billTo": { "firstName": user.firstName.substring(0, 50), "lastName": user.lastName.substring(0, 50), "address": submitRecord.address.substring(0, 60), "city": submitRecord.city.substring(0, 40), "state": submitRecord.state.substring(0, 40), "zip": submitRecord.zip.substring(0, 20), "country": submitRecord.country.substring(0, 60), "phoneNumber": submitRecord.phoneNumber.substring(0, 25) }, "payment": { "creditCard": { "cardNumber": submitRecord.cardNumber, "expirationDate": submitRecord.expirationDate, "cardCode": submitRecord.cvv } }, "defaultPaymentProfile": false }, "validationMode": authDetails.validationMode } }

      authorizeApi(authDetails.apiUrl, createPaymentJson, config)
        .then(paymentDetails => {
          if(paymentDetails.messages.resultCode == 'Ok') {

            const cardDetailJson = {"getCustomerProfileRequest": {"merchantAuthentication": {"name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey}, "customerProfileId": cardDetails.customerProfileId, "unmaskExpirationDate" : "true", "includeIssuerInfo": "true"}}

            authorizeApi(authDetails.apiUrl, cardDetailJson, config)
              .then(cardData => {
                if(cardData.profile != undefined){
                  const checkObject = {"customerPaymentProfileId": "Check", "payment": {"creditCard": { "showCheck": true, "checkTitle": "Pay by Check", "checkText": "When Selecting payment by check your order will be on held until the check is received by us and cleared by your bank. 2% processing fee will apply.", "cardType": "Check" }}}
                  cardData.profile.paymentProfiles.push(checkObject);
                  this.setState({ loading: false, cardDetails: cardData.profile, newPayment: false, activeCard: paymentDetails.customerPaymentProfileId });
                } else {
                    this.setState({ loading: false });
                }
              })
          } else {
             const errorText = paymentDetails.messages.message.map(( code ) => code.text)
             this.setState({ loading: false, alertMessage: true, errorMessage: errorText[0] });
          }
        })

    } else {

        const submitRecord = data.toJS();

        const createProfileJson = { "createCustomerProfileRequest": { "merchantAuthentication": { "name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey }, "profile": { "merchantCustomerId": user.id, "email": user.email, "paymentProfiles": { "customerType": "individual", "billTo": { "firstName": user.firstName.substring(0, 50), "lastName": user.lastName.substring(0, 50), "address": submitRecord.address.substring(0, 60), "city": submitRecord.city.substring(0, 40), "state": submitRecord.state.substring(0, 40), "zip": submitRecord.zip.substring(0, 20), "country": submitRecord.country.substring(0, 60), "phoneNumber": submitRecord.phoneNumber.substring(0, 25) }, "payment": { "creditCard": { "cardNumber": submitRecord.cardNumber, "expirationDate": submitRecord.expirationDate, "cardCode": submitRecord.cvv } } } }, "validationMode": authDetails.validationMode } }

        authorizeApi(authDetails.apiUrl, createProfileJson, config)
        .then(customerProfile => {
          if(customerProfile.messages.resultCode == 'Ok') {

            const updatePaymentProfileJson = { "updateCustomerPaymentProfileRequest": { "merchantAuthentication": { "name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey }, "customerProfileId": customerProfile.customerProfileId, "paymentProfile": { "billTo": { "firstName": user.firstName.substring(0, 50), "lastName": user.lastName.substring(0, 50), "address": submitRecord.address.substring(0, 60), "city": submitRecord.city.substring(0, 40), "state": submitRecord.state.substring(0, 40), "zip": submitRecord.zip.substring(0, 20), "country": submitRecord.country.substring(0, 60),"phoneNumber": submitRecord.phoneNumber.substring(0, 25) }, "payment": { "creditCard": { "cardNumber": submitRecord.cardNumber, "expirationDate": submitRecord.expirationDate, "cardCode": submitRecord.cvv } }, "defaultPaymentProfile": true, "customerPaymentProfileId": customerProfile.customerPaymentProfileIdList[0] }, "validationMode": authDetails.validationMode } }

            authorizeApi(authDetails.apiUrl, updatePaymentProfileJson, config)
              .then(paymentProfile => {
                  if(paymentProfile.messages.resultCode == 'Ok') {

                    const cardDetailJson = {"getCustomerProfileRequest": {"merchantAuthentication": {"name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey}, "customerProfileId": customerProfile.customerProfileId, "unmaskExpirationDate" : "true", "includeIssuerInfo": "true"}}

                  authorizeApi(authDetails.apiUrl, cardDetailJson, config)
                    .then(cardData => {
                      if(cardData.profile != undefined){

                        cardData.profile.paymentProfiles.map((card, i) => {
                          if(card.defaultPaymentProfile == true) {
                            this.setState({ activeCard: card.customerPaymentProfileId, defaultCard: card.customerPaymentProfileId, activePayment: card, cardDetails: cardData.profile})
                          }
                        })
                        const custId = {"authorizeCustId" : parseInt(customerProfile.customerProfileId)}
                        updateCustId(custId)
                          .then(custDetails => {
                            this.setState({ loading: false, newPayment: false });
                          })
                      } else {
                          this.setState({ loading: false });
                      }
                    })
                  } else {
                     this.setState({ loading: false });
                     this.setState({ alertMessage: true, errorMessage: 'Please check the details you entered and try again' });
                   }
              })
          } else {
              const errorText = customerProfile.messages.message.map(( code ) => code.text)
              this.setState({ loading: false, alertMessage: true, errorMessage: errorText[0] });
          }
        })
    }
  }

  makeDefault(card) {

    const { authDetails } = this.props;
    const { cardDetails, alertMessage } = this.state;
    this.setState({ loading: true });
    const config = {
        transformRequest: [(data, headers) => {
          delete headers.common['Authorization'];
          delete headers.common['X-Auth-Token'];
          delete headers.common['timezone'];
            if (data !== undefined) {
               headers.post['Content-Type'] = 'application/json;charset=UTF-8';
               return JSON.stringify(data)
            } else {
              return data;
            }
          return data;
        }]
    };

    if(card.billTo != undefined) {
      const updatePaymentProfileJson = { "updateCustomerPaymentProfileRequest": { "merchantAuthentication": { "name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey }, "customerProfileId": cardDetails.customerProfileId, "paymentProfile": { "billTo": { "firstName": card.billTo.firstName.substring(0, 50), "lastName": card.billTo.lastName.substring(0, 50), "address": card.billTo.address.substring(0, 60), "city": card.billTo.city.substring(0, 40), "state": card.billTo.state.substring(0, 40), "zip": card.billTo.zip.substring(0, 20), "country": card.billTo.country.substring(0, 60), "phoneNumber": card.billTo.phoneNumber.substring(0, 25) }, "payment": { "creditCard": { "cardNumber": card.payment.creditCard.cardNumber, "expirationDate": card.payment.creditCard.expirationDate } }, "defaultPaymentProfile": true, "customerPaymentProfileId": card.customerPaymentProfileId }, "validationMode": authDetails.validationMode } }
     authorizeApi(authDetails.apiUrl, updatePaymentProfileJson, config)
        .then(paymentProfile => {
            if(paymentProfile.messages.resultCode == 'Ok') {
              this.setState({ loading: false, defaultCard: card.customerPaymentProfileId })
            }
        })
    } else {
      const updatePaymentProfileJson = { "updateCustomerPaymentProfileRequest": { "merchantAuthentication": { "name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey }, "customerProfileId": cardDetails.customerProfileId, "paymentProfile": { "payment": { "creditCard": { "cardNumber": card.payment.creditCard.cardNumber, "expirationDate": card.payment.creditCard.expirationDate } }, "defaultPaymentProfile": true, "customerPaymentProfileId": card.customerPaymentProfileId }, "validationMode": authDetails.validationMode } }
     authorizeApi(authDetails.apiUrl, updatePaymentProfileJson, config)
        .then(paymentProfile => {
            if(paymentProfile.messages.resultCode == 'Ok') {
              this.setState({ loading: false, defaultCard: card.customerPaymentProfileId })
            }
        })
    }
  }

 render(){

  const { schema, handleFormSubmit, initialValues, onCancel, authDetails, shipamttotal, orderTotal, saveamttotal, paymentLoading, updatePaymentLoading ,updateSubscribeOrder} = this.props;
  const { activeCard, activePayment, newPayment, defaultCard, cardDetails, alertMessage, netAmount, processingFee } = this.state;

  return (

   <div className={styles.ConfirmOrder}>
    {this.props.children((closeModal) => { closeModal(); this.setState({ showModal: !this.state.showModal, netAmount: this.props.orderTotal, activeCard: this.props.activeCard }) })}

      <Modal show={this.state.showModal} backdrop="static" onHide={this.closeModal.bind(this)} bsSize="large">
        <Modal.Header closeButton>
          {newPayment == false ?
            <div>
              <Modal.Title style={{fontWeight:"bold"}}>{updateSubscribeOrder ? "Update Subscribe" : "Subscribe"}</Modal.Title>
                <div style={{display:"flex",fontSize:15,justifyContent:"space-between",alignItems:"center"}}><div style={{color:"#ea6225",cursor:"pointer"}} onClick={this.closeModal.bind(this)}>Back</div><div style={{display:"flex",fontSize:18,justifyContent:"center",fontWeight:"bold"}}>Step 3 of 3 (Select Payment method)</div><div></div></div></div> : <div style={{display:"flex",fontSize:18,justifyContent:"center",fontWeight:"bold"}}>Add New Credit Card</div> }
        </Modal.Header>
        <Modal.Body>
            {this.state.loading || paymentLoading ? <Spinner paymentStyle={true} /> : null}
            { cardDetails.length != 0 && cardDetails.paymentProfiles != undefined ? newPayment == false ?
              <div>
                <Row className="action-button hidden-print">
                  <Col xs={12} className={styles.addButtonStyle}>
                    <Button bsStyle={'standard'} type="submit" className={styles.buttonStandard} onClick={() => this.setState({ newPayment: true })}>ADD NEW CARD</Button>
                  </Col>
                </Row>
                <ListGroup componentClass="ul">
                  {cardDetails.paymentProfiles.filter((item) => item.customerPaymentProfileId !== "Check" && item.customerPaymentProfileId !== "Finance").slice().reverse().map((card, i) => <CardItem key={i} card={card} active={activeCard === card.customerPaymentProfileId} activeCard ={activeCard} defaultCard ={defaultCard} onSubmit={(card) => this.makeDefault(card)} onClick={(card)=> this.onSelect(card)} />)}
                </ListGroup>
                <div>To remove any of the credit cards in your profile, please email us at: team@novadontics.com</div>
                <Row className="action-button hidden-print">
                  <Col xs={12} className={styles.cancelButtonStyle}>
                    <Button bsStyle={'standard'} type="submit" disabled={activeCard === 0 ? true : false} className={styles.buttonStandard} onClick={(data) => {updatePaymentLoading(); handleFormSubmit(data, activeCard, activePayment, newPayment, shipamttotal, processingFee, orderTotal, saveamttotal, cardDetails, netAmount)}}>{updateSubscribeOrder ? "Update" : "Pay"+ currencyFormatter.format(netAmount)}</Button>
                    <Button bsStyle="standard" type="button" className={styles.cancelBtn} onClick={this.closeModal.bind(this)}>Cancel</Button>
                  </Col>
                </Row>
              </div>
              : <Col xs={12} className={styles.editForm}>
                  <EditRecordForm
                      fields={schema.PaymentForm}
                      onSubmit={(data) => this.saveCard(data)}
                      initialValues = {initialValues}
                      catalogButton={true}
                      disableCancel={true}
                      onCancel={() => this.onCancel()}
                  />
                </Col>
              : <div>
                {newPayment == false ?
                  <div>
                    <Row className="action-button hidden-print">
                      <Col xs={12} className={styles.addButtonStyle}>
                        <Button bsStyle={'standard'} type="submit" className={styles.buttonStandard} onClick={() => this.setState({ newPayment: true })}>ADD NEW CARD</Button>
                      </Col>
                    </Row>
                    <div style={{display:"flex",fontSize:15,justifyContent:"center",fontWeight:"bold"}}>Please add a credit card to your account</div>

                    <Row className="action-button hidden-print">
                      <Col xs={12} className={styles.catalogButtonStyle}>
                        <Button bsStyle={'standard'} type="submit" disabled={activeCard === 0 ? true : false} className={styles.buttonStandard} onClick={(data) => {updatePaymentLoading(); handleFormSubmit(data, activeCard, activePayment, newPayment, shipamttotal, processingFee, orderTotal, saveamttotal, cardDetails, netAmount)}}>Pay {currencyFormatter.format(netAmount)}</Button>
                        <Button bsStyle="standard" type="button" className={styles.cancelBtn} onClick={this.closeModal.bind(this)}>Cancel</Button>
                      </Col>
                    </Row>
                  </div> :
                  <Col xs={12} className={styles.editForm}>
                    <EditRecordForm
                        fields={schema.PaymentForm}
                        onSubmit={(data) => this.saveCard(data)}
                        initialValues = {initialValues}
                        catalogButton={true}
                        disableCancel={true}
                        onCancel={() => this.onCancel()}
                    />
                  </Col>}
                </div> }
              <div className={styles.emptyDiv}></div>
              {this.state.alertMessage ?
                  <Modal show sm className="alertModal">
                      <Modal.Body>
                        <Row className={styles.rowStandard}>
                          <Icon className={`${styles.icon}`} glyph={'icon-fontello-attention'} />
                          <h3>Alert!</h3>
                          <Col>{this.state.errorMessage}</Col>
                        </Row>
                      </Modal.Body>
                      <Modal.Footer className={`${styles.footer}`} onClick={()=>{ this.setState({ alertMessage: false })}}>
                          Dismiss
                      </Modal.Footer>
                  </Modal> : null}
          </Modal.Body>
        </Modal>
          </div>)
  }

  closeModal() {
    this.setState({ showModal: false, newPayment: false });
    this.props.closeCart();
  }

  openModal() {
    this.setState({ showModal: true });
  }
};


export default PaymentForm;

