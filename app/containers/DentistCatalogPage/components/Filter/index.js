/**
*
* Filter
*
*/
import 'rc-slider/dist/rc-slider.min.css';
import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from '@sketchpixy/rubix';
import Slider from 'rc-slider';
import styles from './styles.css';

class Filter extends React.Component {
  render() {
    const { vendors, show, hide, price, selectedVendors, onChange, clearfilter, maxPrice } = this.props;
    const vendorsMap = vendors.map(_=>_.id);
     return (
        <Modal show={show} onHide={hide} backdrop="static" dialogClassName="categorymodel" >
        <Modal.Header closeButton>
          <Modal.Title>Catalog Filter</Modal.Title>
          <div className={styles.modelclear} onClick={clearfilter}>
            Clear Filter
          </div>
        </Modal.Header>
        <Modal.Body>
        <div className={styles.modelpricecontainer}> 
          <div className={styles.modelpricesubcontainer}>
            <div className={styles.modelprice}><span>PRICE RANGE</span><span className={styles.modelpriceslider}>${price}</span></div>
            <Slider dotStyle={{width:50,height:50}} railStyle={{backgroundColor:"#ccc",height:3}} trackStyle={{backgroundColor:"#ea6225",height:3}} handleStyle={{width:26,height:26,marginTop:-11,marginLeft:5,border:"solid 2px #ccc",boxShadow:"0 0 1px #ccc"}}  max={parseInt(maxPrice)} value={price} onChange={(e)=>onChange(e, 'price')} />
          </div>
        </div> 
        <div className={styles.modelpricesubcontainer}>
          <div className={styles.modlesubtitle}>VENDORS</div>
          <div className={styles.modelvendorcontainer}>
            <div className={styles.modelvendorlist}>
              <div>
                Select ALL
              </div>
              <div>
                <label className={styles.switch}>
                  <input name="selectall" type="checkbox" checked={vendorsMap.length === selectedVendors.length} className={styles.checkswitch} onClick={()=>{onChange(vendorsMap.length != selectedVendors.length ? vendorsMap : [] , 'vendors')}} />
                  <span className={`${styles.slider} ${styles.round}`}></span>
                </label>
              </div>
            </div>            
          </div>
          {vendors != undefined? vendors.map((vendor) =>
          <div key={"checkboxdiv_"+vendor.id} className={styles.modelvendorcontainer}>
            <div className={styles.modelvendorlist}>
              <div id={"div_"+vendor.id}  >
                {vendor.name}
              </div>
              <div>
                <label className={styles.switch}>
                  <input name={"check-"+vendor.id} type="checkbox" checked={(selectedVendors.indexOf(vendor.id) > -1)}  onClick={()=>onChange(selectedVendors.indexOf(vendor.id) > -1 ?  selectedVendors.filter(_=>_ !== vendor.id) : selectedVendors.concat(vendor.id), 'vendors')} className={styles.checkswitch}/>                  
                  <span className={`${styles.slider} ${styles.round}`}></span>
                </label>
              </div>
            </div>
          </div>
          ):null}
        </div> 
        </Modal.Body>
      </Modal>
    );
  }

}

Filter.propTypes = {
  vendors: PropTypes.array,
  show: PropTypes.bool,
  hide: PropTypes.func,
  onChange: PropTypes.func,
  price: PropTypes.number,
  selectedVendors: PropTypes.array,
};

export default Filter;
