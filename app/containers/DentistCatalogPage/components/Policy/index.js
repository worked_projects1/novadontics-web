/**
*
* Policy
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from '@sketchpixy/rubix';


class Policy extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { url, show, hide } = this.props;
     return (
        <Modal show={show}  onHide={hide}>
            <Modal.Body>
              <iframe src={url} width="100%" height="100%" style={{minHeight:"500px",border:"none"}}/>
            </Modal.Body>

        </Modal>
    );
  }

}

Policy.propTypes = {
  url: PropTypes.string,
  show: PropTypes.bool,
  hide: PropTypes.func,
};

export default Policy;
