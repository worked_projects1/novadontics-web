import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';
const isSelected = (id, list) =>
  list.some((item)=> item === id);

const Item = ({ name, id,productCount,onChange, ident, selected }) =>
  <label
    htmlFor={`check-${id}`}
    className={styles.itemLabel}
    style={{marginLeft: ident ? 12 : 0}}
    onClick={onChange}
  >
  <div id={`check-${id}`} style={ident ? {fontWeight:'100'} : null} className={`${styles.boldText} ${selected.includes(id) ? styles.boldTextSelect : null}`} name={`check-${id}`} >{ name } <span style={{color:"rgb(169, 169, 173)"}}>({productCount})</span></div>
  </label>;

Item.propTypes = {
  name: PropTypes.string,
  id: PropTypes.number,
  onChange: PropTypes.func,
  selected: PropTypes.array,
  ident: PropTypes.bool,
};

const CategoryList = ({ categories = [], onChange, selected }) =>
  <div>
    {categories.map((category) =>
      <div key={category.id}>
        <Item name={category.name} id={category.id} productCount={category.productCount}  onChange={() => onChange(category.id)} selected={selected} />
        {category.children && category.children.map((child) =>
          <Item
            name={child.name}
            key={child.id}
            id={child.id}
            onChange={() => onChange(child.id)}
            selected={selected}
            productCount={child.productCount} 
            ident
          />
        )}
      </div>
    )}
  </div>;


CategoryList.propTypes = {
  categories: PropTypes.array,
  selected: PropTypes.array,
  onChange: PropTypes.func,
};

export default CategoryList;
