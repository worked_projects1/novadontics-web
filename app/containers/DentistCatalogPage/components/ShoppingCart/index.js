import React from 'react';
import PropTypes from 'prop-types';
import {Button, Modal} from '@sketchpixy/rubix';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import styles from './styles.css';
import {Col, Row,Icon} from "@sketchpixy/rubix/lib/index";
import {groupBy} from "lodash";
import ConfirmOrder from "./ConfirmOrder";
import {addFavourite, removeFavourite, addCart, addSaveLater, moveSaveLater} from 'blocks/products/remotes.js';
import ConfirmButton from 'components/ConfirmButton';
import VendorNotes from '../VendorNotes/index.js';
import { reset } from 'redux-form';


export const OrderTable = ({cart, notesArr, vendors = [], user, removeFromCart, handleVendorNotes, reloadCart, onOvernight, handleEdit, fields, isEditEnabled, addToCart, openProductDetailView, viewOnly, salesTax, showOrderDetails, orderDetails, showQuantity, onFavourite, toggleCount, onClose, toggleFinance, saveForLater, reloadSaveLater}) => {
  const grandTotal = cart.reduce((acc, p) => p.listPrice != undefined ? acc + (p.amount * p.listPrice) : acc + (p.amount * p.price), 0);
  const productGroups = groupBy(cart, 'vendorId');
  var taxtotal =0;
  var shipamttotal =0;  
  var cardtotal =0;  
  var saveamttotal=0;
  var orderHistoryOrderTotal=0;
  var orderHistoryShippingCost=0;
  var orderHistoryShippingCharge=0;
  var orderHistoryCreditCard=0;
  var orderHistoryTotalAmountSaved=0;
  var orderHistoryTax=0;
  var orderHistoryGrandTotal=0;
  for(var y in productGroups)
  {
    var taxamt =0;
    var amt =0;
    
    var selectedvendor={};
    var check=false;
    for(var x in productGroups[y])
    {
      check=true;    
      if(productGroups[y][x].listPrice != undefined) {
         amt += productGroups[y][x].listPrice * productGroups[y][x].amount;
         saveamttotal += (productGroups[y][x].price * productGroups[y][x].amount) - (productGroups[y][x].listPrice * productGroups[y][x].amount);
      } else { 
          amt += productGroups[y][x].price * productGroups[y][x].amount;
      }    
    }
    if(check)
    {
      for(var z in vendors)
      {
        if(vendors[z].id == productGroups[y][0].vendorId)
        {
          selectedvendor= vendors[z];
          break;
        }
      }       
      if(selectedvendor.shippingChargeLimit > amt && !productGroups[y][x].overnight)
      {
        shipamttotal += user.prime == true ? 0 : selectedvendor.shippingCharge;
      }
      if(selectedvendor.tax !=undefined && selectedvendor.tax !="")
      {
        taxtotal += ((amt/100)* selectedvendor.tax);
      }
      if(selectedvendor.creditCardFee !=undefined && selectedvendor.creditCardFee !="")
      {
        cardtotal += ((amt/100)* selectedvendor.creditCardFee);
      }
    }
  }
  //const taxAmount = currencyFormatter.format(taxtotal);
  const taxAmount = currencyFormatter.format(salesTax);
  const ShippTotal = currencyFormatter.format(shipamttotal);
  const savingsTotal = currencyFormatter.format(saveamttotal);
  const CreditCard = currencyFormatter.format(Math.round(cardtotal * 100.0) / 100.0);
  const overnightCount = Object.keys(productGroups).map(key => productGroups[key][0].overnight).filter(Boolean).length;
  const shippingCost = currencyFormatter.format(overnightCount * 52);
  const orderTotal =  grandTotal  + (overnightCount * 52) + shipamttotal + parseFloat(salesTax) + cardtotal;

    if(showOrderDetails == true) {
          const orderHistoryItemGroups = groupBy(orderDetails.items, 'vendorId');
          orderHistoryOrderTotal =  orderDetails.items.reduce((acc, p) => p.listPrice != undefined ? acc + (p.amount * p.listPrice) : acc + (p.amount * p.price), 0);
          const orderHistoryOvernightCount = Object.keys(orderHistoryItemGroups).map(key => orderHistoryItemGroups[key][0].overnight).filter(Boolean).length;
          orderHistoryShippingCharge = user.prime == true ? currencyFormatter.format(0) : currencyFormatter.format(orderDetails.shippingCharge);
          orderHistoryShippingCost = currencyFormatter.format(orderHistoryOvernightCount * 52);
          orderHistoryTax = currencyFormatter.format(orderDetails.tax)
          orderHistoryCreditCard = currencyFormatter.format(orderDetails.creditCardFee)
          orderHistoryTotalAmountSaved = currencyFormatter.format(orderDetails.totalAmountSaved)
          orderHistoryGrandTotal = orderDetails.grandTotal
    }


  return (
    <div className={styles.OrderTable}>
      {
        Object.keys(productGroups).map((vendorId, vendorIndex) =>
          <Col key={vendorIndex}>
          <VendorOrder
            products={productGroups[vendorId]}
            vendor={vendors.find(_ => String(_.id) === String(vendorId))}
            notesValue={notesArr != undefined && notesArr.length > 0 ? notesArr.find(_ => String(_.id) === String(vendorId)) : undefined}
            onRemove={removeFromCart}
            handleVendorNotes={handleVendorNotes}
            onReload={reloadCart}
            toggleFinance={toggleFinance}
            onFavourite = {onFavourite}
            onOvernight={onOvernight}
	          handleEdit={handleEdit}
	          toggleCount={toggleCount}
	          reloadSaveLater={reloadSaveLater}
	          onClose={onClose}
	          notesArr={notesArr}
            fields={fields}
            showQuantity={showQuantity}
            isEditEnabled={isEditEnabled}
            addToCart={(item,amt)=>addToCart(item,amt)}
            openProductDetailView={(id)=> openProductDetailView(id)}
          /></Col>)
      }
      {
        !showOrderDetails ?
          <div>
            <h5><span>Item(s) Subtotal:</span><b>{` ${currencyFormatter.format(grandTotal)}`}</b></h5>
            <h5><span>Overnight Shipping Cost:</span>{shippingCost != "$0.00"?` +`:``}<b>{` ${shippingCost}`}</b></h5>
            <h5><span>Ground Shipping Cost:</span>{ShippTotal != "$0.00"?` +`:``}<b>{` ${ShippTotal}`}</b></h5>
            <h5><span>Sales Tax:</span>{taxAmount != "$0.00"?` +`:``}<b>{` ${taxAmount}`}</b></h5>
            <h5><span>Processing Fee:</span>{CreditCard != "$0.00"?` +`:``}<b>{` ${CreditCard}`}</b></h5>
            <h5><span>Total Amount Saved:</span><b>{` ${savingsTotal}`}</b></h5>
            <h5><span>Grand Total:</span><b>{` ${currencyFormatter.format(Math.round(orderTotal * 100.0) / 100.0)}`}</b></h5>
          </div> :
          <div>
            <h5><span>Item(s) Subtotal:</span><b>{` ${currencyFormatter.format(orderHistoryOrderTotal)}`}</b></h5>
            <h5><span>Overnight Shipping Cost:</span>{orderHistoryShippingCost != "$0.00"?` +`:``}<b>{` ${orderHistoryShippingCost}`}</b></h5>
            <h5><span>Ground Shipping Cost:</span>{orderHistoryShippingCharge != "$0.00"?` +`:``}<b>{` ${orderHistoryShippingCharge}`}</b></h5>
            <h5><span>Sales Tax:</span>{orderHistoryTax != "$0.00"?` +`:``}<b>{` ${orderHistoryTax}`}</b></h5>
            <h5><span>Processing Fee:</span>{orderHistoryCreditCard != "$0.00"?` +`:``}<b>{` ${orderHistoryCreditCard}`}</b></h5>
            <h5><span>Total Amount Saved:</span><b>{` ${orderHistoryTotalAmountSaved}`}</b></h5>
            <h5><span>Grand Total:</span><b>{` ${currencyFormatter.format(orderHistoryGrandTotal)}`}</b></h5>
          </div>
      }
      {!showOrderDetails ?
        <div>
          <h4 style={{color:"#ea6225", fontWeight:"bold"}}>Saved for Later by You</h4>
          <SaveForLater
            products={saveForLater}
            reloadSaveLater={reloadSaveLater}
            onRemove={removeFromCart}
            handleVendorNotes={handleVendorNotes}
            onReload={reloadCart}
            toggleFinance={toggleFinance}
            onFavourite = {onFavourite}
            onOvernight={onOvernight}
	          handleEdit={handleEdit}
	          toggleCount={toggleCount}
	          onClose={onClose}
            showQuantity={showQuantity}
            isEditEnabled={isEditEnabled}
            addToCart={(item,amt)=>addToCart(item,amt)}
            openProductDetailView={(id)=> openProductDetailView(id)} />
        </div> : null}
    </div>)
};
class SaveForLater extends React.PureComponent {
  render(){
    const { user, products = [], vendor, onRemove, onReload, onOvernight, handleEdit, fields, isEditEnabled, addToCart, openProductDetailView, showQuantity, onFavourite, toggleCount, onClose, toggleFinance, reloadSaveLater } = this.props;

    return (
       <div style={{ display: 'flex', marginTop: '20px', width: 'auto', overflowX: 'auto', paddingBottom: '25px' }}>
         {products.length > 0 ? products.map((product, index) =>
           <div key={index}>
             <SaveForLaterList product={product} onFavourite={onFavourite} showQuantity={showQuantity} onRemove={onRemove} onReload={onReload} reloadSaveLater={reloadSaveLater} toggleFinance={toggleFinance} addToCart={(item,amt)=>addToCart(item,amt)} openProductDetailView={(id)=> openProductDetailView(id)} toggleCount={toggleCount} onClose={onClose} />
           </div>
         ) : <div><p><em>No items in saved for later.</em></p></div>}
       </div>
    )
  }
}
class SaveForLaterList extends React.PureComponent {
  constructor(props){
    super(props);
    const { product = {} } = props;
    this.state = {amount: product && product.amount ? product.amount : 1};
  }
  render(){
    const { product = {}, onRemove, onReload, addToCart, openProductDetailView, user, showQuantity, toggleCount, onClose, toggleFinance, reloadSaveLater } = this.props;
    return (
      <div style={{ display: 'flex', width: '300px', paddingLeft: '15px' }}>
        <div className={product.imageUrl?styles.productWithoutImageView:styles.productImageView} onClick={()=>openProductDetailView(product.id)}>
          <div
            style={{
              height: 70,
              width: 70,
              backgroundImage: `url(${product.imageUrl && product.imageUrl.split(",")[0] || defaultImage})`,
              backgroundSize: 'contain',
            }}
          />
        </div>
        <div style={{paddingLeft: '20px',fontWeight: 'normal'}}>
          <div onClick={()=>openProductDetailView(product.id)}>
            <div className={styles.productButton}>
              {product.name}
            </div>
            <div className={styles.productButton}>
             {product.serialNumber}
            </div>
            <div className={styles.productButton}>
              {showQuantity == true ?
                <p>
                  {product.unit != '' && product.unit != null ?
                    product.unit.toLowerCase() == 'each' && product.packageQuantity ? `(${product.packageQuantity} pcs.)` :
                    product.unit.toLowerCase() != 'each' && product.packageQuantity ? `( ${product.packageQuantity} pcs. / ${product.unit} )` :
                    product.unit.toLowerCase() == 'each' && !product.packageQuantity ? `(1 pcs.)` :
                    product.unit.toLowerCase() != 'each' && !product.packageQuantity ? `(1 pcs. / ${product.unit})` : null :
                    product.packageQuantity ? `(${product.packageQuantity} pcs.)` : null
                  }
                </p> : null}
            </div>
            <div style={{ display:"flex",color: "#000"}}>
              {product.listPrice != undefined ?
                <div>
                  <p>
                    {product.unit != '' && product.unit != null ?
                      product.unit.toLowerCase() == 'each' && product.quantity ? `(${product.quantity} pcs.)` :
                      product.unit.toLowerCase() != 'each' && product.quantity ? `( ${product.quantity} pcs. / ${product.unit} )` :
                      product.unit.toLowerCase() == 'each' && !product.quantity ? `(1 pcs.)` :
                      product.unit.toLowerCase() != 'each' && !product.quantity ? `(1 pcs. / ${product.unit})` : null :
                      product.quantity ? `(${product.quantity} pcs.)` : null
                    }
                  </p>
                  <p><b>{currencyFormatter.format(product.listPrice)}</b>
                    <span>(<strike>{currencyFormatter.format(product.price)}</strike>)
                      <div>
                        <b className="save">Save {currencyFormatter.format(product.price - product.listPrice)}</b>
                      </div>
                    </span>
                  </p>
                </div> :
              <p><b>${product.price}</b> </p>}
            </div>
          </div>
          <div className={styles.cartmovebtn} onClick={()=>{this.moveToCart(product.id)}}> Move to Cart </div>
        </div>
      </div>
    )
  }

  moveToCart(id) {
    moveSaveLater(id);
    const cartReload = setInterval(() => {
      this.props.onReload();
      this.props.reloadSaveLater();
      clearReloadTimer();
    }, 300);
    const clearReloadTimer = () => clearInterval(cartReload);
  }
}
const parseChange = (callback) => e => {
  const { name, checked } = e.target;
  const vendorId = name.split('-').slice(-1)[0];
  callback(vendorId, checked)
};
const defaultImage = 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg';
class VendorOrder extends React.PureComponent {
  state = {amount: undefined};
  render(){
  const { user, products = [], vendor, onRemove, onReload, onOvernight, handleEdit, fields, isEditEnabled, addToCart, openProductDetailView, showQuantity, onFavourite, toggleCount, onClose, notesArr, handleVendorNotes, notesValue, toggleFinance, reloadSaveLater } = this.props;
  const initialValue = notesValue != undefined ? notesValue.note : '';
  const amount = this.state.amount !=undefined ?this.state.amount: products[0].amount !=undefined?products[0].amount:1;
  const productPrice = currencyFormatter.format(products[0].price);
  const vendorNotesColumn = [{ value: 'note', name: 'note', label: 'Note', type: 'textarea', editRecord: true }];

  return (

  <div>
    <Row>
      <Col md={6}>
        {vendor && onRemove ?
          <div style={{ display: 'flex' }}>
            <h4><b>{vendor.name}</b></h4>
            <VendorNotes
                id={vendor.id}
                vendor={vendor}
                notesArr={notesArr}
                fields={vendorNotesColumn}
                onSubmit={this.handleNotes.bind(this)}
                form={`vendorNotes.${vendor.id}`}
                vendorId={vendor.id}
                onRef={vendorNotes => (this[`vendorNotes.${vendor.id}`] = vendorNotes)}
                config={{ title: initialValue != '' ? "Edit Note to Vendor" : "Add Note to Vendor", btnType: 'link', btnName: initialValue != '' ? 'Update' : 'Add', closeModal: true }}
            />
          </div>: null}
      </Col>
      <Col md={6}>
        <div className={styles.overnightContainer}>
          <h5 className={styles.overnightLabel}>
            <b>Overnight Shipping</b>
          </h5>
          <label className={styles.switch}>
          <input
            type="checkbox"
            name={`overnight-${vendor && vendor.id}`}
            id={`overnight-${vendor && vendor.id}`}
            onChange={parseChange(onOvernight)}
            checked={products[0].overnight || false}
            disabled={!onOvernight}
            style={{
              padding: 0,
              margin: '0px 4px 0px 0px',
              height: 16,
              width: 16,
            }}
            className={styles.checkswitch}
          />
          <span className={`${styles.slider} ${styles.round}`}></span>
          </label>
        </div>
      </Col>
    </Row>
     {products.map((product, index) =>
     <Col key={index}><ProductList  product = {product} onFavourite = {onFavourite}  showQuantity = {showQuantity} onRemove={onRemove} onReload={onReload} reloadSaveLater={reloadSaveLater} toggleFinance={toggleFinance} addToCart={(item,amt)=>addToCart(item,amt)} openProductDetailView={(id)=> openProductDetailView(id)} toggleCount={toggleCount} onClose={onClose} /></Col>
    )}
  </div>)
  }

  handleNotes(data) {
      const { vendor } = this.props;
      var submitRecord = data.toJS();
      this.props.handleVendorNotes(submitRecord, vendor)
  }

  close() {
      this.setState({ showModal: false });
  }
}
class ProductList extends React.PureComponent {
  constructor(props){
    super(props);
    const { product = {} } = props;
    this.state = {amount: product && product.amount ? product.amount : 1};
  }
  render(){
    const { product = {}, onRemove, onReload,  addToCart, openProductDetailView, user, showQuantity, toggleCount, onClose, toggleFinance, reloadSaveLater } = this.props;
    const { amount } = this.state;
    const productPrice = currencyFormatter.format(product.price);
    return (
      <Row className={styles.productcellcontainer}>
      <Col sm={2} className={styles.imagecontainer} onClick={()=>openProductDetailView(product.id)}>
        
        <div className={product.imageUrl?styles.productWithoutImageView:styles.productImageView} >
          <div
            style={{
              height: 70,
              width: 70,
              backgroundImage: `url(${product.imageUrl && product.imageUrl.split(",")[0] || defaultImage})`,
              backgroundSize: 'contain',
            }}
          >
          </div>
        </div>
       
      </Col>
      <Col  sm={4} style={{paddingLeft:0,fontWeight: 'normal'}} onClick={()=>openProductDetailView(product.id)}>
        <div className={styles.productButton}>
          {product.name}
        </div>
        <div className={styles.productButton} >
         {product.serialNumber}
        </div>
        <div className={styles.productButton} >
          {showQuantity == true ?
            <p>
              {product.unit != '' && product.unit != null ?
                product.unit.toLowerCase() == 'each' && product.packageQuantity ? `(${product.packageQuantity} pcs.)` :
                product.unit.toLowerCase() != 'each' && product.packageQuantity ? `( ${product.packageQuantity} pcs. / ${product.unit} )` :
                product.unit.toLowerCase() == 'each' && !product.packageQuantity ? `(1 pcs.)` :
                product.unit.toLowerCase() != 'each' && !product.packageQuantity ? `(1 pcs. / ${product.unit})` : null :
                product.packageQuantity ? `(${product.packageQuantity} pcs.)` : null
              }
            </p> : null}
        </div>
        <div style={{ display:"flex",color: "#000"}}>
            {product.listPrice != undefined ?
              <div>
                <p>
                  {product.unit != '' && product.unit != null ?
                    product.unit.toLowerCase() == 'each' && product.quantity ? `(${product.quantity} pcs.)` :
                    product.unit.toLowerCase() != 'each' && product.quantity ? `( ${product.quantity} pcs. / ${product.unit} )` :
                    product.unit.toLowerCase() == 'each' && !product.quantity ? `(1 pcs.)` :
                    product.unit.toLowerCase() != 'each' && !product.quantity ? `(1 pcs. / ${product.unit})` : null :
                    product.quantity ? `(${product.quantity} pcs.)` : null
                  }
                </p>
                <p><b>{currencyFormatter.format(product.listPrice)}</b>
                  <span>(<strike>{currencyFormatter.format(product.price)}</strike>)
                    <div>
                      <b className="save">Save {currencyFormatter.format(product.price - product.listPrice)}</b>
                    </div>
                  </span>
                </p>
              </div> :
            <p><b>${product.price}</b> </p>}
        </div>
      </Col>
      <Col  sm={6}>
        <div className={styles.productoption}>
        {onRemove &&
          <div className={styles.productoption}>
            <div>
              <div className={styles.cartaddsub}>
                <div onClick={this.handleChange.bind(this, false)}>
                  <Icon bundle="fontello" glyph="minus"className={styles.cartsub}/>
                </div>
                <input
                  className={styles.cartinput}
                  type="number"
                  step="1"
                  min="1"
                  value={product.amount}
                  onChange={this.onChange}
                />
                <div onClick={this.handleChange.bind(this, true)}>
                  <Icon bundle="fontello" glyph="plus" className={styles.cartadd} />
                </div>
              </div>
              {product.financeEligible ? <div className={styles.financeBtn} onClick={toggleFinance}>Finance</div> : null}
            </div>
          </div>}
          {
            onRemove &&
            <td>
              <Button bsStyle="link" bsSize="xs" className={styles.removeButton} onClick={()=>{this.handleSubscribe(product.id, product.amount)}}>Subscribe</Button>
              <Button bsStyle="link" bsSize="xs" className={styles.removeButton} onClick={()=>{this.handleSaveLater(product.id)}} >Save for Later</Button>
              <ConfirmButton onConfirm={() => onRemove(product.id)}>{(click) => <Button bsStyle="link" bsSize="xs" className={styles.removeButton} onClick={() => click()}>Remove</Button>}</ConfirmButton>
            </td>
          }
          <div>
            <div>{product.amount} items for</div>
            <div>{product.listPrice != undefined ? currencyFormatter.format(product.amount*product.listPrice) : currencyFormatter.format(product.amount*product.price)}</div>
          </div>
        </div>
      </Col>
 </Row>
    )
}

  favourite(id , favourite)
  {
    const { onFavourite } = this.props;
    favourite ? removeFavourite(id) : addFavourite(id);
    onFavourite(id , favourite);
  }

  async handleChange(add){
    const { product={}, addToCart } = this.props;
    const { amount } = this.state;
    const val = add ? product.amount+1 : product.amount-1;
    this.setState({ amount : val > 0 ? val : 1 });
    if(val > 0) {
      await addToCart(product, add ? 1 : -1);
      const cartReload = setInterval(() => {
        this.props.onReload();
        clearReloadTimer();
      }, 300);
      const clearReloadTimer = () => clearInterval(cartReload);
    }
  }

  handleSaveLater(id){
    addSaveLater(id);
    const cartReload = setInterval(() => {
      this.props.onReload();
      this.props.reloadSaveLater();
      clearReloadTimer();
    }, 300);
    const clearReloadTimer = () => clearInterval(cartReload);
  }

  handleSubscribe(id, amount){
    this.props.onClose();
    this.props.toggleCount(id, amount);
  }
}
class ShoppingCart extends React.PureComponent {
  state = {showConfirm: false,page:1, activeCard: 0, defaultCard: 0, activePayment: [], closeModal: false};

  toggleConfirm = () => {
    this.setState({ showConfirm: !this.state.showConfirm});
  };
  nextstep()
  {
    this.setState({page:2});
    this.props.getSalesTax(-1);
    if(this.props.cardDetails.length != 0 && this.props.cardDetails.paymentProfiles != undefined){
      this.props.cardDetails.paymentProfiles.map((card, i) => { if(card.defaultPaymentProfile == true) {this.setState({ activeCard: card.customerPaymentProfileId, defaultCard: card.customerPaymentProfileId, activePayment: card})}})
    }
  }
  closeCart = () => {
    this.setState({ closeModal: !this.state.closeModal});
  }
  render(){
  const { cart = [], user, onClose, addresses, removeFromCart, handleVendorNotes, reloadCart, placeOrder, shown, vendors = [], onOvernight,addToCart, openProductDetailView, viewOnly, salesTax, getSalesTax, cardDetails, notesArr, authDetails, onFavourite, toggleCount, toggleFinance, saveForLater, reloadSaveLater } = this.props;
  const { activeCard, activePayment, defaultCard, closeModal } = this.state;
  var itemcount=0;
  for(var x in cart)
  {
    itemcount += cart[x].amount;
  }
  return (
    <div>
      <Modal show={shown} backdrop="static" onHide={() => {this.setState({page:1}); onClose()}} bsSize="large" className={closeModal ? styles.hideModal : null}>
        <Modal.Header closeButton >
          <Modal.Title style={{fontWeight:"bold"}}>Shopping Cart</Modal.Title>
          <div>You have {itemcount} items in your shopping cart</div>
          {cart && cart.length ?
          this.state.page == 1?<div style={{display:"flex",fontSize:18,justifyContent:"center",fontWeight:"bold"}}>Step 1 of 3 (Order Preview)</div>:
          <div style={{display:"flex",fontSize:15,justifyContent:"space-between",alignItems:"center"}}><div style={{color:"#ea6225",cursor:"pointer"}} onClick={()=>{this.setState({page:1})}}>Back</div><div style={{display:"flex",fontSize:18,justifyContent:"center",fontWeight:"bold"}}>Step 2 of 3 (Select Shipping Address)</div><div></div></div>
          :null}
        </Modal.Header>
        <Modal.Body>
          {this.state.page == 1?
          <div>
          {cart && cart.length ?
            <OrderTable
              user={user}
              cart={cart}
              saveForLater={saveForLater}
              reloadSaveLater={reloadSaveLater}
              notesArr={notesArr}
              onFavourite = {onFavourite}
              removeFromCart={removeFromCart}
              handleVendorNotes={handleVendorNotes}
              reloadCart={reloadCart}
              toggleFinance={toggleFinance}
              onOvernight={onOvernight}
              vendors={vendors}
              addToCart={(item,amt)=>{ addCart({"productId":item.id,"quantity":amt}).then(() => getSalesTax(-1)); addToCart(item,amt)}}
              openProductDetailView={(id)=>openProductDetailView(id)}
              viewOnly={viewOnly}
              salesTax={salesTax}
              toggleCount={toggleCount}
              onClose={onClose}
            />
            :
            <div>
              <p><em>No items in cart.</em></p>
              <div style={{ marginTop: "5em" }}>
                <h4 style={{color:"#ea6225", fontWeight:"bold"}}>Saved for Later by You</h4>
                <SaveForLater
                  products={saveForLater}
                  reloadSaveLater={reloadSaveLater}
                  onRemove={removeFromCart}
                  handleVendorNotes={handleVendorNotes}
                  onReload={reloadCart}
                  toggleFinance={toggleFinance}
                  onFavourite = {onFavourite}
                  onOvernight={onOvernight}
                  toggleCount={toggleCount}
                  onClose={onClose}
                  addToCart={(item,amt)=>{ addCart({"productId":item.id,"quantity":amt}).then(() => getSalesTax(-1)); addToCart(item,amt)}}
                  openProductDetailView={(id)=> openProductDetailView(id)}
                />
              </div>
            </div>
          }
          {cart && cart.length ?<div onClick={()=>this.nextstep()} className={styles.nextbtn}>Next Step</div>:null}
          </div>:
          <div>
            <ConfirmOrder
              user={user}
              shown={this.state.showConfirm}
              onClose={this.toggleConfirm}
              closeCart={this.closeCart}
              onConfirm={placeOrder}
              cart={cart}
              removeFromCart={removeFromCart}
              onOvernight={onOvernight}
              vendors={vendors}
              salesTax={salesTax}
              getSalesTax={(id)=> getSalesTax(id)}
              cardDetails={cardDetails}
              authDetails={authDetails}
              activeCard={activeCard}
              defaultCard={defaultCard}
              activePayment={activePayment}
              itemcount={itemcount}
            />
          </div>
        }
        </Modal.Body>
      </Modal>
    </div>)
  }
}





ShoppingCart.propTypes = {
  cart: PropTypes.array,
  notesArr: PropTypes.array,
  onClose: PropTypes.func.isRequired,
  removeFromCart: PropTypes.func.isRequired,
  handleVendorNotes: PropTypes.func.isRequired,
  placeOrder: PropTypes.func.isRequired,
  onOvernight: PropTypes.func.isRequired,
  vendors: PropTypes.array,
  shown: PropTypes.bool,
  openProductDetailView: PropTypes.func
};

export default ShoppingCart;
