import React from "react";
import classNames from 'classnames';
import {mapProps, compose, withState} from "recompose";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {createSelector} from "reselect";
import selectors from "blocks/records/selectors";
import { Map, List } from 'immutable';
import { Col, Label, Modal, ListGroup, Row, Icon} from "@sketchpixy/rubix/lib/index";
import {get} from 'utils/misc';
import styles from './styles.css';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import {groupBy} from "lodash";
import PaymentForm from "./PaymentForm";
import schema from 'routes/schema';
import axios from 'axios';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';
import Spinner from 'components/Spinner';
import { authorizeApi, updateCustId } from 'blocks/products/remotes.js';


const AddressItem = ({address = {}, disabled, active, onClick}) =>
  <li className={classNames('list-group-item', disabled && 'disabled', active && 'active', active && styles.noHoverColor)} onClick={()=>onClick(address.id)}>
    <Row>
      <Col md={5}>
        <p>{get(address, 'name')}</p>
        <p>{get(address, 'phone')}</p>
        {address.isPractice && <h4><Label>Practice Address</Label></h4>}
      </Col>
      <Col md={7} className={styles.address}>
        <p>{get(address, 'address')}</p>
        <p>{`${get(address, 'city')}, ${get(address, 'state')} ${get(address, 'zip')}`}</p>
        <p>{`${get(address, 'country')}`}</p>
      </Col>
    </Row>
  </li>;

class ConfirmOrder extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { errorMessage: '',page: 2, alertMessage: false, loading: false, activeCard: 0, defaultCard: 0, activePayment: [], showModal: false,};
    this.nextStep = this.nextStep.bind(this);
  }

  handleFormSubmit(data, activeCard, activePayment, newPayment, shipamttotal, cardtotal, orderTotal, saveamttotal, cardDetails, netAmount) {

      const { onConfirm, addresses, setSubmitting, selected, salesTax, getSalesTax, user, authDetails, cart } = this.props;
      const paymentAddress = addresses.find(form => form.id === selected);

      if(activeCard == "Check") {
        setSubmitting(true);
        onConfirm({address: selected, taxtotal: parseFloat(salesTax), shipamttotal: shipamttotal, cardtotal: Math.round(cardtotal * 100.0) / 100.0, grandTotal: Math.round(netAmount * 100.0) / 100.0, totalAmountSaved: saveamttotal, paymentMethod: "check" });
      } else if(activeCard == "Finance") {
        setSubmitting(true);
        onConfirm({address: selected, taxtotal: parseFloat(salesTax), shipamttotal: shipamttotal, cardtotal: Math.round(cardtotal * 100.0) / 100.0, grandTotal: Math.round(netAmount * 100.0) / 100.0, totalAmountSaved: saveamttotal, paymentMethod: "finance" });
      } else {
        this.setState({ loading: true });
        const config = {
            transformRequest: [(data, headers) => {
              delete headers.common['Authorization'];
              delete headers.common['X-Auth-Token'];
              delete headers.common['timezone'];
                if (data !== undefined) {
                   headers.post['Content-Type'] = 'application/json;charset=UTF-8';
                   return JSON.stringify(data)
                } else {
                  return data;
                }
              return data;
            }]
        };

        const chargeProfileJson = { "createTransactionRequest": { "merchantAuthentication": { "name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey }, "transactionRequest": { "transactionType": "authCaptureTransaction", "amount": Math.round(netAmount * 100.0) / 100.0, "profile": { "customerProfileId": cardDetails.customerProfileId, "paymentProfile": { "paymentProfileId": activeCard } }, "order": { "description": "FromCatalogOrders" }, "shipTo": { "firstName": user.firstName.substring(0, 50), "lastName": user.lastName.substring(0, 50), "company": paymentAddress.name.substring(0, 50), "address": paymentAddress.address.substring(0, 60), "city": paymentAddress.city.substring(0, 40), "state": paymentAddress.state.substring(0, 40), "zip": paymentAddress.zip.substring(0, 20), "country": paymentAddress.country.substring(0, 60) }, } } }

        authorizeApi(authDetails.apiUrl, chargeProfileJson, config)
          .then(chargeDetails => {
            if(chargeDetails.messages.resultCode == 'Ok') {
              if(chargeDetails.transactionResponse.responseCode == '1' || chargeDetails.transactionResponse.responseCode == '4') {
                setSubmitting(true);
                onConfirm({address: selected, taxtotal: parseFloat(salesTax), shipamttotal: shipamttotal, cardtotal: Math.round(cardtotal * 100.0) / 100.0, grandTotal: Math.round(netAmount * 100.0) / 100.0, totalAmountSaved: saveamttotal, customerProfileID: parseInt(chargeDetails.transactionResponse.profile.customerProfileId), customerPaymentProfileId: parseInt(chargeDetails.transactionResponse.profile.customerPaymentProfileId), transId: parseInt(chargeDetails.transactionResponse.transId), transactionCode: chargeDetails.transactionResponse.responseCode, paymentMethod: "card" });
                this.setState({ loading: false });
              } else {
                this.setState({ loading: false });
                if(chargeDetails.transactionResponse.errors != undefined) {
                  const errorText = chargeDetails.transactionResponse.errors.map(( code ) => code.errorText)
                  this.setState({ alertMessage: true, errorMessage: errorText[0] });
                } else {
                  const successText = chargeDetails.transactionResponse.messages.map(( code ) => code.description)
                  this.setState({ alertMessage: true, errorMessage: successText[0] });
                }
              }
            } else {
                this.setState({ loading: false });
                if(chargeDetails.transactionResponse != undefined) {
                  const errorText = chargeDetails.transactionResponse.errors.map(( code ) => code.errorText)
                  this.setState({ alertMessage: true, errorMessage: errorText[0] });
                } else {
                  this.setState({ alertMessage: true, errorMessage: 'Transaction unsuccessful' });
                }
            }
          })
      }
  }

  nextStep() {
    this.props.closeCart();
  };


  render(){
    const { user, onConfirm, setSubmitting, placeOrder, addresses = [], selected, setSelected, cart, vendors = [], salesTax, getSalesTax, cardDetails, onClose, closeCart, authDetails, activeCard, activePayment, defaultCard, itemcount } = this.props;
    const { alertMessage } = this.state;
  const grandTotal = cart.reduce((acc, p) => p.listPrice != undefined ? acc + (p.amount * p.listPrice) : acc + (p.amount * p.price), 0);

  const productGroups = groupBy(cart, 'vendorId');
  var taxtotal =0;
  var shipamttotal =0;  
  var cardtotal =0;  
  var saveamttotal=0;
  for(var y in productGroups)
  {
    var taxamt =0;
    var amt =0;
    
    var selectedvendor={};
    var check=false;
    for(var x in productGroups[y])
    {
      check=true;    
      if(productGroups[y][x].listPrice != undefined) {
      amt += productGroups[y][x].listPrice * productGroups[y][x].amount;
      saveamttotal += (productGroups[y][x].price * productGroups[y][x].amount) - (productGroups[y][x].listPrice * productGroups[y][x].amount);
      } else { 
       amt += productGroups[y][x].price * productGroups[y][x].amount;
      } 
    }
    if(check)
    {
      for(var z in vendors)
      {
        if(vendors[z].id == productGroups[y][0].vendorId)
        {
          selectedvendor= vendors[z];
          break;
        }
      }       
      if(selectedvendor.shippingChargeLimit > amt && !productGroups[y][x].overnight)
      {
        shipamttotal += user.prime == true ? 0 : selectedvendor.shippingCharge;
      }
      if(selectedvendor.tax !=undefined && selectedvendor.tax !="")
      {
        taxtotal += ((amt/100)* selectedvendor.tax);
      }
      if(selectedvendor.creditCardFee !=undefined && selectedvendor.creditCardFee !="")
      {
        cardtotal += ((amt/100)* selectedvendor.creditCardFee);
      }
    }
  }
  //const taxAmount = currencyFormatter.format(taxtotal);
  const taxAmount = currencyFormatter.format(salesTax);
  const ShippTotal = currencyFormatter.format(shipamttotal);
  const savingsTotal = currencyFormatter.format(saveamttotal);
  const CreditCard = currencyFormatter.format(Math.round(cardtotal * 100.0) / 100.0);
  const overnightCount = Object.keys(productGroups).map(key => productGroups[key][0].overnight).filter(Boolean).length;
  const shippingCost = currencyFormatter.format(overnightCount * 52);
  const orderTotal =  grandTotal  + (overnightCount * 52) + shipamttotal + parseFloat(salesTax) + cardtotal;

  return (
    <div className={styles.ConfirmOrder}>
      {this.state.loading ? <Spinner paymentStyle={true} /> : null}

          <div>
            <ListGroup componentClass="ul">
              {addresses.map((address, i) => <AddressItem key={i} address={address} active={selected === address.id} onClick={(id)=> {setSelected(id); getSalesTax(id)}}/>)}
            </ListGroup>
            <h5 style={{textAlign: 'left'}}><b>Note: </b> Manage and add shipping addresses in the Shipping Setup Tab.</h5>
            <h5><span>Item(s) Subtotal:</span><b>{` ${currencyFormatter.format(grandTotal)}`}</b></h5>
            <h5><span>Overnight Shipping Cost:</span>{shippingCost != "$0.00"?` +`:``}<b>{` ${shippingCost}`}</b></h5>
            <h5><span>Ground Shipping Cost:</span>{ShippTotal != "$0.00"?` +`:``}<b>{` ${ShippTotal}`}</b></h5>
            <h5><span>Sales Tax:</span>{taxAmount != "$0.00"?` +`:``}<b>{` ${taxAmount}`}</b></h5>
            <h5><span>Processing Fee:</span>{CreditCard != "$0.00"?` +`:``}<b>{` ${CreditCard}`}</b></h5>
            <h5><span>Total Amount Saved:</span>{savingsTotal != "$0.00"?` +`:``}<b>{` ${savingsTotal}`}</b></h5>
            <h5><span>Grand Total:</span><b>{` ${currencyFormatter.format(Math.round(orderTotal * 100.0) / 100.0)}`}</b></h5>
            <PaymentForm
              user={user}
              cart={cart}
              schema={schema().products()}
              initialValues = {{amount: Math.round(orderTotal * 100.0) / 100.0}}
              vendors={vendors}
              salesTax={salesTax}
              cardDetails={cardDetails}
              activeCard={activeCard}
              defaultCard={defaultCard}
              authDetails={authDetails}
              activePayment={activePayment}
              closeCart={closeCart}
              itemcount={itemcount}
              getSalesTax={(id)=> getSalesTax(id)}
              shipamttotal={shipamttotal}
              cardtotal={cardtotal}
              orderTotal={orderTotal}
              grandTotal={grandTotal}
              saveamttotal={saveamttotal}
              handleFormSubmit={this.handleFormSubmit.bind(this)}
            >
              {(open) => <div onClick={open.bind(this, this.nextStep)} className={styles.nextbtn}>Proceed to Pay</div>}
            </PaymentForm>
          </div>
        {this.state.alertMessage ?
          <Modal show sm className="alertModal">
              <Modal.Body>
                <Row className={styles.rowStandard}>
                  <Icon className={`${styles.icon}`} glyph={'icon-fontello-attention'} />
                  <h3>Alert!</h3>
                  <Col>{this.state.errorMessage}</Col>
                </Row>
              </Modal.Body>
              <Modal.Footer className={`${styles.footer}`} onClick={()=>{ this.setState({ alertMessage: false })}}>
                  Dismiss
              </Modal.Footer>
          </Modal> : null}
    </div>)
  }
  closeModal() {
    this.setState({ showModal: false });
  }

  openModal() {
    this.setState({ showModal: true });
  }
};
ConfirmOrder.propTypes = {
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  shown: PropTypes.bool,
};


export default compose(
  connect(
    createSelector(
      selectors('vendors').selectRecordsMetaData(),
      (meta) => ({ addresses: (meta || Map()).get('shipping', List()).toJS() })
    )
  ),
  mapProps(({addresses, ...rest}) => ({addresses: addresses.sort((l,r) => l.isPractice <= r.isPractice), ...rest})),
  withState('submitting', 'setSubmitting', false),
  withState('selected', 'setSelected', props => (props.addresses.find(_ => _.primary) || {}).id)
)(ConfirmOrder);

