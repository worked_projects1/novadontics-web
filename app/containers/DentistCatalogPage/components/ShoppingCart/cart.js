export const addToCart = (product, amount, oldCart) => {
  const cart = oldCart.slice(0);
  const itemInCart = cart.findIndex((p) => p.id === product.id);
  if (itemInCart >= 0) {
    cart[itemInCart].amount += amount;
  } else {
    cart.push(Object.assign({}, product, { amount }));
  }
  return cart;
};

export const removeFromCart = (id, cart) => cart.filter((product) => product.id !== id);

export const countCartItems = (cart) => cart.reduce((acc, p) => acc + p.amount, 0);
