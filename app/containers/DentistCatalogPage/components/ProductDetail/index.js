import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Row, Col, Icon, Button } from '@sketchpixy/rubix';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import styles from './styles.css';
import Lightbox from 'react-image-lightbox';
import Gallery from 'components/Gallery';
import {addFavourite,removeFavourite,addCart} from 'blocks/products/remotes.js';
const defaultImage = 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg';

class ProductDetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { amount: 1, lightbox: false };
    this.addToCart = this.addToCart.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  async favadd(id, favourite) {
    if (id) {
      favourite ? removeFavourite(id) : addFavourite(id);
      this.props.favourite(id, favourite);
    }
  }
  handleClose() {
    this.setState({ lightbox: false })
  }
  render() {
    const { submitting, amount, favstate } = this.state;
    const { product = {}, onClose, toggleFinance } = this.props;
    const { imageUrl, name, serialNumber, description, price, favourite, id, quantity, listPrice, document, video, unit, packageQuantity, financeEligible } = product;
    const documentArr = document != null && document != "" ? document.split(',') : [];
    const images = Object.assign([], (imageUrl && imageUrl.split(',') || [defaultImage]).concat(video || []));
    return (
      <Modal show onHide={onClose} bsSize="large"  backdrop="static" dialogClassName="categorymodel">
        <Modal.Header closeButton style={{ marginBottom: 0 }}>
          <div style={{ paddingLeft: 15 }}>{serialNumber}</div>
          <div style={{ paddingLeft: 15 }}>{name}</div>
        </Modal.Header>
        <Modal.Body>
          <div style={{ padding: "0px 15px" }}>
            <Row style={{ minHeight: 200 }}>
              <Col sm={6} style={{ display: "flex", justifyContent: "center", alignItems: "center", paddingRight: 0 }}>
                <Gallery style={{ width: 'auto', maxWidth: '100%', maxHeight: 250, cursor: "pointer" }} data={{ images: images }} singleImage={true} imageStyle={true} width="100%" />
              </Col>
              <Col sm={6}>
                {description && description.length > 0 ?
                  description.split('\n').map((line, i) =>
                    <span key={i}> {line} <br /></span>) : ''}
              </Col>
            </Row>
            <Row>
              <Col sm={6} className={styles.finance}>
                {financeEligible ? <div className={styles.financeBtn} onClick={toggleFinance}>Finance</div> : null}
              </Col>
              <Col sm={6} className={styles.document}>
                {documentArr && documentArr.length > 0 ?
                  documentArr.map((url, index) =>
                    <a key={index} href={url} target="_blank" data-rel="external">
                      <img src={require('img/patients/pdf2.svg')} alt="pdf" className={styles.img} />
                    </a>
                  ) : null
                }
              </Col>
            </Row>
            <div style={{ display: "flex", justifyContent: "space-between", marginTop: 12, borderTop: "1px solid #ccc", borderBottom: "1px solid #ccc", padding: "9px 3px", marginBottom: 12 }}>
              <div style={{ float: "left", paddingLeft: 25, display: "flex", alignItems: "center" }}>
                <img onClick={() => this.favadd(id, favourite)} src={require(favourite ? 'img/vendor/IconHeart.svg' : 'img/vendor/IconHeartEmpty1.svg')} className={styles.filterImage} />
                {favourite ? <div style={{ paddingLeft: 15 }}>In Your Favorites List</div> : <div style={{ paddingLeft: 15 }}>Add To Favorites</div>}
                <Button bsStyle="link" bsSize="xs" className={styles.removeButton} onClick={()=>{this.props.handleSaveLater(id)}} >Save for Later</Button>
              </div>
              <div style={{ float: "right", display: "flex", alignItems: "center" }}>
                <div style={{ paddingRight: '100px' }}>
                  {packageQuantity ?
                    <p style={{ marginBottom: '0px' }}>
                      {unit != '' && unit != null ?
                        unit.toLowerCase() == 'each' && packageQuantity ? `(${packageQuantity} pcs.)` :
                        unit.toLowerCase() != 'each' && packageQuantity ? `( ${packageQuantity} pcs. / ${unit} )` :
                        unit.toLowerCase() == 'each' && !packageQuantity ? `(1 pcs.)` :
                        unit.toLowerCase() != 'each' && !packageQuantity ? `(1 pcs. / ${unit})` : null :
                        packageQuantity ? `(${packageQuantity} pcs.)` : null
                      }
                    </p> :
                    <p style={{ marginBottom: '0px' }}>
                      {unit != '' && unit != null ?
                        unit.toLowerCase() == 'each' && quantity ? `(${quantity} pcs.)` :
                        unit.toLowerCase() != 'each' && quantity ? `( ${quantity} pcs. / ${unit} )` :
                        unit.toLowerCase() == 'each' && !quantity ? `(1 pcs.)` :
                        unit.toLowerCase() != 'each' && !quantity ? `(1 pcs. / ${unit})` : null :
                        quantity ? `(${quantity} pcs.)` : null
                      }
                    </p>
                  }
                </div>

                {listPrice != undefined ? <div style={{ minWidth: '180px', display: "flex", justifyContent: "flex-end" }}> <span style={{ paddingRight: 10 }}>Price: </span>{currencyFormatter.format(listPrice)} {listPrice != undefined ? <span> (<strike>{currencyFormatter.format(price)}</strike>) <b className="savedetail">Save {currencyFormatter.format(price - listPrice)}</b></span> : null}</div> :
                  <div style={{ minWidth: '180px', display: "flex", justifyContent: "flex-end" }}><span style={{ paddingRight: 10 }}>Price: </span> {currencyFormatter.format(price)}</div>}
              </div>
            </div>
          </div>



          <div style={{ display: "flex", justifyContent: "flex-end", paddingRight: 15 }}>
            <div className={styles.productoption}>
              <div className={styles.cartaddsub}>
                <div onClick={() => { this.setState({ amount: amount <= 1 ? 1 : amount - 1 }) }}>
                  <Icon bundle="fontello" glyph="minus" className={styles.cartsub} />
                </div>
                <input
                  className={styles.cartinput}
                  type="number"
                  step="1"
                  min="1"
                  value={amount}
                  onChange={this.onChange}
                />
                <div onClick={() => { this.setState({ amount: amount + 1 }) }}>
                  <Icon bundle="fontello" glyph="plus" className={styles.cartadd} />
                </div>
              </div>
              <div style={{ width: 20 }}></div>
              <div

                className={styles.cartnewbtn}
                onClick={this.addToCart}
                disabled={submitting}
              >
                {submitting ? 'Added!' : `Add to Cart`}

              </div>
            </div>

            {this.state.lightbox ?
              <Lightbox
                mainSrc={imageUrl}
                onCloseRequest={this.handleClose.bind(this)}
              /> : null}
          </div>
        </Modal.Body>
      </Modal>
    );
  }

  onChange(e) {
    const { value } = e.target;
    const amount = parseFloat(value);
    this.setState({ amount: isNaN(amount) || amount < 1 ? 1 : amount });
  }

  addToCart() {
    const { product = {}, addToCart } = this.props;
    this.setState({ submitting: true });
    this.setSubmitting = setTimeout(() => this.setState({ submitting: false }), 750);
    addToCart(product, this.state.amount);
    addCart({"productId":product.id,"quantity":this.state.amount});
    setTimeout(() => this.props.onClose(), 1000)
  }
}

ProductDetailView.propTypes = {
  product: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
};

export default ProductDetailView;
