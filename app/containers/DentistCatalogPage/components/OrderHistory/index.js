/**
*
* OrderHistory
*
*/

import React from 'react';
import { Modal, Col } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import ReactTable from "react-table";
import schema from 'routes/schema';
import OrderDetails from './OrderDetails.js';
import Spinner from 'components/Spinner';

class OrderHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showOrderDet:false, selectedOrder:false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { show, hide, vendors, orders, load, openProductDetailView, user } = this.props;
    const data = orders ? orders.reduce((order, el)=> {
      order.push(Object.assign({}, el, {status: el.status == 'Void' ? 'Closed' : el.status}));
      return order;
    }, []) : false;
    const { showOrderDet, selectedOrder } = this.state;
    return (
        <Col>
            <Modal show={show && !showOrderDet ? true: false} backdrop="static" className={"PaymentHistory"} bsSize="large" onHide={hide} onEnter={load}>
                <Modal.Header closeButton>
                    <Modal.Title style={{ color: '#ea6225' }}>
                        Order History 
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {data ? 
                    <ReactTable
                        columns={schema().products().orderHistory}
                        data={data ? data.sort((a, b) => b.id - a.id): []}
                        defaultPageSize={10}
                        showPageJump={false}
                        resizable={false}
                        showPageSizeOptions={false}
                        sortable={false}
                        previousText={"Back"}
                        pageText={""}
                        noDataText={"No entries to show."}
                        getTdProps={(state, rowInfo, column, instance) => {
                            return {onClick: () => this.setState({selectedOrder:rowInfo.row._original,showOrderDet:true}) }}
                        }
                    /> : <Spinner /> }
                </Modal.Body>
            </Modal>
            <OrderDetails user={user} show={showOrderDet} hide={()=>this.setState({showOrderDet:false})} order={selectedOrder} vendors={vendors} openProductDetailView={(id)=> openProductDetailView(id)}/>
        </Col>
    );
  }

}

export default OrderHistory;
