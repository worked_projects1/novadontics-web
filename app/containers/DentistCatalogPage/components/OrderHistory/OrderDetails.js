/**
*
* OrderDetails
*
*/

import React from 'react';
import { Col, Modal, Row } from '@sketchpixy/rubix';
import {OrderTable} from '../ShoppingCart/index.js';
import shallowCompare from 'react-addons-shallow-compare';
import {get} from 'utils/misc';
import styles from './styles.css';

class OrderDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = { orders: false, showOrderDetails: true };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { show, hide, order ={}, vendors, openProductDetailView, user } = this.props;
    const { items, address, id} = order;    
    const { showOrderDetails } = this.state;
    
    return (
      <Modal show={show} onHide={hide} bsSize="large" backdrop="static" dialogClassName="categorymodel" >
      <Modal.Header closeButton>
        <Modal.Title><b>Past order #{id}</b></Modal.Title>
        {items && items.length > 0 ? <div>{`${items.length} item${items.length>1?'s':''} in order`}</div>:null}
      </Modal.Header>
      <Modal.Body>
      <Col className={styles.OrderDetails}>
        <OrderTable
            user={user}
            cart={items && items.length > 0 ? items: []}
            vendors={vendors || []}
            openProductDetailView={(id)=> openProductDetailView(id)}
            orderDetails={order}
            showOrderDetails={showOrderDetails}
            showQuantity={true}
            viewOnly
        />
        {address != undefined ?
        <Row>
            <Col className={styles.shippingAddress}>
              <Col><b>Shipping Address</b></Col>
              <Col>{get(address, 'name')} - {get(address, 'phone')}</Col> 
              <Col>{get(address, 'address')}, {`${get(address, 'city')}, ${get(address, 'state')} ${get(address, 'zip')}`}, {`${get(address, 'country')}`}</Col>
            </Col>
        </Row> : null}
      </Col>
      </Modal.Body>
      </Modal>
    );
  }

}

export default OrderDetails;
