import React from 'react';
import PropTypes from 'prop-types';
import ProductCell from './ProductCell.js';
import styles from './styles.css';

const ProductBox = ({ productsInView,subproducts ,addToCart, openSubscribeProduct,getSubscribeProductSalesTax, openProductDetailView,favourite, reloadCart, reloadProduct, toggleFinance}) =>
  <div className={styles.productBox}>
    {productsInView.length > 0 ?
      <div>
        {productsInView.map(((product) =>
          <ProductCell
            key={product.id}
            product={product ? product : {}}
            addToCart={addToCart}
            reloadCart={reloadCart}
            toggleFinance={toggleFinance}
            reloadProduct={reloadProduct}
            onFavourite={favourite}
            subproducts={subproducts}
            openProductDetailView={openProductDetailView}
            getSubscribeProductSalesTax={getSubscribeProductSalesTax}
            openSubscribeProduct={openSubscribeProduct}
          />))}
      </div> :
      <p style={{ opacity: 0.6, fontSize: '0.7em' }}>
        No products match your search.
      </p>}
  </div>;

ProductBox.propTypes = {
  productsInView: PropTypes.array,
  addToCart: PropTypes.func.isRequired,
  openProductDetailView: PropTypes.func.isRequired,
};

export default ProductBox;
