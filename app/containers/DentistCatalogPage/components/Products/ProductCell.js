import React from 'react';
import PropTypes from 'prop-types';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import { Row, Col ,Icon, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import {addFavourite,removeFavourite,addCart, moveSaveLater, deleteSaveLater} from 'blocks/products/remotes.js';
import ConfirmButton from 'components/ConfirmButton';
const defaultImage = 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg';

class ProductCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: 1,
    };
    this.addToCart = this.addToCart.bind(this);
    this.onChange = this.onChange.bind(this);
    this.favourite = this.favourite.bind(this);
  }

  componentWillUnmount() {
    clearTimeout(this.setSubmitting);
  }
  
  render() {
    const { product = {}, openProductDetailView, getSubscribeProductSalesTax, subproducts, openSubscribeProduct, toggleFinance } = this.props;

    const { id, name, imageUrl, price, serialNumber, quantity, favourite, saveLater, listPrice, unit, financeEligible } = product;
    const { amount, submitting } = this.state;
    const productPrice = currencyFormatter.format(price);
    	
	  return (
      <div>
        <Row className={styles.productcellcontainer}>
          <Col sm={2} className={styles.imagecontainer}>
            <div className={imageUrl?styles.productWithoutImageView:styles.productImageView} onClick={openProductDetailView.bind(null, id)}>
              <img width="80" src={imageUrl && imageUrl.split(",")[0] || defaultImage} />
            </div>
          </Col>
          <Col sm={4} style={{paddingLeft:0}}>
            <div className={styles.productButton} onClick={openProductDetailView.bind(null, id)}>{name}</div>
            <div className={styles.productButton} onClick={openProductDetailView.bind(null, id)}>{serialNumber}</div>
            <div style={{ display:"flex",color: "#000"}}>
            {listPrice != undefined ?
            <div>
              <p>
                {unit != '' && unit != null ?
                  unit.toLowerCase() == 'each' && quantity ? `(${quantity} pcs.)` :
                  unit.toLowerCase() != 'each' && quantity ? `( ${quantity} pcs. / ${unit} )` :
                  unit.toLowerCase() == 'each' && !quantity ? `(1 pcs.)` :
                  unit.toLowerCase() != 'each' && !quantity ? `(1 pcs. / ${unit})` : null :
                  quantity ? `(${quantity} pcs.)` : null
                }
              </p>
              <p>
                <b>{currencyFormatter.format(listPrice)}</b>  
                <span>(<strike>{currencyFormatter.format(price)}</strike>) 
                <div><b className="save">Save {currencyFormatter.format(price - listPrice)}</b></div></span>
              </p>
            </div> : <p><b>{productPrice}</b> </p>}
            </div>
          </Col>
          <Col sm={6}>
            {saveLater ?
              <div className={styles.productoption}>
                <div>
                  <div style={{ marginLeft: '7px' }}>
                    <img onClick={()=>{this.favourite(id,favourite)}} src={require(favourite?'img/vendor/IconHeart.svg':'img/vendor/IconHeartEmpty1.svg')} className={styles.filterImage} />
                  </div>
                  <div>
                    <ConfirmButton onConfirm={() => {this.handleDelete(id)}}>{(click) => <Button bsStyle="link" bsSize="xs" className={styles.removeButton} onClick={() => click()}>Delete</Button>}</ConfirmButton>
                  </div>
                </div>
                <div className={styles.cartmovebtn} onClick={()=>{this.moveToCart(id)}} disabled={submitting}> Move to Cart </div>
              </div> :
              <div className={styles.productoption}>
                <div>
                  <div className={styles.cartaddsub}>
                    <div onClick={()=>{this.setState({amount: amount <=1 ? 1: amount-1})}}>
                      <Icon bundle="fontello" glyph="minus"className={styles.cartsub}/>
                    </div>
                    <input className={styles.cartinput} type="number" step="1" min="1" value={amount} onChange={this.onChange} />
                    <div onClick={()=>{this.setState({amount:amount+1})}}>
                      <Icon bundle="fontello" glyph="plus" className={styles.cartadd} />
                    </div>
                  </div>
                  {financeEligible ? <div className={styles.financeBtn} onClick={toggleFinance}>Finance</div> : null}
                </div>
                <img onClick={()=>{this.favourite(id,favourite)}} src={require(favourite?'img/vendor/IconHeart.svg':'img/vendor/IconHeartEmpty1.svg')} className={styles.filterImage} />
                <div>
                  <div className={styles.subscribeButton} onClick={openSubscribeProduct.bind(null, id, amount)}>Subscribe</div>
                  <div className={styles.cartnewbtn} onClick={this.addToCart} disabled={submitting}> {submitting ? 'Added!' : `Add to Cart`}</div>
                </div>
              </div> }
          </Col>
        </Row>
        <hr />
      </div>
    );
  }

  onChange(e) {
    const { value } = e.target;
    const amount = parseFloat(value);
    this.setState({ amount: isNaN(amount) || amount < 1 ? 1 : amount });
  }

  addToCart() {
    const { product = {}, addToCart } = this.props;
    this.setState({ submitting: true });
    this.setSubmitting = setTimeout(() => this.setState({ submitting: false }), 750);
    addToCart(product, this.state.amount);
    addCart({"productId":product.id,"quantity":this.state.amount});
  }

  favourite(id , favourite) {
    const { onFavourite } = this.props; 
    favourite ? removeFavourite(id) : addFavourite(id);
    onFavourite(id , favourite);
  }

  moveToCart(id) {
    moveSaveLater(id);
    const cartReload = setInterval(() => {
      this.props.reloadCart();
      this.props.reloadProduct('true', 'saveLater')
      clearReloadTimer();
    }, 300);
    const clearReloadTimer = () => clearInterval(cartReload);
  }

  handleDelete(id) {
    deleteSaveLater(id);
    const productReload = setInterval(() => {
      this.props.reloadProduct('true', 'saveLater')
      clearReloadTimer();
    }, 300);
    const clearReloadTimer = () => clearInterval(productReload);
  }
}

ProductCell.propTypes = {
  product: PropTypes.object.isRequired,
  addToCart: PropTypes.func.isRequired,
  openProductDetailView: PropTypes.func.isRequired,
};

export default ProductCell;
