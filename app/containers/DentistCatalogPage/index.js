/*
 * DentistCatalogPage Container
 */

import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import CategoryList from './components/Categories/index.js';
import ProductBox from './components/Products/index.js';
import ProductDetailView from './components/ProductDetail/index.js';
import Filter from './components/Filter/index.js';
import OrderHistory from './components/OrderHistory/index.js'
import SubscribeHistory from './components/SubscribeHistory/index.js'
import Policy from './components/Policy/index.js';
import { selectUser } from 'blocks/session/selectors';
import { placeOrder, subscribeOrder } from 'blocks/dentistOrders';
import ShoppingCart from './components/ShoppingCart/index.js';
import SubscribeProduct from './components/SubscribeProduct/index.js';
import { addToCart, countCartItems, removeFromCart } from './components/ShoppingCart/cart.js';
import { Grid, Row, Col, Icon, Pagination } from '@sketchpixy/rubix';
import { findProducts, removeCart, Cart, findSaleTax,findProductSaletax, authorizeApi, loadAuthorize, productTotalCount, addSaveLater } from 'blocks/products/remotes.js';
import { findCategories } from 'blocks/categories/remotes.js';
import { catalogOrders as orderRecords, subscribeRecords } from 'blocks/orders/remotes.js';
import { getProductDetail } from 'blocks/products/remotes.js';
import ReactTooltip from 'react-tooltip';
import numeral from "numeral";
import styles from './style.css';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from '../RecordsPage/messages';


const chunkArray = (arr, len) => {
  const chunks = [];
  let i = 0;
  while (i < arr.length) {
    chunks.push(arr.slice(i, i += len));
  }
  return chunks;
};

export default function (actions, selectors) {
  class DentistCatalogPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        authDetails: [],
        cardDetails: [],
        notesArr: [],
        cart: [],
        saveForLater: [],
        allCategories: false,
        productsValue:'',
        products: false,
        productCount: 0,
        productTotalCount: 0,
        subscribeOrders: false,
        showAllCount: 0,
        orders: false,
        subscribedList: false,
        subscribedSingleList: false,
        productDetail: false,
        showCart: false,
        showSubscribeProduct: false,
        updateSubscribeOrder: false,
        cartSubscribeOrder: false,
        subscribeOrderId: 0,
        subscribedProduct: false,
        subscribedProductId: 0,
        subscribedAmount: false,
        orderFrequency: "1 month",
        showPolicy: false,
        showOrder: false,
        showSubscribeHistory: false,
        showSubscribeOrder: false,
        showFilter: false,
        maxPrice: 10000,
        searchData: { favourites: "false", saveLater: "false", searchText: "", vendors: [], categories: [], price: 0, pageNumber: 1, recPerPage: 25 },
        salesTax: '0',
        shippingAddress:-1,
        alertMessage: false,
        alertCheckMessage: false,
        alertSubscribeMessage: false,
        alertReSubscribeMessage: false,
        alertUpdateSubscribeMessage: false,
        successText: false,
        financeInfo: false,
        financeAlert: false
      };
      this.handleSearch = this.handleSearch.bind(this);
    }

    componentDidMount() {

      const { authDetails } = this.state;
      const { router = {} } = this.props;
      const { location = {} } = router;
      const { state = {} } = location;

      if (this.props.dispatch) {
        this.props.dispatch(actions.loadRecords());
      }
      loadAuthorize()
        .then(authDetail => {
          authDetail.map(authDetails => {
            this.setState({ authDetails: authDetails })
            if(this.props.user.authorizeCustId != 0) {
              const cardDetailJson = {"getCustomerProfileRequest": {"merchantAuthentication": {"name": authDetails.apiLoginId, "transactionKey": authDetails.transactionKey}, "customerProfileId": this.props.user.authorizeCustId, "unmaskExpirationDate" : "true", "includeIssuerInfo": "true"}}
              const config = {
                  transformRequest: [(data, headers) => {
                    delete headers.common['Authorization'];
                    delete headers.common['X-Auth-Token'];
                    delete headers.common['timezone'];
                      if (data !== undefined) {
                         headers.post['Content-Type'] = 'application/json;charset=UTF-8';
                         return JSON.stringify(data)
                      } else {
                        return data;
                      }
                    return data;
                  }]
              };
              authorizeApi(authDetails.apiUrl, cardDetailJson, config)
                .then(cardDetails => {
                  if(cardDetails.profile != undefined){
                    const checkObject = {"customerPaymentProfileId": "Check", "payment": {"creditCard": { "showCheck": true, "checkTitle": "Pay by Check", "checkText": "When you select payment by check, your order will be on hold until the check is received by Novadontics and cleared by your bank. A 1% processing fee will apply.", "cardType": "Check" }}}
                    const financeObject = {"customerPaymentProfileId": "Finance", "payment": {"creditCard": { "showFinance": true, "financeTitle": "Pay by Finance", "cardType": "Finance" }}}
                    cardDetails.profile.paymentProfiles.push(financeObject);
                    cardDetails.profile.paymentProfiles.push(checkObject);
                    this.setState({ cardDetails: cardDetails.profile })
                  } else{
                    this.setState({ cardDetails: [] })
                  }
                }).catch(() => this.setState({ cardDetails: [] }));
            }
          })
      }).catch(() => this.setState({ authDetails: [] }));

      Cart().then(cart => this.setState({ cart: cart }));
      this.loadSaveForLaterHistory();
      this.loadSubscribeHistory();
      productTotalCount().then(productTotal => this.setState({ productTotalCount: productTotal.productCount, subscribeOrders: productTotal.subscribeOrders }));
      if(state.openCart && state.openCart == true) {
        this.handleShowcart();
      }
    }

    render() {
      const { user = {}, records, metaData = [] } = this.props;
      const { vendorGroups } = metaData;
      const { allCategories, showCart, showSubscribeProduct, updateSubscribeOrder, cartSubscribeOrder, subscribeOrderId, cart, showPolicy, showOrder, showSubscribeHistory, showSubscribeOrder, showFilter, searchData = {}, products, productTotalCount, productCount, showAllCount, productDetail, subscribedProduct, subscribedProductId, subscribedAmount,productsValue, shippingAddress,orderFrequency, subscribeOrders, orders, subscribedList, subscribedSingleList, maxPrice, alertMessage, alertCheckMessage, alertSubscribeMessage, alertReSubscribeMessage, alertUpdateSubscribeMessage, successText, cardDetails, authDetails, notesArr, saveForLater } = this.state;
      const { favourites, saveLater, searchText, price, vendors, pageNumber, recPerPage, categories } = searchData;
      const selectedOrder = orders && orders.find((c) => c.items && c.items.find(r => r.id === productDetail));
      const filteredVendorGroups = vendorGroups && vendorGroups.length > 0 && vendorGroups.map(group => group.id) || [];
      const otherItems = records.filter(item => item.groups == '' || item.groups == null || item.groups == []);

      return (
        <Grid className={styles.DentistCatalogPage} style={{ "background": `${!products ? '#6d6e6f' : '#ffff'}` }}>
          <ReactTooltip type="info" html={true} />
          <Row className={styles.header}>
            <Col xs={12} className={styles.navHeader} >
              <Col onClick={this.clearForm.bind(this)}>{products ? `Back` : null}</Col>
              <h4>Catalog</h4>
              <Col onClick={() => this.handleSearch(records.map(_ => _.id), 'vendors')}>{!products ? `Done` : null}</Col>
            </Col>
            <Col xs={12} className={styles.searchBox}>
              <Col md={5} sm={5} className={styles.searchText}>
                <Icon bundle="fontello" glyph="search-1" className={styles.searchIcon} />
                <input type="text" id="categorytxt" value={searchText} placeholder="Search" onChange={(e) => this.handleSearch(e.target.value, 'searchText')} className={styles.searchInput} />
              </Col>
              <Col md={7} sm={7} className={styles.icons}>
                <div style={{ width: '54px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <div style={{ height: '40px' }}>
                    <span style={{ width: '39px', fontSize: '24px' }}>{productTotalCount}</span>
                  </div>
                  <span style={{ width: '54px' }}>Items Available</span>
                </div>
                <div style={{ width: '54px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                    <img src={require(`img/vendor/${favourites == 'true' ? `IconHeart.svg` : `IconHeartEmpty${products ? '1' : ''}.svg`}`)} ref='foo' onClick={() => this.handleSearch(favourites == 'true' ? 'false' : 'true', 'favourites')} style={{ marginLeft: '6px' }} />
                  <span style={{ width: '39px' }}>Favorite Products</span>
                </div>
                <div style={{ width: '60px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <img src={require(`img/vendor/${saveLater == 'true' ? `saveLater.svg` : `saveLaterEmpty${products ? '1' : ''}.svg`}`)} ref='foo' onClick={() => this.handleSearch(saveLater == 'true' ? 'false' : 'true', 'saveLater')} style={{ marginLeft: '8px' }} />
                  <span style={{ width: '54px' }}>Saved for Later</span>
                </div>
                <div style={{ width: '54px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <img src={require(`img/vendor/FilterIcon${products ? '1' : ''}.svg`)} ref='foo' onClick={() => this.setState({ showFilter: true })} />
                  <span style={{ width: '39px' }}>Search Filter</span>
                </div>
                <div style={{ width: '54px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <img src={require(`img/vendor/HistoryIcon${products ? '1' : ''}.svg`)} ref='foo' onClick={() => this.setState({ showOrder: true })} />
                  <span style={{ width: '39px' }}>Order History</span>
                </div>
                <div style={{ width: '54px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <img src={require(`img/vendor/ReturnPolicyIcon${products ? '1' : ''}.svg`)} ref='foo' onClick={() => this.setState({ showPolicy: true })} />
                  <span style={{ width: '39px' }}>Return Policy</span>
                </div>
                <div style={{ width: '60px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <Link to="https://novadontics-uploads.s3.amazonaws.com/ProductCatalogManual.pdf" target="_blank">
                      <img src={require(`img/vendor/Manual${products ? '1' : ''}.svg`)} ref='foo' onClick={(e) => e.stopPropagation()} />
                  </Link>
                  <span style={{ width: '39px' }}>Instruction Manual</span>
                </div>
                <div style={{ width: '54px' }} className={products ? styles.imgTextBlack : styles.imgText}>
                  <img style={{ width: '40px', marginLeft: '3px' }} src={require(`img/vendor/subscriptionEmpty${products ? '1' : ''}.svg`)} ref='foo' onClick={() => {this.setState({ showSubscribeHistory: true })}} />
                  <span style={{ width: '39px' }}>Subscribed Products</span>
                </div>
                <Col onClick={this.handleShowcart.bind(this)} ref='foo' data-place={"bottom"} data-tip={"Cart"}><img src={require(`img/vendor/CartIcon${products ? '1' : ''}.svg`)} /><span className={styles.cartCount}>{countCartItems(cart)}</span></Col>
              </Col>
            </Col>
          </Row>
          {!products ? <Grid style={{ minHeight: `${document.body.clientHeight - 265}px` }}>
            <Col>{
              vendorGroups && vendorGroups.length > 0 ? vendorGroups.map((group, index) => {
                const filteredRecords = records.filter(item => item.groups.groups && item.groups.groups.find(groups => parseInt(groups.groupId) === group.id));

                return filteredRecords && filteredRecords.length > 0 ? <div key={index}>
                  <Row>
                    <h4 className={styles.groupTitle}>{group.name}</h4>
                    {filteredRecords.map((vendor) =>
                      <Col md={4} className={styles.boxCatalog} key={vendor.id}>
                        <div className={styles.vendorOutsideBox}>
                          <Link onClick={() => this.handleSearch(vendors.concat(vendor.id), 'vendors')}>
                            <img className={styles.vendorImageHelper} src={vendor.groups.groups && vendor.groups.groups.find(groups => parseInt(groups.groupId) === group.id).image.indexOf("http") > -1 ? vendor.groups.groups.find(groups => parseInt(groups.groupId) === group.id).image : "http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg"} alt={vendor.name} style={{ width: '80%' }} />
                          </Link>
                        </div>
                        <div className={styles.vendorSecondBox}>
                          <Link onClick={() => this.handleSearch(vendors.concat(vendor.id), 'vendors')}>
                            <img className={styles.vendorImageHelper} src={vendor.logoUrl && vendor.logoUrl.indexOf("http") > -1 ? vendor.logoUrl : "http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg"} alt={vendor.name} style={{ width: '80%', paddingTop:'5px', paddingBottom:'5px' }} />
                          </Link>
                        </div>
                      </Col>
                    )}
                  </Row>
                </div> : null
              }) : null
            }
            {
              otherItems && otherItems.length > 0 ?
                <div>
                  <Row>
                    <h4 className={styles.groupTitle}>Other</h4>
                    {otherItems.map((vendor) =>
                      <Col md={4} className={styles.boxCatalog} key={vendor.id}>
                        <div className={styles.vendorOutsideBox}>
                          <Link onClick={() => this.handleSearch(vendors.concat(vendor.id), 'vendors')}>
                            <img className={styles.vendorImageHelper} src={"http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg"} alt={vendor.name} style={{ width: '80%' }} />
                          </Link>
                        </div>
                        <div className={styles.vendorSecondBox}>
                          <Link onClick={() => this.handleSearch(vendors.concat(vendor.id), 'vendors')}>
                            <img className={styles.vendorImageHelper} src={vendor.logoUrl && vendor.logoUrl.indexOf("http") > -1 ? vendor.logoUrl : "http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg"} alt={vendor.name} style={{ width: '80%', paddingTop:'5px', paddingBottom:'5px' }} />
                          </Link>
                        </div>
                      </Col>
                    )}
                  </Row>
                </div> : null
            }
            </Col>
          </Grid> : null}
          {products ? <Grid>
            <Row>
              <Col xsHidden smHidden md={3} className={styles.filtersWrapper} style={{ height: `${document.body.clientHeight - 340}px`, overflowY: 'auto' }}>
                <div className={styles.filters}>
                  <h3 style={{ marginTop: 0, marginLeft: -13, marginBottom: 20 }}>Categories</h3>
                  <div className={styles.filterBox}>
                    <div id={`check-0`} onClick={() => this.handleSearch([], 'categories')} className={`${styles.boldText} ${showAllCount === productCount && categories.length == 0 ? styles.boldTextSelect : null}`}>Show All <span style={{ color: "rgb(169, 169, 173)" }}>({showAllCount})</span></div>
                    <CategoryList selected={categories} categories={allCategories ? allCategories : []} onChange={(e) => this.handleSearch([e], 'categories')} />
                  </div>
                </div>
              </Col>
              <Col md={9} id="scrolling" className={styles.scrollBar} style={{ height: `${document.body.clientHeight - 340}px`, overflowY: 'auto' }}>
                <ProductBox
                  productsInView={products}
                  addToCart={this.addToCart}
                  reloadCart={this.reloadCart}
                  toggleFinance={this.toggleFinance}
                  handleSaveLater={this.handleSaveLater}
                  openProductDetailView={this.toggleProductDetailView}
                  subproducts={this.state.productsValue}
                  openSubscribeProduct={this.toggleSubscribeProduct}
                  getSubscribeProductSalesTax={this.getSubscribeProductSalesTax.bind(this)}
                  favourite={(id, favourite) => this.addFavourite(id, favourite)}
                  reloadProduct={(input, type) => this.handleSearch(input, type)}
                  saveLater={saveLater}
                />
              </Col>
            </Row>
          </Grid> : null}

          <Row className={styles.footer}>
            {products ? <Col xs={12} className={styles.pagination}>
              <div className={styles.showPages}>{products.length > 0 ? `Showing ${pageNumber} of ${Math.ceil(productCount / recPerPage)} pages.` : `No pages to show.`}</div>
              <Pagination
                prev="Back"
                next="Next"
                boundaryLinks
                items={Math.ceil(productCount / recPerPage)}
                maxButtons={4}
                activePage={pageNumber}
                onSelect={(e) => this.handleSearch(e, 'pageNumber')} />
            </Col> : null}
          </Row>

          {products ?
            <a onClick={this.scrollTop.bind(this)} className={styles.returnTop} id="return-to-top"><Icon bundle="fontello" glyph="up-open" /></a> : null}
          {productDetail ? <ProductDetailView
            product={products && products.find((r) => r.id === productDetail) || cart && cart.find((c) => c.id === productDetail) || saveForLater && saveForLater.find((c) => c.id === productDetail) || selectedOrder && selectedOrder['items'] && selectedOrder['items'].find(_ => _.id === productDetail) || {}}
            onClose={this.toggleProductDetailView}
            addToCart={this.addToCart}
            toggleFinance={this.toggleFinance}
            handleSaveLater={this.handleSaveLater}
            favourite={(id, favourite) => this.addFavourite(id, favourite)}
          /> : null}
          <Filter show={showFilter} hide={() => this.setState({ showFilter: false })} vendors={records} price={price} maxPrice={maxPrice} selectedVendors={vendors} onChange={(input, type) => this.handleSearch(input, type)} clearfilter={this.clearfilter.bind(this)} />
          <OrderHistory user={user} show={showOrder} orders={orders} hide={() => this.setState({ showOrder: false })} load={this.loadOrders.bind(this)} vendors={records} openProductDetailView={(id) => this.toggleProductDetailView(id)} />
          <Policy url={"https://s3.amazonaws.com/static.novadonticsllc.com/ReturnPolicy/index.html"} show={showPolicy} hide={() => this.setState({ showPolicy: false })} />
          <ShoppingCart
            shown={showCart}
            user={user}
            vendors={records}
            cart={cart}
            saveForLater={saveForLater}
            onFavourite={(id, favourite) => this.addFavourite(id, favourite)}
            onClose={this.toggleCart}
            toggleFinance={this.toggleFinance}
            toggleCount={this.toggleCartSubscribeProduct}
            reloadCart={this.reloadCart}
            removeFromCart={this.removeFromCart}
            handleVendorNotes={this.handleVendorNotes}
            placeOrder={this.placeOrder}
            onOvernight={this.setOvernight}
            addToCart={this.addToCart}
            openProductDetailView={(id) => this.toggleProductDetailView(id)}
            salesTax={this.state.salesTax}
            getSalesTax={this.getSalesTax.bind(this)}
            reloadSaveLater={this.loadSaveForLaterHistory.bind(this)}
            cardDetails={cardDetails}
            authDetails={authDetails}
            notesArr={notesArr}
          />
          <SubscribeProduct
            shown={showSubscribeProduct}
            user={user}
            vendors={records}
            toggleCount={this.toggleSubscribeProduct}
            changeFrequency={this.changeFrequency}
            cart={subscribedProduct}
            onClose={this.closeSubscribeProduct}
            subscribeOrder={this.subscribeOrder}
            subproducts={this.state.productsValue}
            onOvernight={this.setOvernight}
            salesTax={this.state.salesTax}
            getSalesTax={this.getSalesTax.bind(this)}
            getSubscribeProductSalesTax={this.getSubscribeProductSalesTax.bind(this)}
            showFrequency={true}
            orderFrequency={orderFrequency}
            updateSubscribeOrder={updateSubscribeOrder}
            subscribeOrderId={subscribeOrderId}
            cardDetails={cardDetails}
            authDetails={authDetails}
          />
          <SubscribeHistory user={user} show={showSubscribeHistory} subscribedList={subscribedList} hide={() => this.setState({ showSubscribeHistory: false })} load={this.loadSubscribeHistory.bind(this)} vendors={records} toggleCount={this.toggleEditSubscribeProduct} />
        {this.state.alertMessage ?
            <AlertMessage catalogSuccess dismiss={() => this.setState({ alertMessage: false })}><FormattedMessage {...messages.catalog} /></AlertMessage> : null}
        {this.state.alertCheckMessage ?
            <AlertMessage catalogSuccess dismiss={() => this.setState({ alertCheckMessage: false })}><FormattedMessage {...messages.catalogCheck} /></AlertMessage> : null}
        {this.state.alertSubscribeMessage ?
            <AlertMessage catalogSubscribe catalogSuccess dismiss={() => { this.closeSubscribeProduct(); this.setState({ alertSubscribeMessage: false })}}>{successText}</AlertMessage> : null}
        {this.state.alertReSubscribeMessage ?
            <AlertMessage alert dismiss={() => {this.setState({ alertReSubscribeMessage: false })}}>You have already subscribed for this product.</AlertMessage> : null}
        {this.state.alertUpdateSubscribeMessage ?
            <AlertMessage catalogSuccess dismiss={() => { this.closeSubscribeProduct(); this.setState({ alertUpdateSubscribeMessage: false })}}>{successText}</AlertMessage> : null}
        {this.state.financeInfo ?
          <AlertMessage finance dismiss={() => this.setState({ financeInfo: false })}></AlertMessage> : null}
        {this.state.financeAlert ?
            <AlertMessage checklist dismiss={() => {this.setState({ financeAlert: false })}}>All products in cart must be eligible for financing. Please remove the products that are not eligible for financing and try again. Products not eligible for financing must be purchased in a separate order.</AlertMessage> : null}
        </Grid>
      );
    }

    loadOrders() {
      orderRecords().then(r => this.setState({ orders: r })).catch(() => this.setState({ orders: [] }));
    }

    loadSubscribeHistory() {
      subscribeRecords().then(r => this.setState({ subscribedList: r })).catch(() => this.setState({ subscribedList: [] }));
    }

    loadSaveForLaterHistory() {
      var saveLaterValue = { 'saveLater': 'true' }
      findProducts(saveLaterValue).then(resp => {
        this.setState({ saveForLater: resp.data });
      }).catch(() => this.setState({ saveForLater: [] }));
    }

    scrollTop() {
      $('body, html, #scrolling').animate({ scrollTop: 0 }, 500);
    }

    async handleSearch(input, type) {
      let { searchData = {}, showAllCount } = this.state;

      if(type == 'vendors') {
        var vendorsValue = {...this.state.searchData}
        searchData.favourites = 'false';
        searchData.saveLater = 'false';
        this.setState({vendorsValue})
      }

      if(type == 'saveLater') {
        var saveLaterValue = {...this.state.searchData}
        searchData.favourites = 'false';
        this.setState({saveLaterValue})
      }

      if(type == 'favourites') {
        var favouritesValue = {...this.state.searchData}
        searchData.saveLater = 'false';
        this.setState({favouritesValue})
      }

      if (type) {
        const records = Object.assign({}, searchData, { [type]: input }, type != 'pageNumber' ? { pageNumber: 1 } : {}, type != 'categories' && type != 'pageNumber' ? { categories: [] } : {});
        this.setState({ searchData: records });
        findProducts(records).then(resp => {
          this.setState({
            products: resp.data,
            productCount: parseInt(resp.headers.productcount),
            showAllCount: type != 'categories' && type != 'pageNumber' ? parseInt(resp.headers.productcount) : showAllCount,
            maxPrice: resp.headers.maxprice
          });

          if(type == 'saveLater' && input == 'true' && this.state.products.length != 0) {
            const saveLaterProducts = this.state.products.map(v => ({...v, saveLater: true}))
            this.setState({ products: saveLaterProducts})
          }
          this.scrollTop();
        }).catch(() => this.setState({ products: [] }));
        type != 'categories' && type != 'pageNumber' ? findCategories(records).then(cat => this.setState({ allCategories: cat })).catch(() => this.setState({ allCategories: [] })) : null;
        return await true;
      } else {
        return await false;
      }
    }

    async handleShowcart() {
      this.setState({ showCart: true });
      this.loadSaveForLaterHistory();
      await this.getSalesTax(-1);
    }


    clearForm() {
      this.setState({
        allCategories: false,
        products: false,
        productCount: 0,
        showAllCount: 0,
        productDetail: false,
        subscribedProduct: false,
        subscribedProductId: 0,
        subscribedAmount: false,
        orderFrequency: "1 month",
        showCart: false,
        showSubscribeProduct: false,
        updateSubscribeOrder: false,
        cartSubscribeOrder: false,
        subscribeOrderId: 0,
        showPolicy: false,
        showOrder: false,
        showSubscribeHistory: false,
        showSubscribeOrder: false,
        showFilter: false,
        maxPrice: 10000,
        searchData: { favourites: "false", saveLater: "false", searchText: "", vendors: [], categories: [], price: 0, pageNumber: 1, recPerPage: 25 }
      })
    }

    addFavourite = async (id, fav) => {
      const { products, cart, saveForLater, orders } = this.state;
      const selectedOrder = orders && orders.find((c) => c.items && c.items.find(r => r.id === id));
      const findProduct = products && products.find(_ => _.id === id) || false;
      const findCart = cart && cart.find(_ => _.id === id) || false;
      const findSaveLater = saveForLater && saveForLater.find(_ => _.id === id) || false;
      const findOrders = selectedOrder && selectedOrder['items'] && selectedOrder['items'].find(_ => _.id === id) || false;
      const findCartDetails = findProduct && findCart && findSaveLater ? true : false;
      if (findCartDetails) {
        cart.map(c => {
          if (c.id === id) {
            c.favourite = fav ? false : true;
            return c;
          } else {
            return c;
          }
        })
        this.setState({ cart: cart })
        saveForLater.map(c => {
          if (c.id === id) {
            c.favourite = fav ? false : true;
            return c;
          } else {
            return c;
          }
        })
        this.setState({ saveForLater: saveForLater })
        products.map(p => {
          if (p.id === id) {
            p.favourite = fav ? false : true;
            return p;
          } else {
            return p;
          }
        })
        this.setState({ products: products })
      } else if (findProduct) {
        products.map(p => {
          if (p.id === id) {
            p.favourite = fav ? false : true;
            return p;
          } else {
            return p;
          }
        })
        this.setState({ products: products })
      } else if (findCart) {
        cart.map(c => {
          if (c.id === id) {
            c.favourite = fav ? false : true;
            return c;
          } else {
            return c;
          }
        })
        this.setState({ cart: cart })
      } else if (findSaveLater) {
        saveForLater.map(c => {
          if (c.id === id) {
            c.favourite = fav ? false : true;
            return c;
          } else {
            return c;
          }
        })
        this.setState({ saveForLater: saveForLater })
      } else if (findOrders) {
        orders.map(r => {
          r.items.map(c => {
            if (c.id === id) {
              c.favourite = fav ? false : true;
              return c;
            } else {
              return c;
            }
          })

        })
        this.setState({ orders: orders })
      }
    }

    addToCart = async (item, amount) => {
      const { cart } = this.state;
      this.setState({ cart: addToCart(item, amount, cart) });
    }

    removeFromCart = async (id) => {
      const { cart, notesArr } = this.state;
      const vendorId = cart && cart.length > 0 && cart.find(_ => _.id == id).vendorId || 0;
      const vendorArr = cart && cart.length > 0 && cart.filter(_ => _.vendorId == vendorId) || [];
      if(vendorArr && vendorArr.length == 1) {
        const filteredNotes = notesArr && notesArr.length > 0 && notesArr.filter(_ => _.id != vendorId) || notesArr;
        this.setState({ notesArr: filteredNotes });
      }
      this.setState({ cart: removeFromCart(id, cart) });
      await removeCart(id);
      await this.getSalesTax();
    }

    handleSaveLater = (id) => {
      addSaveLater(id);
      const cartReload = setInterval(() => {
        this.reloadCart();
        clearReloadTimer();
      }, 750);
      const clearReloadTimer = () => clearInterval(cartReload);
      this.setState({ productDetail: false });
    }

    handleVendorNotes = (data, vendor) => {
      const { notesArr } = this.state;
      if(notesArr.length > 0) {
        const filteredNotes = notesArr.filter(_ => _.id != vendor.id);
        var submitRecord = data;
        submitRecord.id = vendor.id;
        const newNotesArr = filteredNotes.concat([submitRecord])
        this.setState({ notesArr: newNotesArr });
      } else {
        var submitRecord = data;
        submitRecord.id = vendor.id;
        this.setState({ notesArr: notesArr.concat([submitRecord]) });
      }
    }

    reloadCart = () => {
      const { cart } = this.state;
      Cart().then(cart => this.setState({ cart: cart }));
    }

    getSubscribeProductSalesTax = async (subproducts) => {
      await findProductSaletax(subproducts ? subproducts : {}).then(resp => { this.setState({ salesTax: resp && resp.data && resp.data[0] && resp.data[0].salesTax ? resp.data[0].salesTax : '0' }) }).catch(() => { this.setState({ salesTax: '0' }) });
    }

    getSalesTax = async (id) => {
      await findSaleTax(id ? id : 0).then(resp => { this.setState({ salesTax: resp && resp.data && resp.data[0] && resp.data[0].salesTax ? resp.data[0].salesTax : '0' }) }).catch(() => { this.setState({ salesTax: '0' }) });
    }

    toggleCart = () => {
      this.setState({ showCart: !this.state.showCart });
    }

    toggleFinance = () => {
      this.setState({ financeInfo: true });
    }

    closeSubscribeProduct = () => {
      this.setState({ updateSubscribeOrder: false, cartSubscribeOrder: false, subscribeOrderId: 0, showSubscribeProduct: false, subscribedSingleList: false, orderFrequency: "1 month"});
      this.reloadCart();
    }

    changeFrequency = (val) => {
      this.setState({ orderFrequency: val });
    }

    toggleSubscribeProduct = (val, amount) => {
      const { products, cart, showSubscribeProduct, updateSubscribeOrder, cartSubscribeOrder, subscribedSingleList,shippingAddress, subscribedList,productsValue  } = this.state;
      const productsList = updateSubscribeOrder == true ? subscribedSingleList : cartSubscribeOrder == true ? cart : products != false ? products : [];
      const filteredProducts = productsList && productsList.find((r) => r.id === val);
      const subproducts = Object.assign({},{addressId:shippingAddress,productId:val,quantity:amount});
      this.setState({productsValue:subproducts})
      const uniqueProducts = updateSubscribeOrder == false ? subscribedList.filter((r) => r.productId === filteredProducts.id) : [];
      if(uniqueProducts.length == 0){
        const filteredProductsObj = filteredProducts;
        filteredProductsObj.amount = amount;
        const filteredProductsArray = _.castArray(filteredProductsObj);
        if(showSubscribeProduct == false){
          this.getSubscribeProductSalesTax(subproducts);
        }
        this.setState({ showSubscribeProduct: true, subscribedProductId: isNaN(val) ? undefined : val, subscribedAmount: isNaN(amount) ? undefined : amount, subscribedProduct: filteredProductsArray });
      } else {
        this.setState({ alertReSubscribeMessage: true });
      }
    }

    toggleCartSubscribeProduct = (val, amount) => {
      const { products, cart, showSubscribeProduct, subscribedList } = this.state;
      const productsList = cart != false ? cart : [];
      const filteredProducts = productsList && productsList.find((r) => r.id === val);
      const uniqueProducts = subscribedList.filter((r) => r.productId === filteredProducts.id);
      if(uniqueProducts.length == 0){
        const filteredProductsObj = filteredProducts;
        filteredProductsObj.amount = amount;
        const filteredProductsArray = _.castArray(filteredProductsObj);
        if(showSubscribeProduct == false){
          this.getSalesTax(-1);
        }
        this.setState({ showSubscribeProduct: true, cartSubscribeOrder: true, subscribedProductId: isNaN(val) ? undefined : val, subscribedAmount: isNaN(amount) ? undefined : amount, subscribedProduct: filteredProductsArray });
      } else {
        this.setState({ alertReSubscribeMessage: true });
      }
    }

    loadSubscribeSingleRecord(id, val, amount, frequency, shippingAddressId) {
      getProductDetail(val)
        .then(r => {
          this.setState({ subscribedSingleList: _.castArray(r) });
          const { products, showSubscribeProduct, subscribedSingleList } = this.state;
          const productsList = _.castArray(r);
          const filteredProducts = productsList && productsList.find((s) => s.id === val);
          const subproducts = Object.assign({},{addressId:shippingAddressId,productId:val,quantity:amount});
          const filteredProductsObj = filteredProducts;
          filteredProductsObj.amount = amount;
          const filteredProductsArray = _.castArray(filteredProductsObj);
          if(showSubscribeProduct == false){
            this.getSubscribeProductSalesTax(subproducts);
          }
          this.setState({ showSubscribeProduct: true, updateSubscribeOrder: true, subscribeOrderId: isNaN(id) ? undefined : id, subscribedProductId: isNaN(val) ? undefined : val, subscribedAmount: isNaN(amount) ? undefined : amount, subscribedProduct: filteredProductsArray, orderFrequency: frequency });
        })
        .catch(() => this.setState({ subscribedSingleList: [] }));
    }

    toggleEditSubscribeProduct = (id, val, amount, frequency, shippingAddressId) => {
      this.loadSubscribeSingleRecord(id, val, amount, frequency, shippingAddressId);
    }

    setOvernight = (vendor, val) =>
      this.setState({ cart: this.state.cart.map(product => String(product.vendorId) === String(vendor) ? { ...product, overnight: val } : product) });

    placeOrder = ({ address, taxtotal, shipamttotal, cardtotal, grandTotal, totalAmountSaved, customerProfileID, customerPaymentProfileId, transId, transactionCode, paymentMethod }) => {
      var dataformat = {
        address: address,
        tax: taxtotal,
        shippingCharge: shipamttotal,
        creditCardFee: cardtotal,
        grandTotal: grandTotal,
        totalAmountSaved: totalAmountSaved,
        customerProfileID: customerProfileID,
        customerPaymentProfileId: customerPaymentProfileId,
        transId: transId,
        transactionCode: transactionCode,
        paymentMethod: paymentMethod,
        vendors: this.state.notesArr
      }

      var cartItems = this.state.cart.filter(_ => _.financeEligible == false);

      if(paymentMethod == 'finance' && cartItems.length > 0) {
        this.setState({ financeAlert: true });
      } else {
        this.props.dispatch(placeOrder(this.state.cart, dataformat));
        if(paymentMethod == "check") {
          this.setState({ alertCheckMessage: true, notesArr: [] });
        } else {
          this.setState({ alertMessage: true, notesArr: [] });
        }
      }
    }

    subscribeOrder = ({ create, successMessage }) => {
      if(create == true) {
        this.setState({ alertSubscribeMessage: true, successText: successMessage});
      } else {
        this.setState({ alertUpdateSubscribeMessage: true, successText: successMessage});
      }
    }

    toggleProductDetailView = (val) => {
      this.setState({ productDetail: isNaN(val) ? undefined : val });
    }

    async clearfilter() {
      await this.handleSearch(0, 'price');
      await this.handleSearch([], 'vendors');
    }

  }

  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
    };
  }


  DentistCatalogPage.propTypes = {
    dispatch: PropTypes.func,
    catalog: PropTypes.array,
    children: PropTypes.any,
  };

  return connect(
    createSelector(
      selectUser(),
      selectors.selectRecords(),
      selectors.selectRecordsMetaData(),
      (user, vendors, metaData) => ({ user, records: vendors ? vendors.filter((v) => !v.deleted).toJS() : [], metaData: metaData && metaData.toJS() || [] }),
    ),
    mapDispatchToProps,
  )(DentistCatalogPage);
}