import React from 'react'
import styles from './styles.css';

export default function (name, path, url) {
  return (props) =>
    <iframe className={styles.helpFrameContent} src={url}/>
};