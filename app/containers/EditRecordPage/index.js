/*
 * EditRecordPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import EditRecordForm from 'components/EditRecordForm';
import EditProductForm from 'components/EditRecordForm/EditProductForm.js';
import { Alert } from '@sketchpixy/rubix';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from '../RecordsPage/messages';
import moment from 'moment';

const SUCCESS_ALERT_TIMEOUT = 4000;

export default function (name, path, columns, updateRecord, selectRecord, selectError, view, updateRecordsMetaData) {
  function handleEdit(data, dispatch, { form }) {
    const { alertMessage, accountAlertMessage } = this.state;
    const record = data.toJS();
    if(name == 'products.edit') {
      record.categoryId = record.childId != undefined ? record.childId : record.parentId != undefined ? record.parentId : null;
      if (record.stockAvailable == '') {
        record.stockAvailable = false
      }
      if (record.financeEligible == '') {
        record.financeEligible = false
      }
      if(record.video && record.video.length > 0) {
        let videoData = record.video.filter(data => data.url == undefined)
        let videoObjData = record.video.filter(data => data.url != undefined)
        if(videoData.length == 2) {
          record.video = videoData.map(url => ({url}))
        } else if(videoData.length == 1) {
          let videoObjData1 = videoData.map(url => ({url}))
          record.video = videoObjData.concat(videoObjData1)
        }
      }
    }
    if(name == 'tutorials.edit') {
      if(record.sectionType == 'section') {
        if(record.parent != undefined) {
          delete record.parent;
        }
        if(record.link != undefined) {
          delete record.link;
        }
      }
      record.parent = record.parent != null ? parseInt(record.parent) : null;
    }
    if(name == 'manuals.edit') {
      if(record.sectionType == 'section') {
        if(record.parent != undefined) {
          delete record.parent;
        }
        if(record.link != undefined) {
          delete record.link;
        }
      }
      record.parent = record.parent != null ? parseInt(record.parent) : null;
    }
    if(name == 'saasAdmin.edit') {
      record.subStartDate = record.subStartDate != null ? moment(record.subStartDate).format("YYYY-MM-DD") : null;
      record.subExpDate = record.subExpDate != null ? moment(record.subExpDate).format("YYYY-MM-DD") : null;
    }
    if (record.taxExempted == '') {
      record.taxExempted = false
    }
    if (record.genericSubstituteOk == '') {
      record.genericSubstituteOk = false
    }
    if(name == 'practices.edit') {
      if (record.appointmentReminder == '' || record.appointmentReminder == null) {
        record.appointmentReminder = false
      }
      if (record.onlineRegistrationModule == '' || record.onlineRegistrationModule == null) {
        record.onlineRegistrationModule = false
      }
    }
    if(name == 'accounts.edit') {
      if (record.prime == '' || record.prime == null) {
        record.prime = false
      }
      if (record.allowAllCECourses == '' || record.allowAllCECourses == null) {
        record.allowAllCECourses = false
      }
      if (record.accessStartDate != '' && record.accessStartDate != undefined) {
        record.accessStartDate = moment(record.accessStartDate).format("YYYY-MM-DD")
      }
      if (record.expirationDate != '' && record.expirationDate != undefined) {
        record.expirationDate = moment(record.expirationDate).format("YYYY-MM-DD")
      }
      if (record.authorizeCustId != '' && record.authorizeCustId != undefined) {
        record.authorizeCustId = record.authorizeCustId.toString()
      } else {
        record.authorizeCustId = "0"
      }
    }

    if(path.indexOf('vendors') > -1) {

      const groups = _.flow([
                       arr => _.groupBy(arr, 'groupId'),
                       g => _.filter(g, o => o.length > 1),
                       _.flatten
                     ])(record.groups.groups);

      if(groups.length > 0) {
        this.setState({ alertMessage: true });
      } else {
        const vendorReload = setInterval(() => {
          dispatch(updateRecord(record, form));
          clearReloadTimer();
        }, 300);
        const clearReloadTimer = () => clearInterval(vendorReload);
        this.onSuccess();
      }
    } else if(name == 'accounts.edit' && record.appSquares && ((record.appSquares.includes("CE10") && record.appSquares.includes("CE5")) || (record.appSquares.includes("CE5") && record.appSquares.includes("CE3")) || (record.appSquares.includes("CE10") && record.appSquares.includes("CE3")))) {
      this.setState({ accountAlertMessage: true });
    } else {
      const vendorReload = setInterval(() => {
        dispatch(updateRecord(record, form));
        clearReloadTimer();
      }, 300);
      const clearReloadTimer = () => clearInterval(vendorReload);
      this.onSuccess();
    }
  }

  class EditRecordPage extends React.Component {

    constructor(props) {
      super(props);
      this.state = { alertMessage: false, accountAlertMessage: false };
    }

    componentWillMount() {
      columns.filter(function (el) { return (el.value == "collectedDate" || el.value == "amountCollected") ? el.editRecord = false : el })
    }

    componentWillUnmount() {
      if (this.successAlertTimeout) {
        clearTimeout(this.successAlertTimeout);
      }
    }


    render() {
      const { record, error } = this.props;
      const initialValues = columns.reduce((acc, el) => {
        acc[el.value] = record[el.value];
        return acc;
      }, {});
      if(name == 'products.edit') {
        const { vendorId, categoryId, metaData } = record;
        initialValues.categoryId = categoryId;
        const categoryData = metaData.vendorCategoryOptions && vendorId != null && metaData.vendorCategoryOptions.length > 0 ? metaData.vendorCategoryOptions.find(_ => _.vendorId == vendorId) : null;
        const vendorCategoryId = categoryData != undefined ? categoryData.categories : null;
        const categoryIdArray = vendorCategoryId != null ? vendorCategoryId.split(',').map((x) =>parseInt(x)) : null;
        const filteredCategories = metaData.categoriesList && metaData.categoriesList.length > 0 && categoryIdArray && categoryIdArray.length > 0 ? metaData.categoriesList.filter(val => categoryIdArray.includes(val.id)) : [];
        let childMetaData = metaData;
        let childArray = childMetaData && childMetaData.categoryChildOptions && childMetaData.categoryChildOptions.filter(_ => _.value == categoryId)
        let parentArray = childMetaData && childMetaData.categoryParentOptions && childMetaData.categoryParentOptions.filter(_ => _.value == categoryId)
        if(childArray && childArray.length > 0) {
          initialValues.parentId = childArray[0].parent;
          initialValues.childId = childArray[0].value;
          initialValues.filteredCategories = filteredCategories;
        } else {
          initialValues.parentId = parentArray[0].value;
          initialValues.childId = null;
          initialValues.filteredCategories = filteredCategories;
        }
      }
      const { success, alertMessage } = this.state;
      const recordPath = view ? `${path}/${record.id}` : path;
      return (
        <div>
          {name === 'products.edit' ?
            <EditProductForm
              name={name}
              path={recordPath}
              initialValues={initialValues}
              fields={columns}
              form={`editRecord.${record.id}`}
              onSubmit={handleEdit.bind(this)}
              metaData={record.metaData}
              locationState={record.locationState}
              updateRecordsMetaData={updateRecordsMetaData}
              record={record}
            /> :
            <EditRecordForm
              name={name}
              path={recordPath}
              initialValues={initialValues}
              fields={columns}
              form={`editRecord.${record.id}`}
              onSubmit={handleEdit.bind(this)}
              metaData={record.metaData}
              locationState={record.locationState}
              updateRecordsMetaData={updateRecordsMetaData}
              record={record}
            /> }
          {success && !error ?
            <Alert success>Your changes have been saved successfully!</Alert> : null}
          {error ?
            <Alert danger>{error}</Alert> : null}
          {this.state.alertMessage ?
            <AlertMessage warning dismiss={() => { this.setState({ alertMessage: false }) }}><FormattedMessage {...messages.groupAlert} /></AlertMessage> : null}
          {this.state.accountAlertMessage ?
            <AlertMessage warning dismiss={() => { this.setState({ accountAlertMessage: false }) }}><FormattedMessage {...messages.accountAlert} /></AlertMessage> : null}
        </div>
      );
    }

    onSuccess() {
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ success: true });
        if(name == 'tutorials.edit' || name == 'manuals.edit') {
          setTimeout(() => {
            columns.map((field) => {
              if(field.value == 'name' || field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
                field.editRecord = false
              }
            })
          }, 1500)
        }
      }, 500);
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ success: false });
      }, SUCCESS_ALERT_TIMEOUT);
    }
  }

  function mapStateToProps(state, props) {
    let id = props.params.id;
    if (path.indexOf('consents') === -1) {
      id = parseInt(id, 10)
    }
    const selectedRecord = selectRecord(id, 10)(state, props);
    const updateError = selectError()(state, props);
    let record = selectedRecord ? selectedRecord.toJS() : {};
    Object.assign(record, { locationState: props.location.state });
    return {
      record: record,
      error: updateError,
    };
  }

  EditRecordPage.propTypes = {
    record: PropTypes.object,
    error: PropTypes.string,
  };

  return connect(mapStateToProps)(EditRecordPage);
}
