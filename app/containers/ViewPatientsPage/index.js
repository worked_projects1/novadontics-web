/*
 * ViewPatientsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { Row, Col, Panel, PanelContainer, Tab, Tabs, PanelHeader, Label as InfoLabel, Icon } from '@sketchpixy/rubix';
import { selectUser } from 'blocks/session/selectors';

import Label from 'components/Label';
import Gallery from 'components/Gallery';

import Checkbox from './components/Checkbox.js';
import DentistInfo from './components/DentistInfo.js';
import LoadingState from './components/LoadingState.js';
import { parseStructure } from './utils.js';
import styles from './styles.css';
import { Player } from 'video-react';
import "video-react/dist/video-react.css";
import DeleteActionLink from "../../components/DeleteActionLink";

const DUPLICATE_TITLE_IN_SCHMEA = '1D Immediate Loading Risk Factors';

export default function (path, columns, updateRecord, { selectRecord, selectFormSchema }, actions) {
  function getCompletedHashMap(steps) {
    return steps.reduce((hash, step) => Object.assign(hash, { [step.formId]: step.state }), {});
  }

  class ViewPatientsPage extends React.Component {

    static propTypes = {
      record: PropTypes.object,
      dispatch: PropTypes.func,
      params: PropTypes.object,
      user: PropTypes.object,
      formSchema: PropTypes.array,
    };

    shouldComponentUpdate(nextProps, nextState) {
      return shallowCompare(this, nextProps, nextState);
    }

    render() {
      const { record = {}, formSchema, user } = this.props;
      const { recordDidNotInit, id, dentistName, patientName, steps: stepsRoot = { }, dentist = {} } = record;
      const { practice, phone: dentistPhone, email: dentistEmail } = dentist;
      const { steps } = stepsRoot;
      const pdfAuthToken = user && user.secret || user && user.token;
      if (formSchema.length === 0 || recordDidNotInit || !steps) {
        return <LoadingState path={path} />;
      }
      const completedHashMap = getCompletedHashMap(steps);
      return (
        <div className="catalogView">
          <Row>
            <Col xs={12}>
              <PanelContainer>
                <Panel>
                  <PanelHeader>
                  {path ?
                    <Row>
                      <Col sm={12} className={styles.headerActions}>
                        <div className="closeDetailViewButton">
                          <Link to={`${path}`}>Close</Link>
                        </div>
                        <div>
                          <DeleteActionLink deleteRecord={actions.deleteRecord} record={record}/>
                        </div>
                      </Col>
                    </Row> : null}
                    <Row>
                      <Col sm={12}>
                        <h2 className={styles.headTitle}>{patientName} #{id}</h2>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={6}>
                        <DentistInfo
                          wrapperClass={styles.info}
                          dentistInfo={{ dentistName, practice, dentistPhone, dentistEmail }}
                        />
                      </Col>

                      <Col sm={6}>
                        <div className={styles.pdf}>
                          {pdfAuthToken ? 
                          <a href={`${process.env.API_URL}/treatments/${id}/pdf?X-Auth-Token=${pdfAuthToken}`} target="_blank">
                            <Icon glyph="icon-fontello-doc-text" /> View PDF
                          </a> : null}
                        </div>
                      </Col>
                    </Row>
                  </PanelHeader>
                </Panel>
              </PanelContainer>

              <Tabs defaultActiveKey={0} id="ViewConsultationPageTabs">
                {formSchema ? formSchema.map((section, i) =>
                  <Tab key={`section${section.title}`} eventKey={i} title={section.title} className={styles.info}>
                    {section.forms.map((form) =>
                      <Row key={form.id}>
                        <Col xs={12}>
                          <h4 className={styles.subtitle}>
                            {form.title}
                          </h4>
                          <p>
                            {completedHashMap[form.id] && completedHashMap[form.id] === 'Completed' ?
                              <InfoLabel bsStyle="success">Completed</InfoLabel> :
                              <InfoLabel bsStyle="warning">In Progress</InfoLabel>
                            }
                          </p>
                        </Col>

                        {completedHashMap[form.id] ? form.schema.map((schemaItem) =>
                          <Col xs={12} key={`schemaItem-${schemaItem.title}`}>
                            { form.title !== schemaItem.title &&
                              schemaItem.title !== DUPLICATE_TITLE_IN_SCHMEA ?
                              <h4>{schemaItem.title}</h4> : null}

                            {schemaItem.rows.map((row) =>
                                row.columns.map((column) =>
                                  column.columnItems.map((columnItem, columnIndex) =>
                                    <div key={`columnItem-${columnIndex}`}>
                                      {columnItem.field && columnItem.field.type ?
                                        this.renderSimpleView(columnItem.field, steps.find((step) => step.formId === form.id), false, columnIndex) :
                                        this.renderGroup(columnItem, steps.find((step) => step.formId === form.id))}
                                    </div>
                                  )
                                )
                            )}
                          </Col>) :
                          <Col xs={12} key={`nodata-${form.id}`}>
                            <p className={styles.noDataLabel}>No data yet.</p>
                          </Col>}

                        <Col xs={12} key={`hrRule-${form.id}`}>
                          <hr className={styles.hrRule} />
                        </Col>
                      </Row>
                    )}
                  </Tab>
                ) : null}
              </Tabs>
            </Col>
          </Row>
        </div>
      );
    }

    renderSimpleView(field, step = {}, groupMember = false, renderIndex) {
      const { type, name, title, attributes, postfix } = field;
      const { value = {} } = step;

      if (!value[name]) {
        return null;
      }

      let parsedStruct = {};
      if (type === 'Images' || type === 'Checkbox' || type === 'Videos') {
        parsedStruct = parseStructure(value[name]) || {};
      }

      switch (type) {
        case 'Input':
          return (
            <Col xs={12} className={styles.spaceItems} key={`input-${name}-${renderIndex}`}>
              <Label data={{ label: title, value: postfix ? `${value[name]} ${postfix}` : value[name] }} />
            </Col>
          );
        case 'Images':
          return (
            <Col xs={12} className={styles.spaceItems} key={`image-${name}-${renderIndex}`}>
              <Label data={{ label: title, value: '' }} />
              <Gallery data={{ images: parsedStruct && parsedStruct.images ? parsedStruct.images : [] }} />
            </Col>
          );
        case 'Checkbox':
          return <Checkbox key={`checkbox-${name}-${renderIndex}`} struct={parsedStruct} groupMember={groupMember} title={title} selectOptions={attributes ? attributes.options : []} styles={styles} />;
        case 'Select':
          return (
            <Col xs={12} className={styles.spaceItems} key={`select-${name}-${renderIndex}`}>
              <Label data={{ label: title, value: value[name] }} />
            </Col>
          );
        case 'Videos':
          if (parsedStruct.videos) {
            return (
              parsedStruct.videos.map((video, i) =>
                <Col xs={6} className={styles.spaceItems} key={`video-${i}`}>
                  <Player src={video.url} poster={video.thumbnail} fluid />
                </Col>
              )
            );
          }
          return null;
        default:
          return null;
      }
    }

    renderGroup(field = {}, step = {}) {
      const { value = {} } = step;
      const { group = {} } = field;
      const { fields } = group;
      if (group && fields && value) {
        return (
          <Col xs={12} key={`group-${group.title}`} className={styles.spaceItems}>
            <Label data={{ label: group.title, value: '' }} />
            {fields.map((simpleField, simpleIndex) => this.renderSimpleView(simpleField, step, true, simpleIndex))}
          </Col>
        );
      }
      return null;
    }

  }



  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const formSchemaRecord = selectFormSchema('patients')(state, props);
    const user = selectUser()(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : { recordDidNotInit: true },
      formSchema: formSchemaRecord ? formSchemaRecord.toJS() : [],
      user,
    };
  }

  return connect(mapStateToProps)(ViewPatientsPage);
}
