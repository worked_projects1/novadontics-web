export function parseStructure(str = '') {
  return JSON.parse(str.replace(/★/g, '"'));
}
