/*
 * Shows a Consultations Checkbox with optional notes, image gallery and info.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Icon } from '@sketchpixy/rubix';
import Gallery from 'components/Gallery';
import shallowCompare from 'react-addons-shallow-compare';

class ConsultationCheckbox extends React.Component {

  static propTypes = {
    struct: PropTypes.shape({
      checked: PropTypes.bool,
      selectedOption: PropTypes.string,
      images: PropTypes.array,
      notes: PropTypes.string,
    }),
    selectOptions: PropTypes.array,
    styles: PropTypes.object,
    title: PropTypes.string,
    groupMember: PropTypes.bool,
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { title, selectOptions = [], styles = {}, struct = {}, groupMember } = this.props;
    const { checked, selectedOption, images, notes } = struct;
    const wrapperClass = groupMember ? '' : styles.spaceItems;
    const selected = selectOptions.find((o) => o.value === selectedOption);

    if (checked) {
      return (
        <Col xs={12} className={wrapperClass} >
          <p><Icon bundle="fontello" glyph="ok-circle" className={styles.icon} /> <strong>{title}</strong></p>
          {selectedOption && selected ?
            <p><strong>Info:</strong> <span>{selected.title}</span></p> : null}
          {notes && notes.length ?
            <p> <strong>Notes: </strong> {`${notes}`} </p> : null}
          {images ? <Gallery data={{ images }} /> : null}
        </Col>
      );
    }
    return null;
  }

}

export default ConsultationCheckbox;
