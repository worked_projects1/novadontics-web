/*
 * Shows dentist info - practice, name, phone, email.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';

class DentistInfo extends React.Component {

  static propTypes = {
    wrapperClass: PropTypes.string,
    dentistInfo: PropTypes.shape({
      dentistName: PropTypes.string,
      practiceName: PropTypes.string,
      dentistPhone: PropTypes.string,
      dentistEmail: PropTypes.string,
    }),
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { wrapperClass, dentistInfo = {} } = this.props;
    const { dentistName = '', practiceName = '', dentistPhone = '', dentistEmail = '' } = dentistInfo;
    return (
      <Row className={wrapperClass}>
        <Col xs={12}>
          <strong>{dentistName}</strong>
          <p>{practiceName}</p>
          <p>{dentistPhone}</p>
          <p>{dentistEmail}</p>
        </Col>
      </Row>
    );
  }

}

export default DentistInfo;
