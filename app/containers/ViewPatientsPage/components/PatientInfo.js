/*
 * Shows patient info - practice, name, phone, email.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';

class PatientInfo extends React.Component {

  static propTypes = {
    wrapperClass: PropTypes.string,
    patientInfo: PropTypes.shape({
      patientId: PropTypes.string,
      patientName: PropTypes.string,
      phoneNumber: PropTypes.string,
      emailAddress: PropTypes.string,
    }),
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { wrapperClass, patientInfo = {} } = this.props;
    const { patientId = '', patientName = '', phoneNumber = '', emailAddress = '' } = patientInfo;
    return (
      <Row className={wrapperClass}>
        <Col xs={12}>
          <p>#{patientId}</p>
          <p>{patientName}</p>
          <p>{phoneNumber}</p>
          <p>{emailAddress}</p>
        </Col>
      </Row>
    );
  }

}

export default PatientInfo;
