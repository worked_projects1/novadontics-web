/*
 * Shows a Loading State
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row, Panel, PanelContainer, PanelHeader } from '@sketchpixy/rubix';
import Spinner from 'components/Spinner';
import { Link } from 'react-router';

class LoadingState extends React.Component {

  static propTypes = {
    path: PropTypes.string,
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.path !== this.props.path;
  }

  render() {
    const { path } = this.props;
    return (
      <div>
        <Row>
          <Col xs={12}>
            <PanelContainer>
              <Panel>
                <PanelHeader>
                  {path ?
                    <Row>
                      <Col sm={12}>
                        <div className="closeDetailViewButton">
                          <Link to={`${path}`}>Close</Link>
                        </div>
                      </Col>
                    </Row> : null}
                </PanelHeader>
                <Spinner />
              </Panel>
            </PanelContainer>
          </Col>
        </Row>
      </div>
    );
  }

}

export default LoadingState;
