/*
 * EditPatientPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import EditPatientForm from '../PatientsPage/components/EditPatientForm';
import Alert from 'components/Alert';
import { isEmpty } from 'lodash';
import Spinner from 'components/Spinner';

import { parseStructure } from '../ViewPatientsForm/utils.js';
import { selectUser } from 'blocks/session/selectors';

import moment from 'moment';

const SUCCESS_ALERT_TIMEOUT = 4000;

export default function (name, path, columns, updateRecord, { selectRecord, selectFormSchema }, selectError, view, actions, selectExternalRecords) {
  function handleEdit(data, dispatch, { form }) {
    const { formId } = this.props.params;
    const [ externalRecords = {} ] = this.props.externalRecords;
    
    const submitRecord = data.toJS();
    const { patient = {}, steps } = submitRecord;
    const record = {};
    /*
    Creating API input
    **/
    
    record.id = submitRecord.id;  
    record.emailAddress = submitRecord.email ? submitRecord.email : patient.email;
    record.phoneNumber = submitRecord.phone ? submitRecord.phone : patient.phone;
    record.patientId = patient.patientId;
    record.steps = steps;
    record.steps[0]['state'] = 'Completed';
    delete submitRecord.patient; //deleting patient
    delete submitRecord.id; //deleting id
    delete submitRecord.steps; //deleting steps
    //delete submitRecord.state;//deleting state
    /*
    Creating steps API input
    **/
  
    const stepValues = Object.keys(submitRecord).length > 0 ? 
    Object.keys(submitRecord)
    .filter(key => key)
    .reduce((obj, key) => {
      obj[key] = (typeof(submitRecord[key]) === 'object') ? JSON.stringify(submitRecord[key]).replace(/"/g,'★').replace(/\//g,"\\/") : submitRecord[key];
      return obj;
    }, {}) : {};
    record.steps[0]['value'] = stepValues;

    if(formId === '1A' && this.props.user.xvWebURL != '' && !isEmpty(externalRecords)){
      const { full_name, last_name, gender, birth_date } = submitRecord;
      dispatch(actions.updateExternalRecord({Id: externalRecords.Id, name: `${last_name}^${full_name}`,firstName: full_name, lastName: last_name, gender:  gender && gender === 'male' ? 'M' : gender && gender === 'female' ? 'F' : '', birthdate: birth_date && isNaN(Date.parse(birth_date)) ? '' : birth_date && birth_date != null ? moment.utc(birth_date).format('YYYY-MM-DD') : ''}));
    }
    dispatch(updateRecord(record, form));
  }

  class EditPatientPage extends React.Component {

    constructor(props) {
      super(props);
      this.state = { success : false, showError : false};
    }

    componentDidMount() {
      document.getElementById("app").style.overflow = "hidden";
      document.getElementById("body").firstChild.style.overflow = "hidden";
    
      if(selectExternalRecords){
        const interval = setInterval(() => {
          const { record = {} } = this.props;
          const { patient= {} } = record;
          const { name, last_name, birthDate, gender } = patient;
          if(name && last_name && birthDate && gender && this.props.user.xvWebURL != ''){
            this.props.dispatch(actions.loadExternalRecords({firstName: name, lastName: last_name, gender:  gender && gender === 'male' ? 'M' : gender && gender === 'female' ? 'F' : '', birthdate: birthDate && isNaN(Date.parse(birthDate)) ? '' : birthDate && birthDate != null ? moment.utc(birthDate).format('YYYY-MM-DD') : ''})); 
            clearTimer();
          }  
        }, 300);
        const clearTimer = () => clearInterval(interval);
      }
    }

    componentWillUnmount() {
      if (this.successAlertTimeout) {
        clearTimeout(this.successAlertTimeout);
      }
      document.getElementById("app").style.overflow = "initial";
      document.getElementById("body").firstChild.style.overflow = "initial"; 
    }

    render() {
      const { record = {}, location = {}, error, formSchema, dispatch, user } = this.props;
      const { id } = this.props.params;
      const { recordDidNotInit, steps: stepsRoot = { }, patient = {} } = record;
      const { accessLevel, shared } = patient;
      const viewOnly = accessLevel == 'View' ? true : false
      const { steps } = stepsRoot;
      const { formId } = this.props.params;  
      
      const { success, showError } = this.state;
      const recordPath = view ? `${path}/${record.id}` : path;
      const pdfAuthToken = user && user.secret || user && user.token;
      
      if (formSchema.length === 0 || !steps || !pdfAuthToken || recordDidNotInit) {
        return <Spinner />;
      }

      return (
        <div>
          <EditPatientForm
            id={record.id}
            name={name}
            path={recordPath}
            initialValues={getInitialValues(record, steps, formId, formSchema, id)}
            formSchema={formSchema}
            form={`editPatient.${record.id}`}
            onSubmit={handleEdit.bind(this)}
            metaData={record.metaData}
            onSubmitSuccess={this.onSuccess.bind(this)}
            formId={this.props.params.formId}
            locationState={location.state}
            dispatch={dispatch}
            printUrl={`${process.env.API_URL}/treatments/${id}/pdf?X-Auth-Token=${pdfAuthToken}`}
            disable={viewOnly}
            shared={shared}
            platform={user && user.platform}
            record={record}
          />
          {error && showError ?
            <Alert warning dismiss={()=>this.setState({showError:false})}>{error}</Alert> : null}
          {success && !error ?
            <Alert success>Your changes have been saved successfully!</Alert> : null}
        </div>
      );
    }

    onSuccess() {
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ showError: true  });
      }, 500);
      this.successAlertTimeout = setTimeout(() => {
        this.setState({ success: false });
      }, SUCCESS_ALERT_TIMEOUT);
    }
  }


   
  function getInitialValues(record, steps, formId, formSchema, patientId) {
    const { patient = {} } = record;
    const stepValues = steps && steps.length > 0 ? steps.filter(c => c.formId == formId).reduce((obj, c) => Object.assign(obj, c), {}) : {};
    let schemaValues;
    formSchema.map((form) => {
        form['forms'].map((schema, index) => {
          if(form['forms'][index]['id'] == formId)
          {
            schemaValues = form['forms'][index];
          }
        }) 
    })
    const parsedValues = Object.keys(stepValues).length > 0 ? 
    Object.keys(stepValues.value)
    .filter(key => key)
    .reduce((obj, key) => {
      obj[key] = parseStructure(stepValues['value'][key]);
      return obj;
    }, {}) : {};
    parsedValues.patient = patient;
    parsedValues.id = record.id;
    parsedValues.steps = [{formId:stepValues.formId ? stepValues.formId : schemaValues.id,
                          formVersion: stepValues.formVersion ? stepValues.formVersion : schemaValues.version}];
    if(formId === '1A' && (!parsedValues.patient_id || parsedValues.patient_id == '')){
      parsedValues.patient_id = patientId;
    }  
    return parsedValues;
  }

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    const updateError = selectError()(state, props);
    const formSchemaRecord = selectFormSchema('patients')(state, props);
    const user = selectUser()(state, props);
    const externalRecords = user && user.xvWebURL && user.xvWebURL != '' && selectExternalRecords && selectExternalRecords()(state,props) || false;
    return {
      record: selectedRecord ? selectedRecord.toJS() : { recordDidNotInit: true },
      error: updateError,
      formSchema: formSchemaRecord ? formSchemaRecord.toJS() : [],
      user,
      externalRecords: externalRecords ? externalRecords.toJS() : []
    };
  }


  EditPatientPage.propTypes = {
    record: PropTypes.object,
    error: PropTypes.string,
    dispatch: PropTypes.func,
    formSchema: PropTypes.array,
    user: PropTypes.object,
  };

  return connect(mapStateToProps)(EditPatientPage);
}