/*
 * ViewSettingsPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import AddEmailForm from 'components/AddEmailForm';
import styles from './styles.css';

export default function (path, columns, addUser, removeUser, selectRecord) {
  class ViewSettingsPage extends React.Component {
    static propTypes = {
      record: PropTypes.object,
      dispatch: PropTypes.func,
    };

    static handleDelete(userId, e) {
      e.preventDefault();
      const { dispatch, record } = this.props;
      dispatch(removeUser(record, userId));
    }

    static handleAdd(data, dispatch /** 3rd arg => { form } */) {
      const { record } = this.props;
      const { userOptions } = record.metaData || {};
      const user = userOptions.filter((u) => u.name === data.toJS().user)[0];
      dispatch(addUser(record, user));
    }

    constructor(props) {
      super(props);
      this.handleDelete = ViewSettingsPage.handleDelete.bind(this);
      this.handleAdd = ViewSettingsPage.handleAdd.bind(this);
    }

    render() {
      const { record } = this.props;
      const { users: thisUsers = [] } = record;
      const { userOptions } = record.metaData || {};

      const thisUserIds = thisUsers.map((u) => u.id);
      const oneOf = userOptions.filter((u) => thisUserIds.indexOf(u.id) === -1).map((u) => u.name);


      return (
        <div className={styles.ViewSettingsPage}>
          <div className={styles.actions}>
            <div className="closeDetailViewButton">
              <Link to={`${path}`}>Close</Link>
            </div>
            <Link to={`${path}/${record.id}/edit`}>Edit</Link>
          </div>
          {columns.map((column) =>
            (column.viewRecord ?
              <div key={column.id}>
                <div>
                  <div className={styles.label}>{column.label}:</div>
                  <p>{record[column.value]}</p>
                  <hr />
                </div>
              </div>
              : null)
          )}
          <div className={styles.label}>FORWARD ADDRESSES</div>
          <div className={styles.list}>
            {record && record.users ? record.users.map((user) => {
              const { id, name, email } = user;
              return (
                <div key={id} className={styles.row}>
                  <div className={styles.lCol}>
                    <p><Link to={`users/${id}`}>{name}</Link>{`<${email}>`}</p>
                  </div>
                  <div className={styles.rCol}>
                    <Link to={`${path}/${record.id}`} onClick={this.handleDelete.bind(this, id)}>
                      Delete
                    </Link>
                  </div>
                </div>
              );
            }) : null}

            {(oneOf && oneOf.length) ?
              <AddEmailForm
                onSubmit={this.handleAdd}
                data={{ oneOf, value: 'user', label: 'user' }}
                initialValues={{ user: oneOf[0].toString() }}
              /> :
              <label>No emails to add</label>}
          </div>
        </div>
      );
    }
  }

  function mapStateToProps(state, props) {
    const selectedRecord = selectRecord(parseInt(props.params.id, 10))(state, props);
    return {
      record: selectedRecord ? selectedRecord.toJS() : {},
    };
  }

  return connect(mapStateToProps)(ViewSettingsPage);
}
