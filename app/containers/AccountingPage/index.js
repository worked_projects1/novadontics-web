/*
 * AccountingPage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';

import { Row, Grid, Col } from '@sketchpixy/rubix';
import styles from './styles.css';

import { selectUser } from 'blocks/session/selectors';
import { Collapse } from 'antd';
import Reports from './Reports';
import moment from 'moment-timezone';
import ModalForm from 'components/ModalRecordForm';

const { Panel } = Collapse;
export default function (name, path, columns, accountingFormColumns, sagas) {
    const viewIcons = (routes, token) => {
        const handleTotalCollectedAmount = (data, type) => {
            const record = data.toJS();
            const { fromDate = moment().format('YYYY-MM-DD'), toDate = moment().format('YYYY-MM-DD') } = record;
            window.open(`${process.env.API_URL}/${routes}${type}?X-Auth-Token=${token}&fromDate=${moment(fromDate).format('YYYY-MM-DD')}&toDate=${moment(toDate).format('YYYY-MM-DD')}`, '_blank')
        }

        return accountingFormColumns[routes] ? (<Col className={styles.formRow}>
            <ModalForm
                fields={accountingFormColumns[routes]}
                form={`totalCollectedAmountRecord`}
                onSubmit={(data)=> handleTotalCollectedAmount(data, 'CSV')}
                config={{
                    title:"Total Collected Amount", btnType:"image", imageIcon: [require(`img/patients/csv.svg`)],width: '28px',height:'28px',btnName: 'Print', style: {marginRight: '3px', marginTop: '3px'}
                }}
            /> 
            <ModalForm
                fields={accountingFormColumns[routes]}
                form={`totalCollectedAmountRecord`}
                onSubmit={(data)=> handleTotalCollectedAmount(data, 'PDF')}
                config={{
                    title:"Total Collected Amount", btnType:"image", imageIcon: [require(`img/patients/pdf1.svg`)],width: '35px',height:'35px',btnName: 'Print', style: {marginRight: '0px'}
                }}
            /> 
        </Col>) : (<Col>
            <Link to={`${process.env.API_URL}/${routes}CSV?X-Auth-Token=${token}`} target="_blank">
                <img src={require('img/patients/csv.svg')} className={styles.csv} onClick={(e)=> e.stopPropagation()} />
            </Link>
            <Link to={`${process.env.API_URL}/${routes}PDF?X-Auth-Token=${token}`} target="_blank">
                <img src={require('img/patients/pdf1.svg')} className={styles.pdf} onClick={(e)=> e.stopPropagation()} />
            </Link>
        </Col>)
    };

    class AccountingPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

        static propTypes = {
            records: PropTypes.array,
            children: PropTypes.object,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool
        };

        constructor(props) {
            super(props);
            this.state = { activeKeys: [], submitRecord: false};
            this.loadRecords = this.loadRecords.bind(this);
        }

        handleFormSubmit(data, dispatch, { form }) {
            const submitRecord = data.toJS();
            this.setState({submitRecord: submitRecord})
            this.loadRecords(this.state.activeKeys, submitRecord, true)
        }

        shouldComponentUpdate(nextProps, nextState) {
            return shallowCompare(this, nextProps, nextState);
        }

        render() {
            const { user = {} } = this.props;
            const { secret } = user;
            let displayName = name;
            if(name === 'accounting'){
                displayName = 'accounting reports';
            }
            return (
                <div>
                    <Helmet title="Novadontics" />
                    <div className={styles.view}>
                        <Grid>
                            <Row>
                                <Col sm={12} className={styles.flexibleRow}>
                                    <h1 className={styles.titleHead}>{displayName}</h1>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={12} className={styles.timeline}>
                                    <Collapse onChange={this.loadRecords.bind(this)} expandIconPosition={'left'}>
                                        {Object.keys(columns).map((column, index) => {
                                            let header = column;
                                            if (column === 'outstandingInsurance') {
                                                header = "Report of outstanding balance from Insurance";
                                            } else if (column === 'PatientBalance') {
                                                header = "Report of outstanding balance from patients";
                                            } else if (column === 'totalCollectedAmount') {
                                                header = "Report of Total collected amount for a time period";
                                            }

                                            const { name, actions, selectors } = sagas[index] || {};

                                            return <Panel header={header} key={index} extra={viewIcons(name, secret)}>
                                                <Reports
                                                    name={name}
                                                    path={path}
                                                    columns={columns[name]}
                                                    formColumns={accountingFormColumns[name]}
                                                    actions={actions}
                                                    selectors={selectors}
                                                    handleFormSubmit={this.handleFormSubmit.bind(this)}
                                                    submitRecord={this.state.submitRecord}
                                                    {...this.props} />
                                            </Panel>
                                        })}
                                    </Collapse>
                                </Col>
                            </Row>
                        </Grid>
                    </div>
                </div>
            );
        }

        loadRecords(activeKeys, submitRecord, invalidateCache) {
            const { dispatch } = this.props;
            this.setState({activeKeys: activeKeys});
            activeKeys.map(key => {
                const { name, actions } = sagas[key] || {};
                dispatch(accountingFormColumns[name] ? actions.loadRecordsParams(submitRecord || accountingFormColumns[name].reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{}), invalidateCache) : actions.loadRecords());
            })
        }


    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    return connect(
        createSelector(
            selectUser(),
            (user) => ({ user })
        ), mapDispatchToProps)(AccountingPage);
}
