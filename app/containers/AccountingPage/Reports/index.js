import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Alert } from '@sketchpixy/rubix';
import ReactTable from "react-table";
import { FormattedMessage } from 'react-intl';
import messages from 'containers/ViewRecordPage/messages';
import Spinner from 'components/Spinner';
import EditRecordForm from 'components/EditRecordForm';
import moment from 'moment-timezone';

class Reports extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { name, path, error, loading, records, columns, formColumns, handleFormSubmit, submitRecord } = this.props;

        if (loading) {
            return <Spinner />
        }

        const data = records && records.length > 0 ? records.concat([Object.assign({}, {totalColumn: true}, records.reduce((a, el)=>Object.assign({}, a, Object.keys(a).reduce((k, m) => Object.assign({}, k, {[m] : a[m] + el[m]}), {})), columns.filter(_ => _.currency).reduce((a, el) => Object.assign({}, a, {[el.value]: 0}), {})))]) : records;
        
        return <Col>
            {error ?
                <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> : records ?
                    <Col>
                        {formColumns ? <Row><Col sm={6}><EditRecordForm
                            initialValues={submitRecord || formColumns.reduce((a, el)=> Object.assign({}, a, el.type === 'date'? {[el.value]:moment().format('YYYY-MM-DD')} : {}),{})}
                            name={name}
                            fields={formColumns}
                            form={`accountingReportForm`}
                            onSubmit={handleFormSubmit.bind(this)}
                            path={path}
                            disableCancel={true}
                            btnName={'Done'}
                        /> </Col></Row> : null}
                        <ReactTable
                            columns={columns.map((column) => Object.assign(
                                {},
                                { Header: column.label, accessor: column.value, width: column.width, ...column }
                            ))}
                            data={data}
                            getTdProps={(state, rowProps) => {
                                return {
                                    style: rowProps && rowProps.original.totalColumn ? { borderTop: '2px solid gray', borderBottom: '2px solid gray', fontWeight: 'bold'} : {}
                                }
                            }}
                            pageSize={data && data.length || 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            showPagination={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                        />
                    </Col> : null}
        </Col>
    }
}

function mapStateToProps(state, props) {
    const { selectors = {} } = props;
    const { selectRecords, selectError, selectLoading } = selectors;
    let records = selectRecords()(state, props);
    const error = selectError()(state, props);
    const loading = selectLoading()(state, props);
    return {
        records: records.toJS() || false,
        error: error,
        loading: loading
    }
}

export default connect(mapStateToProps)(Reports);