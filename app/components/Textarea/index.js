/**
*
* Textarea
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';

import styles from './styles.css';

const renderTextareaField = (props) => {
  const { input, data = {} } = props;
  return <textarea name={data.mode === 'inline' ? data.name : `${data.name}.notes`} className={data.changeStyle ? styles.textAreaNotes : styles.textarea} defaultValue={input.value} type="input" onBlur={(e) => {input.onChange(e.target.value)
    if(typeof props.inputchange !== "undefined" && props.data.onChange){
      props.inputchange(e.target.value, data.value);
    }
  }} />
}

class Textarea extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };

  render() {
    const { data, inline, options = {}, optionsMode, infostyle } = this.props;
    const { required } = options.general || {};
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={12} className={infostyle ? styles.info : null}>
        {!inline ?
          <label htmlFor={data.label}>
            {data.label}
            {required || extraFieldOptions.required ? <span>*</span> : null}
          </label> : null}
        <div style={infostyle ? {width: '75%'} : null}>
          <Field name={data.value} id={data.label} maxLength={data.maxLength} component={data.renderTextarea ? renderTextareaField : "textarea"} {...this.props} disabled={data.disabled} {...options.general} {...extraFieldOptions} />
          {data.addNote ? <Col><b>Note:</b> {"Please use the placeholders <<patient name>>, <<link>> to populate the respective values."}</Col> : data.addAptNote ? <Col><b>Note:</b> {"Please use the placeholders <<practice name>>, <<patient name>>, <<date>>,<<time>>, <<link>> to populate the respective values."}</Col> : data.addConsentNote ? <Col><b>Note:</b> {"Please use the placeholders <<patient name>>, <<form name>>, <<link>> to populate the respective values."}</Col> : data.addRecallNote ? <Col><b>Note:</b> {"Please use the placeholders <<patient name>>, <<interval>>, <<practiceName>>, <<practicePhone>> to populate the respective values."}</Col> : null}
        </div>
      </Col>
    );
  }
}

export default Textarea;
