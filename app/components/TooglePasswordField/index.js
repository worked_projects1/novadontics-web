/**
 *
 * TogglePasswordField
 *
 */

import React from 'react';
import { Field } from 'redux-form/immutable';
import styles from './styles.css';

import { Button } from '@sketchpixy/rubix';

class TogglePasswordField extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.handleClickShowPassword = ::this.handleClickShowPassword;

    this.state = {
      inputType: 'password',
      label: 'Show password',
    };
  }

  render() {
    const { inputType, label } = this.state;

    return (
      <div>
        <Field name="secret" component="input" type={inputType} className={styles.inputField} required />
        <Button bsStyle="link" bsSize="xs" className={styles.button} onClick={this.handleClickShowPassword}>{label}</Button>
      </div>
    );
  }

  handleClickShowPassword() {
    const { inputType } = this.state;

    if (inputType === 'password') {
      this.setState({
        inputType: 'text',
        label: 'Hide password',
      });
    } else {
      this.setState({
        inputType: 'password',
        label: 'Show password',
      });
    }
  }
}

export default TogglePasswordField;
