import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Col, Alert } from '@sketchpixy/rubix';
import styles from './styles.css';
import remotes from 'blocks/fileupload/remotes.js';
import { capitalize } from '../../containers/PatientsPage/components/ImageUpload/uploadUtils';
import { Map as iMap } from "immutable";
import Gallery from '../../containers/PatientsPage/components/Gallery';

const entityNames = {
  image: 'image',
  video: 'video',
  document: 'document',
  thumbnail: 'thumbnail',
  zip: 'zip'
};

class MultipleImageUpload extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    input: PropTypes.object,
    type: PropTypes.string,
    contentType: PropTypes.string,
    label: PropTypes.string,
    data: PropTypes.object,
    renderMode: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      uploadInProgress: false,
      uploadCompleted: false,
      uploadFailed: false,
    };
  }


  render() {
    const { uploadInProgress, uploadCompleted, uploadFailed } = this.state;
    const { input, type, contentType, label, data = {}, renderMode, inline } = this.props;
    const { attributes = {}, allowImage } = data;
    const { singleImage } = attributes;
    const Input = iMap(input).toJS();
    const entity = entityNames[type];
    const uniqueID = new Date().getUTCMilliseconds();
    const imageData = Input.value && typeof Input.value === 'string' && Input.value.split(',').length > 0 ? Input.value.split(',') : Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : [];
    return (
      <Col xs={12} className={styles.ImageUpload}>
        {!data.inline ?
          <label htmlFor={data.label}>
            {data.label}
          </label> : null}
        <Dropzone onDrop={this.handleDrop.bind(this)} id={`dropzone-${uniqueID}`} className={styles.drop} activeClassName={styles.drag} accept={contentType} />
        <div className={styles.space}>
          {uploadFailed ? <Alert danger>{capitalize(entity)} upload failed.</Alert> : null}
          {uploadInProgress ? <Alert info>{capitalize(entity)} upload in progress. Please Wait..</Alert> : null}
          <Gallery OnDelete={(preview) => this.OnDelete.bind(this, preview)} data={imageData} entity={entity} uniqueID={uniqueID} singleImage={imageData.length === allowImage || imageData.length > allowImage ? true : singleImage} />
        </div>
      </Col>
    );
  }

  /**
   * Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
   * @param { Array } acceptedFiles
   */
  handleDrop(acceptedFiles) {
    const { input, contentType } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && typeof Input.value === 'string' && Input.value.split(',').length > 0 ? Input.value.split(',') : Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : [];
    const fileName = acceptedFiles[0].name.replace(/[^a-zA-Z.0-9]/g, "");
    const fileContentType = contentType.indexOf('video') > -1 ? 'video/mp4' : contentType.indexOf('pdf') > -1 ? 'application/pdf' : contentType.indexOf('image') > -1 ? 'image/jpeg' : contentType;
    const handleThis = this;
    handleThis.onUploadStart(acceptedFiles);
    remotes.getSignature(contentType.indexOf('image') > -1 ? `${fileName.split(/\.(?=[^\.]+$)/)[0]}.jpg` : fileName, fileContentType)
      .then((result) => {
        const { uploadUrl, publicUrl } = result;
        if (uploadUrl && publicUrl) {
          remotes.uploadFile(uploadUrl, acceptedFiles[0], fileContentType)
            .then(() => {
              if (contentType.indexOf('video') > -1) {
                /**
                 * Thumbnail Upload
                 */
                var fileReader = new FileReader();
                fileReader.onload = function () {
                  var blob = new Blob([fileReader.result], { type: acceptedFiles[0].type });
                  var url = URL.createObjectURL(blob);
                  var video = document.createElement('video');
                  video.preload = 'metadata';
                  video.src = url;
                  var canvas = document.createElement('canvas');
                  canvas.width = video.videoWidth;
                  canvas.height = video.videoHeight;
                  canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                  var dataURI = canvas.toDataURL("image/jpeg");
                  var binary = atob(dataURI.split(',')[1]);
                  var array = [];
                  for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                  }
                  var blobData = new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
                  blobData.name = 'video-thumbnail.jpg';
                  remotes.getSignature('video-thumbnail.jpg', 'image/jpeg')
                    .then((thumbResult) => {
                      if (thumbResult.uploadUrl && thumbResult.publicUrl) {
                        remotes.uploadFile(thumbResult.uploadUrl, blobData, 'image/jpeg')
                          .then(() => {
                            inputValue.push({ url: publicUrl, thumbnail: thumbResult.publicUrl });
                            input.onChange(inputValue);
                            handleThis.onUploadCompleted();
                          })
                          .catch(() => {
                            handleThis.onUploadFailed();
                          });
                      } else {
                        handleThis.onUploadFailed();
                      }
                    })
                    .catch(() => {
                      handleThis.onUploadFailed();
                    });
                };
                fileReader.readAsArrayBuffer(acceptedFiles[0]);
              } else {
                inputValue.push(publicUrl);
                input.onChange(inputValue.join(", "));
                handleThis.onUploadCompleted();
              }
            })
            .catch(() => {
              handleThis.onUploadFailed();
            });
        } else {
          handleThis.onUploadFailed();
        }
      })
      .catch(() => {
        handleThis.onUploadFailed();
      });
  }

  /**
   * Inits local state when starting upload.
   * @param { Array } files
   */
  onUploadStart(files) {
    this.setState({
      files,
      uploadInProgress: true,
      uploadFailed: false,
      uploadCompleted: false,
    });
  }
  /**
   * Set upload error in local state.
   */
  onUploadFailed() {
    this.setState({
      uploadInProgress: false,
      uploadFailed: true,
      onUploadingComplete: false,
    });
  }
  /**
   * Set upload success in local state.
   */
  onUploadCompleted() {
    this.setState({
      uploadInProgress: false,
      uploadCompleted: true,
      uploadFailed: false,
    });
  }


  /**
   * Delete image in input.
   */
  OnDelete(preview) {
    const { input } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && typeof Input.value === 'string' && Input.value.split(',').length > 0 ? Input.value.split(',') : Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : [];
    input.onChange(typeof Input.value === 'string' ? inputValue.filter((val) => val != preview).join(", ") : inputValue.filter((val) => typeof preview === 'object' ? val.url != preview.url : val != preview))
  }


}




export default MultipleImageUpload;
