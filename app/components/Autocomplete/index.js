import React from 'react';
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';
import Autocomplete from './autocomplete';

export default class AutocompleteField extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { input, data, metaData } = this.props;
        const isPreDefinedSet = Array.isArray(data.oneOf);
        Autocomplete(document.getElementById(input.name), isPreDefinedSet && data.oneOf ? data.oneOf : metaData[data.oneOf], data, input);
    }

    handleRef() {
    
        setTimeout(() => {
            const { data, input, inline, metaData } = this.props;
            const isPreDefinedSet = Array.isArray(data.oneOf);
            const Options = isPreDefinedSet && data.oneOf || metaData && metaData[data.oneOf] || [];
            const InputValue = Options.find(e => input && input.value && e.value.toString() === input.value.toString());
            if(data.ref && data.search != true){
                data.ref.map(el => {
                    const inputEl = document.getElementById(el);
                    if(inputEl && InputValue && InputValue[el])
                        inputEl.value = InputValue[el];
                });
            }
        }, 3500);
    }

    
    render() {
        const { data, input, inline, metaData } = this.props;
        const isPreDefinedSet = Array.isArray(data.oneOf);
        const Options = isPreDefinedSet && data.oneOf || metaData && metaData[data.oneOf] || [];
        const InputValue = Options.find(e => input && input.value && e.value.toString() === input.value.toString());
        const disableField = data.disabled && (input.value && data.disabled.edit || !input.value && data.disabled.create) || false; 
        
        return <Col xs={12} className={data.ledgerWidth ? styles.LedgerAutocompleteField : styles.AutocompleteField}>
            <label htmlFor={data.label}>
                <span style={data.blueTitle ? {color:"#38a0f1"} : data.orangeTitle ? {color:"#ea6225"} : null}>{data.label}</span>
                {data.required ? <span>*</span> : null}
            </label>
            <input
                className={disableField ? styles.DisableField : inline ? styles.InputField : null}
                name={input && input.name}
                id={input && input.name}
                type="text"
                autoComplete="off"
                onChange={this.handleRef.bind(this)}
                placeholder={data.placeholder ? data.placeholder : "Type to Search"}
                defaultValue={InputValue && InputValue.label || ''}
                required={data.required}
                disabled={disableField} />
        </Col>
    }

    close() {
        this.setState({ showModal: false });
    }
}