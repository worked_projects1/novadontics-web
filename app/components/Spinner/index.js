/**
*
* Spinner
*
*/

import React from 'react';
import classNames from 'classnames';

import styles from './styles.css';

class Spinner extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { paymentStyle } = this.props;
    return (
      <div className={paymentStyle ? styles.skCubeGrid1 : styles.skCubeGrid}>
        <div className={classNames(styles.skCube, styles.skCube1)}></div>
        <div className={classNames(styles.skCube, styles.skCube2)}></div>
        <div className={classNames(styles.skCube, styles.skCube3)}></div>
        <div className={classNames(styles.skCube, styles.skCube4)}></div>
        <div className={classNames(styles.skCube, styles.skCube5)}></div>
        <div className={classNames(styles.skCube, styles.skCube6)}></div>
        <div className={classNames(styles.skCube, styles.skCube7)}></div>
        <div className={classNames(styles.skCube, styles.skCube8)}></div>
        <div className={classNames(styles.skCube, styles.skCube9)}></div>
      </div>
    );
  }
}

export default Spinner;
