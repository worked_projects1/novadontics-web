

import React from 'react';
import { Modal, Button } from '@sketchpixy/rubix';
import {updateRecord} from 'blocks/LicenseAgreement/index.js';



 class LicenseAgreement extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showAgreement: true}
  }
  handleEdit ()
  {
    const submitRecord = {agreementAccepted:true}
    updateRecord(submitRecord).then(requestdata => { 
       this.setState({ showAgreement: false });
      }).catch((error) => console.log("error"))
      
  }

  render() {
    return (
      <div>

        <Modal show={this.state.showAgreement}>
          <Modal.Header>
            <Modal.Title>
               <h4 style={{color:"#EA6225",textAlign:"center",fontWeight:"bold",fontSize:"20px"}}>End User License Agreement</h4> 
                </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
            <iframe src={"https://s3.amazonaws.com/static.novadonticsllc.com/EndUserAgreement/index.html"} width="100%" height="100%" style={{minHeight:"500px",border:"none"}}/>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="standard" type="button" onClick={this.handleEdit.bind(this)}>I Agree</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
  closeModal() {
    this.setState({ showAgreement: false });
  }

  openModal() {
    this.setState({ showAgreement: true });
  }
 }
  export default LicenseAgreement;