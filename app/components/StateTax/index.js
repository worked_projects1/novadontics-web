/**
 * 
 * TreatmentPlanDetails
 * 
 */

import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, Field } from 'redux-form/immutable';
import Spinner from 'components/Spinner';
import styles from './styles.css';
import ReactTable from "react-table";
import { connect } from 'react-redux';
import schema from 'routes/schema';
const columns = schema().vendors().stateTax;
const fieldComponent = ({ input, oneOf, type }) => {
    return type === 'select' ?
        <select name={input.name} className={styles.InputField} defaultValue={input.value} onBlur={e=>input.onChange(e.target.value)} required>
            <option value="" disabled={true}>Please select</option>
            {(oneOf || []).map((option) =>
                <option key={option} value={option}>{option}</option>)}
        </select> : <input name={input.name} className={styles.InputField} type={type} defaultValue={input.value} onBlur={e=>input.onChange(type === 'number' ? parseInt(e.target.value) : e.target.value)} required />;
};


class StateTax extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    editableColumnProps = {
        Cell: props => {
            const { column, tdProps } = props;
            const { onEdit, onCancel, edit } = tdProps.rest;
            return (column.action && !edit ?
                <Col className={styles.actions}>
                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onEdit}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>
                </Col> : column.action && edit ?
                    <Col className={styles.actions}>
                        <Button bsStyle="link" bsSize="xs" type="submit" className={styles.btn}><Icon glyph="icon-fontello-floppy" className={styles.icon} />Save</Button>
                        <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onCancel}><Icon glyph="icon-fontello-cancel" className={styles.icon} />Cancel</Button>
                    </Col> : column.editRecord && edit ?
                        <Field name={column.value} id={column.label} component={fieldComponent} type={column.type} onFocus={e => e.preventDefault()} oneOf={column.oneOf} /> : <span>{props.value}</span>);
        }
    };

    render() {
        const { editing, handleSubmit, onEdit, onCancel, metaData = {} } = this.props;
        const { stateTax = [] } = metaData;
        return <Col className={styles.StateTax}>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} bsSize="large" className={"stateTax"} backdrop="static" onHide={this.close.bind(this)}>
                <Modal.Header closeButton style={{ paddingBottom: 0 }}>
                    <Modal.Title>State Tax</Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles.body} style={{ paddingTop: 0 }}>
                    <form id="StateTax" onSubmit={handleSubmit}>
                        {stateTax ?
                            <ReactTable
                                columns={columns.map((column) => Object.assign(
                                    {},
                                    { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                                ))}
                                data={stateTax}
                                getTdProps={(state, rowProps) => {
                                    return {
                                        edit: rowProps && editing === rowProps.original.id ? true : false,
                                        onEdit: () => onEdit(rowProps),
                                        onCancel: () => onCancel()
                                    }
                                }}
                                pageSize={stateTax && stateTax.length > 0 ? 8 : 0}
                                showPageJump={false}
                                resizable={false}
                                showPageSizeOptions={false}
                                previousText={"Back"}
                                pageText={""}
                                noDataText={"No entries to show."}
                            /> : <Spinner />}
                    </form>
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        const { onCancel } = this.props;
        this.setState({ showModal: false });
        onCancel();
    }

    open() {
        this.setState({ showModal: true });
    }

}

const mapStateToProps = (state, props) => ({
    initialValues: props.selectedData ? props.selectedData : {}, // retrieve name from redux store 
});

export default connect(
    mapStateToProps
)(reduxForm({
    form: `editRecord`,
    enableReinitialize: true,
})(StateTax));
