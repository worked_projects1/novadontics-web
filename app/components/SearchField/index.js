/**
*
* 
* Wrapper for redux-form’s <Field /> and delegates work to Autocomplete component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import Autocomplete from 'components/Autocomplete';
import { Field } from 'redux-form/immutable';




class SearchField extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        data: PropTypes.object,
    };

    render() {
        const { data, metaData } = this.props;
        return (
            <Field name={data.value} id={data.label} data={data} type="input" component={Autocomplete} metaData={metaData} />
        );
    }

}

export default SearchField;
