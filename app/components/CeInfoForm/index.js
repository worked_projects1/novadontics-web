

import React from 'react';
import { Modal } from '@sketchpixy/rubix';
import {updateCeInfo} from '../../blocks/ciicourses/remotes';


 class CeInfoForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showForm: true}
  }
  handleEdit ()
  {
    const submitRecord = {ceInfo: true}
    updateCeInfo(submitRecord).then(requestdata => { 
       this.setState({ showForm: false });
      }).catch((error) => console.log("error"))
      
  }

  render() {
    const { data = {} } = this.props;
    const { attributes = {} } = data;
    const info =  attributes && attributes.info ? attributes.info : data && data.info ? data.info : false;
    const text = info && info.text ? info.text : false;

    return (
      <div>

        <Modal lg={true} show={this.state.showForm} onHide={this.handleEdit.bind(this)}>
        <Modal.Header closeButton>
            <Modal.Title>
            <h4 style={info.labelStyle ? {color: '#EA6225', fontWeight: 'bold'} : null}>{info && info.title ? info.title : data && data.title ? data.title : null}</h4>
                </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div sm={7} className={"html"} dangerouslySetInnerHTML={{__html: text}} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
  closeModal() {
    this.setState({ showForm: false });
  }

  openModal() {
    this.setState({ showForm: true });
  }
 }
  export default CeInfoForm;