/**
 * Created by stefan on 5/17/17.
 */
import React from 'react';
import PDF from 'react-pdf-js';
import PropTypes from 'prop-types';
import Spinner from 'components/Spinner';
import styles from './styles.css';
import {Icon} from "@sketchpixy/rubix";

class PdfViewer extends React.Component {
  constructor(props) {
    super(props);
    this.onDocumentComplete = this.onDocumentComplete.bind(this);
    this.onPageComplete = this.onPageComplete.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.state = {}
  }

  static propTypes = {
    pdfUrl: PropTypes.string
  };

  onDocumentComplete(pages) {
    this.setState({ page: 1, pages });
  }

  onPageComplete(page) {
    this.setState({ page });
  }

  handlePrevious() {
    this.setState({ page: this.state.page - 1 });
  }

  handleNext() {
    this.setState({ page: this.state.page + 1 });
  }

  renderPagination(page, pages) {
    let previousButton = <li className="previous" onClick={this.handlePrevious}><a href="#"><i className="fa fa-arrow-left"/> Previous</a></li>;
    if (page === 1) {
      previousButton = <li className="previous disabled"><a href="#"><i className="fa fa-arrow-left"/> Previous</a></li>;
    }
    let nextButton = <li className="next" onClick={this.handleNext}><a href="#">Next <i className="fa fa-arrow-right"/></a></li>;
    if (page === pages) {
      nextButton = <li className="next disabled"><a href="#">Next <i className="fa fa-arrow-right"/></a></li>;
    }
    let pageNumber = <p className={styles.paginationPageNumber}> {this.state.page } / {this.state.pages } </p>;
    return (
      <nav>
        <ul className="pager">
          {previousButton}
          {pageNumber}
          {nextButton}
        </ul>
      </nav>
    );
  }

  render() {
    const { pdfUrl } = this.props;
    let pagination = null;
    if (this.state.pages) {
      pagination = this.renderPagination(this.state.page, this.state.pages);
    }
    return (
      <div>
      {pdfUrl ?
        <div>
          <a href={pdfUrl} target="_blank"><Icon glyph="icon-fontello-doc-text" /> Open PDF</a>
          <PDF loading={<Spinner/>} file={pdfUrl} onDocumentComplete={this.onDocumentComplete} onPageComplete={this.onPageComplete} page={this.state.page} />
          {pagination}
        </div>
        : null }
      </div>
    )
  }
}

module.exports = PdfViewer;
