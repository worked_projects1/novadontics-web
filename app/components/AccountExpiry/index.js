import React from 'react';
import {connect} from 'react-redux';
import styles from './styles.css';
import {Col, Row} from "@sketchpixy/rubix";
import {compose, withProps} from 'recompose';
import { extendAccountValidity } from '../../blocks/accountValidity';
import ModalForm from 'components/ModalRecordForm';
import schema from '../../routes/schema';
import PaymentHistory from 'components/PaymentHistory';
import moment from 'moment';

const expirationDay = (days) => days > 0 ? `in ${days} day${days > 1 ? 's' : ''}` : `today`;
export const expirationNoticeText = (days) => `Account will expire ${expirationDay(days)}`;

const ExpirationWarning = ({expired}) => expired ? <div className={styles.error}>Account expired</div> : null;

const ExpirationNotice = ({days}) => (days <= 30 && days >= 0) ? <div className={styles.warning}>{expirationNoticeText(days)}</div> : null;

const AccountExpiry = ({record: {daysUntilExpired, expired, expirationDate}, extendAction, record}) => {

	const handleSubmit = (data, dispatch, { form }) => {
		const record = data.toJS();
    if (record.expirationDate != '' && record.expirationDate != undefined) {
      record.expirationDate = moment(record.expirationDate).format("YYYY-MM-DD")
    }
    if (record.accessStartDate != '' && record.accessStartDate != undefined) {
      record.accessStartDate = moment(record.accessStartDate).format("YYYY-MM-DD")
    }
		extendAction(record);
  };

	const metaData = {subscriptionOptions : [{id:1,label:"1 month",value:1},{id:2,label:"2 months",value:2},{id:3,label:"3 months",value:3},{id:4,label:"4 months",value:4},{id:5,label:"5 months",value:5},{id:6,label:"6 months",value:6},{id:7,label:"1 year",value:12},{id:8,label:"2 years",value:24},{id:9,label:"3 years",value:36}]};  
	return (
	  <Row className={styles.container}>
			<Col xs={6}>
				<div className={styles.label}>Access End Date:</div>
				<div>{expirationDate}</div>
				<ExpirationWarning expired={expired}/>
				{!expired && <ExpirationNotice days={daysUntilExpired}/>}
			</Col>
			<Col xs={6} className={styles.center}>
					<ModalForm
					initialValues = {record}
					fields={['Trial', 'Corporate', 'Lead'].includes(record.accountType) ? schema().accounts().extendValidity : schema().accounts().extendPaidValidity}
					form={`modalRecord.${record.id}`}
					onSubmit={handleSubmit.bind(this)}
					metaData={metaData}
					config={{record:record, title:'Extend Validity', confirmDialog: true}}
					/>
					{record.accountType == 'Paid' ? <PaymentHistory id={record.id}/> : null}
			</Col>
	  </Row>
)};



export default compose(
  connect(),
  withProps(({record, dispatch}) => ({
    extendAction: (record) => dispatch(extendAccountValidity(record)),
	record: record
  }))
)(AccountExpiry);
