/*
 * DeleteRecordForm Messages
 *
 * This contains all the text for the DeleteRecordForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  delete: {
    id: 'app.components.DeleteRecordForm.delete',
    defaultMessage: 'Delete',
  },
});
