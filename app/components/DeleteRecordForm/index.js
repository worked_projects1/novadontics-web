/**
*
* DeleteRecordForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm } from 'redux-form/immutable';
import { Modal, Button, Alert } from '@sketchpixy/rubix';

import styles from './styles.css';

class DeleteRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { decorators = {}, record, handleSubmit, error, submitting } = this.props;
    let customDeleteButtonLabel;
    let customDeleteMessage;

    if (decorators.deleteButton) {
      customDeleteButtonLabel = decorators.deleteButton(record);
    }
    if (decorators.deleteMessage) {
      customDeleteMessage = decorators.deleteMessage(record);
    }

    return (
      <div style={{ display: 'inline-block', marginLeft: 6 }}>
        <button className={styles.delete} onClick={this.open.bind(this)}>
          {customDeleteButtonLabel || 'Delete'}
        </button>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>
              {customDeleteButtonLabel || 'Delete Record?'}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>
              { customDeleteMessage ||
                'Are you sure you want to delete this record? The change may be irreversible.'
              }
            </p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close.bind(this)}>Close</Button>
            <form onSubmit={handleSubmit} style={{ display: 'inline-block', marginLeft: 6 }}>
              <Button
                type="submit"
                bsStyle="danger"
                disabled={submitting}
                className={submitting ? 'buttonLoading' : ''}
              >
                {customDeleteButtonLabel || 'Delete'}
              </Button>
            </form>
          </Modal.Footer>
          {error ?
            <Alert danger>{error}</Alert> : null}
        </Modal>
      </div>
    );
  }
  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

}

DeleteRecordForm.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
};

export default reduxForm({
  form: 'deleteRecord',
})(DeleteRecordForm);
