/**
*
* ConfirmButton
*
*/

import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Modal, Row, Button } from '@sketchpixy/rubix';
import styles from './styles.css';


class ConfirmButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    return (
      <Col>
        {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
        <Modal show={this.state.showModal} className={'deleteModal'} onHide={this.close}>
            <Modal.Body>
              <Row className={styles.rowStandard}>
                <h4>Confirm</h4>
                {this.props.onUnsubscribe ? <Col>Are you sure you want to unsubscribe?</Col> : <Col>Are you sure?</Col>}
              </Row>
            </Modal.Body>
            <Modal.Footer className={styles.footer}>
              <Button bsStyle="standard" type="button" onClick={this.close.bind(this)} className={styles.submitButton}>No</Button>
              <Col className={styles.vr}></Col>
              <Button bsStyle="standard" type="button" onClick={this.confirm.bind(this)} className={styles.submitButton}>Yes</Button>
            </Modal.Footer>
        </Modal>
      </Col>
    );
  }

  confirm(){
    this.props.onConfirm();
    this.setState({ showModal: false });
  }

  close() {
    this.setState({ showModal: false });
  }
}


export default ConfirmButton;
