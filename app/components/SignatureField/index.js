/**
*
* <SignatureField />
* Wrapper for redux-form’s <Field /> and delegates work to renderSignature component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import renderSignature from './renderUtils';
import { Field } from 'redux-form/immutable';

class SignatureField extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    data: PropTypes.object,
    shareDet: PropTypes.object,
  };

  render() {
    const { data,shareDet } = this.props;
    return (
      <Field name={data.value} component={renderSignature} shareDet={shareDet} height={data.height} draggable={data.draggable} />
    );
  }

}

export default SignatureField;
