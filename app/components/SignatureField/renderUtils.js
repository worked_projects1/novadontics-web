import React from 'react';
import PropTypes from 'prop-types';
import { Col, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import "video-react/dist/video-react.css";
import { Map as iMap } from "immutable";
import uploadSignature from './uploadUtils'
import Pad from 'react-signature-pad';
import Share from 'components/Share';

class renderSignature extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    input: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = { showPrint: false, resizeHeight: false, signatureUrl: false };
    this.handleSignatureLoad = this.handleSignatureLoad.bind(this)
  }

  componentDidMount() {
      const { draggable } = this.props;
      this.handleSignatureLoad();
      const input = iMap(this.props.input).toJS();
      this.setState({ inputData : this.props.input, signatureUrl: input.value ? input.value : false})
      window.addEventListener('resize',this.handleSignatureLoad);
      if(draggable){
        const element = document.querySelector('.SignaturePad');
        const resizers = document.querySelectorAll('.SignaturePad  .resizer');
        const minimum_size = 20;
        let original_height = 0;
        let original_y = 0;
        let original_mouse_y = 0;
        let original_width = 0;
        let original_mouse_x = 0;
        const This = this;
        for (let i = 0;i < resizers.length; i++) {
          const currentResizer = resizers[i];
          currentResizer.addEventListener('mousedown', function(e) {
            e.preventDefault()
            original_width = parseFloat(getComputedStyle(element, null).getPropertyValue('width').replace('px', ''));
            original_height = parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
            original_mouse_x = e.pageX;
            original_mouse_y = e.pageY;
            window.addEventListener('mousemove', resize)
            window.addEventListener('mouseup', stopResize)
          })
          
          function resize(e) {
            if (currentResizer.classList.contains('bottomRight')) {
              const height = original_height + (e.pageY - original_mouse_y)
              if (height > minimum_size) {
                element.style.height = height + 'px';
                This.setState({resizeHeight: height});
                This.handleSignatureLoad();
              }
            }
            else if (currentResizer.classList.contains('bottomLeft')) {
              const height = original_height + (e.pageY - original_mouse_y)
              if (height > minimum_size) {
                element.style.height = height + 'px';
                This.setState({resizeHeight: height});
                This.handleSignatureLoad();
              }
            }
            else if (currentResizer.classList.contains('topRight')) {
              const height = original_height - (e.pageY - original_mouse_y)
              if (height > minimum_size) {
                element.style.height = height + 'px';
                This.setState({resizeHeight: height});
                This.handleSignatureLoad();
              }
            }
            else {
              const height = original_height - (e.pageY - original_mouse_y)
              if (height > minimum_size) {
                element.style.height = height + 'px';
                This.setState({resizeHeight: height});
                This.handleSignatureLoad();
              }
            }
          }
          
          function stopResize() {
            window.removeEventListener('mousemove', resize)
          }
        }
      }
  }

  render() {
    const { input, shareDet, draggable } = this.props;
    const Input = iMap(input).toJS();
  
    return (
      <Col className={styles.Signature}>
        <Col className={draggable ? `SignaturePad` : `${styles.SignaturePad}`}>
          <Col className="resizers">
            <Pad ref={signature => (this.signature = signature)} onEnd={this.handleSignature.bind(this)} />
            <Col className="resizer topLeft"></Col>
            <Col className="resizer topRight"></Col>
            <Col className="resizer bottomLeft"></Col>
            <Col className="resizer bottomRight"></Col>
          </Col>
        </Col>
        
        <Col className={styles.action}>
          <Button type="button"  className={styles.btn} onClick={this.handleClear.bind(this)}>CLEAR</Button>
          <Button className={styles.btn} type="button" onClick={this.handleReset.bind(this)}>RESET</Button>
          {shareDet ? <Share dataURL={Input.value ? Input.value : shareDet.image ? shareDet.image : null} signatureUrl={this.state.signatureUrl} email={shareDet.email ? shareDet.email : null} subject={shareDet.subject ? shareDet.subject : null} /> : null}  
        </Col>
      </Col>
    );
  }

  handleClear(){
    this.signature.clear();
    const { input } = this.props;
    input.onChange('');
  }

  handleReset(){
    this.handleSignatureLoad(true);
  }

  async handleSignature() {
    const { input } = this.props;
    const dataURL = this.signature.toDataURL();
    input.onChange(dataURL);
    if(dataURL){
      this.setState({signatureUrl:''});
      const signatureUrl = await uploadSignature(dataURL);
      this.setState({signatureUrl:signatureUrl})
    }
  }

  handleSignatureLoad(reset) {
    const { input, height } = this.props;
    const { inputData, resizeHeight } = this.state;
    const Input = reset ? iMap(inputData).toJS() : iMap(input).toJS();
    const canvas = document.createElement("canvas");
    const image = new Image();
    const signature = this.signature;
    const canvasSelector = document.querySelector('canvas');
    
    const elmnt = document.getElementById("modalrecordform");
    
    if(canvasSelector){
      canvasSelector.width = elmnt && elmnt.offsetWidth ? elmnt.offsetWidth-50 : "200";
      canvasSelector.height = resizeHeight ? resizeHeight : height ? height : "160";

     
      image.crossOrigin = "anonymous";
      
      image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;
        const ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0);
        if(canvas){
          const DataURI = canvas.toDataURL();
          if(DataURI && signature){
            signature.fromDataURL(DataURI);
            input.onChange(DataURI);
          }
        }
      };
    }  

    image.src = Input.value;
    
  }
  
}

export default renderSignature;
