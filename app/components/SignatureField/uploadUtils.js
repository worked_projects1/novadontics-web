/*
 * uploadSignature
 */


import remotes from 'blocks/fileupload/remotes.js';


export default async function uploadSignature(dataURL){

    const blobBin = atob(dataURL.split(',')[1]);
    const array = [];
    for(let i = 0; i < blobBin.length; i++) {
        array.push(blobBin.charCodeAt(i));
    }
    const file=new File([new Uint8Array(array)], {type: 'image/png'});
    file.preview = window.URL.createObjectURL(file);
    return await remotes.getSignature('signature.png', 'image/png')
      .then((result) => {
        const { uploadUrl, publicUrl } = result;
        if (uploadUrl && publicUrl) {
          return remotes.uploadFile(uploadUrl, file, 'image/png')
          .then(() => {
            return publicUrl;
          })
          .catch((error) => Promise.reject(error));
        } 
      })
      .catch((error) => Promise.reject(error));
  }