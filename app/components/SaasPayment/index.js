/**
*
* SAAS Payment
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import ModalForm from 'components/ModalRecordForm';
import EditRecordForm from 'components/EditRecordForm';
import { browserHistory } from 'react-router';
import { loadAdminSaasPayment, updateAdminSaasPayment } from 'blocks/saasAdmin/remotes';
import moment from 'moment-timezone';
import schema from 'routes/schema_1';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';

class SaasPayment extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      alertMessage: false,
      alertSuccess: false,
      yearSelected: null,
      records: {}
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  render() {
    const { row, singleImage, style, implantStyle, record } = this.props;
    const { photoIndex, isOpen, yearSelected, records } = this.state;

    const updateYear = (val) => {
      const { record } = this.props;
      const { yearSelected } = this.state;
      this.setState({ yearSelected: val })
      loadAdminSaasPayment(record.id, val)
        .then(saasList => {
          let saasRecords = saasList[0];
          saasRecords.year = parseInt(val);
          this.setState({ records: saasRecords })
        })
        .catch(() => this.setState({ records: {year: parseInt(val)} }));
    }

    return (
      <div className={styles.actions}>
        <Button bsStyle="standard" className='action-button' onClick={() => this.handleOpen()}>
          SaaS Payment
        </Button>
        <Modal show={isOpen} backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={{color: '#ea6225', fontWeight: 'bold'}}>SaaS Payment</span>
            </Modal.Title>
          </Modal.Header>
            <Modal.Body id="modalrecordform">
              <EditRecordForm
                fields={schema().saasAdmin().saasPayment}
                initialValues = {records}
                form={`saasPaymentForm.${record.id}`}
                onSubmit={(data) => this.handleSubmit(data)}
                disableCancel={true}
                catalogButton={true}
                onCancel={() => this.handleClose()}
                updateYear={(val) => updateYear(val)}
              />
              {this.state.alertMessage ?
                  <AlertMessage warning dismiss={() => this.setState({ alertMessage: false })}><FormattedMessage {...messages.feeFailure} /></AlertMessage> : null}
              {this.state.alertSuccess ?
                  <AlertMessage success dismiss={() => this.setState({ alertSuccess: false, isOpen: false })}><FormattedMessage {...messages.saasSuccess} /></AlertMessage> : null}
            </Modal.Body>
        </Modal>
      </div>
    );
  }

  handleOpen() {
    this.setState({ isOpen: true });
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handleSubmit(data) {
    const { row, dispatch, record } = this.props;
    const submitRecord = data.toJS();
    const yearOpted = submitRecord.year
    delete submitRecord.year;
    var records = Object.keys(submitRecord).map(key => ({month: key, amount: submitRecord[key]}));
    var dispatchRecord = { "saasAdmin": records }

    updateAdminSaasPayment(record.id, yearOpted, dispatchRecord)
      .then(response => {
        this.setState({ alertSuccess: true, records: {} });
      })
      .catch(() => this.setState({ alertMessage: true }));
  }
}

export default SaasPayment;
