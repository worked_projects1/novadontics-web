/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.containers.LoginPage.title',
    defaultMessage: 'Novadontics',
  },
  description: {
    id: 'app.containers.LoginPage.description',
    defaultMessage: 'Novadontics - join us and be a part of the disruptive innovation in the dental industry!',
  },
});
