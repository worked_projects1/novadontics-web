/*
 *
 * AnonymousRouteWrapper, styles the LoginPage, ForgotPasswordPage, and ResetPasswordPage.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import CopyrightFooter from 'components/CopyrightFooter';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import styles from './styles.css';

import { Grid, Row, Col } from '@sketchpixy/rubix';

export default class AnonymousRouteWrapper extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: PropTypes.any.isRequired,
  };

  render() {
    const title = <FormattedMessage {...messages.title} />;
    const description = <FormattedMessage {...messages.description} />;

    return (
      <div className={styles.loginPage}>
        <Helmet
          title={title.props.defaultMessage}
          meta={[
            { name: 'description', content: description.props.defaultMessage },
          ]}
        />
        <Grid>
          <Row>
            <Col xsHidden sm={7} md={9} className={styles.loginImage} style={{ backgroundImage: `url(${require('img/login.jpg')})` }} />
            <Col xs={12} sm={5} md={3} className={styles.loginForm}>
              <Row>
                <Col md={8} mdOffset={2} sm={10} smOffset={1} xs={10} xsOffset={1} collapseLeft collapseRight>
                  <div className={styles.loginBody}>
                    <img src={require('img/white-logo.svg')} className="img-responsive" alt="Novadontics" />

                    <div className={styles.form}>
                      {this.props.children}
                    </div>
                  </div>
                </Col>

                <CopyrightFooter showMiotivInfo />
              </Row>
            </Col>
          </Row>
        </Grid>

      </div>
    );
  }
}
