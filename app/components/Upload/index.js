/**
*
* <Upload />
* Wrapper for redux-form’s <Field /> and delegates work to ImageUpload component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import ImageUpload from 'components/ImageUpload';
import MultipleUpload from 'components/MultipleUpload';
import MultipleImageUpload from 'components/MultipleImageUpload';
import NovaUpload from 'components/NovaUpload'
import { Field } from 'redux-form/immutable';

class Upload extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    data: PropTypes.object,
    contentType: PropTypes.string,
  };

  render() {
    const { data, contentType, updateContracts, initialValues, documentUpdate, inputchange, ctScanUpdate, updateCtScan } = this.props;
    return (
      <Field name={data.value} id={data.label} type={data.type} label={data.label} contentType={contentType} component={data.multiImage ? MultipleImageUpload : data.multiUpload ? MultipleUpload : data.novaUpload ? NovaUpload : ImageUpload} data={data} updateContracts={updateContracts} initialValues={initialValues} inputchange={inputchange} documentUpdate={documentUpdate} ctScanUpdate={ctScanUpdate} updateCtScan={updateCtScan} />
    );
  }

}

export default Upload;
