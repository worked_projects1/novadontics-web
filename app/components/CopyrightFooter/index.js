/**
*
* CopyrightFooter, displays copyright info
*
*/

import React from 'react';
import PropTypes from 'prop-types';

class CopyrightFooter extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    showMiotivInfo: PropTypes.bool,
  };

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const currentYear = new Date().getFullYear();
    return (
      <div className="footer-copyright-notice">
        <p>Ⓒ {currentYear > 2016 ? `2016 – ${currentYear}` : 2016} Novadontics LLC. All rights Reserved.</p>
        {this.props.showMiotivInfo ? <p>www.novadontics.com</p> : null}
        Patent Pending
      </div>
    );
  }
}

export default CopyrightFooter;
