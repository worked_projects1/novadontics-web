/**
*
* Table
*
*/

import React from 'react';
import { Col, Modal } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import ReactTable from "react-table";
import Info from '../../containers/PatientsPage/components/Info';
import styles from './styles.css';

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { records, title, columns, notes, infoColumns, className } = this.props;
    return (
      <Col>
        {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
        <Modal show={this.state.showModal} className={className ? className : ''} bsSize="large" onHide={this.close.bind(this)} onEnter={this.load.bind(this)}>
            <Modal.Header closeButton>
              <Modal.Title className={styles.title}>
              <span style={{color:'#EA6225'}}> {title} </span>
                  {infoColumns ? <Info data={infoColumns} /> : null }
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <ReactTable
                columns={columns}
                data={records ? records : []}
                pageSize={records && records.length > 0 ? 8 : 0}
                showPageJump={false}
                resizable={false}
                showPageSizeOptions={false}
                previousText={"Back"}
                pageText={""}
                noDataText={"No entries to show."}
                />
                <span style= {{fontWeight: '500'}}> {notes} </span>
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  load() {
    const { loadRecord } = this.props;
    if(typeof(loadRecord) === 'function'){
      loadRecord(); 
    }
  }

}

export default Table;