/**
*
* CheckboxIcon
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Icon, Col } from '@sketchpixy/rubix';

import styles from './styles.css';


class CheckboxIcon extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.shape({
      label: PropTypes.string,
      title: PropTypes.string,
      value: PropTypes.any,
    }),
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { data } = this.props;
    const { label, title, value } = data;
    const colSize = value ? 8 : 12;

    return (
      <Col xs={colSize} className={styles.space}>
        <Icon bundle="fontello" glyph="ok-circle" className={styles.icon} />
        {label ? <span>{label}</span> : <h3>{title}</h3>}
      </Col>
    );
  }
}

export default CheckboxIcon;
