import CheckboxIcon from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const data = [
  {
    id: 1,
    label: 'test1',
    title: 'test1',
  },
  {
    id: 2,
    label: 'test2',
    title: 'test2',
  },
];

describe('<CheckboxIcon />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <CheckboxIcon
        data={data}
      />
    );
    expect(wrapper.props().data).toEqual(data);
    expect(wrapper.find('.ok-circle')).to.have.length(1);
  });
});
