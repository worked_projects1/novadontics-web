/**
*
* Share
*
*/


import React from 'react';
import PropTypes from 'prop-types';
import { Col, Icon, Button } from '@sketchpixy/rubix';
import EmailShare from 'react-email-share-link';
import styles from './styles.css';

export default class Share extends React.Component {

    static propTypes = {
        dataURL: PropTypes.string,
        email: PropTypes.string,
        subject: PropTypes.string
    };

    constructor(props){
        super(props);
        this.state={showIcon:false, subject: false, body: false};
        this.toggleShare = this.toggleShare.bind(this);
    } 

    render() {
        const { email, subject, signatureUrl } = this.props
        return (
            <Col className={styles.share}>
                <Button bsStyle="standard" type="button" disabled={!signatureUrl || signatureUrl === '' ? true : false} onClick={this.toggleShare.bind(this)} className={signatureUrl === '' ? `buttonLoading` : ''}>SHARE</Button>
                <Col id="shareIcon" className={styles.shareIcon}> 
                    {signatureUrl === '' ? null : <Col> 
                        <EmailShare email={email} subject={subject ? subject : ''} body={signatureUrl ? signatureUrl : null}>
                            {link => (
                                <a href={link} data-rel="external"><Icon className={styles.email} glyph={"icon-fontello-email"} /></a>
                            )}
                        </EmailShare>
                        <Icon className={styles.print} glyph={"icon-fontello-print"} onClick={this.handlePrint.bind(this)} />
                    </Col>}        
                </Col>
                <iframe id="ifmcontentstoprint" className={styles.iframe}></iframe>
            </Col>
        )
    }

    toggleShare(){
        this.setState({ showIcon: !this.state.showIcon});
        const { showIcon } = this.state;
        const train = document.getElementById('shareIcon');
        const start = Date.now();
        const timer = setInterval(function() {
            const timePassed = Date.now() - start;
            train.style.display = !showIcon ? 'flex' : 'none';
            train.style.left = timePassed / 5 + 'px';
            if (timePassed > 150) clearInterval(timer);
        }, 20);
    }

    handlePrint() {
        const { dataURL } = this.props;
        var iframe = document.getElementById("ifmcontentstoprint").contentWindow;
        iframe.document.open();
        iframe.document.write("<html><head><script>function step1(){\n" +
        "setTimeout('step2()', 10);}\n" +
        "function step2(){window.print();window.close()}\n" +
        "</scri" + "pt></head><body onload='step1()'>\n" +
        "<img src='" + dataURL + "' /></body></html>");
        iframe.document.close();
    }
    
}

