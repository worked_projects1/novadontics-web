import React from 'react';
import DeleteRecordForm from '../DeleteRecordForm';

const DeleteActionLink = ({deleteRecord, decorators, record}) => {
  const handleDelete = (data, dispatch, { form }) => {
    const raw = data.toJS();
    let customAction;
    if (decorators && decorators.deleteAction) {
      customAction = decorators.deleteAction(raw);
    }

    if (customAction) {
      dispatch(customAction(raw.id, form));
    } else {
      dispatch(deleteRecord(raw.id, form));
    }
  };

  return (
    deleteRecord ? <DeleteRecordForm
      initialValues={record}
      form={`deleteRecord.${record.id}`}
      onSubmit={handleDelete}
      decorators={decorators}
      record={record}
    /> : null
  )
};

export default DeleteActionLink;