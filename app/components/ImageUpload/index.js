import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Col, Image, Alert } from '@sketchpixy/rubix';
import styles from './styles.css';
import remotes from '../../blocks/fileupload/remotes.js';
import { getWrapperClass, capitalize } from './uploadUtils.js';
import Spinner from "../Spinner/index";
import { Player } from 'video-react';
import "video-react/dist/video-react.css";

const entityNames = {
  image: 'image',
  video: 'file',
  document: 'document',
  thumbnail: 'thumbnail'
};

class ImageUpload extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    input: PropTypes.object,
    type: PropTypes.string,
    contentType: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      uploadInProgress: false,
      uploadCompleted: false,
      uploadFailed: false,
    };
  }

  render() {
    const { files, uploadInProgress, uploadCompleted, uploadFailed } = this.state;
    const { input, type, contentType } = this.props;
    const wrapperClass = getWrapperClass(uploadInProgress, uploadCompleted, uploadFailed);
    const entity = entityNames[type];

    let imagePreview = null;

    if (files.length > 0) {
      imagePreview = this.renderUploadStatus(wrapperClass, files, uploadFailed, uploadCompleted, uploadInProgress, entity);
    } else if (input.value && input.value.length > 0) {
      imagePreview = this.renderCurrentImage(input, entity);
    }

    return (
      <Col xs={12}>
        <Dropzone onDrop={this.handleDrop.bind(this)} className={styles.drop} activeClassName={styles.drag} accept={contentType}>
          <div>Drag and drop your {entity} here, or click to select files to upload.</div>
        </Dropzone>
        {imagePreview}
      </Col>
    );
  }

  renderUploadStatus(wrapperClass, files, uploadFailed, uploadCompleted, uploadInProgress, entity) {
    return (
      <div className={styles.space}>
        {uploadFailed ? <Alert danger>{capitalize(entity)} upload failed. Please try again.</Alert> : null}
        {uploadCompleted ? <Alert success>{capitalize(entity)} uploaded successfully. Submit the form to confirm changes.</Alert> : null}
        {uploadInProgress ? <Alert info>{capitalize(entity)} upload in progress.</Alert> : null}
        {(files && files[0].type !== 'application/pdf') ?
          <div>
            <p>Preview {capitalize(entity)}:</p>
            <div className={wrapperClass}>
              {(entity === 'image' || entity === 'thumbnail') ?
                <Image src={files[0].preview} responsive /> :
                  <Player src={files[0].preview} responsive />
              }
            </div>
          </div> : uploadInProgress ? <Spinner/> : null}
      </div>
    );
  }

  renderCurrentImage(input, entity) {
    return (entity === 'image') ?
        <div className={styles.space}>
          <p>Current Image:</p>
          <div>
            <Image src={input.value} responsive />
          </div>
        </div>
    : null
  }

  /**
   * Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
   * @param { Array } acceptedFiles
   */
  handleDrop(acceptedFiles) {
    const { input, contentType } = this.props;
    this.onUploadStart(acceptedFiles);
    remotes.getSignature(acceptedFiles[0].name, acceptedFiles[0].type)
      .then((result) => {
        const { uploadUrl, publicUrl } = result;
        if (uploadUrl && publicUrl) {
          remotes.uploadFile(uploadUrl, acceptedFiles[0], acceptedFiles[0].type)
            .then(() => {
              input.onChange(publicUrl);
              this.onUploadCompleted();
            })
            .catch(() => {
              this.onUploadFailed();
            });
        } else {
          this.onUploadFailed();
        }
      })
      .catch(() => {
        this.onUploadFailed();
      });
  }

  /**
   * Inits local state when starting upload.
   * @param { Array } files
   */
  onUploadStart(files) {
    this.setState({
      files,
      uploadInProgress: true,
      uploadFailed: false,
      uploadCompleted: false,
    });
  }
  /**
   * Set upload error in local state.
   */
  onUploadFailed() {
    this.setState({
      uploadInProgress: false,
      uploadFailed: true,
      onUploadingComplete: false,
    });
  }
  /**
   * Set upload success in local state.
   */
  onUploadCompleted() {
    this.setState({
      uploadInProgress: false,
      uploadCompleted: true,
      uploadFailed: false,
    });
  }
}

export default ImageUpload;
