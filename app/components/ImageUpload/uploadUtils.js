/**
 * @param {Boolean} uploadInProgress
 * @param {Boolean} uploadCompleted
 * @param {Boolean} uploadFailed
 */
export function getWrapperClass(uploadInProgress, uploadCompleted, uploadFailed) {
  if (uploadInProgress) {
    return 'fileUploadProgress';
  } else if (uploadCompleted) {
    return 'fileUploadCompleted';
  } else if (uploadFailed) {
    return 'fileUploadFailed';
  }
  return '';
}

export function capitalize(str){
  if(str && str.length > 1) {
    return str[0].toUpperCase() + str.slice(1);
  }
}
