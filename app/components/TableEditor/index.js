/**
*
* TableEditor
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import ModalForm from 'components/ModalRecordForm';
import EditRecordForm from 'components/EditRecordForm';
import { browserHistory } from 'react-router';
import { updateImplantData } from 'blocks/implantLog/remotes.js';
import { updateBoneGraftData } from 'blocks/boneGraftLog/remotes.js';
import moment from 'moment-timezone';
import AlertMessage from 'components/Alert';

class TableEditor extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      removalAlert: false,
      alertMessage: "",
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  render() {
    const { row, tableColumns, singleImage, style, implantStyle, name } = this.props;
    const { photoIndex, isOpen, alertMessage } = this.state;
    let implantValues = {}
    let boneGraftValues = {}
    if(name == 'implantLog') {
      let xrayImages = row.finalPA.props != undefined && row.finalPA.props.children.props != undefined ? row.finalPA.props.children.props.data : row.finalPA;
      implantValues.id = row['id'];
      implantValues.dateOfRemoval = row.dateOfRemoval.props != undefined ? row.dateOfRemoval.props.children : row.dateOfRemoval;
      implantValues.reasonForRemoval = row.reasonForRemoval.props != undefined ? row.reasonForRemoval.props.children : row.reasonForRemoval;
      implantValues.implantRestored = row.implantRestored.props != undefined ? row.implantRestored.props.children : row.implantRestored;
      implantValues.implantRestored = row.implantRestored.props != undefined ? row.implantRestored.props.children : row.implantRestored;
      implantValues.finalPA = xrayImages;
    }
    if(name == 'boneGraftLog') {
      let xrayImages = row.finalPA.props != undefined && row.finalPA.props.children.props != undefined ? row.finalPA.props.children.props.data : row.finalPA;
      boneGraftValues.id = row['id'];
      boneGraftValues.failureDate = row.failureDate.props != undefined ? row.failureDate.props.children : row.failureDate;
      boneGraftValues.finalPA = xrayImages;
    }

    return (
      <Col xs={12} className={styles.actions}>
        <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => this.handleOpen()}>
          <Icon glyph="icon-fontello-edit" className={styles.icon} style={{color:'#EA6225'}} />Edit
        </Button>
        <Modal show={isOpen} backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={{color:'#EA6225'}}>{name == 'implantLog' ? 'Edit Implant Logbook' : 'Edit Bone Graft Logbook'}</span>
            </Modal.Title>
          </Modal.Header>
            <Modal.Body id="modalrecordform">
              <EditRecordForm
                fields={tableColumns}
                initialValues = {name == 'implantLog' ? implantValues : name == 'boneGraftLog' ? boneGraftValues : {}}
                form={`implantLogForm.${row['id']}`}
                onSubmit={(data) => this.handleSubmit(data)}
                disableCancel={true}
                catalogButton={true}
                onCancel={() => this.handleClose()}
              />
            </Modal.Body>
            {this.state.removalAlert ?
              <AlertMessage warning dismiss={() => { this.setState({ removalAlert: false });}}>{alertMessage}</AlertMessage> : null}
        </Modal>
      </Col>
    );
  }

  handleOpen() {
    this.setState({
      isOpen: true
    });
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handleSubmit(data) {

    const { row, dispatch, actions, name } = this.props;
    const { removalAlert, alertMessage } = this.state;
    const submitRecord = data.toJS();

    if(name == 'implantLog') {
      submitRecord.id = row.id
      submitRecord.dateOfRemoval = submitRecord.dateOfRemoval != "" ? moment(submitRecord.dateOfRemoval).format('MM/DD/YYYY') : "";
      submitRecord.finalPA = typeof submitRecord.finalPA === 'object' ? submitRecord.finalPA.join(',') : submitRecord.finalPA;
      if(submitRecord.dateOfRemoval != "" && submitRecord.reasonForRemoval == "") {
        this.setState({ removalAlert: true, alertMessage: "Please select Reason for Removal" });
      } else if(submitRecord.dateOfRemoval == "" && submitRecord.reasonForRemoval != "") {
        this.setState({ removalAlert: true, alertMessage: "Please select Date of Removal" });
      } else {
        updateImplantData(submitRecord)
          .then(response => {
            this.setState({ isOpen: false });
            dispatch(actions.loadRecords(true));
          })
          .catch(() => this.setState({ removalAlert: true, alertMessage: "There was an error while updating your changes. Please try again." }));
      }
    }
    if(name == 'boneGraftLog') {
      submitRecord.finalPA = typeof submitRecord.finalPA === 'object' ? submitRecord.finalPA.join(',') : submitRecord.finalPA;
      submitRecord.failureDate = submitRecord.failureDate != "" ? moment(submitRecord.failureDate).format('YYYY-MM-DD') : '';
      if(submitRecord.failureDate === "") {
        delete submitRecord.failureDate;
      }
      updateBoneGraftData(submitRecord)
        .then(response => {
          this.setState({ isOpen: false });
          dispatch(actions.loadRecords(true));
        })
        .catch(() => this.setState({ removalAlert: true, alertMessage: "There was an error while updating your changes. Please try again." }));
    }
  }
}

export default TableEditor;
