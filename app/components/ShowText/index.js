import React from 'react';
import { Row, Col, Modal } from '@sketchpixy/rubix';
import styles from './styles.css';


class ShowText extends React.Component {
    constructor(props){
        super(props);
        this.state = { showModal: false };
    }

    render() {
        const { title = '', text = '', parsedValues, procedureArr } = this.props;
        var procedureDetails = procedureArr ? parsedValues != 'false' && parsedValues.procedure.length > 0 ? parsedValues.procedure : '' : '';
        return <Col>
            {this.props.children(()=>{this.setState({ showModal: !this.state.showModal })})}
            <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {title}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Row className={styles.rowStandard}>
                    { procedureDetails.length > 0 ?
                      <Col sm={12} >
                        {procedureDetails.map((item, index) =>
                          <div id={index} key={index} className={styles.divStyle}>
                            <h5 className={styles.codeStyle}>{item.procedureCode} :</h5>
                            <span>{item.procedureDescription}</span>
                          </div>
                        )}
                      </Col>:
                      <Col dangerouslySetInnerHTML={{__html: text}} /> }
                  </Row>
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false});
    }

}

export default ShowText;