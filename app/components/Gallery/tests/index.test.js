import Gallery from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const data = [
  {
    id: 1,
    images: ['http://test.com/test.jpg', 'http://test.com/test1.jpg'],
  },
  {
    id: 2,
    images: ['http://test.com/test.jpg', 'http://test.com/test1.jpg'],
  },
];

describe('<Gallery />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <Gallery
        data={data}
      />
    );
    expect(wrapper.props().data).toEqual(data);
  });
});
