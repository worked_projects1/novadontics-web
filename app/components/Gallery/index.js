/**
*
* Gallery
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Col } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';

class Gallery extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
    this.handleNext = :: this.handleNext;
    this.handlePrev = :: this.handlePrev;
  }

  componentDidMount() {
    var imageLinks = document.querySelectorAll('#gallery img');
    for (var i = 0; i < imageLinks.length; i++) {
      imageLinks[i].addEventListener('click', function (e) {
        e.preventDefault()
        const vidSrc = e.target.getAttribute('href');
        if (vidSrc) {
          BigPicture({
            el: e.target,
            vidSrc: e.target.getAttribute('href')
          })
        } else {
          BigPicture({
            el: e.target,
            gallery: '#gallery',
          })
        }
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { data, width, singleImage, style, implantStyle, imageStyle } = this.props;
    const { photoIndex, isOpen } = this.state;
    const images = data.images;
    const nextImage = (photoIndex + 1) % images.length;
    const prevImage = (photoIndex + (images.length - 1)) % images.length;

    return (
      <Col xs={12} id="gallery" className={implantStyle ? styles.implantGallery : styles.gallery}>
        {singleImage ?
          <Col style={{display:"flex",flexDirection:"column"}}>
            <button type="button" className={styles.button}>
              <img style={style} data-bp={images && (typeof images[0] === 'object' && images[0].thumbnail || images[0])} src={images && (typeof images[0] === 'object' && images[0].thumbnail || images[0])} width={width ? width : '80'} />
            </button>
            <Col className={styles.sliderView}>
              {images.map((image, i) => {
                return i != 0 ?
                  <button key={i} type="button" className={styles.button}>
                    {typeof image === 'object' ?
                      <img href={image && image.url} src={require('img/catalog/play4.png')} style={imageStyle ? { width: '75px', height: '75px' } : { width: '50px', height: '50px' }} /> :
                      <img data-bp={image} src={image} style={imageStyle ? { width: '100px', height: '100px' } : { width: '50px', height: '50px' }} />}
                  </button> : null
              })}
            </Col>
          </Col> :
          images.map((image, i) =>
            <button key={i} type="button" className={styles.button} onClick={() => this.handleOpen(i)}>
              <img src={image} width={width ? width : '80'} />
            </button>
          )}

        {!singleImage && isOpen &&
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[nextImage]}
            prevSrc={images[prevImage]}
            onCloseRequest={this.handleClose}
            onMovePrevRequest={() => this.handlePrev(prevImage)}
            onMoveNextRequest={() => this.handleNext(nextImage)}
          />
        }
      </Col>
    );
  }

  handleOpen(i) {
    this.setState({
      isOpen: true,
      photoIndex: i,
    });
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handlePrev(nextImage) {
    this.setState({ photoIndex: nextImage });
  }

  handleNext(prevImage) {
    this.setState({ photoIndex: prevImage });
  }
}

export default Gallery;
