import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';

class ImageEditor extends React.Component {

    constructor(props) {
        super(props);
        this.state = { showModal: true };
    }

    componentDidMount() {
        new novaEditor(document.getElementById('novaImage'), this.props);
    }

    render() {
        const { acceptedFiles = [] } = this.props;
        const [file] = acceptedFiles;
        return <Col>
            <Modal show={this.state.showModal} bsSize="large" className="novaEditor" onHide={this.close.bind(this)}>
                <Modal.Header>
                    <Col sm={12} className={`novaEditor-header ${styles.header}`}>
                        <Button onClick={this.close.bind(this)} type="button" className={styles.btn}>Cancel</Button>
                        <h4 className={styles.subtitle}>Image Editor</h4>
                        <Button action="crop" type="button" className={styles.btn}>Done</Button>
                    </Col>
                </Modal.Header>
                <Modal.Body>
                    <Col className="novaEditor-options">
                        <Icon action="zoomIn" glyph="icon-fontello-zoom-in" />
                        <Icon action="zoomOut" glyph="icon-fontello-zoom-out" />
                        <Icon action="moveLeft" glyph="icon-fontello-left" />
                        <Icon action="moveRight" glyph="icon-fontello-right" />
                        <Icon action="moveUp" glyph="icon-fontello-up" />
                        <Icon action="moveDown" glyph="icon-fontello-down" />
                        <Icon action="rotateLeft" glyph="icon-fontello-ccw" />
                        <Icon action="rotateRight" glyph="icon-fontello-cw" />
                        <Icon action="flipHorizontal" glyph="icon-fontello-resize-horizontal" />
                        <Icon action="flipVertical" glyph="icon-fontello-resize-vertical" />
                        <Icon action="disable" glyph="icon-fontello-lock" />
                        <Icon action="enable" glyph="icon-fontello-lock-open-alt" />
                        <Icon action="reset" glyph="icon-fontello-arrows-ccw" />
                    </Col>
                    <Col>
                        <img id="novaImage" src={file && file.preview || ''} alt="Picture" width="400" height="400" />
                    </Col>
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false });
        this.props.onClose();
    }
}

export default ImageEditor;