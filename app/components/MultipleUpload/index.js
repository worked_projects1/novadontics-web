import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Col, Alert, Icon, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import remotes from 'blocks/fileupload/remotes.js';
import { capitalize } from './uploadUtils.js';
import "video-react/dist/video-react.css";
import { Map as iMap } from "immutable";
import ConfirmButton from "components/ConfirmButton";
import moment from 'moment';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';

const entityNames = {
  image: 'image',
  video: 'file',
  document: 'document',
  thumbnail: 'thumbnail',
  zip: 'zip'
};

class MultipleUpload extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    input: PropTypes.object,
    contentType: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      alertMessage : false,
      uploadInStart : false,
      uploadInProgress: false,
      uploadCompleted: false,
      uploadLimit: false,
      uploadFailed: false,
      publicUrl: false,
      percentage:0,
      loaded:0,
      total:0,
      errorMsg: false
    };
    this.onUploadStart = this.onUploadStart.bind(this);
    this.onUploadProgress = this.onUploadProgress.bind(this);
    this.onUploadFailed = this.onUploadFailed.bind(this);
    this.onUploadCompleted = this.onUploadCompleted.bind(this);
    this.OnUploadDelete = this.OnUploadDelete.bind(this);

  }

  componentWillUnmount() {
    if (this.successAlertTimeout) {
      clearTimeout(this.successAlertTimeout);
    }
  }

  render() {
    const { uploadInStart, uploadInProgress, uploadCompleted, uploadLimit, uploadFailed, percentage, loaded, total, errorMsg, alertMessage } = this.state;
    const { input, type, contentType, label, inline, initialValues, documentUpdate, ctScanUpdate } = this.props;
    let initialValue = documentUpdate || ctScanUpdate ? initialValues.toJS() : {}
    const Input = iMap(input).toJS();
    const entity = entityNames[type];
    const uniqueID = new Date().getUTCMilliseconds();
    const accept = (type === 'files') ? {} : { accept: contentType }
    return (
      <Col xs={12} className={styles.MultipleUpload}>
        {documentUpdate && initialValue.documentType == "All" ?
          <div className={styles.drop}>
              <Button bsStyle="standard" onClick={() => this.setState({ alertMessage: true })}>ADD {label}</Button>
          </div> :
          ctScanUpdate && initialValue.disable ? null :
          <Dropzone onDrop={this.handleDrop.bind(this)} className={styles.drop} activeClassName={styles.drag} {...accept} >
              <Button bsStyle="standard">ADD {label}</Button>
          </Dropzone>}
        {uploadInStart ? <Alert info>{capitalize(entity)} upload in progress.</Alert> : null}
        {uploadFailed ? <Alert danger>{errorMsg ? errorMsg : `${capitalize(entity)} upload failed.` }</Alert> : null}
        {uploadLimit ? <Alert danger> Maximum {capitalize(entity)} size allowed 1.2 GB only.</Alert> : null}
        {uploadCompleted ? <Alert success>{capitalize(entity)} uploaded successfully. Submit the form to confirm changes.</Alert> : null}
        {uploadInProgress ? <Alert info>{`${percentage}% completed. upload in progress.`} <br/> {`Uploaded ${((loaded/1024)/1024).toFixed(2)} MB of ${((total/1024)/1024).toFixed(2)} MB`}</Alert> : null}
        <div className={styles.space}>
          {Input && Input.value && Input.value.length > 0 ? Input.value.split(',').map((preview,index) =>
            {
            const fileName = preview.substr(preview.lastIndexOf('/') + 1);   
            const fileUploadDate = fileName ?moment(parseInt(fileName.substr(fileName.lastIndexOf('-') + 1).split('.')[0])).format("LLL") : false; 
            
            return <Col key={`${entity}-${index}-${uniqueID}`} id={`${entity}-${index}-${uniqueID}`} className={styles.preview}>
                <Col className={styles.fileNames}>
                  {fileName && fileName.length > 25 ? fileName.substr(0,25)+'...' : fileName}
                  <span className={styles.uploadDate}>{fileUploadDate ? fileUploadDate : ''}</span>
                </Col>
                <Col className={styles.icons}>
                  <Icon onClick={() => this.onFileDownload(preview)} glyph="icon-fontello-download" className={styles.glyphIcon} />
                  {(documentUpdate && initialValue.documentType == "All") || (ctScanUpdate && initialValue.disable) ?
                    null :
                    <ConfirmButton onConfirm = {()=> this.OnUploadDelete(preview)}>{(click)=><Icon onClick={() => click()} glyph="icon-fontello-trash" className={styles.glyphIcon} />}</ConfirmButton>}
                </Col>
            </Col> 
          }) : label == 'Invoice' ? <Col className={styles.notFound}></Col> : <Col className={styles.notFound}>{`No ${label} Found`}</Col> }
        </div>
        {this.state.alertMessage ?
          <AlertMessage warning dismiss={() => { this.setState({ alertMessage: false }); }}><FormattedMessage {...messages.documentUpload} /></AlertMessage> : null}
      </Col>
    );
  }

  /**
   * Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
   * @param { Array } acceptedFiles
   */
  onFileDownload(url) {
    const fileType = url.substring(url.lastIndexOf('.') + 1)
    if(fileType == "xml"){
      const use= url.substr(url.lastIndexOf('/') + 1);
      fetch(url, {
        method: "GET",
        responseType:'blob'
      })
      .then((response) => response.blob())
      .then((blob)=> {
        const linkUrl = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement("a");
        link.href = linkUrl;
        link.setAttribute("download", use); //or any other extension
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      window.open(url, '_blank')
    }
  }

  handleDrop(acceptedFiles) {
    const { uploadInStart, uploadInProgress } = this.state;
    const { contentType } = this.props;
    const fileSize = acceptedFiles[0] ? ((acceptedFiles[0]['size']/1024)/1024).toFixed(2) : 0;

    if(uploadInStart || uploadInProgress){
        return false;
    } else if(acceptedFiles.length == 0){
      this.onUploadFailed();
      return false;
    } else if(fileSize > 1200){
      this.onUploadLimit();
      return false;
    }

    this.onUploadStart();
    const ajax = new XMLHttpRequest(); 
    ajax.upload.addEventListener("progress", this.onUploadProgress, false);
    ajax.addEventListener("load", this.onUploadCompleted, false);
    ajax.addEventListener("error", this.onUploadFailed, false);
    ajax.addEventListener("abort", this.onUploadFailed, false);
    remotes.getSignature(acceptedFiles[0].name.replace(/[^a-zA-Z.]/g, ""), contentType)
      .then((result) => {
        const { uploadUrl, publicUrl } = result;
        if (uploadUrl && publicUrl) {
          this.setState({publicUrl:publicUrl})
          ajax.open("PUT", uploadUrl, true);
          ajax.setRequestHeader("Content-Type", contentType);
          ajax.send(acceptedFiles[0]);
        } else {
          this.onUploadFailed();
        }
      })
      .catch(() => {
        this.onUploadFailed();
      });
  }

  /**
   * Set upload start in local state.
   * 
   */
  onUploadStart() {
    this.setState({
      uploadInStart:true, 
      uploadInProgress: false,
      uploadFailed: false,
      uploadLimit: false,
      uploadCompleted: false,
    });
  }

  /**
   * Set upload progress in local state.
   * @param { Array } files
   */
  onUploadProgress(event) {
    this.setState({
      uploadInStart:false,
      uploadInProgress: true,
      uploadFailed: false,
      uploadLimit: false,
      uploadCompleted: false,
      percentage: Math.round((event.loaded / event.total) * 100),
      loaded: event.loaded,
      total: event.total,
    });
  }

  /**
   * Set upload error in local state.
   */
  onUploadFailed(error) {
    this.setState({
      uploadInStart:false,
      uploadInProgress: false,
      uploadFailed: true,
      uploadLimit: false,
      uploadCompleted: false,
      onUploadingComplete: false,
      errorMsg : error ? error : false,
    });
    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        errorMsg: false, uploadFailed : false
        });
    }, 3000);
  }

  /**
   * Set upload limit in local state.
   */
  onUploadLimit() {
    this.setState({
      uploadInStart:false,
      uploadInProgress: false,
      uploadFailed: false,
      uploadLimit: true,
      uploadCompleted: false,
      onUploadingComplete: false
    });

    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadLimit: false,
        });
    }, 3000);
  }

  /**
   * Set upload success in local state.
   */
  onUploadCompleted() {
    const { input, initialValues, updateContracts, documentUpdate, ctScanUpdate, updateCtScan } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && Input.value.length > 0 ? Input.value.split(',') : [];
    inputValue.push(this.state.publicUrl);  
    input.onChange(inputValue.toString());
    
    this.setState({
      uploadInStart:false,
      uploadInProgress: false,
      uploadCompleted: true,
      uploadLimit: false,
      uploadFailed: false,
    });

    this.successAlertTimeout = setTimeout(() => {
      if(documentUpdate) {
        let initialValue = initialValues.toJS();
        let submitRecord = {}
        submitRecord.documentType = initialValue.documentType;
        submitRecord.id = initialValue.patientId;
        submitRecord.documents = inputValue.toString();
        updateContracts(submitRecord);
      }
      if(ctScanUpdate) {
        let initialValue = initialValues.toJS();
        let submitRecord = {}
        submitRecord.id = initialValue.id;
        submitRecord.ctScanFile = inputValue.toString();
        updateCtScan(submitRecord);
      }
      this.setState({
        uploadCompleted: false,
        });
    }, 2000);

  }

  /**
   * Set upload delete in local file.
   */
  OnUploadDelete(deleteItem) {
    const { input, initialValues, updateContracts, documentUpdate, ctScanUpdate, updateCtScan } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && Input.value.length > 0 ? Input.value.split(',') : [];
    input.onChange(inputValue.filter((val) => val != deleteItem).toString());
    if(documentUpdate) {
      let initialValue = initialValues.toJS();
      let submitRecord = {}
      submitRecord.documentType = initialValue.documentType;
      submitRecord.id = initialValue.patientId;
      submitRecord.documents = inputValue.filter((val) => val != deleteItem).toString();
      updateContracts(submitRecord);
    }
    if(ctScanUpdate) {
      let initialValue = initialValues.toJS();
      let submitRecord = {}
      submitRecord.id = initialValue.id;
      submitRecord.ctScanFile = inputValue.filter((val) => val != deleteItem).toString();
      updateCtScan(submitRecord);
    }
  }

}

export default MultipleUpload;
