/**
*
* TableData
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import { dataTable } from 'datatables';
import $ from 'jquery';

import { Table } from '@sketchpixy/rubix';

class TableData extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    columns: PropTypes.array,
    children: PropTypes.array,
    name: PropTypes.string,
    activeView: PropTypes.object,
    customTable: PropTypes.bool,
  };

  componentDidMount() {
    const { name } = this.props;

    $(`#${name}`).dataTable();
  }

  render() {
    const { columns, children, activeView, customTable, name } = this.props;

    return (
      <div>
        <Table id={name} condensed responsive>
          <thead>
            <tr>
              {columns.map((column) => {
                if (activeView) {
                  return column.visible && column.viewMode ? <th key={column.id}>{column.label}</th> : null;
                } else if (customTable) {
                  return column.viewRecord ? <th key={column.id}>{column.label}</th> : null;
                }

                return column.visible ? <th key={column.id}>{column.label}</th> : null;
              })}
            </tr>
          </thead>
          <tbody>
            {children}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default TableData;
