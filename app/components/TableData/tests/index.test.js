import TableWrapper from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const columns = [
  {
    id: 1,
    label: 'test1',
  },
  {
    id: 2,
    label: 'test2',
  },
];

const records = [
  {
    id: 1,
    label: 'test1',
  },
  {
    id: 2,
    label: 'test2',
  },
];

const name = 'test';
const path = '/test';

describe('<TableWrapper />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <TableWrapper
        records={records}
        columns={columns}
        path={path}
        name={name}
      />
    );
    expect(wrapper.props().columns).toEqual(columns);
    expect(wrapper.props().records).toEqual(records);
    expect(wrapper.props().path).toEqual(path);
    expect(wrapper.props().name).toEqual(name);
  });
});
