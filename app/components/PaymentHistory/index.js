/**
*
* PaymentHistory
*
*/

import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import shallowCompare from 'react-addons-shallow-compare';
import { loadPaymentHistory, updatePaymentHistory } from '../../blocks/accounts/remotes.js'; 
import Spinner from 'components/Spinner';
import styles from './styles.css';
import ReactTable from "react-table";
import { Field } from 'redux-form/immutable'  ;
import FormProvider from './FormProvider';
import set from "lodash/fp/set";
import moment from 'moment';
import Datepicker from 'react-modern-datepicker';

class PaymentHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, data: false, editing: null, amountCollected: null, collectedDate: null};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  componentDidMount() {
    const { id } = this.props;
    loadPaymentHistory(id).then((records) => {
      this.setState({data:records});
    })
  }  

  fieldComponent = ({ input, value }) => {
    return <input className={styles.inputField} required type="number" {...input} value={value}  />;
  };

  editableColumnProps = {
    Cell: props => {
      const editing = this.state.editing === props.original;
      const changeVal = (e) => {
        if(props.column.id == 'amountCollected'){
          this.setState({ amountCollected : e.target.value})
        } else if(props.column.id == 'collectedDate') {
          this.setState({ collectedDate : e})  
        }
      }
      const editValue = props.column.id == 'amountCollected' && this.state.amountCollected != null? this.state.amountCollected : props.column.id == 'collectedDate' && this.state.collectedDate != null ? this.state.collectedDate : props.value;
      const fieldProps = {
        component: this.fieldComponent,
        props : {value : editValue}
      };

      return editing && props.column.id == 'collectedDate' ?
      <Datepicker 
        date={this.state.collectedDate ? this.state.collectedDate : props.value ? moment.utc(props.value) : moment()} 
        format={'DD MMM, YYYY'} 
        showBorder     
        className={styles.dateField}
        onChange={e => changeVal(e) } 
        minDate={moment(new Date("1/1/2019").valueOf()).subtract('1','days')}
      /> : editing ? 
      <Field name={props.column.id} {...fieldProps} onChange={e => changeVal(e) }/> :
      <span>{props.column.id  == 'amountCollected' && props.value ? parseInt(props.value) : props.column.id  == 'collectedDate' && props.value ? moment.utc(props.value).format('DD MMM, YYYY') : props.value ? props.value : '-' }</span>;
    }
  };


  ActionsCell(props) {
    const {
      mode,
      actions: { onEdit, onCancel }
    } = props.columnProps.rest;
    const editModes = {
      view: props => (<Button bsSize="xs" bsStyle="link" className={styles.btn} onClick={props.onEdit}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>),
      edit: props => (
          <Col>
            <Button type="submit" bsStyle="link" bsSize="xs" className={styles.btn}><Icon glyph="icon-fontello-floppy" className={styles.icon} />Save</Button>
            <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={props.onCancel}><Icon glyph="icon-fontello-cancel" className={styles.icon} />Cancel</Button>
          </Col>
       )
    }
    const Buttons = editModes[mode];
    return <Buttons onEdit={() => onEdit(props.index)} onCancel={onCancel} />;
  }

  getActionProps = (gridState, rowProps) => {
  return (rowProps && {
    mode: this.state.editing === rowProps.original ? "edit" : "view",
    actions: {
      onEdit: () => this.setState({ editing: rowProps.original, collectedDate : null, amountCollected: null }),
      onCancel: () => this.setState({ editing: null })
    }
  }) || {}};

  columns = [
    { Header: "Plan", accessor: "plan"},
    { Header: "Renewal Date", accessor: "renewalDate", Cell: props => <span>{props.value ? moment.utc(props.value).format('DD MMM, YYYY') : '-'}</span> },
    { Header: "Collected Date", accessor: "collectedDate", ...this.editableColumnProps},
    { Header: "Collected Amount", accessor: "amountCollected", ...this.editableColumnProps},
    {
      Header: '',
      accessor: '',
      getProps: this.getActionProps,
      sortable:false,
      Cell: props => this.ActionsCell(props) // Custom cell components!
    }
  ];
  
 
  
  render() {

    const { data, editing, showModal } = this.state;
    return (
      <Col>
        <Col className={styles.history} onClick={this.open.bind(this)}>Payment History</Col>
        <Modal show={showModal} className={"PaymentHistory"} bsSize="large" backdrop="static" onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Payment History
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.body}>
                {data ? 
                    <FormProvider onSubmit={this.handleSubmit.bind(this)} initialValues={editing} >
                    {formProps => {
                      return (
                        <form onSubmit={formProps.handleSubmit}>
                          <ReactTable
                            columns={this.columns}
                            data={this.state.data}
                            pageSize={this.state.data && this.state.data.length > 0 ? 8 : 0}
                            showPageJump={false}
                            resizable={false}
                            showPageSizeOptions={false}
                            previousText={"Back"}
                            pageText={""}
                            noDataText={"No entries to show."}
                          />
                        </form>
                      );
                    }}
                    </FormProvider> : <Spinner /> }
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true })
  }

  handleSubmit(values){
    const currentValues = JSON.parse(JSON.stringify(values));
    currentValues.collectedDate = this.state.collectedDate ? moment(this.state.collectedDate).format('YYYY-MM-DD') : moment(values.collectedDate).format('YYYY-MM-DD');
    currentValues.amountCollected = this.state.amountCollected ? parseInt(this.state.amountCollected) : values.amountCollected;
    this.setState(state => {
      const index = this.state.data.indexOf(this.state.editing);
      return {
        data: set(`[${index}]`, currentValues, state.data)
      };
    });
    updatePaymentHistory(currentValues);
  }

}

export default PaymentHistory;
