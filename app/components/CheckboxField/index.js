/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class CheckboxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };
  render() {
    const { data, options = {}, optionsMode, fullWidth } = this.props;
    const { required } = options.general || {};
    const columnSize = fullWidth || data.fullWidth ? 12 : 6;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={columnSize}>
        <div className={data.practiceCheck ? styles.practiceCheckContainer : styles.checkboxContainer}>
          <Field
            name={data.value}
            id={data.label}
            component="input"
            type={'checkbox'}
            {...options.general}
            {...extraFieldOptions}
            onChange={event => {
              if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
                this.props.inputchange(event.target.value, data.value);
              }
            }}
          />
           <label htmlFor={data.label} style={ data.value == "prime" || data.value == "allowAllCECourses" ? { color: "#ea6225", paddingLeft: '10px' } : data.labelStyle || { paddingLeft: '10px' }}>
            {data.label}
            {required || extraFieldOptions.required ? <span>*</span> : null}
          </label>
        </div>
      </Col>
    );
  }
}

export default CheckboxField;
