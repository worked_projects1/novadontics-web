/**
*
* MultiSelectField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field} from 'redux-form/immutable';
import Select from 'react-select';
import { sortBy } from "lodash.sortby";

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class MultiSelectField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
    metaData: PropTypes.object,
  };

  render() {
    const { data, inline, options = {}, optionsMode, metaData, infostyle } = this.props;
    const { required } = options.general || {};
    const columnSize = inline || data.fullWidth ? 12 : 6;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }
    const isPreDefinedSet = Array.isArray(data.oneOf);
    const normalize = data.number ? (value) => Number(value) : null;

    const renderOptions = isPreDefinedSet ? data.oneOf.map((c) => { return { 'label' : c, 'value' : c} }) : metaData[data.oneOf] ? metaData[data.oneOf].map(a=> Object.assign({}, a, {value: a && a.value && a.value.toString() || ''})) : [];
    let sortedRenderOptions = data.sort ? _.sortBy(renderOptions, "label") : null

    const renderMultiSelectField = ({ input, label, meta: { touched, error }, children }) => {

      const values = typeof input.value === 'number' ? input.value.toString() : input.value;
      const sortedOptionsValue = sortedRenderOptions != null ? sortedRenderOptions.filter((c)=> !values.split(',').includes(c.value)) : [];
      const selectAllOption = { label: 'Select All', value: '*'}
      if(data.allOption && sortedOptionsValue.length > 0) {
        sortedRenderOptions.unshift(selectAllOption);
      }

      return (
        <Col xs={columnSize} className={infostyle ? styles.infoField : null} style={data.accountStyle ? { paddingTop: '15px' } : null}>
              {!inline ?
                <label htmlFor={data.label}>
                  {label}
                  {required || extraFieldOptions.required ? <span>*</span> : null}
                  {data.showAsterisk ? <span>*</span> : null}
                </label> : null}
                <Select
                  name={input.name}
                  defaultValue={renderOptions && values && renderOptions.filter((c)=> values.split(',').includes(c.value))}
                  onChange={this.handleChange.bind(this, input)}
                  options={data.sort ? sortedRenderOptions : renderOptions}
                  isMulti
                  isDisabled={data.disabled}
                  className={data.providerView ? "ProviderView" : styles.MultiSelectField}
                  placeholder={data.placeholder ? data.placeholder : ''}
                />
            
        </Col>
    )}

		
    return ( 
        <Field
          name={data.value}
          id={data.label}
          label={data.label}
          component={renderMultiSelectField}
          normalize={normalize}
          onFocus={e => e.preventDefault()}
          onBlur={e => e.preventDefault()}
        />
    );
  }
  handleChange = (input, selectedOption) => {

    const { data, metaData } = this.props;
    const isPreDefinedSet = Array.isArray(data.oneOf);
    const renderOptions = isPreDefinedSet ? data.oneOf.map((c) => { return { 'label' : c, 'value' : c} }) : metaData[data.oneOf] ? metaData[data.oneOf].map(a=> Object.assign({}, a, {value: a && a.value && a.value.toString() || ''})) : [];
    let sortedRenderOptions = data.sort ? _.sortBy(renderOptions, "label") : null

    const selectAllValue = selectedOption.length > 0 ? selectedOption.filter((d) => d.value == '*').length : 0;

    if(selectAllValue != 0) {
      const value = sortedRenderOptions.length > 0 ? sortedRenderOptions.map((c) => { return c.value }).join(",") : '';
      input.onChange(value);
      if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
        this.props.inputchange(value, data.value);
      }
    } else {
      const value = selectedOption.length > 0 ? selectedOption.map((c) => { return c.value }).join(",") : '';
      input.onChange(value);
      if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
        this.props.inputchange(value, data.value);
      }
    }

  }

}

export default MultiSelectField;
