import SelectField from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const data = [
  {
    id: 1,
    label: 'test1',
    value: 'test1',
    oneOf: ['test1', 'test2'],
  },
  {
    id: 2,
    label: 'test2',
    value: 'test2',
    oneOf: ['test1', 'test2'],
  },
];

describe('<SelectField />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <SelectField
        data={data}
      />
    );
    expect(wrapper.props().data).toEqual(data);
  });
});
