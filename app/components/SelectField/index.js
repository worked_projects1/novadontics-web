/**
*
* SelectField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class SelectField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
    metaData: PropTypes.object,
    record: PropTypes.object,	
  };

  render() {
    const { data, inline, options = {},infostyle, optionsMode, metaData, record, fullWidth, halfWidth, perioStyle } = this.props;
    const { required,practiceStyle} = options.general || {};
    const columnSize = inline ? 12 : data.fullWidth ? 12 : fullWidth ? 12 : infostyle ? 12 : halfWidth ? 3 : 6;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }
    const isPreDefinedSet = Array.isArray(data.oneOf);
    const normalize = data.number ? (value) => Number(value) : null;

    const renderSelectField = ({ input, label, disabled, meta: { touched, error }, children }) => (
      <Col  xs={columnSize} className={infostyle ? styles.info : null}  style={data.noPad ? {padding:'0px'}: null}>
        {!inline && !data.hideLabel ?
          <label htmlFor={data.label}>
            <span style={data.blueTitle ? {color:"#38a0f1"} : data.orangeTitle ? {color:"#ea6225"} : null}>{data.label}</span>
            { practiceStyle ? <span style={{color:"#ea6225",fontSize:"14px"}}>*</span> : null}
            { data.hiddenAsterisk ? <span style={{color:"#fff",fontSize:"18px"}}>*</span> : null}
            {required || data.showAsterisk || extraFieldOptions.required ? <span style={data.greenTitle ? {color: "#87b44d", fontSize: "21px"} : data.blueTitle ? {color: "#38a0f1", fontSize: "18px"} : data.orangeTitle ? {color:"#ea6225"} : data.asteriskColor ? {color: "#ea6225", fontSize: "18px"} : null}>*</span> : null}
          </label> :
          data.hideLabel ?
          <label htmlFor={data.label}>
            <span style={{ color: '#fff' }}>{data.label}</span>
            {required || extraFieldOptions.required ? <span style={{ color: '#fff' }}>*</span> : null}
          </label> : null}
        <select {...input} className={disabled ? styles.disabled : data.changeStyle ? styles.perio : null} disabled={disabled} required={required} style={perioStyle ? {background: '#E5E5EA'} : null}>
          {children}
        </select>
      </Col>)

    const renderSaasSelectField = ({ input, label, disabled, meta: { touched, error }, children }) => (
      <Col>
        <Col xs={3} />
        <Col  xs={columnSize} className={infostyle ? styles.info : null}  style={data.noPad ? {padding:'0px'}: null}>
          {!inline && !data.hideLabel ?
            <label htmlFor={data.label}>
              {data.label}
              { practiceStyle ? <span style={{color:"#ea6225",fontSize:"14px"}}>*</span> : null}
              {required || extraFieldOptions.required ? <span>*</span> : null}
            </label> :
            data.hideLabel ?
            <label htmlFor={data.label}>
              <span style={{ color: '#fff' }}>{data.label}</span>
              {required || extraFieldOptions.required ? <span style={{ color: '#fff' }}>*</span> : null}
            </label> : null}
          <select {...input} className={disabled ? styles.disabled : data.changeStyle ? styles.perio : null} disabled={disabled} required={required} style={perioStyle ? {background: '#E5E5EA'} : null}>
            {children}
          </select>
        </Col>
      </Col>)
		
    return ( 
        <Field
          name={data.value}
          id={data.label}
          required={required}
          component={data.saasSelect ? renderSaasSelectField : renderSelectField}
          {...options.general}
          {...extraFieldOptions}
          normalize={normalize}
          perioStyle={perioStyle}
          onFocus={e => e.preventDefault()}
          disabled={data.disabled}
          onBlur={e => e.preventDefault()}
	        onChange={event => {
            if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
              this.props.inputchange(event.target.value, data.value);
            }
            if (data.label === "Vendor") {
              this.props.inputchange(event.target.value, data.value);
            }
          }}
        >
            {!data.optionsOnly ? <option value="" disabled={data.selectOption ? false : true}>Please select</option> : null }
            {data.Emptyoption ? <option value="" disabled={true}></option> : null }
            {data.none ? <option value="">none</option> : null }
            {data.allOption ? <option value="all">All</option> : null }
            {isPreDefinedSet ?
              (data.oneOf || []).map((option) =>
                <option key={option.value || option} value={option.value || option}>{option.label || option}</option>):
              (metaData[data.oneOf] || []).map((option) =>
                <option key={option.value} value={data.number ? parseInt(option.value) : option.value}>{option.label}</option>)}
        </Field>
    );
  }
}

export default SelectField;
