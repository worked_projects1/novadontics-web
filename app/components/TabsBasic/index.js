import React from 'react';
import PropTypes from 'prop-types';

import { Row, Col, Nav, Tab, NavItem, PanelBody, PanelTabContainer } from '@sketchpixy/rubix';

class TabsBasic extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    children: PropTypes.array,
    tabs: PropTypes.array,
  };

  constructor(props) {
    super(props);

    this.handleActiveState = ::this.handleActiveState;

    this.state = {
      activeTab: '',
    };
  }

  render() {
    const { title, tabs, children } = this.props;

    return (
      <div>
        <Row>
          <Col sm={12}>
            <h3>{title}</h3>
          </Col>
        </Row>
        <PanelTabContainer id="tabs-basic" defaultActiveKey="a1">
          <PanelBody>
            <Row>
              <Col sm={12}>
                <Nav bsStyle="tabs" onSelect={this.handleActiveState} className="tab-lightgreen">
                  {tabs.map((tab) =>
                    <NavItem eventKey={`a${tab.id}`} key={tab.id}>{tab.label}</NavItem>
                  )}
                </Nav>
                <Tab.Content>
                  {children}
                </Tab.Content>
              </Col>
            </Row>
          </PanelBody>
        </PanelTabContainer>
      </div>
    );
  }

  handleActiveState(eventKey) {
    this.setState({
      activeTab: eventKey,
    });
  }
}

export default TabsBasic;
