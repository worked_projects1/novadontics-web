import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Col, Alert } from '@sketchpixy/rubix';
import styles from './styles.css';
import remotes from 'blocks/fileupload/remotes.js';
import { capitalize, getContentType, getFileType } from './uploadUtils.js';
import "video-react/dist/video-react.css";
import { Map as iMap } from "immutable";
import Gallery from '../NovaGallery';
import ImageEditor from "components/ImageEditor";

const entityNames = {
  image: 'image',
  video: 'video',
  document: 'document',
  invoice: 'invoice',
  thumbnail: 'thumbnail',
  agreement: 'PDF/JPG',
  manuals: 'Video/PDF/JPG'
};

const StatusInfo = ({ uploadInStart, uploadFailed, uploadLimit, uploadCompleted, uploadInProgress, entity, percentage, loaded, total }) => (
  <Col className={styles.statusInfo}>
    {uploadInStart ? <Alert info>{capitalize(entity)} upload in progress.</Alert> : null}
    {uploadFailed ? <Alert danger>{capitalize(entity)} upload failed.</Alert> : null}
    {uploadLimit ? <Alert danger> Maximum {capitalize(entity)} size allowed 1.2 GB only.</Alert> : null}
    {uploadCompleted ? <Alert success>{capitalize(entity)} uploaded successfully. Submit the form to confirm changes.</Alert> : null}
    {uploadInProgress ? <Alert info>{percentage}% completed. upload in progress. <br /> {`Uploaded ${((loaded / 1024) / 1024).toFixed(2)} MB of ${((total / 1024) / 1024).toFixed(2)} MB`}</Alert> : null}
  </Col>)

class NovaUpload extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    input: PropTypes.object,
    type: PropTypes.string,
    contentType: PropTypes.string,
    label: PropTypes.string,
    data: PropTypes.object,
    renderMode: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      uploadInStart: false,
      uploadInProgress: false,
      uploadCompleted: false,
      uploadLimit: false,
      uploadFailed: false,
      publicUrl: false,
      percentage: 0,
      loaded: 0,
      total: 0
    };
    this.handleDrop = this.handleDrop.bind(this);
    this.handleCrop = this.handleCrop.bind(this);
    this.onUploadStart = this.onUploadStart.bind(this);
    this.onUploadProgress = this.onUploadProgress.bind(this);
    this.onUploadFailed = this.onUploadFailed.bind(this);
    this.onUploadCompleted = this.onUploadCompleted.bind(this);
    this.OnUploadDelete = this.OnUploadDelete.bind(this);
  }


  render() {
    const { openCrop, acceptedFiles, uploadInStart, uploadInProgress } = this.state;
    const { input, type, contentType, label, data = {}, formId , novaLabel} = this.props;
    const Input = iMap(input).toJS();
    const entity = data.novaLabel ? 'video/PDF' : entityNames[type];
    const uniqueID = new Date().getUTCMilliseconds();
    const accept = (type === 'upload') ? {} : { accept: contentType }
    return (
      <Col xs={12} className={styles.ImageUpload}>
        {label ?
          <label htmlFor={label} className={styles.label}>
            {label}
            {data.required ? <span>*</span> : null}
          </label> : null}
        <StatusInfo {...this.state} entity={entity} />
        <Dropzone onDrop={this.handleCrop.bind(this)} id={`dropzone-${uniqueID}`} className={styles.drop} activeClassName={styles.drag} {...accept} />
        <div className={styles.space} style={formId && formId === '1A' ? { marginTop: '19px', marginBottom: '19px' } : null}>
          {!uploadInStart && !uploadInProgress ? <Gallery OnDelete={(preview) => this.OnUploadDelete.bind(this, preview)} data={Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : []} entity={entity} uniqueID={uniqueID} allowed={data.allowed} saasWidth={data.saasWidth} /> : null}
          {acceptedFiles && openCrop ? <ImageEditor acceptedFiles={acceptedFiles} onClose={this.closeCrop.bind(this)} onSubmit={this.handleDrop.bind(this)} /> : null}
        </div>
      </Col>
    );
  }


  /**
   * Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
   * @param { Array } acceptedFiles
   */
  handleCrop(acceptedFiles) {
    const name = acceptedFiles[0].name.replace(/[^a-zA-Z.0-9]/g, "");
    const type = getFileType(name);
    if (type === 'image') {
      this.setState({
        openCrop: true,
        acceptedFiles
      })
    } else {
      this.handleDrop(acceptedFiles);
    }
  }

  closeCrop() {
    this.setState({
      openCrop: false,
      acceptedFiles: false
    })
  }

  /**
   * Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
   * @param { Array } acceptedFiles
   */
  handleDrop(acceptedFiles) {
    this.closeCrop();
    const { uploadInStart, uploadInProgress } = this.state;
    const fileSize = acceptedFiles[0] ? ((acceptedFiles[0]['size'] / 1024) / 1024).toFixed(2) : 0;
    const name = acceptedFiles[0].name.replace(/[^a-zA-Z.0-9]/g, "");
    const fileContentType = getContentType(name);

    if (uploadInStart || uploadInProgress) {
      return false;
    } else if (acceptedFiles.length == 0) {
      this.onUploadFailed();
      return false;
    } else if (fileSize > 1200) {
      this.onUploadLimit();
      return false;
    }

    this.onUploadStart();
    const ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", this.onUploadProgress, false);
    ajax.addEventListener("load", this.onUploadCompleted, false);
    ajax.addEventListener("error", this.onUploadFailed, false);
    ajax.addEventListener("abort", this.onUploadFailed, false);
    remotes.getSignature(name, fileContentType)
      .then((result) => {
        const { uploadUrl, publicUrl } = result;
        if (uploadUrl && publicUrl) {
          this.setState({ publicUrl: publicUrl })
          ajax.open("PUT", uploadUrl, true);
          ajax.setRequestHeader("Content-Type", fileContentType);
          ajax.send(acceptedFiles[0]);
        } else {
          this.onUploadFailed();
        }
      })
      .catch(() => {
        this.onUploadFailed();
      });
  }

  /**
   * Set upload start in local state.
   * 
   */
  onUploadStart() {
    this.setState({
      uploadInStart: true,
      uploadInProgress: false,
      uploadFailed: false,
      uploadLimit: false,
      uploadCompleted: false,
    });
  }

  /**
   * Set upload progress in local state.
   * @param { Array } files
   */
  onUploadProgress(event) {
    this.setState({
      uploadInStart: false,
      uploadInProgress: true,
      uploadFailed: false,
      uploadLimit: false,
      uploadCompleted: false,
      percentage: Math.round((event.loaded / event.total) * 100),
      loaded: event.loaded,
      total: event.total,
    });
  }

  /**
   * Set upload error in local state.
   */
  onUploadFailed() {
    this.setState({
      uploadInStart: false,
      uploadInProgress: false,
      uploadFailed: true,
      uploadLimit: false,
      uploadCompleted: false
    });
    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadFailed: false
      });
    }, 3000);
  }

  /**
   * Set upload limit in local state.
   */
  onUploadLimit() {
    this.setState({
      uploadInStart: false,
      uploadInProgress: false,
      uploadFailed: false,
      uploadLimit: true,
      uploadCompleted: false
    });

    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadLimit: false,
      });
    }, 3000);
  }

  /**
   * Set upload success in local state.
   */
  onUploadCompleted() {
    const { input, data = {} } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : [];
    if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
      this.props.inputchange(this.state.publicUrl, data.value);
    }
    inputValue.push(this.state.publicUrl);
    input.onChange(data.saveType === 'string' ? inputValue.join(',') : inputValue);

    this.setState({
      uploadInStart: false,
      uploadInProgress: false,
      uploadCompleted: true,
      uploadLimit: false,
      uploadFailed: false,
    });

    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadCompleted: false,
      });
    }, 2000);

  }

  /**
   * Set upload delete in local file.
   */
  OnUploadDelete(preview) {
    const { input, data = {} } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : [];
    input.onChange(data.saveType === 'string' ? inputValue.filter((val) => val != preview).join(',') : inputValue.filter((val) => val != preview))
  }


}

export default NovaUpload;