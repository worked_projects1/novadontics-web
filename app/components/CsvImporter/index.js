import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Button, Modal } from '@sketchpixy/rubix';
import { Link } from 'react-router';
import styles from './styles.css';
import Dropzone from 'react-dropzone';
import Papa from 'papaparse'
import { isEqual } from 'lodash';
import Spinner from 'components/Spinner';

class CsvImporter extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    name: PropTypes.string,
    loading: PropTypes.bool,
    columns: PropTypes.array.isRequired,
    handleImport: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.handleClick = ::this.handleClick.bind(this);
    this.hideModal = ::this.hideModal.bind(this);
    this.handleDrop = ::this.handleDrop.bind(this);
    this.previewFiles = ::this.previewFiles.bind(this);
    this.handleParseDone = ::this.handleParseDone.bind(this);
    this.handleResponse = ::this.handleResponse.bind(this);
    this.state = {showModal: false, files: [], errors: [], results: []};
  }

  handleClick(e){
    e.preventDefault();
    this.setState({showModal: true})
  }

  hideModal(){
    this.setState({showModal: false, files: [], errors: [], results: []})
  }

  handleParseDone(result, file){
    const {data, errors, meta} = result;
    const { fields } = meta;
    const { columns } = this.props;

    if(!isEqual(columns.sort(), fields.sort())){
      this.setState({errors: this.state.errors.concat({message: `Input csv must have columns: ${columns.toString().replace(',', ', ')}`})});
      return
    }

    this.setState({showSpinner: false});
    if(errors.length > 0){
      this.setState({errors: this.state.errors.concat(errors)});
    } else {
      this.setState({files: this.state.files.concat(file),
                     results: this.state.results.concat(data)
                    });
    }
  }

  handleDrop(files){
    const current = this.state.files;
    const unique = files.filter(f => (current.map(c => c.name === f.name).indexOf(true) === -1) );
    const fileSize = files[0] ? Math.ceil(files[0].size / 1000) : 0;
    this.setState({errors: []});

    if(fileSize > 2400){
      this.setState({errors: this.state.errors.concat({message: `File size should not be greater than 2 MB`})})
      return false;
    }

    unique.forEach((f) => {
      this.setState({showSpinner: true});
      Papa.parse(f, {
        skipEmptyLines: true,
        header: true,
        delimiter: ",",
        complete: this.handleParseDone
      })
    });
  }

  previewFiles(files){
    return(
      <div className={styles.preview}>
        {files.map(file =>
          <div key={file.name}>
            <h5 style={{display:'inline-block'}}>{file.name}</h5>
            <h5 style={{float: 'right', display:'inline-block'}}>{`${Math.ceil(file.size / 1000)} KB`}</h5>
          </div>
        )}
      </div>
    )
  }

  handleResponse(data){
    if(data){
      const {error} = data;
      this.setState({'errors': [{message: error.description}]});
      setTimeout(() => {this.setState({files: [], errors: [], results: []})}, 2000)
    } else {
      setTimeout(() => this.hideModal(), 1000)
    }
  }

  render() {
    const { handleImport, name = 'data', loading = false } = this.props;
    const { showModal, files, errors, results } = this.state;
    let i = 0;

    let preview = null;
    if(files.length > 0) {
      preview = this.previewFiles(files);
    }

    return(
      <div className={styles.csvImport}>
        <Link to='' onClick={ this.handleClick }>
          <h5 style={{ display: 'flex', width: '100px' }}>
            <span style={{ marginRight: '5px' }}><Icon glyph="icon-fontello-upload-1" /></span>
            Import CSV
          </h5>
        </Link>

        <Modal show={showModal} onHide={this.hideModal}>
          <Modal.Header closeButton>
            <Modal.Title>Import {name.toLowerCase()}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            {loading ?
              <Spinner/> :
              <Dropzone onDrop={this.handleDrop} className={styles.drop} activeClassName={styles.drag} accept={"text/*,application/vnd.ms-excel"}>
                {errors.length > 0 ?
                  <div className={styles.error}>
                    {errors.map(error => <p key={i++}>{ (error.row ? `Row ${error.row}: ` : '') + `${error.message}` }</p>)}
                  </div>
                  : <div className={styles.text}>Drag and drop your csv file here, or click to select files to upload.</div>
                }
              </Dropzone>
            }
            { preview }
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={this.hideModal}>Cancel</Button>
            <Button bsStyle='primary' disabled={files.length === 0 || errors.length > 0} onClick={handleImport.bind(this, results, this.handleResponse)}>Import</Button>
          </Modal.Footer>
        </Modal>

      </div>
    )
  }
}

export default CsvImporter;