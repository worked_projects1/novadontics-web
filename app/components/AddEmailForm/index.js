/**
 *
 * AddEmailForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import { reduxForm } from 'redux-form/immutable';


import SelectField from 'components/SelectField';


import styles from './styles.css';

import {Col, Button } from '@sketchpixy/rubix';

const AddEmailForm = (props) => {
  const { handleSubmit, data, metaData} = props;
  return (
    <div className={styles.row}>
      <form onSubmit={handleSubmit}>
      <Col xs={8} className={styles.lCol}>
        <SelectField
          data={data}
          inline={true}
        />
      </Col>
      <Col xs={4} >
        <Button bsStyle={"standard"} type="submit">Add</Button>
      </Col>
      </form>
    </div>
  );
};

AddEmailForm.propTypes = {
  handleSubmit: PropTypes.func,
  data: PropTypes.object,
  metaData: PropTypes.object,
};

export default reduxForm({
  form: 'addEmail',
})(AddEmailForm);
