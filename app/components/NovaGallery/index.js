/**
*
* Gallery
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Player } from 'video-react';
import { Col, Image, Icon } from '@sketchpixy/rubix';
import DeleteButton from 'containers/PatientsPage/components/DeleteButton';
import { getFileName, getUploadDate, getFileType } from './galleryUtils';
import BigPicture from 'bigpicture';
import styles from './styles.css';


const FileView = ({ name, uploadDate, type, preview, handleOpen, viewOnly, saasWidth }) => {
  switch (type) {
    case 'image':
      return <Col className={saasWidth ? styles.saasPreview : styles.previewImg} onClick={handleOpen}>
        <Image src={preview} responsive />
        {viewOnly == true && uploadDate == 'Invalid date' ? null :
          <span className={styles.uploadDate}>{uploadDate || ''}</span>}
      </Col>
    case 'video':
      return <Col className={styles.previewImg}  onClick={handleOpen}>
        <Image href={preview.url || preview} className={styles.img} src={preview && preview.thumbnail != '' && preview.thumbnail || 'https://s3.amazonaws.com/novadontics-uploads/dataurifile1584343276879-1584343277318.png'} responsive />
        <span className={styles.uploadDate}>{uploadDate || ''}</span>
        <span className={styles.play}>
          <Image width={60} src={require('img/catalog/play4.png')} />
        </span>
      </Col>
    default:
      return <Col className={styles.previewFile} onClick={handleOpen}>
        <Col>
          <Image src={require(`img/patients/${type === 'pdf' ? 'pdf3' : type}.svg`)} responsive />
          <span>{name.length > 25 ? name.slice(0, 25) + '...' : name}</span>
        </Col>
        <span className={styles.uploadDate}>{uploadDate || ''}</span>
      </Col>
  }
}


class NovaGallery extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.array,
    entity: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
    this.handleNext = :: this.handleNext;
    this.handlePrev = :: this.handlePrev;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { data = [], entity, uniqueID, allowed, OnDelete, viewOnly, saasWidth } = this.props;
    const { photoIndex, isOpen } = this.state;
    const nextImage = (photoIndex + 1) % data.length;
    const prevImage = (photoIndex + (data.length - 1)) % data.length;
    const fileType = getFileType(data[photoIndex]);
    const mainImage = fileType && (fileType === 'image' || fileType === 'video') ? data[photoIndex] : require(`img/patients/${fileType === 'pdf' ? 'pdf3' : fileType}.svg`);
    return (
      <Col id="files" className={styles.gallery}>
        {data.map((preview, index) => {

          const name = getFileName(preview);
          const uploadDate = getUploadDate(preview);
          const type = getFileType(preview);

          return <Col key={`${type}-${index}-${uniqueID}`} className={styles.preview}>
            <FileView
              name={name}
              type={type}
              uploadDate={uploadDate}
              preview={preview}
              viewOnly={viewOnly}
              saasWidth={saasWidth}
              handleOpen={() => this.handleOpen(index)} />
            {viewOnly ? null : <DeleteButton type={type} OnDelete={OnDelete && OnDelete(preview)} />}
          </Col>
        })}

        {allowed === data.length || viewOnly ? null :
          <Col onClick={() => document.getElementById(`dropzone-${uniqueID}`).click()} className={styles.upload}>
            <img src={require('img/patients/AddPhoto.svg')} alt={'upload'} className={styles.img} />
            <Col style={{ textTransform: 'capitalize' }}>{`Add ${entity}`}</Col>
          </Col>}

        {isOpen &&
          <Lightbox
            imageTitle="image"
            mainSrc={mainImage}
            nextSrc={data[nextImage]}
            prevSrc={data[prevImage]}
            onCloseRequest={this.handleClose}
            onMovePrevRequest={() => this.handlePrev(prevImage)}
            onMoveNextRequest={() => this.handleNext(nextImage)}
          />
        }
      </Col>
    );
  }

  handleOpen(i) {
    const { data = [] } = this.props;
    const file = data[i] || '';
    const type = getFileType(file);
    if (type === 'image') {
      this.setState({
        isOpen: true,
        photoIndex: i,
      });
    } else if(type === 'video'){
        BigPicture({
          el: document.getElementById('files'),
          vidSrc: file.url || file
        });
    } else {
      window.open(file, '_blank');
    }
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handlePrev(nextImage) {
    this.setState({ photoIndex: nextImage });
  }

  handleNext(prevImage) {
    this.setState({ photoIndex: prevImage });
  }
}

export default NovaGallery;
