import React from 'react';
import PropTypes from 'prop-types';
import { Col, Icon } from '@sketchpixy/rubix';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import styles from './styles.css';

class ModernDatepicker extends React.Component {
  static propTypes = {
    input: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = { startDate: false };
    this.handleDate = this.handleDate.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.openDatePicker = this.openDatePicker.bind(this);
  }
  

  render() {
    const { input, data, clearable,infostyle } = this.props;
    const { startDate } = this.state;
    return (
        <Col className={infostyle ? styles.pickerWidth : data.changeStyle ? styles.perio : null}>
         <DatePicker 
          selected={input.value ? moment.utc(input.value) : startDate ? startDate : data.defaultDate ? moment.utc() : startDate}
          format={'DD MMM, YYYY'} 
          peekNextMonth
          showMonthDropdown
          showYearDropdown   
          infostyle={infostyle} 
          dropdownMode="select"      
          autoComplete="off"
          id={data.value}
          className={data.stylenew !=undefined?styles.InputField :styles.DatePicker}
          onChange={(selected) => this.handleDate(selected)}
          minDate={!data.noPad && !data.showAll ? moment(new Date("1/1/1901").valueOf()).subtract('1','days') : null}
          ref={(c) => this._calendar = c}
          />
          {clearable ? <Icon className={styles.clear} glyph="icon-fontello-cancel-circle" onClick={()=>this.handleClear()}/> : null}
          {data.calendar ? <Icon className={styles.calendar} glyph="icon-fontello-calendar" onClick={()=>this.openDatePicker()}/> : null}
        </Col>
    );
  }

  handleDate(selected) {
    const { input, data } = this.props;
    this.setState({ startDate: selected });
    if(selected != "" && selected != null) {
      input.onChange(moment(selected).format('YYYY-MM-DD'));
    } else {
      input.onChange("");
    }
    if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
      this.props.inputchange(selected, data.value);
    }
  }

   handleClear() {
    const {input} = this.props;
    this.setState({startDate:false});
    input.onChange('')
  }

  openDatePicker() {
    this._calendar.setOpen(true);
  }

}

export default ModernDatepicker;
