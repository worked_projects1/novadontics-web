/**
*
* EditRecordForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm, change } from 'redux-form/immutable';

import { ImplementationFor, ContentTypes } from 'components/CreateRecordForm/utils';

import styles from './styles.css';

import { Row, Col, Button, Alert } from '@sketchpixy/rubix';

const EditRecordForm = (props) => {
  const { handleSubmit, pristine, submitting, fields, updateRecordsMetaData, dispatch, displaybtn, error, displayText, inline, path, metaData, locationState, record, btnName, disableCancel, disableSubmit, catalogButton, catalogNotes, subscriberBtn, hideActionButton, updateContracts, initialValues, documentUpdate, form, name, patientSubmit, sharedId, handleShare, disableShare } = props;

  const buttonStyle = inline ? 'link' : 'standard';
  const buttonClass = inline ? styles.buttonLink : subscriberBtn ? styles.subscriberButton : styles.buttonStandard;
  const submitClass = styles.submitStandard;
  const buttonGroupClass = !inline ? styles.actionButtons : null;

  const handleClose = () => {
    setTimeout(() => {
      fields.map((field) => {
        if(field.value == 'name' || field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
          field.editRecord = false
        }
      })
    }, 1500)
  }

  const handleChange = (val, value) => {
    if (value === "manufacturer") {
      const options = metaData.categoriesList && metaData.categoriesList.length > 0 ? metaData.categoriesList.filter(function (order1) {
        return val === order1.label;
      }) : false;
      dispatch(updateRecordsMetaData(Object.assign({}, {
        categories: options && options.length > 0  ?
        options: metaData.categoriesList
      })))
    } else {
      fields.filter(function (el) {
        return el.value == "amount" || el.value == "mode" ?
          el.editRecord = (val == 'Closed' ? true : false) :
          el.value == "collectedDate" || el.value == "amountCollected" ?
            el.editRecord = (val == 'Paid' ? true : false)
            : el
      })
    }
    if(form == `editSharedPatient.${sharedId}`) {
      if(value == 'patientConsent') {
        handleShare(val);
      }
    }
    if(value == 'documentType') {
      props.updateType(val);
    }
    if(value == 'year') {
      props.updateYear(val);
    }
    if(value == 'uploadLink') {
      dispatch(change(form, "link", val));
    }
    if(value == 'sectionType') {
      if (val === 'tutorials') {
        fields.map((field) => {
          if(field.value == 'name' || field.value == 'link' || field.value == 'parent') {
            field.editRecord = true
          }
        })
      }
      if (val === 'manuals') {
        fields.map((field) => {
          if(field.value == 'name' || field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
            field.editRecord = true
          }
        })
      }
      if (val === 'section') {
        fields.map((field) => {
          if(field.value == 'name') {
            field.editRecord = true
          }
          if(field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
            field.editRecord = false
          }
        })
      }
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <Row>
        {fields.map((field, i) => {
          const Component = ImplementationFor[field.type];
          const contentType = ContentTypes[field.type];
          return (
            (field.editRecord ?
              <div key={i} className="hidden-print">
                <Component
                  data={field}
                  contentType={contentType}
                  inline={inline}
                  options={field.formFieldDecorationOptions}
                  optionsMode="edit"
                  metaData={metaData}
                  fields={fields}
                  updateContracts={updateContracts}
                  initialValues={initialValues}
                  documentUpdate={documentUpdate}
                  record={record}
                  editPage={true}
                  inputchange={(val, value) => handleChange(val, value)}
                />
              </div>
              : null)
          );
        })}
      </Row>
      {displayText ?
        <Row className={styles.requestform}>
          <Col>
            <span style={{ fontWeight: '500' }}>
              Thank you for requesting CE credits for this course. You will receive an automated email shortly from Novadontics with a questionnaire related to this course material.
              After sending the completed questionnaire back to Novadontics, a CE certificate will be generated with the appropriate CE credits and emailed to you by Novadontics.
            </span></Col>
        </Row> : null}
      {patientSubmit ? <div className={styles.hrStyle}><hr /></div> : null}
      {hideActionButton ? null :
        patientSubmit ?
          <Row className="hidden-print">
            <Col xs={12} className={styles.patientFormStyle}>
                <Button bsStyle="standard" type="button" className={styles.cancelBtn} onClick={props.onCancel.bind(this)}>{props.cancelBtnName || 'Cancel'}</Button>
                <Button bsStyle={buttonStyle} type="submit" disabled={disableShare ? true : !disableShare && (pristine || submitting)} disabled={disableShare ? true : !disableShare && (pristine || submitting)} className={submitting ? `${submitClass} buttonLoading` : submitClass}>{btnName || 'Submit'}</Button>
            </Col>
          </Row> :
          <Row className="action-button hidden-print">
            <Col xs={12} className={catalogButton ? styles.catalogButtonStyle : catalogNotes ? styles.catalogNotesStyle : buttonGroupClass}>
              {displayText ? <Button bsStyle={buttonStyle} type="submit" className={submitting ? `${buttonClass} buttonLoading` : buttonClass && styles.subrequest} >Submit Request</Button> :
                <Button bsStyle={buttonStyle} type="submit" disabled={!disableSubmit && (pristine || submitting) || disableSubmit} className={name == "saas" || name == "feeSchedule" ? "btn-standard-green btn btn-default" : submitting ? `${buttonClass} buttonLoading` : buttonClass}>{btnName || 'Save'}</Button>}
              {!inline && !disableCancel ? <Link to={{ pathname: path, state: locationState }} onClick={name == "tutorials.edit" || name == "manuals.edit" ? handleClose : null}>Cancel</Link> : null}
              {catalogButton ? <Button bsStyle="standard" type="button" className={styles.cancelBtn} onClick={props.onCancel.bind(this)}>{props.cancelBtnName || 'Cancel'}</Button> : null}
            </Col>
          </Row>}
      <Row>
        <Col xs={12}>
          {error ?
            <Alert danger>{error}</Alert> : null}
        </Col>
      </Row>
    </form>
  );
};

EditRecordForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  inline: PropTypes.bool,
  path: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
  record: PropTypes.object,
};

export default reduxForm({
  form: 'editRecord',
  enableReinitialize: true
})(EditRecordForm);


