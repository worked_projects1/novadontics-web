/**
*
* EditProductForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm, change } from 'redux-form/immutable';

import { ImplementationFor, ContentTypes } from 'components/CreateRecordForm/utils';

import styles from './styles.css';

import { Row, Col, Button, Alert } from '@sketchpixy/rubix';

class EditProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { metaData: props.metaData };
  }

  componentDidMount() {
    const { initialValues } = this.props;
    const { metaData } = this.state;
    let initialValue = initialValues.toJS();
    let parentMetaData = this.props.metaData;
    this.setState({ metaData: parentMetaData && parentMetaData.categoryParentOptions && parentMetaData.categoryParentOptions.length > 0 && Object.assign({}, parentMetaData, {categoryParentOptions: initialValue.filteredCategories && initialValue.filteredCategories.length > 0 ? initialValue.filteredCategories : parentMetaData.categoryParentOptions}, {categoryChildOptions: parentMetaData.categoryChildOptions.filter(_ => _.parent == initialValue.parentId)}) })
  }

  render() {
    const { handleSubmit, pristine, submitting, fields, updateRecordsMetaData, dispatch, displaybtn, error, displayText, inline, path, locationState, record, btnName, disableCancel, disableSubmit, catalogButton, form } = this.props;
    const { metaData } = this.state;

    const buttonStyle = inline ? 'link' : 'standard';
    const buttonClass = inline ? styles.buttonLink : styles.buttonStandard;
    const buttonGroupClass = !inline ? styles.actionButtons : null;

    const handleChange = (val, value) => {
      if (value === "manufacturer") {
        const vendorId = metaData.vendorOptions && metaData.vendorOptions.length > 0 ? metaData.vendorOptions.find(_ => _.name == val).id : null;
        const categoryData = metaData.vendorCategoryOptions && vendorId != null && metaData.vendorCategoryOptions.length > 0 ? metaData.vendorCategoryOptions.find(_ => _.vendorId == vendorId) : null;
        const categoryId = categoryData != undefined ? categoryData.categories : null;
        const categoryIdArray = categoryId != null ? categoryId.split(',').map((x) =>parseInt(x)) : null;
        const filteredCategories = metaData.categoriesList && metaData.categoriesList.length > 0 && categoryIdArray && categoryIdArray.length > 0 ? metaData.categoriesList.filter(val => categoryIdArray.includes(val.id)) : [];
          let parentMetaData = this.props.metaData;
          this.setState({ metaData: parentMetaData && parentMetaData.categoryParentOptions && parentMetaData.categoryParentOptions.length > 0 && Object.assign({}, parentMetaData, {categoryParentOptions: filteredCategories && filteredCategories.length > 0 ? filteredCategories : parentMetaData.categoryParentOptions}) })
          dispatch(change(form, "parentId", ""))
          dispatch(change(form, "childId", ""))
      } else {
        fields.filter(function (el) {
          return el.value == "amount" || el.value == "mode" ?
            el.editRecord = (val == 'Closed' ? true : false) :
            el.value == "collectedDate" || el.value == "amountCollected" ?
              el.editRecord = (val == 'Paid' ? true : false)
              : el
        })
      }
      if (value === "parentId") {
          const vendorName = document.getElementsByName("manufacturer")[0].value;
          const vendorId = metaData.vendorOptions && metaData.vendorOptions.length > 0 ? metaData.vendorOptions.find(_ => _.name == vendorName).id : null;
          const categoryData = metaData.vendorCategoryOptions && vendorId != null && metaData.vendorCategoryOptions.length > 0 ? metaData.vendorCategoryOptions.find(_ => _.vendorId == vendorId) : null;
          const categoryId = categoryData != undefined ? categoryData.categories : null;
          const categoryIdArray = categoryId != null ? categoryId.split(',').map((x) =>parseInt(x)) : null;
          const filteredCategories = metaData.categoriesList && metaData.categoriesList.length > 0 && categoryIdArray && categoryIdArray.length > 0 ? metaData.categoriesList.filter(val => categoryIdArray.includes(val.id)) : [];
          let childMetaData = this.props.metaData;
          this.setState({ metaData: childMetaData && childMetaData.categoryChildOptions && Object.assign({}, childMetaData, {categoryParentOptions: filteredCategories && filteredCategories.length > 0 ? filteredCategories : childMetaData.categoryParentOptions}, {categoryChildOptions: childMetaData.categoryChildOptions.filter(_ => _.parent == val)}) })
          dispatch(change(form, "childId", ""))
      }
    }

    return (
      <form onSubmit={handleSubmit}>
        <Row>
          {fields.map((field, i) => {
            const Component = ImplementationFor[field.type];
            const contentType = ContentTypes[field.type];
            return (
              (field.editRecord ?
                <div key={i} className="hidden-print">
                  <Component
                    data={field}
                    contentType={contentType}
                    inline={inline}
                    options={field.formFieldDecorationOptions}
                    optionsMode="edit"
                    metaData={metaData}
                    record={record}
                    inputchange={(val, value) => handleChange(val, value)}
                  />
                </div>
                : null)
            );
          })}
        </Row>
        {displayText ?
          <Row className={styles.requestform}>
            <Col>
              <span style={{ fontWeight: '500' }}>
                Thank you for requesting CE credits for this course. You will receive an automated email shortly from Novadontics with a questionnaire related to this course material.
                After sending the completed questionnaire back to Novadontics, a CE certificate will be generated with the appropriate CE credits and emailed to you by Novadontics.
              </span></Col>
          </Row> : null}
        <Row className="action-button hidden-print">
          <Col xs={12} className={catalogButton ? styles.catalogButtonStyle : buttonGroupClass}>
            {displayText ? <Button bsStyle={buttonStyle} type="submit" className={submitting ? `${buttonClass} buttonLoading` : buttonClass && styles.subrequest} >Submit Request</Button> :
              <Button bsStyle={buttonStyle} type="submit" disabled={!disableSubmit && (pristine || submitting) || disableSubmit} className={submitting ? `${buttonClass} buttonLoading` : buttonClass}>{btnName || 'Save'}</Button>}
            {!inline && !disableCancel ? <Link to={{ pathname: path, state: locationState }}>Cancel</Link> : null}
            {catalogButton ? <Button bsStyle="standard" type="button" className={styles.cancelBtn} onClick={props.onCancel.bind(this)}>{props.cancelBtnName || 'Cancel'}</Button> : null}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            {error ?
              <Alert danger>{error}</Alert> : null}
          </Col>
        </Row>
      </form>
    );
  }
};

EditProductForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  inline: PropTypes.bool,
  path: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
  record: PropTypes.object,
};

export default reduxForm({
  form: 'editRecord',
  enableReinitialize: true
})(EditProductForm);


