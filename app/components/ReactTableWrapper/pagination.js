import React from "react";
import { Col, Pagination } from '@sketchpixy/rubix';
import PropTypes from "prop-types";
import styles from './styles.css';

class ReactTablePagination extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    data: PropTypes.array,
    pageSize: PropTypes.number,
    page: PropTypes.number,
    onPageChange: PropTypes.func,
  };


  render() {
    const { pageSize, headers = {}, page, pages, name  } = this.props;
    const { totalcount, pageNumber, recPerPage } = headers;
    const startCount = ((pageNumber * recPerPage) - recPerPage) + 1;
    const endCount = (pageNumber * recPerPage) < parseInt(totalcount) ? (pageNumber * recPerPage) : parseInt(totalcount);
    const totalPages = totalcount && recPerPage ? Math.ceil(parseInt(totalcount)/parseInt(recPerPage)) : totalcount && pageSize ? Math.ceil(parseInt(totalcount)/parseInt(pageSize)) : 0;
    return (
      <Col className="ReactTablePagination" style={name === 'implantLog' || name === "implantData" || name === "boneGraftLog" ? { display: 'flex', justifyContent: 'space-between' } : null}>
        {(name === 'implantLog' || name === "implantData" || name === "boneGraftLog") && totalcount > 0 ?
          <span className={styles.countStyle}>
            Showing {startCount} to {endCount} of {totalcount} entries.
          </span> : <span></span>}
        <Pagination
          prev = "Back"
          next = "Next"
          boundaryLinks
          items={totalcount ? totalPages : pages}
          maxButtons={4}
          activePage={totalcount ? pageNumber : page+1}
          onSelect={this.handlePagination.bind(this)}
        />
      </Col>
    );
  }

  handlePagination(e){
    const { headers = {}, onPageChange } = this.props;
    const { totalcount } = headers;
    if(totalcount) {
      this.props.filter('pageNumber', e);
    } else {
      onPageChange(e-1)
    }
  }
}

export default ReactTablePagination;