/**
*
* ReactTableFilters
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class ReactTableFilters extends React.Component { // eslint-disable-line react/prefer-stateless-function
  
  constructor(props) {
    super(props);
    this.handleSearchText = this.handleSearchText.bind(this);
  }

  componentDidMount() {
    var searchText = document.getElementById("searchText");
    searchText.addEventListener("keyup", this.handleSearchText);
  }

  componentWillUnmount() {
    var searchText = document.getElementById("searchText");
    searchText.removeEventListener("keyup", this.handleSearchText);
  }

  handleSearchText(event) {
    setTimeout(function(){  this.props.filter('searchText',event.target.value) }.bind(this), 1000);
  }

  render() {
    const { filter, headers = {}, path } = this.props;
    const { searchText, recPerPage, status  } = headers;
    const secret = localStorage.getItem('secret');
    let name = path.replace(/\//g, "");
    if(name === 'catalog'){
      name = 'products';
    }
    const CSVRouter = `${process.env.API_URL}/${name}/generateCSV?X-Auth-Token=${secret && secret.slice(1, -1)}${status != '' ? '&&status='+status : ''}`;
    
    return (
        <Col className={styles.ReactTableFilters}>
            <Col>
              <div className={styles.reactTableSizepicker}>
                Entries per page:
                <select className={styles.reactTableSizepickerSelectfield} onChange={(e)=>filter('recPerPage',e.target.value)} defaultValue={recPerPage}>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
              </div>
              <div className={styles.reactTableSearch}>
                  <label className={styles.reactTableSearchLabel}>
                      Search:  
                      <input type="text" id="searchText" className={styles.reactTableSearchInput} placeholder="Type to search…" defaultValue={searchText}/>
                  </label>
              </div> 
            </Col>  
            <Col>
              <div className={styles.reactTableExportButtonWrapper}>&nbsp;
                {name === "products" || name === "implantLog" || name === "boneGraftLog" ? null :
                  <button onClick={() => window.open(CSVRouter,`_self`)} className={styles.reactTableExportButton}>Export CSV</button> }
              </div>
            </Col>
        </Col>
    );
  }
  
 
}

export default ReactTableFilters;
