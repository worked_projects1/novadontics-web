/**
*
* ReactTableWrapper
*
*/

import React from 'react';
import ReactTable from "react-table";
import { Col, Tabs, Tab, PanelContainer, PanelBody } from '@sketchpixy/rubix';
import ReactTableFilters from './filters';
import styles from './styles.css';

class ReactTableWrapper extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
  }

  render() {
    const { defaultStatusKey, statusColumn,patientsColStyle, loadingRecords, headers = {}, user = {}, hideFilters, name } = this.props;
    return statusColumn ?
      (<Tabs id="ReactTable" defaultActiveKey={defaultStatusKey} onSelect={(e) => this.props.filter('status', statusColumn.oneOf[e])}>
        <Col sm={12} className={styles.filter}>
          <ReactTableFilters filter={(type, input) => this.props.filter(type, input)}  {...this.props} />
        </Col>
        {(statusColumn.oneOf && statusColumn.splitColumn ? statusColumn.oneOf.map((prop, i) =>
          <Tab key={i} eventKey={i} title={prop} disabled={loadingRecords && headers.status !== prop ? true : false}>
            <PanelContainer>
              <PanelBody>
              <Col sm={12} id={ patientsColStyle ? "patientsColStyle" : null}>
                  <ReactTable {...this.props} />
                </Col>
              </PanelBody>
            </PanelContainer>
          </Tab>
        ) : null)}
      </Tabs>) :
      (<PanelContainer>
        <PanelBody>
        <Col sm={12} id={ name == 'implantLog' ? "ImplantLog" : name == 'boneGraftLog' ? "BoneGraftLog" : name === "saasAdmin" ? "SaasAdminPage" : patientsColStyle ? "patientsColStyle" : null} className={name == 'implantData' ? styles.implantReport : null}>
            {hideFilters ? null :
              <ReactTableFilters filter={(type, input) => this.props.filter(type, input)}  {...this.props} />}
            <ReactTable {...this.props} />
          </Col>
        </PanelBody>
      </PanelContainer>);

  }


}

export default ReactTableWrapper;
