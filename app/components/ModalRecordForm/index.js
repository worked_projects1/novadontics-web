/**
*
* ModalRecordForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm, change } from 'redux-form/immutable';
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import { Link } from 'react-router';
import styles from './styles.css';
import moment from 'moment-timezone';
import Info from 'containers/PatientsPage/components/Info';

const ButtonItem = ({config, open}) => {
  const { title, glyphIcon, imageIcon, btnType, width, height, printIcon, referIcon, inviteButton } = config;
  switch(btnType){
    case 'link':
      return <Link className={referIcon ? styles.referStyles : printIcon ? styles.printLink : styles.link} onClick={open}>
                <Icon className={referIcon ? styles.referIcon : styles.icon} glyph={glyphIcon ? glyphIcon : null} />{title || ''}
             </Link>
    case 'image':
      return <Col onClick={open}>
              {imageIcon && imageIcon.map((image,index)=><img key={index} width={width} height={height} src={image} alt={title || ''} className={styles.img} />)}
             </Col>
    default:
      return <Button bsStyle="standard" onClick={open} className={inviteButton ? `action-button ${styles.button}` : `action-button`}>{title || ''}</Button>
  }
}

const Children = ({children}) => children ? <Col style={{textAlign:"center"}} dangerouslySetInnerHTML={{__html: children}} /> : null;

const Tooltip = ({text, show}) => show ? <span className={styles.tooltip}>{text}</span> : null;

class ModalRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false, confirmDialogBox : false, disableShare: false };
  }

  componentDidMount(){
    if(this.props.onRef) {
      this.props.onRef(this);
    }
    if(this.props.form == 'createSharedPatient') {
      this.setState({ disableShare: true })
    }
    if(this.props.form == `editSharedPatient.${this.props.sharedId}`) {
      this.setState({ disableShare: true })
    }
  }
  
  componentWillReceiveProps(props) {
    if(this.props.error != props.error && this.props.config && this.props.config.showError) {
      this.props.config.showError(props.error);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { fields, initialValues, handleSubmit, error, updateError, pristine, submitting, config = {}, noteText, notes, dispatch, form, sharedId, infoColumns, metaData, getCategories, updateCtScan } = this.props;
    const { confirmDialogBox, disableShare} = this.state;
    const { recallProcedureOptions = [] } = metaData || {}
    const { centerAlign, confirmDialog, title, bsSize, tooltip, viewType, shareDet, topView, bottomView, closeModal, modalClass, btnName, className, style, disable, titleStyle, hideSaveButton, hideCancelBtn } = config;
    const RowComponent = viewType == 'tab' ? Tabs : Row;
    const ColComponent = viewType == 'tab' ? Tab : 'div';

    const handleChange = (val, name) => {
      fields.filter(function(el) {
        return (el.value == "checkNumber") ? el.editRecord = ((val == 'Check') ? true : false)
        : el })

      if(form == 'createSharedPatient') {
        if(name == 'patientConsent') {
          val == "true" ? this.setState({ disableShare: true }) : this.setState({ disableShare: false })
        }
      }
      if(form == `vendorCategories`) {
        if(name == 'vendorId') {
          this.props.getCategories(val);
        }
      }
      if(form == 'recallPeriodRecord') {
        if(name == 'recallType') {
          recallProcedureOptions.map(recallProcedure => {
            if(recallProcedure.recallType == val) {
              dispatch(change(form, "recallCode", recallProcedure.code));
              dispatch(change(form, "recallCodeDescription", recallProcedure.description));
            }
          })
        } else if(name == 'intervalNumber') {

          let fromDate = document.getElementById("recallFromDate").value
          let oneDayPlus = document.getElementsByName("recallOneDayPlus")[0].value
          let intervalValue = document.getElementsByName("intervalUnit")[0].value

            fromDate != "" ?
              oneDayPlus == "true" ?
                dispatch(change(form, "recallDueDate", moment(fromDate).add(val, intervalValue).add(1,"days").format('DD MMM, YYYY'))) :
                dispatch(change(form, "recallDueDate", moment(fromDate).add(val, intervalValue).format('DD MMM, YYYY'))) :
                dispatch(change(form, "recallDueDate", ""))

        } else if(name == 'intervalUnit') {

          let fromDate = document.getElementById("recallFromDate").value
          let oneDayPlus = document.getElementsByName("recallOneDayPlus")[0].value
          let dayCount = document.getElementsByName("intervalNumber")[0].value

            fromDate != "" ?
              oneDayPlus == "true" ?
                dispatch(change(form, "recallDueDate", moment(fromDate).add(dayCount, val).add(1,"days").format('DD MMM, YYYY'))) :
                dispatch(change(form, "recallDueDate", moment(fromDate).add(dayCount, val).format('DD MMM, YYYY'))) :
                dispatch(change(form, "recallDueDate", ""))

        } else if(name == 'recallOneDayPlus') {

          let fromDate = document.getElementById("recallFromDate").value
          let dayCount = document.getElementsByName("intervalNumber")[0].value
          let intervalValue = document.getElementsByName("intervalUnit")[0].value

            fromDate != "" ?
              val == "true" ?
                dispatch(change(form, "recallDueDate", moment(fromDate).add(dayCount, intervalValue).format('DD MMM, YYYY'))) :
                dispatch(change(form, "recallDueDate", moment(fromDate).add(dayCount, intervalValue).add(1,"days").format('DD MMM, YYYY'))) :
                dispatch(change(form, "recallDueDate", ""))

        } else if(name == 'recallFromDate') {

          let oneDayPlus = document.getElementsByName("recallOneDayPlus")[0].value
          let dayCount = document.getElementsByName("intervalNumber")[0].value
          let intervalValue = document.getElementsByName("intervalUnit")[0].value

            oneDayPlus == "true" ?
              dispatch(change(form, "recallDueDate", moment(val).add(dayCount, intervalValue).add(1,"days").format('DD MMM, YYYY'))) :
              dispatch(change(form, "recallDueDate", moment(val).add(dayCount, intervalValue).format('DD MMM, YYYY')))
        }
      }
    }

    return (
      <Col style={style} className={title == 'Extend Validity' ? null : `${className ? className : ''} ${styles.ModalRecordForm}`}>
        <Children children={topView} />
        <ButtonItem config={config} open={this.open.bind(this)} />
        <Children children={bottomView} />
        <Tooltip text={title} show={tooltip} />
        <Modal show={this.state.showModal} bsSize={bsSize} onHide={this.close.bind(this)} backdrop="static" className={modalClass ? modalClass : ''} >
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
            <span className={infoColumns ? styles.infoIconCls : styles.header} style={titleStyle ? {color:"#ea6225"} : null}>{title}{infoColumns ? <span className={styles.sharedInfoIcon}><Info data={infoColumns}/></span> : null}</span>
            </Modal.Title>
          </Modal.Header>
          <form onSubmit={handleSubmit}>	
		        <Modal.Body id="modalrecordform">
              {confirmDialogBox ?
                <p>{`Are you sure you want to ${title}? The change may be irreversible.`}</p> : 
                <RowComponent className={styles.rowStandard} id="RowModalForm">
                  {fields.map((field, i) => {
                    const Component = ImplementationFor[field.type];
                    const contentType = ContentTypes[field.type];
                    return (
                    (field.editRecord && !field.createRecord ?
                      <ColComponent key={i} eventKey={i} title={viewType == 'tab' ? field.label : ''} >
                        {centerAlign ? <Col xs={3} /> : null }
                        <Component
                          data={field}
                          contentType={contentType}
                          options={field.formFieldDecorationOptions}
                          optionsMode="edit"
                          metaData={metaData}
                          initialValues={initialValues}
                          updateCtScan={updateCtScan}
                          ctScanUpdate={updateCtScan ? true : false}
                          inline={field.inline}
                          fullWidth={field.fullWidth}
                          halfWidth={field.halfWidth}
                          inputchange={(val, name) => handleChange(val, name)}
                          shareDet={field.share && shareDet ? shareDet : null}
                        />
                        {centerAlign ? <Col xs={3} /> : null }
                      </ColComponent>
                    : null)
                    );
                  })}
                </RowComponent>}
                  { noteText ?
                      <span style= {{fontWeight: '500'}}> {notes} </span> : null }
            </Modal.Body>
            {hideCancelBtn ?
              <Modal.Footer style={{ textAlign: 'center' }}>
                <Button bsStyle={confirmDialogBox ? 'danger' : 'standard'} type="submit" onClick={closeModal ? this.close.bind(this) : null} disabled={disableShare ? true : !disableShare && confirmDialogBox ?  false : !disableShare && !disable && (pristine || submitting) || disable} className={submitting ? `buttonLoading` : ''}>{confirmDialogBox ? 'confirm' : `${btnName ? btnName : 'submit'}`}</Button>
               </Modal.Footer> :
             hideSaveButton ?
              <Modal.Footer>
                <Button className='btn btn-standard btn-default' onClick={this.close.bind(this)}>CLOSE</Button>
              </Modal.Footer> :
              <Modal.Footer>
                <Button onClick={this.close.bind(this)}>CLOSE</Button>
                {confirmDialog && !confirmDialogBox ? <button className='btn btn-standard btn-default' type="button" onClick={this.showconfirmDialog.bind(this)}>{btnName ? btnName : 'submit'}</button> :
                <Button bsStyle={confirmDialogBox ? 'danger' : 'standard'} type="submit" onClick={closeModal ? this.close.bind(this) : null} disabled={disableShare ? true : !disableShare && confirmDialogBox ?  false : !disableShare && !disable && (pristine || submitting) || disable} className={submitting ? `buttonLoading` : ''}>{confirmDialogBox ? 'confirm' : `${btnName ? btnName : 'submit'}`}</Button>}
              </Modal.Footer>}
          </form>	
          {error || updateError ?
            <Alert danger>{error || updateError}</Alert> : null}
        </Modal>
      </Col>
    );
  }

  showconfirmDialog() {
    this.setState({ confirmDialogBox: true });
  }

  close() {
    this.setState({ showModal: false });
  }

  open(e) {
    e.stopPropagation();
    const { config = {}, fields } = this.props;
    const { record = {}, mode } = config;
  	const { start } = this.state;
  	if(!start && (mode == 'vendor' || mode == 'customer')) {
  		fields.filter(function(el) { return (el.value == "checkNumber") ? el.editRecord = ((record.paymentType == 'Check') ? true : false) : el })
    	this.setState({ showModal: true, start: true, confirmDialogBox: false })
    } else {
    	this.setState({ showModal: true, confirmDialogBox: false })
    }	
  }

}

ModalRecordForm.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  updateError: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  record: PropTypes.object,
  fields: PropTypes.array,
  config: PropTypes.object
};

export default reduxForm({
  form: 'modalRecord',
  enableReinitialize: true,
})(ModalRecordForm);
