/**
*
* GroupField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import ReactTable from "react-table";
import { Col, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import { Map as iMap } from "immutable";
import ConfirmButton from 'components/ConfirmButton';
import Info from 'containers/PatientsPage/components/Info';
import SearchField from 'containers/PatientsPage/components/SearchField';
import { ContentTypes } from '../CreateRecordForm/utils';
import ImageUpload from 'containers/PatientsPage/components/ImageUpload';
import DatePicker from 'containers/PatientsPage/components/DatePicker';
import ShowText from 'components/ShowText';

const renderTableField = (props) => {
  const { input, required, data, metaData, renderMode } = props;
  const Input = iMap(input).toJS();
  const InputValue = Input.value && Input.value[input.name] || [];

  const handleData = () => {
    const fieldIndex = document.getElementsByName(`${input.name}_index`)[0];
    const btnElem = document.getElementById(`${input.name}_btn`);
    var submitData = data.fields && data.fields.filter(_ => _.editRecord).reduce((a, el) => Object.assign({}, a, { [el.name]: el.type === "Date" ? document.getElementById(el.name).value : el.type === "Select" ? parseInt(document.getElementsByName(el.name)[0].value) : document.getElementsByName(el.name)[0].value }), {});

    if (fieldIndex.value != '' && InputValue && InputValue.length > 0) {
      const changedData = InputValue.map((a, index) => {
        if (index === parseInt(fieldIndex.value)) {
          return submitData;
        } else {
          return a;
        }
      });
      input.onChange({ [input.name]: changedData });
      fieldIndex.value = '';
      if (btnElem)
        btnElem.innerHTML = 'Add'
    } else {
      input.onChange({ [input.name]: InputValue && InputValue.length > 0 ? InputValue.concat([submitData]) : [submitData] });
    }


    data.fields && data.fields.map(field => {
      const elem = document.getElementsByName(field.name)[0];
      const element = document.getElementById(field.name);


      if (elem) {
        elem.value = '';
      }
      if (element) {
        element.value = '';
      }

      if (props.localState && props.localState[field.name]) {
        props.setValue(field.name, '');
      }
    });
  }

  const InitialData = (index, edit) => {
    const fieldElem = document.getElementsByName(`${input.name}_index`)[0];
    const btnElem = document.getElementById(`${input.name}_btn`);
    if (fieldElem) {
      fieldElem.value = index;
      if (btnElem)
        btnElem.innerHTML = 'Save';
    }

    edit && Object.keys(edit).map(field => {
      const elem = document.getElementsByName(field)[0];
      const element = document.getElementById(field);

      if (elem) {
        elem.value = edit[field];
      }
      if (element) {
        element.value = edit[field];
      }
      if (field.type === 'Images') {
        props.setValue(field.name, edit[field]);
      }

    });

    setTimeout(() => input.onChange({ [input.name]: InputValue }), 1000);
  }

  const editableColumnProps = {
    Cell: props => {
      const { column, tdProps } = props;
      return (column.action ?
        <Col className={styles.actions}>
          <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => InitialData(props.index, props.original)}>
            <Icon glyph="icon-fontello-edit" className={styles.icon} />Edit
          </Button>
          <ConfirmButton onConfirm={() => input.onChange({ [input.name]: InputValue && InputValue.length > 0 ? InputValue.filter((a, index) => index !== props.index) : [] })}>
            {(click) => <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={() => click()}>
              <Icon glyph="icon-fontello-trash" className={styles.icon} />Delete</Button>}
          </ConfirmButton>
        </Col> : <span style={{ display: 'flex' }}>
          {column.type === "Images" && props.value && props.value.length > 0 ?
            <Info data={{ attributes: { info: { image: props.value.split(','), openModal: true } } }} renderMode={renderMode} />
            : column.limit && props.value && props.value.length > column.limit ?
              props.value.substring(0, column.limit) + '...' :
              column.groupColumn && props.value ?
                metaData && metaData['vendorGroupsList'] && metaData['vendorGroupsList'].filter(_ => _.value === parseInt(props.value))[0]['label'] || '' : props.value}
          {column.limit && props.value && props.value.length > column.limit ?
            <ShowText title={column.title} text={props.value}>{(open) => <div className={styles.view} onClick={open.bind(this)} />}</ShowText> : null}
        </span>);
    }
  };
  return <Col>
    <Col className={styles.fields}>
      <input name={`${input.name}_index`} type="hidden" />
      {data.fields && data.fields.filter(_ => _.editRecord).map((field, index) => {
        if (field.type === 'Input') {
          return <Col key={index} style={{ marginRight: '20px' }}>
            <label htmlFor={field.title}>
              {field.title}
              {required ? <span>*</span> : null}
            </label>
            <input name={field.name} className={styles.InputField} type="text" />
          </Col>
        }
        else if (field.type === 'Select') {
          {
            return <Col key={index} style={{ marginRight: '20px' }}>
              <label htmlFor={field.title} style={{ display: 'flex' }}>
                {field.title}
                {field.required ? <span>*</span> : null}
                {field.attributes && field.attributes.info ? <Info data={field} noteStyle={true} renderMode={renderMode} /> : null}
              </label>
              <select name={field.name} className={styles.selectField} type="text">
                <option value="" disabled={false}>Choose Option</option>
                {field.options && metaData[field.options] ?
                  (field.options && metaData[field.options] || []).map((option, index) =>
                    <option key={`select-${option.name}-${index}`} value={field.number ? parseInt(option.value) : option.value}>{option.label}</option>) : null}
              </select>
            </Col>
          }
        }
        else if (field.type === 'Date') {
          return <Col key={index} style={{ marginRight: '20px' }}>
            <DatePicker data={field} flexStyle={true} />
          </Col>
        }
        else if (field.type === 'Images') {
          return <Col id='Finalpatest' key={index} style={{ marginRight: '20px', marginLeft: '20px' }}>
            <input name={field.name} id={field.name} type="hidden" />
            <ImageUpload
              input={{
                name: field.name,
                value: (props.localState && props.localState[field.name]) || (document.getElementById(field.name) && document.getElementById(field.name).value) || '',
                onChange: (val) => {
                  const currField = document.getElementById(field.name);
                  props.setValue(field.name, val.join(','));
                  if (currField)
                    currField.value = val.join(',');
                }
              }}
              type={'images'}
              contentType="images"
              showtype={'button'}
              label={field.title}
              data={field}
            />
          </Col>
        }


        return '';
      })}
      <Button id={`${input.name}_btn`} bsStyle="standard" type="button" className={data.providerBtn ? styles.providerSaveBtn : styles.saveButton} onClick={handleData} style={{ marginLeft: '20px' }}>Add</Button>
    </Col>
    <Col className="groupModule" style={{ marginTop: "20px" }}>

      <ReactTable
        columns={
          data.fields && data.fields.map((field) => Object.assign(
            {},
            { Header: field.title, accessor: field.name, width: field.width, ...field, ...editableColumnProps }
          ))}
        data={InputValue}
        pageSize={InputValue && InputValue.length > 0 ? data.pageSize ? data.pageSize : 4 : 0}
        showPageJump={false}
        resizable={false}
        showPageSizeOptions={false}
        previousText={"Back"}
        pageText={""}
        noDataText={"No entries to show."}
      />
    </Col>

  </Col>
}

class GroupField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
    renderMode: PropTypes.oneOf(['field', 'group']),
  };

  render() {
    const { data = {}, metaData = {}, options = {}, optionsMode, renderMode } = this.props;
    const { required } = options.general || {};

    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={12}>
        <label htmlFor={data.label} style={data.labelStyle || {}}>
          {data.label}
          {required ? <span>*</span> : null}
        </label>
        <Field
          name={data.value}
          id={data.value}
          component={renderTableField}
          data={data}
          metaData={metaData}
          renderMode={renderMode}
          {...options.general}
          {...extraFieldOptions}
          setValue={(name, val) => this.setState({ [name]: val })}
          localState={this.state}

        />
      </Col>
    );
  }
}

export default GroupField;
