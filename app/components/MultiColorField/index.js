/**
 *
 * MultiColorField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { Map as iMap } from "immutable";
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';


const renderMultiColorField = ({ input, meta: { touched, error }, data, required }) => {
    const { colors = []  } = data;
    const isPreDefinedSet = Array.isArray(colors);
    const Input = iMap(input).toJS();
    const inputVal = Input.value ? Input.value : false;
    const handleChange = (val) => {
        input.onChange(val);
    }

    return (<Col>
        <input type="hidden" name={input.name} defaultValue={inputVal} required={required}/>
        {isPreDefinedSet && colors.length > 0 ?
        colors.map((color, index) => (<Col 
            key={index}
            className={`${styles.color} ${(inputVal === color) || (!inputVal && index === 0) ? styles.selected : ''}`} 
            style={{backgroundColor: color}}
            onClick={() => handleChange(color)}></Col>))
        : null}
    </Col>)
}

class MultiColorField extends React.Component { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.state = { showAttributes: null, selectedOptions: false };
    }

    static propTypes = {
        options: PropTypes.object,
        optionsMode: PropTypes.oneOf(['edit', 'create']),
        initialValue: PropTypes.object,
        path: PropTypes.string,
    };

    render() {
        const { data, options = {}, optionsMode, inline } = this.props;
        const { label, value, fullWidth } = data;
        const { required,practiceStyle} = options.general || {};
        const columnSize = inline ? 12 : 6;
        let extraFieldOptions = {};
        if (optionsMode === 'edit') {
            extraFieldOptions = options.edit || {};
        } else if (optionsMode === 'create') {
            extraFieldOptions = options.create || {};
        }

        return (
            <Col xs={fullWidth ? 12 : columnSize}>
                {!inline ? <label htmlFor={label}>
                    {label}
                    { practiceStyle ? <span style={{color:"#ea6225",fontSize:"14px"}}>*</span> : null}
                    {required || extraFieldOptions.required ? <span>*</span> : null}
                </label> : null}
                <Field
                    name={value}
                    component={renderMultiColorField}
                    type={'text'}
                    {...options.general}
                    {...extraFieldOptions}
                    {...this.props}
                    onFocus={e => e.preventDefault()}
                    onBlur={e => e.preventDefault()}
                />
            </Col>
        );
    }


}

export default MultiColorField;
