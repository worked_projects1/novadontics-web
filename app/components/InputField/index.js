/**
*
* InputField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';


const renderExpireField = (props) => {
  const { input, required, data = {} } = props;
            const handleChange = (e) => {
                const monthField = document.getElementsByName("month")[0].value;
                const yearField = document.getElementsByName("year")[0].value;
                input.onChange(`${yearField}-${monthField}`);
            }
  return <div className={styles.Expire}>
             <input type="text" onChange={handleChange} name="month" placeholder="MM" maxLength="2" size="2" className={styles.ExpireField} required />
             <input type="text" onChange={handleChange} name="year" placeholder="YYYY" maxLength="4" size="2" className={styles.ExpireField} required />
         </div>
}

class InputField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };
  render() {
    const { data, inline, options = {}, optionsMode, record, date, fullWidth, halfWidth, perioStyle } = this.props;
    const { required ,practiceStyle} = options.general || {};
    const { style, noPad, labelStyle } = data || {}; 
    const columnSize = inline ? 12 : data.fullWidth ? 12 : fullWidth ? 12 : halfWidth ? 3 : 6;
    const type = data.number && !data.text ? 'number' : 'text';
    const lessThan = otherField => (value, previousValue, allValues) => value < allValues[otherField] ? value : previousValue;
    const normalize = data.negative ? lessThan('max') : data.max ? ((value) => value.length > data.max ? value.slice(0,parseInt(data.max)) : value) : data.tax ? ((value) => parseFloat(value) > 100 ? 100 : value) : data.percentage ? ((value) => parseFloat(value) > 100 ? 100 : parseFloat(value)) : data.number && data.text ? ((value) => /^[0-9.]+$/.test(value) ? parseFloat(value) : '') : data.number ? ((value) => parseFloat(value)) : data.dollar ? ((value) => {  return isNaN(parseFloat(value.replace(/\$/g,''))) ? '$' : '$' + parseFloat(value.replace(/\$/g,''))})  : null;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={columnSize} style={style ? style : noPad ? {padding:'0px'}: null}>
        {!inline ?
          <label htmlFor={data.label} style={labelStyle || {}}>
            {data.label}
            { practiceStyle ? <span style={{color:"#ea6225",fontSize:"font-size: 14px"}}>*</span> : null}
            {required || data.showAsterisk || extraFieldOptions.required ? <span style={data.asteriskColor ? {color: "#ea6225", fontSize: "18px"} : null}>*</span> : null}
            {data.required ? <span>*</span> : null}
          </label> : null}
        <Field
          name={data.value}
          id={data.label}
          className={data.disabled ? styles.disabled : perioStyle ? styles.bgColor : null}
          component={data.dateFormat ? renderExpireField : "input"}
          maxLength={data.maxLength}
          type={data.hideText ? 'password' : type}
          disabled={data.disabled ? true : false}
          normalize={normalize}
          {...options.general}
          {...options.create}
          {...extraFieldOptions}
          required={data.required || required || extraFieldOptions.required ? true : false}
	        onChange={event => {
            if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
              this.props.inputchange(event.target.value, data.value);
            }
          }}
        />
      </Col>
    );
  }
}

export default InputField;
