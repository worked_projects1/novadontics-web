/**
* VariableCharts
*/

import React from 'react';
import PropTypes from 'prop-types';


class VariableCharts extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        data: PropTypes.array,
        title: PropTypes.string,
        labelCurrency: PropTypes.string,
        glyph: PropTypes.string
    };
    componentDidMount() {
        const { data = [], title, glyph, labelCurrency, id } = this.props;
        Highcharts.setOptions({
            colors: ['#7bc8b3', '#419cd6', '#fac952', '#595959', '#7e1f43', ' #bf4746','#49e274','#a64ee4','#f571a3','#bcaa7a','#86939c',' #f1a341','#f7211e'],
              lang: {noData: "No data Available"},
              noData: {
                style: {
                    fontWeight: 'bold',
                    fontSize: '20px'
                }
            },
        });
        Highcharts.chart(id, {
            chart: {
                type: 'variablepie',
                marginTop: 20,
                marginRight: 10,
                marginLeft: 10,
                marginBottom: 20,
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            tooltip:
            {
                enabled: false,
                pointFormat: '',
                pointFormatter: '',
                headerFormat: ''
            },
            plotOptions: {
                variablepie: {
                    Size: '80%',
                    borderWidth: 0,
                    minPointSize: 50,
                    innerSize: '20%',
                    states: {
                        inactive: {
                            opacity: 1,
                        },
                    },
                    data: data
                }
            },
            series: [
                {
                    type: "variablepie",
                    dataLabels: {
                        enabled: true,
                        color: 'black',
                        connectorWidth: 1,
                        connectorShape: 'fixedOffset',
                        crookDistance: '100%',
                        alignTo: 'center',
                        distance:50,
                        fontSize: '18px',
                        connectorColor: 'black',
                        formatter: function () {
                            return '<b>' + this.point.name + '</b><br/>' +
                                Math.round(this.percentage) + '%';
                        }
                    }
                }
            ],
        });
    }


    render() {
        const { data = [], title, glyph, labelCurrency, id } = this.props;
        return (
            <figure class="highcharts-figure">
                <div id={id} style={{ height: "450px" }}></div>
            </figure>
        )
    }
}

export default VariableCharts;
