import DataDisplay from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const data = [
  {
    id: 1,
    value: 'Test',
  },
  {
    id: 1,
    value: 'Test',
  },
];

describe('<DataDisplay />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <DataDisplay
        data={data}
      />
    );
    expect(wrapper.props().data).toEqual(data);
  });
});
