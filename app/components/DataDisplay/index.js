/**
*
* DataDisplay
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col } from '@sketchpixy/rubix';

import styles from './styles.css';


class DataDisplay extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.any,
    }),
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { data } = this.props;
    const { value, label } = data;
    const colSize = label ? 4 : 12;

    return (
      <Col xs={colSize} className={styles.space}>
        <b>{value}</b>
      </Col>
    );
  }
}

export default DataDisplay;
