/**
*
* MultiCheckboxField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field} from 'redux-form/immutable';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class MultiCheckboxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
    metaData: PropTypes.object,
  };

  render() {
    const { data, inline, options = {}, optionsMode, metaData } = this.props;
    const { required } = options.general || {};
    const columnSize = inline || data.fullWidth ? 12 : 6;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }
    const isPreDefinedSet = Array.isArray(data.oneOf);
    const normalize = data.number ? (value) => Number(value) : null;

    const renderOptions = data.reportsGroup ? [{label: data.label, options: data.oneOf}] : isPreDefinedSet ? data.oneOf.map((c) => { return { 'label' : c, 'value' : c} }) : data.group ? metaData[data.oneOf] : [{label: data.label, options: metaData[data.oneOf]}];
    let checkboxOptions = data.reportsGroup ? data.oneOf.map(item => item.value) : [];
    const renderMultiCheckboxField = ({ input, label, meta: { touched, error }, children }) => {
      const handleChange = (val) => {
        let selectedValue = val;
        let previousValue = input.value.split(',');
        let notPresentInData = selectedValue.filter(val => !previousValue.includes(val));
        if(val.indexOf('All') > -1 && notPresentInData.indexOf('All') > -1) {
          checkboxOptions.push("All")
          input.onChange(checkboxOptions.join());
        } else {
          val = val.filter(e => e !== 'All')
          input.onChange(val.join());
        }
      }
      return (
        <Col className={renderOptions.length > 1 ? styles.renderField : null}>
          {data.group ? <label htmlFor={label}>
                  {label}
          </label> : null}
          {renderOptions.map((r,index)=>
          <Col key={index} xs={columnSize}>
              {!inline ?
              <label htmlFor={r.label}>
                  {r.label}
                  {required || extraFieldOptions.required ? <span>*</span> : null}
              </label> : null}
              <CheckboxGroup
                  className={data.singleColumn ? styles.MultiCheckboxFieldSingle : styles.MultiCheckboxField}
                  checkboxDepth={2} // This is needed to optimize the checkbox group
                  name={input.name}
                  value={input.value ? input.value.split(',') : []}
                  onChange={(val)=> {data.allOption ? handleChange(val) : input.onChange(val.join())}}>
                  {data.allOption ? <label key={"All"}><span></span><Checkbox value={"All"} />All</label> : null}
                  { r.options.map((option) =>
                      <label key={option.value}><span></span><Checkbox value={option.value} />{option.label}</label>)}
              </CheckboxGroup>
          </Col>)}
        </Col>
    )}

		
    return ( 
        <Field
          name={data.value}
          id={data.label}
          label={data.label}
          component={renderMultiCheckboxField}
          normalize={normalize}
          onFocus={e => e.preventDefault()}
          onBlur={e => e.preventDefault()}
        />
    );
  }

}

export default MultiCheckboxField;
