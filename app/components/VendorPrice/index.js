import React from 'react';
import {connect} from 'react-redux';
import styles from './styles.css';
import { Link } from 'react-router';
import {Col, Row} from "@sketchpixy/rubix";
import {compose, withProps} from 'recompose';
//import { increaseVendorPrice, priceChangeHistory, deleteAllProducts } from '../../blocks/vendorPrice';
import { browserHistory } from 'react-router';
import ModalForm from 'components/ModalRecordForm';
import UpdateForm from './UpdatePriceForm';
import ConfirmButton from 'components/ConfirmButton';
import schema from '../../routes/schema';
import Alert from 'components/Alert';
import ReactTable from 'components/Table';
import { increaseVendorPrice, priceChangeHistory, deleteAllProducts } from 'blocks/vendors/actions';
import VendorSaleTax from 'components/VendorSaleTax';

const VendorPrice = ({record: { name, metaData, saleTax }, extendAction, loadAction, deleteAction, record, error, success}) => {
  const handleSubmit = (data, dispatch, { form }) => {
    const record = data.toJS();
    extendAction(record);
  };
  
	return (
	  <Row className={styles.container}>
			<Col xs={6}>
				<div className={styles.label}>Vendor:</div>
				<div>{name}</div>
        <ConfirmButton onConfirm = {()=> deleteAction(record.id)}>{(click)=><Col onClick={() => click()} className={styles.deleteProd} >Delete All Products</Col>}</ConfirmButton>
			</Col>
			<Col xs={6} className={styles.center}>
        <UpdateForm
          initialValues = {{id: record.id}}
          fields={schema().vendors().vendorPrice}
          form={`modalRecord.${record.id}`}
          updateError={error}
          metaData={metaData}
          onSubmit={handleSubmit.bind(this)}
          config={{record:record, title:'Update Price', confirmDialog: true, style: {marginRight:'30px'}}}
        />
        <ReactTable 
          title="Price Change History"
          records={record.priceChangeHistory ? record.priceChangeHistory : []}
          loadRecord={() => loadAction(record)}
          columns={schema().vendors().priceChangeHistory}>
          {(click)=><Link onClick={()=> click()} className={styles.link} style={{marginRight:'17px'}}>Price Change History </Link>}
        </ReactTable>
        <VendorSaleTax 
          title="Manage Sale Tax"
          columns={schema().vendors().saleTax}
          records={saleTax || []}
          vendorId={record && record.id}
          metaData={metaData}>
          {(click)=><Link onClick={()=> click()} className={styles.link} style={{marginRight:'32px'}}>Manage Sale Tax </Link>}
        </VendorSaleTax>
			</Col>
      {error ?
        <Alert warning dismiss={()=>browserHistory.push(`/vendors/${record.id}`)}>{error}</Alert> : null}
      {success && !saleTax && !record.priceChangeHistory?
        <Alert success dismiss={()=>browserHistory.push(`/vendors/${record.id}`)}>All products deleted successfully!</Alert> : null}  
	  </Row>
)};

function mapStatetoProps(state, props) {
  const { vendors = {} } = state.toJS();
  const { updateError, success } = vendors;
  return {
    error: updateError,
    success: success
  }
}
export default compose(
  connect(mapStatetoProps),
  withProps(({record, dispatch, error, success}) => ({
    deleteAction: (id) => dispatch(deleteAllProducts(id)),
    extendAction: (record) => dispatch(increaseVendorPrice(record)),
    loadAction: (record) => dispatch(priceChangeHistory(record)),
    record: record,
    error : error,
    success: success
  }))
)(VendorPrice);
