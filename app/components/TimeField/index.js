/**
*
* SelectField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class SelectField extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        data: PropTypes.object,
        inline: PropTypes.bool,
        options: PropTypes.object,
        optionsMode: PropTypes.oneOf(['edit', 'create']),
        metaData: PropTypes.object,
        record: PropTypes.object,
    };

    render() {
        const { data, inline, options = {}, optionsMode, metaData, record } = this.props;
        const { required } = options.general || {};
        const columnSize = inline ? 12 : 6;
        let extraFieldOptions = {};
        if (optionsMode === 'edit') {
            extraFieldOptions = options.edit || {};
        } else if (optionsMode === 'create') {
            extraFieldOptions = options.create || {};
        }

        const renderTimeField = ({ input, meta: { touched, error }, children }) => {
            const handleChange = (e) => {
                const timeField = document.getElementsByName(`${input.name}.time`)[0].value;
                const clockField = document.getElementsByName(`${input.name}.clock`)[0].value;
                input.onChange(`${timeField} ${clockField}`);
                if(typeof this.props.inputchange !== "undefined" && this.props.data.onChange){
                  this.props.inputchange(timeField+" "+clockField, data.value);
                }
            }

            return (<Col xs={columnSize}>
                {!inline ?
                    <label htmlFor={data.label}>
                        {data.label}
                        {required || extraFieldOptions.required ? <span>*</span> : null}
                    </label> : null}
                <Col className={styles.timeField}>
                    <select
                        name={`${input.name}.time`}
                        defaultValue={input.value && input.value.split(" ")[0]}
                        onChange={handleChange}
                        required={required}>
                        <option value="" disabled={true}></option>
                        {data.oneOf && data.oneOf['time'].map((t, i) => <option key={i} value={t}>{t}</option>)}
                    </select>
                    <select
                        name={`${input.name}.clock`}
                        defaultValue={input.value && input.value.split(" ")[1]}
                        onChange={handleChange}
                        required={required}>
                        <option value="" disabled={true}></option>
                        {data.oneOf && data.oneOf['clock'].map((c,y)  => <option key={y} value={c}>{c}</option>)}
                    </select>
                </Col>

            </Col>)
        }

        return (
            <Field
                name={data.value}
                id={data.label}
                component={renderTimeField}
                {...options.general}
                {...extraFieldOptions}
                onFocus={e => e.preventDefault()}
                onBlur={e => e.preventDefault()}
            />
        );
    }
}

export default SelectField;
