import React from 'react';
import { Col, Modal, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import Spinner from 'components/Spinner';
import { createSaleTax, loadSaleTax, updateSaleTax } from 'blocks/vendors/saleTax';
import { compose, withProps, withHandlers, withState } from 'recompose';
import { connect } from 'react-redux';
import ModalForm from 'components/ModalRecordForm';
import ReactTable from "react-table";
import { reduxForm, Field } from 'redux-form/immutable';
import {reset} from 'redux-form';

const fieldComponent = ({ input, type, data, metaData }) => {
    const isPreDefinedSet = Array.isArray(data.oneOf);
    return data.search && type === 'select' ? <AutocompleteField data={data} inline={true} input={input} metaData={metaData} /> :
        type === 'select' ?
            <select name={input.name} className={styles.InputField} defaultValue={input.value} onBlur={e => input.onChange(e.target.value)} required={data.required}>
                <option value="" disabled={true}>Please select</option>
                {isPreDefinedSet ?
                    (data.oneOf || []).map((option) =>
                        <option key={option} value={option}>{option}</option>) :
                    (metaData[data.oneOf] || []).map((option) =>
                        <option key={option.value} value={option.value}>{option.label}</option>)}
            </select> :
                <input name={input.name} className={styles.InputField} type="text" defaultValue={input.value} onBlur={e => input.onChange(e.target.value)} required={data.required} />;
};

class VendorSaleTax extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }


    handleAddSaleTax(data, dispatch, { form }){
        this.props.createRecord(data.toJS());
        this.addForm.close();
        this.addForm.props.reset();
    }

    editableColumnProps = {
        Cell: props => {
            const { column, tdProps } = props;
            const { onEdit, onCancel, edit, metaData } = tdProps.rest;
            const normalize = (val) => column.number ? (/^[0-9.]+$/.test(val) ? parseFloat(val) : '') : val;
            
            return (column.action && !edit ?
                <Col className={styles.actions}>
                    <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onEdit}><Icon glyph="icon-fontello-edit" className={styles.icon} />Edit</Button>
                </Col> : column.action && edit ?
                    <Col className={styles.actions}>
                        <Button bsStyle="link" bsSize="xs" type="submit" className={styles.btn}><Icon glyph="icon-fontello-floppy" className={styles.icon} />Save</Button>
                        <Button bsStyle="link" bsSize="xs" className={styles.btn} onClick={onCancel}><Icon glyph="icon-fontello-cancel" className={styles.icon} />Cancel</Button>
            </Col> : edit && column.fieldRecord ? 
            <Field name={column.value} id={column.label} normalize={normalize} metaData={metaData} component={fieldComponent} type={column.type} data={column} />
            : <span>{column.number && props.value ? parseFloat(props.value).toFixed(2) : props.value}</span>);
        }
    };


    render() {
        const { title, vendorId, records, columns, handleSubmit, error, success, loadRecord, createRecord, updateRecord, metaData, selected, onEditItem, onCancelItem } = this.props;
        const vendorMetaData = Object.assign({}, metaData, { stateOptions: metaData && metaData['stateOptions'] && metaData['stateOptions'].filter(opt => !records.filter(_ => _.state === opt.state)[0]) });
        const { stateOptions = [] } = vendorMetaData;
        return <Col>
            {this.props.children(() => { this.setState({ showModal: !this.state.showModal }) })}
            <Modal show={this.state.showModal} bsSize="large" backdrop="static" onHide={this.close.bind(this)} onEnter={() => loadRecord(vendorId)}>
                <Modal.Header closeButton style={{ paddingBottom: 0 }}>
                    <Modal.Title>
                        <Col className={styles.header}>
                            <h4>{title}</h4>
                            {stateOptions.length > 0 ? <ModalForm
                                initialValues={{ vendorId: vendorId }}
                                fields={columns}
                                form={`modalRecord.${vendorId}`}
                                updateError={error}
                                metaData={vendorMetaData}
                                onSubmit={this.handleAddSaleTax.bind(this)}
                                onRef={addForm => (this.addForm = addForm)}
                                config={{ title: 'Add Sale Tax', resetOnClose: true }}
                            /> : null}
                        </Col>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles.body} style={{ paddingTop: 0 }}>
                    <form id="TreatmentPlanDetails" onSubmit={handleSubmit}>
                        {records ?
                            <ReactTable
                                columns={columns.filter(column => column.visible && column.viewMode).map((column) => Object.assign(
                                    {},
                                    { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps }
                                ))}
                                data={records || []}
                                getTdProps={(state, rowProps) => {
                                    return {
                                        edit: rowProps && selected && selected.id === rowProps.original.id ? true : false,
                                        metaData: metaData,
                                        onEdit: () => onEditItem(rowProps),
                                        onCancel: () => onCancelItem(),
                                    }
                                }}
                                pageSize={records && records.length > 0 ? 10 : 0}
                                showPageJump={false}
                                resizable={false}
                                showPageSizeOptions={false}
                                previousText={"Back"}
                                pageText={""}
                                noDataText={"No entries to show."}
                            /> : <Spinner />}
                    </form>
                </Modal.Body>
            </Modal>
        </Col>
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }
}


export default compose(
    connect(),
    withProps(({ records, dispatch, error, success }) => ({
        createRecord: (record) => dispatch(createSaleTax(record)),
        loadRecord: (id) => dispatch(loadSaleTax(id)),
        records: records,
        error: error,
        success: success
    })),
    withState('selected', 'setData', false),
    withState('initialValues', 'setInitial', {}),
    withHandlers({
        onEditItem: props => (data) => {props.setData(data.original); props.setInitial(data.original)},
        onCancelItem: props => () => props.setData(false),
        onSubmit: props => (data) => {
            const record = data.toJS();
            if(record && record.id && JSON.stringify(props.selected) != JSON.stringify(record)){
                props.dispatch(updateSaleTax(record));
                props.setData(false);
            }    
        }
    }),
    reduxForm({
        form: `saleTax`,
        enableReinitialize: true,
    })
)(VendorSaleTax);