/**
*
* InputField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field, change } from 'redux-form/immutable';
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

const renderInputField = (props) => {
  const { input, required, data = {} } = props;
  return <input name={input.name} id={input.name} defaultValue={input.value} required={required} type={input.type} onBlur={(e) => input.onChange(e.target.value)} />
}

class SimpleField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };


  render() {
    const { data = {}, inline, options = {}, optionsMode } = this.props;
    const type = data.number ? 'number' : 'text';
    const columnSize = inline ? 12 : 6;
    const { required } = options.general || {};
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }


    return (
      <Col xs={columnSize}>
        {!inline ?
          <label htmlFor={data.label}>
            {data.label}
            {required || extraFieldOptions.required ? <span>*</span> : null}
            {data.required ? <span>*</span> : null}
          </label> : null}
        <Field
          name={data.value}
          id={data.value}
          component={renderInputField}
          type={type}
          {...options.general}
          {...extraFieldOptions}
        />
      </Col>
    );
  }
}

export default SimpleField;
