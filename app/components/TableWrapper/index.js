/**
*
* TableWrapper
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, browserHistory } from 'react-router';
import ReactTableWrapper from "components/ReactTableWrapper";
import SpicyDatatable from 'spicy-datatable';
import Gallery from 'components/NovaGallery';
import TableEditor from 'components/TableEditor';

import { PanelContainer, PanelBody, Col } from '@sketchpixy/rubix';
import Marker from "../Marker";
import ReactPagination from "components/ReactTableWrapper/pagination";
import { setApiFilter } from 'utils/api';
import filterRows from 'utils/filter';
import moment from 'moment';
import styles from './styles.css';

class TableWrapper extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    records: PropTypes.array,
    router: PropTypes.object.isRequired,
    columns: PropTypes.array,
    children: PropTypes.object,
    path: PropTypes.string,
    locationState: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  render() {
    const { records, columns, headers = {}, children, path, locationState = {}, reactTable,patientsColStyle, loading, user, columnStyle, name } = this.props;
    const { totalcount, recPerPage, searchText, status } = headers;
    const activePath = location.pathname;
    const customCSVRowsFormatter = () => (['/catalog'].includes(path)) ? records : rows;
    const [statusColumn] = columns.filter((s) => s.value === 'status' || s.value === 'accountType');
    const tableColumns = children ? columns.filter((column) => column.visible && column.viewMode) : columns.filter((column) => column.visible);

    let rows = records != undefined ? filterRows(records.map((record) => Object.assign(
      {},
      record, {
        onClickHandler: this.handleClick.bind(this, record.id, locationState),
        isActive: activePath === `${path}/${record.id}` || activePath === `${path}/${record.id}/edit`,
      }),
    ), tableColumns, searchText, totalcount) : [];

    rows = status != '' ? rows.filter(record => record.status === status || record.accountType === status) : rows;

    const pageSize = rows.length > 0 && recPerPage && rows.length < parseInt(recPerPage) ? rows.length : rows.length > 0 && recPerPage && rows.length >= parseInt(recPerPage) ? parseInt(recPerPage) : rows.length > 0 ? 25 : 0;

    tableColumns.forEach((column) => {
      switch (column.type) {
        case 'date':
          {
            column.implantRedColor ? rows.map((row) => {
              let expDateValue = typeof row.recommendedHT != "object" ? row.recommendedHT : row.recommendedHT.props.children;
              let expDate = expDateValue && expDateValue != "" ?  moment(expDateValue).format('YYYY-MM-DD') : null;
              let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
              let showAlert = row.implantRestored
              let showAlertValue = typeof showAlert != "object" ? showAlert : showAlert.props.children;
              let removalDate = typeof row.dateOfRemoval != "object" ? row.dateOfRemoval : row.dateOfRemoval.props.children;
              if(removalDate != "" && removalDate != undefined) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#91288F' }}>{row[column.value]}</span>
                  },
                )
              } else if(expDate <= newDate && showAlertValue != 'Yes') {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: 'red' }}>{row[column.value]}</span>
                  },
                )
              } else if(row.noteId == 0) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#38a0f1' }}>{row[column.value]}</span>
                  },
                )
              }
            }) : column.boneGraftColor ? rows.map((row) => {
              let expDateValue = typeof row.rrpsd != "object" ? row.rrpsd : row.rrpsd.props.children;
              let expDate = expDateValue && expDateValue != "" ?  moment(expDateValue).format('YYYY-MM-DD') : null;
              let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
              let removalDate = typeof row.failureDate != "object" ? row.failureDate : row.failureDate.props.children;
              if(removalDate != "" && removalDate != undefined) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#91288F' }}>{row[column.value]}</span>
                  },
                )
              } else if(expDate <= newDate) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: 'red' }}>{row[column.value]}</span>
                  },
                )
              } else if(row.noteId == 0) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#38a0f1' }}>{row[column.value]}</span>
                  },
                )
              }
            }) : null }
          break;
        case 'agreement':
          {
            column.label == 'Document' && column.saasIcon == true ? rows.map((row) => {
              if(row[column.value] != null && typeof row[column.value] != "object") {
                if(row[column.value].indexOf('pdf') > -1) {
                  Object.assign(
                    row,
                    {
                      [column.value]:
                        <img
                          src={require(`img/patients/pdf3.svg`)}
                          alt="pdf" style={{ width: '50px', height: '50px' }} />
                    },
                  )
                } else {
                  Object.assign(
                    row,
                    {
                      [column.value]:
                        <img
                          src={`${row[column.value] && row[column.value] || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}`}
                          role="presentation" style={{ height: 75, padding: 4, width: '80%' }}
                        />
                    },
                  )
                }
              }
            }) : null }
          break;
        case 'input':
          {
            column.implantRedColor ? rows.map((row) => {
              let expDateValue = typeof row.recommendedHT != "object" ? row.recommendedHT : row.recommendedHT.props.children;
              let expDate = expDateValue && expDateValue != "" ?  moment(expDateValue).format('YYYY-MM-DD') : null;
              let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
              let showAlert = row.implantRestored
              let showAlertValue = typeof showAlert != "object" ? showAlert : showAlert.props.children;
              let removalDate = typeof row.dateOfRemoval != "object" ? row.dateOfRemoval : row.dateOfRemoval.props.children;
              if(removalDate != "" && removalDate != undefined) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#91288F' }}>{row[column.value]}</span>
                  },
                )
              } else if(expDate <= newDate && showAlertValue != 'Yes') {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: 'red' }}>{row[column.value]}</span>
                  },
                )
              } else if(row.noteId == 0) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#38a0f1' }}>{row[column.value]}</span>
                  },
                )
              }
            }) : column.boneGraftColor ? rows.map((row) => {
              let expDateValue = typeof row.rrpsd != "object" ? row.rrpsd : row.rrpsd.props.children;
              let expDate = expDateValue && expDateValue != "" ?  moment(expDateValue).format('YYYY-MM-DD') : null;
              let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
              let removalDate = typeof row.failureDate != "object" ? row.failureDate : row.failureDate.props.children;
              if(removalDate != "" && removalDate != undefined) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#91288F' }}>{row[column.value]}</span>
                  },
                )
              } else if(expDate <= newDate) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: 'red' }}>{row[column.value]}</span>
                  },
                )
              } else if(row.noteId == 0) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#38a0f1' }}>{row[column.value]}</span>
                  },
                )
              }
            }) : null }
          break;
        case 'select':
          {
            column.implantRedColor ? rows.map((row) => {
              let expDateValue = typeof row.recommendedHT != "object" ? row.recommendedHT : row.recommendedHT.props.children;
              let expDate = expDateValue && expDateValue != "" ?  moment(expDateValue).format('YYYY-MM-DD') : null;
              let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
              let showAlert = row.implantRestored
              let showAlertValue = typeof showAlert != "object" ? showAlert : showAlert.props.children;
              let removalDate = typeof row.dateOfRemoval != "object" ? row.dateOfRemoval : row.dateOfRemoval.props.children;
              if(removalDate != "" && removalDate != undefined) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#91288F' }}>{row[column.value]}</span>
                  },
                )
              } else if(expDate <= newDate && showAlertValue != 'Yes') {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: 'red' }}>{row[column.value]}</span>
                  },
                )
              } else if(row.noteId == 0) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#38a0f1' }}>{row[column.value]}</span>
                  },
                )
              }
            }) : column.boneGraftColor ? rows.map((row) => {
              let expDateValue = typeof row.rrpsd != "object" ? row.rrpsd : row.rrpsd.props.children;
              let expDate = expDateValue && expDateValue != "" ?  moment(expDateValue).format('YYYY-MM-DD') : null;
              let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
              let removalDate = typeof row.failureDate != "object" ? row.failureDate : row.failureDate.props.children;
              if(removalDate != "" && removalDate != undefined) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#91288F' }}>{row[column.value]}</span>
                  },
                )
              } else if(expDate <= newDate) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: 'red' }}>{row[column.value]}</span>
                  },
                )
              } else if(row.noteId == 0) {
                Object.assign(
                  row,
                  {
                    [column.value]:
                      <span style={{ color: '#38a0f1' }}>{row[column.value]}</span>
                  },
                )
              }
            }) : null }
          break;
        case 'image':
          column.label == 'X-Rays' ?
            rows = rows.map((row) => Object.assign(
              row,
              {
                [column.value]:
                  <div style={{ overflowX: 'auto', width: '430px' }}><Gallery data={row[column.value] && typeof row[column.value] === 'string' ? row[column.value].split(',') : []} width="60" viewOnly={true} implantStyle={true} /></div>,
              },
            )) :
          column.label == 'Logo' && column.saasImage == true ?
            rows = rows.map((row) => Object.assign(
              row,
              {
                [column.value]:
                  <img
                    src={`${row[column.value] && row[column.value] || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}`}
                    role="presentation" style={{ height: 70, padding: 4, width: '90%' }}
                  />,
              },
            )) :
            rows = rows.map((row) => Object.assign(
              row,
              {
                [column.value]:
                  <img
                    src={`${row[column.value] && row[column.value].split(',')[0] || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}`}
                    role="presentation" style={{ height: 64, padding: 4 }}
                  />,
              },
            ));
          break;
        case 'action':
          rows = rows.map((row) => Object.assign(
            row,
            {
              [column.value]:
                <TableEditor
                  row={row}
                  name={name}
                  tableColumns={tableColumns}
                  actions={this.props.actions}
                  dispatch={this.props.dispatch}
                />
            },
          ));
          break;
        case 'booleanLabel':
          rows = rows.map((row) => Object.assign(
            row,
            { [column.value]: column.booleanLabelValue(row[column.value]) },
          ));
          break;
        case 'checkbox':
          rows = rows.map((row) => Object.assign(
            row,
            { [column.value]: column.booleanLabelValue(row[column.value]) },
          ));
          break;
        case 'marker':
          rows = rows.map((row) => Object.assign(
            row,
            { [column.value]: <Marker {...column.computed(row)} /> }
          ));
          break;
        default:
          break;
      }
    });

    return reactTable ?
      (<ReactTableWrapper
        columns={
          tableColumns.map((column) => Object.assign(
            {},
            { Header: column.sort ? column.sortLabel : column.label, sortable: column.sortable ? column.sortable : null, accessor: column.value, sortMethod: column.dateSort ? (a, b) => this.handleSort(a, b, column.type) : column.textSort ? (a, b) => this.handleSort(a, b, 'text') : null, width: column.width, ...column }
          ))}
        data={rows}
        showPageJump={false}
        patientsColStyle={patientsColStyle}
        resizable={false}
        showPageSizeOptions={false}
        filter={this.handleFilters.bind(this)}
        user={user}
        pageSize={pageSize}
        pageText={""}
        getTrProps={(state, record) => {
          return {
            onClick: () => this.handleClick(record.original.id, locationState),
            style: {
              background: record && record.original && record.original.id == this.props.params.id ? '#F5F5F5' : 'white'
            }
          }
        }}
        PaginationComponent={ReactPagination}
        noDataText={loading ? "Fetching entries." : "No entries to show."}
        sortable={false}
        name={name}
        headers={headers}
        path={path}
        statusColumn={statusColumn}
        loadingRecords={loading}
        defaultStatusKey={statusColumn && statusColumn.oneOf && status ? statusColumn.oneOf.findIndex((val) => val === status) : 0}
      />) :
      (<PanelContainer>
        <PanelBody>
          <Col sm={12} id={activePath.indexOf('/accounts') > -1 ? "AccountPage" : name == 'saasAdmin' ? "SaasAdminPage" : patientsColStyle ? "patientsColStyle" : null}>
            <SpicyDatatable
              tableKey={path}
              config={{ showDownloadCSVButton: true, customCSVRowsFormatter, itemsPerPageOptions: [25, 50, 100], customCSVKeys: tableColumns.filter(c => !c.computed).map(c => c.value) }}
              columns={
                tableColumns.map((column) => Object.assign(
                  {},
                  { key: column.value, label: column.sort ? column.sortLabel : column.label, sort: column.sort }
                ))}
              rows={rows}
            />
          </Col>
        </PanelBody>
      </PanelContainer>);

  }

  handleClick(id, locationState) {
    const { path } = this.props;
    if(path.indexOf('/implantLog') != 0 && path.indexOf('/boneGraftLog') != 0 && path.indexOf('/friendReferral') != 0 && path.indexOf('/ceCourses') != 0){
      this.props.router.push({
        pathname: path.indexOf('/patients') > -1 ? `${path}/${id}/form` : `${path}/${id}`,
        state: locationState,
      });
    }
  }

  handleSort(startDate, endDate, type) {
    let startDateValue = typeof startDate != "object" ? startDate : startDate.props.children;
    let endDateValue = typeof endDate != "object" ? endDate : endDate.props.children;
    if(startDateValue != null && endDateValue != null) {
      if (startDateValue.length === endDateValue.length && type == 'date') {
        return moment(startDateValue).format("YYYY-MM-DD") > moment(endDateValue).format("YYYY-MM-DD") ? 1 : -1;
      } else if (type == 'text') {
        return startDateValue.toLowerCase() > endDateValue.toLowerCase() ? 1 : -1;
      }
      return startDateValue.length > endDateValue.length ? 1 : -1;
    } else {
      if( startDateValue === null ) return -1;
      if( endDateValue === null ) return 1;
    }
  }

  handleFilters(type, input) {
    const { dispatch, actions, headers = {}, path } = this.props;
    const { totalcount } = headers;
    if (type != 'pageNumber') {
      dispatch(actions.updateHeader({ 'pageNumber': 1 }));//set filter in reducer
    }
    if(type === 'status'){
      dispatch(actions.updateHeader({ 'totalcount': 0 }));//set totalcount in reducer
    }

    dispatch(actions.updateHeader({ [type]: input }));//set filter in reducer
    if (totalcount) {
      setApiFilter({ [type]: input }, true);//set filter in api
      dispatch(actions.loadRecords(true));//loading records
    }
    if(path.indexOf('/orders') > -1) {
      browserHistory.push(path)
    }
  }

}

export default withRouter(TableWrapper);
