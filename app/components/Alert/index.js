/**
*
* Alert
*
*/

import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Modal, Row, Button, Icon } from '@sketchpixy/rubix';
import styles from './styles.css';
import EmailShare from 'react-email-share-link';


class Alert extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    this.setState({ showModal : true})
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { success, danger, warning, dismiss, info ,catalogSuccess,versionAlert, xrayAlert, doseSpot, email, checklist, catalogSubscribe, finance, onlineReg } = this.props;
    return (
      <Col>
        {
        xrayAlert ?
            <Modal show>
                <Modal.Body>
                  <Row className={styles.rowStandards}>
                    <Col>
                    You are currently not subscribed to our cloud-based digital imaging solution. To capture and import your patients' x-rays and CT scans into their files in Novadontics software, please contact us at the link below or by calling 888.838.6682 to setup your x-ray and/or 3D imaging account..
                    </Col>
                  </Row>
                </Modal.Body>
                <Modal.Footer className={styles.footers}>
                  <EmailShare
                    email="team@novadontics.com"
                    subject={''}
                    body={'Type your message here..'}>
                    {link => (
                      <a href={link} data-rel="external"><Button bsStyle="standard" type="button" className={styles.submitButton}>Contact Novadontics</Button></a>
                    )}
                  </EmailShare>
                  <Col className={styles.vr}></Col>
                  <Button bsStyle="standard" type="button" onClick={()=>dismiss()} className={styles.submitButton}>Dismiss</Button>
                </Modal.Footer>
            </Modal> :
        doseSpot ?
            <Modal show className="alertModal">
                <Modal.Body>
                  <Row className={styles.rowStandard}>
                    <Icon className={`${styles.icon} ${catalogSuccess ? styles.successIcon : success ? styles.successIcon : null}`} glyph={'icon-fontello-attention'} />
                    <h3>Alert!</h3>
                    <Col>
                     You are currently not subscribed to our electronic prescription module. To access this feature, please email us at: <a href="mailto:team@novadontics.com">team@novadontics.com</a> or call us at: 888.838.6682.
                    </Col>
                  </Row>
                </Modal.Body>
                <Modal.Footer className={`${styles.footer} ${success ? styles.success : null}`} onClick={()=>dismiss()}>
                    Dismiss
                </Modal.Footer>
            </Modal>:
        onlineReg ?
            <Modal show className="alertModal">
                <Modal.Body>
                  <Row className={styles.rowStandard}>
                    <Col style={{ marginTop: '3em' }}>
                      You are currently not subscribed to our online registration module. To access this feature, please email us at: <a href="mailto:team@novadontics.com">team@novadontics.com</a> or call us at: 888.838.6682.
                    </Col>
                  </Row>
                </Modal.Body>
                <Modal.Footer className={`${styles.footer} ${success ? styles.success : null}`} onClick={()=>dismiss()}>
                    Ok
                </Modal.Footer>
            </Modal>:
        finance ?
            <Modal show className="alertModal">
                <Modal.Body>
                  <Row className={styles.rowStandard}>
                    <Col style={{ marginTop: '30px' }}>
                      To finance this item, place it in the shopping cart and follow the financing instructions
                      provided on the payment method screen upon checking out. Only products eligible for
                      financing can be added to the cart and purchased together. Other products must be
                      purchased in a separate order.
                    </Col>
                  </Row>
                </Modal.Body>
                <Modal.Footer className={`${styles.footer} ${success ? styles.success : null}`} onClick={()=>dismiss()}>
                    Dismiss
                </Modal.Footer>
            </Modal>:
        versionAlert ?
            <Modal show className="alertModal">
                <Modal.Body>
                  <Row className={styles.rowStandard}>
                    <Icon className={`${styles.icon} ${catalogSuccess ? styles.successIcon : success ? styles.successIcon : null}`} glyph={'icon-fontello-attention'} />
                    <h3>Alert!</h3>
                    <Col>
                        You are using older version of Novadontics software. <br />
                        Please do hard refresh to get latest changes. <br /> <br />
                        How to do hard refresh? <br />
                        Click on the following keyboard keys simulataneously <br />
                        <b> For Windows/Linux</b><br />
                        <b style={{color: "#ea6225"}}>Ctrl-F5 </b><br />
                        <b> For Mac (Safari) </b><br />
                        <b style={{color: "#ea6225"}}>Command (⌘)-Option (⌥)-R</b><br />
                        <b> For Mac (Chrome or Firefox) </b><br />
                        <b style={{color: "#ea6225"}}>Command(⌘)-Shift (⇧)-R</b><br />
                    </Col>
                  </Row>
                </Modal.Body>
                <Modal.Footer className={`${styles.footer} ${success ? styles.success : null}`} onClick={()=>dismiss()}>
                    Dismiss
                </Modal.Footer>
            </Modal> :
            <Modal show sm={checklist || catalogSubscribe ? false : true} className="alertModal">
                <Modal.Body>
                  <Row className={styles.rowStandard}>
                    <Icon className={`${styles.icon} ${catalogSuccess ? styles.successIcon : success ? styles.successIcon : null}`} glyph={catalogSuccess ? 'icon-fontello-ok-circled2' : success ? 'icon-fontello-ok-circled2' : danger ? 'icon-fontello-cancel-circled2' : 'icon-fontello-attention'} />
                    <h3>{catalogSuccess ? 'Thank you!' : success ? 'Success' : info ? 'Info': alert ? 'Alert!' : danger ? 'Close' : 'Error!'}</h3>
                    <Col className={catalogSubscribe ? styles.alignText : null}>{this.props.children}</Col>
                  </Row>
                </Modal.Body>
                <Modal.Footer className={`${styles.footer} ${success ? styles.success : null}`} onClick={()=>dismiss()}>
                    {success ? 'Ok' : 'Dismiss'}
                </Modal.Footer>
            </Modal>
        }
      </Col>
    );
  }
}


export default Alert;
