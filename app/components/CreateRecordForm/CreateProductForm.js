/**
*
* CreateProductForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm, change } from 'redux-form/immutable';
import styles from './styles.css';
import { Row, Col, Button, Alert } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from './utils';

class CreateProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { metaData: props.metaData };
  }

  render() {
    const { handleSubmit, pristine, submitting, fields,record,path,updateRecordsMetaData, dispatch, error, locationState, form } = this.props;
    const { metaData } = this.state;

    const handleChange = (val, value) => {
      if (value === "manufacturer") {
        const vendorId = metaData.vendorOptions && metaData.vendorOptions.length > 0 ? metaData.vendorOptions.find(_ => _.name == val).id : null;
        const categoryData = metaData.vendorCategoryOptions && vendorId != null && metaData.vendorCategoryOptions.length > 0 ? metaData.vendorCategoryOptions.find(_ => _.vendorId == vendorId) : null;
        const categoryId = categoryData != undefined ? categoryData.categories : null;
        const categoryIdArray = categoryId != null ? categoryId.split(',').map((x) =>parseInt(x)) : null;
        const filteredCategories = metaData.categoriesList && metaData.categoriesList.length > 0 && categoryIdArray && categoryIdArray.length > 0 ? metaData.categoriesList.filter(val => categoryIdArray.includes(val.id)) : [];
          let parentMetaData = this.props.metaData;
          this.setState({ metaData: parentMetaData && parentMetaData.categoryParentOptions && parentMetaData.categoryParentOptions.length > 0 && Object.assign({}, parentMetaData, {categoryParentOptions: filteredCategories && filteredCategories.length > 0 ? filteredCategories : parentMetaData.categoryParentOptions}) })
          dispatch(change(form, "parentId", ""))
          dispatch(change(form, "childId", ""))
      } else {
         fields.filter(function(el) {
           return (el.value == "collectedDate" || el.value == "amountCollected") ?
                     el.editRecord = (val == 'Paid' ? true : false) : el})
      }
      if (value === "parentId") {
          const vendorName = document.getElementsByName("manufacturer")[0].value;
          const vendorId = vendorName != '' && metaData.vendorOptions && metaData.vendorOptions.length > 0 ? metaData.vendorOptions.find(_ => _.name == vendorName).id : null;
          const categoryData = metaData.vendorCategoryOptions && vendorId != null && metaData.vendorCategoryOptions.length > 0 ? metaData.vendorCategoryOptions.find(_ => _.vendorId == vendorId) : null;
          const categoryId = categoryData != undefined ? categoryData.categories : null;
          const categoryIdArray = categoryId != null ? categoryId.split(',').map((x) =>parseInt(x)) : null;
          const filteredCategories = metaData.categoriesList && metaData.categoriesList.length > 0 && categoryIdArray && categoryIdArray.length > 0 ? metaData.categoriesList.filter(val => categoryIdArray.includes(val.id)) : [];
          let childMetaData = this.props.metaData;
          this.setState({ metaData: childMetaData && childMetaData.categoryChildOptions && Object.assign({}, childMetaData, {categoryParentOptions: filteredCategories && filteredCategories.length > 0 ? filteredCategories : childMetaData.categoryParentOptions}, {categoryChildOptions: childMetaData.categoryChildOptions.filter(_ => _.parent == val)}) })
          dispatch(change(form, "childId", ""))
      }
    }
    return (
      <form onSubmit={handleSubmit}>
        <Row>
          {fields.map((field, i) => {
            const Component = ImplementationFor[field.type];
            const contentType = ContentTypes[field.type];

            return (
              ((field.editRecord || field.createRecord) ?
                <div key={i}>
                  <Component
                    data={field}
                    contentType={contentType}
                    options={field.formFieldDecorationOptions}
                    optionsMode="create"
                    metaData={metaData}
                    record={record}
                    inputchange={(val, value) => handleChange(val, value)}
                  />
                </div>
              : null)
            );
          })}
        </Row>
        <Row className="action-button">
          <Col xs={12} className={styles.actionButtons}>
            <Button bsStyle="standard" type="submit" disabled={pristine || submitting} className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Create</Button>
            <Link to={{pathname: path, state: locationState}}>Cancel</Link>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            {error ?
              <Alert danger>{error}</Alert> : null}
          </Col>
        </Row>
      </form>
    );
  }
};

CreateProductForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  path: PropTypes.string,
  error: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
};

export default reduxForm({
  form: 'createRecord',
})(CreateProductForm);
