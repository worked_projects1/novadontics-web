/**
*
* CreateRecordForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { reduxForm, change } from 'redux-form/immutable';
import styles from './styles.css';
import { Row, Col, Button, Alert } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from './utils';

const CreateRecordForm = (props) => {
  const { handleSubmit, pristine, submitting, fields,record,path,updateRecordsMetaData, dispatch, error, metaData, locationState, form, name} = props;

  const handleClose = () => {
    fields.map((field) => {
      if(field.value == 'name' || field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
        field.editRecord = false
      }
    })
  }

  const handleChange = (val, value) => {
    fields.filter(function(el) {
     return (el.value == "collectedDate" || el.value == "amountCollected") ?
               el.editRecord = (val == 'Paid' ? true : false) : el})

    if(value === 'uploadLink') {
      dispatch(change(form, "link", val));
    }
    if(value == 'sectionType') {
      if (val === 'tutorials') {
        fields.map((field) => {
          if(field.value == 'name' || field.value == 'link' || field.value == 'parent') {
            field.editRecord = true
          }
        })
      }
      if (val === 'manuals') {
        fields.map((field) => {
          if(field.value == 'name' || field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
            field.editRecord = true
          }
        })
      }
      if (val === 'section') {
        dispatch(change(form, "link", ""));
        dispatch(change(form, "parent", ""));
        dispatch(change(form, "uploadLink", ""));
        fields.map((field) => {
          if(field.value == 'link' || field.value == 'parent' || field.value == 'uploadLink') {
            field.editRecord = false
          }
          if(field.value == 'name') {
            field.editRecord = true
          }
        })
      }
    }
  }
  return (
    <form onSubmit={handleSubmit}>
      <Row>
        {fields.map((field, i) => {
          const Component = ImplementationFor[field.type];
          const contentType = ContentTypes[field.type];

          return (
            ((field.editRecord || field.createRecord) ?
              <div key={i}>
                <Component
                  data={field}
                  contentType={contentType}
                  options={field.formFieldDecorationOptions}
                  optionsMode="create"
                  metaData={metaData}
                  record={record}
                  inputchange={(val, value) => handleChange(val, value)}
                />
              </div>
            : null)
          );
        })}
      </Row>
      <Row className="action-button">
        <Col xs={12} className={styles.actionButtons}>
          <Button bsStyle="standard" type="submit" disabled={pristine || submitting} className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Create</Button>
          <Link to={{pathname: path, state: locationState}} onClick={name == "tutorials.create" || name == "manuals.create" ? handleClose : null}>Cancel</Link>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          {error ?
            <Alert danger>{error}</Alert> : null}
        </Col>
      </Row>
    </form>
  );
};

CreateRecordForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  path: PropTypes.string,
  error: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
};

export default reduxForm({
  form: 'createRecord',
})(CreateRecordForm);
