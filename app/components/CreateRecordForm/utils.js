
import Textarea from '../Textarea';
import InputField from '../InputField';
import SelectField from '../SelectField';
import MultiSelectField from '../MultiSelectField';
import Upload from '../Upload';
import DatePicker from '../DatePicker';
import CheckboxField from "../CheckboxField";
import SignatureField from "../SignatureField";
import MultiCheckboxField from "../MultiCheckboxField";
import MultiColorField from "../MultiColorField";
import TimeField from '../TimeField';
import SearchField from '../SearchField';
import SimpleField from '../SimpleField';
import GroupField from '../GroupField';
import RadioBoxField from '../RadioboxField';

export const ImplementationFor = {
  textarea: Textarea,
  input: InputField,
  checkbox: CheckboxField,
  select: SelectField,
  image: Upload,
  thumbnail: Upload,
  document: Upload,
  video: Upload,
  invoice: Upload,
  agreement: Upload,
  manuals: Upload,
  zip: Upload,
  contracts: Upload,
  multiselect: MultiSelectField,
  date: DatePicker,
  signature: SignatureField,
  multicheckbox: MultiCheckboxField,
  multicolor: MultiColorField,
  time: TimeField,
  search: SearchField,
  files: Upload,
  simple: SimpleField,
  group: GroupField,
  Radiobox: RadioBoxField
};
  
export const ContentTypes = {
  image: 'image/*',
  thumbnail: 'image/*',
  video: 'video/*,video/mp4,application/pdf',
  document: 'application/pdf',
  invoice: 'application/pdf',
  zip: 'application/*',
  contracts: 'application/pdf,image/*',
  agreement: 'application/pdf,image/*',
  manuals: 'video/*,video/mp4,application/pdf,image/*',
  files: '*'
};