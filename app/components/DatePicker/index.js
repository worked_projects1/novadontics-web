/**
*
* <DatePicker />
* Wrapper for redux-form’s <Field /> and delegates work to ModernDatePicker component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import ModernDatePicker from 'components/ModernDatePicker';
import { Field } from 'redux-form/immutable';
import { Col } from '@sketchpixy/rubix';

class DatePicker extends React.Component {

  static propTypes = {
    data: PropTypes.object,
  };

  render() {
    const { data, inline, optionsMode, options = {}, record } = this.props;
    const { required,practiceStyle } = options.general || {};
    const columnSize = inline ? 12 : data.fullWidth ? 12 : 6;
    return (
      <Col xs={columnSize} style={data.noPad ? { padding: '0px' } : data.paddingTop ? { paddingTop: '7px' }: null}>
      {!inline ?
        <label htmlFor={data.label}>
          <span style={data.blueTitle ? {color:"#38a0f1"} : data.orangeTitle ? {color:"#ea6225"} : null}>{data.label}</span>
          { practiceStyle ? <span style={{color:"#ea6225",fontSize:"14px"}}>*</span> : null}
          {required ? <span style={data.blueTitle ? {color: "#38a0f1", fontSize: "18px"} : data.orangeTitle ? {color:"#ea6225"} : data.asteriskColor ? {color: "#ea6225", fontSize: "18px"} : null}>*</span> : null}
        </label> : null }
        <Field name={data.value} id={data.label} data={data} component={ModernDatePicker} inputchange={this.props.inputchange} clearable={data.clearOption ? true : false} />
      </Col>
    );
  }

}

export default DatePicker;
