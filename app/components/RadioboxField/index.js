/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { Map as iMap } from "immutable";
import { Col, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import Info from 'containers/PatientsPage/components/Info';

const renderRadioField = (props) => {
  const { input, required, data = {}, fields, initialValues, handleButtonChange, editPage } = props;
  const Input = iMap(input).toJS();

  if(editPage == true) {
    if(Input.value == 'tutorials') {
        props.handleButtonChange(Input.value, Input.name)
    }
    if(Input.value == 'manuals') {
        props.handleButtonChange(Input.value, Input.name)
    }
    if(Input.value == 'section') {
        props.handleButtonChange(Input.value, Input.name)
    }
  }

  const clearInput = () => {
    input.onChange('');
    const radioBoxes = document.getElementsByName(Input.value);
    for (var index = 0; index < radioBoxes.length; ++index) {
      radioBoxes[index].checked = false;
    }  
  }

  return <Col className={styles.fields}>
    {data.oneOf.map((option, index) =>
      <label className={styles.container} key={index} >
        <span className={styles.title}>{option.title}</span>
        <input name={Input.value} type="radio" value={option.value} defaultChecked={Input.value === option.value} onChange={(e) => input.onChange(option.value)} required={data.required} />
        <span className={styles.checkmark}></span>
      </label>)}
  </Col>
}


class MultiRadioBoxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showAttributes: null, selectedOptions: false, isExpand: props.data.collapse ? true : false };
  }

  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    path: PropTypes.string,
  };

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  handleButtonChange(event, name) {
    let selectedValueData = Object.values(event).join("").substring(0, 9);
    let selectedValue = selectedValueData.indexOf("tutorials") > -1 ? "tutorials" : selectedValueData.indexOf("manuals") > -1 ? "manuals" : "section";
    this.props.inputchange(selectedValue, name);
  }

  render() {
    const { data = {}, options = {}, optionsMode, infostyle, fields, initialValues, editPage } = this.props;
    const { isExpand } = this.state;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={12} className={styles.infoField}>
        <label htmlFor={data.value} style={{ display: 'flex' }}>
          {data.collapse ?
            isExpand ?
              <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/plus.svg`)} /> :
                <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/minus.svg`)} /> : null}
          <span style={data.labelStyle ? {color: '#EA6225'} : null}>{data.label}</span>
          {data.info ? <Info data={data} /> : null}
        </label>
        {!isExpand ?
          <Field
            name={data.value}
            id={data.value}
            component={renderRadioField}
            data={data}
            fields={fields}
            editPage={editPage}
            initialValues={initialValues}
            {...options.general}
            {...extraFieldOptions}
            handleButtonChange={this.handleButtonChange.bind(this)}
            onChange={event => {
              if(this.props.data.onChange){
                this.handleButtonChange(event, data.value)
              }
            }}
          /> : null
        }
      </Col>
    );
  }



}

export default MultiRadioBoxField;
