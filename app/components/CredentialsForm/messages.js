/*
 * LoginForm Messages
 *
 * This contains all the text for the LoginForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  identifier: {
    id: 'app.components.LoginForm.identifier',
    defaultMessage: 'Email Address',
  },
  secret: {
    id: 'app.components.LoginForm.secret',
    defaultMessage: 'Password',
  },
  submit: {
    id: 'app.components.LoginForm.submit',
    defaultMessage: 'Log In',
  },
});
