/**
*
* CredentialsForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import { Link } from 'react-router';

import TogglePasswordField from 'components/TooglePasswordField';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { Button, FormGroup } from '@sketchpixy/rubix';

import styles from './styles.css';

const CredentialsForm = (props) => {
  const { handleSubmit, pristine, submitting, inputType, type = 'login', submitLabel = 'Submit' } = props;

  return (
    <form onSubmit={handleSubmit}>
      {type === 'login' || type === 'forgot' ?
        <FormGroup>
          <FormattedMessage {...messages.identifier} />
          <Field name="identifier" component="input" type="email" className={styles.inputField} required />
        </FormGroup> : null}

      {type === 'login' || type === 'reset' ?
        <FormGroup>
          <FormattedMessage {...messages.secret} />
          <TogglePasswordField inputType={inputType} />
        </FormGroup> : null}

      <FormGroup>
        <Button bsSize="lg" bsStyle="white" className={styles.loginButton} type="submit" disabled={submitting}>
          {submitLabel}
        </Button>
      </FormGroup>
      <FormGroup style={{ textAlign: 'right', marginTop: 0 }}>
        <Link className={styles.resetPassword} to={type === 'login' ? '/forgot' : '/'}>
          {type === 'login' ? 'Forgot password?' : 'Back to login.'}
        </Link>
      </FormGroup>
    </form>
  );
};

CredentialsForm.propTypes = {
  type: PropTypes.oneOf(['login', 'forgot', 'reset']),
  submitLabel: PropTypes.string,
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  inputType: PropTypes.string,
};

export default reduxForm({
  form: 'credentials',
})(CredentialsForm);
