import React from 'react';
import PropTypes from 'prop-types'
import styles from './styles.css';
import classNames from 'classnames';
import {branch, renderNothing} from "recompose";

const Marker = ({state, tooltipText}) => (
  <div className={styles.tooltip}>
    <span className={styles.tooltiptext}>{tooltipText}</span>
    <div className={classNames(styles.circle, styles[state])}/>
  </div>
);

Marker.PropTypes = {
  state: PropTypes.oneOf(['warning', 'error']),
  tooltipText: PropTypes.string
};

export default branch(props => !props.state, renderNothing)(Marker);

