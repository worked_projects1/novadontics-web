import Label from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const data = [
  {
    id: 1,
    label: 'test1',
  },
  {
    id: 2,
    label: 'test2',
  },
];

describe('<Label />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <Label
        data={data}
      />
    );
    expect(wrapper.props().data).toEqual(data);
  });
});
