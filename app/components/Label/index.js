/**
*
* Label
*
*/

import React from 'react';
import PropTypes from 'prop-types';

class Label extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  render() {
    const { data = {} } = this.props;
    const { label, value } = data;
    return (
      <div>
        <p><strong><span dangerouslySetInnerHTML={{__html: label}}/></strong></p>
        <p>{value}</p>
      </div>
    );
  }
}

export default Label;
