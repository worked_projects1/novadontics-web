/**
* Chart
*/

import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'chart.js';
import shallowCompare from 'react-addons-shallow-compare';
import styles from './styles.css';
import { findPercentage } from 'utils/tools.js';
import Spinner from '../Spinner';


class Charts extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = { pageload: false }
    this.handleChart = this.handleChart.bind(this)
  }
  static propTypes = {
    records: PropTypes.array,
    labels: PropTypes.array,
    title: PropTypes.string,
    type: PropTypes.string,
    labelCurrency: PropTypes.string
  };

  componentDidMount() {
    this.handleChart();
  }
  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.labels) !== JSON.stringify(this.props.labels) && JSON.stringify(prevProps.records) !== JSON.stringify(this.props.records)) {
      this.setState({ pageload: true });
      setTimeout(() => {
        this.setState({ pageload: false });
        this.handleChart();

      }, 500)

    }
  }

  handleChart() {
    const { records, id, labels, title, type, handlefilter, labelCurrency = "" } = this.props;
    var canvas = document.getElementById(id);
    if (canvas) {
      var ctx = canvas.getContext("2d");
      const hoverColors = ['#7bc8b3', '#419cd6', '#fac952', '#595959', '#7e1f43', ' #bf4746', '#49e274', '#a64ee4', '#f571a3', '#bcaa7a', '#86939c', '#f1a341', '#f7211e'];
      const chartColors = ['#7bc8b3', '#419cd6', '#fac952', '#595959', '#7e1f43', ' #bf4746', '#49e274', '#a64ee4', '#f571a3', '#bcaa7a', '#86939c', ' #f1a341', '#f7211e'];
      const chartOptions = new Chart(ctx, {
        type: 'bar',
        data: {
          labels,
          datasets: [
            {
              label: title,
              backgroundColor: chartColors,
              hoverBackgroundColor:hoverColors,
              data: records,
            },
          ],
        },
        labels: {
          boxWidth: 80,
        },
        options: {
          scales: {
            xAxes: [{
              gridLines: {
                color: "rgba(0, 0, 0, 0)",
              },
              ticks:{
                beginAtZero: true,
                min: 0,
                suggestedMin: 0
              }
            }],
            yAxes: [{
              gridLines: {
                color: "rgba(0, 0, 0, 0)",
              },
              ticks:{
                beginAtZero: true,
                min: 0,
                suggestedMin: 0,
                userCallback: function(label, index, labels) {
                  if (Math.floor(label) === label) {
                      return label;
                  }
                }
              }
            }]
          },
          hover: {
            "enabled": true
          },
          responsiveAnimationDuration: 0,
          legend: {
            display: false,
          },
          tooltips: {
            "enabled": true
          },
        },
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {

    const { records, id, labels, defaultvalue, handlefilter,options } = this.props;
    const { pageload } = this.state;
    if (pageload) {
      return <Spinner />
    }
    return (
      <div className={styles.chart}>
        <select className={styles.chartType} defaultValue={defaultvalue} onChange={handlefilter}>
          {
            options.map((elem,i) => <option value={elem}> 20{elem} </option> )
          }
        </select>
        <canvas ref={(c) => { this.chart = c; }} id={id} height="250" width="250" />
      </div>
    );
  }
}

export default Charts;
