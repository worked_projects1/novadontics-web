import React from 'react';
import AddPage from '../AddPage';
import EditPage from '../EditPage';
import Helmet from 'react-helmet';
import { Alert, Col, Row, Button } from '@sketchpixy/rubix';
import Spinner from 'components/Spinner';
import ModalForm from '../../fragment/ModalForm';
import styles from './styles.css';
import ReactTableWrapper from '../../fragment/ReactTableWrapper';
import ShowAlert from '../../fragment/ShowAlert';
import SaasPaymentSummary from '../../fragment/SummaryTable';
import ProviderForm from '../../fragment/ProviderForm';
import ColumnPage from '../ColumnPage';
import SettingsPage from '../SettingsPage';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import FeeSchedulePage from '../FeeSchedulePage';
import { providerFields } from '../../utils/library';
import AlertMessage from 'components/Alert';



const convertColumns = (props = {}) => {
    const { columns, records, editing, val } = props;
    const record = records && editing && records.filter(_ => _.id === editing)[0] || {};
    const { types } = record;
    let fields = [...columns];
    fields.map(function (el) {
        return (['title', 'firstName', 'lastName', 'types'].includes(el.value) && (types == 'media' || types == 'insurance' || types == 'Lab' || types == 'ER' || types == 'Hospital' || types == 'MRI Lab')) ?
              el.disabled = true :
              (['title', 'value', 'firstName', 'lastName', 'types'].includes(el.value) && types == 'walk-in') ?
                el.disabled = true :
                (['value', 'types'].includes(el.value) && types == 'doctor_referral') ?
                  el.disabled = true :
                  (['title', 'value', 'types'].includes(el.value) && types == 'patient_referral') ?
                    el.disabled = true :
                    (['name', 'types'].includes(el.value) && (types == 'Dental Specialist' || types == 'Medical Specialist' || types == 'Dermatologist' || types == 'Endodontist' || types == 'ENT' || types == 'MD' || types == 'Oral Implantologist' || types == 'Oral Pathologist' || types == 'Oral Surgeon' || types == 'Orthodontist' || types == 'Prosthodontist')) ?
                    el.disabled = true :
                    (['title', 'firstName', 'lastName'].includes(el.value) && (val == 'media' || val == 'insurance' || val == 'Lab' || val == 'ER' || val == 'Hospital' || val == 'MRI Lab')) ?
                      el.disabled = true :
                      (['title', 'value', 'firstName', 'lastName'].includes(el.value) && val == 'walk-in') ?
                        el.disabled = true :
                        (['value'].includes(el.value) && val == 'doctor_referral') ?
                          el.disabled = true :
                          (['name'].includes(el.value) && (val == 'Dental Specialist' || val == 'Medical Specialist' || val == 'Dermatologist' || val == 'Endodontist' || val == 'ENT' || val == 'MD' || val == 'Oral Implantologist' || val == 'Oral Pathologist' || val == 'Oral Surgeon' || val == 'Orthodontist' || val == 'Prosthodontist')) ?
                          el.disabled = true :
                          (['title', 'value'].includes(el.value) && val == 'patient_referral') ?
                            el.disabled = true : el.disabled = false
    });

    return fields;
}

export default function html(props) {
    const { path, name, columns, id, records, error, loading, handleChange, schema = {}, metaData, handleCreate, handleEdit, handleReload, handleError, editing, onCancel, onSelect, onChangeInput, user, showAlert, alertText, onChangeAlert } = props;
    const { create, view, edit, infostyle, inline, label, showTab, settingsView, provider } = schema;
    let preview = {}
    let previewData = {}
    if(provider && editing) {
      preview = records && records.filter(_ => _.id === editing)[0];
      previewData = Object.assign({}, preview, { education: { education: preview.education }, experience: { experience: preview.experience }, awards: { awards: preview.awards } });
    }

    const handleCreateSubmit = (data, dispatch, {form}) => {
      const record = data.toJS();
      if (record.profileImage == '' || record.profileImage == undefined) {
        onChangeAlert(true, 'Please upload Profile Image in Basic Details tab');
      } else if (record.title == '' || record.title == undefined) {
        onChangeAlert(true, 'Please select Title in Basic Details tab');
      } else if (record.firstName == '' || record.firstName == undefined) {
        onChangeAlert(true, 'Please enter First Name in Basic Details tab');
      } else if (record.lastName == '' || record.lastName == undefined) {
        onChangeAlert(true, 'Please enter Last Name in Basic Details tab');
      } else if (record.dob == '' || record.dob == undefined) {
        onChangeAlert(true, 'Please select Date of birth in Basic Details tab');
      } else if (record.gender == '' || record.gender == undefined) {
        onChangeAlert(true, 'Please select Gender in Basic Details tab');
      } else if (record.bio == '' || record.bio == undefined) {
        onChangeAlert(true, 'Please enter Bio in Basic Details tab');
      } else if (record.role == '' || record.role == undefined) {
        onChangeAlert(true, 'Please select Role in Basic Details tab');
      } else if (record.education == undefined) {
        onChangeAlert(true, 'Please enter Education in Other information tab');
      } else if (record.experience == undefined) {
        onChangeAlert(true, 'Please enter Experience in Other information tab');
      } else {
        let educationArr = typeof record.education === 'object' ? record.education.education : [];
        educationArr = educationArr.length > 0 ? educationArr.map((item) => { item.educationYear = parseInt(item.educationYear); return item; }) : []
        let experienceArr = typeof record.experience === 'object' ? record.experience.experience : [];
        let awardArr = typeof record.awards === 'object' ? record.awards.awards : [];
        awardArr = awardArr.length > 0 ? awardArr.map((item) => { item.awardsYear = parseInt(item.awardsYear); return item; }) : [];
        if(educationArr.length === 0) {
          onChangeAlert(true, 'Please enter Education in Other information tab');
        } else if(experienceArr.length === 0) {
          onChangeAlert(true, 'Please enter Experience in Other information tab');
        } else {
          record.education = educationArr;
          record.experience = experienceArr;
          record.awards = awardArr;
          record.color = record.color == '' || record.color == undefined ? '#ffdfd9' : record.color;
          handleCreate(record, dispatch, {form})
        }
      }
    };

    const handleEditSubmit = (data, dispatch, {form}) => {
      const record = data.toJS();
      if (record.profileImage == '' || record.profileImage == null) {
        onChangeAlert(true, 'Please upload Profile Image in Basic Details tab');
      } else if (record.title == '' || record.title == null) {
        onChangeAlert(true, 'Please select Title in Basic Details tab');
      } else if (record.firstName == '' || record.firstName == null) {
        onChangeAlert(true, 'Please enter First Name in Basic Details tab');
      } else if (record.lastName == '' || record.lastName == null) {
        onChangeAlert(true, 'Please enter Last Name in Basic Details tab');
      } else if (record.dob == '' || record.dob == null) {
        onChangeAlert(true, 'Please select Date of birth in Basic Details tab');
      } else if (record.gender == '' || record.gender == null) {
        onChangeAlert(true, 'Please select Gender in Basic Details tab');
      } else if (record.bio == '' || record.bio == null) {
        onChangeAlert(true, 'Please enter Bio in Basic Details tab');
      } else if (record.role == '' || record.role == null) {
        onChangeAlert(true, 'Please select Role in Basic Details tab');
      } else if (record.education == undefined) {
        onChangeAlert(true, 'Please enter Education in Other information tab');
      } else if (record.experience == undefined) {
        onChangeAlert(true, 'Please enter Experience in Other information tab');
      } else {
        let educationArr = typeof record.education === 'object' ? record.education.education : [];
        educationArr = educationArr.length > 0 ? educationArr.map((item) => { item.educationYear = parseInt(item.educationYear); return item; }) : []
        let experienceArr = typeof record.experience === 'object' ? record.experience.experience : [];
        let awardArr = typeof record.awards === 'object' ? record.awards.awards : [];
        awardArr = awardArr.length > 0 ? awardArr.map((item) => { item.awardsYear = parseInt(item.awardsYear); return item; }) : [];
        if(educationArr.length === 0) {
          onChangeAlert(true, 'Please enter Education in Other information tab');
        } else if(experienceArr.length === 0) {
          onChangeAlert(true, 'Please enter Experience in Other information tab');
        } else {
          record.education = educationArr;
          record.experience = experienceArr;
          record.awards = awardArr;
          handleEdit(record, dispatch, {form})
        }
      }
    };

    return (
        <div>
            <Helmet title="Novadontics" />
            <div>
                <Row>
                    <Col className={styles.title}>{label == 'Referral Source' ? label.replace(/Referral Source/g, "Referral Source Setup") : label == 'SaaS' ? label.replace(/SaaS/g, "Manage Your SaaS Subscriptions") : label == 'Fee Schedule' ? label.replace(/Fee Schedule/g, "Manage Fee Schedule") : label == 'Office Info' ? label.replace(/Office Info/g, "Office Setup") : label}</Col>
                    <Col sm={12} className={styles.flexibleRow}>
                        <Col className={styles.title}></Col>
                        {!inline && create && provider ?
                            <ProviderForm
                                basicFields={providerFields.basicDetails}
                                licenseFields={providerFields.licenseInformation}
                                otherFields={providerFields.otherInformation}
                                form={`createRecord`}
                                onSubmit={handleCreateSubmit.bind(this)}
                                config={{ title: `Add New ${label.replace('Setup', '')}`, label: label }}
                                path={path}
                                metaData={metaData}
                                error={error}
                                handleChange={(val) => onChangeInput(val)}
                            /> : !inline && create && !provider ?
                            <ModalForm
                                fields={name === 'referralSources' && convertColumns(Object.assign({}, { columns, records, editing: false, val: props.selectedValue })) ||name === 'referredTo' && convertColumns(Object.assign({}, { columns, records, editing: false, val: props.selectedValue }))  || columns}
                                form={`createRecord`}
                                onSubmit={handleCreate.bind(this)}
                                config={{ title: `Add New ${label.replace('Setup', '')}`, label: label }}
                                path={path}
                                metaData={metaData}
                                error={error}
                                handleChange={(val) => onChangeInput(val)}
                            /> : <div style={{ width: '94.05px' }}>&nbsp;</div>}
                    </Col>
                </Row>
                {inline ?
                  <Row>
                    {create ? <AddPage {...props} inline={true} /> : null}
                    {edit && !create ? <EditPage {...props} error={error} /> : null}
                  </Row> : settingsView && edit ? <SettingsPage {...props} infostyle={true} /> :
                  infostyle && edit ? <ColumnPage {...props} infostyle={true} /> : null }
                <Row className={name == 'insuranceFee' ? styles.bgStyle : null}>
                    <Col sm={12}>
                        <div>
                            {editing && !inline && provider ?
                                 <ProviderForm
                                     initialValues={records && records.filter(_ => _.id === editing)[0]}
                                     basicFields={providerFields.basicDetails}
                                     licenseFields={providerFields.licenseInformation}
                                     otherFields={providerFields.otherInformation}
                                     form={`editRecord`}
                                     showModal={true}
                                     cancelModal={onCancel.bind(this)}
                                     //onSubmit={handleCreate.bind(this)}
                                     onSubmit={handleEditSubmit.bind(this)}
                                     config={{ title: `Edit ${label.replace('Setup', '')}`, noButton: true }}
                                     path={path}
                                     metaData={metaData}
                                     error={error}
                                 /> : editing && !inline && !provider ?
                                <ModalForm
                                    initialValues={records && records.filter(_ => _.id === editing)[0]}
                                    fields={name === 'referralSources' &&  convertColumns(Object.assign({},{columns, records, editing})) || name === 'referredTo' &&  convertColumns(Object.assign({},{columns, records, editing})) || columns}
                                    form={`editRecord`}
                                    showModal={true}
                                    cancelModal={onCancel.bind(this)}
                                    onSubmit={handleEdit.bind(this)}
                                    config={{ title: `Edit ${label.replace('Setup', '')}`, noButton: true, name: 'saas' }}
                                    path={path}
                                    metaData={metaData}
                                    error={error}
                                /> : null}
                            {view ? showTab ? <FeeSchedulePage {...props} /> : <ReactTableWrapper
                                {...props}
                                initialValues={records && records.filter(_ => _.id === editing)[0]}
                                onSelect={onSelect.bind(this)}
                                onCancel={onCancel.bind(this)}
                                onReload={handleReload.bind(this)}
                                onSubmit={handleEdit.bind(this)} /> : null}
                            {error ? <ShowAlert error={error} dismiss={handleError.bind(this)} /> : null}
                            {name == 'saas' ?
                              <div className={styles.feeStyle}>
                                <SaasPaymentSummary user={user} />
                              </div> : null}
                            {name == 'providers' ? <div className={styles.textStyle}>The expiration date of any license will turn red in color when within 30 days of its expiration.</div> : null}
                            {showAlert ?
                              <AlertMessage warning dismiss={() => { onChangeAlert(false, '') }}>{alertText}</AlertMessage> : null}
                        </div>
                    </Col>
                </Row>
                {loading ? <Spinner /> : null}
            </div>
        </div>
    );
}