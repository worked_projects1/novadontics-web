/*
 * ListPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import {compose, withProps, withState, withHandlers} from 'recompose';
import { _map } from '../../utils/library';
import html from './html';
import { selectUser } from 'blocks/session/selectors';

class ListPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
       return html(this.props);
    }

}

function mapStateToProps(state, props) {
    const { selectRecords, selectLoading, selectError, selectUpdateError, selectRecordsMetaData } = props.selectors;
    const user = selectUser()(state, props);
    const loading = selectLoading()(state, props);
    const records = selectRecords()(state, props);
    const error = selectError()(state, props) || selectUpdateError()(state, props);
    const metaData = selectRecordsMetaData()(state, props);
    return {
        user,
        metaData: metaData ? metaData.toJS() : {},
        loading,
        records: records ? records.toJS() : [],
        error
    };
}


export default compose(
    connect(mapStateToProps),
    withProps(({actions, dispatch}) => ({
        handleCreate: (data, dispatch, {form}) => dispatch(actions.createRecord(data.toJS(), form)),
        handleEdit: (data, dispatch, {form}) => dispatch(actions.updateRecord(data.toJS(), form)),
        handleReload: () => dispatch(actions.loadRecords()),
        handleError: () => dispatch(actions.updateRecordError(false))
    })),
    withState('editing', 'onEditing', null),
    withState('selectedData', 'onSelectedData', null),
    withState('selectedValue', 'onSetSelectedValue', null),
    withState('showAlert', 'onSetAlertMessage', null),
    withState('alertText', 'onSetAlertText', null),
    withHandlers({
        onSelect: props => (editing, selectedData) => { 
            props.onEditing(editing);
            props.onSelectedData(selectedData);
        },
        onCancel: props => args => {
            props.onEditing(false);
            props.onSelectedData(false);
        },
        onChangeInput: props => (val) => {
            props.onSetSelectedValue(val);
        },
        onChangeAlert: props => (alert, text) => {
            props.onSetAlertMessage(alert);
            props.onSetAlertText(text);
        }
    })
  )(ListPage);