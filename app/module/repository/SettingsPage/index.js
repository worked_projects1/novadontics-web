/*
 * SettingsPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {compose, withProps} from 'recompose';
import { connect } from 'react-redux';
import SettingsForm from '../../fragment/SettingsForm';
import AlertMessage from 'components/Alert';
import { _map, columns } from '../../utils/library';

class SettingsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { alertMessage: "", showAlert: false };
  }

  handleUpdate(data, dispatch, { form }) {
    const { actions } = this.props;
    const { showAlert, alertMessage } = this.state;
    let submitRecord = data.toJS();
    let onlineRegCheck = submitRecord.onlineRegistrationModule ? submitRecord.onlineInviteMessage.includes('<<link>>') === true && submitRecord.onlineInviteMessage.includes('<<patient name>>') === true ? false : true : false;
    let consentCheck = submitRecord.onlineRegistrationModule ? submitRecord.consentFormMessage.includes('<<link>>') === true && submitRecord.consentFormMessage.includes('<<form name>>') === true && submitRecord.consentFormMessage.includes('<<patient name>>') === true ? false : true : false;
    let postopCheck = submitRecord.onlineRegistrationModule ? submitRecord.postopInstructionsMessage.includes('<<link>>') === true && submitRecord.postopInstructionsMessage.includes('<<form name>>') === true && submitRecord.postopInstructionsMessage.includes('<<patient name>>') === true ? false : true : false;
    let appointmentCheck = submitRecord.appointmentReminder ? submitRecord.appointmentReminderMessage.includes('<<link>>') === true && submitRecord.appointmentReminderMessage.includes('<<patient name>>') === true && submitRecord.appointmentReminderMessage.includes('<<practice name>>') === true && submitRecord.appointmentReminderMessage.includes('<<date>>') === true && submitRecord.appointmentReminderMessage.includes('<<time>>') === true ? false : true : false;
    let recallCheck = submitRecord.appointmentReminder ? submitRecord.recallReminderMessage.includes('<<interval>>') === true && submitRecord.recallReminderMessage.includes('<<patient name>>') === true && submitRecord.recallReminderMessage.includes('<<practice name>>') === true && submitRecord.recallReminderMessage.includes('<<practice phone>>') === true ? false : true : false;
    if(appointmentCheck) {
      this.setState({showAlert: true, alertMessage: "Please use all these placeholders <<practice name>>, <<patient name>>, <<date>>,<<time>>, <<link>> in Appointment Reminder Message."})
    } else if(onlineRegCheck) {
      this.setState({showAlert: true, alertMessage: "Please use all these placeholders <<patient name>>, <<link>> in Online Invite Message."})
    } else if(consentCheck) {
      this.setState({showAlert: true, alertMessage: "Please use all these placeholders <<patient name>>, <<form name>>, <<link>> in Consent Form Message."})
    } else if(postopCheck) {
      this.setState({showAlert: true, alertMessage: "Please use all these placeholders <<patient name>>, <<form name>>, <<link>> in Postop Instructions Message."})
    } else if(recallCheck) {
      this.setState({showAlert: true, alertMessage: "Please use all these placeholders <<patient name>>, <<interval>>, <<practice name>>, <<practice phone>> in Recall Reminder Message."})
    } else {
      dispatch(actions.updateRecord(submitRecord, form))
    }
  };

  render() {
    const { record, loading, location = {}, name, path, infostyle, metaData, actions } = this.props;
    const { showAlert, alertMessage } = this.state;

    return (
      <div>
        {!loading ? <SettingsForm
          initialValues={record || {}}
          name={name}
          path={path}
          onlineInvitationFields={this.getOnlineInvite(columns.onlineInvitation)}
          consentInviteFields={this.getConsentInvite(columns.consentInvite)}
          appointmentConsultationFields={this.getAppointmentConsultation(columns.appointmentConsultation)}
          recallReminderFields={this.getRecallReminder(columns.recallReminder)}
          record={record}
          onSubmit={this.handleUpdate.bind(this)}
          locationState={location.state}
          infostyle={infostyle}
        /> : null}
        {this.state.showAlert ?
          <AlertMessage warning dismiss={() => { this.setState({ showAlert: false });}}>{alertMessage}</AlertMessage> : null}
      </div>
    );
  }

  getOnlineInvite(columns) {
    const { record } = this.props;
    let fields = columns;
    if(record.onlineRegistrationModule == false) {
      fields.map(function (el) {
        return el.disabled = true
      });
    }
    return fields;
  }

  getConsentInvite(columns) {
    const { record } = this.props;
    let fields = columns;
    if(record.onlineRegistrationModule == false) {
      fields.map(function (el) {
        return el.disabled = true
      });
    }
    return fields;
  }

  getAppointmentConsultation(columns) {
    const { record } = this.props;
    let fields = columns;
    if(record.appointmentReminder == false) {
      fields.map(function (el) {
        return el.disabled = true
      });
    }
    return fields;
  }

  getRecallReminder(columns) {
    const { record } = this.props;
    let fields = columns;
    if(record.appointmentReminder == false) {
      fields.map(function (el) {
        return el.disabled = true
      });
    }
    return fields;
  }

}

function mapStateToProps(state, props) {
  const { selectError, selectRecord, selectUpdateError } = props.selectors;
  const selectedRecord = selectRecord(props.practiceId, 10)(state, props);  
  const error = selectError()(state, props) || selectUpdateError()(state, props);
  return {
    record: selectedRecord ? selectedRecord.toJS() : {},
    error: error,
  };
}

export default compose(
  connect(mapStateToProps)
)(SettingsPage);