/*
 * ListPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import {compose, withProps, withState, withHandlers} from 'recompose';
import { _map } from '../../utils/library';
import FeeScheduleForm from './html';

export default  class CopyPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
       return <FeeScheduleForm {...this.props}/>;
    }

}

