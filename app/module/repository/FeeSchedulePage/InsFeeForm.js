/**
*
* InsFeeForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import ModalForm from 'components/ModalRecordForm';
import EditRecordForm from 'components/EditRecordForm';
import { browserHistory } from 'react-router';
import moment from 'moment-timezone';
import { loadFeeFields, updateFeeDetails } from 'blocks/insurance/remotes.js';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';

class InsFeeForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      showSpinner: false,
      successAlert: false,
      failureAlert: false,
      records: [],
      columns: []
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  render() {
    const { row, style, procedureDetails, insuranceCompanyList, dentalCarrierList } = this.props;
    const { photoIndex, isOpen, records, columns, showSpinner } = this.state;

    return (
      <Col xs={12} className={styles.actions}>
        <Button bsStyle="link" bsSize="xs" className={styles.link} onClick={() => this.handleOpen()}>
          Manage Fee
        </Button>
        <Modal show={isOpen} className="insuranceFee" backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={{ color:'#87b44d', fontWeight: 'bold' }}>Manage Fee</span>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body id="modalrecordform">
            <Col sm={12}>
              <div className={styles.divStyle}>
                <h5 className={styles.codeStyle}>{procedureDetails.procedureCode} :</h5>
                <span className={styles.descStyle}>{procedureDetails.procedureType}</span>
              </div>
            </Col>
            <Col className={styles.rowStandard}>
              {!showSpinner ?
                <EditRecordForm
                  fields={columns}
                  initialValues = {records}
                  form={`ManageFeeForm`}
                  onSubmit={(data) => this.handleSubmit(data)}
                  name={"feeSchedule"}
                  disableCancel={true}
                  catalogButton={true}
                  btnName={'Update'}
                  onCancel={() => this.handleClose()}
                /> : <Spinner />}
            </Col>
            {this.state.successAlert ?
              <AlertMessage success dismiss={() => { this.setState({ successAlert: false }); this.handleClose();}}><FormattedMessage {...messages.feeSuccess} /></AlertMessage> : null}
            {this.state.failureAlert ?
              <AlertMessage warning dismiss={() => this.setState({ failureAlert: false })}><FormattedMessage {...messages.feeFailure} /></AlertMessage> : null}
          </Modal.Body>
          <Modal.Footer style={{ border: '0px', padding: '0px' }}>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }

  handleOpen() {
    this.setState({ isOpen: true, showSpinner: true });

    const procedure = this.props.procedureDetails;
    const procedureCode = procedure.procedureCode.split('"').join('');

    loadFeeFields(procedureCode)
      .then(procedureRecords => {

        const filteredColumns = this.props.dentalCarrierList.reduce((a, el, i) => { a.push(Object.assign({}, { value: `fee_${el.id}`, label: el.name, insuranceId: el.id, visible: true, editRecord: true, viewRecord: true, viewMode: true, currency: true, number: true, type: 'input' })); return a; }, Object.assign([], this.state.columns));
        const filteredRecords = procedureRecords.reduce((a, el) => Object.assign({}, a, { [`fee_${el.insuranceId}`]: el.fee }), {});

        setTimeout(() => {
          this.setState({columns: filteredColumns, records: filteredRecords, showSpinner: false});
        }, 2000);

      })
      .catch(() => this.setState({ records: [], columns: [] }));
  }

  handleClose() {
    this.setState({ isOpen: false, records: [], columns: [], showSpinner: false });
  }

  handleSubmit(data) {
    const procedure = this.props.procedureDetails;
    const procedureCode = procedure.procedureCode.split('"').join('');
    const submitRecord = data.toJS();
    var records = Object.keys(submitRecord).map(key => ({insuranceId: key, fee: submitRecord[key]}));
    const updatedData = records.map(item => ({
        insuranceId: parseInt(item.insuranceId.replace('fee_','')),
        fee: parseFloat(item.fee)
    }))
    const finalData = { insuranceFee: updatedData }

    updateFeeDetails(procedureCode, finalData)
      .then(procedureRecords => {
        this.setState({ successAlert: true });
      })
      .catch(() => this.setState({ failureAlert: true }));
  }
}

export default InsFeeForm;
