/*
 * InsurancePage Container
 */

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createSelector } from 'reselect';
import _ from 'lodash';

import Spinner from 'components/Spinner';
import TableWrapper from 'components/TableWrapper';
import ModalForm from 'components/ModalRecordForm';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import styles from './styles.css';
import InsFeeForm from './InsFeeForm.js';

import { Alert, Button, Col, PanelBody, PanelContainer, Row, Icon, Modal } from '@sketchpixy/rubix';
import CsvImporter from 'components/CsvImporter';
import { Grid } from '@sketchpixy/rubix/lib/index';
// import schema from '../../routes/schema';
import { setApiFilter } from 'utils/api';
import { type } from 'os';
import { loadStateTax, updateStateTax } from 'blocks/vendors/stateTax';
import StateTax from 'components/StateTax';
import { Collapse } from 'antd';
import { selectUser } from 'blocks/session/selectors';
import ReactTable from "react-table";
import { currencyFormatter } from 'utils/currencyFormatter.js';
import ReactTooltip from 'react-tooltip';

const { Panel } = Collapse;


 export default  class CopyForm extends React.Component { // eslint-disable-line react/prefer-stateless-function

        static propTypes = {
            records: PropTypes.array,
            children: PropTypes.object,
            error: PropTypes.bool,
            updateError: PropTypes.string,
            loading: PropTypes.bool
        };

        constructor(props) {
            super(props);
            this.state = {
                editing: false,
                selectedData: false,
                showData: false,
                search: '',
                activeChildrenState: this.props.activeChildren,
                searchError: false
            };
        }

        componentDidMount() {
            const { headers = {}, dispatch, metaData = {} ,columns,actions} = this.props;
            const { procedureList = [], insuranceCompanyList = [], dentalCarrierList = [] } = metaData;
            const [status] = columns.filter((s) => s.value === 'status' || s.value === 'accountType');
            headers.status = status && status.oneOf && headers.status == '' ? status.oneOf[0] : headers.status;
            if (dispatch) {
                setApiFilter(Object.keys(headers).filter(_ => _ != "content-length").reduce((r, k) => Object.assign({}, r, { [k]: headers[k] }), {}));
                dispatch(actions.updateHeader({ 'status': headers.status }));
                dispatch(actions.loadRecords());
            }
        }

        componentDidUpdate(nextProps) {
            const { metaData = {}, activeChildren } = this.props;
            const { procedureList = []} = metaData;
            if (nextProps.activeChildren !== activeChildren) {
                this.setState({activeChildrenState: activeChildren })
            }
        }
           

        shouldComponentUpdate(nextProps, nextState) {
            return shallowCompare(this, nextProps, nextState);
        }

        editableColumnProps = {
            Cell: props => {
                const { dispatch, metaData = {} ,actions} = this.props;
                const { insuranceCompanyList = [], dentalCarrierList = [], procedureList = [] } = metaData;
                const { column, tdProps, original = {} } = props;
                const { id } = original;
                const { editing, selectedData = {} } = this.state
                const data = procedureList.filter(_ => _.categoryId === id);

                const handleUpdate = (e) => {
                    if (e.key === 'Enter') {
                        const submitRecord = Object.assign({}, { 'procedureCode': selectedData.procedureCode, 'insuranceId': parseInt(editing.split('_')[1]), 'fee': parseFloat(e && e.target && e.target.value || document.getElementsByName(`${column.value}_${id}`)[0].value) });
                        if (props.value && props.value != '') {
                            dispatch(actions.updateRecord(submitRecord));
                            this.setState({ editing: false });
                        } else {
                            dispatch(actions.createRecord(submitRecord));
                            this.setState({ editing: false });
                        }
                    }
                }

                return this.state.editing === `${column.value}_${id}` && column.editRecord ?
                    <div>
                        <input name={`${column.value}_${id}`} type="number" className={styles.input} defaultValue={props.value} onKeyDown={handleUpdate} />
                        <div className={styles.icons}>
                            <Icon glyph="icon-fontello-ok-circled" className={styles.ok} onClick={() => handleUpdate({ key: 'Enter' })} />
                            <Icon glyph="icon-fontello-cancel-circled" onClick={() => this.setState({ editing: false, selectedData: false })} />
                        </div>
                    </div> : column.action ?
                    <Col className={styles.actions}>
                      <InsFeeForm procedureDetails={{procedureCode: props.original.procedureCode, procedureType: props.original.procedureType}} insuranceCompanyList={insuranceCompanyList} dentalCarrierList={dentalCarrierList.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[{id: 0, name: 'Private'},{id: -1, name: 'UCR'}])} />
                    </Col> :
                    <span
                        className={column.value !== 'procedureType' ? styles.field : props.value.length >= 340 ? styles.descField : styles.descField1}
                        onClick={() => this.setState({ editing: `${column.value}_${id}`, selectedData: original })}
                        data-place={"bottom"} data-tip={column.label} >
                        {column.value === 'procedureType' && props.value.length >= 375 ? <Col>{props.value.substring(0, 375) + '...'}<div onClick={() => this.setState({ showData: props.value })} className={styles.viewDesc}></div></Col> : column.currency && props.value && props.value != '' ? currencyFormatter.format(props.value) : props.value}
                    </span>;
            }
        };

        handleChildren = (e) => {
            const { headers = {}, dispatch, metaData = {} } = this.props;
            const { procedureList = [], insuranceCompanyList = [], dentalCarrierList = [] } = metaData;

            //this.props.dispatch(actions.updateChildren(e));
            this.setState({activeChildrenState: e});
        }

        handleSearchCode = () => {
            if(this.state.search.length < 5) {
                this.setState({searchError: true})
            } else {
                const { headers = {}, dispatch, metaData = {} } = this.props;
                const { procedureList = [], insuranceCompanyList = [], dentalCarrierList = [] } = metaData;
                const data = procedureList.filter(_ => _.procedureCode === this.state.search.toUpperCase()).map(_ => _.categoryId);
                this.setState({activeChildrenState: ["8", `category_${data[0]}`]})
                this.setState({searchError: false})
            }
        }

        handleSearchInput = (e) => {
            const { value } = e.target;
            if(value.length <= 5)
              this.setState({ search: e.target.value })
        }

        render() {
           
            const { error, updateError, loading, children, location = {}, route = {}, dispatch, headers, metaData = {}, user, params, records,columns } = this.props;
            const { procedureList = [], insuranceCompanyList = [], dentalCarrierList = [] } = metaData;

            return (
                <div className="insurancePage">
                    <Helmet title="Novadontics" />
                    <ReactTooltip type="info" html={true} />
                    <div className={styles.view} >
                        <Grid>
                            <Row>
                              <Col sm={12} className={styles.flexibleRow}>
                                <span className={styles.titleFee}></span>
                                <span className={styles.groupSearch}>
                                    <span className={styles.search}>Search by Code: </span>
                                    <input type='text' value={this.state.search} className={styles.inputSearch} onChange={this.handleSearchInput} onKeyPress={event => {if (event.key === 'Enter') {this.handleSearchCode()}}}/>
                                    <Button onClick={this.handleSearchCode} style={{ backgroundColor: '#87b44d', borderColor: '#87b44d' }} className='btn-standard btn btn-default'>Go</Button>
                                    {this.state.searchError ? <span className={styles.errorMessage}>Enter five characters</span> : null}
                                </span>
                              </Col>
                            </Row>
                            <Row>
                              <Col sm={12} className={styles.feetitle}>
                                <h5 style={{fontWeight:"800"}}>Click on the CDT Code groups below to expand their section and edit the fees. Novadontics provides suggested UCR fees and fees for patients on in-office membership or discount plan.</h5>
                              </Col>
                            </Row>
                            <Row>
                                <Col className={styles.docBox} lg={12}>
                                    {error || updateError ?
                                        <Alert danger><span>{error ? <FormattedMessage {...messages.error} /> : updateError}</span></Alert> : null}
                                    <Collapse onChange={this.handleChildren} activeKey={this.state.activeChildrenState}>
                                        {records && procedureList && records.map((category, index) => {
                                            const { id, name } = category;
                                            const data = procedureList.filter(_ => _.categoryId === id);

                                            return <Panel header={name} key={`category_${id}`}>
                                                <Col className={styles.panel}>
                                                    <ReactTable
                                                        columns={columns.filter(a => a.viewRecord).map((column) => Object.assign( {}, { Header: column.label, accessor: column.value, width: column.width, ...column, ...this.editableColumnProps } ))}
                                                        data={data}
                                                        pageSize={data && data.length || 0}
                                                        showPageJump={false}
                                                        resizable={false}
                                                        showPageSizeOptions={false}
                                                        showPagination={false}
                                                        previousText={"Back"}
                                                        pageText={""}
                                                        noDataText={"No entries to show."}
                                                    />
                                                </Col>
                                            </Panel>
                                        })}
                                    </Collapse>
                                </Col>
                            </Row>
                        </Grid>
                        <Modal show={this.state.showData ? true : false} backdrop="static" onHide={()=>this.setState({showData:false})}>
                            <Modal.Header closeButton />
                            <Modal.Body>
                                {this.state.showData}
                            </Modal.Body>
                        </Modal>
                    </div>
                </div>
            );
        }

    }

