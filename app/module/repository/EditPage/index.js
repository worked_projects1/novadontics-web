/*
 * EditPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {compose, withProps} from 'recompose';
import { connect } from 'react-redux';
import html from './html';

class EditPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return html(this.props);
  }
}

function mapStateToProps(state, props) {
  const { selectError, selectRecord, selectUpdateError } = props.selectors;
  const selectedRecord = selectRecord(props.practiceId, 10)(state, props);  
  const error = selectError()(state, props) || selectUpdateError()(state, props);
  return {
    record: selectedRecord ? selectedRecord.toJS() : {},
    error: error,
  };
}

export default compose(
  connect(mapStateToProps),
  withProps(({actions}) => ({
    handleUpdate: (data, dispatch, {form}) => dispatch(actions.updateRecord(data.toJS(), form))
  }))
)(EditPage);