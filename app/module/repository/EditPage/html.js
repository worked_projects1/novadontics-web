import React from 'react';
import EditForm from '../../fragment/EditForm';

export default function html(props) {
    const {record, loading, location = {}, name, path, columns, handleUpdate } = props;
    return (
      <div>
        {!loading ? <EditForm
          initialValues={record || {}}
          name={name}
          path={path}
          fields={columns}
          onSubmit={handleUpdate.bind(this)}
          locationState={location.state}
        /> : null}
      </div>
    );
} 