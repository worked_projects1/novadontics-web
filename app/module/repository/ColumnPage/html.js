import React from 'react';
import ColumnForm from '../../fragment/ColumnForm';

export default function html(props) {
    const {record, loading, location = {}, name, path,infostyle, columns, handleUpdate, metaData } = props;
    return (
      <div>
        {!loading ? <ColumnForm
          initialValues={record || {}}
          name={name}
          path={path}
          fields={columns}
          metaData={metaData}
          onSubmit={handleUpdate.bind(this)}
          locationState={location.state}
          infostyle={infostyle}
        /> : null}
      </div>
    );
} 