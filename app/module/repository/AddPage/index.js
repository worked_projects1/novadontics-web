/*
 * AddPage
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {compose, withProps} from 'recompose';
import { connect } from 'react-redux';
import html from './html';

class AddPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return html(this.props);
  }
}


function mapStateToProps(state, props) {
  const { selectRecords, selectError } = props.selectors;
  const records = selectRecords()(state, props);
  const error = selectError()(state, props);
  return {
      records: records ? records.toJS() : [],
      error
  };
}


export default compose(
  connect(mapStateToProps),
  withProps(({actions}) => ({
    resolveColumns: (columns, records) => typeof columns === 'function' ? columns(records).columns : columns,
    handleSubmit: (data, dispatch, {form}) => dispatch(actions.createRecord(data.toJS(), form))
  }))
)(AddPage);