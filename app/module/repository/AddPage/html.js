import React from 'react';
import AddForm from '../../fragment/AddForm';
import moment from 'moment';

export default function html(props){
    const {records, location = {}, name, path, columns, practiceId, inline, resolveColumns, handleSubmit } = props;
    return (
      <div>
        <AddForm
          initialValues={{practiceId:practiceId, date: moment().format('YYYY-MM-DD')}}
          name={name}
          path={path}
          fields={resolveColumns(columns, records)}
          onSubmit={handleSubmit.bind(this)}
          locationState={location.state}
          inline={inline}
        />
      </div>
    );
}