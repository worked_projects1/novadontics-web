/**
*
* ShowAlert
*
*/

import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Modal, Row, Button } from '@sketchpixy/rubix';
import styles from './styles.css';


class ShowAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { error, dismiss, disableBtn } = this.props;
    return (<Modal show className={'deleteModal'}>
            <Modal.Body>
              <Row className={styles.rowStandard} style={app ? {textAlign: 'left', fontSize:'0.9em'} : ''}>
                <h4>Alert!</h4>
                <Col dangerouslySetInnerHTML={{__html: error ? error : ''}} />
              </Row>
            </Modal.Body>
            {!disableBtn ? <Modal.Footer className={styles.footer}>
              <Button bsStyle="standard" type="button" onClick={()=>dismiss()} className={styles.submitButton}>OK</Button>
            </Modal.Footer> : null}
        </Modal>);
  }

  confirm(){
    this.props.onConfirm();
    this.setState({ showModal: false });
  }

  close() {
    this.setState({ showModal: false });
  }
}


export default ShowAlert;
