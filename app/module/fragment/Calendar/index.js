

import React from 'react';
import CalendarApp from '../../utils/calendar';

class Calendar extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.onStart = this.onInit.bind(this);
    }

    componentDidMount() {
        this.onInit();
        this.props.onRef(this);
    }

    componentWillReceiveProps(nextProps) {
        const prevEvents = this.props.events;
        const nextEvents = nextProps.events;
        const received = (prevEvents.length == nextEvents.length) && prevEvents.every(function(element, index) {
            return JSON.stringify(element) === JSON.stringify(nextEvents[index]) 
        });
        if(!received){
            const calendar = new CalendarApp(nextProps);
        }    
    }

    onInit() {
        const calendar = new CalendarApp(this.props);
    }

    render() {
        return <div id="calendar"></div>
    }
}

export default Calendar;