/**
*
* SAAS Payment
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import { loadSaasPaymentSummary } from '../../utils/sagas.js';
import { loadAdminSaasPaymentSummary } from 'blocks/saasAdmin/remotes';
import moment from 'moment-timezone';
import { columns } from '../../utils/library';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';
import ReactTable from "react-table";
import { currencyFormatter } from 'utils/currencyFormatter.js';
import filterRows from 'utils/filter';
import Spinner from 'components/Spinner';

class SaasPaymentSummary extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      loading: false,
      records: []
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  render() {
    const { row, singleImage, style, user } = this.props;
    const { photoIndex, isOpen, records, loading } = this.state;
    const tableColumns = columns.saasPayment.filter((column) => column.visible);
    let rows = records && records != null && records.length > 0 ? filterRows(records.map((record) => Object.assign(
        {},
        record
      ),
    ), tableColumns) : [];

    if (tableColumns && tableColumns.length > 0) {
      tableColumns.forEach((column) => {
        switch (column.type) {
          case 'input':
            rows = rows.map((row) => Object.assign(
              row,
              { [column.value]: row[column.value] != undefined ? currencyFormatter.format(row[column.value]) : currencyFormatter.format(0) },
            ));
            break;
          default:
            break;
        }
      });
    }

    return (
      <Col xs={12} className={styles.actions}>
        <Button bsSize="xs" bsStyle="link" className={user && user.role == "dentist" ? styles.feeStyle : styles.adminFeeStyle} onClick={() => this.handleOpen()}>
          View Summary
        </Button>
        <Modal show={isOpen} className="saasModule" backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={user && user.role == "dentist" ? {color: '#87b44d', fontWeight: 'bold'} : {color: '#ea6225', fontWeight: 'bold'}}>SaaS Payment Summary</span>
            </Modal.Title>
          </Modal.Header>
            <Modal.Body>
              <Col className="saasModule">
                {!loading ?
                  <ReactTable
                    columns={tableColumns.map((column) => Object.assign(
                        {},
                        { Header: column.label, accessor: column.value, width: column.width, ...column }
                    ))}
                    data={rows}
                    pageSize={records && records.length > 0 ? 10 : 0}
                    showPageJump={false}
                    resizable={false}
                    showPageSizeOptions={false}
                    previousText={"Back"}
                    pageText={""}
                    noDataText={"No entries to show."}
                    sortable={false}
                  /> : <Spinner />}
              </Col>
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  handleOpen() {
    const { user } = this.props;
    if(user && user.role == "dentist") {
      this.setState({ isOpen: true, loading: true });
      loadSaasPaymentSummary()
        .then(summaryList => {
          this.setState({ records: summaryList, loading: false })
        })
        .catch(() => this.setState({ records: [], loading: false }));
    } else {
      this.setState({ isOpen: true, loading: true });
      loadAdminSaasPaymentSummary()
        .then(summaryList => {
          this.setState({ records: summaryList, loading: false })
        })
        .catch(() => this.setState({ records: [], loading: false }));
    }
  }

  handleClose() {
    this.setState({ isOpen: false });
  }
}

export default SaasPaymentSummary;
