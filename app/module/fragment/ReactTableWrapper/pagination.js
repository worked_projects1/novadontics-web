import React from "react";
import { Col, Pagination } from '@sketchpixy/rubix';
import PropTypes from "prop-types";

class ReactTablePagination extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    data: PropTypes.array,
    pageSize: PropTypes.number,
    page: PropTypes.number,
    onPageChange: PropTypes.func,
  };


  render() {
    const { pageSize, page, pages  } = this.props;
    return (
      <Col className="ReactTablePagination">
        <Pagination
          prev = "Back"
          next = "Next"
          boundaryLinks
          items={pages}
          maxButtons={4}
          activePage={page+1}
          onSelect={this.handlePagination.bind(this)} 
        />  
      </Col>
    );
  }

  handlePagination(e){
    this.props.onPageChange(e-1)
  }
}

export default ReactTablePagination;