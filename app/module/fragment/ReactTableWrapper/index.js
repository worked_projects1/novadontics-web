/**
*
* ReactTableWrapper
*
*/

import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { Col, Row, PanelContainer, PanelBody } from '@sketchpixy/rubix';
import Marker from "components/Marker";
import ReactPagination from "./pagination";
import filterRows from 'utils/filter';
import FormButton from '../../fragment/FormButton';
import styles from './styles.css';
import { Link } from 'react-router';
import moment from 'moment';
import Gallery from 'components/Gallery';
import ShowText from 'components/ShowText';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import Sortable from '../../fragment/Sortable';

const fieldComponent = ({ input, oneOf, type }) => {
  return type === 'select' ?
    <select name={input.name} className={styles.InputField} defaultValue={input.value} onBlur={e => input.onChange(e.target.value)} required>
      <option value="" disabled={true}>Please select</option>
      {(oneOf || []).map((option) =>
        <option key={option} value={option}>{option}</option>)}
    </select> : <input name={input.name} className={styles.InputField} type="text" defaultValue={input.value} onBlur={e => input.onChange(e.target.value)} required />;
};

class ReactTableWrapper extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    records: PropTypes.array,
    router: PropTypes.object,
    columns: PropTypes.array,
    children: PropTypes.object,
    path: PropTypes.string,
    locationState: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  editableColumnProps = {
    Cell: props => {
      const { column, tdProps } = props;
      const { dispatch } = this.props;
      const { mode, onEdit, onCancel, onDelete, inline } = tdProps.rest;
      return (column.action ?
        <FormButton onEdit={() => onEdit(props.index)} archive={column.archive} onCancel={() => onCancel()} onDelete={() => onDelete(props.index)} mode={mode} field={column} inline={inline} dispatch={dispatch} rowId={props.original.id} /> :
        column.editRecord && mode === 'edit' && inline ?
          <Field name={column.value} id={column.value} component={fieldComponent} type={column.type} oneOf={column.oneOf} /> : <span> {column.limit && props.value && props.value.length > column.limit ?
          props.value.substring(0, column.limit) + '...' : props.value} 
          {column.limit && props.value && props.value.length > column.limit ?
            <ShowText title={column.label} text={props.value}>{(open) => <div className={styles.view} onClick={open.bind(this)} />}</ShowText> : null}
        </span>
      );
    }
  };

  render() {
    const { name, records, columns, children, path, locationState = {}, loading, schema = {}, handleSubmit, onSelect, onCancel, editing } = this.props;
    const activePath = location.pathname;
    const tableColumns = children && columns && columns.length > 0 ? columns.filter((column) => column.visible && column.viewMode) : columns.filter((column) => column.visible);
    const { inline } = schema;
    let rows = filterRows(records.map((record) => Object.assign(
      {},
      record, {
      isActive: activePath === `${path}/${record.id}` || activePath === `${path}/${record.id}/edit`,
    }),
    ), tableColumns);

    const pageSize = rows.length > 0 && rows.length <= 10 ? rows.length : rows.length > 0 && rows.length > 10 ? 15 : 0;

    if (tableColumns && tableColumns.length > 0) {
      tableColumns.forEach((column) => {
        switch (column.type) {
          case 'agreement':
            {
              column.label == 'Document' && column.saasIcon == true ? rows.map((row) => {
                if(row[column.value] != null && typeof row[column.value] != "object") {
                  if(row[column.value].indexOf('pdf') > -1) {
                    Object.assign(
                      row,
                      {
                        [column.value]:
                          <Link to={`${row[column.value]}`} target="_blank">
                            <img
                              src={require(`img/patients/pdf3.svg`)}
                              alt="pdf" style={{ width: '50px', height: '50px' }} />
                          </Link>
                      },
                    )
                  } else {
                    Object.assign(
                      row,
                      {
                        [column.value]:
                          <Gallery data={{ images: [`${row[column.value] || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}`] }} width="150" implantStyle={true} />
                      },
                    )
                  }
                }
              }) : null }
            break;
          case 'image':
            rows = rows.map((row) => Object.assign(
              row,
              {
                [column.value]:
                  <Gallery data={{ images: [`${row[column.value] || 'http://s3.amazonaws.com/uploads.novadonticsllc.com/img/placeholder.jpg'}`] }} width="150" implantStyle={true} />,
              },
            ));
            break;
          case 'booleanLabel':
            rows = rows.map((row) => Object.assign(
              row,
              { [column.value]: column.booleanLabelValue(row[column.value]) },
            ));
            break;
          case 'checkbox':
            rows = rows.map((row) => Object.assign(
              row,
              { [column.value]: column.booleanLabelValue(row[column.value]) },
            ));
            break;
          case 'marker':
            rows = rows.map((row) => Object.assign(
              row,
              { [column.value]: <Marker {...column.computed(row)} /> }
            ));
            break;
          case 'multicolor':
            rows = rows.map((row) => Object.assign(
              row,
              { [column.value]: row[column.value] ? <Col className={styles.color} style={{ backgroundColor: row[column.value] }}></Col> : row[column.value] }
            ));
            break;
          case 'date':
            {
              column.label == 'Dental License Expiration Date' ? rows.map((row) => {
                let expDate = row.licenseExpDate ? moment(row.licenseExpDate).format('YYYY-MM-DD') : null
                let newDate = moment().add(30, 'days').format('YYYY-MM-DD')
                if(expDate <= newDate) {
                  Object.assign(
                    row,
                    {
                      [column.value]:
                        <span style={{ color: 'red' }}>{row[column.value]}</span>
                    },
                  )
                } else {
                    Object.assign(
                      row,
                      {
                        [column.value]:
                          <span>{row[column.value]}</span>
                      },
                    )
                }
              }) : null }
            break;
          default:
            break;
        }
      });
    }

    return (<Col className="reactListModule">
              <Row>
                {name == 'operatories' ?
                  <div className={styles.feeStyle}>
                    <Sortable reloadOperatories={this.reloadOperatories.bind(this)} />
                  </div> : null}
              </Row>
      <PanelContainer>
        <PanelBody>
          <form id={this.props.name} onSubmit={handleSubmit}>
            <ReactTable
              columns={
                tableColumns && tableColumns.map((column) => Object.assign(
                  {},
                  { Header: column.sort ? column.sortLabel : column.label, accessor: column.value,sortable: column.sortable, width: column.width, ...column, ...this.editableColumnProps }
                ))}
              getTdProps={(state, rowProps) => {
                return {
                  mode: rowProps && editing === rowProps.original.id ? "edit" : "view",
                  inline: inline,
                  onEdit: () => onSelect(rowProps && rowProps.original.id, rowProps && rowProps.original),
                  onCancel: () => onCancel(),
                  onDelete: () => this.props.dispatch(this.props.actions.deleteRecord(rowProps && rowProps.original.id))
                }
              }}
              data={rows}
              showPageJump={false}
              showPageSizeOptions={false}
              pageSize={pageSize}
              pageText={""}
              PaginationComponent={ReactPagination}
              noDataText={loading ? "Fetching entries." : "No entries to show."}
              sortable={false}
            />
          </form>
        </PanelBody>
      </PanelContainer>
    </Col>);

  }

  reloadOperatories() {
    this.props.onReload();
  }
}

const mapStateToProps = (state, props) => ({
  initialValues: props.initialValues,
});

export default connect(
  mapStateToProps
)(reduxForm({
  form: `editRecord`,
  enableReinitialize: true,
})(ReactTableWrapper));

