/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { Map as iMap } from "immutable";
import { Col, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import Info from 'containers/PatientsPage/components/Info';
import { ImplementationFor, ContentTypes } from '../AddForm/utils';

const renderRadioField = (props) => {
  const { input, required, data = {}, fields } = props;
  const Input = iMap(input).toJS();

  if(Input.value == 'numeric') {
    fields.map((field) => {
      if(field.value == 'chartNumberSeq' && field.editRecord == undefined) {
        props.handleButtonChange(Input.value, Input.name)
      }
    })
  }

  const clearInput = () => {
    input.onChange('');
    const radioBoxes = document.getElementsByName(Input.value);
    for (var index = 0; index < radioBoxes.length; ++index) {
      radioBoxes[index].checked = false;
    }  
  }

  return <Col className={styles.fields}>
    {data.oneOf.map((option, index) =>
      <label className={styles.container} key={index} >
        <span className={styles.title}>{option.title}</span>
        <input name={Input.value} type="radio" value={option.value} defaultChecked={Input.value === option.value} onChange={(e) => input.onChange(option.value)} required={data.required} />
        <span className={styles.checkmark}></span>
      </label>)}
  </Col>
}


class MultiRadioBoxField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showAttributes: null, selectedOptions: false, isExpand: props.data.collapse ? true : false };
  }

  static propTypes = {
    data: PropTypes.object,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    path: PropTypes.string,
  };

  handleExpand() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  handleButtonChange(event, name) {
    let selectedValue = Object.values(event).join("").substring(0, 7);
    this.props.handleInputChange(selectedValue, name);
  }

  render() {
    const { data = {}, options = {}, optionsMode, infostyle, fields } = this.props;
    const { isExpand } = this.state;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={12} className={infostyle ? styles.infoField : stylesstyles.MultiRadioBoxField}>
        <label htmlFor={data.value} style={{ display: 'flex' }}>
          {data.collapse ?
            isExpand ?
              <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/plus.svg`)} /> :
                <img onClick={this.handleExpand.bind(this)} className={styles.plusIcon} src={require(`img/patients/minus.svg`)} /> : null}
          <span style={data.labelStyle ? {color: '#87b44d'} : null}>{data.label}</span>
          {data.info ? <Info data={data} /> : null}
        </label>
        {!isExpand ?
          <Field
            name={data.value}
            id={data.value}
            component={renderRadioField}
            data={data}
            fields={fields}
            {...options.general}
            {...extraFieldOptions}
            handleButtonChange={this.handleButtonChange.bind(this)}
            onChange={event => {
              if(this.props.data.onChange){
                this.handleButtonChange(event, data.value)
              }
            }}
          /> : null
        }
      </Col>
    );
  }



}

export default MultiRadioBoxField;
