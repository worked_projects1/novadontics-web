/**
*
* ProviderForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { reduxForm } from 'redux-form/immutable';
import { Col, Row, Icon, Modal, Button, Alert, Tabs, Tab, PanelContainer, PanelBody } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from 'components/CreateRecordForm/utils';
import { Link } from 'react-router';
import styles from './styles.css';

const ButtonItem = ({config, open}) => {
  const { title, glyphIcon, imageIcon, btnType, width, height, label } = config;
  switch(btnType){
    case 'link':
      return <Link className={styles.link} onClick={open}>
                <Icon className={styles.icon} glyph={glyphIcon ? glyphIcon : null} />{title || ''}
             </Link>
    case 'image':
      return <Col onClick={open}>
              {imageIcon && imageIcon.map((image,index)=><img key={index} width={width} height={height} src={image} alt={title || ''} className={styles.img} />)}
             </Col>
    default:
      return <Button bsStyle="standard" onClick={open} className={`action-button ${styles.button}`}>
              {label == 'SaaS' ? <span style={{ textTransform: 'capitalize' }}>ADD NEW SaaS</span> : <span>{title || ''}</span>}
             </Button>
  }
}

const Tooltip = ({text, show}) => show ? <span className={styles.tooltip}>{text}</span> : null;

class ProviderForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, start: false};
  }

  componentDidMount(){
    if(this.props.onRef)
      this.props.onRef(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { basicFields, licenseFields, otherFields, metaData, handleSubmit, error, updateError, pristine, submitting, config = {}, showModal, cancelModal } = this.props;
    const { centerAlign, title, bsSize, tooltip, viewType, shareDet, closeModal, modalClass, btnName, noButton, name } = config;
    const TabsMenu = [{name: "Basic Details"}, {name: "License Information"}, {name: "Other Information"}];

    return (
      <Col className={title == 'Extend Validity' ? null : styles.ModalForm}>
        {!noButton ? <ButtonItem config={config} open={this.open.bind(this)} /> : null }
        <Tooltip text={title} show={tooltip} />
        <Modal show={this.state.showModal || showModal} bsSize={bsSize} backdrop="static" onHide={showModal ? () => cancelModal(): this.close.bind(this)} className={"ProviderForm"} >
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={name == 'saas' ? {color: '#87b44d', fontWeight: 'bold'} : null}>{title}</span>
            </Modal.Title>
          </Modal.Header>
          <form onSubmit={handleSubmit}>
		        <Modal.Body id="ModalForm">
              <Tabs id="Subscriber">
                {(TabsMenu.map((prop, i) =>
                  <Tab key={i} eventKey={i} title={prop.name}>
                    <PanelContainer>
                      <PanelBody className={styles.tabBackground}>
                        <Col style={{ marginLeft: '25px', marginRight: '25px' }}>
                          {prop.name === "Basic Details" ?
                            <Row className={styles.rowStandard} id="RowModalForm">
                              {basicFields.map((field, i) => {
                                const Component = ImplementationFor[field.type];
                                const contentType = ContentTypes[field.type];
                                return (
                                (field.editRecord ?
                                  <div key={i} title={viewType == 'tab' ? field.label : ''} >
                                    {centerAlign ? <Col xs={3} /> : null }
                                    <Component
                                      data={field}
                                      contentType={contentType}
                                      options={field.formFieldDecorationOptions}
                                      optionsMode="edit"
                                      metaData={metaData}
                                      inline={field.inline}
                                      inputchange={(val) => this.props.handleChange(val)}
                                      shareDet={field.share && shareDet ? shareDet : null}
                                    />
                                    {centerAlign ? <Col xs={3} /> : null }
                                  </div>
                                : null)
                                );
                              })}
                            </Row> :
                          prop.name === "License Information" ?
                            <Row className={styles.rowStandard} id="RowModalForm">
                              {licenseFields.map((field, i) => {
                                const Component = ImplementationFor[field.type];
                                const contentType = ContentTypes[field.type];
                                return (
                                (field.editRecord ?
                                  <div key={i} title={viewType == 'tab' ? field.label : ''} >
                                    {centerAlign ? <Col xs={3} /> : null }
                                    <Component
                                      data={field}
                                      contentType={contentType}
                                      options={field.formFieldDecorationOptions}
                                      optionsMode="edit"
                                      metaData={metaData}
                                      inline={field.inline}
                                      inputchange={(val) => this.props.handleChange(val)}
                                      shareDet={field.share && shareDet ? shareDet : null}
                                    />
                                    {centerAlign ? <Col xs={3} /> : null }
                                  </div>
                                : null)
                                );
                              })}
                            </Row> :
                          prop.name === "Other Information" ?
                            <Row className={styles.rowStandard} id="RowModalForm">
                              {otherFields.map((field, i) => {
                                const Component = ImplementationFor[field.type];
                                const contentType = ContentTypes[field.type];
                                return (
                                (field.editRecord ?
                                  <div key={i} title={viewType == 'tab' ? field.label : ''} >
                                    {centerAlign ? <Col xs={3} /> : null }
                                    <Component
                                      data={field}
                                      contentType={contentType}
                                      options={field.formFieldDecorationOptions}
                                      optionsMode="edit"
                                      metaData={metaData}
                                      inline={field.inline}
                                      inputchange={(val) => this.props.handleChange(val)}
                                      shareDet={field.share && shareDet ? shareDet : null}
                                    />
                                    {centerAlign ? <Col xs={3} /> : null }
                                  </div>
                                : null)
                                );
                              })}
                            </Row> : null}
                        </Col>
                      </PanelBody>
                    </PanelContainer>
                  </Tab>
                ))}
              </Tabs>
            </Modal.Body>
            <Modal.Footer>
              <Button className={styles.closeBtn} onClick={showModal ? () => cancelModal() : this.close.bind(this)}>CLOSE</Button>
              <Button bsStyle='standard' type="submit" onClick={closeModal ? this.close.bind(this) : null} className={submitting ? `buttonLoading ${styles.submitBtn}` : styles.submitBtn}>{`${btnName ? btnName : 'submit'}`}</Button>
            </Modal.Footer>
          </form>
          {error || updateError ?
            <Alert danger>{error || updateError}</Alert> : null}
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    	this.setState({ showModal: true });
  }

}

ProviderForm.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  updateError: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  record: PropTypes.object,
  fields: PropTypes.array,
  config: PropTypes.object
};

export default reduxForm({
  form: 'modalRecord',
})(ProviderForm);
