/**
 * Filter
 */

import React from 'react';
import { connect } from 'react-redux';
import { Col, Button, Icon } from '@sketchpixy/rubix';
import api from 'utils/api';
import { compose, withProps } from 'recompose';
import { browserHistory } from 'react-router';
import { loadProviderFilter, updateProviderFilter} from '../../utils/sagas';
import styles from './styles.css';


const CheckboxField = ({ name, data, checked, onChange }) => <div className={styles.checkboxContainer}>
    <input name={name} id={data.id} type="checkbox" {...onChange}  defaultChecked={checked} value={data.id} />
    <label htmlFor={data.id}>
        <span></span>
    </label>
    <div className={styles.titleHead}>
        <span>{data.name}</span>
    </div>
</div>;

class Filter extends React.Component {
    constructor(props) {
        super(props)
        this.state = { providersFiltered: [] }
    }


    componentWillMount() {
        const { metaData = {} } = this.props;
        const { providerFilter = [] } = metaData;
        const { providersFiltered } = providerFilter[0] || [];
        this.setState({ providersFiltered: providersFiltered ? providersFiltered.split(',') : [] });
    }

    handleFilter(e) {
        e.preventDefault();
        const { dispatch, updateAction } = this.props;
        const data = { providers: this.state.providersFiltered.join(",") };
        dispatch(updateAction(data));
    }

    openFilter() {
        var el = document.getElementById('filterImg');
        if (el) {
            var filter = document.getElementById('filter');
            var bound = el.getBoundingClientRect();
            var left = bound.left - 306;
            var top = bound.bottom - 80;
            if (filter) {
                filter.style.position = 'absolute';
                filter.style.top = top + 'px';
                filter.style.left = left + 'px';
                filter.style.zIndex = '99999';
                filter.style.display = 'block';
            }
        }
    }

    closeFilter() {
        var filter = document.getElementById('filter');
        if (filter)
            filter.style.display = 'none';
    }

    checkAll() {
        const selectAll = document.getElementsByName('providerFilterAll')[0];
        var checkboxes = document.getElementsByName('providerFilter');
        for (var i = 0; i < checkboxes.length; i++) {
            const selFilter = this.state.providersFiltered && this.state.providersFiltered.length > 0 ? this.state.providersFiltered.filter(id => parseInt(id) === parseInt(checkboxes[i].value)) : []
            if (checkboxes[i].type == 'checkbox' && selectAll.checked === true) {
                checkboxes[i].checked = true;
                if (selFilter.length === 0) {
                    this.state.providersFiltered.push(checkboxes[i].value);
                }
            } else if (checkboxes[i].type == 'checkbox' && selectAll.checked === false) {
                checkboxes[i].checked = false;
                if (selFilter.length > 0) {
                    this.setState({ providersFiltered: [] })
                }
            }
        }

    }

    checkOne() {
        var checkboxes = document.getElementsByName('providerFilter');
        for (var i = 0; i < checkboxes.length; i++) {
            const selFilter = this.state.providersFiltered && this.state.providersFiltered.length > 0 ? this.state.providersFiltered.filter(id => parseInt(id) === parseInt(checkboxes[i].value)) : []
            if (checkboxes[i].type == 'checkbox' && checkboxes[i].checked === false && selFilter.length > 0) {
                this.setState({ providersFiltered: this.state.providersFiltered && this.state.providersFiltered.length > 0 ? this.state.providersFiltered.filter(id => parseInt(id) != parseInt(checkboxes[i].value)) : [] })
            } else if (checkboxes[i].type == 'checkbox' && checkboxes[i].checked === true && selFilter.length === 0) {
                if (checkboxes[i].value) {
                    this.state.providersFiltered.push(checkboxes[i].value);
                }
            }
        }
    }

    clear() {
        const selectAll = document.getElementsByName('providerFilterAll')[0];
        selectAll.checked = false;
        var checkboxes = document.getElementsByName('providerFilter');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = false;
                this.setState({ providersFiltered: [] })
            }
        }
    }


    render() {
        const { metaData = {} } = this.props;
        const { providers } = metaData;
        const { providersFiltered = [] } = this.state;
        return <Col>
            {this.props.children(this.openFilter.bind(this))}
            <Col id="filter" className={styles.filter}>
                <Col className={styles.header}>
                    <h4>provider list</h4>
                    <div className={styles.clear} onClick={this.clear.bind(this)}>Clear</div>
                </Col>
                <form onSubmit={this.handleFilter.bind(this)}>
                    <div className={styles.providerList}>
                        <Col className={styles.fontBold}><CheckboxField name={'providerFilterAll'} data={{ id: '0', name: 'Select All' }} onChange={{ onChange: this.checkAll.bind(this) }} checked={providersFiltered && providersFiltered.length > 0 && providers && providersFiltered.length === providers.length ? true : false} /><hr className={styles.hr} /></Col>
                        {providers && providers.length > 0 ?
                            providers.map((data, index) => <Col key={index}><CheckboxField name={'providerFilter'} key={index} data={data} onChange={{ onChange: this.checkOne.bind(this) }} checked={providersFiltered && providersFiltered.length > 0 && providersFiltered.filter(id => parseInt(id) === parseInt(data.id))[0] ? true : false} /><hr className={styles.hr} /></Col>) : null}
                    </div>
                    <div className={styles.filterBtn}>
                        <Button type="submit" bsStyle={"standard"}>Save</Button>
                        <Button onClick={this.closeFilter.bind(this)} bsStyle={"standard"}>Close</Button>
                    </div>
                </form>
            </Col>
        </Col>
    }
}

export default compose(
    connect(),
    withProps(({record, dispatch, error}) => ({
      loadAction: () => dispatch(loadProviderFilter()),
      updateAction: (record) => dispatch(updateProviderFilter(record)),
      record: record,
      error : error
    }))
  )(Filter);
