/**
*
* DailyBrief
*
*/

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { compose, withProps } from 'recompose';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Row, Modal } from '@sketchpixy/rubix';
import { columns } from '../../utils/library';
import styles from './styles.css';
import { loadDailyBrief } from '../../utils/sagas';
import Gallery from 'components/Gallery';
import moment from 'moment';

class DailyBrief extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        const { loadAction, metaData = {}, user, date } = this.props;
        const { dailyBrief = [] } = metaData;
        const pdfAuthToken = user && user.secret || user && user.token;
        
        return (
            <Col className={styles.DailyBrief}>
                {this.props.children(this.open.bind(this))}
                <Modal show={this.state.showModal} bsSize={'large'} className="dailyBrief" backdrop="static" onHide={this.close.bind(this)} onEnter={() => loadAction(date ? moment(date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'))}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <Col className={styles.header}>
                                <h4>Daily Brief for {date ? moment(date).format("DD MMM YYYY") : moment().format("DD MMM YYYY")}</h4>
                                {pdfAuthToken ?
                                    <a href={`${process.env.API_URL}/appointment/dailyBriefPDF?X-Auth-Token=${pdfAuthToken}&&date=${moment(date).format('YYYY-MM-DD')}`} target="_blank" data-rel="external">
                                        <img src={require('img/patients/pdf1.svg')} alt="pdf" className={styles.img} />
                                    </a>  : null}
                            </Col>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            {dailyBrief && dailyBrief.length > 0 ?
                                dailyBrief.map(data =>
                                    <Col className={styles.row}>
                                        <Col className={styles.details}>
                                            <table>
                                                {columns && columns['dailyBrief'] ? columns['dailyBrief'].map(col => 
                                                    <tr>
                                                        <td valign="top"><b>{col.label} <span>&nbsp;:</span> </b> </td>
                                                        <td valign="top">&nbsp;&nbsp;{Array.isArray(col.value) ? col.value.map(c=>data[c]).join('-') : data[col.value] ? data[col.value] : '-'}</td>
                                                    </tr>
                                                ) : null}
                                            </table>
                                        </Col>
                                        {data.patientPhoto ? <Col><Gallery data={{ images : [data.patientPhoto] }} width="120" /></Col> : null}
                                    </Col>) : 
                                <Col className={styles.noBrief}>No Daily Brief to show.</Col>}
                        </Row>
                    </Modal.Body>
                </Modal>
            </Col>
        );
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

}

export default compose(
    connect(),
    withProps(({ dispatch }) => ({
        loadAction: (date) => dispatch(loadDailyBrief(date))
    }))
)(DailyBrief);