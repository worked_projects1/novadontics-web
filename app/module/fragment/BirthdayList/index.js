import React from "react";
import shallowCompare from "react-addons-shallow-compare";
import { Col,Modal } from "@sketchpixy/rubix";
import styles from "./styles.css";
import moment from "moment-timezone";
import { connect } from "react-redux";
import { compose, withProps } from "recompose";
import { loadBirthdayList } from "../../utils/sagas";
import ReactTable from "react-table";

class BirthdayList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }
  componentDidMount() {
    const timezone = moment.tz.guess();
  }
  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { loadAction, metaData = {}, date, columns } = this.props;
    const { birthday } = metaData;
    const tableColumns = columns.filter((column) => column.visible);
    let rows = birthday;

    tableColumns.forEach((column) => {
      switch (column.label) {
        case "Email":
          column.label == "Email" && column.link == true && rows
            ? (rows = rows.map((row) =>
                Object.assign(row, {
                  [column.value]: (
                    <a href={`mailto:${row.email}`}>{row.email}</a>
                  ),
                })
              ))
            : (rows = rows);
          break;
        default:
          break;
      }
    });

    return (
      <Col>
        {this.props.children(this.open.bind(this))}
        <Modal
          show={this.state.showModal}
          bsSize={"large"}
          className="birthdayList"
          id="birthdayListModal"
          backdrop="static"
          onHide={this.close.bind(this)}
          onEnter={() =>
            loadAction(
              date
                ? moment(date).format("YYYY-MM-DD")
                : moment().format("YYYY-MM-DD")
            )
          }
        >
          <Modal.Header closeButton>
            <Modal.Title>
              <Col className={styles.header}>
                <span>Upcoming Birthdays</span>
              </Col>
              <span style={{ fontSize: '14px', color: '#252525' }}>Note: Patients with a birthday coming up within the next 7 days will appear here.</span>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className={styles.modalBody}>
            <Col className="BirthdayList">
              <ReactTable
                columns={tableColumns.map((column) =>
                  Object.assign(
                    {},
                    {
                      Header: column.label,
                      accessor: column.value,
                      width: column.width,
                      ...column,
                    }
                  )
                )}
                data={birthday ? birthday : []}
                pageSize={birthday && birthday.length > 0 ? 10 : 0}
                showPageJump={false}
                resizable={false}
                showPageSizeOptions={false}
                previousText={"Back"}
                pageText={""}
                noDataText={"No Upcoming Birthdays to show."}
              />
            </Col>
          </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }
}

export default compose(
  connect(),
  withProps(({ dispatch }) => ({
    loadAction: () => dispatch(loadBirthdayList(moment.tz.guess().toString())),
  }))
)(BirthdayList);
