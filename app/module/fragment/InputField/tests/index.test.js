import InputField from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

const data = [
  {
    id: 1,
    label: 'test1',
    value: 'test1',
  },
  {
    id: 2,
    label: 'test2',
    value: 'test2',
  },
];

describe('<InputField />', () => {
  it('should have props equal to the provided props', () => {
    const wrapper = mount(
      <InputField
        data={data}
      />
    );
    expect(wrapper.props().data).toEqual(data);
  });
});
