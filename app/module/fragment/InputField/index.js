/**
*
* InputField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class InputField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
    inline: PropTypes.bool,
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create']),
  };
  render() {
    const { data, inline,infostyle, options = {}, optionsMode, record } = this.props;
    const { required } = options.general || {};
    const columnSize = inline ? 12 : infostyle ? 12 : 6;
    const type = data.number ? 'number' : 'text';
    const normalize = data.max ? (value) => value.length > data.max ? value.slice(0,parseInt(data.max)) : value : data.tax ? (value) => parseFloat(value) > 100 ? 100 : value : data.percentage ? (value) => parseFloat(value) > 100 ? 100 : parseFloat(value) : data.number ? (value) => parseFloat(value) : data.dollar ? (value) => {  return isNaN(parseFloat(value.replace(/\$/g,''))) ? '$' : '$' + parseFloat(value.replace(/\$/g,''))} : null;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Col xs={columnSize} className={infostyle ? styles.infoField : styles.InputField}>
        <label htmlFor={data.label}>
          {data.label}
          {required || extraFieldOptions.required ? <span style={{color:"#87b44d",fontSize:'21px'}}>*</span> : null}
        </label>
        <Field
          name={data.value}
          id={data.label}
          className={data.disabled ? styles.disabled : null}
          disabled={data.disabled ? true : false}
          component="input"
          type={type}
          maxLength={data.maxLength}
          normalize={normalize}
          {...options.general}
          {...extraFieldOptions}
        />
      </Col>
    );
  }
}

export default InputField;
