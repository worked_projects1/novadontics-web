/**
*
* <DatePicker />
* Wrapper for redux-form’s <Field /> and delegates work to ModernDatePicker component.
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import ModernDatePicker from 'components/ModernDatePicker';
import { Field } from 'redux-form/immutable';
import { Col } from '@sketchpixy/rubix';
import styles from './styles.css';

class DatePicker extends React.Component {

  static propTypes = {
    data: PropTypes.object,
  };

  render() {
    const { data, inline,infostyle, optionsMode, options = {}, record } = this.props;
    const { required } = options.general || {};
    return (
      <Col xs={inline ? 12 : infostyle ? 12 : 6} className={infostyle ? styles.infoField : styles.InputField}>
        <label htmlFor={data.label}>
          {data.label}
          {required ? <span>*</span> : null}
        </label>
        <Field name={data.value} id={data.label} data={data} infostyle={infostyle}  component={ModernDatePicker} />
      </Col>
    );
  }

}

export default DatePicker;
