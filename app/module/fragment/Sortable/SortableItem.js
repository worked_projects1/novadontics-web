import React from 'react';
import { sortable, HORIZONTAL } from 'react-sortable';
import styles from './styles.css';

class Item extends React.Component {
  render() {
    return (
      <li {...this.props} className={styles.listStyle}>
        <span>{this.props.children.name}</span>
      </li>
    )
  }
}

export default sortable(Item, HORIZONTAL)
