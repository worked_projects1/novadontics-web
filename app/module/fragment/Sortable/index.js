/**
*
* SAAS Payment
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import styles from './styles.css';
import { loadOperatories, reorderOperatories } from '../../utils/sagas.js';
import moment from 'moment-timezone';
import { columns } from '../../utils/library';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';
import ReactTable from "react-table";
import { currencyFormatter } from 'utils/currencyFormatter.js';
import filterRows from 'utils/filter';
import Spinner from 'components/Spinner';
import SortableGrid from '../../fragment/Sortable/SortableGrid';

class Sortable extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      loading: false,
      successAlert: false,
      failureAlert: false,
      records: []
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  render() {
    const { row, singleImage, style, user, dispatch, reloadOperatories } = this.props;
    const { photoIndex, isOpen, records, loading } = this.state;

    return (
      <Col xs={12} className={styles.actions}>
        <Button bsStyle="standard" className={styles.actionButton} onClick={() => this.handleOpen()}>
          Rearrange Operatories
        </Button>
        <Modal show={isOpen} className="operatoryModule" backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={{color: '#ea6225', fontWeight: 'bold'}}>Rearrange Operatories</span>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className={styles.centerAlign}>
            <Col className={styles.listWidth}>
              {!loading ?
                <div>
                  <SortableGrid items={records} handleSort={this.handleSort.bind(this)} />
                </div> : <Spinner />}
            </Col>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.handleClose.bind(this)}>CLOSE</Button>
            <Button bsStyle={'standard'} type="submit" onClick={this.handleSave.bind(this)}>Submit</Button>
          </Modal.Footer>
        </Modal>
        {this.state.successAlert ?
          <AlertMessage success dismiss={() => this.setState({ successAlert: false, isOpen: false })}>Operatories rearranged successfully.</AlertMessage> : null}
        {this.state.failureAlert ?
          <AlertMessage warning dismiss={() => this.setState({ failureAlert: false })}>There was an error while saving the operatories. Please try again.</AlertMessage> : null}
      </Col>
    );
  }

  handleOpen() {
    this.setState({ isOpen: true, loading: true });
    loadOperatories()
      .then(recordList => {
        this.setState({ records: recordList, loading: false })
      })
      .catch(() => this.setState({ records: [], loading: false }));
  }

  handleSort(recordList) {
    this.setState({ records: recordList })
  }

  handleSave() {
    this.setState({ loading: true })
    let submitRecord = {}
    submitRecord.operatorId = this.state.records.map(item => item.id);
    reorderOperatories(submitRecord)
      .then(recordList => {
        this.props.reloadOperatories();
        this.setState({ loading: false, successAlert: true });
      })
      .catch(() => this.setState({ loading: false, failureAlert: true }));
  }

  handleClose() {
    this.setState({ isOpen: false });
  }
}

export default Sortable;
