/**
 * Cell
 */

import moment from 'moment';

export const Cell = (props) => {
    const { patientId, patientName, operatoryId, operatoryName, providerId, providerName, medicalCondition, startTime, endTime, status, date, showName, color, reason, metaData = {}, alerts, title } = props;
    const { providers = [], operatories = [], patientList = [] } = metaData;
    const [patientsFilter] = patientList && patientList.length > 0 ? patientList.filter(_ => _.id === parseInt(patientId)) : [];
    const [providersFilter] = providers && providers.length > 0 ? providers.filter(_ => _.id === parseInt(providerId)) : [];
    const [opratoriesFilter] = operatories && operatories.length > 0 ? operatories.filter(_ => _.id === parseInt(operatoryId)) : [];
    const eventStart = new Date(moment(date).format('YYYY/MM/DD ') + " " + moment(startTime, ["h:mm A"]).format("HH:mm"));
    const eventEnd = new Date(moment(date).format('YYYY/MM/DD ') + " " + moment(endTime, ["h:mm A"]).format("HH:mm"));
    const timeDiff = moment.utc(eventEnd).diff(eventStart, 'minutes');
    var PhoneNumber = patientsFilter && patientsFilter.phoneNumber ? patientsFilter.phoneNumber : '';
    var link = patientsFilter && `/patients/${patientsFilter.id}/form`;
    var formatedNum = PhoneNumber.replace(/[()\-.]/gi, '');
    var Phone = formatedNum.substr(0, 3) + '.' + formatedNum.substr(3, 3) + '.' + formatedNum.substr(6,4);


    return `<div class='moduleCell' style='background-color:${providersFilter && providersFilter.color ? providersFilter.color : color ? color : ''}'>
        <div class='moduleCellDesc moduleCellCalendar'>
            <div>    
                <b class='moduleCellTitle'>
                    <span>
                        ${patientName && showName ? patientName : patientsFilter && patientsFilter.name && showName ? patientsFilter.name : ''}
                    </span>
                    <span>
                    ${patientId != 0 ? `<a href=${link} onClick={event.stopPropagation()}>
                    <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDEzLjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMTQ5NDgpICAtLT4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB3aWR0aD0iMjAwcHgiIGhlaWdodD0iMjAwcHgiIHZpZXdCb3g9IjAgMCAyMDAgMjAwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyMDAgMjAwIiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHRpdGxlPmljb24gcGF0aWVudHM8L3RpdGxlPgo8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KPGc+Cgk8ZyBpZD0iUGF0aWVudHMiPgoJCTxwYXRoIGlkPSJDb21iaW5lZC1TaGFwZSIgZmlsbD0iIzQwNDA0MSIgZD0iTTEzNC4zNTcsMTUzLjQ2OWwtMTAuNTk2LTI5LjU3MmMwLDAsMjguODk2LTI3LjUyMSwxMy42Ni01Ny42OQoJCQljLTE0LjEwNy0yNy45MzYtNTIuODM1LTIxLjY3NC02My4wMzMtNy43MjFjLTIuMDI2LDIuNzczLTQuNTYxLDYuODQyLTUuMjMsOS45MjdjLTAuNjcxLDMuMDg1LDAsMTIuMjA4LTEuMjA3LDE0Ljc1NwoJCQlTNTguMTU4LDk5Ljk0LDU4LjE1OCw5OS45NHMtMC42NzIsMS4zNDEsMCwxLjg3OGMwLjY3MSwwLjUzNSw2LjQzOSwwLjQwMiw2LjQzOSwwLjQwMnMwLjY0OSwwLjg2MywwLjY0OSwyLjc0MgoJCQljMCwxLjg3OC0wLjUxNywxOC44NTUtMC41MTcsMTguODU1cy0wLjEzMywwLjQwMiwwLjUzNiwwLjkzOGMwLjY3MSwwLjUzNSwxNi4xMDEsMS4wNzQsMTYuMTAxLDEuMDc0czEuMDczLTAuNTM5LDEuNjA4LDEuNDc2CgkJCWMwLjUzNywyLjAxNCwzLjg5NCwyNi4xNiwzLjg5NCwyNi4xNkwxMzQuMzU3LDE1My40NjlMMTM0LjM1NywxNTMuNDY5eiIvPgoJCTxwYXRoIGlkPSJGaWxsLTYiIG9wYWNpdHk9IjAuMDMiIGZpbGw9IiM0MDQwNDEiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgICAgIiBkPSJNOTAuMzExLDQ3LjUyMQoJCQljLTMuNjQ1LDAuNjMyLTMuNTcsMC42NjctNi44ODcsMy4yODVjLTIuMDg4LDEuNjQ3LTYuNDk0LDIuNTc2LTQuNzk1LDE0LjUxYzMuNzM4LDI2LjIxMywyOC44NjUsMTcuNTU4LDM0LjkxNywyNi45ODgKCQkJYzcuNDkyLDExLjY3NC0yLjk3NSwyNC4wNDksMi42OCwzNy41OTRjNi44OTYsMTYuNTIyLDE2LjgwMywyMS4wODgsMTYuODAzLDIxLjA4OGwtOS42NDMtMjkuOTgxbDEzLjM0OC0xOS42ODdsMy45NDktMTcuMzU3CgkJCWwtOC40MzgtMjQuNTQ1bC0yMS4yNS0xMi4yNDlMOTAuMzExLDQ3LjUyMXoiLz4KCQk8cGF0aCBpZD0iRmlsbC0xMiIgZmlsbD0iIzQwNDA0MSIgZD0iTTg0Ljk4OSwxMjYuNzVjLTAuMjE5LTEuNDU5LDAuODI4LTIuNzk5LDIuMjk3LTIuOTU3YzQuOTg2LTAuNTM5LDE1LjYxNS0yLjAzMSwyMC44NDMtNS4zNjEKCQkJYzAuMDc4LTAuMDUxLDAuMTU0LTAuMSwwLjIzLTAuMTQ3YzIuMzUyLTEuNDk0LDQuNiwwLjgxMywxLjU0NywyLjg1bC02LjAzNywzLjExN2MtNy44MDIsNS42OTYtMTAuMTc5LDE3Ljk5Mi0xMC42NiwyNC4zODgKCQkJYy0wLjAyNiwwLjM5My0wLjE1MSwwLjYxOC0wLjI5OSwwLjYxOGgtMi4zNDRjLTEuMjg5LDAtMi4zODMtMC45NDItMi41Ny0yLjIxN0w4NC45ODksMTI2Ljc1eiIvPgoJCTxnIGlkPSJHcm91cC0xNiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNzQuOTg2ODQyLCAzLjg2MDMxMSkiPgoJCQk8cGF0aCBpZD0iRmlsbC0xNCIgZmlsbD0iIzQwNDA0MSIgZD0iTS0xNC41NjgsOTYuNTk1Yy0wLjAwMiwwLjAwNC0wLjAwNCwwLjAwNy0wLjAwNCwwLjAwOQoJCQkJQy0xNC41NzIsOTYuNjAxLTE0LjU2Niw5Ni41OTctMTQuNTY4LDk2LjU5NUwtMTQuNTY4LDk2LjU5NXogTTcuMzMyLDEyMi43NTNjLTAuMDAyLDAuMDAyLTAuMDAyLDAuMDAyLTAuMDA2LDAuMDAyCgkJCQlDNy4zMywxMjIuNzU1LDcuMzMsMTIyLjc1NSw3LjMzMiwxMjIuNzUzTDcuMzMyLDEyMi43NTN6IE0xMy42LDE0NS44OThoNDEuNzk0TDQ1Ljk5OSwxMTkuNwoJCQkJYy0wLjI3NS0wLjc3MS0wLjA3LTEuNjM3LDAuNTIxLTIuMjAxYzAuMjcxLTAuMjU4LDI3LjAyMS0yNi4yNzIsMTIuOTc0LTU0LjA4MmMtNy4xNy0xNC4xODgtMjAuMzk2LTE3LjI5OS0yNy42NC0xNy44OTQKCQkJCUMxOS41NjYsNDQuNTIxLDYuNDAxLDQ5LjA4NCwxLjIzOCw1Ni4xNDJjLTIuNTIyLDMuNDUtNC4zMDgsNi43OTQtNC43NzQsOC45NDJjLTAuMjMzLDEuMDc0LTAuMjc3LDMuMzk2LTAuMzIsNS42MzkKCQkJCWMtMC4wODMsNC40ODEtMC4yLDcuNTQ3LTEuMDE3LDkuMjY4Yy0xLjA0MywyLjE5Ny03LjE4NywxMi40NDEtOS4xMjYsMTUuNjYxYzEuMTE2LDAuMDU0LDIuNjIzLDAuMDY3LDMuOTEzLDAuMDM3CgkJCQljMC43MjktMC4wMiwxLjMwOCwwLjI5MywxLjcwOCwwLjgyN2MwLjE3NiwwLjIzNCwxLjA1NiwxLjUyMywxLjA1NiwzLjkzNmMwLDEuNjEzLTAuMzcyLDE0LjAwMi0wLjQ4LDE3LjU5MgoJCQkJYzIuMzM4LDAuMjU2LDguNDE1LDAuNTcsMTMuOTYsMC43NjhjMC40NTUtMC4wOSwxLjA3Ny0wLjEwMiwxLjc2LDAuMjA1YzAuOTk4LDAuNDQ3LDEuNjk1LDEuMzkxLDIuMDY3LDIuNzkxCgkJCQlDMTAuNDYxLDEyMy41ODMsMTIuNzg3LDE0MC4wODEsMTMuNiwxNDUuODk4TDEzLjYsMTQ1Ljg5OHogTTU4LjM0NywxNTAuMDVIMTEuNzkxYy0xLjAzNSwwLTEuOTE2LTAuNzY0LTIuMDU5LTEuNzkyCgkJCQljLTEuMjQyLTguOTQtMy4zMjgtMjMuNTE1LTMuNzQzLTI1LjI5NmMtMTUuNDg0LTAuNTQ3LTE2LjA2LTEuMDA4LTE2LjY2OS0xLjQ5NWMtMS4xMDEtMC44OC0xLjM5Ni0xLjkyMS0xLjMwMS0yLjczNwoJCQkJYzAuMDQ4LTEuNTg2LDAuNTAyLTE2LjU3NiwwLjUwMi0xOC4yNzZjMC0wLjIyMi0wLjAxMy0wLjQxOC0wLjAzMi0wLjU4OWMtNS0wLjAyLTUuNjg0LTAuNTEtNi4xMzgtMC44NwoJCQkJYy0xLjQ1NS0xLjE2Mi0xLjIxMS0zLjA5LTAuNTYzLTQuMzkxYzIuNDA2LTMuOTk1LDguNjQ3LTE0LjQyLDkuNTgzLTE2LjM5MWMwLjQ5My0xLjA0MiwwLjU2Ni00Ljk2OSwwLjYxNi03LjU2NgoJCQkJYzAuMDUtMi42NjksMC4wOTMtNC45NzMsMC40MTUtNi40NDRjMC44NC0zLjg3MSw0LjExNC04LjY0LDUuNDgxLTEwLjUxYzYuMDQyLTguMjYzLDIwLjQ3OC0xMy40NDUsMzQuMzEyLTEyLjMwNgoJCQkJYzE0LjAwNiwxLjE0NiwyNS4wMTYsOC4zMDYsMzEuMDA2LDIwLjE2YzYuNDE2LDEyLjY5Niw2LjAzOSwyNi45NDUtMS4wOSw0MS4yMDhjLTQuMjI5LDguNDY0LTkuNTksMTQuNTE1LTExLjc1NiwxNi43NzgKCQkJCWw5Ljk0NywyNy43NGMwLjIyOSwwLjYzNywwLjEzMywxLjM0NC0wLjI1OSwxLjg5NkM1OS42NTYsMTQ5LjcyLDU5LjAyMSwxNTAuMDUsNTguMzQ3LDE1MC4wNUw1OC4zNDcsMTUwLjA1eiIvPgoJCTwvZz4KCTwvZz4KPC9nPgo8L3N2Zz4K" class='moduleCellStatus1'/>
                     </a>` : ''}

                    ${['Completed', 'Confirmed', 'Checked In', 'Scheduled', 'Seated'].includes(status) ?
                        `<img src='${require(`img/module/${status === 'Completed' ? 'green' : status === 'Confirmed' ? 'blue' : status === 'Checked In' ? 'red' : status === 'Seated' ? 'purple' :'white'}.svg`)}' class='moduleCellStatus' />` : ''}
                    ${alerts || (medicalCondition || (patientsFilter && patientsFilter.medicalCondition)) ?
                        `<img src='${require('img/module/medical.svg')}' class='moduleCellMedical' />` : ''}
                    </span>    
                </b>
            </div>
            ${timeDiff > 30 ? `<div style='text-transform: capitalize'><span style='font-weight: 800'>` + (providersFilter && providersFilter.title ? providersFilter.title : title ? title : '') + ' </span>' +
            (providersFilter && providersFilter.firstName && providersFilter.lastName ? providersFilter.firstName + ' ' + providersFilter.lastName : providerName ? providerName : '') + '</div>' : ''}
            ${reason ? '<div><b>Reason</b>: ' + reason + '</div>' : '<div><b>Reason</b>: Continue Treatment </div>'}
            ${timeDiff > 30 ? `<div>${'<b>Operatory</b>: ' + (opratoriesFilter && opratoriesFilter.name ? opratoriesFilter.name : operatoryName ? operatoryName : '') + '</div>'}` : ''}
            ${patientsFilter && showName ? '<div id="phone"><b>Phone #</b>: ' + Phone + '</div>' : ''}
            ${patientsFilter && showName ? '<div><b>Email</b>: ' + (patientsFilter && patientsFilter.email ? patientsFilter.email : '')  + '</div>' : ''}
        
            </div>
        <div class='moduleCellDesc moduleCellBookedList'>
            <div>
                <b class='moduleCellTitle'>
                    <span>
                        ${patientName && showName ? patientName : patientsFilter && patientsFilter.name && showName ? patientsFilter.name : ''}
                    </span>
                    <span>
                    ${['Completed', 'Confirmed', 'Checked In'].includes(status) ?
                        `<img src='${require(`img/module/${status === 'Completed' ? 'gray' : status === 'Confirmed' ? 'orange' : 'green'}.svg`)}' class='moduleCellStatus' />` : ''}
                    ${alerts || (medicalCondition || (patientsFilter && patientsFilter.medicalCondition)) ?
                        `<img src='${require('img/module/medical.svg')}' class='moduleCellMedical' />` : ''}
                    </span>   
                </b>
            </div>
            <div>${startTime}-${endTime}</div>
        </div>
    </div>`
}




