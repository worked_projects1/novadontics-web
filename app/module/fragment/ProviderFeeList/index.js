import React from "react";
import shallowCompare from "react-addons-shallow-compare";
import { Col,Modal } from "@sketchpixy/rubix";
import styles from "./styles.css";
import moment from "moment-timezone";
import { connect } from "react-redux";
import { compose, withProps } from "recompose";
import { loadProviderFee } from "../../utils/sagas";
import ReactTable from "react-table";
import { currencyFormatter } from 'utils/currencyFormatter.js';

class ProviderFeeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }
  componentDidMount() {
    const timezone = moment.tz.guess();
  }
  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  editableColumnProps = {
    Cell: props => {
      const { column } = props;
      return (column.label == "Amount" && column.currency === true ? <span>{currencyFormatter.format(props.value)}</span> : <span>{props.value}</span>);
    }
  };

  render() {
    const { loadAction, metaData = {}, date, columns } = this.props;
    const { providerFeeList } = metaData;
    const tableColumns = columns.filter((column) => column.visible);
    let rows = providerFeeList;

    return (
      <Col>
        {this.props.children(this.open.bind(this))}
        <Modal
          show={this.state.showModal}
          bsSize={"large"}
          className="providerFeeList"
          id="providerFeeModal"
          backdrop="static"
          onHide={this.close.bind(this)}
          onEnter={() =>
            loadAction(
              date
                ? moment(date).format("YYYY-MM-DD")
                : moment().format("YYYY-MM-DD")
            )
          }
        >
          <Modal.Header closeButton>
            <Modal.Title>
              <Col className={styles.header}>
                <span>Provider Fee</span>
              </Col>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className={styles.modalBody}>
            <Col className="BirthdayList">
              <ReactTable
                columns={tableColumns.map((column) =>
                  Object.assign(
                    {},
                    {
                      Header: column.label,
                      accessor: column.value,
                      width: column.width,
                      ...column,
                      ...this.editableColumnProps,
                    }
                  )
                )}
                data={providerFeeList ? providerFeeList : []}
                pageSize={providerFeeList && providerFeeList.length > 0 ? 10 : 0}
                showPageJump={false}
                resizable={false}
                showPageSizeOptions={false}
                previousText={"Back"}
                pageText={""}
                noDataText={"No Providers to show."}
              />
            </Col>
          </Modal.Body>
        </Modal>
      </Col>
    );
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }
}

export default compose(
  connect(),
  withProps(({ dispatch }) => ({
    loadAction: (date) => dispatch(loadProviderFee(date)),
  }))
)(ProviderFeeList);
