/**
*
* EditForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import styles from './styles.css';
import { Row, Col, Button, Alert } from '@sketchpixy/rubix';
import {ImplementationFor, ContentTypes} from '../AddForm/utils';

const EditForm = (props) => {
  const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, eventKey, changeKey, handleChange } = props;
  return (
    <form onSubmit={handleSubmit}>
        {fields.map((field, i) => {
          const Component = ImplementationFor[field.type];
          const contentType = ContentTypes[field.type];

          return (
            ((field.editRecord || field.createRecord) ?
              <div key={i}>
                <Component
                  data={field}
                  contentType={contentType}
                  options={field.formFieldDecorationOptions}
                  optionsMode="edit"
                  metaData={metaData}
                />
              </div>
            : null)
          );
        })}
      <Row className="action-button">
        <Col xs={12} className={styles.actionButtons}>
          <Button bsStyle="standard" type="submit" className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Save</Button>
        </Col>
      </Row>
    </form>
  );
};

EditForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  path: PropTypes.string,
  error: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
};

export default reduxForm({
  form: 'editRecord',
  enableReinitialize: true
})(EditForm);
