/**
 * 
 * FormButton
 * 
 */

import React from 'react';
import { Col, Button } from '@sketchpixy/rubix';
import ConfirmButton from 'components/ConfirmButton';
import SaasPayment from 'module/fragment/PaymentForm';
import styles from './styles.css';

class FormButton extends React.Component {

  render() {
    const { mode, onEdit, onCancel, onDelete, field, inline, archive, rowId, dispatch } = this.props;
    return <Col>
      {mode === 'edit' && inline ?
        <Button type="submit" bsSize="xs" bsStyle="link"><img width="35" height="35" src={require('img/module/save.svg')} /></Button> :
        <Col className={styles.action}>
          {field.editAction ? <Button bsSize="xs" bsStyle="link" onClick={onEdit}><img width="35" height="35" src={require('img/module/edit.svg')} /></Button> : null}
          <ConfirmButton onConfirm={onDelete}>{(click) => <Button bsSize="xs" bsStyle="link" onClick={() => archive ? onDelete() : click()}><img width="35" height="35" src={require(`img/module/${archive ? 'archive' : 'delete'}.svg`)} /></Button>}</ConfirmButton>
          {field.paymentOption ? <SaasPayment form={`saasForm.${rowId}`} saasId={rowId} dispatch={dispatch} /> : null}
        </Col>}
      {mode === 'edit' && inline ?
        <Button bsSize="xs" bsStyle="link" onClick={onCancel}><img width="35" height="35" src={require('img/module/cancel.svg')} /></Button> : null}
    </Col>;
  }
}

export default FormButton;