/**
*
* CreateForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import styles from './styles.css';
import { Row, Col, Button, Alert } from '@sketchpixy/rubix';
import { ImplementationFor, ContentTypes } from './utils';

const AddForm = (props) => {
  const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, eventKey, changeKey, inline } = props;
  return (
    <Col>
      <form onSubmit={handleSubmit} className={styles.form}>
        {fields.map((field, i) => {
          const Component = ImplementationFor[field.type];
          const contentType = ContentTypes[field.type];

          return (
            ((field.editRecord || field.createRecord) ?
              <div key={i}>
                <Component
                  data={field}
                  contentType={contentType}
                  options={field.formFieldDecorationOptions}
                  optionsMode="create"
                  metaData={metaData}
                  inline={inline}
                />
              </div>
              : null)
          );
        })}
        <Button bsStyle="standard" type="submit" className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Add</Button>
      </form>
    </Col>
  );
};

AddForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  fields: PropTypes.array,
  path: PropTypes.string,
  error: PropTypes.string,
  metaData: PropTypes.object,
  locationState: PropTypes.object,
};

export default reduxForm({
  form: 'createRecord',
  enableReinitialize: true
})(AddForm);
