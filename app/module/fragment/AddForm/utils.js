import Textarea from 'components/Textarea';
import InputField from '../InputField';
import SelectField from 'components/SelectField';
import MultiSelectField from 'components/MultiSelectField';
import Upload from 'components/Upload';
import DatePicker from '../Datepicker';
import CheckboxField from "../Checkbox";
import SignatureField from "components/SignatureField";
import MultiCheckboxField from "components/MultiCheckboxField";
import RadioboxField from '../RadioboxField';

export const ImplementationFor = {
  textarea: Textarea,
  input: InputField,
  checkbox: CheckboxField,
  select: SelectField,
  Radiobox : RadioboxField,
  image: Upload,
  thumbnail: Upload,
  document: Upload,
  video: Upload,
  zip: Upload,
  contracts: Upload,
  agreement: Upload,
  multiselect: MultiSelectField,
  date: DatePicker,
  signature: SignatureField,
  multicheckbox: MultiCheckboxField
};
  
export const ContentTypes = {
  image: 'image/*',
  thumbnail: 'image/*',
  video: 'video/*,application/pdf',
  document: 'application/pdf',
  zip: 'application/*',
  contracts: 'application/pdf,image/*'
};