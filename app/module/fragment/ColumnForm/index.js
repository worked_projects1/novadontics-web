/**
*
* ColumnForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, change } from 'redux-form/immutable';
import styles from './styles.css';
import { Row, Col, Button, Alert } from '@sketchpixy/rubix';
import { ImplementationFor, ContentTypes } from '../AddForm/utils';

const ColumnForm = (props) => {
    const { handleSubmit, pristine, submitting, fields, infostyle, path, error, metaData, locationState, eventKey, changeKey, handleChange, dispatch, form } = props;

    const handleInput = (value, name) => {
      if(value === 'numeric') {
        fields.map((field) => {
          if(field.value == 'chartNumberSeq') {
            field.editRecord = true
          }
        })
      } else {
        dispatch(change(form, "chartNumberSeq", null));
        fields.map((field) => {
          if(field.value == 'chartNumberSeq') {
            field.editRecord = false
          }
        })
      }
    };

    return (
        <form className={styles.form} onSubmit={handleSubmit}>
            <div className={styles.mainform}>
                <div className={styles.subform}>
                <Row>
                 <Col className={styles.infolabel}><h4 style={{fontWeight:"bold",fontSize:"16px"}}>OFFICE INFORMATION</h4></Col>  
               </Row>
                    {fields.slice(0,16).map((field, i) => {
                        const Component = ImplementationFor[field.type];
                        const contentType = ContentTypes[field.type];

                        return (
                            ((field.editRecord || field.createRecord) ?
                                <div key={i}>
                                    <Component
                                        data={field}
                                        contentType={contentType}
                                        options={field.formFieldDecorationOptions}
                                        optionsMode="edit"
                                        metaData={metaData}
                                        infostyle={infostyle}
                                    />
                                </div>
                                : null)
                        );
                    })}
                </div>
                <div className={styles.subform}>
                <Row>
                <Col className={styles.infolabel}><h4 style={{fontWeight:"bold",fontSize:"16px"}}>BILLING INFORMATION</h4></Col>  
               </Row>
                    {fields.slice(16,26).map((field, i) => {
                        const Component = ImplementationFor[field.type];
                        const contentType = ContentTypes[field.type];

                        return (
                            ((field.editRecord || field.createRecord) ?
                                <div key={i}>
                                    <Component
                                        data={field}
                                        contentType={contentType}
                                        options={field.formFieldDecorationOptions}
                                        optionsMode="edit"
                                        metaData={metaData}
                                        fields={fields}
                                        initialValues={props.initialValues}
                                        handleInputChange={handleInput.bind(this)}
                                        infostyle={infostyle}
                                    />
                                </div>
                                : null)
                        );
                    })}

                </div>
            </div>

            <Row>
                 <Col className={styles.infodescription}><h4 style={{fontSize:"14px"}}>Please contact Novadontics admin to make any desired edits to the fields with the asterisks</h4></Col>  
            </Row>

            <Row className="action-button">
                <Col xs={12} className={styles.actionButtons}>
                    <Button bsStyle="standard" type="submit" className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton}>Save</Button>
                </Col>
            </Row>
        </form>
    );
};

ColumnForm.propTypes = {
    handleSubmit: PropTypes.func,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
    path: PropTypes.string,
    error: PropTypes.string,
    metaData: PropTypes.object,
    locationState: PropTypes.object,
};

export default reduxForm({
    form: 'editRecord',
    enableReinitialize: true
})(ColumnForm);
