/**
*
* SettingsForm
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, change } from 'redux-form/immutable';
import styles from './styles.css';
import { Row, Col, Button, Alert } from '@sketchpixy/rubix';
import { ImplementationFor, ContentTypes } from '../AddForm/utils';
import { _map, columns } from '../../utils/library';

const SettingsForm = (props) => {
    const { handleSubmit, pristine, submitting, fields, infostyle, path, error, metaData, locationState, eventKey, changeKey, handleChange, dispatch, form, onlineInvitationFields, consentInviteFields, appointmentConsultationFields,recallReminderFields, record } = props;

    return (
      <form className={styles.form} onSubmit={handleSubmit}>
        <div className={styles.mainform} style={{ minHeight: `${document.body.clientHeight - 500}px` }}>
          <div className={styles.subform}>
            <div style={{ marginBottom: '3em' }}>
              <Row>
                <Col className={styles.infolabel}>
                  <h4 style={{ fontWeight: "bold", fontSize: "16px" }}>
                    ONLINE NEW PATIENT REGISTRATION
                  </h4>
                </Col>
                <Col style={{ paddingLeft: '2em' }}>
                  {onlineInvitationFields.map((field, i) => {
                    const Component = ImplementationFor[field.type];
                    const contentType = ContentTypes[field.type];
                    return (
                      ((field.editRecord || field.createRecord) ?
                        <div key={i}>
                          <Component
                            data={field}
                            contentType={contentType}
                            options={field.formFieldDecorationOptions}
                            optionsMode="edit"
                            infostyle={infostyle}
                          />
                        </div>
                      : null)
                    );
                  })}
                </Col>
              </Row>
            </div>
            <div style={{ marginTop: '3em', marginBottom: '3em' }}>
              <Row>
                <Col className={styles.infolabel}>
                  <h4 style={{ fontWeight: "bold", fontSize: "16px" }}>
                    PAPERLESS FORMS
                  </h4>
                </Col>
                <Col style={{ paddingLeft: '2em' }}>
                  {consentInviteFields.map((field, i) => {
                    const Component = ImplementationFor[field.type];
                    const contentType = ContentTypes[field.type];
                    return (
                      ((field.editRecord || field.createRecord) ?
                        <div key={i}>
                          <Component
                            data={field}
                            contentType={contentType}
                            options={field.formFieldDecorationOptions}
                            optionsMode="edit"
                            infostyle={infostyle}
                          />
                        </div>
                      : null)
                    );
                  })}
                </Col>
              </Row>
            </div>
          </div>
          <div className={styles.subform}>
          <div style={{ marginRight: '3em' }}>
            <Row>
              <Col className={styles.infolabel}>
                <h4 style={{ fontWeight: "bold", fontSize: "16px" }}>
                  APPOINTMENT REMINDERS
                </h4>
              </Col>
            
            <Col style={{ paddingLeft: '2em' }}>
            {appointmentConsultationFields.map((field, i) => {
              const Component = ImplementationFor[field.type];
              const contentType = ContentTypes[field.type];
              return (
                ((field.editRecord || field.createRecord) ?
                  <div key={i}>
                    <Component
                      data={field}
                      contentType={contentType}
                      options={field.formFieldDecorationOptions}
                      optionsMode="edit"
                      infostyle={infostyle}
                    />
                  </div>
                : null)
              );
            })}
            </Col>
            </Row>
          </div>
          <div style={{ marginTop: '3em', marginBottom: '3em', marginRight: '3em' }}>
            <Row>
              <Col className={styles.infolabel}>
                <h4 style={{ fontWeight: "bold", fontSize: "16px" }}>
                  RECALL REMINDERS
                </h4>
              </Col>
              <Col style={{ paddingLeft: '2em' }}>
            {recallReminderFields.map((field, i) => {
              const Component = ImplementationFor[field.type];
              const contentType = ContentTypes[field.type];
              return (
                ((field.editRecord || field.createRecord) ?
                  <div key={i}>
                    <Component
                      data={field}
                      contentType={contentType}
                      options={field.formFieldDecorationOptions}
                      optionsMode="edit"
                      infostyle={infostyle}
                    />
                  </div>
                : null)
              );
            })}
            </Col>
            </Row>
          </div>
          </div>
        </div>
        <Row className="action-button">
          <Col xs={12} className={styles.actionButtons}>
            <Button bsStyle="standard" type="submit" className={submitting ? `${styles.saveButton} buttonLoading` : styles.saveButton} disabled={record.onlineRegistrationModule == false && record.appointmentReminder == false ? true : false}>Save</Button>
          </Col>
        </Row>
      </form>
    );
};

SettingsForm.propTypes = {
    handleSubmit: PropTypes.func,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
    path: PropTypes.string,
    error: PropTypes.string,
    metaData: PropTypes.object,
    locationState: PropTypes.object,
};

export default reduxForm({
    form: 'editRecord',
    enableReinitialize: true
})(SettingsForm);
