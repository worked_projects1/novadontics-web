/**
*
* SAAS Payment
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import shallowCompare from 'react-addons-shallow-compare';
import Lightbox from 'react-image-lightbox';
import { Col, Modal, Icon, Button } from '@sketchpixy/rubix';
import BigPicture from 'bigpicture';
import styles from './styles.css';
import ModalForm from 'components/ModalRecordForm';
import EditRecordForm from 'components/EditRecordForm';
import { browserHistory } from 'react-router';
import { loadSaasPayment, updateSaasPayment } from '../../utils/sagas.js';
import moment from 'moment-timezone';
import { columns } from '../../utils/library';
import AlertMessage from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/RecordsPage/messages';

class SaasPayment extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    data: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      alertMessage: false,
      alertSuccess: false,
      yearSelected: null,
      records: {}
    };

    this.handleOpen = :: this.handleOpen;
    this.handleClose = :: this.handleClose;
  }

  render() {
    const { row, singleImage, style, implantStyle, saasId } = this.props;
    const { photoIndex, isOpen, yearSelected, records } = this.state;

    const updateYear = (val) => {
      const { saasId } = this.props;
      const { yearSelected } = this.state;
      this.setState({ yearSelected: val })
      loadSaasPayment(saasId, val)
        .then(saasList => {
          let saasRecords = saasList[0];
          saasRecords.year = parseInt(val);
          this.setState({ records: saasRecords })
        })
        .catch(() => this.setState({ records: {year: parseInt(val)} }));
    }

    return (
      <Col xs={12} className={styles.actions}>
        <Button bsSize="xs" bsStyle="link" onClick={() => this.handleOpen()}><img width="35" height="35" src={require('img/module/payment.svg')} /></Button>
        <Modal show={isOpen} backdrop="static" onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title className={styles.title}>
              <span style={{color: '#87b44d', fontWeight: 'bold'}}>SaaS Payment</span>
            </Modal.Title>
          </Modal.Header>
            <Modal.Body id="modalrecordform">
              <EditRecordForm
                fields={columns.saasPayment}
                initialValues = {records}
                form={`saasPaymentForm.${saasId}`}
                onSubmit={(data) => this.handleSubmit(data)}
                name={"saas"}
                disableCancel={true}
                catalogButton={true}
                onCancel={() => this.handleClose()}
                updateYear={(val) => updateYear(val)}
              />
              {this.state.alertMessage ?
                  <AlertMessage warning dismiss={() => this.setState({ alertMessage: false })}><FormattedMessage {...messages.feeFailure} /></AlertMessage> : null}
              {this.state.alertSuccess ?
                  <AlertMessage success dismiss={() => this.setState({ alertSuccess: false, isOpen: false })}><FormattedMessage {...messages.saasSuccess} /></AlertMessage> : null}
            </Modal.Body>
        </Modal>
      </Col>
    );
  }

  handleOpen() {
    this.setState({ isOpen: true });
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handleSubmit(data) {
    const { row, dispatch, saasId } = this.props;
    const submitRecord = data.toJS();
    const yearOpted = submitRecord.year
    delete submitRecord.year;
    var records = Object.keys(submitRecord).map(key => ({month: key, amount: submitRecord[key]}));
    var dispatchRecord = { "saas": records }

    updateSaasPayment(saasId, yearOpted, dispatchRecord)
      .then(response => {
        this.setState({ alertSuccess: true, records: {} });
      })
      .catch(() => this.setState({ alertMessage: true }));
  }
}

export default SaasPayment;
