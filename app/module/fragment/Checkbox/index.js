/**
 *
 * Checkbox
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { Map as iMap } from "immutable";
import { Col, Row } from '@sketchpixy/rubix';
import styles from './styles.css';


const renderCheckBoxField = ({ input, label, meta: { touched, error }, children, data, metaData }) => {
  const isPreDefinedSet = Array.isArray(data.oneOf);
  const Input = iMap(input).toJS();
  const inputVal = Input.value ? Input.value : [];
  const { defaultValue = {} } = data;
  const handleChange = (val) => {
    const day = document.getElementsByName(val)[0].checked;
    const valueObj = data && data.oneOf.reduce((input, row) => Object.assign({}, input, row.oneOf.reduce((a, el)=> Object.assign({}, a, {[el.value]: el.oneOf.reduce((b, fg) => b + document.getElementsByName(val + '.' + fg)[0].value+ " ", "").trim()}),{})), { day: val });
    const filteredVal = inputVal ? inputVal.filter(_ => _.day != val) : [];
    const selectedVal = inputVal.filter(_ => _.day === val); 
    const resultVal = selectedVal && selectedVal.length > 0 ? selectedVal.reduce((a, el) => Object.assign({}, a, el, valueObj), {}) : valueObj;
    input.onChange(day ? filteredVal.concat([resultVal]) : filteredVal);
  }

  const handleFilter = (val, type, node) => {
    const filterVal = inputVal.find(_ => _.day === val) || false;
    if(type && type.indexOf('Time') > -1){
      return filterVal && filterVal[node] && filterVal[node].split(" ")[0] || defaultValue && defaultValue[node] && defaultValue[node].split(" ")[0];
    } else if(type && type.indexOf('Clock') > -1){
      return filterVal && filterVal[node] && filterVal[node].split(" ")[1] || defaultValue && defaultValue[node] && defaultValue[node].split(" ")[1];
    } else {
      return filterVal;
    }
  }

  return (<Row className={styles.Checkbox}>
    <Col className={styles.days}>
      <h4 className={styles.rowDayLabel}>Working Days</h4>
      {((isPreDefinedSet && data.dayOf) || []).map((day, i) =>
        <div key={i} className={styles.checkboxContainer}>
          <input type="checkbox" name={day.value} id={day.value} defaultChecked={handleFilter(day.value) ? true : false} value={day.value} onChange={() => handleChange(day.value || day)} />
          <label htmlFor={day.value}>
            <span></span>
          </label>
          <Col className={styles.dots}>
            {data.oneOf.length - 1 != i ? Array.from(Array(4)).map((s, l) => <span key={l}></span>) : null}
          </Col>
          <Col className={styles.Label}>{day.label}</Col>
        </div>)}
    </Col>
    {((isPreDefinedSet && data.oneOf) || []).map((row, h) =>
      <Col key={h}>
        <h4 className={styles.rowLabel}>{row.label}</h4>
        <Col className={styles.hours}>
          {((row && row.oneOf) || []).map((col, t) =>
            <Col key={t} className={styles.time}>
              <h4>{col.label}</h4>
              {((data && data.dayOf) || []).map((day, d) =>
                <div key={d} className={styles.checkboxContainer}>
                  {((col && col.oneOf) || []).map((time, s) => 
                  <select
                    key={s}
                    name={`${day.value}.${time}`}
                    onChange={() => handleChange(day.value)}
                    defaultValue={handleFilter(day.value, time, col.value) ? handleFilter(day.value, time, col.value) : ""}
                    style={{ background: `url(${require('img/module/down.png')}) 88% / 18% no-repeat #757575` }}>
                    {((time && time.indexOf('Time') > -1 && data.timeOf) || (time && time.indexOf('Clock') > -1 && data.clockOf) || []).map((option, l) =>
                      <option key={l} value={option.value ? option.value : option}>{option.label ? option.label : option}</option>)}
                  </select>)}
                </div>)}
            </Col>)}
        </Col>
      </Col>)}
  </Row>)
}

class Checkbox extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showAttributes: null, selectedOptions: false };
  }

  static propTypes = {
    options: PropTypes.object,
    optionsMode: PropTypes.oneOf(['edit', 'create', 'request']),
    renderMode: PropTypes.oneOf(['field', 'group']),
    initialValue: PropTypes.object,
    path: PropTypes.string,
  };

  render() {
    const { data, options = {}, optionsMode } = this.props;
    let extraFieldOptions = {};
    if (optionsMode === 'edit') {
      extraFieldOptions = options.edit || {};
    } else if (optionsMode === 'create') {
      extraFieldOptions = options.create || {};
    }

    return (
      <Field
        name={data.value}
        component={renderCheckBoxField}
        type={'checkbox'}
        {...options.general}
        {...extraFieldOptions}
        {...this.props}
        onFocus={e => e.preventDefault()}
        onBlur={e => e.preventDefault()}
      />
    );
  }


}

export default Checkbox;
