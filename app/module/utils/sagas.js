import { call, take, put, race, select } from 'redux-saga/effects';
import api from 'utils/api';
import { DEFAULT_UPDATE_ERROR } from 'utils/errors.js';
import { replace } from 'react-router-redux';
import { moduleBlocks, name, toUpper, remotes, entityUrl } from 'utils/tools';
import patientsRemotes from 'blocks/patients/remotes';
import moment from 'moment';

const url = `ds/${name}`;

const VALID_CACHE_DIFF = -30;
const { actions = {}, selectors = {} } = moduleBlocks();
const {
    loadRecords,
    loadRecordsCacheHit,
    loadRecordsSuccess,
    updateRecordsSuccess,
    loadRecordsError,
    updateRecordsError,
    updateChildren,
    updateRecordsMetaData,
    updateDate } = actions;

const {
    selectChildren,
    selectRecordsMetaData,
    selectUpdateTimestamp,
} = selectors;

export const LOAD_PROVIDER_FILTER = `${url}/LOAD_PROVIDER_FILTER`;
export const UPDATE_PROVIDER_FILTER = `${url}/UPDATE_PROVIDER_FILTER`;
export const LOAD_RECORDS_DATE = `${url}/LOAD_RECORDS_DATE`;
export const UPDATE_RECORDS_DATE = `${url}/UPDATE_RECORDS_DATE`;
export const LOAD_DAILY_BRIEF = `${url}/LOAD_DAILY_BRIEF`;
export const LOAD_DATE = `${url}/LOAD_DATE`;
export const LOAD_BIRTHDAY_LIST = `${url}/LOAD_BIRTHDAY_LIST`;
export const LOAD_PROVIDER_FEE = `${url}/LOAD_PROVIDER_FEE`;

export const loadProviderFilter = () => ({
    type: LOAD_PROVIDER_FILTER
});

export const updateProviderFilter = (record) => ({
    type: UPDATE_PROVIDER_FILTER,
    record,
});

export const loadRecordsDate = (date) => ({
    type: LOAD_RECORDS_DATE,
    date,
});

export const updateRecordsDate = (date) => ({
    type: UPDATE_RECORDS_DATE,
    date,
});

export const loadDailyBrief = (date) => ({
    type: LOAD_DAILY_BRIEF,
    date,
});

export const loadDate = (date) => ({
    type: LOAD_DATE,
    date,
});

export const loadBirthdayList = (timeZone) => ({    
    type: LOAD_BIRTHDAY_LIST,
    timeZone,
});

export const loadProviderFee = (date) => ({
    type: LOAD_PROVIDER_FEE,
    date,
});
export const loadProviderFilterRemote = () =>
    api.get(`/providerFilter`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateProviderFilterRemote = (record) =>
    api.put(`/providerFilter`, record).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadRecordsRemote = (date) =>
    api.get(`/${name}/read${toUpper(name)}?date=${date ? date : moment().format('YYYY-MM-DD')}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const updateRecordsRemote = (date) =>
    api.get(`/${name}/read${toUpper(name)}?date=${date ? date : moment().format('YYYY-MM-DD')}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadDailyBriefRemote = (date) =>
    api.get(`/${name}/dailyBrief?date=${date ? date : moment().format('YYYY-MM-DD')}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadBirthdayListRemote = (timeZone) =>
    api.get(`/birthdayList?zoneOf=${timeZone ? timeZone : moment.tz.guess()}`).then((response) => response.data).catch((error) => Promise.reject(error));

export const loadProviderFeeRemote = (date) =>
    api.get(`/apptProviderFee?date=${date ? date : moment().format('YYYY-MM-DD')}`).then((response) => response.data).catch((error) => Promise.reject(error));

export function loadOperatories() {
  return api.get(`/operatories`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function reorderOperatories(record) {
  return api.put(`/operatorReorder`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadSaasPaymentSummary() {
  return api.get(`/saasPaymentSummary`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function loadSaasPayment(saasId, year) {
  return api.get(`/getSaasPayment?saasId=${saasId}&year=${year}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function updateSaasPayment(saasId, year, record) {
  return api.put(`/updateSaasPayment?saasId=${saasId}&year=${year}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function* loadProviderFilterSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_PROVIDER_FILTER),
        });
        try {
            const result = yield call(loadProviderFilterRemote);
            yield put(updateRecordsMetaData(result));
        } catch (error) {
            console.log(error);
            alert('unable to load provider filter');
        }
    }
}

export function* updateProviderFilterSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(UPDATE_PROVIDER_FILTER),
        });
        const { record } = req;
        if (record) {
            try {
                yield call(updateProviderFilterRemote, record);
                const result = yield call(loadProviderFilterRemote);
                yield put(updateRecordsMetaData({ providerFilter: result }));
                yield put(replace(`/`));
            } catch (error) {
                const errorMessage = error && error.response && error.response.data && error.response.data.error ? error.response.data.error : DEFAULT_UPDATE_ERROR;
                alert(errorMessage);
            }
        }
    }
}


function* loadRecordsDateSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { load } = yield race({
            load: take(LOAD_RECORDS_DATE),
        });
        const { date } = load || {};

        try {
            const records = yield call(loadRecordsRemote, date);
            let recordsMetaData = yield select(selectRecordsMetaData());

            if(recordsMetaData && recordsMetaData.size > 0) {
              const workingHoursRemotes = yield call(remotes, 'workingHours');
              const operatoriesRemotes = yield call(remotes, 'operatories');
              const workingHoursList = yield call(workingHoursRemotes.loadRecords);
              const operatories = yield call(operatoriesRemotes.loadRecords);
              let metaData = recordsMetaData.toJS();
              delete metaData.workingHoursList;
              delete metaData.operatories;
              Object.assign(metaData, {'workingHoursList': workingHoursList}, {'operatories': operatories});
              recordsMetaData = metaData;
            }

            if (recordsMetaData && recordsMetaData.size === 0) {
                const providersRemotes = yield call(remotes, 'providers');
                const operatoriesRemotes = yield call(remotes, 'operatories');
                const procedureRemotes = yield call(remotes, 'procedure');
                const filterRemotes = yield call(remotes, 'providerFilter');
                const workingHoursRemotes = yield call(remotes, 'workingHours');
                const nonWorkingDaysRemotes = yield call(remotes, 'nonWorkingDays');
                const reasonRemotes = yield call(remotes, `${name}Reason`);
                const staffMembersRemotes = yield call(remotes, 'staffMembers');
                const referralSourcesRemotes = yield call(remotes, 'referralSources');
                const referredToRemotes = yield call(remotes, 'referredTo');
                const officeInfoRemotes = yield call(remotes, 'officeInfo');
                const dentalCarrierRemotes = yield call(remotes, 'dentalCarrier'); 
                const clinicalNotesRemotes = yield call (remotes, 'preMadeClinicalNotes');
                const saasRemotes = yield call (remotes, 'saas');
                const insuranceCompanyRemotes = yield call(remotes, 'insuranceCompany');
                const insuranceRemotes = yield call (remotes,'insuranceFee');
                const insuranceList = yield call (insuranceRemotes.loadRecords);
                const clinicalNotesList = yield call (clinicalNotesRemotes.loadRecords);
                const saasList = yield call (saasRemotes.loadRecords);
                const insuranceCompanyList = yield call(insuranceCompanyRemotes.loadRecords);
                const providers = yield call(providersRemotes.loadRecords);
                const operatories = yield call(operatoriesRemotes.loadRecords);
                const procedure = yield call(procedureRemotes.loadRecords);
                const providerFilter = yield call(filterRemotes.loadRecords);
                const patientList = yield call(patientsRemotes.patientList);
                const workingHoursList = yield call(workingHoursRemotes.loadRecords);
                const nonWorkingDaysList = yield call(nonWorkingDaysRemotes.loadRecords);
                const reasonList = yield call(reasonRemotes.loadRecords);
                const activeChildren = yield select(selectChildren());
                const setup = activeChildren === 4 && (providers.length === 0 || operatories.length === 0 || workingHoursList.length === 0)
                const staffMembersList = yield call(staffMembersRemotes.loadRecords);
                const referralSourcesList = yield call(referralSourcesRemotes.loadRecords);
                const referredToList = yield call(referredToRemotes.loadRecords);
                const officeInfoList = yield call(officeInfoRemotes.loadRecords);

                const dentalCarrierList = yield call(dentalCarrierRemotes.loadRecords);

                recordsMetaData = {insuranceList, providers, operatories, procedure, patientList, providerFilter, insuranceCompanyList, workingHoursList, nonWorkingDaysList, reasonList, staffMembersList, referralSourcesList,referredToList,dentalCarrierList, clinicalNotesList, saasList, officeInfoList };

                if (setup)
                    yield put(updateChildren(0));

            }

            if (records) {
                yield put(loadRecordsSuccess(records, recordsMetaData));
            } else {
                yield put(loadRecordsError());
            }
        } catch (error) {
            const errorCode = error && error.response && error.response.data && (error.response.data["status code"] || error.response.data[0]["status code"]) ? error.response.data["status code"] || error.response.data[0]["status code"] : '412';
            if (errorCode === '1001')
                yield put(updateChildren(0));
            else
                yield put(loadRecordsError(error));

        }

    }
}

export function* updateRecordsDateSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { edit } = yield race({
            edit: take(UPDATE_RECORDS_DATE),
        });
        const { date } = edit || {};
        if (date) {
            try {
                const records = yield call(updateRecordsRemote, date);
                if (records !== undefined) {
                    /* TODO fix this logic */
                    yield put(updateRecordsSuccess(records));
                } else {
                    yield put(updateRecordsError());
                }
            } catch (error) {
                yield put(updateRecordsError());
            }
        }
    }
}


export function* loadDailyBriefSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { req } = yield race({
            req: take(LOAD_DAILY_BRIEF),
        });
        const { date } = req || {};
        if (date) {
            try {
                const result = yield call(loadDailyBriefRemote, date);
                yield put(updateRecordsMetaData({ dailyBrief: result }));
            } catch (error) {
                console.log(error);
                alert('unable to load daily brief records');
            }
        }
    }
}


export function* loadDateSaga() {
    while (true) { // eslint-disable-line no-constant-condition
        const { edit } = yield race({
            edit: take(LOAD_DATE),
        });
        const { date } = edit || {};
        if (date) {
            try {
                yield put(replace(`/`));
            } catch (error) {
                yield put(updateRecordsError());
            }
        }
    }
}

export function* loadBirthdayListSaga() {
    while (true) {
        const { req } = yield race({
            req: take(LOAD_BIRTHDAY_LIST),
        });
        const { timeZone } = req || {};
        if (timeZone) {
            try {
                const result = yield call(loadBirthdayListRemote, timeZone);                
                yield put(updateRecordsMetaData({ birthday: result }));
            } catch (error) {
                console.log(error);
                alert('unable to load birthday records');
            }
        }
    }
}

export function* loadProviderFeeSaga() {
    while (true) {
        const { req } = yield race({
            req: take(LOAD_PROVIDER_FEE),
        });
        const { date } = req || {};
        if (date) {
            try {
                const result = yield call(loadProviderFeeRemote, date);
                yield put(updateRecordsMetaData({ providerFeeList: result }));
            } catch (error) {
                console.log(error);
                alert('unable to load provider fee records');
            }
        }
    }
}

export const providerFilterSaga = [
    loadProviderFilterSaga,
    updateProviderFilterSaga,
    loadRecordsDateSaga,
    updateRecordsDateSaga,
    loadDailyBriefSaga,
    loadDateSaga,
    loadBirthdayListSaga,
    loadProviderFeeSaga
];
