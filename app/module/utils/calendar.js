import {loadRecordsDate} from './sagas'
import moment from 'moment';


function CalendarApp(props) {
    'use strict';
    let { date, resources, events, columns, showHours, disableDate, api, formId, autofill } = props || {};
    if (!date || (!(new Date(date) instanceof Date))) date = new Date();

    var dateFormat = this.getDateFormat(date);
    this.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.time = [{ "label": "12:00 AM", "hours": "0", "minutes": "0" }, { "label": "12:15 AM", "hours": "0", "minutes": "15" }, { "label": "12:30 AM", "hours": "0", "minutes": "30" }, { "label": "12:45 AM", "hours": "0", "minutes": "45" }, { "label": "01:00 AM", "hours": "01", "minutes": "0" }, { "label": "01:15 AM", "hours": "01", "minutes": "15" }, { "label": "01:30 AM", "hours": "01", "minutes": "30" }, { "label": "01:45 AM", "hours": "01", "minutes": "45" }, { "label": "02:00 AM", "hours": "02", "minutes": "0" }, { "label": "02:15 AM", "hours": "02", "minutes": "15" }, { "label": "02:30 AM", "hours": "02", "minutes": "30" }, { "label": "02:45 AM", "hours": "02", "minutes": "45" }, { "label": "03:00 AM", "hours": "03", "minutes": "0" }, { "label": "03:15 AM", "hours": "03", "minutes": "15" }, { "label": "03:30 AM", "hours": "03", "minutes": "30" }, { "label": "03:45 AM", "hours": "03", "minutes": "45" }, { "label": "04:00 AM", "hours": "04", "minutes": "0" }, { "label": "04:15 AM", "hours": "04", "minutes": "15" }, { "label": "04:30 AM", "hours": "04", "minutes": "30" }, { "label": "04:45 AM", "hours": "04", "minutes": "45" }, { "label": "05:00 AM", "hours": "05", "minutes": "0" }, { "label": "05:15 AM", "hours": "05", "minutes": "15" }, { "label": "05:30 AM", "hours": "05", "minutes": "30" }, { "label": "05:45 AM", "hours": "05", "minutes": "45" }, { "label": "06:00 AM", "hours": "06", "minutes": "0" }, { "label": "06:15 AM", "hours": "06", "minutes": "15" }, { "label": "06:30 AM", "hours": "06", "minutes": "30" }, { "label": "06:45 AM", "hours": "06", "minutes": "45" }, { "label": "07:00 AM", "hours": "07", "minutes": "0" }, { "label": "07:15 AM", "hours": "07", "minutes": "15" }, { "label": "07:30 AM", "hours": "07", "minutes": "30" }, { "label": "07:45 AM", "hours": "07", "minutes": "45" }, { "label": "08:00 AM", "hours": "08", "minutes": "0" }, { "label": "08:15 AM", "hours": "08", "minutes": "15" }, { "label": "08:30 AM", "hours": "08", "minutes": "30" }, { "label": "08:45 AM", "hours": "08", "minutes": "45" }, { "label": "09:00 AM", "hours": "09", "minutes": "0" }, { "label": "09:15 AM", "hours": "09", "minutes": "15" }, { "label": "09:30 AM", "hours": "09", "minutes": "30" }, { "label": "09:45 AM", "hours": "09", "minutes": "45" }, { "label": "10:00 AM", "hours": "10", "minutes": "0" }, { "label": "10:15 AM", "hours": "10", "minutes": "15" }, { "label": "10:30 AM", "hours": "10", "minutes": "30" }, { "label": "10:45 AM", "hours": "10", "minutes": "45" }, { "label": "11:00 AM", "hours": "11", "minutes": "0" }, { "label": "11:15 AM", "hours": "11", "minutes": "15" }, { "label": "11:30 AM", "hours": "11", "minutes": "30" }, { "label": "11:45 AM", "hours": "11", "minutes": "45" }, { "label": "12:00 PM", "hours": "12", "minutes": "0" }, { "label": "12:15 PM", "hours": "12", "minutes": "15" }, { "label": "12:30 PM", "hours": "12", "minutes": "30" }, { "label": "12:45 PM", "hours": "12", "minutes": "45" }, { "label": "01:00 PM", "hours": "13", "minutes": "0" }, { "label": "01:15 PM", "hours": "13", "minutes": "15" }, { "label": "01:30 PM", "hours": "13", "minutes": "30" }, { "label": "01:45 PM", "hours": "13", "minutes": "45" }, { "label": "02:00 PM", "hours": "14", "minutes": "0" }, { "label": "02:15 PM", "hours": "14", "minutes": "15" }, { "label": "02:30 PM", "hours": "14", "minutes": "30" }, { "label": "02:45 PM", "hours": "14", "minutes": "45" }, { "label": "03:00 PM", "hours": "15", "minutes": "0" }, { "label": "03:15 PM", "hours": "15", "minutes": "15" }, { "label": "03:30 PM", "hours": "15", "minutes": "30" }, { "label": "03:45 PM", "hours": "15", "minutes": "45" }, { "label": "04:00 PM", "hours": "16", "minutes": "0" }, { "label": "04:15 PM", "hours": "16", "minutes": "15" }, { "label": "04:30 PM", "hours": "16", "minutes": "30" }, { "label": "04:45 PM", "hours": "16", "minutes": "45" }, { "label": "05:00 PM", "hours": "17", "minutes": "0" }, { "label": "05:15 PM", "hours": "17", "minutes": "15" }, { "label": "05:30 PM", "hours": "17", "minutes": "30" }, { "label": "05:45 PM", "hours": "17", "minutes": "45" }, { "label": "06:00 PM", "hours": "18", "minutes": "0" }, { "label": "06:15 PM", "hours": "18", "minutes": "15" }, { "label": "06:30 PM", "hours": "18", "minutes": "30" }, { "label": "06:45 PM", "hours": "18", "minutes": "45" }, { "label": "07:00 PM", "hours": "19", "minutes": "0" }, { "label": "07:15 PM", "hours": "19", "minutes": "15" }, { "label": "07:30 PM", "hours": "19", "minutes": "30" }, { "label": "07:45 PM", "hours": "19", "minutes": "45" }, { "label": "08:00 PM", "hours": "20", "minutes": "0" }, { "label": "08:15 PM", "hours": "20", "minutes": "15" }, { "label": "08:30 PM", "hours": "20", "minutes": "30" }, { "label": "08:45 PM", "hours": "20", "minutes": "45" }, { "label": "09:00 PM", "hours": "21", "minutes": "0" }, { "label": "09:15 PM", "hours": "21", "minutes": "15" }, { "label": "09:30 PM", "hours": "21", "minutes": "30" }, { "label": "09:45 PM", "hours": "21", "minutes": "45" }, { "label": "10:00 PM", "hours": "22", "minutes": "0" }, { "label": "10:15 PM", "hours": "22", "minutes": "15" }, { "label": "10:30 PM", "hours": "22", "minutes": "30" }, { "label": "10:45 PM", "hours": "22", "minutes": "45" }, { "label": "11:00 PM", "hours": "23", "minutes": "0" }, { "label": "11:15 PM", "hours": "23", "minutes": "15" }, { "label": "11:30 PM", "hours": "23", "minutes": "30" }, { "label": "11:45 PM", "hours": "23", "minutes": "45" }];
    this.timeOptions = [{ "id": 1, "label": "10:00", "value": "10:00" }, { "id": 2, "label": "10:15", "value": "10:15" }, { "id": 3, "label": "10:30", "value": "10:30" }, { "id": 4, "label": "10:45", "value": "10:45" }, { "id": 5, "label": "11:00", "value": "11:00" }, { "id": 6, "label": "11:15", "value": "11:15" }, { "id": 7, "label": "11:30", "value": "11:30" }, { "id": 8, "label": "11:45", "value": "11:45" }, { "id": 9, "label": "12:00", "value": "12:00" }, { "id": 10, "label": "12:15", "value": "12:15" }, { "id": 11, "label": "12:30", "value": "12:30" }, { "id": 12, "label": "12:45", "value": "12:45" }, { "id": 13, "label": "01:00", "value": "01:00" }, { "id": 14, "label": "01:15", "value": "01:15" }, { "id": 15, "label": "01:30", "value": "01:30" }, { "id": 16, "label": "01:45", "value": "01:45" }, { "id": 17, "label": "02:00", "value": "02:00" }, { "id": 18, "label": "02:15", "value": "02:15" }, { "id": 19, "label": "02:30", "value": "02:30" }, { "id": 20, "label": "02:45", "value": "02:45" }, { "id": 21, "label": "03:00", "value": "03:00" }, { "id": 22, "label": "03:15", "value": "03:15" }, { "id": 23, "label": "03:30", "value": "03:30" }, { "id": 24, "label": "03:45", "value": "03:45" }, { "id": 25, "label": "04:00", "value": "04:00" }, { "id": 26, "label": "04:15", "value": "04:15" }, { "id": 27, "label": "04:30", "value": "04:30" }, { "id": 28, "label": "04:45", "value": "04:45" }, { "id": 29, "label": "05:00", "value": "05:00" }, { "id": 30, "label": "05:15", "value": "05:15" }, { "id": 31, "label": "05:30", "value": "05:30" }, { "id": 32, "label": "05:45", "value": "05:45" }, { "id": 33, "label": "06:00", "value": "06:00" }, { "id": 34, "label": "06:15", "value": "06:15" }, { "id": 35, "label": "06:30", "value": "06:30" }, { "id": 36, "label": "06:45", "value": "06:45" }, { "id": 37, "label": "07:00", "value": "07:00" }, { "id": 38, "label": "07:15", "value": "07:15" }, { "id": 39, "label": "07:30", "value": "07:30" }, { "id": 40, "label": "07:45", "value": "07:45" }, { "id": 41, "label": "08:00", "value": "08:00" }, { "id": 42, "label": "08:15", "value": "08:15" }, { "id": 43, "label": "08:30", "value": "08:30" }, { "id": 44, "label": "08:45", "value": "08:45" }, { "id": 45, "label": "09:00", "value": "09:00" }, { "id": 46, "label": "09:15", "value": "09:15" }, { "id": 47, "label": "09:30", "value": "09:30" }, { "id": 48, "label": "09:45", "value": "09:45" }];
    this.resources = resources || [{ id: 1, title: 'Shahid' }, { id: 2, title: 'Slim Sioussi' }];
    this.formId = formId || false;
    this.itemsPerPage = document.body.clientWidth > 1225 ? 7 : 3;
    this.currentPage = 1;
    this.totalPage = Math.abs(this.resources.length / this.itemsPerPage);
    this.showHours = showHours || [];
    this.disableDate = disableDate || [];
    this.api = api || false;
    this.events = events || [{
        id: 1,
        resourceId: 1,
        start: new Date(dateFormat.year, dateFormat.month - 1, dateFormat.date, 10, 0),
        end: new Date(dateFormat.year, dateFormat.month - 1, dateFormat.date, 11, 30),
        title: "My First Event",
        date: dateFormat.year + '-' + dateFormat.month + '-' + dateFormat.date,
        startTime: '10:00 AM',
        endTime: '11:30 AM'
    }];
    this.columns = columns || [{ id: 1, name: 'title', label: 'Title', type: 'text' }, { id: 2, name: 'date', label: 'Date', type: 'date' }, { id: 3, name: 'startTime', label: 'Start Time', type: 'time' }, { id: 4, name: 'endTime', label: 'End Time', type: 'time' }];
    this.calendarApp = document.getElementById("calendar");
    this.date = new Date(date) || new Date();
    this.datepicker = this.FooPicker.bind(this);
    this.props = props || {};
    this.autofill = autofill && Object.keys(autofill).length > 0 ? autofill : false;
    if (this.calendarApp) {
        this.calendarApp.innerHTML = '';
        /* Start the app */
        this.createHeader(this.date);
        this.showView(this.date);
        this.addEventListeners();
    }
}


CalendarApp.prototype.addEventListeners = function () {
    this.calendarApp.addEventListener("click", this.closeModal.bind(this));
    this.prev.addEventListener("click", this.onChangedate.bind(this, 'prev'));
    this.next.addEventListener("click", this.onChangedate.bind(this, 'next'));
    this.today.addEventListener("click", this.onChangedate.bind(this, 'today'));
    this.calendarApp.addEventListener("resize", this.resizeCalendar.bind(this));
};


CalendarApp.prototype.showView = function (date) {
    if (!date || (!(date instanceof Date))) date = new Date();
    var now = new Date(date),
        y = now.getFullYear(),
        m = now.getMonth(),
        d = now.getDate();

    var today = new Date();
    var day = now.getDay();
    var time = [];
    var [selDay] = this.showHours.filter(_ => _.day === this.days[day]);
    var [selStartTime] = this.time.filter(_ => selDay && selDay.startTime && _.label === selDay.startTime);
    var [selEndTime] = this.time.filter(_ => selDay && selDay.startTime && _.label === selDay.endTime);
    var selTime = this.time.filter(_ => selStartTime && selEndTime && this.convertDate(now, _.hours, _.minutes) >= this.convertDate(now, selStartTime.hours, selStartTime.minutes) && this.convertDate(now, _.hours, _.minutes) <= this.convertDate(now, selEndTime.hours, selEndTime.minutes));
    if (selTime && selTime.length <= 1) {
        time = this.time;
    } else {
        time = selTime;
    }

    var selLunchStartTime = this.time.find(_ => selDay && selDay.lunchStartTime && _.label === selDay.lunchStartTime);
    var selLunchEndTime = this.time.find(_ => selDay && selDay.lunchStartTime && _.label === selDay.lunchEndTime);

    var selResStarts = (this.currentPage - 1) * this.itemsPerPage + 1;
    var selResEnds = this.currentPage * this.itemsPerPage;
    var selResources = this.resources.filter((res, index) => (index + 1) >= selResStarts && (index + 1) <= selResEnds);
    if (selResources && selResources.length < this.itemsPerPage) {
        for (var z = selResources.length + 1; z <= this.itemsPerPage; z++) {
            selResources.push({ title: '' });
        }
    }
    var disableDate = this.disableDate.filter(_ => new Date(_.date + 'T00:00').toString() === new Date(y, m, d).toString());

    var calendarTable = document.getElementById('cl-table');
    if (calendarTable) {
        calendarTable.remove();
    }

    var div = document.createElement('DIV');
    div.setAttribute("id", "cl-table");
    div.setAttribute("class", "cl-table");
    this.calendarApp.appendChild(div);

    if (this.totalPage > 1) {
        if (this.currentPage > 1) {
            var div = document.createElement('DIV');
            div.setAttribute("class", "cl-table-prev-div");
            div.appendChild(document.createTextNode('<'));
            div.onclick = this.onPaginate.bind(this, 'prev');
            document.getElementById('cl-table').appendChild(div);
        }

        if (this.currentPage < this.totalPage) {
            var div = document.createElement('DIV');
            div.setAttribute("class", "cl-table-next-div");
            div.appendChild(document.createTextNode('>'));
            div.onclick = this.onPaginate.bind(this, 'next');
            document.getElementById('cl-table').appendChild(div);
        }
    }

    var table = document.createElement("TABLE");
    document.getElementById("cl-table").appendChild(table);

    var thead = document.createElement("THEAD");
    table.appendChild(thead);

    var tr = document.createElement("TR");
    tr.setAttribute("id", "cl-resources");
    thead.appendChild(tr);

    var th = document.createElement("TH");
    var val = document.createTextNode("");
    th.appendChild(val);
    document.getElementById("cl-resources").appendChild(th);

    selResources.map(resource => {
        var th = document.createElement("TH");
        var val = document.createTextNode(resource.title);
        th.appendChild(val);
        document.getElementById("cl-resources").appendChild(th);
    });



    var tbody = document.createElement("TBODY");
    tbody.setAttribute('id', 'cl-modal-tbody')
    table.appendChild(tbody);

    time.map((t, i) => {

        var tr = document.createElement("TR");
        tr.setAttribute("id", 'cl-table-row' + i);
        tbody.appendChild(tr);

        var td = document.createElement("TD");
        var val = document.createTextNode(t.label);
        td.appendChild(val);
        if (t.label.indexOf(':00') > -1) {
            td.style.fontWeight = 'bold';
        } else {
            td.style.fontWeight = 'lighter';
        }
        td.setAttribute("valign", "top");
        document.getElementById('cl-table-row' + i).appendChild(td);

        selResources.map(r => {
            const time = new Date(y, m, d, t.hours, t.minutes);
            const event = this.events.filter(_ => time.toString() === new Date(_.start).toString() && _.resourceId === r.id);
            const filterEvent = this.events.filter(_ => time > new Date(_.start) && time < new Date(_.end) && _.resourceId === r.id);;
            const eventFrom = event.length > 0 ? new Date(event[0].start) : time;
            const eventEnd = event.length > 0 ? new Date(event[0].end) : time;
            const eventMilliSeconds = time - eventEnd;
            const eventMinutes = Math.abs(eventMilliSeconds / 60000);
            const rowspan = event.length > 0 && eventMinutes > 0 ? Math.abs((eventMinutes / 15)) : Math.abs((eventMinutes / 15) + 1);
            const lunchTime = selLunchStartTime && selLunchEndTime && this.convertDate(now, selLunchStartTime.hours, selLunchStartTime.minutes) <= time && this.convertDate(now, selLunchEndTime.hours, selLunchEndTime.minutes) > time;

            if ((event.length > 0 && eventMinutes > 0) || filterEvent.length === 0) {
                var td = document.createElement("TD");
                var val = document.createTextNode("");
                td.appendChild(val);
                td.onclick = event.length > 0 && eventMinutes > 0 ? this.editEvent.bind(this) : this.addEvent.bind(this);
                td.setAttribute("data-time", time);
                td.setAttribute("data-resource", r.id || '');
                td.setAttribute("data-id", event.length > 0 && eventMinutes > 0 && event[0].id || '');
                td.setAttribute("valign", "top");
                td.setAttribute("rowspan", rowspan);
                td.setAttribute("style", `${(lunchTime || disableDate.length > 0 || !selDay) ? "background-color:#dadada;border-bottom: 1px solid #adadad57;" : ''}`);
                td.innerHTML = event.length > 0 && eventMinutes > 0 ? event[0].title : '';
                document.getElementById('cl-table-row' + i).appendChild(td);

                if (this.autofill && Object.keys(this.autofill).length > 0 && i === 0) {
                    setTimeout(() => td.click(), 3000);
                }
            }
        });
    })

    this.resizeCalendar();
}


CalendarApp.prototype.infoModal = function (title, info) {


    var modal = document.createElement("DIV");
    modal.setAttribute("class", "cl-modal cl-info-modal");
    this.calendarApp.appendChild(modal);

    var content = document.createElement("DIV");
    content.setAttribute("class", "cl-modal-content");

    var body = document.createElement("DIV");
    body.setAttribute("class", "cl-modal-body");

    var div = document.createElement('DIV');
    div.setAttribute('class', 'rowStandard');
    div.innerHTML = `<h4>${title}</h4>` + info;
    body.appendChild(div);
    content.appendChild(body);

    var footer = document.createElement("DIV");
    footer.setAttribute("class", "cl-modal-footer");

    var button = document.createElement('BUTTON');
    button.appendChild(document.createTextNode('OK'));
    button.onclick = () => modal.remove();
    footer.appendChild(button);

    footer.appendChild(button);
    content.appendChild(footer);

    modal.appendChild(content);

    modal.style.display = 'block';
}

CalendarApp.prototype.confirmModal = function (action) {


    var modal = document.createElement("DIV");
    modal.setAttribute("class", "cl-modal cl-confirm-modal");
    this.calendarApp.appendChild(modal);

    var content = document.createElement("DIV");
    content.setAttribute("class", "cl-modal-content");

    var body = document.createElement("DIV");
    body.setAttribute("class", "cl-modal-body");

    var div = document.createElement('DIV');
    div.setAttribute('class', 'rowStandard');

    var h4 = document.createElement('H4');
    h4.appendChild(document.createTextNode('Confirm'));
    div.appendChild(h4);

    var div2 = document.createElement('DIV');
    div2.appendChild(document.createTextNode('Are you Sure?'));
    div.appendChild(div2);
    body.appendChild(div);
    content.appendChild(body);

    var footer = document.createElement("DIV");
    footer.setAttribute("class", "cl-modal-footer");

    var button = document.createElement('BUTTON');
    button.appendChild(document.createTextNode('NO'));
    button.onclick = () => modal.remove();
    footer.appendChild(button);

    var div = document.createElement('DIV');
    div.setAttribute('class', 'vr');
    footer.appendChild(div);

    var button = document.createElement('BUTTON');
    button.appendChild(document.createTextNode('YES'));
    button.onclick = () => this[action]();
    footer.appendChild(button);
    content.appendChild(footer);

    modal.appendChild(content);

    modal.style.display = 'block';
}

CalendarApp.prototype.alertModal = function (error) {


    var modal = document.createElement("DIV");
    modal.setAttribute("class", "cl-modal cl-alert-modal");
    this.calendarApp.appendChild(modal);

    var content = document.createElement("DIV");
    content.setAttribute("class", "cl-modal-content");

    var body = document.createElement("DIV");
    body.setAttribute("class", "cl-modal-body");

    var div = document.createElement('DIV');
    div.setAttribute('class', 'rowStandard');

    var h4 = document.createElement('H4');
    h4.appendChild(document.createTextNode('Alert!'));
    div.appendChild(h4);

    var div2 = document.createElement('DIV');
    div2.appendChild(document.createTextNode(error));
    div.appendChild(div2);
    body.appendChild(div);
    content.appendChild(body);

    var footer = document.createElement("DIV");
    footer.setAttribute("class", "cl-modal-footer");

    var button = document.createElement('BUTTON');
    button.appendChild(document.createTextNode('OK'));
    button.onclick = () => modal.remove();
    footer.appendChild(button);

    footer.appendChild(button);
    content.appendChild(footer);

    modal.appendChild(content);

    modal.style.display = 'block';
}

CalendarApp.prototype.createModal = function (date, title) {
    var modal = document.createElement("DIV");
    modal.setAttribute("id", "cl-modal");
    modal.setAttribute("class", "cl-modal");
    this.calendarApp.appendChild(modal);

    var content = document.createElement("DIV");
    content.setAttribute("class", "cl-modal-content");
    modal.appendChild(content);

    var header = document.createElement("DIV");
    header.setAttribute("class", "cl-modal-header");
    content.appendChild(header);

    var span = document.createElement("SPAN");
    span.innerHTML = '&times;';
    span.setAttribute("id", "cl-modal-close");
    span.setAttribute("class", "cl-modal-close");
    header.appendChild(span);

    var h2 = document.createElement("H2");
    var val = document.createTextNode(title);
    h2.appendChild(val);
    header.appendChild(h2);

    var body = document.createElement("DIV");
    body.setAttribute("id", "cl-modal-body");
    body.setAttribute("class", "cl-modal-body");
    content.appendChild(body);

    var footer = document.createElement("DIV");
    footer.setAttribute("class", "cl-modal-footer");
    content.appendChild(footer);
}

CalendarApp.prototype.openModal = function () {
    document.getElementById("cl-modal").style.display = 'block';
}

CalendarApp.prototype.closeModal = function (e) {
    var modal = document.getElementById("cl-modal");
    var closeModal = document.getElementById("cl-modal-close");
    var closeModalBtn = document.getElementById("cl-modal-close-btn");
    if (e.target == closeModal || e.target == closeModalBtn || e === 'close') {
        this.props.clear();
        modal.remove();
    }
}

CalendarApp.prototype.addEvent = function (e) {
    e.stopPropagation();
    var resourceId = e.currentTarget.getAttribute('data-resource');
    if (resourceId) {
        this.createModal(false, 'Add Appointment');
        this.createForm(e);
        this.openModal();
    }
}

CalendarApp.prototype.createHeader = function (date) {

    if (!date || (!(date instanceof Date))) date = new Date();
    var now = new Date(date),
        y = now.getFullYear(),
        m = now.getMonth(),
        d = now.getDate();

    var div = document.createElement("DIV");
    div.setAttribute("id", "cl-header");
    div.setAttribute("class", "cl-header");
    this.calendarApp.appendChild(div);

    var div = document.createElement("DIV");
    var text = document.createTextNode("today");
    div.appendChild(text);
    div.setAttribute("id", "cl-today");
    div.setAttribute("class", "cl-today");
    document.getElementById("cl-header").appendChild(div);
    this.today = document.getElementById("cl-today");


    var div = document.createElement("DIV");
    //var text = document.createTextNode("〱");
    var text = document.createTextNode("<");
    div.appendChild(text);
    div.setAttribute("id", "cl-prev");
    div.setAttribute("class", "cl-prev");
    document.getElementById("cl-header").appendChild(div);
    this.prev = document.getElementById("cl-prev");

    var div = document.createElement("DIV");
    div.setAttribute("id", "cl-date");
    div.setAttribute("class", "cl-date");

    var span = document.createElement("SPAN");
    span.setAttribute("id", "cl-selected-date");
    span.setAttribute("value", y + '-' + m + '-' + d);
    var text = document.createTextNode(`${d} ${this.months[m]} ${y}`);
    span.appendChild(text);
    div.appendChild(span);

    document.getElementById("cl-header").appendChild(div);


    this.datepicker({
        id: 'cl-selected-date',
        dateFormat: 'dd/MM/yyyy',
        showCalendar: true,
        disable: this.disableDate.map(el => {
            var getDate = this.getDateFormat(el.date);
            return getDate.date + '/' + getDate.month + '/' + getDate.year;
        })
    });

    var div = document.createElement("DIV");
    //var text = document.createTextNode("〱");
    var text = document.createTextNode(">");
    div.appendChild(text);
    div.setAttribute("id", "cl-next");
    div.setAttribute("class", "cl-next");
    document.getElementById("cl-header").appendChild(div);
    this.next = document.getElementById("cl-next");

}

CalendarApp.prototype.onChangedate = function (type) {
    if (type === 'next') {
        this.date.setDate(this.date.getDate() + 1);
    } else if (type === 'prev') {
        this.date.setDate(this.date.getDate() - 1);
    } else if (type === 'today') {
        this.date = new Date();
    }
    if (this.props.onChangeDate) {
        this.props.onChangeDate(this.date);
    }
    this.showView(this.date);
    var now = new Date(this.date);
    var y = now.getFullYear(),
        m = now.getMonth(),
        d = now.getDate();
    document.getElementById("cl-selected-date").innerHTML = `${d} ${this.months[m]} ${y}`;
}

CalendarApp.prototype.editEvent = function (e) {
    e.stopPropagation();
    this.createModal(false, 'Edit Appointment');
    this.editForm(e);
    this.openModal();
}

CalendarApp.prototype.deleteEvent = function () {
    var confirmModal = document.getElementsByClassName('cl-confirm-modal')[0];
    if (confirmModal)
        confirmModal.remove();

    if (this.selEvent && this.selEvent.id) {
        if (this.api && this.api.url) {
            this.request('DELETE', { id: this.selEvent.id });
        } else if (this.props.onDelete) {
            this.props.onDelete(this.selEvent.id)
        } else {
            this.events = this.events.filter(_ => _.id === this.selEvent.id);
        }
    }
}

CalendarApp.prototype.createForm = function (e) {
    var dayEle = e.currentTarget;

    var selDate = dayEle.getAttribute('data-time');
    var resourceId = dayEle.getAttribute('data-resource');
    this.selEvent = { date: selDate, resourceId: parseInt(resourceId) };

    var div = document.createElement("DIV");
    div.setAttribute("id", "cl-modal-form");
    div.setAttribute("class", "cl-modal-form");
    document.getElementById("cl-modal-body").innerHTML = '';
    document.getElementById("cl-modal-body").appendChild(div);

    var form = document.createElement("FORM");
    form.setAttribute('id', "cl-modal-add-form");
    form.setAttribute('autocomplete', 'off');
    form.onsubmit = this.onSubmit.bind(this);
    document.getElementById("cl-modal-form").appendChild(form);

    var formEle = document.getElementById("cl-modal-form").getElementsByTagName('form')[0];

    var input = document.createElement('INPUT');
    input.setAttribute('name', 'resourceId');
    input.setAttribute('type', 'hidden');
    input.value = resourceId;
    formEle.appendChild(input);

    const autofillEle = this.autofill;

    this.columns.map(col => {
        if (col.display && col.display.create) {
            return false;
        }
        if (col.type === 'date') {
            this.dateField(col, formEle, this.getDateFormat(selDate));
        } else if (col.type === 'time') {
            this.timeField(col, formEle, new Date(selDate));
        } else if (col.type === 'select' && col.search) {
            this.searchField(col, formEle, col.resourceColumn ? parseInt(resourceId) : autofillEle && autofillEle[col.name] ? autofillEle[col.name] : false);
        } else if (col.type === 'select') {
            this.selectField(col, formEle, col.resourceColumn ? parseInt(resourceId) : autofillEle && autofillEle[col.name] ? autofillEle[col.name] : false);
        } else if (col.type === 'multiSelect') {
            this.multiSelectField(col, formEle, col.resourceColumn ? parseInt(resourceId) : autofillEle && autofillEle[col.name] ? autofillEle[col.name] : false);
        } else if (col.type === 'table') {
            this.tableField(col, formEle, col.resourceColumn ? parseInt(resourceId) : autofillEle && autofillEle[col.name] ? autofillEle[col.name] : false);
        } else {
            this.inputField(col, formEle, autofillEle && autofillEle[col.name]);
        }
    });

    var div = document.createElement("DIV");

    var btn = document.createElement("BUTTON");
    btn.setAttribute('type', 'submit');
    btn.appendChild(document.createTextNode('Save'));
    div.appendChild(btn);

    var btn = document.createElement("BUTTON");
    btn.setAttribute('type', 'button');
    btn.setAttribute('id', 'cl-modal-close-btn');
    btn.onclick = this.closeModal.bind(this);
    btn.appendChild(document.createTextNode('Close'));
    div.appendChild(btn);

    div.setAttribute("class", "cl-modal-button");
    formEle.appendChild(div);

    this.bookedList({ date: dayEle.getAttribute('data-time'), resourceId: parseInt(resourceId) });
}

CalendarApp.prototype.bookedList = function (el) {


    var selDate = this.getDateFormat(el.date);

    var filterEvent = this.events.filter(_ => {
        var evDate = this.getDateFormat(_.start);
        return selDate.fullDate.toString() === evDate.fullDate.toString() && el.date.toString() != new Date(_.start).toString() && _.resourceId === el.resourceId && this.selEvent.id != _.id
    });

    var ele = document.getElementById('cl-modal-booked-list');
    if (ele)
        ele.remove();

    var div = document.createElement('DIV');
    div.setAttribute('id', 'cl-modal-booked-list');

    var h4 = document.createElement('H4');
    var operatory = `Already booked appointments in this operatory on this date`;
    var provider = `Already booked appointments with the same provider on this date`;
    h4.appendChild(document.createTextNode(this.props.tab ? operatory : provider));
    div.appendChild(h4);

    var div2 = document.createElement('DIV');
    div2.setAttribute('class', 'cl-modal-booked-list');

    if (filterEvent && filterEvent.length > 0) {
        filterEvent.map(el => {
            var div3 = document.createElement('DIV');
            div3.setAttribute('class', 'cl-modal-booked-list-div')
            div3.innerHTML = el.title;
            div2.appendChild(div3);
        })
    } else {
        var div3 = document.createElement('DIV');
        div3.setAttribute('class', 'cl-modal-booked-msg-div')
        div3.appendChild(document.createTextNode('No appointments scheduled yet'));
        div2.appendChild(div3);
    }

    div.appendChild(div2);

    document.getElementById('cl-modal-form').appendChild(div);

}

CalendarApp.prototype.editForm = function (e) {
    var dayEle = e.currentTarget;

    var selDate = this.getDateFormat(dayEle.getAttribute('data-time'));
    var resourceId = dayEle.getAttribute('data-resource');

    const [selEvent] = this.events.filter(_ => dayEle.getAttribute('data-time').toString() === new Date(_.start).toString() && _.resourceId === parseInt(resourceId));
    this.selEvent = selEvent;
    var div = document.createElement("DIV");
    div.setAttribute("id", "cl-modal-form");
    div.setAttribute("class", "cl-modal-form");
    document.getElementById("cl-modal-body").innerHTML = '';
    document.getElementById("cl-modal-body").appendChild(div);

    var form = document.createElement("FORM");
    form.setAttribute('id', "cl-modal-edit-form");
    form.onsubmit = this.onEdit.bind(this);
    document.getElementById("cl-modal-form").appendChild(form);

    var formEle = document.getElementById("cl-modal-form").getElementsByTagName('form')[0];

    this.columns.map(col => {
        if (col.display && col.display.edit) {
            return false;
        }
        if (col.type === 'date') {
            this.dateField(col, formEle, selDate, selEvent[col.name]);
        } else if (col.type === 'time') {
            this.timeField(col, formEle, selDate, selEvent[col.name]);
        } else if (col.type === 'select' && col.search) {
            this.searchField(col, formEle, col.valColumn && parseInt(selEvent[col.name]) === 0 ? selEvent[col.valColumn] : selEvent[col.name]);
        } else if (col.type === 'select') {
            this.selectField(col, formEle, selEvent[col.name]);
        } else if (col.type === 'multiSelect' && col.search) {
            this.multiSelectField(col, formEle, col.valColumn && parseInt(selEvent[col.name]) === 0 ? selEvent[col.valColumn] : selEvent[col.name]);
        } else if (col.type === 'table') {
            this.tableField(col, formEle, col.valColumn && parseInt(selEvent[col.name]) === 0 ? selEvent[col.valColumn] : selEvent[col.name], true);
        } else {
            this.inputField(col, formEle, selEvent[col.name]);
        }
    });

    var div = document.createElement("DIV");

    var btn = document.createElement("BUTTON");
    btn.setAttribute('type', 'submit');
    btn.appendChild(document.createTextNode('Save'));
    div.appendChild(btn);


    // var btn = document.createElement("BUTTON");
    // btn.setAttribute('type', 'button');
    // btn.onclick = () => this.confirmModal('deleteEvent');
    // btn.appendChild(document.createTextNode('Delete'));
    // div.appendChild(btn);

    var btn = document.createElement("BUTTON");
    btn.setAttribute('type', 'button');
    btn.setAttribute('id', 'cl-modal-close-btn');
    btn.onclick = this.closeModal.bind(this);
    btn.appendChild(document.createTextNode('Close'));
    div.appendChild(btn);

    div.setAttribute("class", "cl-modal-button");
    formEle.appendChild(div);


    this.bookedList({ date: dayEle.getAttribute('data-time'), resourceId: parseInt(resourceId) });

}


CalendarApp.prototype.updateEvent = function (data) {
    this.events = this.events.filter(_ => _.id != data.id);
    this.events.push(Object.assign({}, data, {
        id: this.events.length + 1,
        resourceId: data.id,
        start: new Date(data.date + ' ' + data.startTime),
        end: new Date(data.date + ' ' + data.endTime),
        title: data.title
    }));
    this.showView();
    document.getElementById("cl-modal").style.display = "none";
}


CalendarApp.prototype.inputField = function (el, formEle, val) {
    var div = document.createElement("DIV");
    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);
    var input = document.createElement("INPUT");
    input.setAttribute('type', el.type);
    input.setAttribute('name', el.name);
    input.required = el.required ? true : false
    input.value = val ? val : el.type == 'number' ? 0 : '';
    if(el.onInputChange){
        input.onchange = (e) => {
            el.onInputChange(e.target.value);
        }
    }
    div.appendChild(input);
    div.setAttribute("class", "cl-modal-group");
    formEle.appendChild(div);
}

CalendarApp.prototype.timeField = function (el, formEle, selDate, val) {
    var selTime, selXm;
    if (val) {
        selTime = val.split(' ')[0];
        selXm = val.split(' ')[1];
    }
    if (el.duration && !val) {
        selDate.setMinutes(selDate.getMinutes() + el.duration);
    }
    var selDateFormat = this.getDateFormat(selDate);
    var div = document.createElement("DIV");
    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);

    var div2 = document.createElement("DIV");
    div2.setAttribute("class", "cl-modal-select");
    var select = document.createElement("SELECT");
    select.setAttribute('name', el.name + '.time');
    select.required = el.required ? true : false;
    this.timeOptions.map(opt => {
        var option = document.createElement("OPTION");
        option.text = opt.label;
        option.value = opt.value;
        if ((!val && opt.label === selDateFormat.time) || (val && selTime && opt.label === selTime)) {
            option.selected = true;
        }
        select.add(option);
    })
    div2.appendChild(select);

    var select = document.createElement("SELECT");
    select.setAttribute('name', el.name + '.xm');
    select.required = el.required ? true : false;
    var option = document.createElement("OPTION");
    option.text = 'AM';
    option.value = 'AM';
    if ((!val && selDateFormat.xm === 'AM') || (val && selXm && selXm === 'AM')) {
        option.selected = true;
    }
    select.add(option);
    var option = document.createElement("OPTION");
    option.text = 'PM';
    option.value = 'PM';
    if ((!val && selDateFormat.xm === 'PM') || (val && selXm && selXm === 'PM')) {
        option.selected = true;
    }
    select.add(option);
    div2.appendChild(select);

    div.appendChild(div2);
    div.setAttribute("class", "cl-modal-group");
    formEle.appendChild(div);
}

CalendarApp.prototype.dateField = function (el, formEle, selDate, val) {

    var div = document.createElement("DIV");

    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);

    var div2 = document.createElement("DIV");
    div2.setAttribute("class", "cl-modal-input-div");
    div.appendChild(div2);

    var input = document.createElement("INPUT");
    input.setAttribute('type', 'text');
    input.setAttribute('name', el.name);
    input.required = el.required ? true : false;
    input.setAttribute('id', 'cl-modal-' + el.name);
    input.setAttribute('autocomplete', 'off');
    input.value = val ? val : selDate.year + '-' + selDate.month + '-' + selDate.date;
    div2.appendChild(input);

    var img = document.createElement("IMG");
    img.setAttribute('src', 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxMy4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDE0OTQ4KSAgLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjUwcHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MCA1MCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8bGluZSBmaWxsPSJub25lIiBzdHJva2U9IiM1OTU5NTkiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHgxPSIxMy4xNzEiIHkxPSIxLjc1IiB4Mj0iMTMuMTcxIiB5Mj0iNy44MTQiLz4NCjxsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzU5NTk1OSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgeDE9IjMuMjc3IiB5MT0iMTMuODI1IiB4Mj0iNDcuNjM5IiB5Mj0iMTMuODI1Ii8+DQo8bGluZSBmaWxsPSJub25lIiBzdHJva2U9IiM1OTU5NTkiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHgxPSIxMy4yNzgiIHkxPSIyMi43NjEiIHgyPSIxNi4wNDMiIHkyPSIyMi43NjEiLz4NCjxsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzU5NTk1OSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgeDE9IjIzLjkxNiIgeTE9IjIyLjc2MSIgeDI9IjI2LjY4MiIgeTI9IjIyLjc2MSIvPg0KPGxpbmUgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNTk1OTU5IiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiB4MT0iMzQuMzQxIiB5MT0iMjIuNzYxIiB4Mj0iMzcuMTA3IiB5Mj0iMjIuNzYxIi8+DQo8bGluZSBmaWxsPSJub25lIiBzdHJva2U9IiM1OTU5NTkiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHgxPSIxMy4yNzgiIHkxPSIzMC4yMDgiIHgyPSIxNi4wNDMiIHkyPSIzMC4yMDgiLz4NCjxsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzU5NTk1OSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgeDE9IjIzLjkxNiIgeTE9IjMwLjIwOCIgeDI9IjI2LjY4MiIgeTI9IjMwLjIwOCIvPg0KPGxpbmUgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNTk1OTU5IiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiB4MT0iMzQuMzQxIiB5MT0iMzAuMjA4IiB4Mj0iMzcuMTA3IiB5Mj0iMzAuMjA4Ii8+DQo8bGluZSBmaWxsPSJub25lIiBzdHJva2U9IiM1OTU5NTkiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHgxPSIxMy4yNzgiIHkxPSIzNy40NDEiIHgyPSIxNi4wNDMiIHkyPSIzNy40NDEiLz4NCjxsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzU5NTk1OSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgeDE9IjIzLjkxNiIgeTE9IjM3LjQ0MSIgeDI9IjI2LjY4MiIgeTI9IjM3LjQ0MSIvPg0KPGxpbmUgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNTk1OTU5IiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiB4MT0iMzcuMTA3IiB5MT0iMS43NSIgeDI9IjM3LjEwNyIgeTI9IjcuODE0Ii8+DQo8Zz4NCgk8cGF0aCBmaWxsPSJub25lIiBzdHJva2U9IiM1OTU5NTkiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSJNNDcuNjM5LDQ0Ljc1YzAsMS4xLTAuOSwyLTIsMkg0Ljc0Ng0KCQljLTEuMSwwLTItMC45LTItMlY2LjcyOWMwLTEuMSwwLjktMiwyLTJoNDAuODkzYzEuMSwwLDIsMC45LDIsMlY0NC43NXoiLz4NCjwvZz4NCjwvc3ZnPg0K');
    img.setAttribute('class', 'cl-modal-date-img');
    img.setAttribute('id', 'cl-modal-date-img');
    div2.appendChild(img);

    var img2 = document.createElement("IMG");
    img2.setAttribute('src', 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxMy4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDE0OTQ4KSAgLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjUwcHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MCA1MCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgkNCgkJPGxpbmUgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNTk1OTU5IiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiB4MT0iMS45OTIiIHkxPSIxMy40NjUiIHgyPSIyMy45MjMiIHkyPSIyNi44MzEiLz4NCgkNCgkJPGxpbmUgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNTk1OTU5IiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiB4MT0iMjQuNjc0IiB5MT0iMjcuMjg1IiB4Mj0iNDYuNjA1IiB5Mj0iMTMuOTE5Ii8+DQoJPGc+DQoJCTxwYXRoIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzU5NTk1OSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIGQ9Ik00Ny4yOTksMzkuNDU2YzAsMS4xMTYtMC45MTUsMi4wMzEtMi4wMzMsMi4wMzENCgkJCUgzLjczMWMtMS4xMTcsMC0yLjAzLTAuOTE1LTIuMDMtMi4wMzFWOS41NDZjMC0xLjExOCwwLjkxMy0yLjAzMywyLjAzLTIuMDMzaDQxLjUzNGMxLjExOCwwLDIuMDMzLDAuOTE1LDIuMDMzLDIuMDMzVjM5LjQ1NnoiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg==');
    img2.setAttribute('class', 'cl-modal-date-img');
    img2.onclick = function () {
        const selEmail = document.getElementsByName('email');
        if (selEmail) {
            window.open(`mailto:${selEmail[0].value}`)
        }
    }.bind(this);
    div2.appendChild(img2);

    div.setAttribute("class", "cl-modal-date");
    formEle.appendChild(div);

    new this.FooPicker({
        id: 'cl-modal-' + el.name,
        dateFormat: 'yyyy-MM-dd',
        optionalId: 'cl-modal-date-img',
        disable: this.disableDate.map(el => {
            var getDate = this.getDateFormat(el.date);
            return getDate.year + '-' + getDate.month + '-' + getDate.date;
        }),
        showList: true,
        onChange: (date) => {
            // this.date = date;
            // this.onChangedate();
            // this.bookedList({ date: this.date, resourceId: parseInt(this.selEvent.resourceId) });
        },
        optionsId: 'cl-modal-date-img'
    }, this);
}


CalendarApp.prototype.getDateFormat = function (dateString) {
    let date = new Date(dateString)
    let yy = date.getFullYear()
    let mm = (date.getMonth() + 1) // because month starts from 0 to 11
    let dd = date.getDate() < 10 ? '0' + date.getDate() : date.getDate() // it's day of month
    let hours = (date.getHours() % 12) < 10 ? '0' + (date.getHours() % 12) : (date.getHours() % 12)
    let minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
    let time = (parseInt(hours) === 0 ? 12 : hours) + ':' + minutes
    let xm = date.getHours() >= 12 ? 'PM' : 'AM'
    return {
        fullDate: new Date(yy, mm - 1, dd),
        year: yy,
        month: mm,
        date: dd,
        hours: hours,
        minutes: minutes,
        time: time,
        xm: xm
    }
}

CalendarApp.prototype.searchField = function (el, formEle, val) {
    var [selected] = val && el && el.options && el.options.length > 0 ?
        el.options.filter(e => (e['value'] && e['value'].toString().toLowerCase() === val.toString().toLowerCase()) || (e.toString().toLowerCase() === val.toString().toLowerCase())) : [];

    var div = document.createElement("DIV");
    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);

    var hidden = document.createElement("INPUT");
    hidden.setAttribute('type', 'hidden');
    hidden.setAttribute('id', `cl-modal-tab-${el.name}`);
    hidden.value = (val && selected && selected.value) || !val ? false : true;
    label.appendChild(hidden);

    if (el.tab) {
        var div2 = document.createElement("DIV");

        div2.setAttribute("class", "cl-modal-tab")
        var btn = document.createElement("BUTTON");
        btn.appendChild(document.createTextNode('New'));
        btn.setAttribute('type', 'button');
        div2.appendChild(btn);

        var btn2 = document.createElement("BUTTON");
        btn2.appendChild(document.createTextNode('Existing'));
        btn2.setAttribute('type', 'button');
        div2.appendChild(btn2);

        if ((val && selected && selected.value) || !val) {
            btn2.setAttribute('class', 'cl-modal-selected');
        } else {
            btn.setAttribute('class', 'cl-modal-selected');
        }

        btn.onclick = (e) => {
            if ((val && el.disabled && el.disabled.edit) || (!val && el.disabled && el.disabled.create)) {
                hidden.value = (val && selected && selected.value) || !val ? false : true;
            } else {
                if (el.ref) {
                    el.ref.map(inp => {
                        const selInp = document.getElementsByName(inp);
                        if (selInp)
                            selInp[0].value = '';
                    })
                }

                hidden.value = hidden.value == 'false' ? true : false;
                if (hidden.value == 'false') {
                    btn.removeAttribute('class');
                    btn2.setAttribute('class', 'cl-modal-selected');
                } else {
                    btn2.removeAttribute('class');
                    btn.setAttribute('class', 'cl-modal-selected');
                }
            }
        };

        btn2.onclick = (e) => {
            if ((val && el.disabled && el.disabled.edit) || (!val && el.disabled && el.disabled.create)) {
                hidden.value = (val && selected && selected.value) || !val ? false : true;
            } else {
                hidden.value = hidden.value == 'false' ? true : false;
                if (hidden.value == 'false') {
                    btn.removeAttribute('class');
                    btn2.setAttribute('class', 'cl-modal-selected');
                } else {
                    btn2.removeAttribute('class');
                    btn.setAttribute('class', 'cl-modal-selected');
                }
            }
        };

        label.appendChild(div2);
    }

    var [filterVal] = el.options && el.options.filter(e => (e['value'] && e['value'].toString().toLowerCase() === val.toString().toLowerCase()) || (e.toString().toLowerCase() === val.toString().toLowerCase()));
    var input = document.createElement("INPUT");
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', el.name);
    input.value = val || '';
    div.appendChild(input);

    var div3 = document.createElement("DIV");
    div3.setAttribute("class", "cl-modal-input-div");
    div.appendChild(div3);

    var input = document.createElement("INPUT");
    input.setAttribute('type', 'select-autocomplete');
    input.setAttribute('autocomplete', 'off');
    input.setAttribute('id', el.name);
    if (el.link || el.view)
        input.classList.add("cl-modal-link");
    input.required = el.required ? true : false
    if ((val && el.disabled && el.disabled.edit) || (!val && el.disabled && el.disabled.create)) {
        input.setAttribute('readonly', true);
        input.style.background = '#c8c8c885';
    }
    input.value = (filterVal && filterVal.label) || filterVal || val || '';
    div3.appendChild(input);

    if (el.link) {
        var img = document.createElement('DIV');
        img.setAttribute('id', `cl-modal-link-${el.name}`);
        img.setAttribute('class', `cl-modal-link-img ${val && selected && selected.value ? 'cl-modal-link-enable' : ''}`);
        img.onclick = () => val && selected && selected.link ? window.open(selected.link, '_blank') : false;
        div3.appendChild(img);
    }

    if (el.view) {
        var view = document.createElement('DIV');
        view.setAttribute('id', `cl-modal-view-${el.name}`);
        view.setAttribute('class', `cl-modal-view-img`);
        view.onclick = () => input && input.value ? this.infoModal(el.label, input.value) : false;
        div3.appendChild(view);
    }

    div.setAttribute("class", "cl-modal-group cl-modal-searchField");
    formEle.appendChild(div);

    this.autocomplete(document.getElementById(el.name), document.getElementsByName(el.name)[0], el.options, el);
}



CalendarApp.prototype.multiSelectField = function (el, formEle, val) {
    var div = document.createElement("DIV");

    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);

    var input = document.createElement("INPUT");
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', el.name);
    input.value = val || '';
    div.appendChild(input);

    var div3 = document.createElement("DIV");
    div3.setAttribute("class", "cl-modal-input-div");
    div.appendChild(div3);

    var input = document.createElement("INPUT");
    input.setAttribute('type', 'select-autocomplete');
    input.setAttribute('autocomplete', 'off');
    input.setAttribute('class', 'dropdown');
    input.setAttribute('id', el.name);
    input.required = el.required ? true : false;
    input.value = val || '';
    div3.appendChild(input);

    div.setAttribute("class", "cl-modal-group cl-modal-searchField");
    formEle.appendChild(div);

    this.autoselection(document.getElementById(el.name), document.getElementsByName(el.name)[0], el.options, el);
}


CalendarApp.prototype.selectField = function (el, formEle, val) {
    var div = document.createElement("DIV");
    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);

    var select = document.createElement("SELECT");
    select.setAttribute('type', 'text');
    select.setAttribute('name', el.name);
    select.required = el.required ? true : false;
    if ((val && el.disabled && el.disabled.edit) || (!val && el.disabled && el.disabled.create)) {
        select.setAttribute('disabled', true);
        select.style.background = '#c8c8c885';
    }
    if (el.resourceColumn) {
        select.onchange = (e) => {
            if(el.onInputChange){
                el.onInputChange(e.target.value, el.refOptions || el.options, (arrData) => {
                    const htmlDiv = document.getElementById(`cl-modal-table-${el.refNode}`);
                    const callInp = el.refCol && el.refCol.name && document.getElementsByName(el.refCol.name) || false;
                    if(callInp && callInp[0]){
                        callInp[0].value = JSON.stringify(Object.assign({}, { [el.refCol.name]: arrData })).replace(/"/g,'★').replace(/\//g,"\\/");
                    }
                    if(htmlDiv){
                        this.createTable(Object.assign({}, el.refCol, { options: el.refOptions}), htmlDiv, arrData);
                    }


                });
            }

            this.bookedList({ date: this.date, resourceId: parseInt(e.target.value) });
            this.selEvent = Object.assign({}, this.selEvent, { date: this.date, resourceId: parseInt(e.target.value) });
        };
    } else {
        select.onchange = (e) => {
            if(el.onInputChange)
                el.onInputChange(e.target.value, el.refOptions || el.options, (arrData) => {
                    const htmlDiv = document.getElementById(`cl-modal-table-${el.refNode}`);
                    const callInp = el.refCol && el.refCol.name && document.getElementsByName(el.refCol.name) || false;
                    if(callInp && callInp[0]){
                        callInp[0].value = JSON.stringify(Object.assign({}, { [el.refCol.name]: arrData })).replace(/"/g,'★').replace(/\//g,"\\/");
                    }
                    if(htmlDiv){
                        this.createTable(Object.assign({}, el.refCol, { options: el.refOptions}), htmlDiv, arrData);
                    }
                });

        };
    }
    if (!el.optionsOnly) {
        var option = document.createElement("OPTION");
        option.text = ' ';
        option.value = '';
        option.disabled = true;
        option.selected = true;
        select.add(option);
    }
    if (el.options && el.options.length > 0) {
        el.options.map(opt => {
            var option = document.createElement("OPTION");
            option.text = opt.label || opt;
            option.value = opt.value || opt.value === 0 && '0' || opt;
            if (val && opt && ((opt.value && opt.value.toString() === val.toString()) || opt.toString() === val.toString())) {
                option.selected = true;
            }
            select.add(option);
        })
    }
    div.appendChild(select);

    if (el.legend) {
        var div2 = document.createElement('DIV');
        div2.innerHTML = el.legend;
        div.appendChild(div2);
    }

    div.setAttribute("class", "cl-modal-group");
    formEle.appendChild(div);
}

CalendarApp.prototype.tableField = function (el, formEle, val, edit) {
    var parsedVal = val && typeof val == 'string' && val.includes('★') && JSON.parse(val.replace(/★/g, '"')) || false;
    var div = document.createElement("DIV");
    var label = document.createElement("LABEL");
    label.innerHTML = el.label;
    div.appendChild(label);
    div.setAttribute("class", "cl-modal-group cl-modal-table-field")

    /*var button = document.createElement("INPUT");
    button.setAttribute("id", "cl-modal-add-table-field");
    button.setAttribute("type", "button");
    button.setAttribute("value", "ADD");
    button.onclick = this.tableAction.bind(this, el, 'add');
    div.appendChild(button);*/

    var div2 = document.createElement("DIV");
    div2.setAttribute("style", `margin-top: 20px`)
    div2.setAttribute("id", `cl-modal-table-${el.name}`);

    this.createTable(el, div2, parsedVal && parsedVal[el.value] || false, val, edit);

    div.appendChild(div2);

    var input = document.createElement('INPUT');
    input.setAttribute('name', el.name);
    input.setAttribute('type', 'hidden');
    input.value = val;
    div.appendChild(input);

    formEle.appendChild(div);
}

/*CalendarApp.prototype.tableAction = function (el, type, index) {
    const tableField = document.getElementsByName(el.name) && document.getElementsByName(el.name)[0];
    const parseData = tableField && tableField.value && typeof tableField.value == 'string' && tableField.value.includes('★') && JSON.parse(tableField.value.replace(/★/g, '"')) || false;
    let parseDataArr = parseData && parseData[el.name] || [];

    if(type === 'add'){
        const columnsData = el.columns.reduce((obj, d) => {
            if (d.type != 'action') {
                const field = document.getElementsByName(d.parent) && document.getElementsByName(d.parent)[0];
				const fieldId = document.getElementById(d.parent);
				const fieldVal = field && field.value || '';
				field.value = ''

				if(fieldId){
					fieldId.value = '';
				}
                return Object.assign({}, obj, { [d.value]: fieldVal });
            } else {
                return obj;
            }
        }, {});

        parseDataArr.push(columnsData);
    } else if(type === 'delete') {
        parseDataArr = parseDataArr.filter((parse, parseIndex) => parseIndex != index);
    }


	const vaidParseDataArr = parseDataArr.reduce((x, m) => {
		const valid = el.columns.reduce((b, sm) => {
			if(!m[sm.value] && sm.type != 'action'){
				return true;
			} else {
				return b;
			}
		}, false);
		if(!valid){
			x.push(m);
			return x;
		} else {
			return x;
		}
	}, []);

    const InputValue = JSON.stringify(Object.assign({}, { [el.name]: vaidParseDataArr })).replace(/"/g,'★').replace(/\//g,"\\/");
    tableField.value = InputValue;
    const htmlDiv = document.getElementById(`cl-modal-table-${el.name}`);

    if(el.onInputChange){
        el.onInputChange(InputValue, el.options, (arrData) => {
            const callInp = document.getElementsByName(el.name) || false;
            if(callInp && callInp[0]){
                callInp[0].value = JSON.stringify(Object.assign({}, { [el.name]: arrData })).replace(/"/g,'★').replace(/\//g,"\\/");
            }
            if(htmlDiv){
                this.createTable(el, htmlDiv, arrData);
            }
        });
    }

    if (htmlDiv) {
        this.createTable(el, htmlDiv, vaidParseDataArr);
    }

}*/

CalendarApp.prototype.tableAction = function (el, type, index, val) {
    const tableField = document.getElementsByName(el.name) && document.getElementsByName(el.name)[0];
    const parseData1 = tableField && tableField.value && typeof tableField.value == 'string' && tableField.value.includes('★') && JSON.parse(tableField.value.replace(/★/g, '"')) || false;
    const parseData = val && typeof val == 'string' && val.includes('★') && JSON.parse(val.replace(/★/g, '"')) || false; 
    let parseDataArr = parseData && parseData[el.name] || parseData1 && parseData1[el.name] || [];
    let selectedRow = [];
    let selectedRow1 = [];


    var parent = document.getElementsByClassName('cl-modal-group')[10];
    const newItem = document.createElement("LABEL");
    newItem.setAttribute('style', `line-height: 35px; height: 34px`);
    const division = document.createElement("DIV");
    const span = document.createElement("SPAN");
    const star = document.createTextNode(" ");
    span.appendChild(star);
    const text = document.createTextNode("Reason for Appointment  ");
    division.appendChild(text);
    division.appendChild(span);
    newItem.appendChild(division);
    const newItem1 = document.createElement("LABEL");
    newItem1.setAttribute('style', `line-height: 35px; height: 34px`);
    const division1 = document.createElement("DIV");
    const span1 = document.createElement("SPAN");
    const star1 = document.createTextNode("*");
    span1.setAttribute('style', `color:  #EA6225; font-size: 20px;`);
    span1.appendChild(star1);
    const text1 = document.createTextNode("Reason for Appointment  ");
    division1.appendChild(text1);
    division1.appendChild(span1);
    newItem1.appendChild(division1);
    var reason = document.getElementsByName("reason")[0];

    var checked = false;

    if(type === 'check') {
      const selectedId = index;
      selectedRow = parseDataArr.map(item => {
                            if(item.id == selectedId) {
                              item.checked = item.checked == undefined || item.checked == false ? true : false;
                            }
                            return item;
                          })

        selectedRow1 = parseDataArr.filter(item =>  item.checked == true)

        parseDataArr.map(item => {
        if(item.checked) {
            checked = true;
        }
        })
    }

    if(checked){
        parent.replaceChild(newItem, parent.childNodes[0]);
        reason.required = false
    } else {
        parent.replaceChild(newItem1, parent.childNodes[0]);
        reason.required = true
    }
    const InputValue = JSON.stringify(Object.assign({}, { [el.name]: selectedRow1 })).replace(/"/g,'★').replace(/\//g,"\\/");
    tableField.value = InputValue;
    const htmlDiv = document.getElementById(`cl-modal-table-${el.name}`);

    if(el.onInputChange){
        el.onInputChange(InputValue, el.options, (arrData) => {
            const callInp = document.getElementsByName(el.name) || false;
            if(callInp && callInp[0]){
                callInp[0].value = JSON.stringify(Object.assign({}, { [el.name]: arrData })).replace(/"/g,'★').replace(/\//g,"\\/");
            }
            if(htmlDiv){
                this.createTable(el, htmlDiv, arrData,  JSON.stringify(parseData || parseData1).replace(/"/g, '★').replace(/\//g,"\\/"));
            }
        });
    }

    if (htmlDiv) {
        this.createTable(el, htmlDiv, selectedRow,  JSON.stringify(parseData || parseData1).replace(/"/g, '★').replace(/\//g,"\\/"));
    }

}

CalendarApp.prototype.createTable = function (el, elem, arr = [], val1, edit) {
    elem.innerHTML = '';

    var table = document.createElement("TABLE");
    elem.appendChild(table);

    var thead = document.createElement("THEAD");
    table.appendChild(thead);

    var tr = document.createElement("TR");
    thead.appendChild(tr);

    el.columns.map(r => {
      if(edit) {
        // if(r.type != 'action') {
          var th = document.createElement("TH");
          var val = document.createTextNode(r.label);
          th.appendChild(val);
          tr.appendChild(th);
        // }
      } else {
        var th = document.createElement("TH");
        var val = document.createTextNode(r.label);
        th.appendChild(val);
        tr.appendChild(th);
      }
    });

    var tbody = document.createElement("TBODY");
    table.appendChild(tbody);

    var parent = document.getElementsByClassName('cl-modal-group')[10];
    const newItem = document.createElement("LABEL");
    newItem.setAttribute('style', `line-height: 35px; height: 34px`);
    const division = document.createElement("DIV");
    const span = document.createElement("SPAN");
    const star = document.createTextNode(" ");
    span.appendChild(star);
    const text = document.createTextNode("Reason for Appointment  ");
    division.appendChild(text);
    division.appendChild(span);
    newItem.appendChild(division);
    const newItem1 = document.createElement("LABEL");
    newItem1.setAttribute('style', `line-height: 35px; height: 34px`);
    const division1 = document.createElement("DIV");
    const span1 = document.createElement("SPAN");
    const star1 = document.createTextNode("*");
    span1.setAttribute('style', `color:  #EA6225; font-size: 20px;`);
    span1.appendChild(star1);
    const text1 = document.createTextNode("Reason for Appointment  ");
    division1.appendChild(text1);
    division1.appendChild(span1);
    newItem1.appendChild(division1);
    var reason = document.getElementsByName("reason")[0];   
    var checked = false


    //Calculate Ref
    el.columns.map(k => {
        if(k.ref && k.ref.length > 0 && el.options && el.options.length > 0 ){
            k.ref.map(f => {
                const refEl = document.getElementsByName(f) && document.getElementsByName(f)[0];
                if(refEl) {
                    const total = arr.reduce((b, ol) => {
                        const optVal = el.options.find(p => ol[k.value] === p[k.value])
                        return optVal && optVal[f] ? b + parseInt(optVal[f]) : b;
                    }, 0);
                    refEl.value = total;
                }
            });
        }
    });

    if (arr && arr.length > 0) {
        arr.map((a, i) => {
            var tr = document.createElement("TR");
            tbody.appendChild(tr);

            el.columns.map(c => {
                // if(edit) {
                //   if(c.type != 'action') {
                //       var td = document.createElement("TD");
                //       if(a.status == 'Proposed') {
                //         td.setAttribute('style', `color: #ea6225 !important`);
                //       }
                //       if(a.status == 'Accepted') {
                //         td.setAttribute('style', `color: #38a0f1 !important`);
                //       }
                //       tr.appendChild(td);
                //       var val = document.createTextNode(c.limit && a[c.value] ? a[c.value].substring(0, c.limit) + '...' : c.currency && a[c.value] ? currencyFormatter.format(a[c.value]) : a[c.value] != undefined ? a[c.value] : '');
                //       td.appendChild(val);

                //       if(c.view){
                //           var view = document.createElement('DIV');
                //           view.setAttribute('class', `cl-modal-view-img`);
                //           view.setAttribute('style', `float: right;height:37px;border:none`);
                //           view.onclick = () => a && a[c.value] ? this.infoModal(c.label, a[c.value]) : false;
                //           td.appendChild(view);
                //       }
                //   }
                // } else {
                  var td = document.createElement("TD");
                  if(a.status == 'Proposed') {
                    td.setAttribute('style', `color: #ea6225 !important`);
                  }
                  if(a.status == 'Accepted') {
                    td.setAttribute('style', `color: #38a0f1 !important`);
                  }
                  tr.appendChild(td);
                  if (c.type === 'action') {
                      var input = document.createElement("INPUT");
                      input.setAttribute("type", 'checkbox');
                      input.setAttribute("id", a.id);
                      if(a.checked != undefined) {
                        if(a.checked == true){
                          checked = true;
                          input.setAttribute("checked", a.checked);
                        } else {
                          input.setAttribute("defaultChecked", a.checked);
                        }
                      }
                      if (checked) {
                          parent.replaceChild(newItem, parent.childNodes[0]);
                          reason.required = false
                      } else {
                          parent.replaceChild(newItem1, parent.childNodes[0]);
                          reason.required = true
                      }
                      input.onclick = this.tableAction.bind(this, el, 'check', a.id, val1);
                      td.appendChild(input);
                  } else {
                      var val = document.createTextNode(c.limit && a[c.value] ? a[c.value].substring(0, c.limit) + '...' : c.currency && a[c.value] ? currencyFormatter.format(a[c.value]) : a[c.value] != undefined ? a[c.value] : '');
                      td.appendChild(val);

                      if(c.view){
                          var view = document.createElement('DIV');
                          view.setAttribute('class', `cl-modal-view-img`);
                          view.setAttribute('style', `float: right;height:37px;border:none`);
                          view.onclick = () => a && a[c.value] ? this.infoModal(c.label, a[c.value]) : false;
                          td.appendChild(view);
                      }
                  }
                // }
            });
        });

    } else {
        var tr = document.createElement("TR");
        tbody.appendChild(tr);

        var td = document.createElement("TD");
        td.setAttribute("colspan", el.columns.length);
        var val = document.createTextNode("No Records Found");
        td.appendChild(val);
        tr.appendChild(td);
    }

}

CalendarApp.prototype.onSubmit = function (e) {
    e.preventDefault();
    var inputs, select, index;
    inputs = e.target.getElementsByTagName('input');
    select = e.target.getElementsByTagName('select');
    var data = {};
    for (index = 0; index < inputs.length; ++index) {
        //data = Object.assign({}, data, { [inputs[index].name]: inputs[index].value.match(/^[0-9]+$/) != null ? parseInt(inputs[index].value) : inputs[index].value });
        data = Object.assign({}, data, { [inputs[index].name]: inputs[index].value });
    }
    for (index = 0; index < select.length; ++index) {
        //data = Object.assign({}, data, { [select[index].name]: select[index].value.match(/^[0-9]+$/) != null ? parseInt(select[index].value) : select[index].value });
        data = Object.assign({}, data, { [select[index].name]: select[index].value });
    }

    data.startTime = data['startTime.time'] + ' ' + data['startTime.xm'];
    data.endTime = data['endTime.time'] + ' ' + data['endTime.xm'];

    let procedureData = data.procedure != undefined ? JSON.parse(data.procedure.replace(/★/g, '"')) : []
    let filteredProcedure = procedureData.procedure && procedureData.procedure.length > 0 && procedureData.procedure.filter(item => item.checked === true).map(el => { delete el.checked; return el; });
    let selectedProcedure = {"procedure" : filteredProcedure}
    let submitProcedure = JSON.stringify(selectedProcedure).replace(/"/g,'★').replace(/\//g,"\\/");
    data.procedure = submitProcedure;

    if (this.api && this.api.url) {
        this.request('POST', data);
    } else if (this.props.onCreate) {
        this.props.onCreate(data);
    } else {
        this.createEvent(data);
    }
    setTimeout(()=>
    this.props.dispatch(loadRecordsDate(moment(this.date).format('YYYY-MM-DD'))), 1000)
}

CalendarApp.prototype.request = function (method, data) {
    const calendar = this;
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        const { response, readyState, status } = this;
        const result = response ? JSON.parse(response) : false;
        const errorMessage = (result && typeof result === 'string') ? result : (result[0] && result[0].message && typeof result[0].message === 'string') ? result[0].message : 'Error Occurred';
        if (readyState == 4 && status == 200) {
            calendar.closeModal('close');
            if (calendar.props.onChange)
                calendar.props.onChange(method, Object.assign({}, data, typeof result === 'object' && method === 'POST' ? result : {}));
        } else if (readyState == 4) {
            calendar.alertModal(errorMessage);
        }
    };
    xmlHttp.open(method, data && data.id ? `${this.api.url}/${data.id}` : this.api.url, true);
    Object.keys(this.api.headers).map(el => xmlHttp.setRequestHeader(el, this.api.headers[el]));
    xmlHttp.setRequestHeader('Content-Type', 'application/json');
    xmlHttp.send(JSON.stringify(data));
    return xmlHttp;
}


CalendarApp.prototype.createEvent = function (data) {
    this.events.push({
        id: this.events.length + 1,
        resourceId: data.id,
        start: new Date(data.date + ' ' + data.startTime),
        end: new Date(data.date + ' ' + data.endTime),
        title: data.title
    });
    this.showView();
    document.getElementById("cl-modal").style.display = "none";
}

CalendarApp.prototype.onEdit = function (e) {
    e.preventDefault();
    var inputs, select, index;
    inputs = e.target.getElementsByTagName('input');
    select = e.target.getElementsByTagName('select');
    var data = {};
    for (index = 0; index < inputs.length; ++index) {
        //data = Object.assign({}, data, { [inputs[index].name]: inputs[index].value.match(/^[0-9]+$/) != null ? parseInt(inputs[index].value) : inputs[index].value });
        data = Object.assign({}, data, { [inputs[index].name]: inputs[index].value });
    }
    for (index = 0; index < select.length; ++index) {
        //data = Object.assign({}, data, { [select[index].name]: select[index].value.match(/^[0-9]+$/) != null ? parseInt(select[index].value) : select[index].value });
        data = Object.assign({}, data, { [select[index].name]: select[index].value });
    }

    data.startTime = data['startTime.time'] + ' ' + data['startTime.xm'];
    data.endTime = data['endTime.time'] + ' ' + data['endTime.xm'];
    if (this.api && this.api.url) {
        this.request('PUT', Object.assign({}, this.selEvent, data));
    } else if (this.props.onUpdate) {
        this.props.onUpdate(Object.assign({}, this.selEvent, data));
    } else {
        this.updateEvent(Object.assign({}, this.selEvent, data));
    }
    setTimeout(()=>
    this.props.dispatch(loadRecordsDate(moment(this.date).format('YYYY-MM-DD'))), 1000)
}

var hasEventListener = window.addEventListener;
var weeks = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

CalendarApp.prototype.FooPicker = function () {

    var _self = this;
    var _id;

    var defaults = {
        className: 'foopicker',
        dateFormat: 'dd-MMM-yyyy',
        disable: []
    };

    if (arguments[0] && typeof arguments[0] === "object") {
        _self.options = extendOptions(defaults, arguments[0]);
        _id = _self.options.id;
    }

    // Show date picker on click
    _self.showPicker = function () {
        _self.buildPicker();
        var pickerField = document.getElementById(_id);
        var pickerDiv = document.getElementById('foopicker-' + _id);
        if (pickerField) {
            var datepicker = pickerField.getBoundingClientRect();
            var left = datepicker.left;
            var top = datepicker.bottom - 7;
            if (pickerDiv) {
                pickerDiv.style.position = 'fixed';
                pickerDiv.style.top = top + 'px';
                pickerDiv.style.left = left + 'px';
                pickerDiv.style.zIndex = '999999999';
            }
        }
    };

    // Hide date picker
    _self.hidePicker = function (event) {
        setTimeout(function () {
            var pickerDiv, pickerField;
            if (!_self.monthChange && !_self.isPickerClicked) {
                _self.removeListeners(_id);
                pickerDiv = document.getElementById('foopicker-' + _id);
                pickerDiv.removeEventListener('click', self.handlePickerClick, false);
                if (pickerDiv) {
                    pickerDiv.innerHTML = '';
                }
                _self.isDateClicked = false;
            } else if (!_self.isPickerClicked) {
                pickerField = document.getElementById(_self.options.id);
                if (pickerField) {
                    pickerField.focus();
                }
                _self.monthChange = false;
            }
        }, 210);
    };

    _self.todayDate = function (event) {
        _self.monthChange = false;
        var pickerField = document.getElementById(_id);
        var now = new Date(),
            year = now.getFullYear(),
            month = now.getMonth(),
            day = now.getDate();

        var date = format(_self, parseInt(day), parseInt(month) + 1, parseInt(year));
        _self.selectedDate = date;
        _self.selectedDay = parseInt(day);
        _self.selectedMonth = parseInt(month);
        _self.selectedYear = parseInt(year);
        if (pickerField) {
            pickerField.value = date;
            if (_self.options.showCalendar) {
                _self.date = new Date(parseInt(year), parseInt(month), parseInt(day));
                _self.onChangedate();
            } else if (_self.options.showList) {
                _self.options.onChange(new Date(parseInt(year), parseInt(month), parseInt(day)));
            }
            pickerField.focus();
            setTimeout(function () {
                pickerField.blur();
            }, 100);
        }

        _self.isPickerClicked = false;
        _self.isDateClicked = true;
        _self.hidePicker();
    }

    // Select date
    //new Date(year, month, day, hours, minutes, seconds, milliseconds)
    _self.selectDate = function (event) {
        _self.monthChange = false;
        var el = document.getElementById(event.target.id);
        var pickerField = document.getElementById(_id);
        if (el) {
            el.classList.add('foopicker__day--selected');
            var date = format(_self, el.dataset.day, el.dataset.month, el.dataset.year);
            _self.selectedDate = date;
            _self.selectedDay = parseInt(el.dataset.day);
            _self.selectedMonth = parseInt(el.dataset.month);
            _self.selectedYear = parseInt(el.dataset.year);
            if (pickerField) {
                pickerField.value = date;
                if (_self.options.showCalendar) {
                    _self.date = new Date(parseInt(el.dataset.year), parseInt(el.dataset.month) - 1, parseInt(el.dataset.day));
                    _self.onChangedate();
                } else if (_self.options.showList) {
                    _self.options.onChange(new Date(parseInt(el.dataset.year), parseInt(el.dataset.month) - 1, parseInt(el.dataset.day)));
                }
                pickerField.focus();
                setTimeout(function () {
                    pickerField.blur();
                }, 100);
            }
        }
        _self.isPickerClicked = false;
        _self.isDateClicked = true;
        _self.hidePicker();
    };

    _self.removeListeners = function (id) {
        var picker = document.getElementById(id);
        if (picker) {
            var el = picker.getElementsByClassName('foopicker__day');
            if (el && el.length) {
                for (var count = 0; count < el.length; count++) {
                    if (typeof el[count].onclick === "function") {
                        var elem = document.getElementById(id + '-foopicker__day--' + (count + 1));
                        removeEvent(elem, 'click', _self.selectDate, false);
                    }
                }
            }
        }
        document.removeEventListener('keydown', keyDownListener, false);

        var htmlRoot = document.getElementsByTagName('html')[0];
        htmlRoot.removeEventListener('click', _self.handleDocumentClick, false);
    };

    _self.changeMonth = function (event) {
        var className = event.target.className, positive = false;
        if (className.indexOf('foopicker__arrow--next') !== -1) {
            positive = true;
        }
        var month = positive ? _self.currentMonth + 1 : _self.currentMonth - 1;
        _self.updateCalendar(month);
    };

    _self.updateCalendar = function (newMonth, newYear) {
        _self.monthChange = true;
        var day = _self.currentDate;
        var year = newYear || _self.currentYear;
        _self.currentMonth = newMonth;
        Calendar.date = new Date(year, newMonth, day);
        var pickerDiv = document.getElementById('foopicker-' + _id);
        if (pickerDiv) {
            var datepicker = pickerDiv.querySelector('.foopicker');
            datepicker.innerHTML = Calendar.buildHeader() + Calendar.buildCalendar() + Calendar.buildFooter();
            _self.isPickerClicked = false;
            Calendar.removeListeners(_self);
            Calendar.addListeners(_self);
        }
    };

    _self.buildPicker = function () {
        var pickerDiv = document.getElementById('foopicker-' + _id);
        var pickerField = document.getElementById(_id);
        if (pickerDiv && !hasPicker(pickerDiv)) {
            var fragment, datepicker, calendar;
            fragment = document.createDocumentFragment();
            datepicker = document.createElement('div');
            // Add default class name
            datepicker.className = _self.options.className;

            // Date is selected show that month calendar
            var date;
            if (pickerField && pickerField.value && pickerField.value.length >= 10) {
                date = parse(_self, pickerField.value);
                _self.selectedDay = date.getDate();
                _self.selectedMonth = date.getMonth() + 1;
                _self.selectedYear = date.getFullYear();
                _self.selectedDate = format(_self, date.getDate(), date.getMonth() + 1, date.getFullYear());
            } else {
                _self.selectedDate = null;
                _self.selectedDay = null;
                _self.selectedMonth = null;
                _self.selectedYear = null;
                date = new Date();
            }
            Calendar.date = date;

            // Add calendar to datepicker
            datepicker.innerHTML = Calendar.buildHeader() + Calendar.buildCalendar() + Calendar.buildFooter();
            // Append picker to fragment and add fragment to DOM
            fragment.appendChild(datepicker);
            pickerDiv.appendChild(fragment);

            Calendar.addListeners(_self);

            // add event listener to handle clicks anywhere on date picker
            addEvent(pickerDiv, 'click', _self.handlePickerClick, false);
        }
        document.addEventListener('keydown', keyDownListener, false);

        // Close the date picker if clicked anywhere outside the picker element
        var htmlRoot = document.getElementsByTagName('html')[0];
        addEvent(htmlRoot, 'click', _self.handleDocumentClick, false);
    };

    _self.handlePickerClick = function (event) {
        event.stopPropagation();
        if (!_self.isDateClicked) {
            _self.isPickerClicked = true;
        }
    };

    _self.handleDocumentClick = function (event) {
        var pickerField = document.getElementById(_self.options.id);
        var optionField = document.getElementById(_self.options.optionsId);
        var pickerDiv = document.getElementById('foopicker-' + _self.options.id);
        _self.isPickerClicked = false;
        _self.monthChange = false;
        if (event.target !== pickerField && event.target !== pickerDiv && event.target !== optionField) {
            _self.hidePicker();
        }
    };

    _self.buildTemplate = function () {
        var oldPicker = document.getElementById('foopicker-' + _id);
        if (oldPicker)
            oldPicker.remove();

        var pickerDiv = document.createElement('div');
        pickerDiv.id = 'foopicker-' + _id;
        document.body.appendChild(pickerDiv);
        addListeners(_self);
    };

    _self.destroy = function () {
        var pickerDiv = document.getElementById('foopicker-' + _id);
        if (pickerDiv) {
            document.body.removeChild(pickerDiv);
        }
    };

    function keyDownListener() {
        _self.monthChange = false;
        _self.hidePicker();
    }

    _self.buildTemplate();
};

// Date formatter
function format(instance, day, month, year) {
    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;
    switch (instance.options.dateFormat) {
        case 'dd-MM-yyyy':
            return day + '-' + month + '-' + year;
        case 'dd-MMM-yyyy':
            return day + '-' + getShortMonth(parseInt(month)) + '-' + year;
        case 'dd.MM.yyyy':
            return day + '.' + month + '.' + year;
        case 'dd.MMM.yyyy':
            return day + '.' + getShortMonth(parseInt(month)) + '.' + year;
        case 'dd/MM/yyyy':
            return day + '/' + month + '/' + year;
        case 'dd/MMM/yyyy':
            return day + '/' + getShortMonth(parseInt(month)) + '/' + year;
        case 'MM-dd-yyyy':
            return month + '-' + day + '-' + year;
        case 'MM.dd.yyyy':
            return month + '.' + day + '.' + year;
        case 'MM/dd/yyyy':
            return month + '/' + day + '/' + year;
        case 'yyyy-MM-dd':
            return year + '-' + month + '-' + day;
        case 'yyyy-MMM-dd':
            return year + '-' + getShortMonth(parseInt(month)) + '-' + day;
        case 'yyyy.MM.dd':
            return year + '.' + month + '.' + day;
        case 'yyyy.MMM.dd':
            return year + '.' + getShortMonth(parseInt(month)) + '.' + day;
        case 'yyyy/MM/dd':
            return year + '/' + month + '/' + day;
        case 'yyyy/MMM/dd':
            return year + '/' + getShortMonth(parseInt(month)) + '/' + day;
        default:
            return day + '-' + getShortMonth(parseInt(month)) + '-' + year;
    }
}

// Currency formatter
const currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
});

// Date parser
function parse(instance, value) {
    var date, dateArray;
    switch (instance.options.dateFormat) {
        case 'dd-MM-yyyy':
            dateArray = value.split('-');
            date = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0]));
            return date;
        case 'dd-MMM-yyyy':
            dateArray = value.split('-');
            date = new Date(parseInt(dateArray[2]), getMonthNumber(dateArray[1]), parseInt(dateArray[0]));
            return date;
        case 'dd.MM.yyyy':
            dateArray = value.split('.');
            date = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0]));
            return date;
        case 'dd.MMM.yyyy':
            dateArray = value.split('.');
            date = new Date(parseInt(dateArray[2]), getMonthNumber(dateArray[1]), parseInt(dateArray[0]));
            return date;
        case 'dd/MM/yyyy':
            dateArray = value.split('/');
            date = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0]));
            return date;
        case 'dd/MMM/yyyy':
            dateArray = value.split('/');
            date = new Date(parseInt(dateArray[2]), getMonthNumber(dateArray[1]), parseInt(dateArray[0]));
            return date;
        case 'MM-dd-yyyy':
            dateArray = value.split('-');
            date = new Date(parseInt(dateArray[2]), parseInt(dateArray[0]) - 1, parseInt(dateArray[1]));
            return date;
        case 'MM.dd.yyyy':
            dateArray = value.split('.');
            date = new Date(parseInt(dateArray[2]), parseInt(dateArray[0]) - 1, parseInt(dateArray[1]));
            return date;
        case 'MM/dd/yyyy':
            dateArray = value.split('/');
            date = new Date(parseInt(dateArray[2]), parseInt(dateArray[0]) - 1, parseInt(dateArray[1]));
            return date;
        case 'yyyy-MM-dd':
            dateArray = value.split('-');
            date = new Date(parseInt(dateArray[0]), parseInt(dateArray[1]) - 1, parseInt(dateArray[2]));
            return date;
        case 'yyyy-MMM-dd':
            dateArray = value.split('-');
            date = new Date(parseInt(dateArray[0]), getMonthNumber(dateArray[1]), parseInt(dateArray[2]));
            return date;
        case 'yyyy.MM.dd':
            dateArray = value.split('.');
            date = new Date(parseInt(dateArray[0]), parseInt(dateArray[1]) - 1, parseInt(dateArray[2]));
            return date;
        case 'yyyy.MMM.dd':
            dateArray = value.split('.');
            date = new Date(parseInt(dateArray[0]), getMonthNumber(dateArray[1]), parseInt(dateArray[2]));
            return date;
        case 'yyyy/MM/dd':
            dateArray = value.split('/');
            date = new Date(parseInt(dateArray[0]), parseInt(dateArray[1]) - 1, parseInt(dateArray[2]));
            return date;
        case 'yyyy/MMM/dd':
            dateArray = value.split('/');
            date = new Date(parseInt(dateArray[0]), getMonthNumber(dateArray[1]), parseInt(dateArray[2]));
            return date;
        default:
            dateArray = value.split('-');
            date = new Date(parseInt(dateArray[2]), getMonthNumber(dateArray[1]), parseInt(dateArray[0]));
            return date;
    }
}

// Extend default options
function extendOptions(defaults, options) {
    var property;
    for (property in options) {
        if (options.hasOwnProperty(property)) {
            defaults[property] = options[property];
        }
    }
    return defaults;
}

// Build Calendar
var Calendar = {
    // Get current date
    date: new Date(),

    // Get day of the week
    day: function () {
        return this.date.getDay();
    },

    // Get today day
    today: function () {
        return this.date.getDate();
    },

    // Get current month
    month: function () {
        return this.date.getMonth();
    },

    // Get current year
    year: function () {
        return this.date.getFullYear();
    },

    getCurrentYear: function () {
        return new Date().getFullYear();
    },

    rowPadding: function () {
        var startWeekDay = getWeekDay(1, this.month(), this.year());
        return [6, 0, 1, 2, 3, 4, 5][startWeekDay];
    },

    // Build calendar header
    buildHeader: function () {
        return '<div class="foopicker__header">' +
            '<div class="foopicker__arrow foopicker__arrow--prev"></div>' +
            '<div class="foopicker__date">' + this.buildMonths() +
            '&nbsp;&nbsp;' + this.buildYears() + '</div>' +
            '<div class="foopicker__arrow foopicker__arrow--next"></div></div>';
    },

    // Build calendar footer
    buildFooter: function () {
        return '<div class="foopicker__footer">' +
            '<div class="foopicker__today">today</div>' +
            '</div>';
    },

    // Build months select
    buildMonths: function () {
        var elem = '<select class="foopicker__date--month">', month = this.month();
        for (var i = 0; i < months.length; i++) {
            elem += '<option value="' + i + '"';
            if (i === month) {
                elem += ' selected';
            }
            elem += '>' + months[i] + "</option>";
        }
        elem += '</select>';
        return elem;
    },

    // Build years select
    buildYears: function () {
        var elem = '<select class="foopicker__date--year">', currentYear = this.getCurrentYear();
        var year = this.year();
        for (var i = year - 20; i <= currentYear + 5; i++) {
            elem += '<option value="' + i + '"';
            if (i === year) {
                elem += ' selected';
            }
            elem += '>' + i + '</option>';
        }
        elem += '</select>';
        return elem;
    },

    // Build calendar body
    buildCalendar: function () {
        var index;
        var daysInMonth = getDaysInMonth(this.year(), this.month());
        var template = '<div class="foopicker__calendar"><table><tr>';
        for (index = 0; index < weeks.length; index++) {
            template += '<td><div class="foopicker__week">' + weeks[index] + '</div></td>';
        }
        template += '</tr><tr>';
        var columnIndex = 0, dayClass = '';
        var day = 0 - this.rowPadding();
        for (; day < daysInMonth; day++) {
            if (day < 0) {
                template += '<td></td>';
            } else {
                dayClass = day === (this.today() - 1) ? 'foopicker__day--today' : '';
                template += '<td><div class="foopicker__day ' + dayClass + '" ';
                template += 'data-day="' + (day + 1) + '" data-month="' + (this.month() + 1);
                template += '" data-year="' + this.year() + '" ';
                template += '>' + (day + 1) + '</div></td>';
            }
            columnIndex++;
            if (columnIndex % 7 === 0) {
                columnIndex = 0;
                template += '</tr><tr>';
            }
        }
        template += '</tr></table></div>';
        return template;
    },

    // Header click listeners
    addListeners: function (instance) {
        var id = instance.options.id;
        var pickerDiv = document.getElementById('foopicker-' + id);
        if (pickerDiv) {
            var prevBtn = pickerDiv.getElementsByClassName('foopicker__arrow--prev')[0];
            var nextBtn = pickerDiv.getElementsByClassName('foopicker__arrow--next')[0];
            addEvent(prevBtn, 'click', instance.changeMonth, false);
            addEvent(nextBtn, 'click', instance.changeMonth, false);

            var monthSelect = pickerDiv.getElementsByClassName('foopicker__date--month')[0];
            var yearSelect = pickerDiv.getElementsByClassName('foopicker__date--year')[0];
            var today = pickerDiv.getElementsByClassName('foopicker__footer')[0];

            // add event listener for month change
            addEvent(monthSelect, 'change', this.handleMonthChange.bind(null, instance), false);

            // add event listener for year change
            addEvent(yearSelect, 'change', this.handleYearChange.bind(null, instance), false);

            // add event listener for today date change
            addEvent(today, 'click', instance.todayDate, false);
        }

        this.changeInstanceDate(instance);
        this.modifyDateClass(instance);

        var el = pickerDiv.getElementsByClassName('foopicker__day');
        if (el && el.length) {
            for (var count = 0; count < el.length; count++) {
                if (typeof el[count].onclick !== "function") {
                    if (el[count].className && el[count].className.indexOf('foopicker__day--disabled') === -1) {
                        var elem = document.getElementById(id + '-foopicker__day--' + (count + 1));
                        addEvent(elem, 'click', instance.selectDate, false);
                    }
                }
            }
        }
    },

    handleMonthChange: function (instance, event) {
        instance.updateCalendar(event.target.value);
    },

    handleYearChange: function (instance, event) {
        instance.updateCalendar(instance.currentMonth, event.target.value);
    },

    removeListeners: function (instance) {
        var id = instance.options.id;
        var pickerDiv = document.getElementById('foopicker-' + id);
        if (pickerDiv) {
            var monthSelect = pickerDiv.getElementsByClassName('foopicker__date--month')[0];
            var yearSelect = pickerDiv.getElementsByClassName('foopicker__date--year')[0];

            monthSelect.removeEventListener('change', this.handleMonthChange, false);
            yearSelect.removeEventListener('change', this.handleYearChange, false);
        }
    },

    modifyDateClass: function (instance) {
        var id = instance.options.id, day = instance.selectedDay, currentDate, disabled;
        var date = new Date();
        var month = date.getMonth(), year = date.getFullYear(), dayClass;
        var pickerDiv = document.getElementById('foopicker-' + id);
        if (pickerDiv) {
            var el = pickerDiv.getElementsByClassName('foopicker__day');
            if (el && el.length) {
                for (var count = 0; count < el.length; count++) {
                    disabled = '';
                    currentDate = format(instance, el[count].dataset.day, el[count].dataset.month,
                        el[count].dataset.year);
                    if (instance.options.disable && instance.options.disable.indexOf(currentDate) !== -1) {
                        disabled = 'foopicker__day--disabled';
                    }

                    el[count].className = 'foopicker__day';
                    if ((count + 1) === day && this.month() === instance.selectedMonth - 1 &&
                        this.year() === instance.selectedYear) {
                        el[count].className += ' foopicker__day--selected' + ' ' + disabled;
                    } else {
                        if (el[count].dataset.day === this.today() && month === this.month() && year === this.year()) {
                            dayClass = ' foopicker__day--today';
                        } else {
                            dayClass = '';
                        }
                        el[count].className += dayClass + ' ' + disabled;
                    }

                    if ((count + 1) === date.getDate() && this.month() === month && this.year() === year) {
                        el[count].classList.add('foopicker__day--today');
                    }
                    el[count].id = id + '-foopicker__day--' + (count + 1);
                }
            }
        }
    },

    // Change date in instance
    changeInstanceDate: function (instance) {
        instance.currentDay = this.day();
        instance.currentDate = this.today();
        instance.currentMonth = this.month();
        instance.currentYear = this.year();
    }
};

function addListeners(picker) {
    var el = document.getElementById(picker.options.id);
    var optionsEl = document.getElementById(picker.options.optionsId);
    if (el) {
        addEvent(el, 'click', picker.showPicker, false);
        // addEvent(el, 'blur', picker.hidePicker, false);
        if (optionsEl) {
            addEvent(optionsEl, 'click', picker.showPicker, false);
        }
    }
}

function getMonths(month) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'];
    return month >= 0 ? months[month] : months;
}

function getShortMonth(month) {
    return months[parseInt(month) - 1];
}

function getMonthNumber(month) {
    var formatted = month.charAt(0).toUpperCase() + month.substr(1, month.length - 1).toLowerCase();
    return months.indexOf(formatted);
}

function getDaysInMonth(year, month) {
    return [31, isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
}

function getWeekDay(date, month, year) {
    return new Date(year, month, date).getDay();
}

// Check if current year is leap year
function isLeapYear(year) {
    return year % 100 === 0 ? year % 400 === 0 ? true : false : year % 4 === 0;
}

// Check if foopicker is already built and added to DOM
function hasPicker(el) {
    return el && el.querySelector('.foopicker') ? true : false;
}

// Function to add events
function addEvent(el, type, callback, capture) {
    if (hasEventListener) {
        if (el) {
            el.addEventListener(type, callback, capture);
        }
    } else {
        if (el) {
            el.attachEvent('on' + type, callback);
        }
    }
}

// Function to remove events
function removeEvent(el, type, callback, capture) {
    if (hasEventListener) {
        if (el) {
            el.removeEventListener(type, callback, capture);
        }
    } else {
        if (el) {
            el.detachEvent('on' + type, callback);
        }
    }
}

CalendarApp.prototype.convertDate = function (date, hours, minutes) {
    if (!date || (!(date instanceof Date))) date = new Date();
    var now = new Date(date),
        y = now.getFullYear(),
        m = now.getMonth(),
        d = now.getDate();

    return new Date(y, m, d, hours, minutes);
}

CalendarApp.prototype.onPaginate = function (type) {
    this.currentPage = (type === 'prev') ? this.currentPage - 1 : this.currentPage + 1;
    this.showView(this.date);
}

CalendarApp.prototype.resizeCalendar = function () {
    var calendar = document.getElementById('calendar');
    var tbody = document.getElementById('cl-modal-tbody');
    if (calendar) {
        calendar.style.height = `${document.body.clientHeight - 85}px`;
        tbody.style.maxHeight = `${document.body.clientHeight - 150}px`;
    }
}

CalendarApp.prototype.getElementByAttribute = function (attr, value, root) {
    root = root || document.body;
    if (root.hasAttribute(attr) && root.getAttribute(attr) == value) {
        return root;
    }
    var children = root.children,
        element;
    for (var i = children.length; i--;) {
        element = this.getElementByAttribute(attr, value, children[i]);
        if (element) {
            return element;
        }
    }
    return null;
}

CalendarApp.prototype.autocomplete = function (inp, inp2, arr, el) {
    const { name, ref, link, manual, tab, view, onInputChange, options, refOptions } = el;
    const callbackFunc = (arrData) => {
        const htmlDiv = document.getElementById(`cl-modal-table-${el.refNode}`);
        const callInp = el.refCol && el.refCol.name && document.getElementsByName(el.refCol.name) || false;
        if(callInp && callInp[0]){
            callInp[0].value = JSON.stringify(Object.assign({}, { [el.refCol.name]: arrData })).replace(/"/g,'★').replace(/\//g,"\\/");
        }
        if(htmlDiv){
            this.createTable(Object.assign({}, el.refCol, { options: el.refOptions}), htmlDiv, arrData);
        }
    };
    var currentFocus;
    inp.addEventListener("input", function (e) {
        var div, div2, i, val = this.value;
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        if (ref && Array.isArray(ref)) { inp2.value = inp.value }
        const width = link || view ? inp.offsetWidth + 30 : inp.offsetWidth;
        div = document.createElement("DIV");
        div.setAttribute("id", this.id + "autocomplete-list");
        div.setAttribute("class", "autocomplete-items");
        div.setAttribute("style", `width:${width}px !important`)
        this.parentNode.parentNode.appendChild(div);
        const selectedTab = document.getElementById(`cl-modal-tab-${name}`);
        for (i = 0; i < arr.length; i++) {
            if (((arr[i]['label'] && arr[i]['label'].toLowerCase().includes(val.toLowerCase())) || (typeof arr[i] === 'string' && arr[i].toLowerCase().includes(val.toLowerCase()))) && selectedTab && selectedTab.value === 'false') {
                div2 = document.createElement("DIV");
                div2.innerHTML = arr[i].label || arr[i];
                div2.innerHTML += "<input type='hidden' " + (arr[i].ref ? "data-ref='" + JSON.stringify(arr[i].ref).replace(/'/g, '&#39;') + "'" : "") + (arr[i].link ? "data-link='" + arr[i].link + "'" : "") + "data-label='" + (arr[i].label || arr[i]) + "' value='" + (arr[i].value || arr[i]) + "'>";
                div2.addEventListener("click", function (e) {
                    const currLabel = this.getElementsByTagName("input")[0].getAttribute('data-label');
                    const currValue = this.getElementsByTagName("input")[0].value;
                    inp.value = currLabel;
                    inp2.value = currValue;
                    if (ref && Array.isArray(ref)) {
                        const currRef = JSON.parse(this.getElementsByTagName("input")[0].getAttribute('data-ref'));
                        ref.map(refVal => {
                            var refInp = document.getElementsByName(refVal)[0];
                            if (refInp)
                                refInp.value = currRef && currRef[refVal];
                        })

                    }
                    if (link) {
                        const currLink = this.getElementsByTagName("input")[0].getAttribute('data-link');
                        setLink(currLink);
                    }
                    if(onInputChange){
                        onInputChange(currValue, refOptions || options, callbackFunc);
                    }
                    closeAllLists();
                });
                div.appendChild(div2);
            } else {
                clearLink();
            }
        }
    });

    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        } else if (e.keyCode == 9) {
            matchActive();
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    function setLink(currLink) {
        const linkInp = document.getElementById(`cl-modal-link-${name}`);
        if (linkInp && link) {
            linkInp.classList.add('cl-modal-link-enable');
            linkInp.onclick = () => window.open(currLink, '_blank');
        }
    }

    function clearLink() {
        const linkInp = document.getElementById(`cl-modal-link-${name}`);
        if (linkInp && link) {
            linkInp.classList.remove('cl-modal-link-enable');
            linkInp.onclick = () => false;
        }
    }

    function matchActive() {
        var selected = arr.filter(e => (e['value'] && e['value'].toString().toLowerCase() === inp2.value.toString().toLowerCase()) || (e.toString().toLowerCase() === inp2.value.toString().toLowerCase()))
        const selectedTab = document.getElementById(`cl-modal-tab-${name}`);

        if (selectedTab && selectedTab.value === 'true' && selected.length > 0) {
            clearLink();
        }
        if ((selected.length === 0 && !manual && selectedTab && selectedTab.value === 'false') || (selectedTab && selectedTab.value === 'true' && selected.length > 0)) {
            inp.value = '';
            inp2.value = '';
        } else if (selected.length > 0) {
            inp.value = selected[0].label || selected[0];
            inp2.value = selected[0].value || selected[0];
        } else {
            inp2.value = inp.value;
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
        matchActive();
    });
}

CalendarApp.prototype.autoselection = function (inp, inp2, arr, options) {
    var currentFocus;

    const selection = (e) => {
        var div, div2, i, val = inp.value;
        closeAllLists();
        currentFocus = -1;
        const width = inp.offsetWidth;
        div = document.createElement("DIV");
        div.setAttribute("id", inp.id + "selection-list");
        div.setAttribute("class", "selection-items");
        div.setAttribute("style", `width:${width}px !important`)
        inp.parentNode.parentNode.appendChild(div);
        const inpVal = inp.value.split(',').map(a => a.trim());
        for (i = 0; i < arr.length; i++) {
            div2 = document.createElement("DIV");
            div2.setAttribute('class', 'select-list')
            div2.innerHTML = arr[i].label || arr[i];
            div2.innerHTML += "<span " + "class='" + (inpVal.includes(arr[i]) ? 'tick' : '') + "' ></span><input type='hidden' " + "data-label='" + (arr[i].label || arr[i]) + "' value='" + (arr[i].value || arr[i]) + "'>";
            div2.addEventListener("click", function (e) {
                const currLabel = this.getElementsByTagName("input")[0].getAttribute('data-label');
                const currValue = this.getElementsByTagName("input")[0].value;
                inp.value = inpVal.length > 0 && inpVal.includes(currLabel) ? inpVal.filter(l => currLabel != l).join(',') : inp.value ? inp.value + ', ' + currLabel : currLabel;
                inp2.value = inpVal.length > 0 && inpVal.includes(currValue) ? inpVal.filter(b => currValue != b).join(',') : inp.value ? inp.value + ', ' + currValue : currValue;
                closeAllLists();
            });
            div.appendChild(div2);
        }
    }

    inp.addEventListener("click", selection);

    inp.addEventListener("input", selection);

    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "selection-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        } else if (e.keyCode == 9) {
            matchActive();
        }
    });



    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("selection-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("selection-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("selection-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    function matchActive() {
        var valueArr = inp.value ? inp.value.split(',') : [];
        if (valueArr.length > 0) {
            var filterArr = valueArr.filter(val => arr.includes(val.trim())).join(',');
            inp.value = filterArr;
            inp2.value = filterArr;
        } else {
            inp.value = '';
            inp2.value = '';
        }
    }

    document.addEventListener("click", function (e) {
        if (e.target.id === inp2.name) {
            matchActive();
        } else {
            closeAllLists();
            matchActive();
        }
    });
}

export default CalendarApp;