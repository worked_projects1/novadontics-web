

import React from 'react';
import App from 'containers/App';

export default function (name, pages, module, children, route) {
    const Template = App(pages); // eslint-disable-line new-cap
    const Page = route(name , process.env.PUBLIC_PATH || '', module.actions, module.selectors, children);
    const Component = () => <Template><Page /></Template>;

    Component.await = Page.await;

    return Component;
}