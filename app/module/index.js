/*
 * Module
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Col, Tab, Row, Button, Alert, Tabs } from '@sketchpixy/rubix';
import { createSelector } from 'reselect';
import ListPage from './repository/ListPage';
import { _map, columns } from './utils/library';
import { toUpper } from 'utils/tools';
import { selectUser } from 'blocks/session/selectors';
import moment from 'moment';
import styles from './styles.css';
import { FormattedMessage } from 'react-intl';
import messages from 'containers/ViewRecordPage/messages';
import { loadRecordsDate, updateRecordsDate, parseStructure } from './utils/sagas';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/Alert';
import Calendar from './fragment/Calendar';
import Filter from './fragment/Filter';
import { Cell } from './fragment/Cell';
import ShowAlert from './fragment/ShowAlert';
import DailyBrief from './fragment/DailyBrief';
import ModalForm from './fragment/ModalForm';
import { Link } from 'react-router';
import { clearAppointmentDetails } from 'blocks/patients/treatmentPlanDetails';
import { currencyFormatter } from 'utils/currencyFormatter.js';
import { browserHistory } from 'react-router';
import BirthdayList from './fragment/BirthdayList';
import ProviderFeeList from './fragment/ProviderFeeList';

export default function (name, path, actions, selectors, children) {
  const {
    selectLoading,
    selectRecordsMetaData,
    selectExternalRecord,
    selectRecords,
    selectChildren,
    selectDate,
    selectError,
    selectUpdateError
  } = selectors;

  class Module extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
      records: PropTypes.array,
      error: PropTypes.bool,
      loading: PropTypes.bool,
    };

    constructor(props) {
      super(props);
      this.state = { add: false, tab: true, pdf: false, showName: false, alertMessage: false }
      this.handleTab = this.handleTab.bind(this);
    }

    componentDidMount() {
      const { dispatch, activeChildren, date } = this.props;
      const [activeSaga] = children && children.length > 0 ? children.filter((a, el) => el === activeChildren) : [];

      if (window.location.pathname.indexOf('setup') > -1 && activeChildren === 13) {
        this.props.dispatch(actions.updateChildren(false));
        return false;
      }

      if (activeSaga)
        dispatch(activeSaga.actions.loadRecords());
      else
        dispatch(loadRecordsDate(moment.utc(date).format('YYYY-MM-DD')));

      document.getElementById('body').setAttribute('style', 'margin-top: 75px !important');
      document.getElementById('app').style.background = '#818284';
      document.getElementById('body').style.background = '#818284';
      document.getElementById('body').style.padding = '0px';
      if (this.calendar) {
        window.onresize = function () {
          this.calendar.onInit();
        }.bind(this);
      }
    }

    componentWillUnmount() {
      document.getElementById('app').style.background = '#e7e7e8';
      document.getElementById('body').style.background = '#e7e7e8';
      document.getElementById('body').style.paddingTop = '25px';
    }

    render() {
      const { error, loading, records = [], user = {}, date, activeChildren, metaData = {}, updateError, dispatch } = this.props;
      const dateFiltered = records.length != 0 ? records.filter(l => moment(l.date).format('YYYY/MM/DD') == date) : [];
      const totalAmount = dateFiltered.length != 0 ? dateFiltered.map(o => parseInt(o.amountToBeCollected)).reduce((a, c) => { return a + c }) : 0;
      const { tab, pdf, showName, alertMessage } = this.state;
      const { providers = [], operatories = [], procedure = [], patientList = [], dentalCarrierList = [], workingHoursList = [], providerFilter = [], nonWorkingDaysList = [], reasonList = [], staffMembersList = [], insuranceCompanyList = [], referralSourcesList = [] } = metaData;
      const { workingHours = [] } = workingHoursList[0] || [];
      const { providersFiltered } = providerFilter[0] || [];
      const staff = staffMembersList && staffMembersList.length > 0 ? staffMembersList.map(el => Object.assign({}, { label: el.name,value:`S:${el.id}`})) : [];
      const prov =  providers && providers.length > 0 ? providers.map(el => Object.assign({}, { label: el.name, value:`P:${el.id}`})) : [];
      const creator = prov.concat(staff);
      const providersMod = providers && providers.length > 0 && providersFiltered && providersFiltered.length > 0 ? providers.filter(_ => providersFiltered.split(',').filter(l => parseInt(l) === _.id).length > 0) : providers;
      const resources = (tab && operatories && operatories.length > 0) ? operatories.reduce((a, el) => { a.push({ id: el.id, title: el.name }); return a }, []) : providersMod && providersMod.length > 0 ? providersMod.reduce((a, el) => { a.push({ id: el.id, title: el.name }); return a }, []) : [];

      if (loading || (window.location.pathname.indexOf('setup') > -1 && activeChildren === 13)) {
        return <Spinner />
      }

      const appMetaData = {};
      appMetaData.providers = providers && providers.length > 0 ? providers.map(el => Object.assign({}, { label: el.name, value: el.id })) : [];
      appMetaData.operatories = operatories && operatories.length > 0 ? operatories.map(el => Object.assign({}, { label: el.name, value: el.id })) : [];
      appMetaData.patients = patientList && patientList.length > 0 ? patientList.map(el => Object.assign({}, { label: `${el.name} (${el.birthDate})`, value: el.id, ref: el, link: `/patients/${el.id}/form` })) : [];
      appMetaData.procedureType = procedure && procedure.length > 0 ? procedure.map(el => Object.assign({}, { label: el.procedureType.replace("'", ""), value: el.procedureType.replace("'", ""), ref: el })) : [];
      appMetaData.procedureCode = procedure && procedure.length > 0 ? procedure.map(el => Object.assign({}, { label: el.procedureCode, value: el.procedureCode, ref: Object.assign({}, el, { procedureType: el.procedureType.replace("'", ""), amountToBeCollected:el.privateFee || 0 }) })) : [];
      appMetaData.procedureList = procedure && procedure.length > 0 ? procedure.map(el => Object.assign({}, el, { procedureType: el.procedureType && el.procedureType.replace("'", "") || el.procedureType, amountToBeCollected: el.privateFee || 0 })) : [];
      appMetaData.reason = reasonList && reasonList.length > 0 ? reasonList.map(el => el.reason) : [];
      appMetaData.staffMembers = staffMembersList && staffMembersList.length > 0 ? staffMembersList.map(el => Object.assign({}, { label: el.name, value: el.id })) : [];
      appMetaData.insuranceCompany = insuranceCompanyList && insuranceCompanyList.length > 0 ? insuranceCompanyList.map(el => Object.assign({}, { label: el.name, value: el.id })) : [];
      appMetaData.dentalCarrier = dentalCarrierList && dentalCarrierList.length > 0 ? dentalCarrierList.map(el => Object.assign({}, { label: el.name, value: el.id })) : [];
      appMetaData.dentalCarrier = appMetaData.dentalCarrier && appMetaData.dentalCarrier.length > 0 ? appMetaData.dentalCarrier.reduce((a, el)=>{a.push(Object.assign({}, el)); return a;},[ { label: 'UCR', value: -1 }, { label: 'Private', value: 0 } ]) : [];
      appMetaData.referralSources = referralSourcesList && referralSourcesList.length > 0 ? referralSourcesList.map(el => Object.assign({}, { label: el.value, value: el.value })) : [];

      appMetaData.creatorList =  creator && creator.length > 0 ? creator.map(el => Object.assign({}, { label: el.label, value: `${el.value}` })) : [];
      const TabsMenu = window.location.pathname.indexOf('setup') > -1 ? children : children.concat({name: 'appointment'})
      return error ?
        <Alert danger><span><FormattedMessage {...messages.error} /></span></Alert> :
        activeChildren <= (_map.length - 1) ?
          (<Col>
            <Tabs className="practiceTabs" activeKey={activeChildren} onSelect={this.handleChildren.bind(this)}>
              {(TabsMenu || []).map((c, i) =>
                <Tab key={i} eventKey={i} title={c.name === 'appointment' ? 'Apt. Book' : _map && _map.length > 0 && _map.find(s => s.name === c.name).label.replace(' Setup', '')}>
                </Tab>
              )}
            </Tabs>
            <Tab.Container id="module" activeKey={activeChildren}>
              <Col className={`clearfix ${styles.children}`} sm={12}>
                <Tab.Content animation>
                  {(children || []).map((child, id) =>
                    <Tab.Pane key={id} eventKey={id}>
                      {activeChildren === id ?
                        <ListPage
                          id={id}
                          name={child.name}
                          path={`${process.env.PUBLIC_PATH || ''}/${child.name}`}
                          columns={columns && columns[child.name] ? columns[child.name] : []}
                          actions={child.actions}
                          selectors={child.selectors}
                          handleChange={this.handleChildren.bind(this)}
                          practiceId={user && user.practiceId}
                          schema={_map && _map.length > 0 && _map.filter(s => s.name === child.name)[0]}
                          params={this.props.params}
                        /> : null}
                    </Tab.Pane>
                  )}
                </Tab.Content>
              </Col>
            </Tab.Container>
          </Col>) : (
            <Col>
              {/*<Col xsHidden className={styles.tab} onClick={this.handleTab.bind(this)}>
                <Button className={!tab ? styles.selected : ''} >Provider</Button>
                <Button className={tab ? styles.selected : ''} >Operatories</Button>
                </Col>*/}
              <ProviderFeeList {...this.props} {...this.state} columns={columns['providerFeeList']}>
                {(click)=> <Col xsHidden className={styles.tab} onClick={() => click()}>{currencyFormatter.format(totalAmount)}</Col>}
              </ProviderFeeList>
              <Col xsHidden className={styles.links}>
                <ModalForm
                  fields={columns && columns['pdf'] && pdf ? columns['pdf'].filter(_ => _.value != 'providerId') : columns && columns['pdf'] ? columns['pdf'] : []}
                  form={`createRecord`}
                  onSubmit={this.handlePdf.bind(this)}
                  handleChange={this.handlePdfChange.bind(this)}
                  path={path}
                  metaData={{ providers: providers && providers.length > 0 ? providers.reduce((a, el) => { a.push(Object.assign({}, { label: el.name, value: el.id })); return a; }, [{ label: 'All', value: 'ALL' }]) : [], type: [{ id: 1, label: `Provider ${toUpper(name)} Report`, value: "scheduled" }, { id: 1, label: "Cancelled/No Show report", value: "canceled" }] }}
                  onRef={pdfForm => (this.pdfForm = pdfForm)}
                  config={{
                    title: "Generate Report", btnType: "image", imageIcon: [require(`img/module/report.svg`)], width: '28px', height: '28px', btnName: 'Done'
                  }}
                />
                <img width="28px" height="28px" onClick={this.handleName.bind(this)} src={require('img/module/show.svg')} />
                <Link onClick={() => browserHistory.push(`/setup`)}>
                  <img width="28px" height="28px" src={require('img/module/setup1.svg')} />
                </Link>
                <Filter {...this.props} >{(click) => <img width="28px" height="28px" id="filterImg" onClick={() => click()} src={require(`img/module/${providers && providers.length > 0 && providersFiltered && providersFiltered.length > 0 ? 'filter' : 'filter'}.svg`)} />}</Filter>
                <DailyBrief {...this.props} {...this.state}>{(click) => <img width="34px" height="34px" className={styles.briefImg} id="filterImg" onClick={() => click()} src={require(`img/module/dailyBrief.svg`)} />}</DailyBrief>
                <Link to="https://novadontics-uploads.s3.amazonaws.com/AppointmentBookManual.pdf" target="_blank">
                  <img width="30px" height="32px" className={styles.briefImg} src={require(`img/module/manual.svg`)} onClick={(e) => e.stopPropagation()} />
                </Link>
                <BirthdayList {...this.props} {...this.state} columns={columns['birthdayList']}>
                  {(click)=> <img width="30px" height="30px" className={styles.briefImg} id="birthdayImg" onClick={() => click()} src={require(`img/module/birthday.svg`)} />}
                </BirthdayList>
              </Col>
              {this.state.alertMessage ?
                <AlertMessage alert dismiss={() => this.setState({ alertMessage: false })}><FormattedMessage {...messages.pdfError} /></AlertMessage> : null}
              <Calendar
                resources={resources}
                events={records && records.length > 0 ? records.filter(el => el.status != 'Canceled' && el.status != 'No show' && (providersFiltered && providersFiltered.split(',').includes(el.providerId.toString()) || (!providersFiltered))).reduce((a, el) => {
                  a.push({
                    ...el, title: Cell({ tab: tab, showName: showName, ...el, ...this.props }), start: moment(el.date).format('YYYY/MM/DD ') + " " + moment(el.startTime, ["h:mm A"]).format("HH:mm"), end: moment(el.date).format('YYYY/MM/DD ') + " " + moment(el.endTime, ["h:mm A"]).format("HH:mm"), resourceId: tab ? parseInt(el.operatoryId) : parseInt(el.providerId), editable: true
                  });
                  return a
                }, []) : []}
                columns={
                  columns && columns['module'] ?
                    columns['module'].map(el => Object.assign({}, { name: el.value, options: (appMetaData && appMetaData[el.oneOf]) || el.oneOf || [], refOptions: (appMetaData && appMetaData[el.refOneOf]) || [], resourceColumn: (tab && el.value === 'operatoryId') || (!tab && el.value === 'providerId') ? true : false, ...el })).filter(el => el.editRecord) : []}
                showHours={workingHours}
                disableDate={nonWorkingDaysList}
                onChange={this.handleChange.bind(this)}
                onChangeDate={this.handleChangeDate.bind(this)}
                onRef={calendar => (this.calendar = calendar)}
                api={{ url: `${process.env.API_URL}/${name}`, headers: { 'X-Auth-Token': user.secret } }}
                date={date ? date : false}
                tab={tab}
                autofill={this.props.externalRecord}
                dispatch={this.props.dispatch}
                clear={() => dispatch(clearAppointmentDetails())} />
              {updateError ? <ShowAlert error={updateError} dismiss={this.handleError.bind(this)} /> : null}
            </Col>)

    }

    handleName() {
      this.setState({ showName: !this.state.showName });
    }

    handleError() {
      const { dispatch } = this.props;
      dispatch(actions.updateRecordError(false));
    }

    handlePdf(data) {
      const { user } = this.props;
      const pdfAuthToken = user && user.secret || user && user.token;
      const params = data.toJS();
      const { fromDate, toDate, providerId, type, format } = params || {};
      if (toDate < fromDate) {
        this.setState({ alertMessage: true });
      } else {
        this.pdfForm.close();
        window.open(`${process.env.API_URL}/${type === 'canceled' ? `cancelReport${format ? format : 'PDF'}` : `providerAppointment${format ? format : 'PDF'}`}?X-Auth-Token=${pdfAuthToken}&&fromDate=${fromDate ? fromDate : moment().format('YYYY-MM-DD')}&&toDate=${toDate ? toDate : moment().format('YYYY-MM-DD')}&&providerId=${providerId ? providerId : 'ALL'}`, '_blank');
      }
    }

    handlePdfChange() {
      this.setState(prevState => ({
        pdf: !prevState.pdf
      }));
    }

    handleChange(type, data) {
      const { dispatch } = this.props;
      
      if (type == 'POST') {
        dispatch(actions.createRecordSuccess(data));
      } else if (type == 'PUT') {
        dispatch(actions.updateRecordSuccess(data));
      } else if (type == 'DELETE') {
        dispatch(actions.deleteRecordSuccess(data.id));
      }
    }

    handleChangeDate(date) {
      const { dispatch } = this.props;
      setTimeout(() => dispatch(updateRecordsDate(moment(date).format('YYYY-MM-DD'))), 500);
      dispatch(actions.updateDate(moment(date).format('YYYY/MM/DD')));
    }

    handleTab() {
      this.setState({ tab: !this.state.tab });
      var calendar = this.calendar;
      setTimeout(function () { calendar.onInit() }, 500);
    }

    handleChildren(action) {
      if(action === 0) {
        action = false
      }
      this.props.dispatch(actions.updateChildren(action));
    }

  }

  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
    };
  }

  return connect(
    createSelector(
      selectLoading(),
      selectExternalRecord(),
      selectRecordsMetaData(),
      selectRecords(),
      selectChildren(),
      selectDate(),
      selectUser(),
      selectError(),
      selectUpdateError(),
      (loading, externalRecord, recordsMetaData,records, activeChildren, date, user, error, updateError) => ({ loading, externalRecord, metaData: recordsMetaData ? recordsMetaData.toJS() : {}, records: records ? records.toJS() : [], activeChildren, date, user, error, updateError }),
    ),
    mapDispatchToProps,
  )(Module);
}
